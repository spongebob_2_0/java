package Demo1;

import java.util.*;

/*
 * public class ListNode {
 *   int val;
 *   ListNode next = null;
 * }
 */

public class Solution3 {
    /**
     * 
     * @param head1 ListNode类 
     * @param head2 ListNode类 
     * @return ListNode类
     */
    public ListNode addInList (ListNode head1, ListNode head2) {
        // write code here
        if(head1 == null) {
            return head2;
        }
        if(head2 == null) {
            return head1;
        }
        head1 =reverseList(head1);
        head2 =reverseList(head2);
        ListNode pHead =new ListNode(0);    
        ListNode prev =pHead;
        int count =0;
        while(head1 != null || head2 != null || count !=0) {
            int val1 =head1 == null?0:head1.val;
            int val2 =head2 == null?0:head2.val;
            int tmp =val1 + val2 + count;
            count =tmp/10;
            tmp %=10;
            prev.next =new ListNode(tmp);
            prev =prev.next;
            if(head1 != null) {
                head1 =head1.next;
            }
            if(head2 != null) {
                head2 =head2.next;
            }
        }
        return reverseList(pHead.next);
    }
    private ListNode reverseList(ListNode head) {
        if(head.next == null){
            return head;
        }
        ListNode cur =head.next;
        head.next =null;
        while( cur != null) {
            ListNode curNext =cur.next;
            cur.next =head;
            head =cur;
            cur =curNext;
        }
        return head;
    }
}