package Demo1;

import java.util.Scanner;

public class Main6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int start = scanner.nextInt();
        int end = scanner.nextInt();
        method(start,end);
    }

    public static void method(int start, int end) {
        int count=0;

        //write your code here......
        if(start > end) {
            int tmp =start;
            start =end;
            end =tmp;
        }
        for(int i =start; i<=end; i++) {
            int j =2;
            for(; j<i; j++) {
                if(i%j == 0) {
                    break;
                }
            }
            if(i == j && i>2) {
                count++;
            }
        }

        System.out.println(start+"到"+end+"之间有"+count+"个大于2的素数");
    }
}