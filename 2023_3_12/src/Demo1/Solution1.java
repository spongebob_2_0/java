package Demo1;

import java.util.*;

class ListNode {
    int val;
    ListNode next = null;

    public ListNode(int val) {
        this.val = val;
    }
}

public class Solution1 {
    /**
     *
     * @param head ListNode类
     * @param m int整型
     * @param n int整型
     * @return ListNode类
     */

    /*
     * public class ListNode {
     *   int val;
     *   ListNode next = null;
     * }
     */

    public class Solution2 {
        /**
         *
         * @param head ListNode类
         * @param m int整型
         * @param n int整型
         * @return ListNode类
         */
        public ListNode reverseBetween (ListNode head, int m, int n) {
            // write code here
            if(head == null) {
                return null;
            }
            if(head.next == null || m == n) {
                return head;
            }
            ListNode pHead =new ListNode(-1);
            ListNode prev =pHead;
            ListNode cur =head;
            for(int i =0; i<m-1; i++) {
                prev =cur;
                cur =cur.next;
            }
            ListNode cur1 =cur;
            ListNode next;
            for(int i =0; i<n-m; i++) {
                next =cur.next;
                cur.next =next.next;
                next.next =cur1;
                cur1 =next;
                prev.next =next;
            }
            return pHead.next;
        }
    }
    public static ListNode reverseBetween(ListNode head, int m, int n) {
        // write code here
        if(head == null) {
            return null;
        }
        if(head.next == null) {
            return head;
        }
        ListNode prev=head;
        int size =0;
        while (prev != null) {
            prev =prev.next;
            size++;
        }
        ListNode start =head;
        ListNode end =head;
        int countStart =1;
        int countEnd =1;
        while(start != null){
            if(countStart == m){
                break;
            }
            start =start.next;
            countStart++;
        }
        while(end != null){
            if(countEnd == n){
                break;
            }
            end =end.next;
            countEnd++;
        }
        ListNode cur =start.next;
        start.next =end.next;
        ListNode curHead =start;
        while(cur !=null && cur !=end.next) {
            ListNode curNext =cur.next;
            cur.next =curHead;
            curHead =cur;
            cur =curNext;
        }
        if(countStart == 1){
            return curHead;
        }
        if(countStart == size) {
            return head;
        }
        head.next =curHead;
        return head;
    }

    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int m= scanner.nextInt();
        int n= scanner.nextInt();
        ListNode head1 =new ListNode(1);
        ListNode head2 =new ListNode(2);
        ListNode head3 =new ListNode(3);
        head1.next =head2;
        head2.next =head3;
        ListNode cur =reverseBetween(head1,m,n);
        while (cur != null){
            System.out.print(cur.val+" ");
            cur =cur.next;
        }
    }
}