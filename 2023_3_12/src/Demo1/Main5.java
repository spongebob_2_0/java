package Demo1;

import java.util.Scanner;

public class Main5 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        Main5 main = new Main5();
        int number = console.nextInt();
        System.out.println(main.palindromeNumber(number));
    }

    public Boolean palindromeNumber(int number) {

        //write your code here......
        String str =String.valueOf(number);
        int left =0;
        int right =str.length()-1;
        while (left <right) {
            if(str.charAt(left) == str.charAt(right)) {
                left++;
                right--;
            }else {
                return false;
            }
        }
        return true;
    }
}