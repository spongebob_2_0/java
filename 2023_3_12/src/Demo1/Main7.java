package Demo1;

import java.util.Arrays;
import java.util.Scanner;
public class Main7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
         int[] arr = new int[7];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.nextInt();
        }
        scanner.close();

        //write your code here......
        for(int i = 0; i<arr.length-1; i++) {
            boolean flg =true;
            for(int j =0; j<arr.length-1-i; j++) {
                if(arr[j] > arr[j+1]) {
                    int tmp =arr[j];
                    arr[j] =arr[j+1];
                    arr[j+1] =tmp;
                    flg =false;
                }
            }
            if(flg == true) {
                break;
            }
        }

        for (int k = 0; k < arr.length; k++) {
            System.out.print(arr[k]+" ");
        }
    }
}