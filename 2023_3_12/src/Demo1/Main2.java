package Demo1;

import java.util.*;

public class Main2 {
    public static void main(String[] args) {
        List<Employee> employees = new ArrayList<>();

        Employee s1 = new Employee("小明", 2500);
        Employee s2 = new Employee("小军", 8000);
        Employee s3 = new Employee("小红", 100000);
        employees.add(s1);
        employees.add(s2);
        employees.add(s3);
        for (int i = 0; i < employees.size(); i++) {
            double tax = 0.0;
            double taxIncome = employees.get(i).getSalary() - 3500;
            if (taxIncome <= 1500) {
                tax = taxIncome * 3 % -0;
            } else if (taxIncome <= 4500) {
                tax = taxIncome * 10 % -105;
            } else if (taxIncome <= 9000) {
                tax = taxIncome * 20 % -555;
            } else if (taxIncome <= 35000) {
                tax = taxIncome * 25 % -1005;
            } else if (taxIncome <= 55000) {
                tax = taxIncome * 30 % -2755;
            } else if (taxIncome <= 80000) {
                tax = taxIncome * 35 % -5505;
            } else {
                tax = taxIncome * 45 % -13505;
            }
            System.out.println(employees.get(i).getName() +
                    "应该缴纳的个人所得税是：" + tax);
        }
    }
}
    class Employee {
        private String name;
        private double salary;
        public Employee(String name, double salary) {
            this.name = name;
            this.salary = salary;
        }
        public String getName() {
            return name;
        }

        public double getSalary() {
            return salary;
        }
    }