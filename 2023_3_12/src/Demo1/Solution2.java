package Demo1;

import java.util.*;

/*
 * public class ListNode {
 *   int val;
 *   ListNode next = null;
 * }
 */

public class Solution2 {
    /**
     * 
     * @param head ListNode类 
     * @param m int整型 
     * @param n int整型 
     * @return ListNode类
     */
    public ListNode reverseBetween (ListNode head, int m, int n) {
        // write code here
        if(head == null) {
            return null;
        }
        if(head.next == null || m == n) {
            return head;
        }
        ListNode pHead =new ListNode(-1);
        ListNode prev =pHead;
        ListNode cur =head;
        for(int i =0; i<m-1; i++) {
            prev =cur;
            cur =cur.next;
        }
        ListNode cur1 =cur;
        ListNode next;
        for(int i =0; i<n-m; i++) {
            next =cur.next;
            cur.next =next.next;
            next.next =cur1;
            cur1 =next;
            prev.next =next;
        }
        return pHead.next;
    }
}