package test1;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-04-22
 * Time: 10:44
 */
public class Test9 {
    //快速排序
    public static void quickSort(int[] array) {
        quick(array,0,array.length - 1);
    }
    public static void quick(int[] array,int start, int end) {
        if(start >= end) { // 为什么要大于end？如果是有序的情况，== 就会失效
            return;
        }
        int pivot = partition(array,start,end);
        quick(array,0,pivot - 1);
        quick(array,pivot+1,end);
    }
    //前后指针法 【了解即可】
    private static int partition(int[] array,int left,int right) {
        int prev = left ;
        int cur = left+1;
        while (cur <= right) {
            if(array[cur] < array[left] && array[++prev] != array[cur]) {
                swap(array,cur,prev);
            }
            cur++;
        }
        swap(array,prev,left);
        return prev;
    }
    private static void swap(int[] array, int i, int j) {
        int tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
    }
    public static void main(String[] args) {
        int[] array ={9,8,7,6,5,4,3,2,1};
        quickSort(array);
        System.out.println(Arrays.toString(array));
    }
}
