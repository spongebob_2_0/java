package test1;

import java.util.Comparator;
import java.util.PriorityQueue;

class Solution {
     public int[] smallestK(int[] arr, int k) {
         int[] vec = new int[k];
         if (vec == null || k == 0) {
             return vec;
         }
         //传入比较器，按照大根堆调整
         PriorityQueue<Integer> queue = new PriorityQueue<Integer>(new Comparator<Integer>() {
            public int compare(Integer num1, Integer num2) {
                 return num2 - num1;
             }
         });
         //存入K个 元素
         for (int i = 0; i < k; ++i) {
             queue.offer(arr[i]);
         }
         //比较堆顶元素与剩余n - k个元素的值的大小
         //如果堆顶元素较大，则弹出堆顶，重新调整，元素入堆
         for (int i = k; i < arr.length; ++i) {
             if (queue.peek() > arr[i]) {
                 queue.poll();
                 queue.offer(arr[i]);
             }
         }
         //将堆中元素存入数组中
         for (int i = 0; i < k; ++i) {
             vec[i] = queue.poll();
         }
         return vec;
     }
 }