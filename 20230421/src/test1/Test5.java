package test1;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-04-22
 * Time: 2:33
 */
public class Test5 {
        //直接插入排序
        public static void insertSort(int[] arr) {
            for (int i = 1; i < arr.length; i++) {
                int tmp = arr[i];
                int j;
                for (j = i - 1; j >= 0 ; j--) {
                    if(arr[j] > tmp) {
                        //比插入的数大就向后移
                        arr[j + 1] = arr[j];
                    }else {
                        //比插入的数小，跳出循环
                        break;
                    }
                }
                //tmp放到比插入的数小的数的后面
                arr[j + 1] = tmp;
            }
        }

    public static void main(String[] args) {
        int[] array ={9,8,7,6,5,4,3,2,1};
        insertSort(array);
        System.out.println(Arrays.toString(array));
    }
}
