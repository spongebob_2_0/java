package test1;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-04-22
 * Time: 2:29
 */
public class Test4 {
    public static void shellSort(int[] array) {
        // gap初始值设置成数组长度的一半
        int gap = array.length >> 1;
        // gap 为 1 的时候直接插入排序
        while (gap > 1) {
            shell(array, gap);
            gap >>= 1; // 更新 gap 值 等价于 -> gap /= 2;
        }
        //整体进行插入排序
        shell(array, 1);
    }
    private static void shell(int[] array, int gap) {
        for (int i = gap; i < array.length; i++) {
            int tmp = array[i];
            int j = i - gap;
            for (; j >= 0; j -= gap) {
                if (array[j] > tmp) {
                    //比插入的数大就向后移动gap,因为每一组数据元素之间的间隔大小是gap,
                    array[j + gap] = array[j];
                } else {
                    //比插入的数小，跳出循环
                    break;
                }
            }
            //tmp放到比插入的数小的数的后面
            array[j + gap] = tmp;
        }
    }

    public static void main(String[] args) {
        int[] array ={9,8,7,6,5,4,3,2,1};
        shellSort(array);
        System.out.println(Arrays.toString(array));
    }
}
