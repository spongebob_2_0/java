package test1;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-04-22
 * Time: 6:08
 */
public class Test6 {
    //选择排序
    public static void selectSort(int[] arr) {
        /*判断数组为空或为一个元素的情况，即边界检查*/
        if (arr == null || arr.length < 2) {
            return;
        }

        /*每次要进行比较的两个数，的前面那个数的下标*/
        for (int i = 0; i < arr.length - 1; i++) {
            //min变量保存该趟比较过程中，最小元素所对应的索引，
            //先假设前面的元素为最小元素
            int minIndex = i;
            /*每趟比较，将前面的元素与其后的元素逐个比较*/
            for (int j = i + 1; j < arr.length; j++) {
                //如果后面的元素小，将后面元素的索引极为最小值的索引
                if(arr[j] < arr[minIndex]) {
                    minIndex = j;
                }
            }
            //然后交换此次查找到的最小值和原始的最小值
            swap(arr, i, minIndex);
        }
    }

    public static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }
    public static void main(String[] args) {
        int[] array ={9,8,7,6,5,4,3,2,1};
        selectSort(array);
        System.out.println(Arrays.toString(array));
    }
}
