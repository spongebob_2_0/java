package test1;

import java.util.Arrays;


/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-04-22
 * Time: 22:06
 */
public class MergeSort2 {
    //非递归实现归并排序
    public static void mergeSort(int[] array) {
        int gap = 1;
        while (gap < array.length) {
            // i += gap * 2 当前gap组的时候，去排序下一组
            for (int i = 0; i < array.length; i += gap * 2) {
                int left = i;
                int mid = left+gap-1;//有可能会越界
                if(mid >= array.length) {
                    mid = array.length-1;
                }
                int right = mid+gap;//有可能会越界
                if(right>= array.length) {
                    right = array.length-1;
                }
                merge(array,left,right,mid);
            }
            //当前为2组有序  下次变成4组有序
            gap *= 2;
        }
    }
    //排序 合并子序列
    public static void merge(int[] array, int start, int end, int mid){
        int s1 = start; //初始化s1，左边有序序列的初始索引
        int s2 = mid + 1; //初始化s2，右边有序序列的初始索引
        int k = 0; //指向tmp数组的当前索引
        int[] tmp =new int[end -start +1];
        while (s1 <= mid && s2 <= end) {
            if(array[s1] <= array[s2]) {
                tmp[k++] =array[s1++];
            }else {
                tmp[k++] =array[s2++];
            }
        }
        //将剩余子序列中的元素挪到新数组中
        while (s1 <= mid) {
            tmp[k++] =array[s1++];
        }
        while (s2 <= end) {
            tmp[k++] =array[s2++];
        }
        for (int i = 0; i < tmp.length; i++) {
            //i + start 是防止拷贝第二段子序列时，覆盖第一段，且达不到想到的效果
            array[i+start] =tmp[i];
        }
    }
    public static void main(String[] args) {
        int[] array = {9,8,7,6,5,4,3,2,1};
        mergeSort(array);
        System.out.println(Arrays.toString(array));
    }
}
