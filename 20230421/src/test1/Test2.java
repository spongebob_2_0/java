package test1;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-04-22
 * Time: 1:21
 */
public class Test2 {
    //堆排序
    public static void heapSort(int[] array) {
        createHeap(array);//O(N)
        int end = array.length - 1;
        //O(N*log(N))
        while(end > 0) {
            swap(array, 0, end);
            //从0下标开始调整,向下调整的结束条件是小于end,所以下标end的位置不参与调整
            shiftDown(array, 0, end);
            end--;
        }
    }
    //向下调整
    private static void shiftDown(int[] array, int root, int len) {
        int child = 2 * root + 1;
        while(child < len) {
            if(child + 1 < len && array[child] < array[child + 1]) {
                child++;
            }
            if(array[child] > array[root]) {
                swap(array, child, root);
                root = child;
                child = 2 * root + 1;
            } else {
                break;
            }
        }
    }
    //建大根堆 O(n)
    private static void createHeap(int[] array) {
        for (int p = (array.length-1-1)/2; p >= 0 ; p--) {
            shiftDown(array, p, array.length);
        }

    }
    private static void swap(int[] array, int child, int root) {
        int tmp = array[child];
        array[child] = array[root];
        array[root] = tmp;
    }
    public static void main(String[] args) {
        int[] array ={9,8,7,6,5,4,3,2,1};
        heapSort(array);
        System.out.println(Arrays.toString(array));
    }
}
