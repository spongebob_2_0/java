package test1;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-04-22
 * Time: 13:44
 */
public class Test10 {
    //快速排序（非递归）
    public static void quickSort(int[] array){
        int left =0;
        int right =array.length-1;
        Deque<Integer> stack = new LinkedList<>();
        int pivot = partition(array,left,right);
        if(pivot > left+1){
            //说明左边有两个或两个以上数据
            stack.push(left);
            stack.push(pivot-1);
        }
        //说明右边有两个或两个以上数据
        if(pivot < right-1){
            stack.push(pivot+1);
            stack.push(right);
        }
        while (!stack.isEmpty()){
            right = stack.pop();
            left = stack.pop();

            pivot = partition(array,left,right);
            if(pivot > left+1){
                //说明左边有两个或两个以上数据
                stack.push(left);
                stack.push(pivot-1);
            }
            if(pivot < right-1){
                stack.push(pivot+1);
                stack.push(right);
            }
        }
    }
    //挖坑法
    private static int partition(int[] array, int left, int right) {
        int tmp = array[left];
        while (left < right) { // 一趟排序
            while (left < right && array[right] >= tmp) {
                right--;
            }
            array[left] = array[right];
            while (left < right && array[left] <= tmp) {
                left++;
            }
            array[right] = array[left];
        }
        array[left] = tmp;
        return left;
    }
    public static void main(String[] args) {
        int[] array ={9,8,7,6,5,4,3,2,1};
        quickSort(array);
        System.out.println(Arrays.toString(array));
    }
}
