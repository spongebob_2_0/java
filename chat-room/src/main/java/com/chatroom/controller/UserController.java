package com.chatroom.controller;

import java.util.List;

import com.chatroom.entity.query.UserQuery;
import com.chatroom.entity.po.User;
import com.chatroom.entity.vo.ResponseVO;
import com.chatroom.service.UserService;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 *  Controller
 */
@RestController("userController")
@RequestMapping("/user")
public class UserController extends ABaseController{

	@Resource
	private UserService userService;
	/**
	 * 根据条件分页查询
	 */
	@RequestMapping("/loadDataList")
	public ResponseVO loadDataList(UserQuery query){
		return getSuccessResponseVO(userService.findListByPage(query));
	}

	/**
	 * 新增
	 */
	@RequestMapping("/add")
	public ResponseVO add(User bean) {
		Integer result = userService.add(bean);
		return getSuccessResponseVO(result);
	}


	/**
	 * 批量新增
	 */
	@RequestMapping("/addBatch")
	public ResponseVO addBatch(@RequestBody List<User> listBean) {
		userService.addBatch(listBean);
		return getSuccessResponseVO(null);
	}

	/**
	 * 批量新增/修改
	 */
	@RequestMapping("/addOrUpdateBatch")
	public ResponseVO addOrUpdateBatch(@RequestBody List<User> listBean) {
		userService.addBatch(listBean);
		return getSuccessResponseVO(null);
	}

	/**
	 * 根据Userid查询对象
	 */
	@RequestMapping("/getUserByUserid")
	public ResponseVO getUserByUserid(Integer userid) {
		return getSuccessResponseVO(userService.getUserByUserid(userid));
	}

	/**
	 * 根据Userid修改对象
	 */
	@RequestMapping("/updateUserByUserid")
	public ResponseVO updateUserByUserid(User bean,Integer userid) {
		userService.updateUserByUserid(bean,userid);
		return getSuccessResponseVO(null);
	}

	/**
	 * 根据Userid删除
	 */
	@RequestMapping("/deleteUserByUserid")
	public ResponseVO deleteUserByUserid(Integer userid) {
		userService.deleteUserByUserid(userid);
		return getSuccessResponseVO(null);
	}

	/**
	 * 根据Username查询对象
	 */
	@RequestMapping("/getUserByUsername")
	public ResponseVO getUserByUsername(String username ) {
		return getSuccessResponseVO(userService.getUserByUsername(username));
	}

	@RequestMapping("/login")
	public ResponseVO login(String username, String password, HttpServletRequest request) {
		if(!StringUtils.hasLength(username) && !StringUtils.hasLength(password)) {
			return getBusinessErrorResponseVO();
		}
	}


	/**
	 * 根据Username修改对象
	 */
	@RequestMapping("/updateUserByUsername")
	public ResponseVO updateUserByUsername(User bean,String username) {
		userService.updateUserByUsername(bean,username);
		return getSuccessResponseVO(null);
	}

	/**
	 * 根据Username删除
	 */
	@RequestMapping("/deleteUserByUsername")
	public ResponseVO deleteUserByUsername(String username) {
		userService.deleteUserByUsername(username);
		return getSuccessResponseVO(null);
	}
}