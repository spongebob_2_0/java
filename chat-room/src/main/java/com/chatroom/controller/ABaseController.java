package com.chatroom.controller;
import com.chatroom.entity.enums.ResponseCodeEnum;
import com.chatroom.entity.vo.ResponseVO;
import com.chatroom.exception.BusinessException;


/**
 * ABaseController 是所有控制器的基类，定义了几个用于构造 ResponseVO 对象的辅助方法。
 * 通过这些方法，可以方便地在子类中构造出标准的 API 响应对象。
 */
public class ABaseController {
    // 定义了成功状态的常量
    protected static final String STATUC_SUCCESS = "success";
    // 定义了成功状态的常量
    protected static final String STATUC_ERROR = "error";

    /**
     * 生成一个表示成功的响应对象。响应状态被设置为 "success"，
     * 响应码被设置为 ResponseCodeEnum.CODE_200 的 code，
     * 响应信息被设置为 ResponseCodeEnum.CODE_200 的 msg，
     * 并且响应数据是方法参数 t。
     *调用该泛型方法时，可以根据传入的具体类型来确定 data 属性的类型，并返回一个带有正确类型数据的 ResponseVO 对象。
     * @param t 响应数据
     * @return 表示成功的响应对象
     */
    protected <T> ResponseVO getSuccessResponseVO(T t) {
        ResponseVO<T> responseVO = new ResponseVO<>();
        responseVO.setStatus(STATUC_SUCCESS);
        responseVO.setCode(ResponseCodeEnum.CODE_200.getCode());
        responseVO.setInfo(ResponseCodeEnum.CODE_200.getMsg());
        responseVO.setData(t);
        return responseVO;
    }

    /**
     * 生成一个表示业务错误的响应对象。响应状态被设置为 "error"，
     * 如果 BusinessException 的 code 为空，则响应码被设置为 ResponseCodeEnum.CODE_600 的 code，否则设置为 BusinessException 的 code，
     * 响应信息被设置为 BusinessException 的 message，
     * 并且响应数据是方法参数 t。
     *
     * @param e 业务异常
     * @param t 响应数据
     * @return 表示业务错误的响应对象
     */
    protected <T> ResponseVO getBusinessErrorResponseVO(BusinessException e, T t) {
        ResponseVO<T> vo = new ResponseVO();
        vo.setStatus(STATUC_ERROR);
        if (e.getCode() == null) {
            vo.setCode(ResponseCodeEnum.CODE_600.getCode());
        } else {
            vo.setCode(e.getCode());
        }
        vo.setInfo(e.getMessage());
        vo.setData(t);
        return vo;
    }

    /**
     * 生成一个表示服务错误的响应对象。响应状态被设置为 "error"，
     * 响应码被设置为 ResponseCodeEnum.CODE_500 的 code，
     * 响应信息被设置为 ResponseCodeEnum.CODE_500 的 msg，
     * 并且响应数据是方法参数 t。
     *泛型方法中传入的类型是在调用这个方法时,根据输入的参数自动确定的,不需要使用泛型列表显示出来
     * @param t 响应数据
     * @return 表示服务错误的响应对象
     */
    protected <T> ResponseVO getServerErrorResponseVO(T t) {
        //实例化对象时没传入泛型参数,默认object类型
        ResponseVO<T> vo = new ResponseVO();
        vo.setStatus(STATUC_ERROR);
        vo.setCode(ResponseCodeEnum.CODE_500.getCode());
        vo.setInfo(ResponseCodeEnum.CODE_500.getMsg());
        vo.setData(t);
        return vo;
    }

}
