package com.chatroom.controller;
import com.chatroom.entity.enums.ResponseCodeEnum;
import com.chatroom.entity.vo.ResponseVO;
import com.chatroom.exception.BusinessException;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;

/**
 * AGlobalExceptionHandlerController 是一个全局异常处理器，继承自 ABaseController。
 * 使用 @RestControllerAdvice 注解，它可以处理所有控制器抛出的异常。
 */
@RestControllerAdvice
public class AGlobalExceptionHandlerController extends ABaseController {

    // 创建 Logger 对象，用于记录日志
    private static final Logger logger = LoggerFactory.getLogger(AGlobalExceptionHandlerController.class);

    /**
     * 处理所有的异常。根据异常的类型，设置不同的响应码和信息。
     *
     * @param e 异常对象
     * @param request HTTP 请求
     * @return 一个包含错误码和错误信息的 ResponseVO 对象
     */
    @ExceptionHandler(value = Exception.class)
    Object handleException(Exception e, HttpServletRequest request) {
        // 记录错误日志
        logger.error("请求错误，请求地址{},错误信息:", request.getRequestURL(), e);
        // 创建 ResponseVO 对象
        ResponseVO ajaxResponse = new ResponseVO();
        // 如果是 NoHandlerFoundException，设置响应码为 404 和相应的错误信息
        if (e instanceof NoHandlerFoundException) {
            ajaxResponse.setCode(ResponseCodeEnum.CODE_404.getCode());
            ajaxResponse.setInfo(ResponseCodeEnum.CODE_404.getMsg());
            ajaxResponse.setStatus(STATUC_ERROR);
        } else if (e instanceof BusinessException) {
            //业务错误
            BusinessException biz = (BusinessException) e;
            ajaxResponse.setCode(biz.getCode() == null ? ResponseCodeEnum.CODE_600.getCode() : biz.getCode());
            ajaxResponse.setInfo(biz.getMessage());
            ajaxResponse.setStatus(STATUC_ERROR);
        } else if (e instanceof BindException|| e instanceof MethodArgumentTypeMismatchException) {
            //参数类型错误
            ajaxResponse.setCode(ResponseCodeEnum.CODE_600.getCode());
            ajaxResponse.setInfo(ResponseCodeEnum.CODE_600.getMsg());
            ajaxResponse.setStatus(STATUC_ERROR);
        } else if (e instanceof DuplicateKeyException) {
            //主键冲突
            ajaxResponse.setCode(ResponseCodeEnum.CODE_601.getCode());
            ajaxResponse.setInfo(ResponseCodeEnum.CODE_601.getMsg());
            ajaxResponse.setStatus(STATUC_ERROR);
        } else {
            // 其他类型的异常，设置响应码为 500 和相应的错误信息
            ajaxResponse.setCode(ResponseCodeEnum.CODE_500.getCode());
            ajaxResponse.setInfo(ResponseCodeEnum.CODE_500.getMsg());
            ajaxResponse.setStatus(STATUC_ERROR);
        }
        return ajaxResponse;
    }
}
