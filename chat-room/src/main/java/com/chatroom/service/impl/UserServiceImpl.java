package com.chatroom.service.impl;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import com.chatroom.entity.enums.PageSize;
import com.chatroom.entity.query.UserQuery;
import com.chatroom.entity.po.User;
import com.chatroom.entity.vo.PaginationResultVO;
import com.chatroom.entity.query.SimplePage;
import com.chatroom.mappers.UserMapper;
import com.chatroom.service.UserService;
import com.chatroom.utils.StringTools;

/**
 * 业务接口的实现
 * 用户服务的实现类 ,它实现了用户服务接口(UserService)的方法，提供了对用户数据进行增删改查等操作的业务逻辑处理
 */
@Service("userService")
public class UserServiceImpl implements UserService {

	// 使用Spring的Resource注解自动注入UserMapper实例
	@Resource
	private UserMapper<User, UserQuery> userMapper;

	/**
	 * 根据条件查询用户列表
	 */
	@Override
	public List<User> findListByParam(UserQuery param) {
		return this.userMapper.selectList(param);
	}

	/**
	 * 根据条件查询用户数量
	 */
	@Override
	public Integer findCountByParam(UserQuery param) {
		return this.userMapper.selectCount(param);
	}

	/**
	 * 分页查询用户列表
	 */
	@Override
	public PaginationResultVO<User> findListByPage(UserQuery param) {
		int count = this.findCountByParam(param); // 查询总数量
		int pageSize = param.getPageSize() == null ? PageSize.SIZE15.getSize() : param.getPageSize(); // 获取每页显示的数量

		// 创建分页对象
		SimplePage page = new SimplePage(param.getPageNo(), count, pageSize);
		param.setSimplePage(page); // 设置查询参数的分页信息

		// 查询分页列表
		List<User> list = this.findListByParam(param);

		// 创建并返回分页结果
		PaginationResultVO<User> result = new PaginationResultVO(count, page.getPageSize(), page.getPageNo(), page.getPageTotal(), list);
		return result;
	}

	/**
	 * 新增用户
	 */
	@Override
	public Integer add(User bean) {
		return this.userMapper.insert(bean);
	}

	/**
	 * 批量新增用户
	 */
	@Override
	public Integer addBatch(List<User> listBean) {
		if (listBean == null || listBean.isEmpty()) {
			return 0; // 如果列表为空，则直接返回0
		}
		return this.userMapper.insertBatch(listBean);
	}

	/**
	 * 批量新增或者修改用户
	 */
	@Override
	public Integer addOrUpdateBatch(List<User> listBean) {
		if (listBean == null || listBean.isEmpty()) {
			return 0; // 如果列表为空，则直接返回0
		}
		return this.userMapper.insertOrUpdateBatch(listBean);
	}

	/**
	 * 根据条件更新用户
	 */
	@Override
	public Integer updateByParam(User bean, UserQuery param) {
		StringTools.checkParam(param); // 检查参数是否有效
		return this.userMapper.updateByParam(bean, param);
	}

	/**
	 * 根据条件删除用户
	 */
	@Override
	public Integer deleteByParam(UserQuery param) {
		StringTools.checkParam(param); // 检查参数是否有效
		return this.userMapper.deleteByParam(param);
	}

	/**
	 * 根据用户ID查询用户
	 */
	@Override
	public User getUserByUserid(Integer userid) {
		return this.userMapper.selectByUserid(userid);
	}

	/**
	 * 根据用户ID更新用户
	 */
	@Override
	public Integer updateUserByUserid(User bean, Integer userid) {
		return this.userMapper.updateByUserid(bean, userid);
	}

	/**
	 * 根据用户ID删除用户
	 */
	@Override
	public Integer deleteUserByUserid(Integer userid) {
		return this.userMapper.deleteByUserid(userid);
	}

	/**
	 * 根据用户名查询用户
	 */
	@Override
	public User getUserByUsername(String username) {
		return this.userMapper.selectByUsername(username);
	}

	/**
	 * 根据用户名更新用户
	 */
	@Override
	public Integer updateUserByUsername(User bean, String username) {
		return this.userMapper.updateByUsername(bean, username);
	}

	/**
	 * 根据用户名删除用户
	 */
	@Override
	public Integer deleteUserByUsername(String username) {
		return this.userMapper.deleteByUsername(username);
	}
}
