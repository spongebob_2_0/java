package com.chatroom.service;

import java.util.List;
import com.chatroom.entity.query.UserQuery;
import com.chatroom.entity.po.User;
import com.chatroom.entity.vo.PaginationResultVO;

/**
 * 用户业务接口
 *
 */
public interface UserService {

	/**
	 * 根据条件查询用户列表
	 */
	List<User> findListByParam(UserQuery param);

	/**
	 * 根据条件查询用户数量
	 */
	Integer findCountByParam(UserQuery param);

	/**
	 * 分页查询用户
	 */
	PaginationResultVO<User> findListByPage(UserQuery param);

	/**
	 * 新增用户
	 */
	Integer add(User bean);

	/**
	 * 批量新增用户
	 */
	Integer addBatch(List<User> listBean);

	/**
	 * 批量新增或修改用户
	 */
	Integer addOrUpdateBatch(List<User> listBean);

	/**
	 * 根据条件更新用户信息
	 */
	Integer updateByParam(User bean,UserQuery param);

	/**
	 * 根据条件删除用户
	 */
	Integer deleteByParam(UserQuery param);

	/**
	 * 根据用户ID查询用户
	 */
	User getUserByUserid(Integer userid);


	/**
	 * 根据用户ID修改用户
	 */
	Integer updateUserByUserid(User bean,Integer userid);

	/**
	 * 根据用户ID删除用户
	 */
	Integer deleteUserByUserid(Integer userid);

	/**
	 * 根据用户名查询用户
	 */
	User getUserByUsername(String username);

	/**
	 * 根据用户名修改用户
	 */
	Integer updateUserByUsername(User bean,String username);

	/**
	 * 根据用户名删除用户
	 */
	Integer deleteUserByUsername(String username);

}
