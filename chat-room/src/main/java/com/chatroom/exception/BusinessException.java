package com.chatroom.exception;
import com.chatroom.entity.enums.ResponseCodeEnum;

/**
 * BusinessException 是一个业务异常类，它继承自运行时异常类 RuntimeException。
 * 该类包含了业务操作中可能出现的异常编码和异常信息，方便在业务处理过程中进行统一的异常管理。
 */
public class BusinessException extends RuntimeException {

    // 异常编码枚举
    private ResponseCodeEnum codeEnum;

    // 异常编码
    private Integer code;

    // 异常信息
    private String message;

    /**
     * 构造函数，接收异常信息和异常对象。
     *
     * @param message 异常信息
     * @param e 异常对象
     */
    public BusinessException(String message, Throwable e) {
        super(message, e);
        this.message = message;
    }

    /**
     * 构造函数，仅接收异常信息。
     *
     * @param message 异常信息
     */
    public BusinessException(String message) {
        super(message);
        this.message = message;
    }

    /**
     * 构造函数，仅接收异常对象。
     *
     * @param e 异常对象
     */
    public BusinessException(Throwable e) {
        super(e);
    }

    /**
     * 构造函数，接收异常编码枚举。
     *
     * @param codeEnum 异常编码枚举
     */
    public BusinessException(ResponseCodeEnum codeEnum) {
        super(codeEnum.getMsg());
        this.codeEnum = codeEnum;
        this.code = codeEnum.getCode();
        this.message = codeEnum.getMsg();
    }

    /**
     * 构造函数，接收异常编码和异常信息。
     *
     * @param code 异常编码
     * @param message 异常信息
     */
    public BusinessException(Integer code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    /**
     * 获取异常编码枚举。
     *
     * @return 异常编码枚举
     */
    public ResponseCodeEnum getCodeEnum() {
        return codeEnum;
    }

    /**
     * 获取异常编码。
     *
     * @return 异常编码
     */
    public Integer getCode() {
        return code;
    }

    /**
     * 获取异常信息。
     *
     * @return 异常信息
     */
    @Override
    public String getMessage() {
        return message;
    }

    /**
     * 重写 fillInStackTrace 方法，业务异常不需要堆栈信息，可以提高效率。
     *
     * @return 当前异常对象
     */
    @Override
    public Throwable fillInStackTrace() {
        return this;
    }

}
