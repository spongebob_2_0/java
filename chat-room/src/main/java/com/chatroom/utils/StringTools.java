package com.chatroom.utils;
import com.chatroom.exception.BusinessException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * StringTools工具类，用于处理字符串和检查对象字段
 */
public class StringTools {

    /**
     * 检查输入对象的所有字段，如果所有字段都为空，则抛出BusinessException异常
     *
     * @param param 要检查的对象
     * @throws BusinessException 如果所有字段都为空
     */
    public static void checkParam(Object param) {
        try {
            // 获取对象的所有字段
            Field[] fields = param.getClass().getDeclaredFields();
            boolean notEmpty = false;
            // 遍历所有字段
            for (Field field : fields) {
                // 构造getter方法的名字
                String methodName = "get" + StringTools.upperCaseFirstLetter(field.getName());
                // 获取getter方法
                Method method = param.getClass().getMethod(methodName);
                // 调用getter方法，获取字段的值
                Object object = method.invoke(param);
                // 判断字段的值是否为空
                if (object != null && object instanceof String && !StringTools.isEmpty(object.toString())
                        || object != null && !(object instanceof String)) {
                    notEmpty = true;
                    break;
                }
            }
            // 如果所有字段都为空，抛出异常
            if (!notEmpty) {
                throw new BusinessException("多参数更新，删除，必须有非空条件");
            }
        } catch (BusinessException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException("校验参数是否为空失败");
        }
    }

    /**
     * 将输入字符串的首字母转为大写
     *
     * @param field 输入的字符串
     * @return 首字母大写的字符串
     */
    public static String upperCaseFirstLetter(String field) {
        if (isEmpty(field)) {
            return field;
        }
        //如果第二个字母是大写，第一个字母不大写
        if (field.length() > 1 && Character.isUpperCase(field.charAt(1))) {
            return field;
        }
        // 将首字母转为大写
        return field.substring(0, 1).toUpperCase() + field.substring(1);
    }

    /**
     * 检查输入的字符串是否为空
     *
     * @param str 输入的字符串
     * @return 如果为空，返回true，否则返回false
     */
    public static boolean isEmpty(String str) {
        // 判断字符串是否为null，空字符串，"null"字符串，或者全部由空白字符组成
        if (null == str || "".equals(str) || "null".equals(str) || "\u0000".equals(str)) {
            return true;
        } else if ("".equals(str.trim())) {
            return true;
        }
        return false;
    }
}
