package com.chatroom.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

// DateUtil类，一个工具类，主要用于处理日期相关的操作
public class DateUtil {

    // 定义一个锁对象，用于synchronized同步块，保证多线程时的线程安全性
    private static final Object lockObj = new Object();
    // 定义一个Map，用来保存每个线程自己的SimpleDateFormat对象
    private static Map<String, ThreadLocal<SimpleDateFormat>> sdfMap = new HashMap<String, ThreadLocal<SimpleDateFormat>>();

    /**
     * 根据传入的日期格式模式，从Map中获取对应的ThreadLocal<SimpleDateFormat>，
     * 如果不存在，则创建一个新的并存入Map
     */
    private static SimpleDateFormat getSdf(final String pattern) {
        // 首先尝试从Map中获取
        ThreadLocal<SimpleDateFormat> tl = sdfMap.get(pattern);
        if (tl == null) { // 如果获取到的为null，即Map中没有对应的SimpleDateFormat对象
            synchronized (lockObj) { // 使用synchronized块进行同步，保证线程安全
                tl = sdfMap.get(pattern); // 再次尝试获取
                if (tl == null) { // 如果依然为null，则创建一个新的ThreadLocal，并设置初始值为一个新的SimpleDateFormat对象
                    // 创建新的ThreadLocal<SimpleDateFormat>，并重写initialValue方法，设置初始值为新的SimpleDateFormat对象
                    tl = new ThreadLocal<SimpleDateFormat>() {
                        @Override
                        protected SimpleDateFormat initialValue() {
                            return new SimpleDateFormat(pattern);
                        }
                    };
                    // 将新创建的ThreadLocal<SimpleDateFormat>存入Map
                    sdfMap.put(pattern, tl);
                }
            }
        }
        // 返回ThreadLocal中的SimpleDateFormat对象
        return tl.get();
    }

    /**
     * 使用指定的日期格式模式将Date对象格式化为字符串
     */
    public static String format(Date date, String pattern) {
        // 首先获取对应的SimpleDateFormat对象，然后使用它将Date对象格式化为字符串
        return getSdf(pattern).format(date);
    }

    /**
     * 使用指定的日期格式模式将字符串解析为Date对象
     */
    public static Date parse(String dateStr, String pattern) {
        try {
            // 首先获取对应的SimpleDateFormat对象，然后使用它将字符串解析为Date对象
            return getSdf(pattern).parse(dateStr);
        } catch (ParseException e) { // 如果解析过程中发生异常，则捕获该异常并打印堆栈跟踪
            e.printStackTrace();
        }
        // 如果解析过程中发生异常，则返回当前时间的Date对象
        return new Date();
    }
}
