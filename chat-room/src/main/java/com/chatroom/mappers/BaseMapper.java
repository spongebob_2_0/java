package com.chatroom.mappers;

import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * 基础的数据库操作接口
 * T: 数据库中的一种实体类型
 * P: 查询参数的类型
 */
interface BaseMapper<T, P> {

	/**
	 * 根据参数查询集合
	 * @param p 查询参数
	 * @return 查询结果集合
	 */
	List<T> selectList(@Param("query") P p);

	/**
	 * 根据参数查询数量
	 * @param p 查询参数
	 * @return 查询结果的数量
	 */
	Integer selectCount(@Param("query") P p);

	/**
	 * 插入一条记录
	 * @param t 要插入的记录
	 * @return 插入的记录数
	 */
	Integer insert(@Param("bean") T t);

	/**
	 * 插入或更新一条记录
	 * @param t 要插入或更新的记录
	 * @return 插入或更新的记录数
	 */
	Integer insertOrUpdate(@Param("bean") T t);

	/**
	 * 批量插入记录
	 * @param list 要插入的记录集合
	 * @return 插入的记录数
	 */
	Integer insertBatch(@Param("list") List<T> list);

	/**
	 * 批量插入或更新记录
	 * @param list 要插入或更新的记录集合
	 * @return 插入或更新的记录数
	 */
	Integer insertOrUpdateBatch(@Param("list") List<T> list);

	/**
	 * 根据查询参数更新记录
	 * @param t 更新的记录内容
	 * @param p 查询参数
	 * @return 更新的记录数
	 */
	Integer updateByParam(@Param("bean") T t,@Param("query") P p);

	/**
	 * 根据查询参数删除记录
	 * @param p 查询参数
	 * @return 删除的记录数
	 */
	Integer deleteByParam(@Param("query") P p);
}
