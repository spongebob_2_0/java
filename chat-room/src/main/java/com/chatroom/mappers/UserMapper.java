package com.chatroom.mappers;

import org.apache.ibatis.annotations.Param;

/**
 * 用户数据库操作接口
 * 泛型参数T代表要操作的实体类，P代表查询参数类
 */
public interface UserMapper<T,P> extends BaseMapper<T,P> {

	/**
	 * 根据用户id更新记录
	 * @param t 要更新的实体对象
	 * @param userid 用户id
	 * @return 更新的记录数
	 */
	Integer updateByUserid(@Param("bean") T t,@Param("userid") Integer userid);


	/**
	 * 根据用户id删除记录
	 * @param userid 用户id
	 * @return 删除的记录数
	 */
	Integer deleteByUserid(@Param("userid") Integer userid);


	/**
	 * 根据用户id查询记录
	 * @param userid 用户id
	 * @return 查询到的实体对象
	 */
	T selectByUserid(@Param("userid") Integer userid);


	/**
	 * 根据用户名更新记录
	 * @param t 要更新的实体对象
	 * @param username 用户名
	 * @return 更新的记录数
	 */
	Integer updateByUsername(@Param("bean") T t,@Param("username") String username);


	/**
	 * 根据用户名删除记录
	 * @param username 用户名
	 * @return 删除的记录数
	 */
	Integer deleteByUsername(@Param("username") String username);


	/**
	 * 根据用户名查询记录
	 * @param username 用户名
	 * @return 查询到的实体对象
	 */
	T selectByUsername(@Param("username") String username);


}
