package com.chatroom.entity.vo;

/**
 * 当你创建一个 ResponseVO 对象的实例时，你需要指定一个具体的类型来替换 <T>，
 * 例如 ResponseVO<String> 或 ResponseVO<List<User>>。
 * 泛型类的类型参数主要用于确定泛型类的某些属性、方法的类型，以便在编译时进行类型检查和类型安全。
 * 并不是所有的属性和方法都必须依赖于泛型参数类型
 * @param <T>
 */
public class ResponseVO<T> {
    private String status; // 响应状态
    private Integer code;  // 响应编码
    private String info;   // 响应消息
    private T data;        // 响应消息

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * public T getData() 和 public void setData(T data) 这两个方法分别用于获取和设置响应数据。
     * 注意这里的 T 表示方法返回类型和参数类型分别是 data 属性的类型。
     * @return
     */
    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
