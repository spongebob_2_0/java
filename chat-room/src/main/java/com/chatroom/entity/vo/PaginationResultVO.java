package com.chatroom.entity.vo;
import java.util.ArrayList;
import java.util.List;

/**
 * PaginationResultVO 是一个分页结果的值对象。它包含了一些分页信息，比如总数、页大小、页码、总页数，
 * 以及一个列表来存储具体的数据。
 * <T> 泛型标识--类型形参
 * 在创建对象时指定具体的数据类型给T(PaginationResultVO<String>  paginationResultVO = new PaginationResultVO<>())
 * 实例化对象时不指定参数就默认object类型
 * 泛型类的类型参数主要用于确定泛型类的某些属性、方法的类型，以便在编译时进行类型检查和类型安全。
 * 并不是所有的属性和方法都必须依赖于泛型参数类型
 */
public class PaginationResultVO<T> {
	private Integer totalCount; // 总数
	private Integer pageSize;  // 页大小
	private Integer pageNo;	   // 页码
	private Integer pageTotal; // 总页数
	private List<T> list = new ArrayList<T>(); // 列表数据

	public static void main(String[] args) {
	}
	/**
	 * 构造方法，接收总数、页大小、页码、列表数据，计算总页数。
	 * @param totalCount 总数
	 * @param pageSize 页大小
	 * @param pageNo 页码
	 * @param list 列表数据
	 */
	public PaginationResultVO(Integer totalCount, Integer pageSize, Integer pageNo, List<T> list) {
		this.totalCount = totalCount;
		this.pageSize = pageSize;
		this.pageNo = pageNo;
		this.list = list;
	}

	/**
	 * 构造方法，接收总数、页大小、页码、总页数、列表数据，如果页码为0，将其设置为1。
	 * @param totalCount 总数
	 * @param pageSize 页大小
	 * @param pageNo 页码
	 * @param pageTotal 总页数
	 * @param list 列表数据
	 */
    public PaginationResultVO(Integer totalCount, Integer pageSize, Integer pageNo, Integer pageTotal, List<T> list) {
        if (pageNo == 0) {
            pageNo = 1;
        }
        this.totalCount = totalCount;
        this.pageSize = pageSize;
        this.pageNo = pageNo;
        this.pageTotal = pageTotal;
        this.list = list;
    }

	/**
	 * 构造方法，仅接收列表数据。
	 * @param list 列表数据
	 */
	public PaginationResultVO(List<T> list) {
		this.list = list;
	}


	/**
	 * 默认构造方法。
	 */
	public PaginationResultVO() {

	}

	/**
	 * 获取总数。
	 * @return 总数
	 */
	public Integer getTotalCount() {
		return totalCount;
	}

	// 下面是其他的 getter 和 setter 方法，用于获取和设置属性的值。
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public Integer getPageTotal() {
        return pageTotal;
    }

    public void setPageTotal(Integer pageTotal) {
        this.pageTotal = pageTotal;
    }
}
