package com.chatroom.entity.enums;

/**
 * ResponseCodeEnum 是一个枚举类型，定义了几种可能的响应码和对应的消息。
 * 这些响应码和消息会被用来标识处理请求的结果，比如请求成功、请求的地址不存在、请求的参数错误等等。
 */
public enum ResponseCodeEnum {
    // 请求成功的响应码和消息
    CODE_200(200, "请求成功"),

    // 请求地址不存在的响应码和消息
    CODE_404(404, "请求地址不存在"),

    // 请求参数错误的响应码和消息
    CODE_600(600, "请求参数错误"),

    // 信息已经存在的响应码和消息
    CODE_601(601, "信息已经存在"),

    // 服务器返回错误的响应码和消息
    CODE_500(500, "服务器返回错误，请联系管理员");

    // 响应码
    private Integer code;

    // 响应消息
    private String msg;

    /**
     * 构造函数，用于创建枚举实例，并设置关联的响应码和消息。
     * 注意，虽然这个构造函数是 private 的，但是在定义枚举实例时，
     * Java会自动调用这个构造函数为枚举实例设置关联的响应码和消息。
     *
     * @param code 响应码
     * @param msg 响应消息
     */
    ResponseCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 获取枚举实例关联的响应码。
     *
     * @return 响应码
     */
    public Integer getCode() {
        return code;
    }

    /**
     * 获取枚举实例关联的响应消息。
     *
     * @return 响应消息
     */
    public String getMsg() {
        return msg;
    }
}
