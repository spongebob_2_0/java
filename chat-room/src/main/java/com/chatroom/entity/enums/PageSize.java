package com.chatroom.entity.enums;

/**
 * PageSize 是一个枚举类型，用于定义页面大小的几种可能值。
 * 这个枚举定义了5个实例，分别表示15、20、30、40和50这五种页面大小。
 * 每个枚举实例都关联一个整型值，表示具体的页面大小。
 */
public enum PageSize {
	// 页面大小为15的枚举实例
	SIZE15(15),

	// 页面大小为20的枚举实例
	SIZE20(20),

	// 页面大小为30的枚举实例
	SIZE30(30),

	// 页面大小为40的枚举实例
	SIZE40(40),

	// 页面大小为50的枚举实例
	SIZE50(50);

	// 页面大小
	int size;

	/**
	 * 构造函数，用于创建枚举实例，并设置关联的页面大小值。
	 * 注意，虽然这个构造函数是 private 的，但是在定义枚举实例时，
	 * Java会自动调用这个构造函数为枚举实例设置关联的页面大小值。
	 *
	 * @param size 页面大小
	 */
	private PageSize(int size) {
		this.size = size;
	}

	/**
	 * 获取枚举实例关联的页面大小值。
	 *
	 * @return 页面大小
	 */
	public int getSize() {
		return this.size;
	}
}
