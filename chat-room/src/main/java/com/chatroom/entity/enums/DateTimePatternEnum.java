package com.chatroom.entity.enums;

/**
 * DateTimePatternEnum 是一个枚举类型，用于定义一些日期时间格式的模式字符串。
 * 枚举类型是一种特殊的类，它有固定数量的实例，这些实例可以直接用它们的名字来访问。
 * 在这个枚举类型中，定义了两个模式字符串："yyyy-MM-dd HH:mm:ss" 和 "yyyy-MM-dd"。
 */
public enum DateTimePatternEnum {
    // "yyyy-MM-dd HH:mm:ss" 的枚举实例
    YYYY_MM_DD_HH_MM_SS("yyyy-MM-dd HH:mm:ss"),

    // "yyyy-MM-dd" 的枚举实例
    YYYY_MM_DD("yyyy-MM-dd");

    // 模式字符串
    private String pattern;

    /**
     * 枚举的构造函数，用于初始化模式字符串。注意，虽然这个构造函数是 public 的，
     * 但你不能用 new 关键字来创建枚举的实例。枚举的实例在枚举类型被加载时就已经被创建好了。
     *
     * @param pattern 模式字符串
     */
    DateTimePatternEnum(String pattern) {
        this.pattern = pattern;
    }

    /**
     * 获取模式字符串。
     *
     * @return 模式字符串
     */
    public String getPattern() {
        return pattern;
    }
}
