package com.chatroom.entity.query;

/**
 * UserQuery 是一个用于封装用户查询条件的类，它继承自 BaseParam，
 * 因此除了具有 BaseParam 的属性外，还包含了用户的 ID、用户名和密码以及对应的模糊查询字段。
 * 在实际的业务场景中，可以根据这个类中的属性进行精确查询或模糊查询。
 */
public class UserQuery extends BaseParam {

	/**
	 * 用户 ID。
	 */
	private Integer userid;

	/**
	 * 用户名。
	 */
	private String username;

	/**
	 * 用于模糊查询的用户名。
	 */
	private String usernameFuzzy;

	/**
	 * 密码。
	 */
	private String password;

	/**
	 * 用于模糊查询的密码。
	 */
	private String passwordFuzzy;

	/**
	 * 设置用户 ID。
	 * @param userid 用户 ID
	 */
	public void setUserid(Integer userid){
		this.userid = userid;
	}

	/**
	 * 获取用户 ID。
	 * @return 用户 ID
	 */
	public Integer getUserid(){
		return this.userid;
	}

	/**
	 * 设置用户名。
	 * @param username 用户名
	 */
	public void setUsername(String username){
		this.username = username;
	}

	/**
	 * 获取用户名。
	 * @return 用户名
	 */
	public String getUsername(){
		return this.username;
	}

	/**
	 * 设置用于模糊查询的用户名。
	 * @param usernameFuzzy 用于模糊查询的用户名
	 */
	public void setUsernameFuzzy(String usernameFuzzy){
		this.usernameFuzzy = usernameFuzzy;
	}

	/**
	 * 获取用于模糊查询的用户名。
	 * @return 用于模糊查询的用户名
	 */
	public String getUsernameFuzzy(){
		return this.usernameFuzzy;
	}

	/**
	 * 设置密码。
	 * @param password 密码
	 */
	public void setPassword(String password){
		this.password = password;
	}

	/**
	 * 获取密码。
	 * @return 密码
	 */
	public String getPassword(){
		return this.password;
	}

	/**
	 * 设置用于模糊查询的密码。
	 * @param passwordFuzzy 用于模糊查询的密码
	 */
	public void setPasswordFuzzy(String passwordFuzzy){
		this.passwordFuzzy = passwordFuzzy;
	}

	/**
	 * 获取用于模糊查询的密码。
	 * @return 用于模糊查询的密码
	 */
	public String getPasswordFuzzy(){
		return this.passwordFuzzy;
	}

}
