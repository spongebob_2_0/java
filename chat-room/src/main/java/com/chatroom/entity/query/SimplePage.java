package com.chatroom.entity.query;
import com.chatroom.entity.enums.PageSize;

/**
 * SimplePage 是一个简单的分页对象，它主要封装了分页查询时所需要的一些参数，
 * 如页码（pageNo）、总数（countTotal）、页大小（pageSize）、总页数（pageTotal）、起始位（start）和结束位（end）等。
 */
public class SimplePage {
	// 页码
	private int pageNo;

	// 总数
	private int countTotal;

	// 页大小
	private int pageSize;

	// 总页数
	private int pageTotal;

	// 开始位置
	private int start;

	// 结束位置
	private int end;

	/**
	 * 默认构造方法。
	 */
	public SimplePage() {
	}

	/**
	 * 构造方法，接收页码、总数和页大小，然后调用 action() 方法进行处理。
	 *
	 * @param pageNo 页码
	 * @param countTotal 总数
	 * @param pageSize 页大小
	 */
	public SimplePage(Integer pageNo, int countTotal, int pageSize) {
		if (null == pageNo) {
			pageNo = 0;
		}
		this.pageNo = pageNo;
		this.countTotal = countTotal;
		this.pageSize = pageSize;
		action();
	}

	/**
	 * 构造方法，接收开始位置和结束位置。
	 *
	 * @param start 开始位置
	 * @param end 结束位置
	 */
	public SimplePage(int start, int end) {
		this.start = start;
		this.end = end;
	}

	/**
	 * 这个方法主要用于处理页大小、总页数、开始位置和结束位置的计算。
	 */
	public void action() {
		if (this.pageSize <= 0) {
			this.pageSize = PageSize.SIZE20.getSize();
		}
		if (this.countTotal > 0) {
			this.pageTotal = this.countTotal % this.pageSize == 0 ? this.countTotal / this.pageSize
					: this.countTotal / this.pageSize + 1;
		} else {
			pageTotal = 1;
		}

		if (pageNo <= 1) {
			pageNo = 1;
		}
		if (pageNo > pageTotal) {
			pageNo = pageTotal;
		}
		this.start = (pageNo - 1) * pageSize;
		this.end = this.pageSize;
	}

	// 下面是 getter 和 setter 方法，用于获取和设置属性的值。
	public int getStart() {
		return start;
	}

	public int getEnd() {
		return end;
	}

	public int getPageTotal() {
		return pageTotal;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public void setPageTotal(int pageTotal) {
		this.pageTotal = pageTotal;
	}

	public int getCountTotal() {
		return countTotal;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public void setCountTotal(int countTotal) {
		this.countTotal = countTotal;
		this.action();
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
}
