package com.chatroom.entity.query;

/**
 * BaseParam 是一个基础查询参数类。这个类主要用于封装查询时的一些公共参数，
 * 如分页信息（页码和页大小）以及排序字段等。
 */
public class BaseParam {
	// 分页信息
	private SimplePage simplePage;

	// 页码
	private Integer pageNo;

	// 页大小
	private Integer pageSize;

	// 排序字段
	private String orderBy;

	/**
	 * 获取分页信息。
	 *
	 * @return 分页信息
	 */
	public SimplePage getSimplePage() {
		return simplePage;
	}

	/**
	 * 设置分页信息。
	 *
	 * @param simplePage 分页信息
	 */
	public void setSimplePage(SimplePage simplePage) {
		this.simplePage = simplePage;
	}

	/**
	 * 获取页码。
	 *
	 * @return 页码
	 */
	public Integer getPageNo() {
		return pageNo;
	}

	/**
	 * 设置页码。
	 *
	 * @param pageNo 页码
	 */
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	/**
	 * 获取页大小。
	 *
	 * @return 页大小
	 */
	public Integer getPageSize() {
		return pageSize;
	}

	/**
	 * 设置页大小。
	 *
	 * @param pageSize 页大小
	 */
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * 设置排序字段。
	 *
	 * @param orderBy 排序字段
	 */
	public void setOrderBy(String orderBy){
		this.orderBy = orderBy;
	}

	/**
	 * 获取排序字段。
	 *
	 * @return 排序字段
	 */
	public String getOrderBy(){
		return this.orderBy;
	}
}
