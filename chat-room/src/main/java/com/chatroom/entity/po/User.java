package com.chatroom.entity.po;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;


/**
 * 
 */
public class User implements Serializable {


	/**
	 * 
	 */
	private Integer userid;

	/**
	 * 
	 */
	private String username;

	/**
	 * 
	 */
	private String password;


	public void setUserid(Integer userid){
		this.userid = userid;
	}

	public Integer getUserid(){
		return this.userid;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return this.username;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return this.password;
	}

	@Override
	public String toString (){
		return "userid:"+(userid == null ? "空" : userid)+"，username:"+(username == null ? "空" : username)+"，password:"+(password == null ? "空" : password);
	}
}
