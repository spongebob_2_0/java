/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-01
 * Time: 0:03
 */

import java.util.*;

public class Test5 {
    public static int sum(int a,int b){
        return a+b;
    }
    public static double sum(double c,double d,double e){
        return c+d+e;
    }
    public static void main(String[] args) {
        int a = 10;
        int b = 20;
        double c =30.0d;
        double d =40.0d;
        double e =33.0d;
        int ret1 =sum(a,b);
        double ret2 =sum(c,d,e);
        System.out.println(ret1);
        System.out.println(ret2);
    }
}
