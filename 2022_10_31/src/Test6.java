/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-01
 * Time: 0:11
 */

import java.util.*;

public class Test6 {
    public static int maxNum(int a, int b) {
        int max;
        return max = a > b ? a : b;
    }

    public static double maxNum2(double c, double d, double e) {
        double max = c;
        if (max < d) {
            max = d;
        }
        if (max < e) {
            max = e;
        }
        return max;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        double c = scanner.nextDouble();
        double d = scanner.nextDouble();
        double e = scanner.nextDouble();
        int ret1 = maxNum(a, b);
        double ret2 = maxNum2(c, d, e);
        System.out.println("两个整数中的最大值为" + ret1);
        System.out.println("两个小数中的最大值为" + ret2);
    }
}
