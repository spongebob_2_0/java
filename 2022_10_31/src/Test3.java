/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-10-31
 * Time: 23:11
 */

import java.util.*;

public class Test3 {
    public static int maxInt1(int a, int b) {
        int max = 0;
        return max = a > b ? a : b;
    }

    public static int maxInt2(int a, int b, int c) {
        return maxInt1(maxInt1(a, b), c);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        int ret1 = maxInt1(a, b);
        int ret2 = maxInt2(a, b, c);
//        System.out.println("两个数中的最大值为" + ret1);
        System.out.println("三个数中的最大值为" + ret2);
    }
}
