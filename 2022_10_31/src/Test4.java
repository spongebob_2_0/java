/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-10-31
 * Time: 23:42
 */

import java.util.*;

public class Test4 {
    public static int fibonacc(int n){
        if(n==1 || n==2){
            return 1;
        }
        int a=1,b=1,c=0;
        for(int i=3;i<=n;i++){
            c=a+b;
            a=b;
            b=c;
        }
        return c;
    }
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
       while (scanner.hasNext()){
           int n = scanner.nextInt();
           int ret =fibonacc(n);
           System.out.println("斐波那契数列的第"+n+"项为"+ret);
       }
    }
}
