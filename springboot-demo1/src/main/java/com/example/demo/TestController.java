package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.xml.ws.RequestWrapper;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-09
 * Time: 2:47
 */
@Controller
@ResponseBody  //加在类上, 表示当前类中的所有方法返回的都是非静态页面的数据
public class TestController {
    private static final Logger log = LoggerFactory.getLogger(TestController.class);
    @RequestMapping ("/sayHi")   // = WebServlet("/URL")
    public String sayHi() {
        log.trace("i'm trace");
        log.warn("i'm warn");
        log.debug("i'm debug");
        log.info("i'm info");
        log.error("i'm error");
        return "hello world ";
    }
}
