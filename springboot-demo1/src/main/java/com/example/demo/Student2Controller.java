package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-10
 * Time: 7:16
 */

@RestController // = @ResponseBody + @Controller
@Slf4j //给当前类添加一个叫做log的日志对象( = SLF4J 里面提供的Logger)
public class Student2Controller {

    @RequestMapping("/stu/sayHi")
    @Value("${server.port}")
    public String sayHi() {
        log.info("我是 student的 info");
        log.error("我是 student的 error");
        return "student say hi";
    }
}
