package com.example.demo;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("student1")//这里不需要使用$
@Getter
@Setter //这里是通过set方法赋值属性的，提供set方法
@ToString
public class Student {
    //字段与yml里的名称相同
    private int id;
    private String name;
    private int age;
}
