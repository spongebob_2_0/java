// 1、无头单向非循环链表实现
 public class SingleLinkedList {

        static class Node{
            public int val ;  //存储的数据
            public Node next;  //存储下一个节点的地址
            public Node(int val){
                this.val =val;
            }
        }

        public Node head;  //代表当前链表头节点的引用

        public void createLink(){
            Node node1 =new Node(12);
            Node node2 =new Node(45);
            Node node3 =new Node(23);
            Node node4 =new Node(90);
            node1.next =node2;
            node2.next =node3;
            node3.next =node4;
            head =node1;
        }

     //头插法
     public void addFirst(int data){
        Node node =new Node(data);
        node.next =head;
        head =node;
     }

     //尾插法
     public void addLast(int data){
        Node node =new Node(data);
        if(head ==null){
            head =node;
            return;
        }
        Node cur =head;
        while (cur.next != null){
            cur =cur.next;
        }
        cur.next =node;
     }

     //任意位置插入,第一个数据节点为0号下标
     public void addIndex(int index,int data) throws ListIndexOutOfException{
            checkIndex(index);
        if(index == 0){
            addFirst(data);
            return;
        }
        if(index == size()){
            addLast(data);
            return;
        }
        Node cur =findIndexSubOne(index);
        Node node =new Node(data);
        node.next =cur.next;
        cur.next =node;
     }

     //让cur走到index前一个位置
     private Node findIndexSubOne(int index){
            Node cur =head;
            int count =0;
            while (count != index -1){
                cur =cur.next;
                count++;
            }
            return cur;
     }
     //检查坐标是否合法
    private void checkIndex(int index) throws ListIndexOutOfException{
            if(index <0 || index > size() ){
                throw new ListIndexOutOfException("index位置不合法!");
            }
    }
     //查找是否包含关键字key是否在单链表当中
     public boolean contains(int key) {
         Node cur = head;
         while (cur != null) {
             if (cur.val == key) {
                 return true;
             }
             cur =cur.next;
         }
         return false;
     }
     //删除第一次出现关键字为key的节点
     public void remove(int key){
        if(head ==null){
            return;  //一个节点都没有
        }
        if(head.val ==key){
            head =head.next;
            return;
        }
        Node cur =searchPrev(key);
        if(cur ==null){
            return;
        }
        Node del =cur.next;  //del是要删除的节点
         cur.next=del.next;
     }
     //找到要删除节点的前一个位置
     private Node searchPrev(int key){
            Node cur =head;
            while ((cur.next != null)){
                if(cur.next.val ==key){
                    return cur;
                }
                cur =cur.next;
            }
            return null;   //没有你要删除的节点
     }


     //删除所有值为key的节点
     public void removeAllKey(int key){
        if(head ==null){
            return;
        }
        Node prev =head;
        Node cur =head.next;
        while (cur !=null){
            if(cur.val ==key){
                prev.next =cur.next;
                cur =cur.next;
            }else{
                prev = cur;
                cur =cur.next;
            }
        }
        if(head.val ==key){
            head =head.next;
        }
     }
     //得到单链表的长度
     public int size(){
        int count =0;
        Node cur =head;
        while(cur != null){
            count++;
            cur =cur.next;
        }
        return count;
     }

     //打印链表
     //如果说 把整个链表 遍历完成 那么 就需要 head == null
     // 如果说 你遍历到链表的尾巴  head.next == null
     public void display(){
        Node cur =head;
        while (cur != null){
            System.out.println(cur.val+" ");
            cur= cur.next;
        }
         System.out.println();
     }
    //清空链表
     public void clear(){
        head =null;
     }
 }