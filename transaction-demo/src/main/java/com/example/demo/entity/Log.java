package com.example.demo.entity;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-19
 * Time: 23:32
 */
@Data
public class Log {
    private  int id;
    private LocalDateTime timestamp;
    private String  message;

}
