package com.example.demo.controller;

import com.example.demo.entity.Userinfo;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-19
 * Time: 5:46
 */
//这是一个组合注解，它组合了@Controller和@ResponseBody
//表明这个类是一个控制器，会处理HTTP请求，并且返回的数据会自动转换为JSON或其他格式
@RestController
//这个注解表明这个控制器会处理以"/user"开头的URL
@RequestMapping("/user3")
public class UserController3 {
    @Autowired
    private UserService userService;
    @RequestMapping("/add")
    @Transactional(propagation = Propagation.NESTED)
    public int add (String username, String password) {
        if(null == username || null == password || username.equals("") || password.equals("")) {
            return  0;
        }
        Userinfo user = new Userinfo();
        user.setUsername(username);
        user.setPassword(password);
        int result = userService.add(user);

        // 用户添加操作
        return  result;
    }
}
