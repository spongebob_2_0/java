package com.example.demo.controller;

import com.example.demo.mapper.UserMapper;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-19
 * Time: 5:46
 */
//这是一个组合注解，它组合了@Controller和@ResponseBody
//表明这个类是一个控制器，会处理HTTP请求，并且返回的数据会自动转换为JSON或其他格式
@RestController
//这个注解表明这个控制器会处理以"/user"开头的URL
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService; //这是一个业务逻辑服务类，用于处理与用户相关的业务逻辑
    //编程式事务
    //Spring的事务管理可以确保当在执行数据库操作时，如果出现错误，所有的操作都可以回滚，以保证数据的一致性。
    @Autowired
    //transactionManager 负责管理数据库事务
    private DataSourceTransactionManager transactionManager;
    @Autowired
    //transactionDefinition 它定义了事务的各种属性，比如隔离级别、传播行为、超时时间、是否为只读事务等。
    private TransactionDefinition transactionDefinition;

    @RequestMapping("/del")
    public int del(Integer id) {
        if(id == null || id <= 0) {
            return 0;
        }
        //1.开启事务, 以便后续的事务操作，比如提交或回滚事务。
        TransactionStatus transactionStatus = null;
        int result = 0;
        try {
            transactionStatus = transactionManager.getTransaction(transactionDefinition);
            // 业务操作, 删除用户
            result = userService.del(id);
            System.out.println("删除: " + result);
            //2.提交事务/回滚事务
//            transactionManager.commit(transactionStatus);     //提交事务
        }catch (Exception e) {
            if(transactionStatus != null) {
                transactionManager.rollback(transactionStatus);
            }   //回滚事务
        }
        return result;
    }
}
