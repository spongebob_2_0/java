package com.example.demo.mapper;

import com.example.demo.entity.Log;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-19
 * Time: 23:44
 */
@Mapper
public interface LogMapper {
    int add(Log log);
}
