package com.example.demo.service;

import com.example.demo.entity.Log;
import com.example.demo.entity.Userinfo;
import com.example.demo.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-19
 * Time: 5:50
 */
@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private LogService logService;

    public int del(Integer id) {
        return userMapper.del(id);
    }

    @Transactional(propagation = Propagation.NESTED)
    public int add(Userinfo userinfo) {
        // 给用户表添加用户信息
        int addUserResult = userMapper.add(userinfo);
        System.out.println("添加用户结果: " + addUserResult);
        // 添加日志信息
        Log log = new Log();
        log.setMessage("添加用户信息");
        logService.add(log);
        return addUserResult;
    }
}
