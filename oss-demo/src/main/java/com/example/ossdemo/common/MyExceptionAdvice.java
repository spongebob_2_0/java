package com.example.ossdemo.common;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice //定义一个全局异常处理类,返回类型的格式为json格式
public class MyExceptionAdvice {

    @ExceptionHandler(Exception.class) //定义异常处理方法
    public AjaxResult doException(Exception e) {
        return AjaxResult.fail(-1,e.getMessage());
    }
}