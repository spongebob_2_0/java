package com.example.ossdemo.common;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.Map;

public class JwtUtils {

    //设置令牌的key
    private static String signKey = "wuyimin";
    //设置过期令牌时间
    private static Long expire = 43200000L;

    /**
     * 生成JWT令牌
     * @param claims JWT第二部分负载 payload 中存储的内容
     * @return
     */
    public static String generateJwt(Map<String, Object> claims){
        String jwt = Jwts.builder()
                .addClaims(claims)  //自定义内容(map集合可用于存储用户信息)
                .signWith(SignatureAlgorithm.HS256, signKey)   //签名算法
                .setExpiration(new Date(System.currentTimeMillis() + expire))  //设置令牌有效期
                .compact();
        return jwt;
    }

    /**
     * 解析JWT令牌
     * @param jwt JWT令牌
     * @return JWT第二部分负载 payload 中存储的内容
     */
    public static Claims parseJWT(String jwt){
        Claims claims = Jwts.parser()
                .setSigningKey(signKey)  // 解析指定的签名密钥
                .parseClaimsJws(jwt)     // 解析JWT令牌
                .getBody();  //拿到自定义的内容
        return claims;  //解析令牌成功后会返回里面存储的信息(比如用户信息)
    }
}
