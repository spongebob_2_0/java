package com.example.ossdemo.controller;

import com.example.ossdemo.common.AjaxResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-08-20
 * Time: 19:27
 */
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    //设置cookie
    @RequestMapping("/c1")
    public AjaxResult cookiel(HttpServletResponse response) {
        //设置响应对象的cookie值
        response.addCookie(new Cookie("login_username","itheima"));
        log.info("设置成功");
        return AjaxResult.success("设置成功");
    }

    //获取cookie
    @RequestMapping("/c2")
    public AjaxResult cookie2(HttpServletRequest request) {
        //从请求对象中获取cookie
        Cookie[] cookies = request.getCookies();
        for(Cookie cookie: cookies) {
            if(cookie.getName().equals("login_username")) {
                System.out.println("login_username = " + cookie.getValue());
                log.info("获取成功");
                return AjaxResult.success("获取成功");
            }
        }
        return AjaxResult.fail(-1,"获取失败");
    }



}
