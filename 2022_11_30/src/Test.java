/**
 * Created with IntelliJ IDEA.
 * Description:  用集合来打印杨辉三角
 * User: W
 * Date: 2022-11-30
 * Time: 19:22
 */

import java.util.*;

public class Test {
    /*打印杨辉三角实现的一个逻辑,首先我们要知道它的每一行的第一个位置和最后一个位置都是1
     中间位置的值都等于(上一行当前列的值和前一列的值的和) */
    public static List<List<Integer>> generate(int numRows){
        List<List<Integer>> ret =new ArrayList<>();

        List<Integer> row = new ArrayList<>();
        row.add(1);

        ret.add(row);
        for (int i = 1; i < numRows; i++) {
            List<Integer> prevRow =ret.get(i-1);   //获取杨辉三角当前行的上一行
            List<Integer> curRow =new ArrayList<>();
            curRow.add(1);  //杨辉三角每一行中的第一个1
            //中间curRow List的赋值
            for (int j = 1; j < i; j++) {
                int x =prevRow.get(j) +prevRow.get(j-1);
                curRow.add(x);
            }
            curRow.add(1);  //杨辉三角每一行中的最后一个1
            ret.add(curRow);   //把每一行添加到这个二维数组里面
        }
        return ret;
    }

    public static void main(String[] args) {
        List<List<Integer>> arr =new ArrayList<>();
        arr = generate(9);
        for (Object x: arr) {
            System.out.println(x);
        }
    }
}
