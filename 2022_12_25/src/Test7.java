/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-25
 * Time: 17:31
 */
public class Test7 {
    public static int arraySum(int[] array,int len){
        if(len==1){
            return array[0];
        }
        int tmp =array[len-1]+arraySum(array,len-1);
        return tmp;
    }
    public static void main(String[] args) {
        int[] array ={1,2,3,4,5,6,7,8,9,10,20};
        int len =array.length;
        System.out.println(arraySum(array, len));
    }
}
