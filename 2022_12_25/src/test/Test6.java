package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-25
 * Time: 17:15
 */
public class Test6 {
    public static void main(String[] args) {
        for (int i = 1; i < 10000; i++) {
            int j =divsum(i);   //j代表乙的约数之和
            if(i != j  && divsum(j) ==i){
                System.out.println(j+" "+j);
            }
        }
    }
    //求一个数的约数之和
    public static int divsum(int n){
        int sum =0;
        for (int i = 1; i < n; i++) {
            if(n%i==0){
                sum +=i;
            }
        }
        return sum;
    }
}
