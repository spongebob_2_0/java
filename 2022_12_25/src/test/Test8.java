package test;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-25
 * Time: 17:50
 */
public class Test8 {
    public static boolean func(int n){
        double ret =n;
        if(ret==1) {
            return true;
        }
        while(ret>1){
            ret /=2.0;
        }
        if(ret==1){
            return true;
        }
        return false;
    }
    public static boolean func1(int n){
        int count =0;
        for (int i = 0; i < 32; i++) {
            if(((n>>i) & 1) ==1){
                count++;
            }
            }
        if(count ==1){
            return true;
        }
        return false;
    }
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        while (scanner.hasNext()){
            int n = scanner.nextInt();
            System.out.println(func1(n));
        }
    }
}
