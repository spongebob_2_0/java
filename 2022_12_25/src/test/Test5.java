package test;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-25
 * Time: 13:35
 */
public class Test5 {
    public static void main(String[] args) {
        Random random =new Random();
        int[][] array =new int[3][3];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] =random.nextInt(100);
            }
        }
        int sum =0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if(i==j || i+j==2){
                   sum += array[i][j];
                }
            }
        }
        System.out.println(sum);
    }
}
