package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-25
 * Time: 3:50
 */
public class Tes3 {
    public static void main(String[] args) {
        double n= 100;
        double sum =0;
        for (int i = 0; i < 10; i++) {
            sum +=n;
            if(i<9){
                n =n/2;
                sum +=n;
            }
        }
        System.out.println(sum);  //共经过多少米
        System.out.println(n/2);  //第10反弹多少米
    }
}
