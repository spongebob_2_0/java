package test;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-25
 * Time: 13:13
 */
public class Test4 {
    public static int func(int n) {
        if(n<3){
            return 1;
        }
        int f1 =1;
        int f2 =1;
        int f3 =0;
        for (int i = 3; i <= n; i++) {
            f3 =f1+f2;
            f1 =f2;
            f2 =f3;
        }
        return f3;
    }    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(func(n));
    }
}
