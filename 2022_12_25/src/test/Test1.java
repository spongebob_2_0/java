package test;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-25
 * Time: 1:25
 */
public class Test1 {
    public static boolean perfectNumber(int num ){
        int sum =0;
        for (int i = 1; i < num; i++) {
            if(num %i ==0){
                sum +=i;
            }
        }
        if(sum ==num){
            return true;
        }
        return false;
    }
    public static void main(String[] args) {
        for (int i = 1; i <=10000; i++) {
            if(perfectNumber(i)){
                System.out.print(i+" ");
            }
        }
    }
}
