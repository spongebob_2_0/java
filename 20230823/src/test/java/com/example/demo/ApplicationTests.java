package com.example.demo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.demo.entity.Userinfo;
import com.example.demo.mapper.UserinfoMapper;
import com.example.demo.service.UserinfoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.context.annotation.ApplicationScope;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class ApplicationTests {

    @Autowired
    private UserinfoMapper userinfoMapper;

    @Autowired
    private UserinfoService userinfoService;
    @Test
    void contextLoads() {
    }

    @Test
    void test1() {
        LambdaQueryWrapper<Userinfo> wrapper = new LambdaQueryWrapper<>();
        wrapper.gt(Userinfo::getUserid,12);
        userinfoService.remove(wrapper);
    }

}
