package api;

import model.Blog;
import model.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-04
 * Time: 12:14
 */
@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("Utf-8");
        resp.setContentType("text/html;charset=utf8");

        //从http请求获取参数
        String username = req.getParameter("username");
        String password1 = req.getParameter("password1");
        String password2 = req.getParameter("password2");

        if(username == null || username.equals("")) {
            resp.getWriter().write("<h3> 您未输入用户名</h3>");
            return;
        }
        if(password1 == null || password1.equals("")) {
            resp.getWriter().write("<h3> 您未输入密码</h3>");
            return;
        }
        if(password2 == null || password2.equals("")) {
            resp.getWriter().write("<h3> 您未再次输入密码</h3>");
            return;
        }

        //判断第二次输入密码是否正确
        if(!password1.equals(password2)) {
            resp.getWriter().write("<h3> 您两次输入的密码不一致,请返回,并重新输入! </h3>");
            return;
        }

        //判断数据库是否有重名的用户
        UserDao userDao = new UserDao();
        if(userDao.selectByUsername(username) != null) {
            resp.getWriter().write("<h3> 非常抱歉，你注册的用户名已经被其他用户使用，请换一个试试吧~</h3>");
            return;
        }
        //代码走到这里,说明一切正常
        //开始往数据库插入数据
        userDao.adduser(username,password1);
        //一切都完成后,重定向至登录界面
        resp.sendRedirect("html/login.html");
    }
}
