package api;
 
import model.Blog;
import model.BlogDao;
import model.User;
 
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/blogDelete")
public class BlogDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 设置请求的字符编码为UTF-8。
        req.setCharacterEncoding("utf-8");
        // 从请求中获取名为"blogId"的参数。这个参数应该是要删除的博客的ID。
        String blogId = req.getParameter("blogId");

        // 创建一个BlogDao对象，用于操作数据库。
        BlogDao blogDao = new BlogDao();
        // 调用BlogDao的delete方法，传入要删除的博客的ID。注意，这里将blogId从String转换为了int。
        blogDao.delete(Integer.parseInt(blogId));

        // 重定向用户到博客列表页。重定向是将用户的浏览器从一个URL导向到另一个URL。
        resp.sendRedirect("html/blog_list.html");
    }
}
