package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.User;
import model.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-05-30
 * Time: 9:07
 */
@WebServlet("/login")
public class LonginServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //设置请求的编码格式, 告诉 servlet 按照啥格式来理解请求
        req.setCharacterEncoding("utf8");
        //设置响应的编码格式, 告诉 servlet 按照啥格式来理解响应
        //resp.setCharacterEncoding("utf8");

        resp.setContentType("text/html;charset=utf8");
        //1.读取参数中的用户名和密码
        //注意!! 如果用户名和密码包括中文,此处可能会乱码
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if(username == null || "".equals(username) || password == null || "".equals(password)) {
            //登录失败
            String html = "<h3> 登录失败! 用户名或者密码错误 </h3>";
            resp.getWriter().write(html);
            return;
        }
        //2.读数据库,看看用户名是否存在,并且密码是否匹配
        UserDao userDao = new UserDao();
        User user = userDao.selectByUsername(username);
        if(user == null) {
            //用户不存在
            String html = "<h3> 登录失败! 用户名或者密码错误 </h3>";
            resp.getWriter().write(html);
            return;
        }
        if(!password.equals(user.getPassword())) {
            //密码不对
            String html = "<h3> 登录失败! 用户名或者密码错误 </h3>";
            resp.getWriter().write(html);
            return;
        }
        //3.用户名和密码验证成功, 登录成功,接下来就会创建会话, 使用该会话保存用户的信息
        HttpSession session = req.getSession();
        session.setAttribute("user",user);
        //4.进行重定向,跳转到博客列表页
        resp.sendRedirect("html/blog_list.html");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=utf8");
        //使用这个方法获取到用户的登录状态

        //如果未登录,这里的会话就拿不到!!
        HttpSession session = req.getSession(false);
        if(session == null) {
            //未登录 , 返回一个空user对象
            User user = new User();
            //将user对象转换成json格式的字符串写入响应的body中
            objectMapper.writeValue(resp.getWriter(),user);
            return;
        }
        User user = (User)session.getAttribute("user");
        if(user == null) {
            user = new User();
            //将user对象转换成json格式的字符串写入响应的body中
            objectMapper.writeValue(resp.getWriter(),user);
            return;
        }
        //确实成功取出了对象,直接返回即可
        //将user对象转换成json格式的字符串写入响应的body中
        objectMapper.writeValue(resp.getWriter(),user);
    }
}
