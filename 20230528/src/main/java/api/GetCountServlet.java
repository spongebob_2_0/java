package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Blog;
import model.BlogDao;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-04
 * Time: 17:20
 */
@WebServlet("/count")
public class GetCountServlet extends HttpServlet {
    private ObjectMapper objectMapper =new ObjectMapper();
    //将所有博客返回给请求数据的客户端
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf8");
        resp.setContentType("application/json;charset=utf8");
        BlogDao blogDao =new BlogDao();
        List<Blog> blogs = blogDao.selectAll();
        objectMapper.writeValue(resp.getWriter(),blogs);
    }
}
