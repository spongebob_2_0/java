package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * model.User: WuYimin
 * Date: 2023-05-28
 * Time: 10:19
 */
//通过这个类封装对 博客表的基本操作
//此处暂时不涉及到修改博客~~ (修改博客也可以通过 删除/新增 )
public class BlogDao {
    //1.新增一个博客
    public void add(Blog blog) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //1. 数据库建立连接
            connection = DBUtil.getConnection();
            //2. 构造 SQL
            String sql = "insert into blog values(null,?,?,?,?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1, blog.getTitle());
            statement.setString(2,blog.getContent());
            statement.setTimestamp(3,blog.getPostTimestamp());
            statement.setInt(4,blog.getUserId(  ));
            //执行 sql
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtil.close(connection,statement,null);
        }
    }

    //2.根据博客 id 来指定查询博客(博客详情页中)
    public Blog selectById(int blogId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造 sql 语句
            String sql = "select * from blog where blogId = ?";
            //prepareStatement(sql)这个方法见sql语句发送给数据库进行预编译
            //返回了一个PreparedStatement对象用于执行预编译的SQL语句
            statement = connection.prepareStatement(sql);
            statement.setInt(1,blogId);
            //3.执行SQL语句
            resultSet = statement.executeQuery();
            //4.遍历结果集合, 由于 blogID 在 blog 表中是唯一的(它是主键)
            // 此时的查询结果, 要么是没有查到任何数据, 要么只有一条记录!!
            //此处可以不使用while,直接使用if即可
            if (resultSet.next()) {
                Blog blog = new Blog();
                blog.setBlogId(resultSet.getInt("blogId"));
                blog.setTitle(resultSet.getString("title"));
                blog.setContent(resultSet.getString("content"));
                blog.setPostTime(resultSet.getTimestamp("postTime"));
                blog.setUserId(resultSet.getInt("userId"));
                return blog;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            //5.释放必要的资源
            DBUtil.close(connection,statement,resultSet);
        }
        return null;
    }

    //3.直接查出数据库中所有的博客列表(用于博客列表页)
    public List<Blog> selectAll() {
        List<Blog> blogs = new ArrayList<>();
        //连接数据库的对象
        Connection connection = null;
        //执行预编译SQL语句的对象
        PreparedStatement statement = null;
        //遍历结果结合的对象
        ResultSet resultSet = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造SQL语句  在SQL中加入order by 让 postTime 按降序排序
            String sql = "select * from blog order by postTime desc";
            //获取执行预编译SQL语句的对象
            statement = connection.prepareStatement(sql);
            //3. 执行SQL语句
            resultSet = statement.executeQuery();
            //4.遍历结果结合
            while (resultSet.next()) {
                Blog blog = new Blog();
                blog.setBlogId(resultSet.getInt("blogId"));
                blog.setTitle(resultSet.getString("title"));
                //注意这里的正文!!! 在博客列表页中, 我们不需要把整个正文内容显示出来!!
                String content = resultSet.getString("content");
                if(content.length() >= 100) {
                    content = content.substring(0,100) + "...";
                }
                blog.setContent(content);
                blog.setPostTime(resultSet.getTimestamp("postTime"));
                blog.setUserId(resultSet.getInt("userId"));
                blogs.add(blog);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            //5.释放必要的资源
            DBUtil.close(connection,statement,resultSet);
        }

        return blogs;
    }

    //4.删除指定博客
    public void delete(int blogId) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造SQL语句
            String sql = "delete from blog where blogId = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, blogId);
            //3.执行SQL语句
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            //4.释放必要的资源
            DBUtil.close(connection, statement, null);
        }
    }

}
