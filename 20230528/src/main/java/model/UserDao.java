package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * model.User: WuYimin
 * Date: 2023-05-28
 * Time: 10:28
 */
//针对用户表提供的基本操作
//由于此处没有写注册的功能, 也就不必 add
//也没有用户删号功能, 也就不必 delete
public class UserDao {

        //1.根据 userId 来查询用户信息
    public User selectById(int userId) {
        //获取连接数据库的对象
        Connection connection = null;
        //获取执行预编译SQL语句的对象
        PreparedStatement statement = null;
        //获取遍历结果集合的对象
        ResultSet resultSet = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造SQL语句
            String sql = "select * from user where userId = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,userId);
            //3.执行SQL语句
            resultSet = statement.executeQuery();
            //4.遍历结果结果
            if (resultSet.next()) {
                User user = new User();
                user.setUserId(resultSet.getInt("userId"));
                user.setUsername(resultSet.getString("username"));
                user.setPassword(resultSet.getString("password"));
                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            //5.释放必要的资源
            DBUtil.close(connection,statement,resultSet);
        }
        return null;
    }

    //2.    根据username来查询用户信息(登录的时候)
    public User selectByUsername(String username) {
        //获取连接数据库的对象
        Connection connection = null;
        //获取执行预编译SQL语句的对象
        PreparedStatement statement = null;
        //获取遍历结果集合的对象
        ResultSet resultSet = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造SQL语句
            String sql = "select * from user where username = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1,username);
            //3.执行SQL语句
            resultSet = statement.executeQuery();
            //4.遍历结果结果
            if (resultSet.next()) {
                User user = new User();
                user.setUserId(resultSet.getInt("userId"));
                user.setUsername(resultSet.getString("username"));
                user.setPassword(resultSet.getString("password"));
                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            //5.释放必要的资源
            DBUtil.close(connection,statement,resultSet);
        }
        return null;
    }

    //3. 将 userId 和username 插入数据库 (注册的时候)
    public User adduser(String username, String password ) {
        // 获取数据库连接的对象
        Connection connection =null;
        // 获取预编译SQL语句的对象
        PreparedStatement statement = null;
        try {
            //1.和数据库建立连接
            connection =DBUtil.getConnection();
            //2.构造SQL语句
            String sql = "insert into user values(null,?,?) ";
            statement = connection.prepareStatement(sql);
            statement.setString(1,username);
            statement.setString(2,password);
            //3.执行SQL语句
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtil.close(connection,statement,null);
        }
        return null;
    }
}
