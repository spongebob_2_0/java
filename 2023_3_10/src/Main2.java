import java.util.*;
import java.util.Scanner;

public class Main2 {

    public static void main(String[] args) {
        ArrayDeque deque = new ArrayDeque();

        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String name = scanner.next();
            // 初始化队列中的数据
            deque.offerLast(name);
        }

        // write your code here......
        while (!deque.isEmpty()) {
            System.out.println(deque.pollFirst());
            if(!deque.isEmpty()) {
                System.out.println(deque.pollLast());
            }
        }
    }

}
