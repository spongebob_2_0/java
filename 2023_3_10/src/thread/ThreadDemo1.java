package thread;

import sun.awt.windows.ThemeReader;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-11
 * Time: 22:15
 */

class MyThread extends Thread {
    @Override
    public void run() {
        while (true) {
            System.out.println("hello-run");
        }
    }
}

public class ThreadDemo1 {
    public static void main(String[] args) {
        Thread t =new MyThread();
        t.start();
        while (true) {
            System.out.println("hello-main");
        }
    }
}
