package thread;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int score = scanner.nextInt();

        //write your code here......
        try {
            if(score>0 && score<100) {
                System.out.println(score);
            }else {
                throw new ScoreException("分数不合法");
            }
        }catch (ScoreException e) {
            System.out.println(e.getMessage());
        }

    }
}

class ScoreException extends Exception {

    public ScoreException(String message) {
        super(message);
    }
}