import java.util.*;

public class Main3 {
    public static void main(String[] args) {
        Scanner scanner  = new Scanner(System.in);
        String name = scanner.next();
        Map<Integer, String> map = new HashMap<Integer, String>();
        map.put(1, "Amy");
        map.put(2, "Joe");
        map.put(3, "Tom");
        map.put(4, "Susan");

        for(Map.Entry<Integer,String> entry: map.entrySet()) {
            System.out.println(entry.getKey()+":"+entry.getValue());
        }
        map.put(5,name);
        map.remove(4);
        map.put(3,"Tommy");
        System.out.println();

        for(Map.Entry<Integer,String> entry: map.entrySet()) {
            System.out.println(entry.getKey()+":"+entry.getValue());
        }
    }
}
