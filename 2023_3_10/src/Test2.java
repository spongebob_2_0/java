import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-11
 * Time: 18:20
 */
public class Test2 {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        boolean negative = scanner.hasNextBoolean();
        System.out.println(posNeg(a, b, negative));
    }

    private static Boolean posNeg(int a, int b, boolean negative) {
        if(negative == true) {
            if(a<0 && b <0) {
                return true;
            }else {
                return false;
            }
        }else {
            if((a<0 && b>0) || (a>0 && b<0)){
                return true;
            }else {
                return false;
            }
        }
    }
}
