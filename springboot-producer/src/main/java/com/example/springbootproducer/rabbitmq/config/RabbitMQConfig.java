package com.example.springbootproducer.rabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-08-25
 * Time: 14:17
 */
@Configuration
public class RabbitMQConfig {


    @Bean
    public DirectExchange directExchange(){
        return new DirectExchange("TemplateDirectEx",false,false);
    }

    @Bean
    public FanoutExchange fanoutExchange(){
        return new FanoutExchange("TemplateFanoutEx",false,false);
    }

    @Bean
    public TopicExchange topicExchange(){
        return new TopicExchange("TemplateTopicEx",false,false);
    }

    /*创建队列*/
    @Bean
    public Queue directQueue1(){
        return new Queue("directQueue1",true);
    }

    @Bean
    public Queue directQueue2(){
        return new Queue("directQueue2",true);
    }

    @Bean
    public Queue topicQueue1(){
        return QueueBuilder.durable("topicQueue1").build();
    }

    @Bean
    public Queue topicQueue2(){
        return QueueBuilder.durable("topicQueue2").build();
    }	/*创建绑定关系方法一*/
    @Bean
    public Binding directBind1(){
        return new Binding("directQueue1", Binding.DestinationType.QUEUE,
                "TemplateDirectEx","WeiXin",null);
    }

    @Bean
    public Binding directBind2(){
        return BindingBuilder.bind(new Queue("directQueue2",false))
                .to(new DirectExchange("TemplateDirectEx"))
                .with("WeiXin");
    }
}
