import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-19
 * Time: 11:03
 */
public class Test1 {
    public static double func(int n){
        int flag =1;
        double sum =0;
        for (int i = 1; i <= n; i++) {
            sum +=1.0/i*flag;
            flag =-flag;
        }
        return sum;
    }
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int n = scanner.nextInt();
        double sum =func(n);
        System.out.println(sum);
    }
}
