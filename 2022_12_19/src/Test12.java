/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-19
 * Time: 22:30
 */
public class Test12 {
    public static String myToString(int[] array) {
        if(array ==null){
            return "null";
        }
        String ret = "[";
        for (int i = 0; i < array.length; i++) {
                ret += array[i];
                if(i<array.length-1){
                    ret +=", ";
                }
        }
        ret = ret + "]";
        return ret;
    }
    public static void main(String[] args) {
        int[] array ={1,2,3,4};
        System.out.println(myToString(array));
    }
}
