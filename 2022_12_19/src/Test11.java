import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-19
 * Time: 19:26
 */
public class Test11 {
    public static void printf(int[] array){
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]+" ");
        }
    }
    public static void main(String[] args) {
        int[] array=new int[100];
        for (int i = 0; i < 100; i++) {
            array[i]=i+1;
        }
//        System.out.println(Arrays.toString(array));
        printf(array);
    }
}
