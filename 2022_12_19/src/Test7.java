/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-19
 * Time: 15:48
 */
public class Test7 {
    public static int func(int n){
        if(n==1){
            return 1;
        }
        return n+func(n-1);
    }
    public static int func1(int n){
        if(n<10){
            return n;
        }
        return n%10 +func1(n/10);
    }
    public static void main(String[] args) {
//        System.out.println(func(100));
        System.out.println(func1(1234));
    }
}
