import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-19
 * Time: 11:42
 */
public class Test4 {
    //循环求斐波那契数列
    public static int func(int n){
        if(n<=2){
            return 1;
        }
        int f1 =1;
        int f2 =1;
        int f3 =0;
        for (int i = 3; i <= n; i++) {
            f3 =f1+f2;
            f1 =f2;
            f2 =f3;
        }
        return f3;
    }
    //递归求斐波那契数列
    public static int func1(int n){
        if(n<3){
            return 1;
        }
        return func(n-1)+func(n-2);
    }
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int n = scanner.nextInt();
//        System.out.println(func(n));
        System.out.println(func1(n));
    }
}
