/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-20
 * Time: 0:32
 */
public class Test13 {
    public static int find(int[] array,int n){
        for (int i = 0; i < array.length; i++) {
            if(n==array[i]){
                return i;
            }
        }
        return -1;
    }
    public static void TwoFind(int[] array,int n){
        int left =0;
        int right =array.length-1;
        while (left<=right) {
            int mid = (left + right) / 2;
            if (n > array[mid]) {
                left = mid + 1;
            } else if (n < array[mid]) {
                right = mid - 1;
            } else {
                System.out.println("恭喜你,找到了,下标是" + mid);
                break;
            }
        }
        if(left>right){
            System.out.println("找不到!");
        }
    }
    public static void main(String[] args) {
        int[] array ={1,2,3,4,5,6,7,8,};
            /*int ret = find(array,0);
        System.out.println(ret);*/
        TwoFind(array,8);
    }
}
