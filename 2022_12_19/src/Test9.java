import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-19
 * Time: 17:51
 */
public class Test9 {
    public static void hannta(int n,char pos1,char pos2,char pos3){
        if(n ==1){
            move(pos1,pos3);
            return;
        }
        hannta(n-1,pos1,pos3,pos2);
        move(pos1,pos3);
        hannta(n-1,pos2,pos1,pos3);
    }
    public static void move(char pos1,char pos2){
        System.out.print(pos1+"移动到"+pos2+" ");
    }
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        System.out.print("请输入汉诺塔的个数:");
        int n = scanner.nextInt();
        hannta(n,'A','B','C');
    }
}
