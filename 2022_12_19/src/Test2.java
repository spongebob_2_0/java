import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-19
 * Time: 11:19
 */
public class Test2 {
    public static void func(int n) {
        while (n != 0) {
            System.out.print(n % 10 + " ");
            n /= 10;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        func(n);
    }
}
