import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-19
 * Time: 11:24
 */
public class Test3 {
    public  static void login(){
        Scanner scanner =new Scanner(System.in);
        String password ="123456";
        int count =3;
        while (true){
            System.out.print("请输入密码:");
            String n = scanner.next();
            if(n.equals(password)){
                System.out.println("密码正确,登录成功!");
                return;
            }else{
                count--;
                if(count ==0 ){
                    System.out.println("账户冻结!");
                    return;
                }
                System.out.println("密码错误,你还有"+count+"次机会");
            }
        }
    }

    public static void main(String[] args) {
        login();
    }
}
