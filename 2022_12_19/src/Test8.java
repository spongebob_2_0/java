/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-19
 * Time: 16:53
 */
public class Test8 {
    public static void hannota(int n,char pos1,char pos2,char pos3){
        if(n==1){
            move(pos1,pos3);
            return;
        }
        hannota(n-1,pos1,pos3,pos2);
        move(pos1,pos3);
        hannota(n-1,pos2,pos1,pos3);
    }
    public static void move(char pos1,char pos2){
        System.out.print(pos1+"->"+pos2+" ");
    }
    public static void main(String[] args) {
        hannota(1,'A','B','C');
        System.out.println();
        hannota(2,'A','B','C');
        System.out.println();
        hannota(3,'A','B','C');
    }
}
