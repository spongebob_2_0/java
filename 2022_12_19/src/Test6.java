import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-19
 * Time: 14:51
 */
public class Test6 {
    public static void func(int n){
        if(n<10){
            System.out.print(n+" ");
            return;
        }
        func(n/10);
        System.out.print(n%10+" ");
    }
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int n = scanner.nextInt();
        func(n);
    }
}
