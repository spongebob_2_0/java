import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-19
 * Time: 12:18
 */
public class Test5 {
    public static int func(int n){
        if(n==1){
            return 1;
        }
        return n*func((n-1));
    }
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(func(n));
    }
}
