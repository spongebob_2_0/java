/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-04
 * Time: 22:08
 */

import java.util.*;

public class Test2 {
    public static void transform(int[] arr){
        for(int i=0;i<arr.length;i++){
            arr[i]=arr[i]*2;
        }
    }
    public static void main(String[] args) {
        int[] arr ={1,2,3};
        transform(arr);
        System.out.println(Arrays.toString(arr));
    }
}
