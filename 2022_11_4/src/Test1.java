/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-04
 * Time: 20:37
 */

import java.util.*;

public class Test1 {
    public static String myToString(int[] arr){
        if(arr==null){
            return "null";
        }
        String ret ="[";
        for(int i =0;i<arr.length;i++){
            ret +=arr[i];
            if(i !=arr.length-1){
                ret +=", ";
            }
        }
        ret = ret+"]";
        return ret;
    }
    public static void main(String[] args) {
        int[] arr ={1,2,3,4,5,6};
        myToString(arr);
        System.out.println(myToString(arr));
        System.out.println(myToString(null));
    }
}
