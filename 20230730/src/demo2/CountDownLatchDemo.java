package demo2;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchDemo {
    private static final int THREAD_COUNT = 3;

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(THREAD_COUNT);

        for (int i = 0; i < THREAD_COUNT; i++) {
            new Thread(() -> {
                // 模拟任务执行
                try {
                    Thread.sleep((long) (Math.random() * 1000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.println(Thread.currentThread().getName() + " 执行完成");
                countDownLatch.countDown();
            }).start();
        }

        // 等待所有子线程执行完毕
        countDownLatch.await();

        // 输出最终结果
        System.out.println("所有子线程执行完毕，主线程继续执行");
    }
}