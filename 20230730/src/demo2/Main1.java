package demo2;

import sun.awt.windows.ThemeReader;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-30
 * Time: 21:02
 */
public class Main1 {
    private static Object oj  = new Object();
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(3);
        ExecutorService threadPool = Executors.newFixedThreadPool(10);
        for (int i = 0; i < 10; i++) {
            threadPool.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                            semaphore.acquire(); //获取资源
                            System.out.println(Thread.currentThread().getName() + "获取到停车位");
                            Thread.sleep(2000);

                            semaphore.release(); //释放资源  并发执行
                            System.out.println(Thread.currentThread().getName() + "释放到停车位");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        threadPool.shutdown();
    }
}
