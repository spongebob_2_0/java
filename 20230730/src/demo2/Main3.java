package demo2;

import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-08-01
 * Time: 14:17
 */
public class Main3 {
    public static boolean flags = true;

    public static void main(String[] args) throws InterruptedException {
        new Thread(()->{
            while (flags) {
//                System.out.println("flags被修改，线程停止");
                System.out.println("hello ---- A");
            }
        },"A").start();
        TimeUnit.SECONDS.sleep(5);
        new Thread(()->{
            flags = false;
        },"B").start();
    }
}
