package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-30
 * Time: 21:25
 */
public class Main {
    public static void main(String[] args) throws InterruptedException {
        Object ob = new Object();
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                //默认false
                while (true) {
                    //默认false  i++  当前i,下一次为i+1
                    try {
                        // 当调用interrupt方法的时候,遇见阻塞的方法,不管是什么方法,都会InterruptedException
                        //立即结束阻塞状态,并且立即清除标志位为false
                        Thread.sleep(3000);
                        System.out.println("循环中...");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }finally {
                        break;
                    }
                    //跳出循环的条件    Thread.currentThread().isInterrupted() 会被 t.interrupt(); //设置表中为true
//                    if (Thread.currentThread().isInterrupted() == true) {
////                        System.out.println(Thread.interrupted());
//                        break;
//                    }
                }
            }
        });

        t.start();
//        t2.start();
        t.interrupt(); //设置表中为true
        Thread.sleep(2000);
        System.out.println(Thread.currentThread().getName() + "线程执行完毕");

    }
}
