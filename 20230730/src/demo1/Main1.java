package demo1;

import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-30
 * Time: 18:47
 */
class Test {
    public synchronized static void func () {
        System.out.println(Thread.currentThread().getName() + "进入");
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public synchronized static void  func2(){
        System.out.println(Thread.currentThread().getName() + "进入");
    }
}
public class Main1 {
    public static void main(String[] args) {
        Test t = new Test();
        Test t3 = new Test();
        Test t2 = new Test();

        new Thread(new Runnable() {
            @Override
            public void run() {
                t.func();
            }
        },"线程1").start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                t3.func();
            }
        },"线程3").start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                t2.func2();
            }
        },"线程2").start();
    }
}
