public class Solution3 {
    public boolean Find(int target, int [][] array) {
        if(array == null || array.length == 0 || array[0].length == 0) {
            return false;
        }
        int m =array.length;   //行
        int n =array[0].length;//列
        for(int i =m-1,j=0; i>=0 && j<n;) {
            if(array[i][j] > target) {
                i--;
            }else if(array[i][j] < target) {
                j++;
            }else {
                return true;
            }
        }
        return false;
    }
}
