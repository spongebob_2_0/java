import java.util.*;

/*
 * public class ListNode {
 *   int val;
 *   ListNode next = null;
 *   public ListNode(int val) {
 *     this.val = val;
 *   }
 * }
 */

public class Solution2 {
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     * 
     * @param head ListNode类 
     * @return ListNode类
     */
    public static ListNode oddEvenList (ListNode head) {
        // write code here
        ListNode fast =head;
        ListNode slow =head.next;
        ListNode cur =slow;
        while(fast != null && slow != null) {
            if(slow != null) {
                fast.next =slow.next;
                if(fast.next != null) {
                    fast = fast.next;
                }
            }
            if(fast != null) {
                slow.next =fast.next;
                slow =slow.next;
            }
        }
        fast.next =cur;
        return head;
    }

    public static void main1(String[] args) {
        ListNode head1 =new ListNode(41);
        ListNode head2 =new ListNode(67);
        ListNode head3 =new ListNode(34);
        ListNode head4 =new ListNode(0);
        ListNode head5 =new ListNode(69);
        ListNode head6 =new ListNode(24);
        ListNode head7 =new ListNode(78);
        ListNode head8 =new ListNode(68);
        ListNode head9 =new ListNode(62);
        ListNode head10 =new ListNode(64);
        head1.next =head2;
        head2.next =head3;
        head3.next =head4;
        head4.next =head5;
        head5.next =head6;
        head6.next =head7;
        head7.next =head8;
        head8.next =head9;
        head9.next =head10;
        ListNode cur =oddEvenList(head1);
        while (cur != null) {
            System.out.print(cur.val + " ");
            cur =cur.next;
        }
    }

    public static void main(String[] args) {
        Set<Integer> set =new HashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
    }
}