import java.util.*;


class ListNode {
  int val;
  ListNode next = null;

    public ListNode(int val) {
        this.val = val;
    }
}


public class Solution1 {
    /**
     *
     * @param head ListNode类 the head node
     * @return ListNode类
     */
    public static ListNode sortInList (ListNode head) {
        // write code here
        ListNode pHead =new ListNode(0);
        ListNode prev =pHead;
        ListNode cur =head;
        int len =0;
        while(cur != null) {
            cur =cur.next;
            len++;
        }
        cur =head;
        int[] array =new int[len];
        int i =0;
        while(cur != null) {
            array[i] =cur.val;
            i++;
            cur =cur.next;
        }
        bubbleSort(array);
        for(i =0; i<array.length;i++) {
            prev.next =new ListNode(array[i]);
            prev =prev.next;
        }
        return pHead.next;

    }
    private static void bubbleSort(int[] array) {
        for(int i =0; i<array.length-1; i++) {
            boolean flg =true;
            for(int j =0; j<array.length-1-i; j++) {
                if(array[j] > array[j+1]) {
                    int tmp =array[j];
                    array[j] =array[j+1];
                    array[j+1] =tmp;
                }
            }
            if(flg == true) {
                return;
            }
        }
    }

    public static void main(String[] args) {
        ListNode head1 =new ListNode(1);
        ListNode head2 =new ListNode(3);
        ListNode head3 =new ListNode(2);
        ListNode head4 =new ListNode(4);
        ListNode head5 =new ListNode(5);
        head1.next =head2;
        head2.next =head3;
        head3.next =head4;
        head4.next =head5;

        ListNode cur = sortInList(head1);
        while (cur != null) {
            System.out.print(cur.val+" ");
            cur =cur.next;
        }
    }
}