$(document).ready(function() {
    // 获取 DOM 元素
// 获取 DOM 元素
let containerDiv = $('.container');
let fromInput = $('#from-input');
let toInput = $('#to-input');
let messageInput = $('#message-input');
let submitButton = $('#submit');
let revertButton = $('#revert');
let messageDisplay = $('#messageDisplay');
    // 音乐播放器元素
    let musicSelect = $('#music-select');
    let musicUpload = $('#music-upload');
    let adminUpload = $('#admin-upload');
    let player = $('#player');
    let playPauseButton = $('#play-pause');
  
    // 消息列表
    let messages = [];
  
    // 提交按钮点击事件
    submitButton.click(function() {
        let fromValue = fromInput.val();
        // console.log('From: ', fromValue);  // 打印 from 的值
    
        let to = toInput.val();
        let msg = messageInput.val();
    
      
        // 验证输入内容
        if (fromValue === '' || to === '' || msg === '') {
          return;
        }
      
        // 创建新的消息元素
        let newDiv = $('<div>').addClass('row message').html(fromValue + ' 对 ' + to + ' 说: ' + msg);
        messageDisplay.prepend(newDiv); // 逆序插入新消息
      
        // 清空输入框
        fromInput.val('');
        toInput.val('');
        messageInput.val('');
      
        // 封装请求体
        let body = {
          "fromTo": fromValue,
          "to": to,
          "message": msg
        };
        let strBody = JSON.stringify(body);
      
        // 发送请求，保存消息
        $.ajax({
          type: 'post',
          url: '/savelovemessage',
          data: strBody,
          contentType: "application/json; charset=utf8",
          success: function(responseBody) {
            // 保存成功后，添加消息到 messages 数组
            alert("成功发送表白!");
            messages.push({
              id: responseBody,
              element: newDiv
            });
          }
        });
      });
      
  
    // 撤销按钮点击事件
    revertButton.click(function() {
      if (messages.length === 0) {
        alert("当前你还未发布表白信息!");
        return;
      }
  
      // 请求后端删除当前用户的最新消息
      $.ajax({
        type: 'post',
        url: '/deletelovemessage',
        success: function(responseBody) {
          if (responseBody == -1) {
            alert("当前你还未发布表白信息!");
            return;
          }
          // 从 messages 数组中移除被删除的消息
          for (let i = 0; i < messages.length; i++) {
            if (messages[i].id == responseBody) {
              alert("删除成功!");
              // 检查元素是否仍然是其父节点的子节点
              if (messageDisplay.has(messages[i].element).length > 0) {
                messages[i].element.remove();
              }
              messages.splice(i, 1);
              break;
            }
          }
        }
      });
    });
  
    // 定义函数来获取并展示所有消息
    function fetchAllMessages() {
      $.ajax({
        type: 'post',
        url: '/getAllmessage',
        success: function(body) {
          // 清空消息显示框
          messageDisplay.empty();
          messages = []; // 也清空 messages 数组
  
          // 逆序展示消息
          for (let i = body.length - 1; i >= 0; i--) {
            let message = body[i];
            let rowDiv = $('<div>').addClass('row message').text(message.fromTo + ' 对 ' + message.to + ' 说: ' + message.message).attr('data-id', message.messageId);
            messageDisplay.append(rowDiv);
            messages.push({
              id: message.messageId,
              element: rowDiv
            });
          }
        }
      });
    }
  
    // 初始调用来获取并展示消息
    fetchAllMessages();
    // 每4秒获取并展示消息的函数
    function fetchAndUpdateMessages() {
      setInterval(function() {
        fetchAllMessages();
      }, 4000);
    }
    fetchAndUpdateMessages();
  
    // 判断用户是否登录,获取用户信息，以判断是否显示上传按钮
    function checkLoginStatus() {
      $.ajax({
        type: 'post',
        url: '/user/getuserinfo',
        success: function(body) {
          if (body.userId == 0) {
            // 用户未登录
            alert("用户未登录");
            location.href = 'login.html';
            console.log('用户未登录');
          } else {
            // 用户已登录
            // 用户是管理员
            if (body.userId == 1) {
              adminUpload.show();
            }
            console.log('用户已登录');
          }
        }
      });
    }
  
    // 页面加载时检查登录状态
    checkLoginStatus();
  
    // 创建一个函数用来检查所有输入框是否已填满
    // 创建一个函数用来检查所有输入框是否已填满
    function isInputFull() {
        if (fromInput.val() === '' || toInput.val() === '' || messageInput.val() === '') {
          return false;
        } else {
          return true;
        }
      }
  
  
    // 添加一个键盘事件监听器，当所有输入框被填满且按下回车键时，自动提交表单
    $(document).keyup(function(event) {
      if (event.key === 'Enter' && isInputFull()) {
        submitButton.click();
      }
    });
  
    // 管理员点击上传音乐按钮，触发音乐上传输入框的点击事件
    adminUpload.click(function() {
      musicUpload.click();
    });
  
    // 音乐上传输入框的改变事件
    musicUpload.change(function() {
      if (this.files.length === 0) {
        return;
      }
  
      let file = this.files[0];
  
      // 验证文件类型
      // if (file.type !== 'audio/mp3') {
      //     alert('只能上传 MP3 格式的音频文件');
      //     return;
      // }
  
      let formData = new FormData();
      formData.append('musicFile', file);
  
      $.ajax({
        type: 'post',
        url: '/user/uploadMusic',
        data: formData,
        contentType: false,
        processData: false,
        success: function(body) {
          if (body == 1) {
            alert("上传成功! ");
            location.href = location.href;
            fetchMusicList();
          } else {
            alert(body);
          }
        }
      });
    });
  
    // 音乐选择下拉列表的改变事件
    musicSelect.change(function() {
      player.attr('src', $(this).val());
      player[0].play(); // 这行新代码会立即播放选中的音乐
      playPauseButton.text('暂停'); // 同步更新播放/暂停按钮的文字
    });
  
    // 播放/暂停按钮的点击事件
    playPauseButton.click(function() {
      if (player[0].paused) {
        player[0].play();
        $(this).text('暂停');
      } else {
        player[0].pause();
        $(this).text('播放');
      }
    });
  
    // 获取音乐列表并更新下拉列表的函数
    function fetchMusicList() {
      $.ajax({
        type: 'post',
        url: '/user/musicList',
        success: function(body) {
          musicSelect.empty();
          for (let music of body) {
            let option = $('<option>').val(music).text(music.split('-').pop().trim());
            musicSelect.append(option);
          }
          player.attr('src', body[0]);
        }
      });
    }
  
    // 页面加载时获取音乐列表
    fetchMusicList();
  });
  