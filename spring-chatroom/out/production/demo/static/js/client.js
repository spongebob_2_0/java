
// let myusername = localStorage.getItem("username");
// 这里实现标签页的切换
function initSwitchTab() {
    //1.获取到相关的元素(标签页的按钮,会话列表和好友列表)
    let tabSession = document.querySelector(".tab .tab-session");
    let tabFriend = document.querySelector(".tab .tab-friend");
    //querySelectorAll 可以同时选中多个元素,得到的结果是个数组
    //[0] 好友列表
    //[1] 会话列表
    let lists = document.querySelectorAll(".list");
    //2.针对标签按钮,注册点击事件
    //如果是点击会话按钮,就会把会话按钮的图片进行设置
    //同时把会话列表显示出来 ,把好友列表隐藏
    //如果是点击好友标签按钮,就会把好友列表按钮的图片进行设置
    //同时把好友列表显示出来,把会话列表隐藏
    tabSession.onclick = function() {
        //a. 设置图标
        tabSession.style.backgroundImage = 'url(image/对话信息_填充.png)';
        tabFriend.style.backgroundImage = 'url(image/用户未被点击.png)';
        //b. 让会话列表显示出来,让好友列表进行隐藏
        lists[0].classList = "list";
        lists[1].classList = "list hide";

    }
    tabFriend.onclick = function() {
        //a. 设置图标
        tabFriend.style.backgroundImage = 'url(image/用户被点击.png)';
        tabSession.style.backgroundImage = 'url(image/对话信息_填充2.png)';
        //b. 让会话列表显示出来,让好友列表进行隐藏
        lists[0].classList = "list hide";
        lists[1].classList = "list";
    }
}
initSwitchTab();

//操作 websocket  
//创建 websocket 的实例
// let websocket = new WebSocket("wss://" + location.host +"/websocketmessage");
let websocket = new WebSocket("ws://" + location.host +"/websocketmessage");


websocket.onopen = function() {
    console.log("websocket 连接成功! ");
}

websocket.onmessage = function(e) {
    console.log("websocket 收到消息! " + e.data);
    //此时收到的e.data 是个 json 字符串, 需要转成 js 对象    
    let resp = JSON.parse(e.data);
    if(resp.type == 'message') {
        //处理消息响应
        handleMessage(resp);
    }else {
        //resp 的 type 出错
        console.log("resp.type 不符合要求! ")
    }
}


function handleMessage(resp) {
    //把客户端收到的消息给展示出来
    //展示到对应的会话预览区域, 以及右侧消息列表中

    //1. 根据响应中的 sessionId 获取到当前会话的对应的 li 标签
    //   如果 li 标签不存在
    let curSessionLi = findSessionLi(resp.sessionId);
    if(curSessionLi == null) {
        //此时需要创建一个新的 li 标签, 表示新会话
        curSessionLi = document.createElement('li');
        curSessionLi.setAttribute('message-session-id',resp.sessionId);
        //此处 p 标签内部应该放消息的 预览内容, 一会后面统一完成, 这里先置空
        curSessionLi.innerHTML = '<h3>' + resp.fromName + '</h3>'
                + '<p></p>';
        //给这个li 标签页加上点击事件的处理
        curSessionLi.onclick = function() {
            clickSession(curSessionLi);
        }
    }
    //2. 把新的消息,展示到会话的预览区域(li 标签里的 p 标签中)
    //   如果消息太长,就需要进行截断
    let p  = curSessionLi.querySelector('p');
    p.innerHTML = resp.content;
    if(p.innerHTML.length > 10) {
        p.innerHTML = p.innerHTML.substring(0,10) + '...';
    }
    //3. 把收到消息的会话,给放到会话列表最上面
    let sessionListUL = document.querySelector('#session-list');
    sessionListUL.insertBefore(curSessionLi, sessionListUL.children[0]);

    //4. 如果当前收到消息的会话处于被选中状态, 则把当前的消息给放到右侧列表中
    //   新增消息的同时,注意调整滚动条的位置,保证新消息虽然在底部, 但是能够被用户直接看到
    //   其它操作,还可以在会话窗口给个提示(红色的数字, 有几条消息未读), 还可以播放个提示音
    let messageShowDiv = document.querySelector('.right .message-show');
    if(curSessionLi.className == 'selected') {
        //把消息列表添加一个新消息
        addMessage(messageShowDiv, resp);
        scrollBottom(messageShowDiv);
    }

}

function findSessionLi(targetSessionId) {
    //获取到所有的会话列表中的 li 标签
    let sessionLis = document.querySelectorAll('#session-list li');
    for(let li of sessionLis) {
        let sessionId = li.getAttribute('message-session-id');
        if(sessionId == targetSessionId) {
            return li;
        }
    }
    //什么情况下会触发下面这个操作呢,如果当前是新的用户直接给当前用户发送消息,此时没存在现成的li
    return null;
}


websocket.onclose = function() {
    console.log("websocket 连接关闭! ");
}

websocket.onerror = function() {
    console.log("websocket 连接异常");
}


function initSendButton() {
    // 获取发送按钮、消息输入框和发送选项下拉框
    let sendButton = document.querySelector("#sendButton");
    let messageInput = document.querySelector(".right .message-input");
    let sendOption = document.querySelector("#sendOption");
  
    // 给发送按钮注册点击事件
    sendButton.onclick = function () {
      // 判断输入框内容是否为空
      if (!messageInput.value) {
        return;
      }
  
      // 获取当前选中的 li 标签的 sessionId
      let selectedLi = document.querySelector("#session-list .selected");
      if (selectedLi == null) {
        return;
      }
      let sessionId = selectedLi.getAttribute("message-session-id");
  
      // 构造 JSON 数据
      let req = {
        type: "message",
        sessionId: sessionId,
        content: messageInput.value,
      };
  
      // 将 req 转换成 JSON 格式的字符串
      req = JSON.stringify(req);
      console.log("websocket send: " + req);
  
      // 通过 WebSocket 发送消息
      websocket.send(req);
  
      // 发送完成后，清空输入框
      messageInput.value = "";
    };
  
    // 监听消息输入框的键盘事件
    messageInput.addEventListener("keydown", function (event) {
      // 获取当前选择的发送选项
      let selectedOption = sendOption.value;
  
      // 判断是否按下回车键（键码13为回车键）
      if (event.keyCode === 13) {
        // 判断发送选项，根据选择的方式触发发送按钮的点击事件
        if (selectedOption === "enter") {
          event.preventDefault(); // 阻止回车键的默认行为（防止换行）
          sendButton.click(); // 模拟点击发送按钮
        } else if (selectedOption === "ctrlEnter" && event.ctrlKey) {
          event.preventDefault(); // 阻止回车键的默认行为（防止换行）
          sendButton.click(); // 模拟点击发送按钮
        }
      }
    });
  }
  
  initSendButton();
  
  
  

// 从服务器获取到用户登录数据
function getUserInfo() {
    $.ajax({
        url:"/user/getuserinfo",
        type: "post",
        success: function(body) {
            //从服务器获取数据
            //效验结果是否有效
            if(body != null && body.userId > 0 ) {
                //如果结果有效,则把用户名显示到界面上
                //同时也可以记录userId到html的属性标签中 (以备后用)
                   let userDiv = document.querySelector(".main .left .user");
                   userDiv.innerHTML = body.username;
                   userDiv.setAttribute("user-id",body.userId);
            }else {
                //如果结果无效,则跳转到登录页面.
                alert("当前用户未登录");
                location.assign("login.html");
                
            }

        }
    });
}
getUserInfo();

//获取好友列表
function getFriendList() {
    $.ajax({
        url:"/friend/getfriendlist",
        type:"post",
        data:{},
        success: function(body) {
            //1.先把之前的好友列表清空
            let friendListUl = document.querySelector("#friend-list");
            friendListUl.innerHTML = '';
            //2.遍历body,把服务器响应的结果加回到 friend-list 中去
            for(let friend of body) {
                let li = document.createElement("li");
                li.innerHTML = '<h4>' + friend.friendName + '</h4>';
                //此处把friendId 也记录下来,以防备用
                //把 friendId 作为一个html的属性, 加到li标签上就行了
                li.setAttribute("friend-id",friend.friendId);
                friendListUl.appendChild(li);

                // 每一个li标签, 就对应界面上面一个好友的选项,给这个li标签加上点击事件
                li.onclick = function() {
                    //参数区分了 当前用户点击的是哪个好友
                    clickFriend(friend);
                }
            }
        },
        error: function() {
            console.log("获取好友列表失效! ");
        }
    });
}   
getFriendList();

//获取会话列表
function getSessionList(){ 
    $.ajax({
        url: "/message/sessionList",
        type:"post",
        data:{},
        success: function(body) {
            //1.清空之前的会话列表
            let sessionListUL = document.querySelector("#session-list");
            sessionListUL.innerHTML ="";
            //2. 遍历响应数组,针对结果来构造页面
            for(let session of body) {
                if (!session.friends || session.friends.length === 0) {
                    // 没有friends或者friends为空数组的处理逻辑
                    continue; // 跳过本次循环，处理下一个session
                }
                //针对lastMessage的长度进行截断处理
                if(session.lastMessage.length > 10) {
                    session.lastMessage = session.lastMessage.substring(0,10) + '...';
                }
                let li = document.createElement("li");
                li.setAttribute("message-session-id",session.sessionId);
                li.innerHTML = '<h3>' + session.friends[0].friendName + '</h3>'
                + '<p>' + session.lastMessage + '</p>';
                sessionListUL.appendChild(li);

                //给li标签新增点击事件
                li.onclick = function() {
                    //此处的这种写法能保证后续点击某个li标签就能触发这个回调函数
                    clickSession(li);
                }
             }
        }
    });
}

getSessionList();

//li标签的点击事件
function clickSession(currentLi) {
    //1.设置高亮
    let allLis = document.querySelectorAll("#session-list>li");
    activeSession(allLis,currentLi);
    //2.获取指定会话的历史消息
    let sessionId = currentLi.getAttribute("message-session-id");
    getHistoryMessage(sessionId);
}

//消除未被点击的li标签的高亮效果
function activeSession(allLis,currentLi) {
    //这里的循环遍历,主要目的是为了取消未被选中的li标签的className
    for(let li of allLis) {
        if(li == currentLi) {
            li.className = 'selected'; 
        }else {
            li.className = '';
        }
    }
}

//这个函数负责获取指定会话的历史消息
function getHistoryMessage(sessionId) {
    console.log("获取历史消息 sessionId = " + sessionId);
    //1.先清空右侧列表的已有内容
    let titleDiv = document.querySelector(".right .title");
    titleDiv.innerHTML = '';
    let messageShowDiv = document.querySelector(".right .message-show");
    messageShowDiv.innerHTML = '';

    //2. 重新设置会话的标题,新的会话标题就是你点击的那个会话上面显示的标题
    // 先找到当前选中的会话是哪个 , 被选中的会话带有 selected 类的
    let selectedH3 = document.querySelector('#session-list .selected>h3');
    if(selectedH3) {
        //selectedH3 是有可能不存在的,比如页面加载阶段, 可能并没有哪个会话被选中
        //此时也就没有会话带有 selected 标签, 此时就无法查询出这个 selectedH3
        titleDiv.innerHTML = selectedH3.innerHTML;
    }

    //3. 发送 ajax 请求给服务器, 获取到该会话的历史消息
    $.ajax({
        url:"/getmessage?sessionId=" + sessionId,
        type:"post",
        data: {},
        success: function(body) {
            //此处返回的body是个就是 js对象数组, 里面的每一个元素都是一条消息,
            //直接遍历即可
            for(let message of body) {
                addMessage(messageShowDiv,message);
            }
            //加个操作,在构造好消息列表之后,控制滚动条,自动滚动到最下方
            scrollBottom(messageShowDiv);
        }
    });
}

//此函数用来将返回来的每一条消息展示到对话框里面
function addMessage(messageShowDiv,message) {
    //使用这个div来表示一条消息
    let messageDiv = document.createElement("div");  // 修改这里
    //此时需要针对当前用户消息是不是用户自己发的,决定消息是靠左还是靠右
    let selfUsername = document.querySelector(".left .user").innerHTML;  // 你的原始代码里有拼写错误，你写的是selfUsernae
    if(selfUsername == message.fromName) {
        //如果用户名相等,则消息是自己发的,那么消息靠右边
        messageDiv.className = 'message message-right';  // 修改这里
    }else {
        //消息是别人发的,所以消息靠左边
        messageDiv.className = 'message message-left';  // 修改这里
    }
    messageDiv.innerHTML = '<div class="box">'
        + '<h4>' + message.fromName + '</h4>'
        + '<p>'  + message.content + '</p>'
        +  '</div>';
    messageShowDiv.appendChild(messageDiv);
}

//每次打开聊天页面,自动把messageShowDiv里面的聊天内容滚动到最底部
function scrollBottom(elem) {
    //1. 获取到可视区域的高度
    let clientHeight = elem.offsetHeight;
    //2. 获取到内容的总高度
    let scrollHeight = elem.scrollHeight;
    //3. 进行滚动操作, 第一个方向是水平方向滚动的尺寸,第二个方向是垂直方向滚动的尺寸
    elem.scrollTo(0, scrollHeight - clientHeight);

}


//点击好友列表项,触发的函数
function clickFriend(friend) {
    //1. 先判定一下当前这个好友是否有对应的会话
    //使用一个单独的函数来实现,这个函数参数是用户的名字,返回的是一个li标签,找到了就是返回对应的会话列表里的li;如果没找到,返回null
    let sessionLi = findSessionByName(friend.friendName);
    let sessionListUL = document.querySelector("#session-list")
    if(sessionLi) {
        //2. 如果存在匹配的结果,就把这个会话设置成选中状态,获取历史消息,并且置顶
        // insertBefore 把这个找到的 li 标签放到最前面去
        sessionListUL.insertBefore(sessionLi,sessionListUL.children[0]);
        //此处设置会话选中状态,获取历史消息,这两个功能在上面clickSession函数椎间盘吗已经实现
        //此处直接调用clickSession即可
        // clickSession(sessionLi);
        //或者还可以模拟一下点击操作
        sessionLi.click();
    }else {
        //3. 如果不存在匹配的结果,就创建一个新会话(创建li标签 + 通知服务器)
        sessionLi = document.createElement("li");
        //构造具体的li内容, 由于新会话没有 "最后一条消息",p标签内容设置为空即可
        sessionLi.innerHTML = '<h3>' + friend.friendName + '</h3>' + '<p></p>';
        //把标签进行置顶
        sessionListUL.insertBefore(sessionLi,sessionListUL.children[0]);
        sessionLi.onclick =function() {
            clickSession(sessionLi);
        }
        sessionLi.click();
        //发送消息给服务器,告诉服务器当前新创建的会话是怎么样的 
        cereatSession(friend.friendId,sessionLi);

    }
    //4. 还需要把标签页切换到会话列表
    // 只要找到会话列表标签页按钮,模拟一个点击操作即可
    let tabSession = document.querySelector(".tab .tab-session");
    tabSession.click();
}

function findSessionByName(username) {
    //先获取到会话列表中所有的li标签
    //然后依次遍历,看看这些li标签谁的名字和查找的名字一致
    let sessionLis = document.querySelectorAll("#session-list>li");
    for(let sessionLi of sessionLis) {
        //获取到该li标签里的h3标签,进一步得到名字
        let h3 = sessionLi.querySelector('h3');
        if(h3.innerHTML == username) {
            return sessionLi;
        }
    }
    return null;
}   

//friendId 是构造HTTP请求时必备的信息
function cereatSession(friendId,sessionLi) {
    $.ajax({
        url: "/message/session?toUserId=" + friendId,
        type: "post",
        data:{},
        success: function(body) {
            console.log("会话创建成功! sessionId = " + body.sessionId);
            sessionLi.setAttribute("message-session-id",body.sessionId);
        },
        error: function() {
            console.log("会话创建失败! ");
        }
    });
}




    //上传照片
    $(document).ready(function(){

        // 当用户选择了新的头像时
        // 我们上传这个头像并更新页面上的头像URL
        $('#upload').change(function(e){
            var file = e.target.files[0];
            uploadProfilePhoto(file);
        });

        // 当页面加载完成时,看浏览器缓存有没有保存头像
        // var myphoto = localStorage.getItem(myusername);
        // if(myphoto) {
        //     $('#profilePhoto').attr('src', myphoto);
        //     return;
        // }

        // 发起一个请求，获取当前用户的头像URL
        $.ajax({
            url: '/user/getuser',  // 获取当前用户信息的服务器接口地址
            type: 'post',
            success: function(data){
                if(data.photo == null) {
                    return;
                }

                $('#profilePhoto').attr('src', data.photo);
                // localStorage.setItem(myusername,data.photo);
            },
            error: function(){
                alert('获取头像失败');
            }
        });
    });

    function uploadProfilePhoto(file){
    // 检查文件类型
    var fileType = file.type;
    if (!fileType.startsWith('image/')) {
        // 如果文件不是图片，显示错误信息并停止执行
        alert('上传失败!请上传图片');
        return;
    }
    var formData = new FormData();
        formData.append('myfile', file);
    $.ajax({
        url: '/user/upload',  // 上传图片的服务器接口地址
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        success: function(data){
            if(data == null) {
                alert("上传失败!!");
            }
            alert("上传成功!");
            $('#profilePhoto').attr('src', data);
            // localStorage.removeItem(myusername);
            // localStorage.removeItem(myusername);
        },
        error: function(){
            alert('图片上传失败');
        }
    });
    }


//实现搜索添加好友的功能
$(document).ready(function() {
    // 搜索函数
    function search() {
        $.ajax({
            url: "/user/search",
            type: "post",
            data: {
                username: $('.search input').val()
            },
            success: function(data) {
                if(data == -1) {
                    alert("输入不能为空!");
                    return;
                }

                if (!data || data.length === 0) {
                    // 如果返回的数据为空，显示"查不到此用户"
                    alert("查不到用户! ")
                    // $('.right').append("<h3 class='title'>查不到此用户</h3>");
                    // 在这里清空输入框
                    $('.search input').val('');
                    return;
                }
                $('.title').empty();
                $('.message-show').empty();


                $('.title').text("好友查询结果");

                // 遍历返回的数据
                data.forEach(function(user) {
                    //创建一个容器用于包含用户名和添加好友按钮
                    var userContainer = $('<div style="display: flex; align-items: center; justify-content: space-between;"></div>');

                    var searchResult = $('<div class="search-result"></div>');
                    var username = $('<h4 style="color: blue;margin-left:220px"></h4>').text(user.username);
                    searchResult.append(username);

                    //将搜索结果添加到用户容器中
                    userContainer.append(searchResult);

                    var addButton = $('<button style="background-color: green; color: white; margin-left: 10px; margin-right:30%;border-radius:5px;">添加好友</button>');
                    // 使用JQuery的hover方法，第一个函数处理鼠标进入，第二个函数处理鼠标离开
                    addButton.hover(
                        function() {
                            // 鼠标进入时，改变按钮的背景色和文字颜色
                            $(this).css({
                                'background-color': 'green',
                                'color': 'yellow'
                            });
                        },
                        function() {
                            // 鼠标离开时，恢复按钮的背景色和文字颜色
                            $(this).css({
                                'background-color': 'white',
                                'color': 'black'
                            });
                        }
                    );
                    



                    //将添加按钮也添加到用户容器中
                    userContainer.append(addButton);

                    //最后将用户容器添加到消息展示区
                    $('.message-show').append(userContainer);


                    // 在这里清空输入框
                    $('.search input').val('');

                    // 点击添加按钮时执行的操作
                    addButton.click(function() {
                        var friendName = username.text();
                        // 执行添加好友的操作，发送请求给后端
                        $.ajax({
                            url: "/user/becomefriends",
                            type: "post",
                            data: {
                                friendName: friendName
                            },
                            success: function(data) {
                                if(data == -3) {
                                    alert("已经是好友关系! ");
                                    return;
                                }
                                if (data == 1) {
                                    alert("添加好友成功");
                                    location.reload();  // 刷新当前页面
                                } else if(data == -2) {
                                    // 显示错误信息
                                    alert("不能添加自己为好友! ");
                                }else {
                                    // 否则，显示错误信息
                                    alert("添加好友失败");
                                }
                            },
                            error: function() {
                                alert("添加好友请求失败");
                            }
                        });
                    });

                });
            },
            error: function() {
                alert("搜索失败");
            }
        });
    }

    // 搜索按钮点击事件
    $('.search button').click(function() {
        search();
    });

    // 输入框键盘按下事件
    $('.search input').on('keypress', function(e) {
        if (e.which == 13) {
            // 按下回车键，触发搜索
            search();
        }
    });
});


//实现注销
$(document).ready(function(){
    $(".tab-logout").click(function(){
        var confirmLogout = confirm("你确定要退出吗？");
        if(confirmLogout){
            $.ajax({
                url: "/user/logout",  
                type: 'POST',    
                data: {},        
                success: function(data) {
                    alert("注销成功"); 
                    // 重定向用户到登录页面，
                    // localStorage.removeItem("username");
                    window.location.href = "/login.html";  
                },
                error: function(xhr, status, error) {
                    // 如果注销请求失败，这里处理错误
                    console.error("注销失败: " + error);
                    alert("注销失败，请重试！");
                }
            });
        }
    });
});


//表白墙
$('.tab-friendCircleBtn').on('click', function() {
    window.location.href = 'love.html';
});
