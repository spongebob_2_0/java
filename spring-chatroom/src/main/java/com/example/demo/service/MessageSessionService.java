package com.example.demo.service;

import com.example.demo.entity.Friend;
import com.example.demo.entity.MessageSession;
import com.example.demo.entity.MessageSessionUserItem;
import com.example.demo.mapper.MessageSessionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-04
 * Time: 0:14
 */
@Service
public class MessageSessionService {

    @Autowired
    private MessageSessionMapper messageSessionMapper;

    //1. 根据userId 获取到该用户都在哪些会话中存在,返回结果是一组sessionId
    public List<Integer> getSessionIdByUserId(int userId) {
        return messageSessionMapper.getSessionIdByUserId(userId);
    }

    //2. 根据sessionId 再来查询这个会话都包含了哪些用户(刨除最初的自己)
    public List<Friend> getFriendBySessionId(int sessionId, int selfUserId) {
        return messageSessionMapper.getFriendBySessionId(sessionId,selfUserId);
    }

    //3. 新增一个会话记录,返回会话的id
    // 这样的方法返回值int表示的是插入操作影响到几行
    // 此处获取sessionId 是通过 参数messageSession 的 sessionId 属性获取的
    public int addMessageSession(MessageSession messageSession) {
        return messageSessionMapper.addMessageSession(messageSession);
    }

    //4. 给message_session_user 表也新增对应的记录
    public void addMessageSessionUser(MessageSessionUserItem messageSessionUserItem) {
        messageSessionMapper.addMessageSessionUser(messageSessionUserItem);
        return;
    }
}
