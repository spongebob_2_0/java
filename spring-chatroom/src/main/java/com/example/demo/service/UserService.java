package com.example.demo.service;

import com.example.demo.entity.Friend;
import com.example.demo.entity.Userinfo;
import com.example.demo.mapper.UserMapper;
import org.apache.catalina.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-02
 * Time: 20:43
 */
@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    //把用户插入到数据库总 --> 注册
    public int insert(Userinfo userinfo) {
        return  userMapper.insert(userinfo);
    }

    //根据用户名查询用户信息 --> 登录
     public Userinfo selectByUsername(String username) {
        return userMapper.selectByUsername(username);
     }

    //将上传的照片路径保存到数据库的对应用户信息中
    public void updateUserImgPath(int userId,String imgPathForDb) {
        userMapper.updateUserImgPath(userId,imgPathForDb);
        return;
    }

    //将用户id和好友id插入到friend表中,建立好友关系
    public void becomeFriends(@Param("userId") int userId, @Param("friendId") int friendId) {
        userMapper.becomeFriends(userId,friendId);
        return;
    }

    //根据用户名进行模糊查询
    public List<Userinfo> fuzzySearchByUsername(String username) {
        return userMapper.fuzzySearchByUsername(username);
    }

    //根据 用户id 和 好友id 去查询 是否存在好友关系
    public Friend isFriends(int userId, int friendId) {
        Friend friends = userMapper.isFriends(userId, friendId);
        return friends;
    }

    //将音乐的url路径添加到数据库
    public void addMusic(String music) {
        userMapper.addMusic(music);
        return;
    }

    //获取音乐列表
    public List<String> getAllMusic() {
        return userMapper.getAllMusic();
    }

}
