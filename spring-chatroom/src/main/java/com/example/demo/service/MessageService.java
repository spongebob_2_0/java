package com.example.demo.service;

import com.example.demo.entity.LoveMessage;
import com.example.demo.entity.Message;
import com.example.demo.mapper.MessageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-04
 * Time: 5:38
 */
@Service
public class MessageService {

    @Autowired
    private MessageMapper messageMapper;

    //通过sessionId获取最后一条会话的信息
    public String getLastMessageBySessionId(int sessionId) {
        return messageMapper.getLastMessageBySessionId(sessionId);
    }

    //获取指定会话历史消息列表
    public List<Message> getMessagesBySessionId(int sessionId) {
        return messageMapper.getMessagesBySessionId(sessionId);
    }

    //通过这个方法实现插入消息到数据库表中
    public void add(Message message) {
        messageMapper.add(message);
        return;
    }

    //向表白墙这种表插入数据
    public int addLoveMessage(String fromTo,String to, String message,String username) {
        return messageMapper.addLoveMessage(fromTo,to,message,username);
    }

    //获取用户发送的最后消息的id
    public Integer getlastMessageId(String username) {
        return messageMapper.getlastMessageId(username);
    }

    //根据id和姓名删除指定消息
    public int deleteLastMessageId(int messageId,String username) {
        return messageMapper.deleteLastMessageId(messageId,username);
    }

    //查询表白墙的所有数据
    public List<LoveMessage> getLoveMessage() {
        return messageMapper.getLoveMessage();
    }

}
