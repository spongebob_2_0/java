package com.example.demo.service;

import com.example.demo.entity.Friend;
import com.example.demo.mapper.FriendMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-03
 * Time: 21:14
 */
@Service
public class FriendService {
    @Autowired
    private FriendMapper friendMapper;

    //根据用户ID查询好友列表
    public List<Friend> selectFriendList(int userId) {
        return friendMapper.selectFriendList(userId);
    }




}
