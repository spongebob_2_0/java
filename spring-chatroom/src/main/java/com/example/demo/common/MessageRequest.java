package com.example.demo.common;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description: 表示一个请求
 * User: WuYimin
 * Date: 2023-07-05
 * Time: 4:16
 */
@Data
public class MessageRequest {

    private  String type = "message";
    private int sessionId;
    private String content;
}
