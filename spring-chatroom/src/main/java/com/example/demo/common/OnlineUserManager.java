package com.example.demo.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 创建于 IntelliJ IDEA.
 * 描述: 这个类用于跟踪当前用户的在线状态。
 * 它维护了userId与WebSocketSession之间的映射关系
 * 用户: WuYimin
 * 日期: 2023-07-05
 * 时间: 3:47
 */
@Component
public class OnlineUserManager {

    // 这个哈希表要考虑到线程安全问题, 因此使用了 ConcurrentHashMap,这意味着多个线程可以同时进行读取和写入操作，不会出现数据不一致的问题
    //WebSocketSession会有多个线程去操作（用户登录、登出和获取），因此必须使用线程安全的集合类来保存WebSocketSession。
    // 如果使用非线程安全的HashMap，可能会出现并发问题，如数据不一致、数据覆盖等问题.
    private ConcurrentHashMap<Integer, WebSocketSession> sessions = new ConcurrentHashMap<>();

    // 1. 用户上线, 在哈希表中插入键值对
    public void online(int userId, WebSocketSession session) {
        if(sessions.get(userId) != null) {
            // 如果用户已经在线，那么就会登录失败, 不会记录这个映射关系
            // 由于不记录映射关系，后续用户就接收不到任何消息（因为我们是通过这个映射关系来实现消息转发的）
            System.out.println("[" + userId + "] 已经登录，无法重复登录！ ");
            return;
        }
        // 将新上线的用户的id和对应的WebSocketSession放入映射表中
        sessions.put(userId,session);
        System.out.println("[" + userId + "] 上线了！ ");
    }

    // 2. 用户下线, 从哈希表中删除相应的键值对
    public void offline(int userId, WebSocketSession session) {
        WebSocketSession existSession = sessions.get(userId);
        // 只有当传入的session与存在的session一致时，才进行下线操作
        // 这是为了防止错误的下线请求
        if(existSession == session) {
            sessions.remove(userId);
            System.out.println("[" + userId + "] 下线了！ ");
        }
    }

    // 3. 根据用户ID获取到对应的WebSocketSession
    // 这个方法主要是在消息转发时使用，通过userId找到对应的WebSocketSession，然后向其发送消息
    public WebSocketSession getSession(int userId) {
        return sessions.get(userId);
    }
}
