package com.example.demo.common;

import com.example.demo.entity.Friend;
import com.example.demo.entity.Message;
import com.example.demo.entity.Userinfo;
import com.example.demo.service.MessageService;
import com.example.demo.service.MessageSessionService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-04
 * Time: 19:06
 */
@Component  // 标记这是一个Spring组件，将其注册为Spring容器的Bean
public class WebSocketUtils extends TextWebSocketHandler {

    @Autowired
    // 自动注入用户管理类，用于在线用户的管理
    private OnlineUserManager onlineUserManager;

    @Autowired
    // 自动注入消息会话服务，用于消息会话的处理
    private MessageSessionService messageSessionService;

    @Autowired
    // 自动注入消息服务，用于消息的处理
    private MessageService messageService;

    // 定义Jackson的ObjectMapper，用于对象与JSON之间的转换
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        //这个方法会在 websocket 连接成功后, 被自动调用
        System.out.println("WebSocketUtils 连接成功! ");
        //由于前面注册了拦截器,就可以让每个HttpSession中加入的Attribute 在WebSocketSession 也加入一份
        //然后通过在WebSocketSession中提供的API取出存入的userinfo对象
        Userinfo userinfo = (Userinfo) session.getAttributes().get("userinfo");
        if(userinfo == null) {
            return;
        }
        // 用户登录后，将用户的信息（ID和WebSocketSession）存储起来把这个键值对存起来
        onlineUserManager.online(userinfo.getUserId(), session);
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        //这个方法是在 websocket 收到消息的时候, 被自动调用的
        System.out.println("WebSocketUtils 收到消息! " + message.toString());
        //1. 先获取到当前用户的信息, 后续要进行消息转发
        Userinfo userinfo = (Userinfo) session.getAttributes().get("userinfo");
        if(userinfo == null) {
            System.out.println("[WebSocketUtils] user == null 未登录用户,无法进行消息转发" );
            return;
        }
        //2. 使用Jackson的ObjectMapper将收到的JSON格式的消息转化为MessageRequest对象
        //message是一个TextMessage对象，它表示WebSocket的文本消息。调用getPayload()方法将返回这个文本消息的字符串形式
        //进一步使用Jackson的ObjectMapper将这个字符串解析为MessageRequest对象
        MessageRequest messageRequest = objectMapper.readValue(message.getPayload(), MessageRequest.class);
        if(messageRequest.getType().equals("message")) {
            // 如果消息类型为“message”，则进行消息转发
            transferMessage(userinfo,messageRequest);
        }else {
            System.out.println("[WebSocketUtils] messageRequest.type 有误! " + message.getPayload() );
        }

    }

    //通过这个方法来完成消息实际的转发工作
    //消息转发方法，输入参数为发送消息的用户和消息请求对象
    private void transferMessage(Userinfo userinfo, MessageRequest messageRequest) throws IOException {
        //1. 先构造一个对转发的响应对象 MessageResponse
        MessageResponse  resp = new MessageResponse();
        // 设置消息响应对象的属性
        resp.setType("message");  //不设置也行,默认也就是message
        resp.setFromId(userinfo.getUserId());
        resp.setFromName(userinfo.getUsername());
        resp.setSessionId(messageRequest.getSessionId());
        //把收到的消息放到这个响应对象里面
        resp.setContent(messageRequest.getContent());
        // 使用Jackson的ObjectMapper将消息响应对象转化为JSON格式的字符串
        String respJson = objectMapper.writeValueAsString(resp);
        System.out.println("[transferMessage] respJson " + respJson);

        // 2. 根据消息请求中的sessionId和发送消息的用户ID（当前登录的用户），查询出消息会话中的所有好友列表。
        //请注意，数据库查询会自动排除当前发送消息的用户，因此需要将当前用户手动添加到好友列表中。
        List<Friend> friends = messageSessionService.getFriendBySessionId(messageRequest.getSessionId(), userinfo.getUserId());

        // 将当前用户也添加到好友列表中，以便稍后发送给自己一份响应消息。
        Friend myself = new Friend();
        myself.setFriendId(userinfo.getUserId());
        myself.setFriendName(userinfo.getUsername());
        friends.add(myself);

        // 3. 循环遍历上述好友列表，给列表中的每个用户都发送一份响应消息。
        //    注意：除了给查询到的好友发送消息，还要给自己发送一份，以便在自己的客户端上显示自己发送的消息。
        //    另外，尽管前端可能未实现群聊功能，但后端的API和数据库都支持群聊，所以这里的转发逻辑也支持群聊。
        for(Friend friend: friends) {
            // 通过每个好友的userID，查询OnlineUserManager以获取对应的WebSocketSession。
            // WebSocketSession代表了与每个在线客户端的连接，可以用它来发送消息。
            WebSocketSession webSocketSession = onlineUserManager.getSession(friend.getFriendId());

            // 如果WebSocketSession为null，表示该用户未在线，因此不进行消息发送。
            if(webSocketSession == null) {
                continue; // 跳过此次循环，处理下一个好友。
            }

            // 使用WebSocketSession发送消息给对应的客户端。消息内容是respJson，它是响应对象转换成的JSON字符串。
            webSocketSession.sendMessage(new TextMessage(respJson));
        }


        //4. 转发的消息还要放到数据库里面, 后续如果用户下线之后,重新上线, 还可以通过历史消息的方式拿到之前的消息
        //需要往 message 表里面写一条记录
        Message message = new Message();
        message.setFromId(userinfo.getUserId());
        message.setContent(messageRequest.getContent());
        message.setSessionId(messageRequest.getSessionId());
        //像自增主键,还有时间这样的属性,都可以让 SQL 在数据库中自己生成
        messageService.add(message);

    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        //这个方法是在连接异常的时候 , 被自动调用的
        System.out.println("WebSocketUtils 连接异常了! " + exception.toString());

        Userinfo userinfo = (Userinfo) session.getAttributes().get("userinfo");
        if(userinfo == null) {
            // 如果userinfo为空，即用户未登录，不做任何处理
            return;
        }
        // 用户退出登录或发生异常，将用户的信息从在线用户管理中移除
        onlineUserManager.offline(userinfo.getUserId(), session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        //这个方法是在连接正常关闭后,被自动调用的
        System.out.println("WebSocketUtils 连接关闭! " + status.toString());

        Userinfo userinfo = (Userinfo) session.getAttributes().get("userinfo");
        if(userinfo == null) {
            // 如果userinfo为空，即用户未登录，不做任何处理
            return;
        }
        // 用户退出登录或发生异常，将用户的信息从在线用户管理中移除
        onlineUserManager.offline(userinfo.getUserId(), session);
    }
}
