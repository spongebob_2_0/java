package com.example.demo.common;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description: 表示一个响应
 * User: WuYimin
 * Date: 2023-07-05
 * Time: 4:16
 */
@Data
public class MessageResponse {

    private  String type = "message";
    private int fromId;
    private String fromName;
    private int sessionId;
    private String content;
}
