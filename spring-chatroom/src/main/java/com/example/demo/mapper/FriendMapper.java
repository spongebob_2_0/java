package com.example.demo.mapper;

import com.example.demo.entity.Friend;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-03
 * Time: 20:45
 */
@Mapper
public interface FriendMapper {
    //根据用户ID查询好友列表
    List<Friend> selectFriendList(int userId);
}
