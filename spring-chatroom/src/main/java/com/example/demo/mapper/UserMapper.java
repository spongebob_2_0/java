package com.example.demo.mapper;

import com.example.demo.entity.Friend;
import com.example.demo.entity.Userinfo;
import org.apache.catalina.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-02
 * Time: 20:39
 */
@Mapper
public interface UserMapper {

    //把用户插入到数据库总 --> 注册
    int insert(Userinfo userinfo);

    //根据用户名查询用户信息 --> 登录
    Userinfo selectByUsername(@Param("username") String username);

    //将上传的照片路径保存到数据库的对应用户信息中
    void updateUserImgPath(@Param("userId") int userId,@Param("imgPathForDb") String imgPathForDb);

    //将用户id和好友id插入到friend表中,建立好友关系
    void becomeFriends(@Param("userId") int userId, @Param("friendId") int friendId);

    //根据用户名进行模糊查询
    List<Userinfo>  fuzzySearchByUsername(String username);

    //根据 用户id 和 好友id 去查询 是否存在好友关系
    Friend isFriends(int userId, int friendId);

    //将音乐的url路径添加到数据库
    void addMusic(String music);

    //获取音乐列表
    List<String> getAllMusic();

}
