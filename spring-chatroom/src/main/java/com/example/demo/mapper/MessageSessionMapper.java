package com.example.demo.mapper;

import com.example.demo.entity.Friend;
import com.example.demo.entity.MessageSession;
import com.example.demo.entity.MessageSessionUserItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.logging.log4j.message.Message;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-04
 * Time: 0:13
 */
@Mapper
public interface MessageSessionMapper {
    //1. 根据userId 获取到该用户都在哪些会话中存在,返回结果是一组sessionId
    List<Integer> getSessionIdByUserId(int userId);

    //2. 根据sessionId 再来查询这个会话都包含了哪些用户(刨除最初的自己)
    List<Friend> getFriendBySessionId(int sessionId, int selfUserId);

    //3. 新增一个会话记录,返回会话的id
    // 这样的方法返回值int表示的是插入操作影响到几行
    // 此处获取sessionId 是通过 参数messageSession 的 sessionId 属性获取的
    int addMessageSession(MessageSession messageSession);

    //4. 给message_session_user 表也新增对应的记录
    void addMessageSessionUser(MessageSessionUserItem messageSessionUserItem);
}
