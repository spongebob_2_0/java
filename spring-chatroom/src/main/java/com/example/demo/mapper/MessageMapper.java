package com.example.demo.mapper;

import com.example.demo.entity.LoveMessage;
import com.example.demo.entity.Message;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-04
 * Time: 5:34
 */
@Mapper
public interface MessageMapper {

    //通过sessionId获取指定会话最后一条信息
    String getLastMessageBySessionId(int sessionId);

    //获取指定会话历史消息列表
    //有的会话里,历史消息可能特别多
    //此处做一个限制,,默认只取最近的100条消息
    List<Message> getMessagesBySessionId(int sessionId);

    //通过这个方法实现插入消息到数据库表中
    void add(Message message);

    //向表白墙这种表插入数据
    int addLoveMessage(String fromTo,String to,String message,String username);

    //获取用户发送的最后消息的id
    Integer getlastMessageId(String username);

    //根据id和用户名删除指定消息
    int deleteLastMessageId(int messageId,String username);

    //查询表白墙的所有数据
    List<LoveMessage> getLoveMessage();

}
