package com.example.demo.entity;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-12
 * Time: 4:18
 */
@Data
public class LoveMessage {
    int messageId;
    String fromTo;
    String to;
    String message;
    String username;
}
