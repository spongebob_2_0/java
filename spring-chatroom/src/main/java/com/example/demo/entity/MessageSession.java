package com.example.demo.entity;

import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description: 使用这个类表示一个会话
 * User: WuYimin
 * Date: 2023-07-04
 * Time: 0:09
 */
@Data
public class MessageSession {
    private int sessionId;
    private List<Friend> friends;
    private String lastMessage;
}
