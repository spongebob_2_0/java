package com.example.demo.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-02
 * Time: 20:39
 */
@Data
public class Userinfo implements Serializable {
    private int userId;
    private String username = "";
    private String password = "";
    private String photo;
}
