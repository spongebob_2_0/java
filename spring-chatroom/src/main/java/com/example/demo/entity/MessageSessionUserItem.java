package com.example.demo.entity;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description: 使用这个类的对象来表示message_session_user 表里的一个记录
 * 只有在同一个会话里 用户才可以聊天
 * User: WuYimin
 * Date: 2023-07-04
 * Time: 2:47
 */
@Data
public class MessageSessionUserItem {
    private int sessionId;   //表示一个会话
    private int userId;      //表示一个用户
}
