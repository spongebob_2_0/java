package com.example.demo.entity;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description: 使用一个Friend对象表示一个好友
 * User: WuYimin
 * Date: 2023-07-03
 * Time: 20:43
 */
@Data
public class Friend {
    private int friendId;
    private String friendName;

}
