package com.example.demo.entity;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:   存储会话信息的实体类
 * User: WuYimin
 * Date: 2023-07-04
 * Time: 6:22
 */
@Data
public class Message {
    private int messageId;
    private int fromId;  //表示发送者的用户id
    private String fromName;  //表示发送者的用户名
    private int sessionId;
    private String content;
}
