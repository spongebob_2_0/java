package com.example.demo.controller;

import com.example.demo.entity.LoveMessage;
import com.example.demo.entity.Message;
import com.example.demo.entity.Userinfo;
import com.example.demo.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-04
 * Time: 6:47
 */
@RestController
public class MessageController {

    @Autowired
    private MessageService messageService;

    /**
     * 获取历史会话信息
     * @param sessionId
     * @return
     */
    @RequestMapping("/getmessage")
    public Object getMessage(int sessionId) {
        List<Message> messages = messageService.getMessagesBySessionId(sessionId);
        //我们从数据库查出的消息是最近的100条(所以要降序取),但是返回给前端需要升序的消息(因为聊天的信息框显示的消息是从旧到新升序排列的)
        Collections.reverse(messages);
        return  messages;
    }

    //存储表白的信息
    @RequestMapping("/savelovemessage")
    public Object saveLoveMessage(@RequestBody LoveMessage loveMessage, HttpServletRequest request) {
        // 从请求中获取 HttpSession 对象
        HttpSession session = request.getSession(false);

        // 如果 HttpSession 对象为 null，则表示用户未登录，此时返回一个空的 Userinfo 对象
        if(session == null) {
            System.out.println("[getUserinfo] 当前获取不到session对象" );
            return  new Userinfo();
        }

        // 从 HttpSession 中获取 Userinfo 对象
        Userinfo userinfo = (Userinfo) session.getAttribute("userinfo");
        int result = messageService.addLoveMessage(loveMessage.getFromTo(),
                loveMessage.getTo(),loveMessage.getMessage(), userinfo.getUsername());
        if(result > 0) {
            System.out.println("表白信息添加成功! ");
            int messageId = messageService.getlastMessageId(userinfo.getUsername());
            return messageId;
        }
        return -1; //说明数据插入失败
    }

    //删除最后一条表白的消息
    @RequestMapping("/deletelovemessage")
    public Object deleteLoveMessage(HttpServletRequest request) {
        // 从请求中获取 HttpSession 对象
        HttpSession session = request.getSession(false);

        // 如果 HttpSession 对象为 null，则表示用户未登录，此时返回一个空的 Userinfo 对象
        if(session == null) {
            System.out.println("[getUserinfo] 当前获取不到session对象" );
            return  new Userinfo();
        }

        // 从 HttpSession 中获取 Userinfo 对象
        Userinfo userinfo = (Userinfo) session.getAttribute("userinfo");
        Integer messageId = messageService.getlastMessageId(userinfo.getUsername());
        if(messageId == null) {
            return -1;
        }
        int result = messageService.deleteLastMessageId(messageId, userinfo.getUsername());
        return  messageId;
    }

    //显示所有表白墙的信息
    @RequestMapping("/getAllmessage")
    public Object getAllMessage() {
        List<LoveMessage> loveMessages = messageService.getLoveMessage();
        return loveMessages;
    }
}
