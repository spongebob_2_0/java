package com.example.demo.controller;

import com.example.demo.entity.Friend;
import com.example.demo.entity.MessageSession;
import com.example.demo.entity.MessageSessionUserItem;
import com.example.demo.entity.Userinfo;
import com.example.demo.service.MessageService;
import com.example.demo.service.MessageSessionService;
import org.apache.catalina.connector.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-04
 * Time: 0:41
 */
@RestController
@RequestMapping("/message")
public class MessageSessionController {

    @Autowired
    private MessageSessionService messageSessionService;

    @Autowired
    private MessageService messageService;

    /**
     * 获取和当前用户相关的所有消息会话列表的最后一条消息
     * @param request
     * @return
     */
    @RequestMapping("/sessionList")
    public Object getMessageSessionList(HttpServletRequest request) {
        // 创建一个空的 MessageSession 对象列表
        List<MessageSession> messageSessionList = new ArrayList<>();

        // 从请求中获取 HttpSession 对象。如果当前请求没有对应的 session 则返回 null
        HttpSession session = request.getSession(false);
        // 判断 session 是否存在，如果不存在则返回空的 messageSessionList
        if(session == null) {
            // 打印调试信息
            System.out.println("[getMessageSessionList] session == null");
            return  messageSessionList;
        }
        // 从 session 中获取 userinfo 对象
        Userinfo userinfo = (Userinfo) session.getAttribute("userinfo");
        // 判断 userinfo 是否存在，如果不存在则返回空的 messageSessionList
        if(userinfo == null) {
            // 打印调试信息
            System.out.println("[getMessageSessionList] userinfo == null");
            return messageSessionList;
        }

        // 根据用户ID从服务中获取所有会话ID的列表
        List<Integer> sessionIdList = messageSessionService.getSessionIdByUserId(userinfo.getUserId());
        // 遍历每一个会话ID
        for(int sessionId: sessionIdList) {
            // 创建一个新的 MessageSession 对象，并设置它的 sessionId
            MessageSession messageSession = new MessageSession();
            messageSession.setSessionId(sessionId);

            // 获取该会话中所有的好友列表
            List<Friend> friends = messageSessionService.getFriendBySessionId(sessionId, userinfo.getUserId());
            // 将好友列表设置到 messageSession 对象中
            messageSession.setFriends(friends);

            // 获取该会话的最后一条消息
            String lastMessage = messageService.getLastMessageBySessionId(sessionId);
            // 如果最后一条消息为 null，则设置为一个空字符串 (用户可能还没发消息)
            if(lastMessage == null) {
                messageSession.setLastMessage("");
            } else {
                // 否则，将获取到的最后一条消息设置到 messageSession 对象中
                messageSession.setLastMessage(lastMessage);
            }

            // 将设置好的 messageSession 对象添加到 messageSessionList 中
            messageSessionList.add(messageSession);
        }
        // 返回包含所有会话信息的 messageSessionList
        return messageSessionList;
    }


    /**
     * 新增会话记录(同时填充message_session_user表里面记录)
     * @param toUserId
     * @param userinfo
     * @return
     */
    @RequestMapping("/session")
    @Transactional
    public Object addMessageSession(int toUserId, @SessionAttribute("userinfo") Userinfo userinfo) {
        HashMap<String,Integer> resp = new HashMap<>();
        //进行数据库的插入操作
        //1. 先给 message_session 表里面插入记录 ,使用这个参数的目的主要是为了获取到会话的sessionId
        // 换而言之, message_session 里面的friends 和 lastMessage 属性此处都用不上
        MessageSession messageSession = new MessageSession();
        messageSessionService.addMessageSession(messageSession);
        //2. 给 message_session_user 表插入记录
        MessageSessionUserItem item1 = new MessageSessionUserItem();
        item1.setSessionId(messageSession.getSessionId());
        item1.setUserId(userinfo.getUserId());
        messageSessionService.addMessageSessionUser(item1);
        //3. 给 message_session_user 表插入记录
        MessageSessionUserItem item2 = new MessageSessionUserItem();
        item2.setSessionId(messageSession.getSessionId());
        item2.setUserId(toUserId);
        messageSessionService.addMessageSessionUser(item2);
        System.out.println("[addMessageSession] 新增会话成功! sessionId = " + messageSession.getSessionId()
                            + "userId1 = " + userinfo.getUserId() + "usreId2 = " + toUserId);
        resp.put("sessionId",messageSession.getSessionId());
        //返回的map会被转换成json格式的字符串返回
        return resp;
    }


}
