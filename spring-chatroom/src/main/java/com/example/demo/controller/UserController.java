package com.example.demo.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.CircleCaptcha;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.common.auth.CredentialsProvider;
import com.aliyun.oss.common.auth.DefaultCredentialProvider;
import com.example.demo.common.AliOSSUtils;
import com.example.demo.common.MailUtils;
import com.example.demo.common.PasswordTools;
import com.example.demo.entity.Friend;
import com.example.demo.entity.Userinfo;
import com.example.demo.service.UserService;

import com.google.code.kaptcha.Producer;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.jdbc.Null;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.yaml.snakeyaml.constructor.DuplicateKeyException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-02
 * Time: 20:42
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;


    @Autowired  //操作邮件发送的对象
    private MailUtils mailUtils;

    @Autowired  //生成随机验证码的对象
    private Producer checkCode;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private AliOSSUtils aliOSSUtils;


//    @Value("${img.routePath}")
//    private String routePath; //imgPath通过注解获取到了路由的映射
//
//    @Value("${img.path}")
//    private String imgPath; //imgPath通过注解获取到了文件保存的路径


    /**
     * 生成一个验证码图片，并把它发送给客户端
     * @param response
     * @param request
     * @throws IOException
     */
    @GetMapping("code")
    void getCode(HttpServletResponse response, HttpServletRequest request) throws IOException {

        // 使用hutool工具库中的CaptchaUtil类生成一个圆形的验证码图片，该图片的宽度为100像素，高度为40像素，验证码字符的个数为4，干扰线的条数为5
        CircleCaptcha captcha = CaptchaUtil.createCircleCaptcha(100, 40, 4, 5);
        // 从生成的验证码图片中获取验证码字符
        String code = captcha.getCode();
        // 获取HttpSession对象，并将验证码字符保存在session中，便于以后进行验证
        HttpSession session = request.getSession();
        session.setAttribute("code", code);
        // 把验证码图片的二进制数据写入到HttpServletResponse对象的输出流中，这样当客户端发送请求时，服务器会返回一张验证码图片
        captcha.write(response.getOutputStream());
    }


    /**
     * 登录
     * @param username
     * @param password
     * @param request
     * @return
     */
    @RequestMapping("/login")
    public Object login(String username, String password, String captcha, HttpServletRequest request) {

        // 判断用户名、密码和验证码是否为空，如果任一为空则返回-1。
        if (!StringUtils.hasLength(username) || !StringUtils.hasLength(password) || !StringUtils.hasLength(captcha)) {
            return -1;  //参数有误
        }

        // 从会话中获取验证码
        String sessionCaptcha = (String) request.getSession().getAttribute("code");
        // 如果会话中的验证码不存在或者与用户提交的验证码不一致，则返回-2
        if (sessionCaptcha == null || !sessionCaptcha.equalsIgnoreCase(captcha)) {
            return -2;   //验证码错误
        }

        // 从数据库中查询用户名对应的用户信息
        Userinfo userinfo = userService.selectByUsername(username);
        // 如果用户名不存在或者用户ID小于等于0，则返回-2
        if (userinfo == null || userinfo.getUserId() <= 0) {
            return -4;  //用户名或者密码有误
        }
        // 如果密码验证失败（即用户提交的密码和数据库中的密码不匹配），则返回-3
        if (!PasswordTools.decrypt(password, userinfo.getPassword())) {
            return -3;  //用户名或者密码有误
        }

        // 将当前成功登录的用户信息存储到session中，getSession方法如果有就存，如果没有就会创建一个会话
        HttpSession session = request.getSession(true);
        session.setAttribute("userinfo",userinfo);
        // 登录成功，返回1
        return 1;
    }



    /**
     * 发送邮箱验证码
     * @param postbox
     * @return
     */
    @RequestMapping("/getcode")
    public Object contextLoad(@RequestParam String postbox) {
        try {
            // 定义一个邮箱的正则表达式
            String emailRegex = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$";
            // 验证传入的邮箱是否符合正则表达式，即验证邮箱格式是否正确
            boolean isValidEmail = Pattern.matches(emailRegex, postbox);
            // 如果邮箱格式不正确，返回-1
            if (!isValidEmail) {
                return -1;
            }
            // 生成验证码
            String s = checkCode.createText();
            // 获取一个用于操作Redis中的value类型数据的对象
            ValueOperations valueOperations = redisTemplate.opsForValue();
            // 发送邮件到指定邮箱，邮件内容是验证码
            mailUtils.sendMail(postbox,"验证码: " + s,"邮件注册验证");
            // 将生成的验证码存入Redis，并设置其有效期为120秒
            valueOperations.set("regCode" , s, 120, TimeUnit.SECONDS);
            // 如果以上所有步骤都成功执行，返回1
            return 1;
        } catch (Exception e) {
            // 如果在执行以上步骤时出现异常，记录异常日志，并返回-1
            log.error("方法执行出错! ", e);
            return -1;
        }
    }


    /**
     * 注册
     * @param code
     * @param userinfo
     * @return
     */
// 定义一个处理"/reg"路径的请求的方法，接收一个名为"code"的验证码和一个名为"userinfo"的用户信息。
    @RequestMapping("/reg")
    public Object register(String code,Userinfo userinfo) {

        // 验证用户信息是否为空，用户名或密码是否为空，如果任一为空则返回-1。
        if(userinfo == null || !StringUtils.hasLength(userinfo.getUsername())
                || !StringUtils.hasLength(userinfo.getPassword())) {
            return -1;
        }

        // 从Redis中获取存储的验证码
        String value = (String) redisTemplate.opsForValue().get("regCode");
        // 如果Redis中的验证码已过期（即不存在），返回-1
        if(value == null) {
            return -1;  //验证码已过期
        }
        // 如果传入的验证码与Redis中的验证码不匹配，返回-2
        else if(!value.equalsIgnoreCase(code)) {
            return -2;  //验证码不正确
        }

        // 从数据库中查询是否存在用户名相同的用户
        Userinfo user = userService.selectByUsername(userinfo.getUsername());
        // 如果已存在用户名相同的用户，返回-3
        if(user != null) {
            return -3;  //该用户已经注册
        }

        // 对密码进行加盐加密
        userinfo.setPassword(PasswordTools.encrypt(userinfo.getPassword()));
        // 调用userService的插入方法将用户信息插入数据库，并获取操作结果
        int result = userService.insert(userinfo);
        // 如果插入操作成功（result大于0），删除Redis中的验证码
        if(result > 0) {
            //注册成功删除验证码,防止验证码被被滥用
            redisTemplate.delete("regCode");
        }
        // 注册成功，返回1
        return 1;  //注册成功
    }


    /**
     * 获取session中的用户对象
     * @param request
     * @return
     */
    @RequestMapping("/getuserinfo")
    public Object getUserInfo(HttpServletRequest request) {
        // 1.先从请求中拿到会话
        HttpSession session = request.getSession(false);
        if(session == null) {
            //会话不存在,用户尚未登录,此时返回一个空对象即可
            System.out.println("[getUserinfo] 当前获取不到session对象" );
            return new Userinfo();
        }
        // 2.从会话中获取到之前保存的用户对象
        Userinfo userinfo = (Userinfo) session.getAttribute("userinfo");
        if(userinfo == null) {
            System.out.println("[getUserinfo] 当前获取不到usernfo对象" );
            return new Userinfo();
        }
        userinfo.setPassword("");
        return userinfo;
    }

    /**
     * 模糊匹配查找好友
     * @param username
     * @return
     */
    @RequestMapping("/search")
    public Object fuzzySearchBy(@RequestParam String username) {
        if(username == "" || username == null) {
            return -1;
        }
        List<Userinfo> userinfos = userService.fuzzySearchByUsername(username);
        if(userinfos == null) {
            System.out.println("获取好友数据失败 " + userinfos);
            return null;
        }
        for(Userinfo user: userinfos) {
            user.setPassword("");
        }
        System.out.println("获取好友数据成功 " + userinfos);
        return  userinfos;
    }


    /**
     * 将用户上传的图片保存到指定路径中,并将该路径存储到数据库中
     * @param request
     * @param file
     * @return
     */
//// 定义一个处理 "/upload" 请求的方法，该方法用于上传文件
//    @RequestMapping("/upload")
//    public Object upload (HttpServletRequest request, @RequestParam("myfile") MultipartFile file) {
//        // 判断上传的文件是否为空，如果为空则返回null
//        if(file.isEmpty()) {
//            return null;
//        }
//        // 获取上传的文件的原始文件名
//        String filename = file.getOriginalFilename();
//
//        // 从原始文件名中获取文件的后缀名
//        filename.substring(filename.lastIndexOf("."));
//
//        // 生成一个唯一的文件名，这是通过在原始文件名前面添加一个UUID实现的
//        filename = UUID.randomUUID().toString() + filename;
//
//        // 创建一个File对象，该对象描述了上传文件应保存到的位置
//        File destinationFile = new File(imgPath, filename);
//
//        // 如果目标文件所在的目录不存在，则创建该目录
//        if(!destinationFile.exists()) {
//            destinationFile.mkdirs();
//        }
//        try {
//            // 使用 MultipartFile 对象的 transferTo 方法将上传的文件保存到指定的位置
//            file.transferTo(destinationFile);
//
//            // 创建一个字符串，该字符串描述了图片在数据库中的路径
//            String imgPathForDb = routePath + filename;
//
//            // 从请求中获取 HttpSession 对象
//            HttpSession session = request.getSession(false);
//
//            // 如果 HttpSession 对象为 null，则表示用户未登录，此时返回一个空的 Userinfo 对象
//            if(session == null) {
//                System.out.println("[getUserinfo] 当前获取不到session对象" );
//                return  new Userinfo();
//            }
//
//            // 从 HttpSession 中获取 Userinfo 对象
//            Userinfo userinfo = (Userinfo) session.getAttribute("userinfo");
//
//            // 更新用户的图片路径
//            userService.updateUserImgPath(userinfo.getUserId(), imgPathForDb);
//
//            String username = "photo:" + userinfo.getUsername();
//            Userinfo user = userService.selectByUsername(userinfo.getUsername());
//            user.setPassword("");
//            redisTemplate.opsForValue().set(username,user);
//            // 返回图片在数据库中的路径
//            return imgPathForDb;
//        } catch (IOException e) {
//            // 如果在上面的操作中出现任何 IO 错误，则记录错误信息并返回 null
//            log.error("图片上传失败! " + e.getMessage());
//        }
//        return null;
//    }


    // 定义一个处理 "/upload" 请求的方法，该方法用于上传文件
    @RequestMapping("/upload")
    public Object upload (HttpServletRequest request, @RequestParam("myfile") MultipartFile file) throws IOException {
        // 判断上传的文件是否为空，如果为空则返回null
        if(file.isEmpty()) {
            return null;
        }
        //使用阿里云oss将文件上传到阿里云存储桶(同时返回ur地址)
        String fileUrl = aliOSSUtils.upload(file);

        if(fileUrl == null) {
            return null;
        }
        // 从请求中获取 HttpSession 对象
        HttpSession session = request.getSession(false);
        // 如果 HttpSession 对象为 null，则表示用户未登录，此时返回一个空的 Userinfo 对象
        if(session == null) {
            System.out.println("[getUserinfo] 当前获取不到session对象" );
            return  new Userinfo();
        }

        // 从 HttpSession 中获取 Userinfo 对象
        Userinfo userinfo = (Userinfo) session.getAttribute("userinfo");

        // 更新用户的图片路径
        userService.updateUserImgPath(userinfo.getUserId(), fileUrl);

        String username = "photo:" + userinfo.getUsername();
        Userinfo user = userService.selectByUsername(userinfo.getUsername());
        user.setPassword("");
        redisTemplate.opsForValue().set(username,user);
//         返回图片在数据库中的路径
        return fileUrl;
    }




    /**
     * 通过session中用户名去重新获取用户信息
     * 每次上传头像后,用户信息都会改变,刷新页面的时候需要重新去获取用户头像的url地址
     * @param request
     * @return
     */
    @RequestMapping("/getuser")
    public Object getUser(HttpServletRequest request) {
        // 1.先从请求中拿到会话
        HttpSession session = request.getSession(false);
        if(session == null) {
            //会话不存在,用户尚未登录,此时返回一个空对象即可
            System.out.println("[getUserinfo] 当前获取不到session对象" );
            return new Userinfo();
        }
        // 2.从会话中获取到之前保存的用户对象
        Userinfo userinfo = (Userinfo) session.getAttribute("userinfo");
        if(userinfo == null) {
            System.out.println("[getUserinfo] 当前获取不到usernfo对象" );
            return new Userinfo();
        }
        String username = "photo:" + userinfo.getUsername();
        Userinfo user1 = (Userinfo) redisTemplate.opsForValue().get(username);
        if(user1 != null) {
            return user1;
        }
        Userinfo user2 = userService.selectByUsername(userinfo.getUsername());
        user2.setPassword("");
        redisTemplate.opsForValue().set(username,user2);
        return user2;
    }

    /**
     * 通过id建立好友关系
     * @param request
     * @param friendName
     * @return
     */
    @RequestMapping("/becomefriends")
    public Object becomeFriends(HttpServletRequest request, @RequestParam("friendName") String friendName) {

        //根据名字获取好友的信息
        Userinfo friendInfo = userService.selectByUsername(friendName);
        // 1.先从请求中拿到会话
        HttpSession session = request.getSession(false);
        if(session == null) {
            //会话不存在,用户尚未登录,此时返回一个空对象即可
            System.out.println("[getUserinfo] 当前获取不到session对象" );
            return -1;
        }
        // 2.从会话中获取到之前保存的用户对象
        Userinfo userinfo = (Userinfo) session.getAttribute("userinfo");
        if(friendInfo.getUsername().equals(userinfo.getUsername())) {
            return -2;   //自己不能添加自己为好友
        }
        Friend friends = userService.isFriends(userinfo.getUserId(), friendInfo.getUserId());
        if(friends != null) {
            return -3;   //已经是好友关系
        }

        //通过用户id和好友id建立还有可以好友关系
        userService.becomeFriends(userinfo.getUserId(),friendInfo.getUserId());
        return  1;
    }

    /**
     * 注销
     * @param request
     * @return
     */
    @RequestMapping("/logout")
    public Object logout(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        session.removeAttribute("userinfo");
        return 1;  //表示注销成功
    }


//    @RequestMapping("/uploadMusic")
//    public Object uploadMusic(HttpServletRequest request, @RequestParam("musicFile") MultipartFile file) {
//        // 判断上传的文件是否为空，如果为空则返回null
//        if(file.isEmpty()) {
//            return null;
//        }
//
//        // 获取上传的文件的原始文件名
//        String filename = file.getOriginalFilename();
//
//        // 从原始文件名中获取文件的后缀名
//        filename.substring(filename.lastIndexOf("."));
//
//        // 生成一个唯一的文件名，这是通过在原始文件名前面添加一个UUID实现的
//        filename = UUID.randomUUID().toString() + filename;
//
//        // 创建一个File对象，该对象描述了上传文件应保存到的位置
//        File destinationFile = new File(imgPath, filename);
//
//        // 如果目标文件所在的目录不存在，则创建该目录
//        if(!destinationFile.getParentFile().exists()) {
//            destinationFile.getParentFile().mkdirs();
//        }
//
//        try {
//            // 使用 MultipartFile 对象的 transferTo 方法将上传的文件保存到指定的位置
//            file.transferTo(destinationFile);
//
//            // 创建一个字符串，该字符串描述了音乐在数据库中的路径
//            String musicPathForDb = routePath + filename;
//
//            // 将音乐路径添加到数据库
//            userService.addMusic(musicPathForDb);
//            // 返回音乐在数据库中的路径
//            return 1;
//        } catch (IOException e) {
//            // 如果在上面的操作中出现任何 IO 错误，则记录错误信息并返回 null
//            log.error("音乐上传失败! " + e.getMessage());
//        }
//        return -1;
//    }


    @RequestMapping("/uploadMusic")
    public Object uploadMusic(HttpServletRequest request, @RequestParam("musicFile") MultipartFile file) throws IOException {
        // 判断上传的文件是否为空，如果为空则返回null
        if(file.isEmpty()) {
            return null;
        }
        //这里是外网访问的域名(根据你选择的阿里云地域节点去确定)
        String endpoint = "https://oss-cn-beijing.aliyuncs.com";
        // RAM用户的访问密钥（AccessKey ID和AccessKey Secret）。
        String accessKeyId = "LTAI5t78iL22k1A3pc3FqLZX";
        String accessKeySecret = "Jb1RXOViCtOxdbRCNVEavdS7yF6IQE";
        String bucketName = "web-183";
        // 使用代码嵌入的RAM用户的访问密钥配置访问凭证。
        CredentialsProvider credentialsProvider = new DefaultCredentialProvider(accessKeyId, accessKeySecret);
        // 获取上传的文件的输入流
        InputStream inputStream = file.getInputStream();


        //上传文件到 OSS
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        ossClient.putObject(bucketName, file.getOriginalFilename(), inputStream);

        //文件访问路径 (这里拼接的就是对象存储桶+域名+存储桶中文件保存的位置)
        String url = endpoint.split("//")[0] + "//" + bucketName + "." + endpoint.split("//")[1] + "/" + file.getOriginalFilename();
        // 关闭ossClient
        ossClient.shutdown();
        userService.addMusic(url);
        if(url == null) {
            return  -1;
        }
        return 1;
    }


    @RequestMapping("/musicList")
    public Object getMusicList() {
        return userService.getAllMusic();
    }

}
