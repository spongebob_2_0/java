package com.example.demo.controller;

import com.example.demo.entity.Friend;
import com.example.demo.entity.Userinfo;
import com.example.demo.service.FriendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-03
 * Time: 21:12
 */
@RestController
@RequestMapping("/friend")
public class FriendController {
    @Autowired
    private FriendService friendService;

    /**
     * 获取好友列表
     * @param request
     * @return
     */
    @RequestMapping("/getfriendlist")
    public List<Friend> getFriendList(HttpServletRequest request) {
        //1.先从会话中, 获得userId
        HttpSession session = request.getSession(false);
        if(session == null) {
            //当前会话不存在,未登录,直接返回一个空的列表即可
            System.out.println("[getFriendList] session 不存在");
            return new ArrayList<Friend>();
        }
        Userinfo userinfo = (Userinfo) session.getAttribute("userinfo");
        if(userinfo == null) {
            //当前用户对象没在会话中存在
            System.out.println("[getFriendList] userinfo 不存在");
            return new ArrayList<Friend>();
        }
        //2.根据userId从数据库查询结果即可
        List<Friend> friendList = friendService.selectFriendList(userinfo.getUserId());
        return  friendList;
    }

}
