package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-21
 * Time: 15:47
 */
// @Configuration 注解表示这是一个配置类
//@Configuration
//public class MyConfig implements WebMvcConfigurer {
//
//    @Value("${img.routePath}")
//    private String routePath; //imgPath通过注解获取到了路由的映射
//    @Value("${img.path}")
//    private String imgPath; //imgPath通过注解获取到了文件保存的路径
//
//
//    /**
//     * 这个就是springboot中springMVC让程序开发者去配置文件上传的额外静态资源服务的配置
//     * 这段代码的作用是将存储在指定文件路径（`imgPath`）下的静态资源映射为以`routePath`开头的URL路径。
//     * 当用户通过该URL访问时，Spring MVC会自动查找并返回对应的静态资源文件。
//     * @param registry
//     */
//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler(routePath+"**").addResourceLocations("file:" + imgPath);
//
//    }
//}
