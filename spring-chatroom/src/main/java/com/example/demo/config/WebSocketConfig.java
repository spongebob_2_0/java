package com.example.demo.config;

import com.example.demo.common.TestWebSocketUtils;
import com.example.demo.common.WebSocketUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

/**
 * Created with IntelliJ IDEA.
 * Description:  注册拦截器,目的是为了将 HttpSession 中的存的信息复制到 WebSocketSession 中一份
 * User: WuYimin
 * Date: 2023-07-04
 * Time: 19:32
 */
//    @Autowired
//    private TestWebSocketUtils testWebSocketUtils;


@Configuration    //注册到spring容器中
@EnableWebSocket  //启动WebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Autowired
    private WebSocketUtils webSocketUtils;

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        //registry.addHandler(webSocketUtils, "/websocketmessage")
        //将webSocketUtils注册到指定的路径，所以当有客户端连接到这个路径时，请求会被转发到webSocketUtils来处理。
        registry.addHandler(webSocketUtils,"/websocketmessage")
                //添加了一个握手拦截器，用于在WebSocket握手时拦截和修改请求和响应，
                // 这里是将HttpSession中的属性(用户信息)复制到WebSocketSession中。
                .addInterceptors(new HttpSessionHandshakeInterceptor());   //注册拦截器
    }
}
