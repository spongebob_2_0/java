//获取当前目录 例： /项目名/index/user
let pathName = window.document.location.pathname;
//获取项目名称
let path = pathName.substring(1,pathName.substring(1).indexOf("/")+1);
//live2d角色
let role = localStorage.getItem("role");
console.log("====="+role);
//角色个数 需要自己填
let role_num = 8;
//歌曲索引
let bgm_num = sessionStorage.getItem("bgm_num");
//进行一些初始化
init();
function init() {
    //给role赋予当前角色
    if (!role){//为null 则赋初始值
        role = "live2d_0"//使用初始默认角色
    }
    //将当前角色保存到本地
    localStorage.setItem("role",role);
//给bgm赋予当前歌曲编号
    if (!bgm_num){
        bgm_num = 0;
    }
    sessionStorage.setItem("bgm_num",bgm_num);
}
// //获取站主和用户昵称
// function getAdminAndUser(vue){
//     axios.get(
//         "/"+path+"/getAdminAndUser"
//     ).then(response=>{
//         vue.admin = response.data[0];
//         vue.user = response.data[1];
//         //非当前函数业务但需依赖于此函数的额外功能1
//         try {
//             //在这里调用文章页面判断当前文章该用户是否点赞（因为需要保证已经获取到用户名），用try来使得别的页面不至于报错(因为别的页面无该方法)。
//             vue.ifAppreciate();
//             vue.ifCollect();
//         }catch (e) {
//         }
//         try {
//             //用于收藏页面获取初始化的文章。 原因同上
//             vue.getCollectWrites(1);
//         }catch (e) {
//         }
//         try {
//             //个人中心获得信息
//             vue.getMessage();
//             vue.getEmail();
//         }catch (e) {

//         }
//     });
//     //非当前函数业务但需依赖于此函数的额外功能2，因为live2d_bgm 和 role都是具体页面的元素
//     //给id=bgm_num对象赋予当前歌曲编号
//     $("#live2d_bgm").attr("data-bgm",bgm_num);

// }
// //获取用户头像
// function getUserHeadImg() {
//     axios.post(
//         "/"+path+"/getUserHeadImg"
//     ).then(response =>{
//         $(".headImg").attr("src","/"+path+"/static/img/fore/headImg/"+response.data);
//     });
// }
// //搜索博客
// function search() {
//     let search = $(".searchbox").val();
//     if (search.trim().length == 0){
//         layer.msg("不能为空哦！",{icon:7,time:1000});
//         return;
//     }
//     document.location.href = "/"+path+"/fore/weblog?word="+search;
// }
// //鼠标悬浮用户名显示功能栏
// function hoveName() {
//     $(".uNamefunction").removeClass("hidden");
// }
// //鼠标悬浮用户名隐藏功能栏
// function leaveHoveName() {
//     $(".uNamefunction").addClass("hidden");
// }

