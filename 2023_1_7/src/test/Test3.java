package test;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-01-08
 * Time: 23:08
 */
public class Test3 {
    public static void func(int[] nums, int k){
        int len =nums.length;
        reverse(nums,0,len-1);
        reverse(nums,0,k-1);
        reverse(nums,k,len-1);
    }
    public static void reverse(int[] nums,int start,int end){
        while (start<end){
            int tmp =nums[start];
            nums[start] =nums[end];
            nums[end] =tmp;
            start++;
            end--;
        }
    }
    public static void main(String[] args) {
        int[] nums ={1,2,3,4,5,6,7};
        Scanner scanner =new Scanner(System.in);
        int k = scanner.nextInt();
        func(nums,k);
        System.out.println(Arrays.toString(nums));
    }
}
