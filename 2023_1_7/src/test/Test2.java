package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-01-07
 * Time: 23:49
 */
public class Test2 {
    public static String func(String s){
        StringBuilder ret =new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char ch =s.charAt(i);
            if(ch >=65 && ch<=90){
                ch +=32;
            }
            ret.append(ch);
        }
        return ret.toString();
    }
    public static void main(String[] args) {
        String s ="AASFS";
        System.out.println(func(s));
    }
}
