import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-18
 * Time: 20:10
 */
public class Test6 {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        while(scanner.hasNext()) {
            int m = scanner.nextInt();
            int n = scanner.nextInt();
            int max = m > n ? n : m;
            while (true) {
                if (m % max == 0 && n % max == 0) {
                    System.out.println("最大公约数是:" + max);
                    break;
                }
                max--;
            }
        }
    }
}
