/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-18
 * Time: 18:11
 */
public class Test3 {
    public static void main(String[] args) {
        int i =1;
        while(i>0) {
            if(i%3==0 && i%5==0){
                System.out.print(i+" ");
            }
            i++;
            if(i==100){
                break;
            }
        }
    }
}
