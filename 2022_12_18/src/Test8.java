import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-18
 * Time: 20:38
 */
public class Test8 {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int n = scanner.nextInt();
        int count =0;
        while (n !=0){
            if((n&1) != 0){
                count++;
            }
            n =n>>>1;
        }
        /*while (n !=0){
            n =n&(n-1);
             count++;
        }*/
        System.out.println("count="+count);
    }
}
