import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-18
 * Time: 15:39
 */
public class Test1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            int n = scanner.nextInt();
            if (n % 2 == 0) {
                System.out.println(n + "是偶数!");
            } else {
                System.out.println(n + "是奇数!");
            }
        }
    }
}
