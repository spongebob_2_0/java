/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-18
 * Time: 19:26
 */
public class Test5 {
    public static void main(String[] args) {
        for (int i = 1; i <=100 ; i++) {
            int flag =1;
            for (int j = 2; j < i; j++) {
                if (i % j == 0) {
                    flag =0;  //不是素数
                }
            }
            if(flag ==1){    //是素数
                System.out.print(i+" ");
            }
        }
    }
}
