import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-18
 * Time: 20:42
 */
public class Test9 {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        System.out.print("请输入一个数:");
        int n = scanner.nextInt();
        int tmp =0;
        System.out.print("奇数位是:");
        for (int i = 31; i >=0; i=i-2) {
            tmp =(n>>>i)&1;
            System.out.print(tmp);
        }
        System.out.println();
        System.out.print("偶数位是:");
        for (int i = 30; i >=0; i=i-2) {
            tmp =(n>>>i)&1;
            System.out.print(tmp);
        }
    }
}
