import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-18
 * Time: 20:17
 */
public class Test7 {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 0; i <= n; i++) {
            int sum =0;
            int a =i;
            int count =0;
            while(a !=0){
                a /=10;
                count++;
            }
            a =i;
            while(a !=0){
                sum +=Math.pow(a%10,count);
                a /=10;
            }
            if(i == sum){
                System.out.print(sum+" ");
            }
        }
    }
}
