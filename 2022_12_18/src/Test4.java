import java.util.Random;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-18
 * Time: 18:41
 */
public class Test4 {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        Random random =new Random();
        int k = random.nextInt(100);
        while(true) {
            System.out.print("请输入你要猜的数字:");
            int n = scanner.nextInt();
            if (n > k) {
                System.out.println("猜大了!");
            } else if (n < k) {
                System.out.println("猜小了!");
            } else {
                System.out.println("恭喜你,猜对了");
                break;
            }
        }
    }
}
