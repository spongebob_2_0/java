import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-18
 * Time: 16:38
 */
public class Test2 {
    public static void func(){
        System.out.println("666");
    }
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        /*while(scanner.hasNext()) {
            int n = scanner.nextInt();
            int sum =1;
            int i =1;
            while (i<=n) {
                sum *= i;
                i++;
            }
            System.out.println(sum);
        }*/

        /*while(scanner.hasNext()) {
            int n = scanner.nextInt();
            int sum =1;
            while (n > 0) {
                sum *= n;
                n--;
            }
            func();
            System.out.println(sum);
        }*/
        /*while (scanner.hasNext()){
            int n = scanner.nextInt();
            int sum =1;
            for (int i = 1; i <= n; i++) {
                sum *=i;
            }
            System.out.println(sum);
        }*/

        /*int n = scanner.nextInt();
        int sum =0;
        int ret =1;
        for (int i = 1; i <= n; i++) {
            ret *=i;
            sum +=ret;
        }
        System.out.println(sum);*/

        while (scanner.hasNext()) {
            int n = scanner.nextInt();
            int sum = 0;
            for (int i = 1; i <= n; i++) {
                int ret = func1(i);
                sum += ret;
            }
            System.out.println(sum);
        }
    }
    public static int func1(int n) {
        int sum = 1;
        for (int i = 1; i <= n; i++) {
            sum *= i;
        }
        return  sum;
    }
}
