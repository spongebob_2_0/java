package com.example.demo;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.demo.entity.Userinfo;
import com.example.demo.mapper.UserinfoMapper;
import com.example.demo.service.UserinfoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.transform.Source;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SpringBootTest
class DemoApplicationTests {

	@Autowired
	private UserinfoService userinfoService;
//	@Autowired
//	UserinfoMapper userinfoMapper;

	@Test
	void contextLoads() {
	}

	@Test
	@Transactional
	void testSelect() {
		Userinfo userinfo = userinfoService.getById(1);
		System.out.println(userinfo);
		long count = userinfoService.count();
		System.out.println(count);
	}

	@Test
	void testInsert() {
		List<Userinfo> lists = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			Userinfo user = new Userinfo();
			user.setUsername("小雪"+i);
			user.setPassword("123");
			lists.add(user);
		}
		boolean b = userinfoService.saveBatch(lists);
		System.out.println(b);

	}
}
