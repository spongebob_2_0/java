package com.example.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.entity.MessageSessionUser;
import com.example.demo.service.MessageSessionUserService;
import com.example.demo.mapper.MessageSessionUserMapper;
import org.springframework.stereotype.Service;

/**
* @author WuYimin
* @description 针对表【message_session_user】的数据库操作Service实现
* @createDate 2023-08-22 03:44:41
*/
@Service
public class MessageSessionUserServiceImpl extends ServiceImpl<MessageSessionUserMapper, MessageSessionUser>
    implements MessageSessionUserService{

}




