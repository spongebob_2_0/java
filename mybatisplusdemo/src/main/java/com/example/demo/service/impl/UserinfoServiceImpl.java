package com.example.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.entity.Userinfo;
import com.example.demo.service.UserinfoService;
import com.example.demo.mapper.UserinfoMapper;
import org.springframework.stereotype.Service;

/**
* @author WuYimin
* @description 针对表【userinfo】的数据库操作Service实现
* @createDate 2023-08-22 03:44:41
*/
@Service
public class UserinfoServiceImpl extends ServiceImpl<UserinfoMapper, Userinfo>
    implements UserinfoService{

}




