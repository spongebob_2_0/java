package com.example.demo.service;

import com.example.demo.entity.Friend;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author WuYimin
* @description 针对表【friend】的数据库操作Service
* @createDate 2023-08-22 03:44:41
*/
public interface FriendService extends IService<Friend> {

}
