package com.example.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.entity.LoveMessage;
import com.example.demo.service.LoveMessageService;
import com.example.demo.mapper.LoveMessageMapper;
import org.springframework.stereotype.Service;

/**
* @author WuYimin
* @description 针对表【love_message】的数据库操作Service实现
* @createDate 2023-08-22 03:44:41
*/
@Service
public class LoveMessageServiceImpl extends ServiceImpl<LoveMessageMapper, LoveMessage>
    implements LoveMessageService{

}




