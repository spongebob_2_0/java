package com.example.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.entity.Message;
import com.example.demo.service.MessageService;
import com.example.demo.mapper.MessageMapper;
import org.springframework.stereotype.Service;

/**
* @author WuYimin
* @description 针对表【message】的数据库操作Service实现
* @createDate 2023-08-22 03:44:41
*/
@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message>
    implements MessageService{

}




