package com.example.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.entity.Friend;
import com.example.demo.service.FriendService;
import com.example.demo.mapper.FriendMapper;
import org.springframework.stereotype.Service;

/**
* @author WuYimin
* @description 针对表【friend】的数据库操作Service实现
* @createDate 2023-08-22 03:44:41
*/
@Service
public class FriendServiceImpl extends ServiceImpl<FriendMapper, Friend>
    implements FriendService{

}




