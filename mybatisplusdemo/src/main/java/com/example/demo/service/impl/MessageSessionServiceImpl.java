package com.example.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.entity.MessageSession;
import com.example.demo.service.MessageSessionService;
import com.example.demo.mapper.MessageSessionMapper;
import org.springframework.stereotype.Service;

/**
* @author WuYimin
* @description 针对表【message_session】的数据库操作Service实现
* @createDate 2023-08-22 03:44:41
*/
@Service
public class MessageSessionServiceImpl extends ServiceImpl<MessageSessionMapper, MessageSession>
    implements MessageSessionService{

}




