package com.example.demo.mapper;

import com.example.demo.entity.Resource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author WuYimin
* @description 针对表【resource】的数据库操作Mapper
* @createDate 2023-08-22 03:44:41
* @Entity com.example.demo.entity.Resource
*/
public interface ResourceMapper extends BaseMapper<Resource> {

}




