package com.example.demo.mapper;
import org.apache.ibatis.annotations.Param;

import com.example.demo.entity.Userinfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author WuYimin
* @description 针对表【userinfo】的数据库操作Mapper
* @createDate 2023-08-22 03:44:41
* @Entity com.example.demo.entity.Userinfo
*/
public interface UserinfoMapper extends BaseMapper<Userinfo> {

    int insertSelective(Userinfo userinfo);

    int deleteByUserid(@Param("userid") Integer userid);
}




