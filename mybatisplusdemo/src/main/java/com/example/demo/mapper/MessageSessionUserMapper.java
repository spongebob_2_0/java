package com.example.demo.mapper;

import com.example.demo.entity.MessageSessionUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author WuYimin
* @description 针对表【message_session_user】的数据库操作Mapper
* @createDate 2023-08-22 03:44:41
* @Entity com.example.demo.entity.MessageSessionUser
*/
public interface MessageSessionUserMapper extends BaseMapper<MessageSessionUser> {

}




