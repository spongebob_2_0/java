package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName message
 */
@TableName(value ="message")
@Data
public class Message implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer messageid;

    /**
     * 
     */
    private Integer fromid;

    /**
     * 
     */
    private Integer sessionid;

    /**
     * 
     */
    private String content;

    /**
     * 
     */
    private Date posttime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}