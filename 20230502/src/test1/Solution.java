package test1;

import java.util.*;

class Solution {

    public static List<List<Integer>> threeSum(int[] nums) {
        // 对输入数组进行排序
        Arrays.sort(nums);
        

        // 创建一个 ArrayList 用于存储满足条件的三元组
        List<List<Integer>> result = new ArrayList<>();

        // 遍历数组，略过最后两个元素，因为我们需要至少三个元素来形成一个三元组
        for (int i = 0; i < nums.length - 2; i++) {
            // 如果当前元素大于0，不可能再找到和为0的三元组
            if (nums[i] > 0) {
                break;
            }

            // 跳过重复元素，以避免重复的三元组
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }

            // 初始化双指针，一个指针从当前元素的下一个开始，另一个指针从数组的最后一个元素开始
            int left = i + 1;
            int right = nums.length - 1;

            // 当左指针小于右指针时，继续寻找满足条件的三元组
            while (left < right) {
                int sum = nums[i] + nums[left] + nums[right];

                // 根据三数之和与0的关系，移动左指针或右指针
                if (sum == 0) {
                    // 找到一个满足条件的三元组，添加到结果列表中
                    result.add(Arrays.asList(nums[i], nums[left], nums[right]));

                    // 跳过重复元素，以避免重复的三元组
                    while (left < right && nums[left] == nums[left + 1]) {
                        left++;
                    }
                    while (left < right && nums[right] == nums[right - 1]) {
                        right--;
                    }

                    // 找到一个满足条件的三元组后，同时移动左右指针继续寻找下一个三元组
                    left++;
                    right--;
                } else if (sum < 0) {
                    // 如果和小于0，需要增加和的值，因此将左指针向右移动
                    left++;
                } else {
                    // 如果和大于0，需要减小和的值，因此将右指针向左移动
                    right--;
                }
            }
        }

        // 返回包含满足条件的三元组的结果列表
        return result;
    }



    public static void main(String[] args) {
        int[] nums ={-1,0,1,2,-1,-4};
        List<List<Integer>> list = threeSum(nums);
        System.out.println(list);
    }
}