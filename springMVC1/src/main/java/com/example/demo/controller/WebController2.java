package com.example.demo.controller;

import com.example.demo.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

@Slf4j
@Controller  // 加载并注册类
@ResponseBody // 当前类返回的是非静态页面
@RequestMapping("/web") // 使用 "/web" 可以访问到当前类 (可省略)
public class WebController2 {
    //获取配置文件的保存路径
    @Value("${img.path}")
    private String imgpath;  //imgpath通过注解获取到了文件保存的路径
    //上传文件
    @RequestMapping("/upload") // 使用 "/web" + "/hello" 可以访问到当前方法
    public boolean upload(String name, @RequestPart("myfile")MultipartFile file) throws IOException {
        boolean result = false;
        //得到原图片的名称
        String filename = file.getOriginalFilename();
        //得到图片后缀（png)
        filename = filename.substring(filename.lastIndexOf("."));
        //生成不重名的文件名
        filename = UUID.randomUUID().toString() + filename;
        try {
            //将图片保存
            file.transferTo(new File( imgpath+ filename));
            result = true;
        } catch (IOException e) {
            log.error("图片上传失败!" + e.getMessage());
        }
        return result;
    }
}