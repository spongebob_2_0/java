package com.example.demo.controller;

import com.example.demo.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

@Controller  // 加载并注册类
@ResponseBody // 当前类返回的是非静态页面
@RequestMapping("/web") // 使用 "/web" 可以访问到当前类 (可省略)
public class WebController {
    @RequestMapping("/index") // 使用 "/web" + "/hello" 可以访问到当前方法
    public HashMap<String,Object> getForm() {
        HashMap<String,Object> map =new HashMap<>();
        map.put("dog","狗");
        map.put("cat","猫");
        map.put("big",1);
        return map;
    }
}