package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller  // 加载并注册类
@ResponseBody // 当前类返回的是非静态页面
@RequestMapping("/web") // 使用 "/web" 可以访问到当前类 (可省略)
public class WebController5 {
    @RequestMapping("/get_head") // 使用 "/web" + "/hello" 可以访问到当前方法
    public String getHead(@RequestHeader("User-Agent") String userAgent) {
        return "User-Agent " + userAgent;
    }

}