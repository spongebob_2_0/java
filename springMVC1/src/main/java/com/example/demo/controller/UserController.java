package com.example.demo.controller;

import com.example.demo.entity.UserInfo;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Console;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-11
 * Time: 12:15
 */

//@Controller
//@ResponseBody
@RestController
public class UserController {

    /**
     * 获取对象
     * @param userInfo
     * @return
     */
    @RequestMapping("/reg1")
    public Object reg(UserInfo userInfo) {
        System.out.println(userInfo);
        return userInfo;
    }
    @RequestMapping("/reg2")
    public Object reg1(@RequestParam("username") String name, String password) {
        return "name = " + name + "password = " + password;
    }

    @RequestMapping(value = "say1")  //可以是一级路由,也可以是n级路由
    public String sayHi1(String name) { //不传递那么的参数,那么使用的是String的默认值null
        return "hi " + name ;
    }
    @RequestMapping(value = "say2")  //可以是一级路由,也可以是n级路由
    public String sayHi2(Integer id) {
        return "hi " + id ;
    }
    @RequestMapping(value = "say3")  //可以是一级路由,也可以是n级路由
    public String sayHi3(int id) {  //参数传递不要使用基本数据类型 -> 因为基本数据类型会报错
        return "hi " + id ;
    }
    @RequestMapping(value = "say4")  //可以是一级路由,也可以是n级路由
    public String sayHi4(String name, String password) throws IOException {  //参数传递不要使用基本数据类型 -> 因为基本数据类型会报错
//        response.sendRedirect("https://www.baidu.com");
        return "hi " + "name =" +name +" password = " + password;
    }
    @PostMapping("/hello2")
    public String sayHello() {
        return "hello spring mvc";
    }
    @GetMapping("hi")
    public String  hi() {
        return "你好 世界";
    }
}
