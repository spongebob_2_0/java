package com.example.demo.controller;

import org.apache.catalina.Session;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller  // 加载并注册类
@ResponseBody // 当前类返回的是非静态页面
@RequestMapping("/web") // 使用 "/web" 可以访问到当前类 (可省略)
public class WebController6 {
    @RequestMapping("/setsess") // 使用 "/web" + "/hello" 可以访问到当前方法
    public String setSession(HttpServletRequest request) {
        HttpSession session =request.getSession();
        if(session != null) {
            session.setAttribute("username","lisi");
        }
        return "session 存储成功";
    }
    @RequestMapping("/getsess")
    public String getSession(@SessionAttribute("username") String name) {
        return "name: " + name;
    }
}