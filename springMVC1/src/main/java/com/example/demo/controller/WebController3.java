package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;

@Controller  // 加载并注册类
@ResponseBody // 当前类返回的是非静态页面
@RequestMapping("/web") // 使用 "/web" 可以访问到当前类 (可省略)
public class WebController3 {
    @RequestMapping("/hello/{username}/{age}") // 使用 "/web" + "/hello" 可以访问到当前方法
    public String getURLParam(@PathVariable("username") String name,
                              @PathVariable Integer age) {
        return "name: " + name +" | age: " +age;
    }

}