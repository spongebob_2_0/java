package com.example.demo.entity;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-11
 * Time: 19:55
 */
@Data
public class User {
    private int id;
    private String name;
    private int age;
    private String sex;
}
