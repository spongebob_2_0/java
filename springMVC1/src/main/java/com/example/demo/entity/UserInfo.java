package com.example.demo.entity;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-11
 * Time: 15:30
 */
@Data
public class UserInfo {
    private int id;
    private String name;
    private String password;
    private int age;
}
