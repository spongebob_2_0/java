package com.example.demo.common;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-18
 * Time: 9:02
 */
@Aspect //标识当前类为一个切面
@Component //不能省略
public class UserAop {
    //定义切点，设置拦截规则
    @Pointcut("execution(* com.example.demo.controller.UserController.*(..))")
    public void pointcut(){}

    // 前置通知
    @Before("pointcut()")
    public void doBefore() {
        System.out.println("执行了前置通知 " + LocalDateTime.now());
    }

    // 后置通知
    @After("pointcut()")
    public void doAfter() {
        System.out.println("执行了后置通知 " + LocalDateTime.now());
    }

    // return 之前通知
    @AfterReturning("pointcut()")
    public void doAfterReturning() {
        System.out.println("执行了返回之后通知 " + LocalDateTime.now());
    }

    // 抛出异常之前通知
    @AfterThrowing("pointcut()")
    public void doAfterThrowing() {
        System.out.println("执行了返回之后通知 " + LocalDateTime.now());
    }

    // 环绕通知
    @Around("pointcut()")
    public Object doAround(ProceedingJoinPoint joinPoint) {  // 拿到目标方法的执行对象
        // 这个对象是框架能否继续执行后续流程的对象, 与目标方法是否返回值, 以及返回类型无关
        Object res = null;
        // 前置业务代码
        System.out.println("执行了环绕通知的前置方法 " + LocalDateTime.now());
        try {
            // 执行目标方法
            res = joinPoint.proceed();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        // 后置业务代码
        System.out.println("执行了环绕通知的后置方法 " + LocalDateTime.now());
        return res;
    }
}
