package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-18
 * Time: 9:49
 */
@RestController
public class ArticleController {

    @RequestMapping("/art/sayHi")
    public String sayHi() {
        System.out.println("执行了 Article 方法");
        return "Article: hi, spring boot aop.";
    }

}
