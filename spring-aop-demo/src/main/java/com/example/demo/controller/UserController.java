package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-18
 * Time: 9:24
 */
//创建连接点
@RestController
public class UserController {

    @RequestMapping("/sayHi")
    public String sayHi() {
        System.out.println("执行了 sayHi 方法");
        return "hi, spring boot aop.";
    }
}
