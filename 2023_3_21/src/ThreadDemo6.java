/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-21
 * Time: 18:19
 */
public class ThreadDemo6 {
    public static void main(String[] args) throws InterruptedException {
        Object o =new Object();
        Thread t1 =new Thread(() -> {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName());
        },"a");
        Thread t2 =new Thread(() -> {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName());
        },"b");
        Thread t3 =new Thread(() -> {
            System.out.println(Thread.currentThread().getName());

        },"c");
        t1.start();
        t2.start();
        t3.start();
    }
}
