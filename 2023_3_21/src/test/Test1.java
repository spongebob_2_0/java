package test;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-21
 * Time: 15:56
 */
public class Test1 {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        while (scanner.hasNext()) {
            String str1 =scanner.nextLine();
            String str2 =scanner.nextLine();
            ArrayList<Character> list =new ArrayList<>();
            for(int i = 0; i < str1.length(); i++) {
                char ch =str1.charAt(i);
                if(!str2.contains(ch+"")) {
                    list.add(ch);
                }
            }
            for (int i = 0; i < list.size(); i++) {
                System.out.print(list.get(i));
            }
        }
    }
}
