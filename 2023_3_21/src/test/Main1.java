package test;

import java.util.Scanner;
public class Main1{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            int n = sc.nextInt();
            int[] array = new int[n];
            for (int i = 0; i <n; i++) {
                array[i] = sc.nextInt();
            }
            int flag = 0;//用标志位确定当前序列状态，0表示不增不减，1表示递增，-1表示递减
            int count = 1;//计算排序子序列的个数，默认为1也就是从当前序列开始
            for (int i = 1; i < n; i++) {
                if (array[i - 1] < array[i]) {
                    if (flag == 0) {   //
                        flag = 1;//   确定现在是递增是递增序列
                    }
                    if (flag == -1) {  //从递减序列转到下一个序列
                        flag = 0;//重置，避免重复分组时重复利用同一个
                        //（如：{1,3,1,3}错误分为{1,3}{3,1}{1,3}三组，正确情况应是{1,3}{1,3}）
                        count++;   //进入下一个序列计数器加1
                    }
                } else if (array[i - 1] > array[i]) {
                    if (flag == 0) {
                        flag = -1;   //确定现在是递减序列
                    }
                    if (flag == 1) {   //从递增序列转到下一个序列
                        flag = 0;   //重置
                        count++;   //进入下一个序列计数器加1
                    }
                }
            }
            System.out.println(count);
        }
    }
}