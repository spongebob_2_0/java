package test;

public class Singleton {

    // 提供一个对象的引用
    private static volatile Singleton instance = null;

    // 防止外部new对象
    private Singleton(){}

    // 用于创建对象或使得外部能够访问到创建的对象
    public static Singleton getInstance(){
        //用于判断是否加锁
        if(instance == null) {
            synchronized (Singleton.class) {
                //用于判断是否创建对象实例
                if (instance == null) {
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }
}