package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-21
 * Time: 0:57
 */
class MyRunnable implements Runnable {
    int ticket =10;
    @Override
    public void run() {
        BuyTicket();
    }
    public void BuyTicket() {
        for(int i = 1; i < 100; i++) {
            synchronized (this) {
                if (ticket > 0) {
                    System.out.println("第" + i + "人在" + Thread.currentThread().getName() + "买到了第" + ticket + "张票");
                    ticket--;
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
public class ThreadDemo2 {
    public static void main(String[] args) {
        MyRunnable runnable =new MyRunnable();
        Thread t1 =new Thread(runnable,"窗口1");
        Thread t2 =new Thread(runnable,"窗口2");
        t1.start();
        t2.start();
    }
}
