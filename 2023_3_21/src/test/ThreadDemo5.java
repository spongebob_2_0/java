package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-21
 * Time: 18:01
 */
public class ThreadDemo5 {
    public static void main(String[] args) throws InterruptedException {
        Object o =new Object();
        for(int i =0; i < 10; i++) {
            Thread t1 = new Thread(() -> {
                synchronized (o) {
                    System.out.print("A");
                }
            });
            t1.start();
            Thread t2 = new Thread(() -> {
                synchronized (o) {
                    System.out.print("B");
                }
            });
            t2.start();
            Thread t3 = new Thread(() -> {
                synchronized (o) {
                    System.out.println("C");
                }
            });
            t3.start();
            Thread.sleep(1000);
        }
    }
}
