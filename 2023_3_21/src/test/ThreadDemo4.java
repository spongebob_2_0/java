package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-21
 * Time: 14:13
 */
public class ThreadDemo4 {
    public static void main(String[] args) throws InterruptedException {
        Object o =new Object();
        Thread Thread0 =new Thread(() -> {
            synchronized (o) {
                System.out.println("wait方法开始");
                try {
                    o.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("wait方法结束");
            }
        });
        Thread0.start();
        //此处睡眠1秒保证线程Thread0先执行
        Thread.sleep(1000);
        Thread Thread1 =new Thread(() -> {
            synchronized (o) {
                System.out.println("notify方法开始");
                o.notify();
                System.out.println("notify方法结束");
            }
        });
        Thread1.start();
    }
}
