package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-21
 * Time: 1:09
 */
class BuyTicket {
    int ticket =10;
    public void BuyTicket(){
        for(int i = 1; i < 100; i++) {
            synchronized (this) {
                if (ticket > 0) {
                    System.out.println("第" + i + "人在" + Thread.currentThread().getName() + "买到了第" + (ticket--) + "张票");
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if(ticket == 0) {
                return;
            }
        }
    }
}
public class ThreadDemo3 {
    public static void main(String[] args) {
        BuyTicket buyTicket =new BuyTicket();
        Thread t1 =new Thread(() -> {
            buyTicket.BuyTicket();
        },"窗口1");
        Thread t2 =new Thread(() -> {
            buyTicket.BuyTicket();
        },"窗口2");
        t1.start();
        t2.start();
    }
}
