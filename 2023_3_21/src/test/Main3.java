package test;

import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextLine()) { // 注意 while 处理多个 case
            String str =in.nextLine();
            // str =str + "a";
            String str1 ="";
            String str2 ="";
            for(int i = 0; i < str.length(); i++) {
                char ch =str.charAt(i);
                if(ch >= '0' && ch <= '9') {
                    str1 =str1 +ch+ "";
                }else {
                    if(str1.length() > str2.length()) {
                        str2 = str1;
                    }
                    str1 ="";
                }
            }
            if(str1.length() > str2.length()) {
                str2 =str1;
            }
            System.out.println(str2);
        }
    }
}