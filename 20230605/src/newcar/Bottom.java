package newcar;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-05
 * Time: 14:00
 */
public class Bottom {
    private Tire tire;

    public Bottom (Tire tire) {
        this.tire = tire;
    }

    public void init() {
        System.out.println("do tire");
        tire.init();
    }
}
