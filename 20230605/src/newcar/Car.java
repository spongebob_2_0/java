package newcar;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-05
 * Time: 13:55
 */
public class Car {
    private Framework  framework;

    public Car(Framework framework) {
        this.framework = framework;
    }
    public void init() {
        System.out.println("do car....");
        framework.init();
    }

    public static void main(String[] args) {
        Tire tire = new Tire(18,"红色");
        Bottom bottom = new Bottom(tire);
        Framework framework = new Framework(bottom);
        Car car = new Car(framework);
        car.init();
    }
}
