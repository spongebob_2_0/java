package newcar;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-05
 * Time: 14:02
 */
public class Tire {
    private int size = 17;
    private String color ;

    public Tire (int size,String color) {
        this.size = size;
        this.color = color;
    }
    public void init() {
        System.out.println("size = " + size + " color = " + color);
    }
}
