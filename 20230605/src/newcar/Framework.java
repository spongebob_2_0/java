package newcar;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-05
 * Time: 13:58
 */
public class Framework {
    private Bottom bottom;

    public Framework (Bottom bottom) {
        this.bottom = bottom;
    }

    public void init() {
        System.out.println("do bottom");
        bottom.init();
    }
}
