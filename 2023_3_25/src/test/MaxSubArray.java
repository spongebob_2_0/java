package test;

public class MaxSubArray {
    
    public int bruteMethod(int[] A){
        int maxResult = A[0];        
        int maxTemp = 0;;
        for(int i = 0;i < A.length;i++){
            for(int j = i;j < A.length;j++){
                for(int k = i;k <= j;k++){
                    maxTemp += A[k];
                }
                if(maxTemp > maxResult)
                    maxResult = maxTemp;
                maxTemp = 0;             //完成一个子序列求和后，重新赋值为0
            }
        }
        return maxResult;
    }
    
    public static void main(String[] args){
        MaxSubArray test = new MaxSubArray();
        int[] A = {1,-2,3,10,-4,7,2,10,-5,4};
        int x =test.bruteMethod(A);
        System.out.println("使用蛮力法求解数组A的最大连续子数组和为："+x);
    }
}
