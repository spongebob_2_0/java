package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-25
 * Time: 21:38
 */
public class ThreadDemo3 {
    public static void main(String[] args) throws InterruptedException {
        Object o =new Object();
        Thread t1 =new Thread(() -> {
            System.out.println("hello");

            synchronized (o) {
                try {
//                    o.wait(1000);
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        Thread t2 = new Thread(() -> {
            synchronized (o) {
                System.out.println("hello-线程二");
            }
        });
//        System.out.println(t.getState());
        t1.start();
        Thread.sleep(100);
        t2.start();
        Thread.sleep(100);
//        t.join();
//        Thread.sleep(100);
        System.out.println(t2.getState());
//        Thread.sleep(3000);


    }
}
