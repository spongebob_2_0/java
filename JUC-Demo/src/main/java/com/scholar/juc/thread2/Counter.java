package com.scholar.juc.thread2;

public class Counter {
    private static int count = 0; // 将count声明为静态变量

    // 使用对象锁同步静态变量的增加操作
    public synchronized void increase() {
        count++;
    }

    public static int getCount() {
        return count;
    }

    public static void main(String[] args) throws InterruptedException {
        Counter counter = new Counter();

        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                counter.increase(); // t1使用counter1实例增加count
            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                counter.increase(); // t2使用counter2实例增加count
            }
        });

        t1.start();
        t2.start();

        t1.join();
        t2.join();

        System.out.println("Final count is: " + Counter.getCount());
    }
}
