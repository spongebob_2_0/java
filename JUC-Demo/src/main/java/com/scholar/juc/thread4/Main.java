package com.scholar.juc.thread4;

import com.scholar.juc.thread3.MiTest;

import java.util.concurrent.atomic.AtomicStampedReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-03-15
 */
public class Main {
	private static Object obj = new Object();
	private static int count;

	public static void main1(String[] args) {
		synchronized (obj) {
			count++;
		}
	}

	static ThreadLocal tl1 = new ThreadLocal();
	static ThreadLocal tl2 = new ThreadLocal();

	public static void main15(String[] args) {

		tl1.set("123");
		tl2.set("456");
		Thread t1 = new Thread(() -> {
			System.out.println("t1:" + tl1.get());
			System.out.println("t1:" + tl2.get());
		});
		t1.start();

		System.out.println("main:" + tl1.get());
		System.out.println("main:" + tl2.get());

	}

	public static void main18(String[] args) throws InterruptedException {
		// 初始化AtomicStampedReference，引用初始值为"A"，初始版本号为1
		AtomicStampedReference<String> reference = new AtomicStampedReference<>("A", 1);

		int initialStamp = reference.getStamp(); // 获取初始版本号
		// 线程1：将"A"更改为"B"，然后再更改回"A"
		Thread t1 = new Thread(() -> {
			reference.compareAndSet(reference.getReference(), "B", initialStamp, initialStamp + 1); // 将"A"改为"B"
			int newStamp = reference.getStamp(); // 获取"B"后的新版本号
			reference.compareAndSet(reference.getReference(), "A", newStamp, newStamp + 1); // 将"B"改回"A"
		});
		t1.start();
		t1.join(); // 确保t1操作完成

		// 线程2尝试使用“初始的版本号”（即1）将"A"更新为"C"
		Thread t2 = new Thread(() -> {
			// 使用初始的版本号1尝试更新，预期由于版本号不匹配而失败
			boolean isSuccess = reference.compareAndSet("A", "C", initialStamp, initialStamp + 1);
			System.out.println("是否修改成功: " + isSuccess + "，当前版本号：" + reference.getStamp() + "，当前值：" + reference.getReference());
		});
		t2.start();
		t2.join(); // 确保t2操作完成
	}


	public static void main19(String[] args) {
		ThreadLocal<String> stringThreadLocal = new ThreadLocal<>();
		stringThreadLocal.set("测试ThreadLocal存储");
		stringThreadLocal.remove();
	}

	private volatile static boolean flag = true;

	public static void main21(String[] args) throws InterruptedException {
		Thread t1 = new Thread(() -> {
			while (flag) {
				// ....
			}
			System.out.println("t1线程结束");
		});

		t1.start();
		Thread.sleep(10);
		flag = false;
		System.out.println("主线程将flag改为false");
	}


	public static void main22(String[] args) throws InterruptedException {
		Thread t1 = new Thread(() -> {
			while (flag) {
				synchronized (MiTest.class){
					//...
				}
				System.out.println(111);
			}
			System.out.println("t1线程结束");

		});

		t1.start();
		Thread.sleep(10);
		flag = false;
		System.out.println("主线程将flag改为false");
	}

	private static Lock lock = new ReentrantLock();

	public static void main(String[] args) throws InterruptedException {
		Thread t1 = new Thread(() -> {
			while (flag) {
				lock.lock();
				try{
					//...
				}finally {
					lock.unlock();
				}
			}
			System.out.println("t1线程结束");

		});

		t1.start();
		Thread.sleep(10);
		flag = false;
		System.out.println("主线程将flag改为false");
	}

}
