package com.scholar.juc.thread4;

public class Singleton {
 
    // 提供一个对象的引用
    private static volatile Singleton instance = null;
 
    // 防止外部new对象
    private Singleton(){}
 
    // 提供静态方法创建对象或使得外部能够访问到创建的对象
    public static Singleton getInstance(){

        // 为了防止编译器优化不去主内存读取最新的值，我们给“instance” 加上volatile关键字保证内存可见性
        if(instance == null) {  // 这一步判断目的是，如果“instance”对象已经存在，就不需要进入加锁的代码去创建了
            synchronized (Singleton.class) { // 加一个类锁，保证创建对象的操作具有原子性
                if (instance == null){
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }
}