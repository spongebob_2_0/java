//package com.scholar.juc.thread6;
//
//import java.util.Arrays;
//import java.util.List;
//import java.util.Scanner;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
///**
// * Description
// *
// * @author: WuYimin
// * Date: 2024-03-17
// */
//public class Main9 {
//	static int e = 100;
//
//	public class MyClass {
//		public static void main(String[] args){
//			int i = 100;
//			System.out.println(i);
//			for(int k = 0; k < 10; i++){
//				int f = 100;
//			}
//			// 这里是无法访问f变量的
//			System.out.println(f);
//
//			// 这里是可以访问e的
//			System.out.println(e);
//		}
//		public static void m(){
//			// 这里无法访问main方法中的i
//			System.out.println(i);
//		}
//	}
//
//}
