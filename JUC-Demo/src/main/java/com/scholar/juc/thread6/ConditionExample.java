package com.scholar.juc.thread6;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConditionExample {
    private static final Lock lock = new ReentrantLock();
    private static final Condition conditionA = lock.newCondition();
    private static final Condition conditionB = lock.newCondition();

    public static void awaitA() {
        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + ": 等待A");
            conditionA.await();
            System.out.println(Thread.currentThread().getName() + ": 被A唤醒");
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } finally {
            lock.unlock();
        }
    }

    public static void awaitB() {
        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + ": 等待B");
            conditionB.await();
            System.out.println(Thread.currentThread().getName() + ": 被B唤醒");
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } finally {
            lock.unlock();
        }
    }

    public static void signalA() {
        lock.lock();
        try {
            System.out.println("唤醒所有等待A的线程");
            conditionA.signalAll();
        } finally {
            lock.unlock();
        }
    }

    public static void signalB() {
        lock.lock();
        try {
            System.out.println("唤醒所有等待B的线程");
            conditionB.signalAll();
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        new Thread(ConditionExample::awaitA, "Thread-A1").start();
        new Thread(ConditionExample::awaitA, "Thread-A2").start();
        new Thread(ConditionExample::awaitB, "Thread-B1").start();
        new Thread(ConditionExample::awaitB, "Thread-B2").start();

        Thread.sleep(1000); // 确保所有线程都已启动并等待

        signalA(); // 只唤醒等待A的线程
        Thread.sleep(1000); // 等待一会儿看输出
        signalB(); // 然后唤醒等待B的线程
    }
}
