package com.scholar.juc.thread6;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SimpleThreadPoolDemo {
    public static void main(String[] args) {

        //1,创建一个默认的线程池对象.池子中默认是空的.默认最多可以容纳int类型的最大值.
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        // 提交第一个任务给线程池执行
        executorService.submit(() -> {
            // 打印当前线程的名称，显示它在执行任务
            System.out.println(Thread.currentThread().getName() + " 在执行任务1");
        });

        // 提交第二个任务给线程池执行
        executorService.submit(() -> {
            // 同样打印当前线程的名称，显示它在执行任务
            System.out.println(Thread.currentThread().getName() + " 在执行任务2");
        });

        // 关闭线程池，不接受新任务，已提交的任务继续执行
        executorService.shutdown();
    }
}
