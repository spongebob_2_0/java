package com.scholar.juc.thread6;

import java.io.IOException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-03-16
 */
public class Main6 {
	public static void main(String[] args) throws InterruptedException, IOException {
		// 创建一个ReentrantLock实例
		ReentrantLock lock = new ReentrantLock();
		// 基于ReentrantLock实例创建一个Condition实例
		Condition condition = lock.newCondition();

		new Thread(() -> {
			lock.lock(); // 子线程加锁
			System.out.println("子线程获取锁资源并await挂起线程");
			try {
				Thread.sleep(5000); // 模拟子线程执行一些操作，延时5秒
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			try {
				condition.await(); // 子线程在Condition上await，等待被唤醒
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("子线程挂起后被唤醒！持有锁资源");
			lock.unlock(); // 子线程释放锁
		}).start();

		Thread.sleep(100); // 主线程延时一段时间，确保子线程先执行并挂起

		// =================main======================
		lock.lock(); // 主线程加锁
		System.out.println("主线程等待5s拿到锁资源，子线程执行了await方法");
		condition.signal(); // 主线程调用signal方法，唤醒在Condition上等待的子线程
		System.out.println("主线程唤醒了await挂起的子线程");
		lock.unlock(); // 主线程释放锁
	}

}
