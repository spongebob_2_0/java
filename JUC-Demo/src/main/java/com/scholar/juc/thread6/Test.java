package com.scholar.juc.thread6;

class Parent {
    String name = "Parent's name";

    public static void func() {
        System.out.println("我是父类方法");
    }
}

class Child extends Parent {
    String name = "Child's name";
    public static void func() {
        System.out.println("我是子类方法");
    }
}

public class Test {
    public static void main(String[] args) {
        Parent p = new Child();
        System.out.println(p.name); // 输出: Parent's name
        p.func();
    }
}
