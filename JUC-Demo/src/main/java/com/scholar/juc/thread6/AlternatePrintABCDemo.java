package com.scholar.juc.thread6;

import java.util.concurrent.locks.LockSupport;

public class AlternatePrintABCDemo {

	private static Thread threadA, threadB, threadC;

	public static void main(String[] args) {
		threadA = new Thread(() -> printLetter("A", threadB), "Thread-A");
		threadB = new Thread(() -> printLetter("B", threadC), "Thread-B");
		threadC = new Thread(() -> printLetter("C", threadA), "Thread-C");

		threadA.start();
		threadB.start();
		threadC.start();

		// 初始唤醒线程A开始打印
		LockSupport.unpark(threadA);

	}

	private static void printLetter(String letter, Thread nextThread) {
		for (int i = 0; i < 3; i++) {
			try {
				// 当前线程阻塞，等待被唤醒
				LockSupport.park();
				System.out.println(letter);
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
			// 唤醒下一个线程
			LockSupport.unpark(nextThread);
		}
	}
}
