package com.scholar.juc.thread6;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadPoolExample {

    public static void main(String[] args) {
        // 定义核心线程数
        int CORE_POOL_SIZE = 5;
        // 定义最大线程数
        int MAXIMUM_POOL_SIZE = 10;
        // 定义空闲线程的存活时间
        long KEEP_ALIVE = 1L;

        // 创建任务队列，用于存放等待执行的任务
        BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>(50);

        // 创建线程工厂，用于创建新线程
        ThreadFactory threadFactory = new ThreadFactory() {
            private final AtomicInteger threadNumber = new AtomicInteger(1);

            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r, "my-pool-" + threadNumber.getAndIncrement());
                return t;
            }
        };

        // 创建线程池
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(
            CORE_POOL_SIZE, // 核心线程数
            MAXIMUM_POOL_SIZE, // 最大线程数
            KEEP_ALIVE, // 空闲线程等待新任务的最长时间
            TimeUnit.SECONDS, // KEEP_ALIVE的时间单位
            workQueue, // 任务队列
            threadFactory // 线程工厂
        );

        // 向线程池提交任务
        for (int i = 0; i < 20; i++) {
            int finalI = i;
            threadPool.execute(() -> {
                System.out.println(Thread.currentThread().getName() + " 执行任务 " + finalI);
                try {
                    // 模拟任务执行时间
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            });
        }

        // 关闭线程池
        // 逐步关闭线程池，不再接收新任务，但会等待队列里现有任务执行完毕
        // 注意：在实际使用场景中，通常只需要调用shutdown()或shutdownNow()中的一个。
        threadPool.shutdown();
        // 或者，如果需要立即关闭线程池，可以调用shutdownNow()
        // List<Runnable> notExecutedTasks = threadPool.shutdownNow();
    }
}