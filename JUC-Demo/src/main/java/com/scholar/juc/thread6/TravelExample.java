package com.scholar.juc.thread6;

import java.util.concurrent.CyclicBarrier;

public class TravelExample {
    public static void main(String[] args) throws InterruptedException {
        // 创建CyclicBarrier对象，设置屏障点为3，以及屏障点达到后执行的任务
        CyclicBarrier barrier = new CyclicBarrier(3, () -> {
            System.out.println("等到各位大佬都到位之后，分发护照和签证等内容！");
        });

        // Tom到位的线程
        new Thread(() -> {
            System.out.println("Tom到位！！！");
            try {
                barrier.await(); // 等待其他人到位
            } catch (Exception e) {
                System.out.println("悲剧，人没到齐！");
                return;
            }
            System.out.println("Tom出发！！！"); // 所有人都到齐后继续执行
        }).start();

        Thread.sleep(100); // 确保线程按顺序启动

        // Jack到位的线程
        new Thread(() -> {
            System.out.println("Jack到位！！！");
            try {
                throw new RuntimeException();
                //barrier.await(); // 等待其他人到位
            } catch (Exception e) {
                System.out.println("悲剧，人没到齐！");
                return;
            }
            //System.out.println("Jack出发！！！"); // 所有人都到齐后继续执行
        }).start();

        Thread.sleep(100); // 确保线程按顺序启动

        // Rose到位的线程
        new Thread(() -> {
            System.out.println("Rose到位！！！");
            try {
                barrier.await(); // 等待其他人到位
            } catch (Exception e) {
                System.out.println("悲剧，人没到齐！");
                return;
            }
            System.out.println("Rose出发！！！"); // 所有人都到齐后继续执行
        }).start();
        // 输出预期顺序：Tom到位，Jack到位，Rose到位 -> 导游分发护照和签证 -> Tom出发，Jack出发，Rose出发
    }
}