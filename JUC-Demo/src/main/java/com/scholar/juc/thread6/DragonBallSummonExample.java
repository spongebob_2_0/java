package com.scholar.juc.thread6;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class DragonBallSummonExample {
    // 假设需要7个召唤者（线程）集齐7颗龙珠
    private static final int SUMMONERS = 7;
    // 创建CyclicBarrier对象，并设置当屏障触发时执行的任务，即召唤神龙
    private static CyclicBarrier barrier = new CyclicBarrier(SUMMONERS, new Runnable() {
        @Override
        public void run() {
            // 所有召唤者都集齐龙珠后，执行这里的代码召唤神龙
            System.out.println("所有龙珠已集齐，现在召唤神龙！");
        }
    });

    public static void main(String[] args) {
        // 启动7个线程，每个线程代表一个召唤者
        for (int i = 1; i <= SUMMONERS; i++) {
            final int dragonBall = i;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.println("召唤者" + Thread.currentThread().getId() + "已经收集到第" + dragonBall + "颗龙珠");
                    try {
                        // 调用await方法等待其他召唤者集齐龙珠
                        barrier.await();
                        System.out.println("召唤者" + Thread.currentThread().getId() + "下线");
                    } catch (InterruptedException | BrokenBarrierException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }
}
