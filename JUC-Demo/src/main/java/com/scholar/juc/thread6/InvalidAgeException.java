package com.scholar.juc.thread6;

public class InvalidAgeException extends Exception {
    // 1. 无参构造方法
    public InvalidAgeException() {
        super();
    }

    // 2. 带有详细消息字符串的构造方法
    public InvalidAgeException(String message) {
        super(message);
    }

    // 3. 带有详细消息字符串和原因异常的构造方法
    public InvalidAgeException(String message, Throwable cause) {
        super(message, cause);
    }

    // 4. 带有原因异常的构造方法
    public InvalidAgeException(Throwable cause) {
        super(cause);
    }
}