package com.scholar.juc.thread6;

public class Demo {
    public static void main(String[] args) {
        try {
            check(-10);
        } catch (AgeException e) {
            e.printStackTrace();
        }
    }

    public static void check(int age) throws AgeException {
        if (age < 0) {
            throw new AgeException("年龄不能小于0");
        } else {
            System.out.println(age);
        }

    }
}
