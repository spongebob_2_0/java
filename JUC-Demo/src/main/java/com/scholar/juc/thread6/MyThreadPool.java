package com.scholar.juc.thread6;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-03-17
 */
public class MyThreadPool {

	public static void main(String[] args) {
		ThreadPoolExecutor threadPool = new ThreadPoolExecutor(
				3, // 核心线程数
				10, // 最大线程数
				10, // 线程闲置超时时长
				TimeUnit.SECONDS, // 单位秒
				new LinkedBlockingDeque<>(40),// 任务队列
				new ThreadPoolExecutor.DiscardOldestPolicy() // 拒绝策略
		);
	}
}
