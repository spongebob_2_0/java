package com.scholar.juc.thread6;

import java.util.concurrent.Semaphore;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-03-16
 */
public class Main5 {
	public static void main(String[] args) throws InterruptedException {
		// 初始化一个信号量，设置10个许可，模拟环球影城最多接纳10个人
		Semaphore semaphore = new Semaphore(10);

		// 模拟一家三口来到环球影城
		new Thread(() -> {
			System.out.println("一家三口来到环球影城~~");
			try {
				// 尝试获取3个许可，模拟一家三口占用3个名额
				semaphore.acquire(3);
				System.out.println("一家三口进去了~~~");
				Thread.sleep(10000); // 模拟游玩时间
			} catch (InterruptedException e) {
				e.printStackTrace();
			}finally {
				System.out.println("一家三口走了~~~");
				// 释放3个许可，让其他游客能够进入
				semaphore.release(3);
			}
		}).start();

		// 模拟其他7个游客陆续到达环球影城
		for (int i = 0; i < 7; i++) {
			int j = i;
			new Thread(() -> {
				System.out.println(j + "大哥来到环球影城。");
				try {
					// 每个游客尝试获取1个许可
					semaphore.acquire();
					System.out.println(j + "大哥进去了~~~");
					Thread.sleep(10000); // 模拟游玩时间
				} catch (InterruptedException e) {
					e.printStackTrace();
				}finally {
					System.out.println(j + "大哥走了~~~");
					// 释放1个许可
					semaphore.release();
				}
			}).start();
		}

		Thread.sleep(10);

		// 主线程模拟额外的游客尝试进入环球影城
		System.out.println("main大哥来到环球影城。");
		// 尝试获取1个许可，如果获取失败，则表示没有足够的许可
		if (semaphore.tryAcquire()) {
			System.out.println("main大哥进来了。");
		}else{
			System.out.println("资源不够，main大哥没能进来。"); // 注意这里的文本更正
		}
		Thread.sleep(10000); // 模拟等待时间

		System.out.println("main大哥又来了。");
		// 再次尝试获取1个许可
		if (semaphore.tryAcquire()) {
			System.out.println("main大哥进来了。");
			// 释放1个许可
			semaphore.release();
		}else{
			System.out.println("资源不够，main大哥没能进来。"); // 注意这里的文本更正
		}
	}
}
