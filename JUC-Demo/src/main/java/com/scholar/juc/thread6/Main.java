package com.scholar.juc.thread6;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

public class Main {
    public static void main(String[] args) {
        //三个停车位
        Semaphore semaphore = new Semaphore(3);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    semaphore.acquire();
                    System.out.println(Thread.currentThread().getName() + " 占据了停车位");
                    // 模拟车辆停留时间
                    Thread.sleep((long) (Math.random() * 20000));
                    semaphore.release();
                    System.out.println(Thread.currentThread().getName() + " 离开了停车位");
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        //假设5个线程相当于5辆车
        for (int i = 1; i <= 5; i++) {
            Thread t = new Thread(runnable);
            t.start();
        }

        ReentrantLock lock = new ReentrantLock();
        lock.newCondition();
    }
}
