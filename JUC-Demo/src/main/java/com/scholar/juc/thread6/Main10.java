package com.scholar.juc.thread6;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-03-17
 */
public class Main10 {
	public static void main(String[] args) {
		// 1. 创建定长线程池对象 & 设置线程池线程数量固定为3
		ExecutorService fixedThreadPool = Executors.newFixedThreadPool(3);
		// 2. 创建好Runnable类线程对象 & 需执行的任务
		Runnable task = new Runnable() {
			public void run() {
				System.out.println("执行任务啦");
			}
		};
		// 3. 向线程池提交任务
		fixedThreadPool.execute(task);
	}
}
