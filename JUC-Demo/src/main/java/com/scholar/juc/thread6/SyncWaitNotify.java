package com.scholar.juc.thread6;

class SyncWaitNotify {
    // 1 代表打印 a，2 代表打印 b，3 代表打印 c
    private int flag;
    // 循环次数
    private int loopNumber;
    
    public SyncWaitNotify(int flag, int loopNumber) {
        this.flag = flag;
        this.loopNumber = loopNumber;
    }
    // curFlag：当前线程，nextFlag：下一个执行的线程，str：打印的信息
    public void print(int curFlag, int nextFlag,String str) {
        for (int i = 0; i < loopNumber; i++) {
            synchronized (this) {
                while (this.flag != curFlag) {
                    try {
                        this.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.print(str);
                flag = nextFlag;
                this.notifyAll();
            }
        }
    }
    public static void main(String[] args) {
        // 初始化 1 打印，循环 5 次
        SyncWaitNotify syncWaitNotify = new SyncWaitNotify(1, 5);

        new Thread(() -> {
            syncWaitNotify.print(1, 2, "a");
        }).start();

        new Thread(() -> {
            syncWaitNotify.print(2, 3, "b");
        }).start();

        new Thread(() -> {
            syncWaitNotify.print(3, 1, "c");
        }).start();
    }
}
