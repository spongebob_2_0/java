package com.scholar.juc.thread6;

import java.util.concurrent.Semaphore;

public class ParkingLotSemaphoreExample {
    // 假设停车场只有3个停车位
    private static final Semaphore parkingLot = new Semaphore(3);

    static class Car extends Thread {
        private int carNumber;

        Car(int carNumber) {
            this.carNumber = carNumber;
        }

        @Override
        public void run() {
            try {
                // 尝试获得一个停车位
                parkingLot.acquire();
                System.out.println("Car#" + carNumber + " parked.");
                // 模拟车辆停留时间
                Thread.sleep((long) (Math.random() * 10000));
                System.out.println("Car#" + carNumber + " left.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                // 释放停车位
                parkingLot.release();
            }
        }
    }

    public static void main(String[] args) {
        // 模拟5辆车进入停车场
        for (int i = 1; i <= 5; i++) {
            new Car(i).start();
        }
    }
}
