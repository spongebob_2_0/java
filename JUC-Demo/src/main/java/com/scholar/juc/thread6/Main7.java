package com.scholar.juc.thread6;

import java.util.concurrent.locks.LockSupport;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-03-16
 */
public class Main7 {
	public static void main(String[] args) {
		Thread a = new Thread(() -> {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println(Thread.currentThread().getName() + "\t" + "-----come in " + System.currentTimeMillis());
			LockSupport.park();  // 被阻塞....等待通知，他需要通过许可证
			System.out.println(Thread.currentThread().getName() + "\t" + "-----被 唤 醒 " + System.currentTimeMillis());
		}, "a");
		a.start();

		Thread b = new Thread(() -> {
			LockSupport.unpark(a);
			System.out.println(Thread.currentThread().getName() + "\t" + "-----通知了");
		}, "b");
		b.start();
	}
}
