package com.scholar.juc.thread6;

import java.util.concurrent.locks.LockSupport;

public class AlternatePrintDemo {

    static Thread thread1, thread2;

    public static void main(String[] args) {
        thread1 = new Thread(() -> {
            for (int i = 1; i <= 26; i++) {
                System.out.print(i);
                // 唤醒thread2线程
                LockSupport.unpark(thread2);
                // 阻塞当前线程
                LockSupport.park();
            }
        });

        thread2 = new Thread(() -> {
            for (char c = 'A'; c <= 'Z'; c++) {
                // 阻塞当前线程，等待被thread1唤醒
                LockSupport.park();
                System.out.print(c);
                // 唤醒thread1线程
                LockSupport.unpark(thread1);
            }
        });

        thread1.start();
        thread2.start();
    }
}
