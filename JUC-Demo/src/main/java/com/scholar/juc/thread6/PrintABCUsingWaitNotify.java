package com.scholar.juc.thread6;

public class PrintABCUsingWaitNotify {
    private int state = 0;
    private final int times = 10;

    public synchronized void printLetter(String name, int targetState) {
        for (int i = 0; i < times;) {
            while (state % 3 != targetState) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            state++;
            i++;
            System.out.println(name);
            notifyAll();
        }
    }

    public static void main(String[] args) {
        PrintABCUsingWaitNotify printABC = new PrintABCUsingWaitNotify();
        new Thread(() -> printABC.printLetter("A", 0), "A").start();
        new Thread(() -> printABC.printLetter("B", 1), "B").start();
        new Thread(() -> printABC.printLetter("C", 2), "C").start();
    }
}
