package com.scholar.juc.thread6;

import java.util.concurrent.locks.LockSupport;

public class LockSupportDemo {
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(() -> {
            System.out.println("线程开始，即将进入等待状态");
            // 阻塞当前线程
            LockSupport.park();
            System.out.println("线程被唤醒，继续执行");
        });

        thread.start();
        // 主线程暂停一秒，确保子线程已经在park等待
        Thread.sleep(1000);
        System.out.println("一秒后，主线程准备唤醒子线程");
        // 唤醒指定的线程
        LockSupport.unpark(thread);
    }
}
