package com.scholar.juc.thread6;

public class AgeVerification {
    public static void verifyAge(int age) throws InvalidAgeException {
        if (age < 0 || age > 150) {
            throw new InvalidAgeException("年龄无效: " + age);
        }
        System.out.println("年龄验证通过: " + age);
    }

    public static void main(String[] args) {
        try {
            verifyAge(-1); // 尝试一个无效的年龄值
        } catch (InvalidAgeException e) {
            System.err.println(e.getMessage());
        }
    }
}