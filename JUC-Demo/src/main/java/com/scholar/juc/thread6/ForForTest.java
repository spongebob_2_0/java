package com.scholar.juc.thread6;

class ForForTest {
    public static void main(String[] args) {
        
        // 打印一行星号
        // 结果: ******
        for(int i = 1; i <= 6; i++) {
            System.out.print("*");
        }
        System.out.println(); // 换行

        System.out.println(); // 为不同的图形间增加空行，便于区分

        // 打印四行六列的星号
        // 结果:
        // ******
        // ******
        // ******
        // ******
        for(int i = 1; i <= 4; i++) {  
            for(int j = 1; j <= 6; j++) {  
                System.out.print('*');
            }
            System.out.println(); // 换行
        }

        System.out.println(); // 为不同的图形间增加空行，便于区分

        // 打印直角三角形
        // 结果:
        // *
        // **
        // ***
        // ****
        // *****
        for(int i = 1; i <= 5; i++) { // 控制行数
            for(int j = 1; j <= i; j++) { // 控制列数，每行星号的数量等于行号
                System.out.print("*");
            }
            System.out.println();
        }

        System.out.println(); // 为不同的图形间增加空行，便于区分

        // 打印倒直角三角形
        // 结果:
        // *****
        // ****
        // ***
        // **
        // *
        for(int i = 1; i <= 5; i++) {
            for(int j = 1; j <= 5 - i; j++) {
                System.out.print("*");
            }
            System.out.println();
        }

        System.out.println(); // 为不同的图形间增加空行，便于区分

        // 打印菱形的上半部分
        // 结果:
        // *
        // **
        // ***
        // ****
        // *****
        for(int i = 1; i <= 5; i++) {
            for(int j = 1; j <= i; j++) {
                System.out.print("*");
            }
            System.out.println();
        }

        // 打印菱形的下半部分
        // 结果（继续上半部分后）:
        // ****
        // ***
        // **
        // *
        for(int i = 1; i <= 4; i++) {
            for(int j = 1; j <= 4 - i + 1; j++) {
                System.out.print("*");
            }
            System.out.println();
        }

        System.out.println(); // 为不同的图形间增加空行，便于区分

        // 九九乘法表
        // 结果:
        // 1*1=1 
        // 2*1=2 2*2=4 
        // ...
        // 9*1=9 9*2=18 ... 9*9=81
        for(int i = 1; i <= 9; i++) {
            for(int j = 1; j <= i; j++) {
                System.out.print(i + "*" + j + "=" + (i * j) + " ");
            }
            System.out.println(); // 换行
        }
    }
}
