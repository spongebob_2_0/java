package com.scholar.juc.thread6;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DemoClass {
    // 控制打印顺序的标志
    private int state = 0;
    private Lock lock = new ReentrantLock();
    // 创建条件变量，分别控制打印A、B、C
    private Condition conditionA = lock.newCondition();
    private Condition conditionB = lock.newCondition();
    private Condition conditionC = lock.newCondition();

    // 打印A的方法
    public void printA(int round) {
        lock.lock();
        try {
            // 当不是A打印的轮次时，A等待
            while (state != 0) {
                conditionA.await();
            }
            System.out.println(Thread.currentThread().getName() + "输出A, 第" + round + "轮开始");
            // 打印5次A
            for (int i = 0; i < 5; i++) {
                System.out.println("A");
            }
            // 更改轮次，唤醒打印B的线程
            state = 1;
            conditionB.signal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    // 打印B的方法
    public void printB(int round) {
        lock.lock();
        try {
            // 当不是B打印的轮次时，B等待
            while (state != 1) {
                conditionB.await();
            }
            System.out.println(Thread.currentThread().getName() + "输出B, 第" + round + "轮开始");
            // 打印10次B
            for (int i = 0; i < 10; i++) {
                System.out.println("B");
            }
            // 更改轮次，唤醒打印C的线程
            state = 2;
            conditionC.signal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    // 打印C的方法
    public void printC(int round) {
        lock.lock();
        try {
            // 当不是C打印的轮次时，C等待
            while (state != 2) {
                conditionC.await();
            }
            System.out.println(Thread.currentThread().getName() + "输出C, 第" + round + "轮开始");
            // 打印15次C
            for (int i = 0; i < 15; i++) {
                System.out.println("C");
            }
            // 打印结束，开始下一轮，唤醒打印A的线程
            System.out.println("-----------------------------------------");
            state = 0;
            conditionA.signal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) {
        DemoClass demoClass = new DemoClass();
        // 创建三个线程分别执行打印A、B、C的任务
        new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                demoClass.printA(i);
            }
        }, "线程A").start();

        new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                demoClass.printB(i);
            }
        }, "线程B").start();

        new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                demoClass.printC(i);
            }
        }, "线程C").start();
    }
}
