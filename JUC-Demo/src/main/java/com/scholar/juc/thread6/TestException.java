package com.scholar.juc.thread6;

public class TestException {
    public static void main(String[] args) {
        int result;
        try {
            result = foo();
        } catch (Exception e) {
            System.out.println(e.getMessage());    //输出：我是finaly中的Exception
        }
 
 
        try {
            result = bar();
        } catch (Exception e) {
            System.out.println(e.getMessage());    //输出：我是finaly中的Exception
        }
    }
 
    //catch中的异常被抑制
    @SuppressWarnings("finally")
    public static int foo() throws Exception {
        try {
            int a = 5 / 0;
            return 1;
        } catch (ArithmeticException amExp) {
            throw new Exception("我将被忽略，因为下面的finally中抛出了新的异常");
        } finally {
            throw new Exception("我是finally中的Exception");
        }
    }
 
    //try中的异常被抑制
    @SuppressWarnings("finally")
    public static int bar() throws Exception {
        try {
            int a = 5 / 0;  // 这里会抛出算术异常
            return 1;
        } finally {
            throw new Exception("我是finally中的Exception");
        }
 
    }
}