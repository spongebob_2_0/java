package com.scholar.juc.thread6;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class LockCondition {
    private final ReentrantLock lock = new ReentrantLock();
    // 使用一个条件变量
    private final Condition condition = lock.newCondition();
    // 状态变量来控制打印顺序
    private int state = 0;
    private final int times = 10; // 打印次数

    public void printLetter(String name, int targetState) {
        for (int i = 0; i < times;) {
            lock.lock();
            try {
                // 循环检查当前状态是否符合当前线程打印条件
                while (state % 3 != targetState) {
                    condition.await();
                }
                // 打印字母并更新状态和打印次数
                System.out.print(name);
                state++;
                i++;
                // 唤醒所有等待线程
                condition.signalAll();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }

    public static void main(String[] args) {
        LockCondition printABC = new LockCondition();
        // 创建并启动三个线程
        new Thread(() -> printABC.printLetter("A", 0), "A").start();
        new Thread(() -> printABC.printLetter("B", 1), "B").start();
        new Thread(() -> printABC.printLetter("C", 2), "C").start();
    }
}
