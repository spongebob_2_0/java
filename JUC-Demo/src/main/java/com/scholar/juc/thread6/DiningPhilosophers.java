package com.scholar.juc.thread6;

import java.util.concurrent.CountDownLatch;

public class DiningPhilosophers {
 
    private static final int NUM_PHILOSOPHERS = 5;
    private static final Object[] forks = new Object[NUM_PHILOSOPHERS];
 
    public static void main(String[] args) {
        // 初始化筷子对象
        for (int i = 0; i < NUM_PHILOSOPHERS; i++) {
            forks[i] = new Object();
        }
 
        // 创建哲学家线程并启动
        for (int i = 0; i < NUM_PHILOSOPHERS; i++) {
            final int philosopher = i;
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        // 哲学家思考
                        think(philosopher);
                        // 拿起左边的筷子
                        synchronized (forks[philosopher]) {
                            System.out.println("哲学家 " + philosopher + " 拿起左边的筷子");
                            // 拿起右边的筷子
                            synchronized (forks[(philosopher + 1) % NUM_PHILOSOPHERS]) {
                                System.out.println("哲学家 " + philosopher + " 拿起右边的筷子");
                                // 哲学家进餐
                                eat(philosopher);
                            }
                            System.out.println("哲学家 " + philosopher + " 放下右边的筷子");
                        }
                        System.out.println("哲学家 " + philosopher + " 放下左边的筷子");
                    }
                }
            });
            thread.start();

        }
    }
 
    private static void think(int philosopher) {
        try {
            Thread.sleep((long) (Math.random() * 10000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("哲学家 " + philosopher + " 思考");
    }
 
    private static void eat(int philosopher) {
        try {
            Thread.sleep((long) (Math.random() * 10000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("哲学家 " + philosopher + " 进餐");
    }
}