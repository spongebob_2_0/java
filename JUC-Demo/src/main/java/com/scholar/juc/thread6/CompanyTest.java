package com.scholar.juc.thread6;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class CompanyTest {

    // 创建固定大小为3的线程池，用于并发执行任务
    static ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(3);

    // 使用CountDownLatch等待3个任务完成
    static CountDownLatch countDownLatch = new CountDownLatch(3);

    public static void main(String[] args) throws InterruptedException {
        System.out.println("主业务开始执行");
        sleep(1000); // 模拟主业务执行前的准备工作

        // 提交三个并行任务到线程池执行
        executor.execute(CompanyTest::a);
        executor.execute(CompanyTest::b);
        executor.execute(CompanyTest::c);
        System.out.println("三个任务并行执行,主业务线程等待");

        // 使用CountDownLatch等待所有任务完成，或超时
        if (countDownLatch.await(10, TimeUnit.SECONDS)) {
            // 所有任务在规定时间内完成
            System.out.println("三个任务处理完毕，主业务线程继续执行");
        } else {
            // 任务没有在规定时间内全部完成
            System.out.println("三个任务没有全部处理完毕，执行其他的操作");
        }
    }

    // 任务A的具体执行逻辑
    private static void a() {
        System.out.println("A任务开始");
        sleep(1000); // 模拟任务执行耗时
        System.out.println("A任务结束");
        countDownLatch.countDown(); // 任务完成，计数器减1
    }

    // 任务B的具体执行逻辑
    private static void b() {
        System.out.println("B任务开始");
        sleep(1500); // 模拟任务执行耗时
        System.out.println("B任务结束");
        countDownLatch.countDown(); // 任务完成，计数器减1
    }

    // 任务C的具体执行逻辑
    private static void c() {
        System.out.println("C任务开始");
        sleep(2000); // 模拟任务执行耗时
        System.out.println("C任务结束");
        countDownLatch.countDown(); // 任务完成，计数器减1
    }

    // 模拟任务执行过程中的延时
    private static void sleep(long timeout){
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}