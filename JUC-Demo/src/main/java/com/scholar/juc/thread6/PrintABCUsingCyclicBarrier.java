package com.scholar.juc.thread6;

import java.util.concurrent.CyclicBarrier;

public class PrintABCUsingCyclicBarrier {
    private int times = 10; // 控制打印次数
    // 初始化CyclicBarrier，设置屏障打开前需要到达的线程数为3
    private CyclicBarrier cyclicBarrier = new CyclicBarrier(3);

    // 打印方法，接受一个待打印字符和一个Runnable任务
    private void print(String name) {
        for (int i = 0; i < times; i++) {
            try {
                // 打印字符
                System.out.print(name);
                // 等待其他线程到达屏障
                cyclicBarrier.await();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        PrintABCUsingCyclicBarrier printABC = new PrintABCUsingCyclicBarrier();
        // 创建三个线程，分别执行打印A、B、C的任务
        new Thread(() -> printABC.print("A")).start();
        new Thread(() -> printABC.print("B")).start();
        new Thread(() -> printABC.print("C")).start();
    }
}
