package com.scholar.juc.thread6;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConditionTest {
    private final Lock lock = new ReentrantLock();
    private final Condition condition = lock.newCondition();
    
    // 等待方法
    public void waiter() {
        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + ": 开始等待");
            condition.await();
            System.out.println(Thread.currentThread().getName() + ": 结束等待");
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } finally {
            lock.unlock();
        }
    }

    // 发送信号方法
    public void signaler() {
        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + ": 发送信号");
            condition.signal();
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        ConditionTest conditionTest = new ConditionTest();

        Thread waiterThread = new Thread(conditionTest::waiter, "等待线程");
        Thread signalerThread = new Thread(conditionTest::signaler, "信号线程");

        // 启动等待线程
        waiterThread.start();
        // 确保等待线程开始等待
        Thread.sleep(1000);
        // 启动信号线程
        signalerThread.start();

        // 等待线程结束
        waiterThread.join();
        signalerThread.join();
    }
}
