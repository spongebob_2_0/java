package com.scholar.juc.thread6;

import java.util.concurrent.Semaphore;

public class PrintABCUsingSemaphore {
    private final Semaphore semA = new Semaphore(1);
    private final Semaphore semB = new Semaphore(0);
    private final Semaphore semC = new Semaphore(0);

    public void printA() {
        try {
            for (int i = 0; i < 10; i++) {
                semA.acquire();
                Thread.sleep(1000);
                System.out.println("A");
                semB.release();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void printB() {
        try {
            for (int i = 0; i < 10; i++) {
                semB.acquire();
                Thread.sleep(1000);
                System.out.println("B");
                semC.release();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void printC() {
        try {
            for (int i = 0; i < 10; i++) {
                semC.acquire();
                Thread.sleep(1000);
                System.out.println("C");
                semA.release();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        PrintABCUsingSemaphore printABC = new PrintABCUsingSemaphore();
        new Thread(printABC::printA).start();
        new Thread(printABC::printB).start();
        new Thread(printABC::printC).start();
    }
}
