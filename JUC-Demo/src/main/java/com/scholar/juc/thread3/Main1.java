package com.scholar.juc.thread3;

import java.util.concurrent.ExecutionException;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-03-14
 */
public class Main1 {
	public static void main1(String[] args) {
		Thread t1 = new Thread(() -> {

		});
		System.out.println(t1.getState());

	}
	public static void main2(String[] args) throws InterruptedException {
		Thread t1 = new Thread(() -> {
			while(true){

			}
		});
		t1.start();
		Thread.sleep(500);
		System.out.println(t1.getState());
	}
	public static void main3(String[] args) throws InterruptedException {
		Object obj = new Object();
		Thread t1 = new Thread(() -> {
			synchronized (obj){
				// t1线程尝试进入同步块，但由于main线程持有锁，t1线程被阻塞
			}
		});
		synchronized (obj) {
			t1.start(); // 启动线程t1
			Thread.sleep(500); // 确保t1有足够的时间尝试进入同步块
			// 打印出BLOCKED，因为t1线程试图获取已被main线程持有的锁
			System.out.println(t1.getState());
		}
	}
	public static void main4(String[] args) throws InterruptedException {
		Object obj = new Object();
		Thread t1 = new Thread(() -> {
			synchronized (obj){
				try {
					obj.wait(); // t1线程在obj对象上等待，进入WAITING状态
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		t1.start(); // 启动线程
		Thread.sleep(500); // 确保线程t1有足够的时间进入wait状态
		// 打印出WAITING，因为t1线程正在obj上等待
		System.out.println(t1.getState());
	}
	public static void main5(String[] args) throws InterruptedException {
		Thread t1 = new Thread(() -> {
			try {
				Thread.sleep(1000); // t1线程睡眠1秒
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});
		t1.start(); // 启动线程
		Thread.sleep(500); // 主线程等待500毫秒，这时t1线程还在sleep
		// 打印出TIMED_WAITING，因为t1线程正在执行带有时间限制的等待
		System.out.println(t1.getState());
	}
	public static void main7(String[] args) throws InterruptedException {
		Thread t1 = new Thread(() -> {
			try {
				Thread.sleep(500); // t1线程睡眠500毫秒然后结束
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});
		t1.start(); // 启动线程
		Thread.sleep(1000); // 主线程等待1秒，确保t1线程已经执行完毕
		// 打印出TERMINATED，因为t1线程已经运行完毕
		System.out.println(t1.getState());
	}

	public static void main8(String[] args) throws ExecutionException, InterruptedException {
		// 获取当前线程的方法
		Thread main = Thread.currentThread();
		System.out.println(main);
		// "Thread[" + getName() + "," + getPriority() + "," +  group.getName() + "]";
		// Thread[main,5,main]
	}


	public static void main9(String[] args) throws InterruptedException {
		// 创建一个新的线程t1
		Thread t1 = new Thread(() -> {
			// t1线程执行的任务：循环打印消息，并在每次打印后暂停1秒
			for (int i = 0; i < 10; i++) {
				System.out.println("t1:" + i);
				try {
					Thread.sleep(1000); // 让t1线程暂停1秒
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		// 将t1线程设置为守护线程。这一行必须在t1线程启动之前执行。
		// 守护线程的特点是：它不会阻止JVM的退出。当所有的非守护线程都结束时，
		// JVM会自动退出，不会等待守护线程完成。
		t1.setDaemon(true);
		// 启动t1线程
		t1.start();
		// 程序运行很快就结束了
	}

	public static void main11(String[] args) throws ExecutionException, InterruptedException {
		// 方式一
		Thread t1 = new Thread(() -> {
			System.out.println(Thread.currentThread().getName()); // 获取当前线程名称
		});
		t1.setName("模块-功能-计数器");  // 在线程启动之前设置线程名称
		t1.start();

		// 方式二
		Thread t2 = new Thread(() -> {
			System.out.println(Thread.currentThread().getName()); // 获取当前线程名称
		},"模块-功能-点赞");   // 在创建线程时直接通过构造函数设置线程名称
	}

	public static void main(String[] args) throws ExecutionException, InterruptedException {
		Thread t1 = new Thread(() -> {
			for (int i = 0; i < 1000; i++) {
				System.out.println("t1:" + i);
			}
		});
		Thread t2 = new Thread(() -> {
			for (int i = 0; i < 1000; i++) {
				System.out.println("t2:" + i);
			}
		});
		t1.setPriority(Thread.MAX_PRIORITY);  // 设置线程优先级
		t2.setPriority(10); // 设置线程优先级
		t2.start();
		t1.start();
	}


}

