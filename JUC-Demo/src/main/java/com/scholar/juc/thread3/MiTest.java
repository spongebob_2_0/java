package com.scholar.juc.thread3;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-03-15
 */
public class MiTest {
	public static void main1(String[] args) throws InterruptedException {
		// 创建线程t1
		Thread t1 = new Thread(() -> {
			sync(); // 调用同步方法
		},"t1");

		// 创建线程t2
		Thread t2 = new Thread(() -> {
			sync(); // 调用同步方法
		},"t2");

		t1.start(); // 启动线程t1
		t2.start(); // 启动线程t2

		// 主线程等待12秒，确保t1和t2进入wait状态
		Thread.sleep(12000);

		// 在MiTest类的锁上进行同步
		synchronized (MiTest.class) {
			// 唤醒所有等待MiTest类锁的线程
			MiTest.class.notifyAll();
		}
	}

	// 定义一个同步方法，这个方法在MiTest类的锁上同步
	public static synchronized void sync()  {
		try {
			for (int i = 0; i < 10; i++) {
				if(i == 5) {
					// 当i等于5时，当前线程在MiTest类的锁上等待
					// 这会释放锁并使当前线程进入等待状态，直到其他线程调用notify()或notifyAll()
					MiTest.class.wait();
				}
				Thread.sleep(1000); // 每次循环延时1秒
				// 打印当前线程的名字
				System.out.println(Thread.currentThread().getName());
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public static void main12(String[] args) throws InterruptedException {
		Thread t1 = new Thread(() -> {
			while(true){
				// 获取任务
				// 拿到任务，执行任务
				// 没有任务了，让线程休眠
				try {
					Thread.sleep(1000);
					System.out.println(Thread.currentThread().getName() + "继续执行");
				} catch (InterruptedException e) {
					e.printStackTrace();
					//System.out.println("基于打断形式结束当前线程");
					//return;
				}
			}
		},"线程2");
		t1.start();
		Thread.sleep(500);
		t1.interrupt();  // 调用 interrupt() 方法来通知
	}

	public static void main15(String[] args) throws InterruptedException {
		Object obj = new Object();
		Thread t1 = new Thread(() -> {
			while(true){
				// 获取任务
				// 拿到任务，执行任务
				// 没有任务了，让线程休眠
				try {
					synchronized (obj) {
						obj.wait(1000);
						System.out.println(Thread.currentThread().getName() + "继续执行");
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
					System.out.println("基于打断形式结束当前线程");
					//return;
				}
			}
		});
		t1.start();
		Thread.sleep(500);
		t1.interrupt();  // 调用 interrupt() 方法来通知
	}

	private static int count;

	public static void increment(){
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		count++;
	}

	public static void main23(String[] args) throws InterruptedException {
		Thread t1 = new Thread(() -> {
			for (int i = 0; i < 100; i++) {
				increment();
			}
		});
		Thread t2 = new Thread(() -> {
			for (int i = 0; i < 100; i++) {
				increment();
			}
		});
		t1.start();
		t2.start();
		t1.join();
		t2.join();
		System.out.println(count);
	}


	private static boolean flag = true;

	public static void main24(String[] args) throws InterruptedException {
		Thread t1 = new Thread(() -> {
			while (flag) {
				//synchronized (MiTest.class){
				//	//...
				//}
				System.out.println(111);
			}
			System.out.println("t1线程结束");

		});

		t1.start();
		Thread.sleep(10);
		flag = false;
		System.out.println("主线程将flag改为false");
	}


	static int a, b, x, y;

	public static void main(String[] args) throws InterruptedException {
		for (int i = 0; i < Integer.MAX_VALUE; i++) {
			// 重置变量
			a = 0;
			b = 0;
			x = 0;
			y = 0;

			// 线程1执行
			Thread t1 = new Thread(() -> {
				a = 1; // T1.1
				x = b; // T1.2
			});

			// 线程2执行
			Thread t2 = new Thread(() -> {
				b = 1; // T2.1
				y = a; // T2.2
			});

			t1.start();
			t2.start();
			t1.join();
			t2.join();

			// 检查x和y是否同时为0，这种情况理论上在没有重排序的情况下不应该发生
			if (x == 0 && y == 0) {
				System.out.println("第" + i + "次，x = " + x + ", y = " + y);
			}
		}
	}

	private static volatile MiTest test;

	private MiTest(){}

	public static MiTest getInstance(){
		// B
		if(test  == null){
			synchronized (MiTest.class){

				if(test == null){
					// A，开辟空间，test指向地址，初始化
					test = new MiTest();
				}
			}
		}
		return test;
	}

}
