package com.scholar.juc.thread3;

/**
 * Description
 * 饿汉模式
 * @author: WuYimin
 * Date: 2024-03-15
 */
public class Singleton {

	private static Singleton singleton = new Singleton();

	private Singleton() {

	}

	public static Singleton getSingLeTon() {
		return singleton;
	}
}

class Singleton2 {
	private static volatile Singleton2 singleton2 = null;

	private Singleton2() {

	}

	public static Singleton2 getSingleton2() {
		if(singleton2 == null) {
			synchronized (Singleton2.class) {
				if(singleton2 == null) {
					singleton2 = new Singleton2();
				}
			}
		}
		return singleton2;
	}
}
