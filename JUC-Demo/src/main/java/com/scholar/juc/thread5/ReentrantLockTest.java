package com.scholar.juc.thread5;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-03-16
 */
public class ReentrantLockTest {
	public static void main(String[] args) {
		// 默认是非公平锁
		ReentrantLock lock1 = new ReentrantLock(false);
		// 有参构造设置为true，公平锁
		ReentrantLock lock2 = new ReentrantLock(true);
		Thread thread = new Thread(() ->{

		});
	}
}
