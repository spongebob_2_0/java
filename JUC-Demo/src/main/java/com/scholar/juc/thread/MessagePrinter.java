package com.scholar.juc.thread;

public class MessagePrinter {
    // 定义一个成员变量作为锁对象
    private  static final Object lock = new Object();

    // 模拟的消息内容
    private String message;

    public MessagePrinter(String message) {
        this.message = message;
    }

    // 打印消息的方法，只有打印部分需要同步
    public void printMessage() {
        synchronized(lock) { // 使用成员变量lock作为锁
            // 下面这行代码是需要同步的操作，以保证消息完整打印
            try {
                System.out.println(Thread.currentThread().getName() + "进入了");
                Thread.sleep(1);
                System.out.println(Thread.currentThread().getName() +"打印了："+message);
                System.out.println(Thread.currentThread().getName() + "退出了");
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        System.out.println(Thread.currentThread().getName()+"进行非同步操作呢");
        // 可以添加更多非同步的操作
    }

    public static void main(String[] args) {
        MessagePrinter printer1 = new MessagePrinter("Hello, World!");
        MessagePrinter printer2 = new MessagePrinter("Hello, World!");
        MessagePrinter printer3 = new MessagePrinter("Hello, World!");

        // 创建多个线程来调用printMessage方法
        Thread t1 = new Thread(printer1::printMessage);
        Thread t2 = new Thread(printer2::printMessage);
        Thread t3 = new Thread(printer3::printMessage);

        // 启动线程
        t1.start();
        t2.start();
        t3.start();
    }
}