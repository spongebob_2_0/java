package com.scholar.juc.thread;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Counter counter1 = new Counter();
        Counter counter2 = new Counter();

        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                counter1.increase();
            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                counter2.increase();
            }
        });

        t1.start();
        t2.start();

        t1.join();
        t2.join();

        System.out.println(counter1.getCount()); // 输出1000
        System.out.println(counter2.getCount()); // 输出1000
    }
}

class Counter {
    private int count = 0;

    public synchronized void increase() {
        count++;
    }

    public int getCount() {
        return count;
    }
}