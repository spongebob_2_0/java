package com.scholar.juc.thread;

public class BankAccount {
    private int balance = 0; // 账户余额

    // 存款方法
    public void deposit(int amount) {
        synchronized(this) { // 使用当前实例对象作为锁
            balance += amount; // 增加余额
            System.out.println("存款：" + amount + "，当前余额：" + balance);
        }
    }

    // 取款方法
    public void withdraw(int amount) {
        synchronized(this) { // 使用当前实例对象作为锁
            if (balance >= amount) {
                balance -= amount; // 减少余额
                System.out.println("取款：" + amount + "，当前余额：" + balance);
            } else {
                System.out.println("余额不足，取款失败");
            }
        }
    }

    public static void main(String[] args) {

        // 创建一个存款线程
        Thread depositThread = new Thread(() -> {
            BankAccount account = new BankAccount();
            account.deposit(100);
        });

        // 创建一个取款线程
        Thread withdrawThread = new Thread(() -> {
            BankAccount account = new BankAccount();
            account.withdraw(50);
        });

        depositThread.start(); // 启动存款线程
        withdrawThread.start(); // 启动取款线程
    }
}