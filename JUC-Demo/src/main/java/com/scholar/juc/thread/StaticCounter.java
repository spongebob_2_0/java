package com.scholar.juc.thread;

public class StaticCounter {
    private static  int count = 0; // 定义一个静态变量count，用于计数

    // 定义一个静态同步方法increase，每次调用时count增加1
    public  synchronized static void increase() {
        count++;
    }

    // 获取当前的count值
    public  static  int getCount() {
        return count;
    }

    public static void main(String[] args) throws InterruptedException {
        StaticCounter staticCounter = new StaticCounter(); // 每个线程创建一个实例

        Thread[] threads = new Thread[100]; // 创建一个线程数组，总共100个线程
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Thread(() -> {
                for (int j = 0; j < 1000; j++) {
                    staticCounter.increase(); // 每个线程调用increase方法1000次
                }
            });
            threads[i].start(); // 启动线程
        }

        for (Thread thread : threads) {
            thread.join(); // 等待所有线程执行完毕
        }

        // 所有线程执行完毕后，打印最终的count值
        System.out.println("Final count is: " + StaticCounter.getCount()); // 应该输出100000
    }
}