/**
 * Created with IntelliJ IDEA.
 * Description:猜数字小游戏
 * User: W
 * Date: 2022-10-28
 * Time: 0:26
 */
import java.util.Scanner;
import java.util.Random;
public class test2 {
    public static void main(String[] args) {
        Scanner scan =new Scanner(System.in);
        Random random =new Random();
        int rand =random.nextInt(100);  //(100) 括号中的数字表示生成随机数的范围
        while(true){
            System.out.println("请输入一个数字:");
            int num = scan.nextInt();
            if(num>rand){
                System.out.println("猜大了");
            }else if(num<rand){
                System.out.println("猜小了");
            }else{
                System.out.println("恭喜你,猜对了");
                break;
            }
        }
        System.out.println("游戏结束");
    }
}
