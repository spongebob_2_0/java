/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-10-28
 * Time: 2:01
 */

import java.util.*;

//public class test4 {
//    public static void main(String[] args) {
//        Scanner scanner =new Scanner(System.in);
//        int n =scanner.nextInt();
//        int count =0;
//        for(int i=0;i<32;i++){
//            if(((n>>i) & 1) ==1){
//                count++;
//            }
//        }
//        System.out.println("二进制中1的个数为"+count);
//    }
//}


//方法2
public class test4{
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int n = scanner.nextInt();
        int count =0;
        while(n!=0){
            n = n&(n-1);
            count++;
        }
        System.out.println("count:"+count);
    }
}
