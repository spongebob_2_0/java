/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-10-29
 * Time: 10:55
 */

import javax.crypto.spec.PSource;
import java.util.*;

public class test7 {
    public static int fabonaci(int n){
        if(n==1 || n==2){
            return 1;
        }
        return fabonaci(n-1)+fabonaci(n-2);
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int ret = fabonaci(n);
        System.out.println(ret);
    }
}
