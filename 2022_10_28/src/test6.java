/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-10-28
 * Time: 19:39
 */

import java.util.*;

public class test6 {
    public static int fac(int n){
        if(n==1){
            return 1;
        }
        return n*fac(n-1);
    }
    public static void func(int n){
        if(n>9){
            func(n/10);
        }
        System.out.println(n%10);
    }


    public static int add(int n){
       if(n==1){
           return 1;
       }
        return n+add(n-1);
    }
    public static void main(String[] args) {
//        int ret = fac(5);
//        System.out.println(ret);
        Scanner scanner =new Scanner(System.in);
        int n = scanner.nextInt();
//        func(n);
        int ret = add(n);
        System.out.println(ret);
    }
}
