package test1;

import java.util.Scanner;

public class Main4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextInt()) {
            int n = in.nextInt();
            // 输出上半部分的菱形
            for (int i = 1; i <= n; i++) {
                // 输出每一行前面的空格
                for (int j = 1; j <= n - i; j++) {
                    System.out.print(" ");
                }
                // 输出每一行的星号
                for (int k = 1; k <= i; k++) {
                    System.out.print("* ");
                }
                System.out.println();
            }
            // 输出下半部分的菱形
            for (int i = 1; i <= n; i++) {
                // 输出每一行前面的空格
                for (int j = 1; j <= i-1; j++) {
                    System.out.print(" ");
                }
                // 输出每一行的星号
                for (int k = 1; k <= 2 * (n - i) - 1; k++) {
                    System.out.print("* ");
                }
                System.out.println();
            }
            // 输出空行
            System.out.println();
        }
    }
}
