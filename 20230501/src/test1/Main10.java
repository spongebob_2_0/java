package test1;

import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main10 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int n = in.nextInt();
            //总共3n行,用d来确定第一节*的位置
            int d = 3 * n - 1;
            //总计n轮循环
            for (int i = 1; i <= n; i++) {
                //打印第一部分
                for (int j = d - 1; j >= 0; j--) {
                    System.out.println((" "));
                }
                for (int k = 0; k < i; k++) {
                    System.out.println("*     ");
                    ;
                }
                System.out.println();
                //打印第二部分
                for (int j = d - 2; j >= 0; j--) {
                    System.out.println((" "));
                }
                for (int k = 0; k < i; k++) {
                    System.out.println(("* *   "));
                }
                System.out.println();
                //打印第三部分
                for (int j = d - 3; j >= 0; j--) {
                    System.out.println((" "));
                }
                for (int k = 0; k < i; k++) {
                    System.out.println(("* * * "));
                }
                System.out.println();
                //打印完一次循环之后行数减3进入下一次循环
                d = d - 3;
            }
            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= 3 * n; j++) {
                    if (j == 3 * n) {
                        System.out.print("* ");
                    } else {
                        System.out.print(" ");
                    }
                }
                System.out.println();
            }
        }
    }
}