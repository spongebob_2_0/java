/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-02
 * Time: 19:32
 */

import java.util.*;

public class Test1 {
    public static int func(int n){
        if(n==1){
            return 1;
        }
        return n*func(n-1);
    }
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int n = scanner.nextInt();
        int ret = func(n);
        System.out.println(ret);
    }
}
