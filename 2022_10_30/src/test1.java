/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-10-30
 * Time: 12:57
 */

import java.util.*;

public class test1 {
    public static void menu() {
        System.out.println("***************************");
        System.out.println("******       1.play    ****");
        System.out.println("******       0.exit    ****");
        System.out.println("***************************");
    }

    public static void game() {
        Random random = new Random();
        int n = random.nextInt(100);
        while (true) {
            System.out.print("请输入要猜的数字");
            Scanner scanner = new Scanner(System.in);
            int input = scanner.nextByte();
            if (input > n) {
                System.out.println("猜大了");
            } else if (input < n) {
                System.out.println("猜小了");
            } else {
                System.out.println("恭喜你,猜对了");
                break;
            }
        }
    }

    public static void main(String[] args) {
        int input = 0;
        do {
            menu();
            Scanner scanner = new Scanner(System.in);
            System.out.print("请选择:");
            input = scanner.nextByte();
            switch (input) {
                case 1:
                    game();
                    break;
                case 0:
                    System.out.println("退出游戏");
                    break;
                default:
                    System.out.println("选择错误,重新选择!");
                    break;
            }
        } while (input != 0);
    }
}
