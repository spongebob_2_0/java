import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
 
@WebServlet("/redirect")
public class RediractServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //实现重定向，让浏览器自动跳转到百度浏览器
        resp.setStatus(302);
        resp.setHeader("Location","https://www.baidu.com");
        //另一种更简单的重定向写法
        //resp.sendRedirect("https://www.baidu.com");
    }
}
 