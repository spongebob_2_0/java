import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-05-25
 * Time: 13:15
 */

class Student {
    public String userId;
    public String classId;

    @Override
    public String toString() {
        return "Student{" +
                "userId='" + userId + '\'' +
                ", classId='" + classId + '\'' +
                '}';
    }
}

@WebServlet("/JsonParameter")
public class JsonParameterServelet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //通过这个方法处理body为json格式的数据
        //直接把req对象里的body完整的读出来
        // 1.先拿到body的长度，单位是字节
//        int length = req.getContentLength();
//        // 2.准备一个字节数组，来存放body的内容
//        byte[] buffer = new byte[length];
//        // 3.获取到InputStream对象
//        InputStream inputStream = req.getInputStream();
//        // 4. 读取数据，从InputStream对象中，读到数据，放到buffer这个数组中
//        inputStream.read(buffer);
//        //把这个数组构造成String,打印出来
//        String body =  new String(buffer,0,length,"utf8");
//        System.out.println("body = "+body);

        //使用json涉及到的核心对象
        ObjectMapper objectMapper = new ObjectMapper();
        //readValue 就是把一个 json 格式的字符串 转成java对象
        Student student =  objectMapper.readValue(req.getInputStream(),Student.class);
        resp.getWriter().write(student.toString());
        System.out.println(student.userId+", "+student.classId);;
    }
}
