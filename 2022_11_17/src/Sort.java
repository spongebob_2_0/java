/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-17
 * Time: 23:38
 */

import java.util.*;

public class Sort {
    public static void main(String[] args) {
        int[] arr = {4, 3, 5, 1, 7, 9, 3};
        Arrays.sort(arr);
        for (int i : arr) {
            System.out.println(i);
        }
    }
}