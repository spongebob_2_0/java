package operation;/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-19
 * Time: 14:35
 */

import book.Book;
import book.BookList;

import java.util.*;

public class DelOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        Scanner scanner =new Scanner(System.in);
        System.out.println("删除图书");
        System.out.println("请输入要删除的图书的名称");
        String name = scanner.next();
        int n = bookList.getUsedSize();
        for (int i = 0; i < n; i++) {
            Book book = bookList.getBook(i);
            if(book.getName().equals(name)){
                for (int j = i; j < n-1; j++) {
                    bookList.setBook(j,bookList.getBook(j+1));
                }
                bookList.setUsedSize(n-1);
                System.out.println("图书删除成功");
                return; //如果找到了,就直接return出去
            }
        }
        System.out.println("抱歉,没有找到你需要删除的书!");
    }
}
