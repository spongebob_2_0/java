package operation;/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-19
 * Time: 14:33
 */

import book.Book;
import book.BookList;

import java.util.*;

public class AddOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        Scanner scanner =new Scanner(System.in);
        System.out.println("新增图书");
        //增加的图书只要放到当前数组的usedSize位置就可以添加成功
        System.out.println("请输入图书的名字:");
        String name = scanner.nextLine();
        System.out.println("请输入图书的作者:");
        String author = scanner.nextLine();
        System.out.println("请输入图书的价格:");
        double price = scanner.nextDouble();
        System.out.println("请输入图书的类型:");
        String type = scanner.next();
        Book book =new Book(name,author,price,type);
        int usedSize = bookList.getUsedSize();
        bookList.setBook(usedSize,book);
        bookList.setUsedSize(usedSize+1);
        System.out.println("图书添加成功");
    }
}
