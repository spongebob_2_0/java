package operation;/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-19
 * Time: 14:36
 */

import book.BookList;

import java.util.*;

public class ShowOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("显示所有图书");
        for (int i = 0; i < bookList.getUsedSize(); i++) {
            System.out.println(bookList.getBook(i));
        }
    }
}
