package operation;/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-19
 * Time: 14:38
 */

import book.Book;
import book.BookList;

import java.util.*;

public class FindOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        Scanner scanner =new Scanner(System.in);
        System.out.println("查找图书");
        System.out.println("请输入图书的名称");
        String name = scanner.next();
        for (int i = 0; i < bookList.getUsedSize(); i++) {
            Book book = bookList.getBook(i);
            if(book.getName().equals(name)){
                System.out.println("找到该图书!");
                System.out.println(book);
                return; //如果找到了,就直接return出去
            }
        }
        System.out.println("没有找到该图书!");
    }
}
