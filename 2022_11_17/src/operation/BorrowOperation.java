package operation;/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-19
 * Time: 14:35
 */

import book.Book;
import book.BookList;

import java.util.*;

public class BorrowOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        Scanner scanner =new Scanner(System.in);
        System.out.println("借阅图书");
        System.out.println("请输入图书的名称");
        String name = scanner.next();
        for (int i = 0; i < bookList.getUsedSize(); i++) {
            Book book = bookList.getBook(i);
            if(book.getName().equals(name) ){
                if(!book.isBorrowed()){    //如果找到该书切该书没有被借,那就借阅成功
                    book.setBorrowed(true);
                    System.out.println("图书借阅成功");
                }else{
                    System.out.println("该图书已被借阅");
                }
                return; //如果找到了,就直接return出去
            }
        }
        System.out.println("该图书不存在!");   //走到这里说明这本书不存在
    }
}
