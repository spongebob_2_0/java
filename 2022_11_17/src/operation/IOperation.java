package operation;/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-19
 * Time: 14:02
 */

import book.BookList;

import java.util.*;

public interface IOperation {
    public abstract void work(BookList bookList);  //接口中有一个工作的抽象方法,传了一个书架的对象过去
}
