package operation;/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-19
 * Time: 14:38
 */

import book.Book;
import book.BookList;

import java.util.*;

public class ReturnOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("归还图书");
        System.out.println("请输入需要归还图书的名称");
        String name = scanner.next();
        for (int i = 0; i < bookList.getUsedSize(); i++) {
            Book book = bookList.getBook(i);
            if(book.getName().equals(name)){
                if(book.isBorrowed()){
                    book.setBorrowed(false);
                    System.out.println("图书归还成功");//如果找到该书,且该书已被借阅,那就归还成功
                }else{
                    System.out.println("该书未被借阅");
                }
                return; //如果找到了,就直接return出去
            }
        }
        System.out.println("该图书不存在!");  //走到这里说明这本书不存在
    }
}
