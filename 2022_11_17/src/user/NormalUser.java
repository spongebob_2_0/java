package user;/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-19
 * Time: 14:02
 */

import operation.*;

import java.util.*;

public class NormalUser extends User{
    public NormalUser(String name) {
        super(name);
        this.iOperations =new IOperation[]{
                new ExitOperation(),
                new FindOperation(),
                new BorrowOperation(),
                new ReturnOperation()
        };
    }

    @Override
    public int menu() {
        System.out.println("==========================================");
        System.out.println("尊贵的用户-"+this.name+",欢迎使用本图书管理系统");
        System.out.println("1.查找图书");
        System.out.println("2.借阅图书");
        System.out.println("3.归还图书");
        System.out.println("0.退出系统");
        System.out.println("==========================================");
        System.out.println("请输入操作:");
        Scanner scanner =new Scanner(System.in);
        int choice = scanner.nextInt();
        return choice;
    }
}
