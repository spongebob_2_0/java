package user;/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-19
 * Time: 14:03
 */

import book.BookList;
import operation.IOperation;

public abstract class User {
    protected String name;   //使用者的姓名
    IOperation[] iOperations;  //定义了一个接口数组,用于存放接口的实现类对象;

    //构造方法

    public User(String name) {
        this.name = name;
    }

    //定义一个菜单的抽象方法
    public abstract int menu();

    public void doWork(int choice, BookList bookList){
        iOperations[choice].work(bookList);   //用接口数组里面的第choice个对象去调用实现类中工作的方法,同时把书架这个对象传过去
    }
}
