package book;/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-19
 * Time: 14:01
 */

import java.util.*;

public class BookList {
    private Book[] books =new Book[10];  // 声明了一个可以存放10本书的数组
    private int usedSize;  //默认存放了0本书

    //无参构造方法,用于初始化
    public BookList(){
        books[0] =new Book("三国演义","罗贯中",88,"小说");
        books[1] =new Book("java编程思想","陈昊鹏",108,"小说");
        books[2] =new Book("西游记","罗贯中",95,"小说");
        usedSize =3;  //表示现在书架存了三本书
    }

    //对应的get/set方法

    public Book getBook(int pos) {   //取到pos位置的那本书
        return this.books[pos];
    }

    public Book setBook(int pos,Book book) {    //将pos位置上的书置为book
        return this.books[pos] =book;
    }

    public int getUsedSize() {
        return usedSize;
    }

    public void setUsedSize(int usedSize) {
        this.usedSize = usedSize;
    }
}
