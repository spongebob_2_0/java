/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-19
 * Time: 14:53
 */

import book.BookList;
import user.AndminUser;
import user.NormalUser;
import user.User;

import java.util.*;

public class Main {
    public static User login(){    //返回类型是父类类名,意味着可以返回改类的对象(包括子类对象)
        Scanner scanner =new Scanner(System.in);
        System.out.println("请输入您的姓名:");
        String name =scanner.nextLine();
        System.out.println("请输入您的身份:1->管理员 0->普通用户");
        int choice = scanner.nextInt();
        if(choice ==1){
            return new AndminUser(name);    //返回管理员类的对象(子类)
        }else{
            return new NormalUser(name);    //返回普通用户类的对象(子类)
        }
    }

    public static void main(String[] args) {
        BookList bookList =new BookList();  //创建了一个书架的对象
        User user =login();  //向上转型(父类引用指向子类对象)
        while (true){    //写成死循环,这样就可以不断的调用菜单,直到退出系统
            int choice = user.menu();    //多态的形式调用菜单这个方法,并返回一个返回值
            user.doWork(choice,bookList);  //多态的形式调用doWork这个方法,同时把返回值和书架这个对象传过去
        }
    }
}
