import beans.Student;
import beans.demo.bit.ClassController;
import beans.demo.bit.UserController;
import javafx.application.Application;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import service.ClassMate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-05
 * Time: 18:17
 */
public class App {
    public static void main(String[] args) {
        //1,得到Spring
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        //2.从spring容器中获取 Bean 对象 ,此处的"student" 必须和spring-config.xml配置中的Bean的id保持一致
        //getBean的返回类型为Object，所以强转一下,该行代码就是实现IOC思想的DI操作
//        Student student =(Student) context.getBean("student");
//        Student student =context.getBean("student",Student.class);
//        //3.使用Bean对象(非必须,也可以获取不用)
//        student.sayHi();

//        UserController userController =
//                context.getBean("userController",UserController.class);
//        userController.sayHi();

//        ClassMate classMate =
//                context.getBean("classMate",ClassMate.class);
//        classMate.sayHi();

        ClassController classController =
                context.getBean("classController",ClassController.class);
        classController.sayHi();
    }
}
