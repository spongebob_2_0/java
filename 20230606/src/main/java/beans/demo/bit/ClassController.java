package beans.demo.bit;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-06
 * Time: 12:35
 */
@Configuration
public class ClassController {
    public void sayHi() {
        System.out.println("我在包底下,我没有注解");
    }
}
