package beans.demo.bit;

import org.springframework.stereotype.Controller;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-06
 * Time: 12:27
 */
@Controller
public class UserController {
    public void sayHi() {
        System.out.println("do UserController .....");
    }
}
