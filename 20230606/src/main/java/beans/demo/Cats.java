package beans.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;

@Controller

public class Cats {

    @Bean
    public Cat cat1(){
        Cat cat = new Cat();
        cat.setName("糯米");
        cat.setColor("白色");
        return cat;
    }
    
    @Bean
    public Cat cat2(){
        Cat cat = new Cat();
        cat.setName("汤圆");
        cat.setColor("黑色");
        return cat;
    }
}
