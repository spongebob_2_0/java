package beans.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

@Controller
public class CatController {

    @Autowired
    @Qualifier(value = "cat1")
    private Cat cat;

    public void getCat(){
        System.out.println(cat);
    }
}
