package beans;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class Students {
    @Bean(name = "student1")
    public Student s1(){
        Student student = new Student();
        student.setName("小诗诗");
        student.setAge(18);
        return student;
    }
}
