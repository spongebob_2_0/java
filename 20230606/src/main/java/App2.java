import beans.Student;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-06
 * Time: 1:09
 */
public class App2 {
    public static void main(String[] args) {
//        //1.得到Spring上下文对象
//        BeanFactory beanFactory =new XmlBeanFactory(new ClassPathResource("spring-config.xml"));
//        //2.从Spring中获取 bean 对象
//        Student student =(Student) beanFactory.getBean("student");
//        //3,调用bean对象的方法
//        student.sayHi();
    }
}
