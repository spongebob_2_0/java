/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-07
 * Time: 14:34
 */

import java.util.*;

public class Test1 {
    public static void main(String[] array){
        int[] arr ={2,3,2,3,6,6,5,8,8};
        int ret = func(arr);
        System.out.println(ret);
    }
    public static int func(int[] arr ){
        int ret =arr[0];
        for(int i =1;i<arr.length;i++){
            ret ^=arr[i];
        }
        return ret;
    }
}
