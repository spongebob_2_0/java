import controller.BeanLifeController;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");
        // 根据 id 获取 bean 对象
        BeanLifeController controller =
                context.getBean("beanLife", BeanLifeController.class);
        // 使用 bean
        controller.use();
        // 销毁 bean
        context.destroy();
    }
}