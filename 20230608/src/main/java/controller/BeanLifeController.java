package controller;

import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanLifeController implements BeanNameAware, BeanPostProcessor {
    @Override
    public void setBeanName(String s) {
        System.out.println("执行各种通知：" + s);
    }
 
    /**
     * xml 中 init-method 指定的前置方法
     */
    public void initMethod() {
        System.out.println("执行 init-method 前置方法");
    }
 
    /**
     * 改用注解后的前置方法
     */
    @PostConstruct
    public void PostConstruct() {
        System.out.println("执行 PostConstruct 前置方法");
    }

    /**
     * 销毁前执行方法
     */
    @PreDestroy
    public void PreDestroy() {
        System.out.println("执行 PreDestroy 销毁方法");
    }
 
    public void use() {
        System.out.println("使用 bean - 执行 use 方法");
    }
}