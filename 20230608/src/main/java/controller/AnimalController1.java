package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

//@Controller
public class AnimalController1 {

    @Autowired
    private Animal animal1;

    public void getAnimal(){
        System.out.println("修改前的数据 = "+animal1);
        System.out.println("=====================");
        Animal animal = this.animal1;
        animal.setName("加菲猫");
        System.out.println("A修改后拿到的数据 = "+animal);
    }
}
