package controller;

import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

public class Animals {

    @Bean
    @Scope("prototype")
    public Animal animal(){
        Animal animal = new Animal();
        animal.setName("老虎");
        return animal;
    }
}
