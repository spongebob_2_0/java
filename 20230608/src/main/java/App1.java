import controller.AnimalController1;
import controller.AnimalController2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App1 {
    public static void main(String[] args) {
        //得到Spring上下文的对象
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        //执行A用户的代码
        AnimalController1 animalController1 = context.getBean("animalController1",AnimalController1.class);
        animalController1.getAnimal();
        //执行B用户的代码
        AnimalController2 animalController2 = context.getBean("animalController2",AnimalController2.class);
        animalController2.getAnimal();
    }
}
