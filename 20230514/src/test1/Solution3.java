package test1;


class ListNode {
    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}

public class Solution3 {
    public ListNode removeNthFromEnd(ListNode head, int n) {
        if(head == null || n <= 0) {
            return null;
        }
        ListNode PHead =new ListNode(-1);
        PHead.next =head;
        ListNode slow = PHead;
        ListNode fast = PHead;
        while(n != 0) {
            fast =fast.next;
            n--;
        }
        if(fast == null) {
            return null;
        }
        while(fast.next != null) {
            fast = fast.next;
            slow = slow.next;
        }
        slow.next = slow.next.next;
        return PHead.next;
    }
}