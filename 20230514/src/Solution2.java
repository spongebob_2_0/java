import java.util.ArrayDeque;
import java.util.Deque;

class Solution2 {
    public int evalRPN(String[] tokens) {
        Deque<Integer> stack =new ArrayDeque<>();
        for(String x: tokens) {
            
                if(x.equals("+")) {
                    int num1 = stack.pop();
                    int num2 = stack.pop();
                    stack.push(num2+num1);
                }else if(x.equals("-")) {
                    int num1 = stack.pop();
                    int num2 = stack.pop();
                    stack.push(num2-num1);
                }else if(x.equals("*")) {
                    int num1 = stack.pop();
                    int num2 = stack.pop();
                    stack.push(num2*num1);
                }else if(x.equals("/")){
                    int num1 = stack.pop();
                    int num2 = stack.pop();
                    stack.push(num2/num1);
                }else {
                    stack.push(Integer.valueOf(x));
                }
        }
        return stack.pop();
    }

}