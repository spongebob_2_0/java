package com.example;


class Article {
	public final String title;

	public final String url;

	public Article(String title, String url) {
		this.title = title;
		this.url = url;
	}
}

