package demo0;

import java.io.PrintStream;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-30
 * Time: 17:43
 */
public class Main {

    public static void printString(Printable p){
        p.print("测试打印");
    }

    public static void main(String[] args) {

        printString(s -> {
            System.out.println(s);
        });

        PrintStream out = System.out;
        printString(out::println);
    }
}

