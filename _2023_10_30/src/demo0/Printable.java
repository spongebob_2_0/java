package demo0;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-30
 * Time: 17:49
 */
@FunctionalInterface
public interface Printable {
    void print(String s);
}
