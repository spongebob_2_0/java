package demo1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-30
 * Time: 18:28
 */
public class Maiin {
    public static void main1(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("张无忌");
        list.add("周芷若");
        list.add("赵敏");
        list.add("张良");
        list.add("张三丰");

        List<String> listA = new ArrayList<>();
        for(String s : listA) {
            if(s.startsWith("张")) {
                listA.add(s);
            }
        }


        list.stream()
                .filter(name -> name.startsWith("张"))
                .filter(name -> name.length() == 3)
                .forEach(name -> System.out.println(name));
    }
    public static void main(String[] args) {
        Map<String,Object> map = new HashMap<>();
        map.put("name","小诗诗");
        map.put("age",18);
        map.put("id",1);
        map.put("sex","女");
        Stream<String> stream = map.keySet().stream(); // key
        Stream<Object> stream1 = map.values().stream(); // value
        Stream<Map.Entry<String, Object>> stream2 = map.entrySet().stream(); // entry
        stream.forEach(System.out::println);
        stream1.forEach(System.out::println);
        stream2.forEach(System.out::println);
    }
}
