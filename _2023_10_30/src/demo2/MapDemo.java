package demo2;

import java.util.*;

public class MapDemo {
    public static void main(String[] args) {
        Map<String , Integer> map = new HashMap<>();
        map.put("小飞飞",1);
        map.put("小甜甜",18);
        map.put("小诗诗",13);
        map.put("小沐沐",1);
        map.put("阿宝",1);

//        Set<String> keys = map.keySet();
//        for(String s: keys) {
//            System.out.println(s);
//        }
//
//        Collection<Integer> values = map.values();
//        for(Integer i: values) {
//            System.out.println(i);
//        }

        Set<Map.Entry<String, Integer>> entries = map.entrySet();
//        for(Map.Entry<String,Integer> entry :entries) {
//            System.out.println(entry.getKey()+entry.getValue());
//        }


        List<Integer> list = Arrays.asList(new Integer[]{1,2,3,4});
        list.forEach((s) -> {
            System.out.print(s+" ");
        });
        System.out.println();
        System.out.println("=================");
        list.stream().forEach(s -> {
            System.out.print(s + " ");
        });

//        Iterator<Integer> iterator = list.iterator();
//        while (iterator.hasNext()) {
//            Integer next = iterator.next();
//            System.out.println(next);
//        }
//        for(Map.Entry<String, Integer> key: map.entrySet()) {
//            System.out.println(key);
//        }

//        map.forEach((k,v) -> {
//            System.out.println(v);
//        });
    }
}
