package demo2;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-11-03
 * Time: 12:21
 */
public class Main3 {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder("Hello, Java!");
        // 设置指定索引处的字符
        sb.setCharAt(7, 'W'); // "Hello, Wava!"
        System.out.println(sb);
        // 获取指定索引处的字符
        char character = sb.charAt(6); // ' '
        System.out.println(character);
        // 复制字符到目标字符数组
        char[] destination = new char[5];

//        Arrays.asList(destination).forEach(s -> {
//            System.out.println(s);
//        });
        sb.getChars(7, 12, destination, 0); // destination = {'W', 'a', 'v', 'a', '!'}
        for(char c: destination) {
            System.out.println(c);
        }
    }
}
