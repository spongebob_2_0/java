package demo2;

import java.util.*;

public class Main {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            List<String> list = Arrays.asList(new String[]{"A","B","C","D","F"});
            String s = scanner.nextLine();
            String[] newArray = s.split(" ");
            int sum = 0;
            double avg = 0;
            for(int i = 0; i < newArray.length; i++) {
                if(!list.contains(newArray[i])) {
                    System.out.println("Unknown");
                    return;
                }
                int tmp = func(newArray[i]);
                sum += tmp;
            }
            avg += sum * 1.0 / newArray.length;
            System.out.printf("%.2f\n", avg);
        }
    }
    private static int func(String num) {
        switch(num) {
            case "A":
                return 4;
            case "B":
                return 3;
            case "D":
                return 2;
            case "E":
                return 1;
            case "F":
                return 0;
        }
        return -1;
    }
}