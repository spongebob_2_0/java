package com.wym.redis.redisdemo2.controller;

import com.wym.redis.redisdemo2.util.JedisPoolUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

import java.util.List;
import java.util.Random;

@RestController
public class SecKill {

    // 定义一个处理秒杀请求的接口
    @RequestMapping("/secKill")
    public boolean secKill(){
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        // 随机生成一个6位数字的用户ID，仅作示例
        for (int i = 0; i < 6; i++) {
            int code = random.nextInt(10);
            sb.append(code);
        }

        // 执行秒杀操作
        return doSecKill(sb.toString(), "123");
    }
    
    // 执行秒杀的具体逻辑
    public boolean doSecKill(String uid,String productId){
        
        // 参数检查
        if(uid == null || productId == null){
            return false;
        }
        
        // 获取Redis连接
        Jedis jedis = JedisPoolUtils.getJedisPoolInstance().getResource();
        // 定义秒杀商品的库存键和参与秒杀的用户集合键
        String kcKey = "sk:" + productId + ":qt"; // 库存Key
        String userKey = "sk:" + productId + ":user"; // 用户Key
        
        // 使用watch命令对库存进行监视
        jedis.watch(kcKey);
        
        // 获取商品库存
        String kc = jedis.get(kcKey);
        // 如果库存不存在，表示秒杀尚未开始
        if(kc == null){
            System.out.println("秒杀没有开始");
            jedis.close();
            return false;
        }
        
        // 判断库存是否已经卖完
        if(Integer.parseInt(kc) <= 0){
            System.out.println("秒杀已经结束");
            jedis.close();
            return false;
        }
        
        // 使用事务处理秒杀逻辑
        Transaction multi = jedis.multi(); // 开启事务
        multi.decr(kcKey); // 减少库存
        multi.sadd(userKey, uid); // 将用户添加到秒杀成功的集合中
        List<Object> exec = multi.exec(); // 提交事务
        // 判断事务是否执行成功
        if(exec == null || exec.size() == 0){
            System.out.println("秒杀失败");
            jedis.close();
            return false;
        }

        System.out.println("秒杀成功");
        jedis.close();
        return true;
    }
}
