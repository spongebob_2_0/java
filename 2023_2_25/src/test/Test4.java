package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-02-25
 * Time: 21:38
 */
public class Test4 {
    // 归并排序---递归
    public static void mergeSort(int[] array){
        mergeSortFunc(array,0,array.length-1);
    }

    private static void mergeSortFunc(int[] array, int left, int right) {
        if(left>=right) {
            return;
        }
        int mid =(left+right)/2;
        mergeSortFunc(array,left,mid);
        mergeSortFunc(array,mid+1,right);
        merge(array,left,right,mid);  //合并
    }

    private static void merge(int[] array, int start, int end, int mid) {
        int s1 =start;
        int s2 =mid+1;
        int[] tmp =new int[end-start+1];
        int k =0;//tmp数组的下标
        while(s1<=mid && s2 <=end) {
            if(array[s1] <= array[s2]) {
                tmp[k++] =array[s1++];
            }else {
                tmp[k++] =array[s2++];
            }
        }
        while(s1 <=mid) {
            tmp[k++] =array[s1++];
        }
        while (s2 <=end) {
            tmp[k++] =array[s2++];
        }
        for(int i =0; i<tmp.length; i++) {
            array[i+start] =tmp[i];
        }
    }

    // 归并排序---非递归
    public static void mergeSort2(int[] array){
        int gap =1;
        while(gap < array.length) {
            for(int i =0; i< array.length; i+=gap*2) {
                int left =i;
                int mid =left+gap-1;
                if(mid >=array.length) {
                    mid =array.length-1;
                }
                int right=mid+gap;
                if(right>=array.length) {
                    right =array.length-1;
                }
                merge(array,left,right,mid);
            }
            gap *=2;
        }
    }

// 计数排序
    public static void countSort(int[] array){
        int max =array[0];
        int min =array[0];
        for(int i =0; i < array.length; i++) {
            if(array[i] < min) {
                min =array[i];
            }
            if(array[i] > max) {
                max =array[i];
            }
        }
        int len =max-min+1;
        int[] count =new int[len];
        for(int i =0; i < array.length; i++) {
            count[array[i]-min]++;
        }
        int index =0;
        for(int i =0; i < count.length; i++) {
            while(count[i] > 0) {
                array[index] =i+min;
                index++;
                count[i]--;
            }
        }
    }

    public static void main(String[] args) {
        int[] array ={42,25,43,31,1,13,41,30,20,25};
//        mergeSort2(array);
        countSort(array);
        for(int x: array) {
            System.out.print(x+" ");
        }
    }
}
