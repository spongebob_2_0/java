package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-02-25
 * Time: 16:07
 */
public class Test3 {
    // 冒泡排序
    public static void bubbleSort(int[] array){
        for(int i =0; i < array.length-1; i++) {
            for(int j =0; j < array.length-1-i; j++) {
                if(array[j] > array[j+1]){
                    swap(array,j,j+1);
                }
            }
        }
    }

    private static void swap(int[] array, int j, int i) {
        int tmp =array[i];
        array[i] =array[j];
        array[j] =tmp;
    }

    // 快速排序
    public static void quickSort(int[] array){
        quick(array,0,array.length-1);
    }

    private static void quick(int[] array, int start, int end) {
        if(start >= end){
            return;
        }

        //性能优化,目的为了减少递归次数
        if(end-start+1 <=14) {
            insertSort(array,start,end);
            return;
        }
        //三数取中法
        int index = midTree(array,start,end);
        swap(array,index,start);
        int pivot =partition2(array,start,end);  //划分找基准值
        quick(array,start,pivot-1);
        quick(array,pivot+1,end);
    }
    //对区间进行插入排序
    private static void insertSort(int[] array, int left, int right) {
        for(int i =left; i< right; i++) {
            int tmp =array[i];
            int j =i-1;
            for(; j >= left; j--) {
                if(array[j] > tmp) {
                    array[j+1] = array[j];
                }else {
                    break;
                }
            }
            array[j+1] =tmp;
        }
    }

    //三数取中法
    private static int midTree(int[] array, int left, int right) {
        int mid =(left+right)/2;
        if(array[left]<array[right]) {
            if(array[mid]<array[left]) {
                return left;
            }else if(array[mid]>array[right]) {
                return right;
            }else {
                return mid;
            }
        }else {
            if(array[mid] <array[right]) {
                return right;
            }else if(array[mid]<array[left]) {
                return left;
            }else {
                return mid;
            }
        }
    }

    //挖坑法
    private static int partition(int[] array, int left, int right) {
        int tmp =array[left];
        while (left < right) {
            while (left<right && array[right]>=tmp){
                right--;
            }
            array[left] =array[right];
            while (left<right && array[left]<=array[right]) {
                left++;
            }
            array[right] =array[left];
        }
        array[left] =tmp;
        return left;
    }
    //Hoare法
    private static int partition2(int[] array,int left,int right) {
        int tmp =array[left];
        int i =left;
        while(left<right) {
            while (left<right && array[right]>=tmp) {
                right--;
            }
            while (left<right && array[left]<=tmp) {
                left++;
            }
            swap(array,left,right);
        }
        swap(array,left,i);
        return left;
    }

    public static void main(String[] args) {
        int[] array ={42,25,43,31,1,13,41,30,20,25};
//        bubbleSort(array);
        quickSort(array);
        for(int x: array) {
            System.out.print(x+" ");
        }
    }
}
