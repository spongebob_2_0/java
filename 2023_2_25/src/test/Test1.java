package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-02-25
 * Time: 13:51
 */
public class Test1 {
    //直接插入排序
    public static void insertSort(int[] array) {
        for(int i =1; i< array.length; i++) {
            int tmp =array[i];
            int j =i-1;
            for(; j >= 0; j--) {
                if(array[j] > tmp) {
                    array[j+1] = array[j];
                }else {
                    break;
                }
            }
            array[j+1] =tmp;
        }
    }
    //希尔排序
    public static void shellSort(int[] array) {
        int gap =array.length;
        while(gap > 1) {
            shell(array,gap);
            gap /= 2;
        }
        shell(array,1);
    }
    //插入排序
    private static void shell(int[] array, int gap) {
        for(int i =gap; i< array.length; i++) {
            int tmp  =array[i];
            int j =i-gap;
            for(; j>= 0; j -=gap) {
                if(array[j] > tmp) {
                    array[j+gap]  =array[j];
                }else {
                    break;
                }
            }
            array[j+gap] =tmp;
        }
    }

    public static void main(String[] args) {
        int[] array ={42,25,43,31,1,13,41,30,20,25};
//        insertSort(array);
        shellSort(array);
        for(int x: array){
            System.out.print(x+" ");
        }
    }
}
