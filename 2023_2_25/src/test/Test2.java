package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-02-25
 * Time: 14:33
 */
public class Test2 {
    // 选择排序
    public static void selectSort(int[] array){
        for(int i =0; i< array.length; i++) {
            int minIndex =i;
            int j =i+1;
            for(; j<array.length; j++) {
                if(array[j] < array[minIndex]) {
                    minIndex =j;
                }
            }
            swap(array,i,minIndex);
        }
    }

    private static void swap(int[] array, int i, int minIndex) {
        int tmp =array[i];
        array[i] =array[minIndex];
        array[minIndex] = tmp;
    }


    // 堆排序
    public static void heapSort(int[] array){
        createBigHeap(array);
        int end =array.length-1;
        while(end >0) {
            swap(array,0,end);
            shiftDown(array,0,end);
            end--;
        }
    }

    private static void createBigHeap(int[] array) {
        for(int parent =(array.length-1-1)/2; parent>=0; parent--) {
            shiftDown(array,parent,array.length);
        }
    }

    private static void shiftDown(int[] array, int parent, int len) {
        int child =2*parent+1;
        while (child < len) {
            if(child+1<len && array[child] < array[child+1]) {
                child++;
            }
            if(array[child] > array[parent]) {
                swap(array,child,parent);
                parent =child;
                child =2*parent+1;
            }else {
                break;
            }
        }
    }

    public static void main(String[] args) {
        int[] array ={42,25,43,31,1,13,41,30,20,25};
//        selectSort(array);
        heapSort(array);
        for(int x: array){
            System.out.print(x+" ");
        }
    }
}
