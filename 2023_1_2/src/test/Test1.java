package test;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-01-02
 * Time: 3:20
 */
public class Test1 {
    public static String func(String str){
        StringBuilder sb =new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char ch =str.charAt(i);
            if(!sb.toString().contains(ch+"")){
                sb.append(ch);
            }
        }
        return sb.toString();
    }
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String str = scanner.nextLine();
            String ret = func(str);
            System.out.println(ret);
        }
    }
}
