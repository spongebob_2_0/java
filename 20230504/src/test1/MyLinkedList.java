package test1;

class MyLinkedList {
    class ListNode {
        int val;
        ListNode next;
        public ListNode (){};
        public ListNode(int val) {
            this.val =val;
        }
    }
    ListNode pHead;
    int usedSize;
    public MyLinkedList() {
        pHead =new ListNode(0);
    }

    public int get(int index) {
        if(index < 0 || index > usedSize -1) {
            return -1;
        }
        ListNode cur =pHead.next;
        while(index != 0) {
            cur =cur.next;
            index--;
        }
        return cur.val;
    }

    public void addAtHead(int val) {
        ListNode node =new ListNode(val);
        node.next =pHead.next;
        pHead.next =node;
        usedSize++;
    }

    public void addAtTail(int val) {
        ListNode node = new ListNode(val);
        ListNode cur = pHead;
        while(cur.next != null) {
            cur = cur.next;
        }
        cur.next =node;
        usedSize++;
    }

    public void addAtIndex(int index, int val) {
        if(index < 0 || index > usedSize) {
            return;
        }
        if(index == 0) {
            addAtHead(val);
            return;
        }
        if(index == usedSize) {
            addAtTail(val);
            return;
        }
        ListNode cur =pHead;
        while(index != 0) {
            cur = cur.next;
            index--;
        }
        //此时cur就在要插入节点的前一个位置
        ListNode node =new ListNode(val);
        node.next = cur.next;
        cur.next = node;
        usedSize++;
    }

    public void deleteAtIndex(int index) {
        if(index < 0 || index > usedSize-1) {
            return;
        }
        ListNode cur = pHead;
        while(index != 0) {
            cur = cur.next;
            index--;
        }
        cur.next =cur.next.next;
        usedSize--;
    }
}

/**
 * Your MyLinkedList object will be instantiated and called as such:
 * MyLinkedList obj = new MyLinkedList();
 * int param_1 = obj.get(index);
 * obj.addAtHead(val);
 * obj.addAtTail(val);
 * obj.addAtIndex(index,val);
 * obj.deleteAtIndex(index);
 */