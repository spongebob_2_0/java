package test1;

class Solution {
    public int[][] generateMatrix(int n) {
        int start = 0;  //开始的位置
        int count = 1;  //填充的数
        int i =0;  //行
        int j =0;  //列
        int loap =0; //循环的圈数
        int[][] array =new int[n][n];
        while(loap++ < n/2) {
            for(j = start; j < n-loap; j++) {
                array[start][j] = count++;
            }
            for(i = start; i < n-loap; i++) {
                array[i][j] = count++;
            }
            for(; j >= loap; j--) {
                array[i][j] = count++;
            }
            for(; i >= loap; i--) {
                array[i][j] = count++;
            }
            start++;
        }
        if(n%2 == 1) {
            array[start][start] =count;
        }
        return array;
    }
}