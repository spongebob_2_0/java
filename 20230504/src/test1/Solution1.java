package test1;

class Solution1 {
    public int minSubArrayLen(int target, int[] nums) {
        int min =Integer.MAX_VALUE;
        for(int i = 0; i < nums.length; i++) {
            int sum = 0;
            int count = 0;
            for(int j = i+1; j < nums.length; j++) {
                sum += nums[j];
                count++;
                if(sum >= target) {
                    min = min > count?count:min;
                }
            }
        }
        return min == Integer.MAX_VALUE?0:min;
    }
}