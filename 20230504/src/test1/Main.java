package test1;

import java.util.*;
public class Main {
    private static boolean isLeapYear(int year) {
        return year % 400 == 0 || (year % 4 == 0 && year % 100 != 0);
    }
    private static int profitOfYear(int year) {
        return 2 * 31
               + 1 * 28
               + 1 * 31
                + 2 * 30
                + 1 * 31
                + 2 * 30
                + 1 * 31
                + 2 * 31
                + 2 * 30
                + 2 * 31
                + 1 * 30
                + 2 * 31
                + (isLeapYear(year) ? 1 : 0);
    }
    private static boolean isPrime(int month) {
        return month == 2 || month == 3 || month == 5 || month == 7 || month == 11;
    }
    private static int profitOfThisYear(int year, int month, int day) {
        int profit = 0;
        if (!isPrime(month)) {
            profit = day * 2;
        } else {
            profit = day;
        }
        while (--month > 0) {
            switch (month) {
            case 1: case 8: case 10: case 12:
            profit += 62;
            break;
            case 3: case 5: case 7:
            profit += 31;
            break;
            case 4: case 6: case 9:
            profit += 60;
            break;
            case 11:
            profit += 30;
            break;
            default: // case 2
            profit += (28 + (isLeapYear(year) ? 1 : 0));
            break;
            }
        }
    return profit;
    }
    public static void main(String[] args) {
        int year1, month1, day1, year2, month2, day2;
        int profit = 0;
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            year1 = scanner.nextInt();
            month1 = scanner.nextInt();
            day1 = scanner.nextInt();
            year2 = scanner.nextInt();
            month2 = scanner.nextInt();
            day2 = scanner.nextInt();
            profit = profitOfYear(year1) - profitOfThisYear(year1, month1, day1 - 1);
            profit += profitOfThisYear(year2, month2, day2);
            if (year1 == year2) {
                profit -= profitOfYear(year1);
            }
            for (int i = year1 + 1; i < year2; i++) {
                profit += profitOfYear(i);
            }
        System.out.println(profit);
        }
    }
}