/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-10-27
 * Time: 16:28
 */
import java.util.Scanner;
public class test13 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int m = scan.nextInt();
        int n = scan.nextInt();
        int max;
        max = m > n ? n : m;
        while (true) {
            if (m % max == 0 && n % max == 0) {
                System.out.printf("最大公约数是:%d\n", max);
                break;
            }
            max--;
        }
    }
}
