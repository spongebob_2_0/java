/**
 * Created with IntelliJ IDEA.
 * Description:判断某一年是否是闰年
 * User: W
 * Date: 2022-10-27
 * Time: 13:29
 */
import java.util.Scanner;
public class test4 {
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
       while(scan.hasNext()){
            int a = scan.nextInt();
            if((a%4 == 0 && a%100 != 0) || a%400 == 0) {
                System.out.println("a是闰年");
            }
            else{
                System.out.println("a不是闰年");
            }
        }
    }
}
