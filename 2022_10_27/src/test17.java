/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-10-27
 * Time: 18:45
 */
import java.util.Scanner;
import java.lang.String;
public class test17 {
    public static void main(String[] args) {
        int count =1;
        System.out.print("请输入密码:>");
        String password ="abc123";
        Scanner scanner =new Scanner(System.in);
        while(scanner.hasNext()){          //循环输入密码
            String str =scanner.next();
            if(0 == password.compareTo(str)){    //通过返回值是否为0判断密码是否相等
                System.out.println("登陆成功");
                break;                    //密码相等跳出循环
            }else if(count<3){
                System.out.println("输入错误,你还有"+(3-count)+"次机会");
                count++;                 //密码输入次数+1
            }else{
                break;                   //密码输错三次退出循环
            }
        }
    }
}
