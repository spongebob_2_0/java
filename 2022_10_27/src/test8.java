/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-10-27
 * Time: 14:32
 */
import java.util.Scanner;
public class test8 {
    public static void main(String[] args) {
        Scanner scan =new Scanner(System.in);
        while(scan.hasNext()){
            int age = scan.nextInt();
            if(age>0){
                if(age<18){
                    System.out.println("少年");
                }
                if(age>18 && age<28){
                    System.out.println("青年");
                }
                if(age>28 && age<55){
                    System.out.println("中年");
                }
                if(age>55){
                    System.out.println("老年");
                }
            }else{
                System.out.println("输入错误");
            }
        }
    }
}
