package test2;

public enum TestEnum {
    RED("red", 1), BLACK("black", 2), WHITE("white", 3), GREEN("green", 4);
    private String name;
    private int key;

    /**
     * 1、当枚举对象有参数后，需要提供相应的构造函数
     * 2、枚举的构造函数默认是私有的 这个一定要记住
     *
     * @param name
     * @param key
     */
    private TestEnum(String name, int key) {
        this.name = name;
        this.key = key;
    }

    public static TestEnum getEnumKey(int key) {
        for (TestEnum t : TestEnum.values()) {
            if (t.key == key) {
                return t;
            }
        }
        return null;
    }

    public static void main(String[] args) {
        System.out.println(getEnumKey(2));
    }
}
