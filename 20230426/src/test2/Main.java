package test2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
 
 
/**
 * 1. 将实际输入的文字 str2 放在集合 set 中
 * 2. for 循环遍历应该输入的文字 str1, 
 * 若集合 set 不包含某个字符，且线性表之前也未出现该字符，就输出打印
 * 3. 将步骤 2 中不包含的字符放入顺序表中,以便下一次判断
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
 
        String str1 = scanner.nextLine(); //应该输入的文字
        String str2 = scanner.nextLine(); //实际输入的文字
        out(str1,str2);
    }
 
    public static void out(String str1, String str2){
        Set<Character> set = new HashSet<>();
        ArrayList<Character> list = new ArrayList<>();
 
        for (char x:str2.toUpperCase().toCharArray()) {
            set.add(x); //1.
        }
 
        for(char x:str1.toUpperCase().toCharArray()){ //2.
            if(!set.contains(x) && !list.contains(x)){ 
                System.out.print(x);
            }
            list.add(x); //3.
        }
    }
}