package test1;

public enum enumDemo01 {
    ONE,TWE("twe",2),TREE;//枚举对象
 
    public String figure;
    public int ordinal;
    //枚举的构造方法默认是私有的
    enumDemo01(String figure, int ordinal) {
        this.figure = figure;
        this.ordinal = ordinal;
    }
    enumDemo01() {
    }
}