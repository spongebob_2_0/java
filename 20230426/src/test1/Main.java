package test1;
public class Main {
    public static void main(String[] args) throws ClassNotFoundException {
//        Person person = new Person();
        Class<?> p1 = Class.forName("test1.Person");
        try {
            Person person =(Person) p1.newInstance();
            System.out.println(person);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
//        Class<?> p2 = Person.class;
//        Class<?> p3 = person.getClass();
//        System.out.println(p1.equals(p2));
//        System.out.println(p1.equals(p3));
//        System.out.println(p2.equals(p3));
    }
}
