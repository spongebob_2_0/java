package test1;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class Demo01 {
    public static void reflectPrivateConstructor() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
            Class<?> c1 = Class.forName("test1.enumDemo01");
            Constructor<?> constructor =
                    c1.getDeclaredConstructor(String.class, int.class,String.class, int.class);
            constructor.setAccessible(true);
 
            enumDemo01 e1 = (enumDemo01)constructor.newInstance("xigua",66);
            System.out.println(e1);
    }
 
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        reflectPrivateConstructor();
    }
}