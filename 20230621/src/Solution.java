import java.util.*;


public class Solution {
    /**
     * 
     * @param prices int整型一维数组 
     * @return int整型
     */
    public int maxProfit (int[] prices) {
        int max=0;
        for(int i=0;i<prices.length-1;i++){
            for(int j=prices.length-1;j>i;j--)
                max=max>prices[j]-prices[i]?max:prices[j]-prices[i];
        }
        return max;
    }
}