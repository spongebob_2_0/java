package book;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-24
 * Time: 11:20
 */
public class BookList {
    private Book[] books =new Book[10]; //存放10本书
    private int usedSize;  //默认0本书

    public BookList() {
        books[0] =new Book("三国演义","罗贯中",88,"小说");
        books[1] =new Book("西游记","吴承恩",88,"小说");
        books[2] =new Book("水浒传","施耐庵",66,"小说");
        this.usedSize =3;
    }

    public Book getBook(int pos) {
        return this.books[pos];
    }

    public Book setBook(int pos,Book book) {
        return this.books[pos] =book;
    }

    public int getUsedSize() {
        return usedSize;
    }

    public void setUsedSize(int usedSize) {
        this.usedSize = usedSize;
    }
}
