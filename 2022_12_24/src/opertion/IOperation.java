package opertion;

import book.Book;
import book.BookList;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-24
 * Time: 11:21
 */
public interface IOperation {
    public abstract void work(BookList bookList);
}
