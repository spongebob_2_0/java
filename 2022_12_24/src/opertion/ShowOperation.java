package opertion;

import book.BookList;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-24
 * Time: 11:21
 */
public class ShowOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("显示所有图书!");
        for (int i = 0; i < bookList.getUsedSize(); i++) {
            System.out.println(bookList.getBook(i));
        }
    }
}
