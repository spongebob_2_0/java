package opertion;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-24
 * Time: 11:21
 */
public class AddOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("添加图书!");
        Scanner scanner =new Scanner(System.in);
        System.out.print("请输入书名:");
        String name = scanner.nextLine();
        System.out.print("请输入作者:");
        String author = scanner.nextLine();
        System.out.print("请输入价格:");
        int price = scanner.nextInt();
        System.out.print("请输入类型:");
        String type = scanner.next();
        Book book =new Book(name,author,price,type);
        //添加到usedSize位置上
        int n =bookList.getUsedSize();
        for (int i = 0; i < n; i++) {
            Book book1 =bookList.getBook(i);
            if(book1.getName().equals(name)){
                System.out.println("该书已存在!");
                return;
            }
        }
        bookList.setBook(n,book);
        bookList.setUsedSize(n+1);
        System.out.println("添加成功!");
    }
}
