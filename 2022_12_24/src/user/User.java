package user;

import book.BookList;
import opertion.IOperation;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-24
 * Time: 11:20
 */
public abstract class User {
    protected String name;
    protected IOperation[] iOperations;

    public User(String name) {
        this.name = name;
    }

    public abstract int menu();
    public void doWork(int choice, BookList bookList){
        this.iOperations[choice].work(bookList);
    }
}
