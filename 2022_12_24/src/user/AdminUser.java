package user;

import opertion.*;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-24
 * Time: 11:20
 */
public class AdminUser extends User{

    public AdminUser(String name) {
        super(name);
        this.iOperations =new IOperation[]{
                new ExitOperation(),
                new FindOperation(),
                new AddOperation(),
                new DelOperation(),
                new ShowOperation()
        };
    }

    @Override
    public int menu() {
        System.out.println("=========================");
        System.out.println("1.查找图书");
        System.out.println("2.添加图书");
        System.out.println("3.删除图书");
        System.out.println("4.显示图书");
        System.out.println("0.退出系统");
        System.out.println("请输入你的选择:");
        Scanner scanner =new Scanner(System.in);
        int choice =scanner.nextInt();
        return choice;
    }
}
