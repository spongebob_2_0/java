package test;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-24
 * Time: 18:23
 */
public class Test2 {
    public static int func(int[] array){
        int ret =array[0];
        for (int i = 1; i < array.length; i++) {
            ret ^=array[i];
        }
        return ret;
    }
    public static void main(String[] args) {
        int[] array={2,2,3,3,3,4};
        Arrays.sort(array);
        System.out.println(array.length/2);
    }
}
