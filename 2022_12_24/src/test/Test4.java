package test;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-24
 * Time: 21:18
 */
public class Test4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            char ch = scanner.next().charAt(0);  //获取该字符串0位置的字符
            if (ch >= 'a' && ch <= 'z') {
                ch = (char) (ch - 32);
            } else {
            }
            System.out.println(ch);
        }
    }
}
