package test;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-24
 * Time: 21:45
 */
public class Test5 {
    public static int isDays(int year,int month,int day) {
        int flag = 1;  //1是平年(365天,二月份只有28天),-1代表闰年(366天,二月份有29天)
        if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
            flag = -1;
        }
        int sum = 0;
        int[] days1 = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};  //平年
        int[] days2 = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};  //闰年
        if (flag == 1) {
            for (int i = 0; i < days1.length; i++) {
                if (i < month-1) {
                    sum += days1[i];
                } else {
                    sum += day;
                    break;
                }
            }
        } else {
            for (int i = 0; i < days2.length; i++) {
                if (i < month-1) {
                    sum += days2[i];
                } else {
                    sum += day;
                    break;
                }
            }
        }
        return sum;
    }
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int year = scanner.nextInt();
        int month = scanner.nextInt();
        int day = scanner.nextInt();
        int sum =isDays(year,month,day);
        System.out.println(sum);
    }
}
