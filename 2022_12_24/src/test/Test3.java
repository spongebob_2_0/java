package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-24
 * Time: 18:30
 */
public class Test3 {
    public static int func(String s){
        int[] array =new int[26];
        for (int i = 0; i < s.length(); i++) {
            char ch =s.charAt(i);
            array[ch-'a']++;
        }
        for (int i = 0; i < s.length(); i++) {
            char ch =s.charAt(i);
            if(array[ch-'a'] ==1){
                return i;
            }
        }
        return -1;
    }
    public static void main(String[] args) {
        String s ="aabcbcdefgg";
        int ret =func(s);
        System.out.println(ret);
    }
}
