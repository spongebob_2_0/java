package test1;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-04-03
 * Time: 22:08
 */
public class Demo1 {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int n  = scanner.nextInt();
        System.out.println(n);   // 测试程序执行效率
    }
}
