
package test1;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class FutureTaskThreadDemo {
    public static void main(String[] args) {
        // 创建一个实现 Callable 接口的匿名类实例，该实例在 call() 方法中返回一个字符串
        Callable<String> callableTask = new Callable<String>() {
            @Override
            public String call() throws Exception {
                // 在这里执行你的任务，并返回结果
                String result = "Hello from Callable!";
                return result;
            }
        };

        // 使用 Callable 任务创建一个 FutureTask 实例
        FutureTask<String> futureTask = new FutureTask<>(callableTask);

        // 创建一个新线程，并将 FutureTask 作为参数传递给它
        Thread taskThread = new Thread(futureTask);

        // 启动新线程，这将执行 FutureTask 中的 Callable 任务
        taskThread.start();

        try {
            // 从 FutureTask 对象中获取计算结果，如果任务尚未完成，此方法会阻塞等待
            String result = futureTask.get();
            System.out.println("Result: " + result);
        } catch (InterruptedException e) {
            // 当前线程在等待过程中被中断时抛出 InterruptedException
            e.printStackTrace();
        } catch (ExecutionException e) {
            // 计算任务抛出异常时抛出 ExecutionException
            e.printStackTrace();
        }
    }
}
