package test2;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
/**
 * @author : msb-zhaoss
 */
public class Test05 {
    //这是一个main方法，是程序的入口：
    public static void main(String[] args) throws IOException {
        //功能：完成图片的复制：
        //1.有一个源图片
        File f1 = new File("D:/b/hello.png");
        //2.有一个目标图片：
        File f2 = new File("D:/b/hello3.png");
        //3.有一个输入的管道 怼 到 源文件：
        FileInputStream fis = new FileInputStream(f1);
        //4.有一个输出的管道 怼到  目标文件上：
        FileOutputStream fos = new FileOutputStream(f2);
        //5.开始复制：（边读边写）
        //利用缓冲数组：
        byte[] b = new byte[1024*8];
        int len = fis.read(b);
        while(len!=-1){
            fos.write(b,0,len);
            len = fis.read(b);
        }
        //6.关闭流：(倒着关闭流，先用后关)
        fos.close();
        fis.close();
    }
}
