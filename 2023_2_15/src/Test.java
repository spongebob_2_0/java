public class Test {
    public static void main(String[] args) {
        TestBinaryTree testBinaryTree = new TestBinaryTree();
        TestBinaryTree.TreeNode root = testBinaryTree.createTree();
        testBinaryTree.preOrderNor(root);
        testBinaryTree.inOrderNor(root);
        testBinaryTree.postOrderNor(root);
        System.out.println(testBinaryTree.isCompleteTree(root));
    }
}