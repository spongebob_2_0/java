package com.example.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2024-01-15
 * Time: 0:10
 */

@RestController
public class TestController {

    @RequestMapping("/test")
    public String test() {
        return "测试SpringBoot！！";
    }

}
