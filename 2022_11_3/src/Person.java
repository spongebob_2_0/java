/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-03
 * Time: 23:34
 */

import java.util.*;

//创建类:人类
public class Person {
    //属性(成员变量)
    int age;
    String name;
    double height;
    double weight;
    //吃饭的方法
    public void eat(){
        int num =10;  //局部变量:放在方法中
        System.out.println("我喜欢吃饭");
    }
    //睡觉的方法
    public void sleep(String adress){
        System.out.println("我在"+adress+"睡觉");
    }
    //自我介绍
    public String introduce(){
        return "我的名字是:"+name+",我的年龄是:"+age+",我的身高是:"+height+",我的体重是:"+weight;
    }

}
