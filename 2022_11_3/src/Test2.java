/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-03
 * Time: 1:43
 */

import java.util.*;

public class Test2 {
    public static void move(int n,char tower1,char tower2){
        System.out.println("编号为"+n+"的盘子正在从"+tower1+"移动到"+tower2);
    }

    public static void hanotta(int n,char A,char B,char C){
        if(n==1){
            move(n,A,C);
            return;
        }
        hanotta(n-1,A,C,B);
        move(n,A,C);
        hanotta(n-1,B,A,C);
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入盘数:");
        int n = scanner.nextInt();
        hanotta(n,'A','B','C');
    }
}
