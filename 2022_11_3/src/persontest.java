/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-03
 * Time: 23:43
 */

import java.util.*;

public class persontest {
    public static void main(String[] args) {
        //创建一个人类的对象(实例)
        //创建一个对象,对象的名字叫:zs
        //Person属于引用数据类型
        //第一次加载类的时候,会进行类的加载,初始化创建对象的时候,对象的属性没有给赋值,有默认的初始化的值
        Person zs =new Person();
        zs.name ="张三";
        zs.age =18;
        zs.height=180.4;
        zs.weight=170.4;

        //再次创建类的时候就不会进行类的加载了,类的加载只在第一次需要的时候加载一次
        Person ls =new Person();
        ls.name ="李四";
        ls.age =20;
        ls.height=190.4;
        ls.weight=180.4;

        //对属性值进行读取
        System.out.println(zs.name);
        System.out.println(zs.age);
        System.out.println(zs.weight);
        System.out.println(zs.height);
        System.out.println(ls.name);
        System.out.println(ls.age);
        System.out.println(ls.weight);
        System.out.println(ls.height);

        //对方法进行操作
        //不同的对象,属性有自己特有的值,但是方法都是调用类中通用的方法
        //属性:各个对象的属性是独立的
        //方法:各个对象的方法是共享的
        zs.eat();
        ls.eat();
        ls.sleep("教室");
        String str =zs.introduce();
        System.out.println(str);
//        String str2 = ls.introduce();
//        System.out.println(str2);
        System.out.println(ls.introduce());
    }
}
