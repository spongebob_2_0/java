import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-02-10
 * Time: 14:37
 */
public class TestHeap {
    public int[] elem;
    public int usedSize;
    public TestHeap(){
        this.elem =new int[10];
    }

    public void initElem(int[] array){
        for(int i =0; i< array.length; i++ ){
            elem[i] =array[i];
            usedSize++;
        }
    }

    //创建大根堆
    public void createHeap() {
        for(int parent =(usedSize-1-1)/2; parent >= 0; parent--){
            shiftDown(parent,usedSize);
        }
    }

    private void shiftDown(int parent, int len) {
        int child =2*parent+1;
        //最起码有左孩子
        while (child < len) {
            if(child+1 < len && elem[child] < elem[child+1]){
                child++;
            }
            if(elem[child] > elem[parent]){
                int tmp =elem[child];
                elem[child] =elem[parent];
                elem[parent] =tmp;
                parent =child;
                child =2*parent+1;
            }else{
                break;
            }
        }
    }
     public void offer(int val){
        if(isFull()){
            //扩容
            elem = Arrays.copyOf(elem,2*elem.length);
        }
         elem[usedSize++] =val;
         //向上调整
         shiftUp(usedSize-1);
     }

    public boolean isFull() {
        return usedSize == elem.length;
    }

    private void shiftUp(int child){
        int parent =(child-1)/2;
        while(child > 0){
            if(elem[child] > elem[parent]){
                int tmp =elem[child];
                elem[child] =elem[parent];
                elem[parent] =tmp;
                child =parent;
                parent =(child-1) /2;
            }else{
                break;
            }
        }
    }
    public boolean isEmpty(){
        return usedSize == 0;
    }
    public void pop(){
        if(isEmpty()){
            return;
        }
        swap(elem,0,usedSize-1);
        usedSize--;
        shiftDown(0,usedSize);
    }

    private void swap(int[] elem, int i, int j) {
        int tmp = elem[i];
        elem[i] =elem[j];
        elem[j] =tmp;
    }
}
