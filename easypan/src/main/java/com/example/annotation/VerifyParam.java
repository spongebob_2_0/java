package com.example.annotation;



import com.example.entity.enums.VerifyRegexEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention(RetentionPolicy.RUNTIME) // @Retention注解用于指定这个注解的信息会保留到什么时候，这里是运行时
@Target({ElementType.PARAMETER, ElementType.FIELD}) // @Target注解用于指定这个注解可以加在Java元素（如参数、字段等）的哪些地方
public @interface VerifyParam {  // 定义一个名为VerifyParam的公共注解
    /**
     * 校验正则,默认不校验
     *
     * @return
     */
    VerifyRegexEnum regex() default VerifyRegexEnum.NO;

    /**
     * 参数最小长度,默认-1
     *
     * @return
     */
    int min() default -1;

    /**
     * 参数最大长度,默认-1
     *
     * @return
     */
    int max() default -1;

    /**
     * 参数是否必传,默认不是必传参数
     * @return
     */
    boolean required() default false;
}
