package com.example.annotation;


import org.springframework.web.bind.annotation.Mapping;

import java.lang.annotation.*;



@Target({ElementType.METHOD, ElementType.TYPE}) // @Target注解用于指定这个注解可以加在Java元素（如类、方法等）的哪些地方
@Retention(RetentionPolicy.RUNTIME) // @Retention注解用于指定这个注解的信息会保留到什么时候
@Documented  // @Documented注解表明这个注解应该被javadoc工具记录
@Mapping   // @Mapping是Spring的元注解，表明这个自定义注解是一个映射注解
public @interface GlobalInterceptor { // 定义一个名为GlobalInterceptor的公共注解

    /**
     * 校验是否登录,默认登录状态
     *
     * @return
     */
    boolean checkLogin() default true;

    /**
     * 校验参数是否符合要求,默认校验
     *
     * @return
     */
    boolean checkParams() default false;

    /**
     * 校验是否是管理员,默认不是管理员
     *
     * @return
     */
    boolean checkAdmin() default false;
}
