package com.example.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author WuYimin
 */ // 使用@Component标记这个类为Spring组件，并指定其bean名称为"applicationContextProvider"
@Component("applicationContextProvider")
public class ApplicationContextProvider implements ApplicationContextAware {

    // 声明并初始化SLF4J日志记录器，用于记录日志
    private static final Logger logger = LoggerFactory.getLogger(ApplicationContextProvider.class);

    // 声明一个静态的ApplicationContext实例，用于存放Spring的上下文对象
    private static ApplicationContext applicationContext;

    // 实现ApplicationContextAware接口的方法，通过这个方法，Spring会自动注入ApplicationContext对象
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    /**
     * 获取applicationContext的静态方法
     *
     * @return 返回当前的ApplicationContext
     */
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    /**
     * 通过bean的名称获取Bean对象
     *
     * @param name Bean的名称
     * @return 返回名为name的Bean对象
     */
    public static Object getBean(String name) {
        try {
            return getApplicationContext().getBean(name);
        } catch (NoSuchBeanDefinitionException e) {
            // 如果找不到指定的Bean，则记录异常日志
            logger.error("获取bean异常", e);
            return null;
        }
    }

    /**
     * 通过class类型获取Bean对象
     *
     * @param clazz Bean的类型
     * @param <T> Bean的泛型类型
     * @return 返回类型为clazz的Bean对象
     */
    public static <T> T getBean(Class<T> clazz) {
        return getApplicationContext().getBean(clazz);
    }

    /**
     * 通过bean的名称和class类型获取Bean对象
     *
     * @param name Bean的名称
     * @param clazz Bean的类型
     * @param <T> Bean的泛型类型
     * @return 返回名为name且类型为clazz的Bean对象
     */
    public static <T> T getBean(String name, Class<T> clazz) {
        return getApplicationContext().getBean(name, clazz);
    }
}
