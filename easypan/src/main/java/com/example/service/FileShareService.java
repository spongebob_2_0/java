package com.example.service;

import com.example.entity.po.FileShare;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author WuYimin
* @description 针对表【file_share(分享信息)】的数据库操作Service
* @createDate 2023-11-21 16:28:06
*/
public interface FileShareService extends IService<FileShare> {

}
