package com.example.service;

import com.example.entity.po.EmailCode;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author WuYimin
* @description 针对表【email_code(邮箱验证码)】的数据库操作Service
* @createDate 2023-11-21 16:28:06
*/
public interface EmailCodeService extends IService<EmailCode> {

}
