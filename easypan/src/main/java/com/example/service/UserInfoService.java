package com.example.service;

import com.example.entity.po.UserInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

/**
* @author WuYimin
* @description 针对表【user_info(用户信息)】的数据库操作Service
* @createDate 2023-11-21 16:28:06
*/
public interface UserInfoService extends IService<UserInfo> {

}
