package com.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.component.BusinessException;
import com.example.component.RedisComponent;
import com.example.config.AppConfig;
import com.example.entity.constants.Constants;
import com.example.entity.dto.SysSettingsDto;
import com.example.entity.po.EmailCode;
import com.example.entity.po.UserInfo;
import com.example.mapper.UserInfoMapper;
import com.example.service.EmailCodeService;
import com.example.mapper.EmailCodeMapper;
import com.example.utils.StringTools;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.mail.internet.MimeMessage;
import java.awt.print.PrinterAbortException;
import java.util.Date;

/**
* @author WuYimin
* @description 针对表【email_code(邮箱验证码)】的数据库操作Service实现
* @createDate 2023-11-21 16:28:06
*/
@Service
public class EmailCodeServiceImpl extends ServiceImpl<EmailCodeMapper, EmailCode>
    implements EmailCodeService{

    @Resource
    private UserInfoMapper userInfoMapper;

    @Resource
    private EmailCodeMapper emailCodeMapper;

    @Resource
    private AppConfig appConfig;

    @Resource
    private JavaMailSender javaMailSender;

    @Resource
    private RedisComponent redisComponent;


    /**
     * 发送邮件的方法
     * @param toEmail
     * @param type
     */
    // 声明这个方法需要进行事务管理，如果发生异常则进行回滚操作
    @Transactional(rollbackFor = Exception.class)
    // 定义一个发送电子邮件验证码的方法，需要提供收件邮箱地址和验证码类型
    public void sendEmail(String toEmail, Integer type) {
        // 如果是注册，校验邮箱是否已存在
        if (type.equals(Constants.ZERO)) {  // 如果验证码类型为0，表示是注册操作
            LambdaQueryWrapper<UserInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(UserInfo::getEmail,toEmail);
            UserInfo userInfo = userInfoMapper.selectOne(lambdaQueryWrapper);  // 调用userInfoMapper的方法，根据邮箱地址查询用户信息
            if (null != userInfo) {  // 如果查询到的用户信息不为空，表示这个邮箱已经被注册过了
                throw new BusinessException("邮箱已经存在");  // 抛出一个业务异常，提示邮箱已存在
            }
        }

        String code = StringTools.getRandomNumber(Constants.LENGTH_5);  // 调用StringTools工具类方法生成一个指定长度的随机验证码

        // 调用sendEmailCode方法，将验证码发送到指定的邮箱地址
        sendEmailCode(toEmail, code);
        // 调用emailCodeMapper的方法，将该邮箱地址的旧验证码设置为无效
        emailCodeMapper.disableEmailCode(toEmail);

        EmailCode emailCode = EmailCode.builder()  // 创建一个新的EmailCode对象，用于存放新的验证码信息
                                .code(code)  // 设置验证码
                                .email(toEmail)  // 设置验证码
                                .status(Constants.ZERO)  // 设置验证码的状态为0
                                .createTime(new Date())  // 设置验证码的创建时间为当前时间
                                .build();
        emailCodeMapper.insert(emailCode);  // 调用emailCodeMapper的方法，将新的验证码信息插入到数据库中
    }

    /**
     * 将验证码发送到指定的邮箱地址
     * @param toEmail
     * @param code
     */
    private void sendEmailCode(String toEmail, String code) {
        try {
            // 创建一个MimeMessage对象，用于设置邮件内容
            MimeMessage message = javaMailSender.createMimeMessage();

            // 创建MimeMessageHelper对象，辅助设置邮件内容
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            // 设置邮件的发件人
            helper.setFrom(appConfig.getSendUserName());

            // 设置邮件的收件人，可以是一个或多个
            helper.setTo(toEmail);

            // 从Redis中获取系统设置信息
            SysSettingsDto sysSettingsDto = redisComponent.getSysSettingsDto();

            // 设置邮件的主题
            helper.setSubject(sysSettingsDto.getRegisterEmailTitle());

            String emailCode = String.format(sysSettingsDto.getRegisterEmailContent(),code);
            // 设置邮件的内容，使用系统设置中的内容模板，将验证码插入到模板中
            String html = "<head>\n" +
                    "    <meta charset=\"utf-8\">\n" +
                    "    <base target=\"_blank\"/>\n" +
                    "    <style type=\"text/css\">::-webkit-scrollbar {\n" +
                    "        display: none;\n" +
                    "    }</style>\n" +
                    "    <style id=\"cloudAttachStyle\" type=\"text/css\">#divNeteaseBigAttach, #divNeteaseBigAttach_bak {\n" +
                    "        display: none;\n" +
                    "    }</style>\n" +
                    "    <style id=\"blockquoteStyle\" type=\"text/css\">blockquote {\n" +
                    "        display: none;\n" +
                    "    }</style>\n" +
                    "    <style type=\"text/css\">\n" +
                    "        body {\n" +
                    "            font-size: 14px;\n" +
                    "            font-family: arial, verdana, sans-serif;\n" +
                    "            line-height: 1.666;\n" +
                    "            padding: 0;\n" +
                    "            margin: 0;\n" +
                    "            overflow: auto;\n" +
                    "            white-space: normal;\n" +
                    "            word-wrap: break-word;\n" +
                    "            min-height: 100px\n" +
                    "        }\n" +
                    "\n" +
                    "        td, input, button, select, body {\n" +
                    "            font-family: Helvetica, 'Microsoft Yahei', verdana\n" +
                    "        }\n" +
                    "\n" +
                    "        pre {\n" +
                    "            white-space: pre-wrap;\n" +
                    "            white-space: -moz-pre-wrap;\n" +
                    "            white-space: -pre-wrap;\n" +
                    "            white-space: -o-pre-wrap;\n" +
                    "            word-wrap: break-word;\n" +
                    "            width: 95%\n" +
                    "        }\n" +
                    "\n" +
                    "        th, td {\n" +
                    "            font-family: arial, verdana, sans-serif;\n" +
                    "            line-height: 1.666\n" +
                    "        }\n" +
                    "\n" +
                    "        img {\n" +
                    "            border: 0\n" +
                    "        }\n" +
                    "\n" +
                    "        header, footer, section, aside, article, nav, hgroup, figure, figcaption {\n" +
                    "            display: block\n" +
                    "        }\n" +
                    "\n" +
                    "        blockquote {\n" +
                    "            margin-right: 0px\n" +
                    "        }\n" +
                    "    </style>\n" +
                    "</head>\n" +
                    "\n" +
                    "<body tabindex=\"0\" role=\"listitem\">\n" +
                    "<table width=\"700\" border=\"0\" align=\"center\" cellspacing=\"0\" style=\"width:700px;\">\n" +
                    "    <tbody>\n" +
                    "    <tr>\n" +
                    "        <td>\n" +
                    "            <div style=\"width:700px;margin:0 auto;border-bottom:1px solid #ccc;margin-bottom:30px;\">\n" +
                    "                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"700\" height=\"39\"\n" +
                    "                       style=\"font:12px Tahoma, Arial, 宋体;\">\n" +
                    "                    <tbody>\n" +
                    "                    <tr>\n" +
                    "                        <td width=\"210\"></td>\n" +
                    "                    </tr>\n" +
                    "                    </tbody>\n" +
                    "                </table>\n" +
                    "            </div>\n" +
                    "            <div style=\"width:680px;padding:0 10px;margin:0 auto;\">\n" +
                    "                <div style=\"line-height:1.5;font-size:14px;margin-bottom:25px;color:#4d4d4d;\">\n" +
                    "                    <strong style=\"display:block;margin-bottom:15px;\">尊敬的用户，您好：<span\n" +
                    "                            style=\"color:#f60;font-size: 16px;\"></span></strong>\n" +
                    "                    <span\n" +
                    "                            style=\"color:#f60;font-size: 24px\">"+ emailCode +"</span>" +
                    "                    </strong>\n" +
                    "                </div>\n" +
                    "                <div style=\"margin-bottom:30px;\">\n" +
                    "                    <small style=\"display:block;margin-bottom:20px;font-size:12px;\">\n" +
                    "                        <p style=\"color:#747474;\">\n" +
                    "                            注意：此操作可能会注册一个新账号。如非本人操作，请及时登录并修改密码以保证帐户安全\n" +
                    "                            <br>（工作人员不会向你索取此验证码，请勿泄漏！)\n" +
                    "                        </p>\n" +
                    "                    </small>\n" +
                    "                </div>\n" +
                    "            </div>\n" +
                    "\n" +
                    "            <div style=\"width:700px;margin:0 auto;\">\n" +
                    "                <div style=\"padding:10px 10px 0;border-top:1px solid #ccc;color:#747474;margin-bottom:20px;line-height:1.3em;font-size:12px;\">\n" +
                    "                    <p>此为系统邮件，请勿回复<br>\n" +
                    "                        请保管好您的邮箱，避免账号被他人盗用\n" +
                    "                    </p>\n" +
                    "                    <p>XXXXXXX QA团队</p>\n" +
                    "                </div>\n" +
                    "            </div>\n" +
                    "        </td>\n" +
                    "    </tr>\n" +
                    "    </tbody>\n" +
                    "</table>\n" +
                    "</body>\n";
            helper.setText(html,true);

            // 设置邮件的发送时间
            helper.setSentDate(new Date());

            // 使用JavaMailSender对象发送邮件
            javaMailSender.send(message);
        } catch (Exception e) {
            // 如果发送过程中出现异常，记录异常日志并抛出业务异常
            log.error("邮件发送失败", e);
            throw new BusinessException("邮件发送失败");
        }
    }

    /**
     * 校验邮箱验证码
     * @param email
     * @param code
     */
    public void checkCode(String email, String code) {
        // 通过邮箱和验证码查询数据库，检查验证码是否存在
        LambdaQueryWrapper<EmailCode> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(EmailCode::getEmail,email)
                          .eq(EmailCode::getCode,code);
        EmailCode emailCode = emailCodeMapper.selectOne(lambdaQueryWrapper);
        // 如果未查到验证码信息，说明验证码不正确，抛出业务异常
        if (null == emailCode) {
            throw new BusinessException("邮箱验证码不正确");
        }

        // 如果验证码的状态为1，或者验证码已经超过指定的有效时间，那么验证码失效，抛出业务异常
        if (emailCode.getStatus() == 1 || System.currentTimeMillis() - emailCode.getCreateTime().getTime() > Constants.LENGTH_15 * 1000 * 60) {
            throw new BusinessException("邮箱验证码已失效");
        }

        // 使邮箱地址对应的验证码失效
        emailCodeMapper.disableEmailCode(email);
    }
}




