package com.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.component.BusinessException;
import com.example.entity.constants.Constants;
import com.example.entity.dto.SessionShareDto;
import com.example.entity.enums.PageSize;
import com.example.entity.enums.ResponseCodeEnum;
import com.example.entity.enums.ShareValidTypeEnums;
import com.example.entity.po.FileInfo;
import com.example.entity.po.FileShare;
import com.example.entity.query.FileShareQuery;
import com.example.entity.vo.FileInfoVO;
import com.example.entity.vo.FileShareVO;
import com.example.entity.vo.PaginationResultVO;
import com.example.service.FileShareService;
import com.example.mapper.FileShareMapper;
import com.example.utils.DateUtil;
import com.example.utils.StringTools;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;

/**
* @author WuYimin
* @description 针对表【file_share(分享信息)】的数据库操作Service实现
* @createDate 2023-11-21 16:28:06
*/
@Service
public class FileShareServiceImpl extends ServiceImpl<FileShareMapper, FileShare>
    implements FileShareService{

    @Resource
    private FileShareMapper fileShareMapper;

    /**
     * 文件分享的分页方法
     * @param query
     * @return
     */
    public PaginationResultVO<FileShareVO> findListByPage(FileShareQuery query) {

        // 判断查询参数中是否有设置每页显示数量，如果没有则默认为15条记录，否则使用设置的数量
        int pageSize = query.getPageSize() == null ? PageSize.SIZE15.getSize() : query.getPageSize();
        int pageNo  = query.getPageNo() == null ? 1: query.getPageNo();

        // 创建分页结果对象，并设置相关属性如：总记录数、每页显示数量、当前页号、总页数和当前页的数据列表
        Page<FileShare> page = new Page<>(pageNo, pageSize);
        // 调用Mapper的selectPage方法进行分页查询
        Page<FileShareVO> fileInfoPage = fileShareMapper.selectSharePage(page,query);

        PaginationResultVO<FileShareVO> result = new PaginationResultVO((int) fileInfoPage.getTotal(), (int) fileInfoPage.getSize(), pageNo, (int) fileInfoPage.getPages(), fileInfoPage.getRecords());
        // 返回分页结果对象
        return result;
    }

    /**
     * 分享文件 （将分享的文件插入数据库）
     */
    public void saveShare(FileShare share) {
        // 根据share的ValidType获取对应的文件共享有效类型枚举
        ShareValidTypeEnums typeEnum = ShareValidTypeEnums.getByType(share.getValidType());
        // 如果获取的枚举为空，则抛出一个业务异常
        if (null == typeEnum) {
            throw new BusinessException(ResponseCodeEnum.CODE_600);
        }
        // 如果该文件共享的有效类型不是"永久"，则计算其到期时间
        if (typeEnum != ShareValidTypeEnums.FOREVER) {
            // 使用 getAfterDate方法 去获取指定天数后的日期
            share.setExpireTime(DateUtil.getAfterDate(typeEnum.getDays()));
        }
        // 设置文件共享的时间为当前时间
        Date curDate = new Date();
        share.setShareTime(curDate);
        // 如果文件共享的code为空，则为其随机生成一个长度为5的字符串
        if (StringTools.isEmpty(share.getCode())) {
            share.setCode(StringTools.getRandomString(Constants.LENGTH_5));
        }
        // 为文件共享生成一个长度为20的随机shareId
        share.setShareId(StringTools.getRandomString(Constants.LENGTH_20));
        // 调用fileShareMapper的insert方法插入文件共享记录
        this.fileShareMapper.insert(share);
    }

    /**
     * 批量删除指定的文件共享记录，并检查删除的数量是否与预期相符
     * @param shareIdArray
     * @param userId
     */
    @Transactional(rollbackFor = Exception.class)
    public void deleteFileShareBatch(String[] shareIdArray, String userId) {
        // 调用fileShareMapper的批量删除方法，传入待删除的shareId数组和用户ID
//        Integer count = this.fileShareMapper.deleteFileShareBatch(shareIdArray, userId);

        int count = this.fileShareMapper.deleteFileShareBatch(shareIdArray,userId);
        // 如果实际删除的数量与预期不符，则抛出一个业务异常
        if (count != shareIdArray.length) {
            throw new BusinessException(ResponseCodeEnum.CODE_600);
        }
    }

    /**
     * 检查提取码是否正确
     * @param shareId
     * @param code
     * @return
     */
    public SessionShareDto checkShareCode(String shareId, String code) {
        // 通过shareId查询对应的文件共享信息
        LambdaQueryWrapper<FileShare> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(FileShare::getShareId,shareId);
        FileShare share = this.fileShareMapper.selectOne(lambdaQueryWrapper);
        // 如果共享信息不存在，或者已经过期，则抛出一个业务异常
        if (null == share || (share.getExpireTime() != null && new Date().after(share.getExpireTime()))) {
            throw new BusinessException(ResponseCodeEnum.CODE_902);
        }
        // 如果传入的提取码与共享信息中的提取码不匹配，则抛出一个业务异常
        if (!share.getCode().equals(code)) {
            throw new BusinessException("提取码错误");
        }

        // 更新该文件共享的浏览次数
        LambdaUpdateWrapper<FileShare> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        lambdaUpdateWrapper
                .eq(FileShare::getShareId,shareId)
                .setSql("show_count = show_count + 1");
        this.fileShareMapper.update(null,lambdaUpdateWrapper);
        // 创建一个新的共享会话数据传输对象
        SessionShareDto shareSessionDto = new SessionShareDto();
        shareSessionDto.setShareId(shareId);
        shareSessionDto.setShareUserId(share.getUserId());
        shareSessionDto.setFileId(share.getFileId());
        shareSessionDto.setExpireTime(share.getExpireTime());
        // 返回该共享会话数据传输对象
        return shareSessionDto;
    }
}




