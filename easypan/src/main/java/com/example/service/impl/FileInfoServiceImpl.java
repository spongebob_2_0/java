package com.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.component.BusinessException;
import com.example.component.RedisComponent;
import com.example.config.AppConfig;
import com.example.entity.constants.Constants;
import com.example.entity.dto.SessionWebUserDto;
import com.example.entity.dto.UploadResultDto;
import com.example.entity.dto.UserSpaceDto;
import com.example.entity.enums.*;
import com.example.entity.po.FileInfo;
import com.example.entity.po.UserInfo;
import com.example.entity.query.FileInfoQuery;
import com.example.entity.vo.FileInfoVO;
import com.example.entity.vo.PaginationResultVO;
import com.example.mapper.UserInfoMapper;
import com.example.service.FileInfoService;
import com.example.mapper.FileInfoMapper;
import com.example.utils.DateUtil;
import com.example.utils.ProcessUtils;
import com.example.utils.ScaleFilter;
import com.example.utils.StringTools;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.*;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
* @author WuYimin
* @description 针对表【file_info(文件信息)】的数据库操作Service实现
* @createDate 2023-11-21 16:28:06
*/
@Service
@Slf4j
public class FileInfoServiceImpl extends ServiceImpl<FileInfoMapper, FileInfo>
    implements FileInfoService{


    @Resource
    private FileInfoMapper fileInfoMapper;

    @Resource
    private RedisComponent redisComponent;

    @Resource
    private UserInfoMapper userInfoMapper;

    @Resource // 注入自己的Bean，用于AOP和事务处理，需要@Lazy注解来延迟初始化bean
    @Lazy  //加上@lazy解决循环依赖问题
    private FileInfoServiceImpl fileInfoService;

    @Resource
    private AppConfig appConfig;

    @Resource
    @Lazy
    private FileInfoService fileInfoService2;

    /**
     * 文件分页方法
     * @param query
     * @return
     */
    public PaginationResultVO<FileInfo> findListByPage(FileInfoQuery query) {

      // 判断查询参数中是否有设置每页显示数量，如果没有则默认为15条记录，否则使用设置的数量
        int pageSize = query.getPageSize() == null ? PageSize.SIZE15.getSize() : query.getPageSize();
        int pageNo  = query.getPageNo() == null ? 1: query.getPageNo();

        // 创建分页结果对象，并设置相关属性如：总记录数、每页显示数量、当前页号、总页数和当前页的数据列表
        Page<FileInfo> page = new Page<>(pageNo, pageSize);
        // 调用Mapper的selectPage方法进行分页查询
        Page<FileInfo> fileInfoPage = fileInfoMapper.selectPageVo(page,query);
        PaginationResultVO<FileInfo> result = new PaginationResultVO((int) fileInfoPage.getTotal(), (int) fileInfoPage.getSize(), pageNo, (int) fileInfoPage.getPages(), fileInfoPage.getRecords());
        // 返回分页结果对象
        return result;
    }


    /**
     * 上传文件
     * 该方法支持文件的分块上传。当所有文件块上传完毕后，会将这些文件块合并为一个完整的文件。
     * 如果系统中已存在相同MD5值的文件，该方法会直接使用已存在的文件，实现秒传功能，不再重新上传。
     * @param webUserDto  当前上传文件的用户信息
     * @param fileId      上传文件的唯一标识。如果为空，方法内部会生成一个新的标识
     * @param file        上传的文件块
     * @param fileName    上传的文件名
     * @param filePid     文件的父级ID，用于文件的层级结构
     * @param fileMd5     上传文件的MD5值，用于秒传功能
     * @param chunkIndex  当前上传文件块的索引，从0开始
     * @param chunks      文件分块的总数
     * @return            返回文件上传的结果，包括文件ID和上传状态等信息
     * @throws BusinessException 当文件上传出现业务异常时，如存储空间不足，会抛出该异常
     */
    @Transactional(rollbackFor = Exception.class) // 这是一个事务操作，如果在该方法执行过程中出现异常，则所有的数据库操作都会回滚
    public UploadResultDto uploadFile(SessionWebUserDto webUserDto, String fileId, MultipartFile file, String fileName,
                                      String filePid, String fileMd5, Integer chunkIndex, Integer chunks) {


        File tempFileFolder = null;  // 临时文件夹
        Boolean uploadSuccess = true;  // 文件是否成功上传的标志，默认为true

        try {
            UploadResultDto resultDto = new UploadResultDto(); // 初始化文件上传结果的DTO对象
            // 如果没有传入文件ID，则生成一个10位随机字符串作为文件ID
            if (StringTools.isEmpty(fileId)) {
                fileId = StringTools.getRandomString(Constants.LENGTH_10);
            }
            resultDto.setFileId(fileId); // 在结果DTO中设置文件ID

            // 从Redis中获取当前用户已使用的存储空间信息(这个在登录的时候就会把存储空间的信息设置到redis里面了)
            UserSpaceDto spaceDto = redisComponent.getUserSpaceUse(webUserDto.getUserId());

            // 如果当前是上传的第一个分片
            if (chunkIndex == 0) {

                LambdaQueryWrapper<FileInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
                lambdaQueryWrapper
                        .eq(FileInfo::getFileMd5,fileMd5) // 根据文件的MD5值去查询是否有相同的文件已存在
                        .eq(FileInfo::getStatus,FileStatusEnums.USING.getStatus()) //查询条件为使用中状态
                        .last("limit 5");      // 自定义SQL，最多返回5条数据

                List<FileInfo> dbFileList = this.fileInfoMapper.selectList(lambdaQueryWrapper);

                // 如果数据库中存在相同MD5的文件，则进行秒传（无需重新上传，直接复用原文件）
                if (!dbFileList.isEmpty()) {
                    FileInfo dbFile = dbFileList.get(0);

                    // 如果新的文件大小加上已使用的存储空间超出总存储空间，则抛出异常
                    if (dbFile.getFileSize() + spaceDto.getUseSpace() > spaceDto.getTotalSpace()) {
                        throw new BusinessException(ResponseCodeEnum.CODE_904);
                    }

                    // 根据新上传的文件信息，更新数据库中文件的相关属性
                    dbFile.setFileId(fileId);   // 文件ID
                    dbFile.setFilePid(filePid); // 文件父级ID
                    dbFile.setUserId(webUserDto.getUserId());  // 用户ID
                    dbFile.setCreateTime(new Date());  //文件创建时间
                    dbFile.setLastUpdateTime(new Date());  // 文件最后修改时间
                    dbFile.setStatus(FileStatusEnums.USING.getStatus());  // 文件为使用中状态
                    dbFile.setDelFlag(FileDelFlagEnums.USING.getFlag());  // 文件为使用中问题
                    dbFile.setFileMd5(fileMd5);        // 设置文件MD5值
                    dbFile.setFileName(autoRename(filePid, webUserDto.getUserId(), fileName)); //设置文件重命名

                    // 插入新的文件信息到数据库
                    this.fileInfoMapper.insert(dbFile);

                    // 设置上传文件的状态为秒传 （这个是要返回给前端看的）
                    resultDto.setStatus(UploadStatusEnums.UPLOAD_SECONDS.getCode());

                    // 这个方法用来更新用户已使用的存储空间信息
                    updateUserSpace(webUserDto, dbFile.getFileSize());

                    return resultDto;
                }
            }

            // 不是秒传走下面的逻辑
            // 定义临时文件夹目录路径，用于存储上传的文件块 /temp/
            String tempFolderName = appConfig.getProjectFolder() + Constants.FILE_FOLDER_TEMP;
            // 设置临时文件夹目录里面的存储分片的文件夹名称 等于 用户ID + 文件ID
            String currentUserFolderName = webUserDto.getUserId() + fileId;
            // new 一个 存储分片的文件夹
            tempFileFolder = new File(tempFolderName + currentUserFolderName);

            // 如果临时文件夹不存在，则创建
            if (!tempFileFolder.exists()) {
                tempFileFolder.mkdirs();
            }

            // 再次检查上传的文件大小与用户可用存储空间，避免超出
            Long currentTempSize = redisComponent.getFileTempSize(webUserDto.getUserId(), fileId);
            if (file.getSize() + currentTempSize + spaceDto.getUseSpace() > spaceDto.getTotalSpace()) {
                throw new BusinessException(ResponseCodeEnum.CODE_904);
            }

            // 创建新的临时文件，并将上传的文件块内容写入临时文件夹里面
            File newFile = new File(tempFileFolder.getPath() + "/" + chunkIndex);
            file.transferTo(newFile);

            // 更新在Redis中记录的临时文件大小
            redisComponent.saveFileTempSize(webUserDto.getUserId(), fileId, file.getSize());

            // 如果当前不是最后一个文件块，则直接返回上传中的状态
            if (chunkIndex < chunks - 1) {
                resultDto.setStatus(UploadStatusEnums.UPLOADING.getCode());
                return resultDto;
            }

            // 所有文件上传完成后
            // 以下操作是针对所有文件块都上传完成后的处理(文件信息插入数据库,然后异步合并分片)
            // 获取当前月份，可能用于文件存储的目录结构
            String month = DateUtil.format(new Date(), DateTimePatternEnum.YYYYMM.getPattern());
            String fileSuffix = StringTools.getFileSuffix(fileName);  // 获取文件后缀名
            String realFileName = currentUserFolderName + fileSuffix;  // 当前文件夹名称（用户ID + 文件ID）+文件后缀名
            // 根据文件后缀名获取文件类型 （）
            FileTypeEnums fileTypeEnum = FileTypeEnums.getFileTypeBySuffix(fileSuffix);

            // 自动重命名文件
            fileName = autoRename(filePid, webUserDto.getUserId(), fileName);

            // 创建文件信息对象，并设置相关属性
            FileInfo fileInfo = FileInfo.builder()
                    .fileId(fileId)  // 文件ID
                    .filePid(filePid)     // 文件父级ID
                    .userId(webUserDto.getUserId())  // 用户ID
                    .fileMd5(fileMd5)     // 文件MD5值
                    .fileName(fileName)   // 文件名
                    .filePath(month + "/" + realFileName)  // 文件路径
                    .createTime(new Date())       // 文件创建时间
                    .lastUpdateTime(new Date())   // 文件修改时间
                    .fileCategory(fileTypeEnum.getCategory().getCategory()) // 用于存储文件类别的数字表示
                    .fileType(fileTypeEnum.getType())        // 文件类型
                    .status(FileStatusEnums.TRANSFER.getStatus())  // 文件当前状态
                    .folderType(FileFolderTypeEnums.FILE.getType())  // 文件夹类型
                    .delFlag(FileDelFlagEnums.USING.getFlag())   //  文件是否删除的状态
                    .build();

            // 插入文件信息到数据库
            this.fileInfoMapper.insert(fileInfo);

            // 从Redis中获取用户文件的临时大小
            Long totalSize = redisComponent.getFileTempSize(webUserDto.getUserId(), fileId);
            // 这个方法用来更新用户已使用的存储空间信息
            updateUserSpace(webUserDto, totalSize);

            // 设置上传状态为完成
            resultDto.setStatus(UploadStatusEnums.UPLOAD_FINISH.getCode());

            // 当事务提交后，调用异步方法处理文件块的合并操作
            TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronization() {
                @Override
                public void afterCommit() {
                    fileInfoService.transferFile(fileInfo.getFileId(), webUserDto);
                }
            });
            return resultDto;

        } catch (BusinessException e) {
            // 捕获自定义业务异常
            uploadSuccess = false;
            log.error("文件上传失败", e);
            throw e;

        } catch (Exception e) {
            // 捕获其他类型的异常
            uploadSuccess = false;
            log.error("文件上传失败", e);
            throw new BusinessException("文件上传失败");

        } finally {
            // 如果上传失败，清除已上传的临时文件块
            if (tempFileFolder != null && !uploadSuccess) {
                try {
                    FileUtils.deleteDirectory(tempFileFolder);
                } catch (IOException e) {
                    log.error("删除临时目录失败");
                }
            }
        }
    }


    /**
     * 文件重命名方法。
     * 当用户上传的文件与已有文件名称重复时，该方法会自动为上传的文件重新命名，以避免名称冲突。
     * @param filePid  文件的父级ID，用于查找同一文件夹内的文件
     * @param userId   当前用户ID
     * @param fileName 原始的文件名
     * @return 返回重命名后的文件名。如果没有重名情况，返回原文件名。
     */
    private String autoRename(String filePid, String userId, String fileName) {
        // 构造查询条件，查找同一文件夹内、同一用户、状态为使用中、且文件名与待上传文件相同的文件数量
        LambdaQueryWrapper<FileInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper
                .eq(FileInfo::getUserId,userId)   // 用户ID
                .eq(FileInfo::getFilePid,filePid) // 文件父级ID
                .eq(FileInfo::getDelFlag,FileDelFlagEnums.USING.getFlag()) // 文件为使用中状态
                .eq(FileInfo::getFileName,fileName);  // 文件名

        // 查询是否存在同名文件 （数量大于1说明这个文件已经存在了）
        Long count = this.fileInfoMapper.selectCount(lambdaQueryWrapper);

        // 如果存在同名文件，使用工具类对文件进行重命名
        if (count > 0) {
            return StringTools.rename(fileName);
        }

        // 否则返回原文件名
        return fileName;
    }

    /**
     * 更新用户的存储空间使用情况。
     * 该方法会首先在数据库中更新用户的存储空间使用情况，然后在Redis中进行相应的更新。
     * @param webUserDto 当前用户的信息
     * @param useSpace  需要增加的存储空间大小
     * @throws BusinessException 如果数据库更新失败，会抛出该异常
     */
    private void updateUserSpace(SessionWebUserDto webUserDto, Long useSpace) {
        // 尝试在数据库中更新用户的存储空间使用情况,
        Integer count = userInfoMapper.updateUserSpace(webUserDto.getUserId(), useSpace, null);

        // 如果更新失败（返回的count为0），抛出业务异常 (更新失败,说明容量不够)
        if (count == 0) {
            throw new BusinessException(ResponseCodeEnum.CODE_904);
        }

        // 获取Redis中的用户存储空间使用情况
        UserSpaceDto spaceDto = redisComponent.getUserSpaceUse(webUserDto.getUserId());
        // 更新Redis中的存储空间使用大小
        spaceDto.setUseSpace(spaceDto.getUseSpace() + useSpace);
        // 保存更新后的信息到Redis
        redisComponent.saveUserSpaceUse(webUserDto.getUserId(), spaceDto);
    }

    /**
     * 异步执行的文件传输方法。
     * 该方法的主要任务是将临时目录中的文件转移到目标目录，并针对视频和图像文件进行特定的处理。
     * @param fileId 文件ID
     * @param webUserDto 当前用户的信息
     */
    @Async
    @Transactional
    public void transferFile(String fileId, SessionWebUserDto webUserDto) {
        Boolean transferSuccess = true; // 标记是否转码成功
        String targetFilePath = null; // 目标文件路径
        String cover = null; // 文件封面
        FileTypeEnums fileTypeEnum = null; // 文件类型枚举

        // 根据文件ID和用户ID获取文件信息
        LambdaQueryWrapper<FileInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper
                .eq(FileInfo::getFileId,fileId)  // 文件ID
                .eq(FileInfo::getUserId,webUserDto.getUserId()); // 用户ID
        FileInfo fileInfo = fileInfoMapper.selectOne(lambdaQueryWrapper);

        try {
            // 检查文件是否存在 和 文件状态是否为转码中状态(TRANSFER)
            if (fileInfo == null || !FileStatusEnums.TRANSFER.getStatus().equals(fileInfo.getStatus())) {
                return; //如果不是就不用处理了
            }

            // 获取临时文件夹目录路径 (/temp/)
            String tempFolderName = appConfig.getProjectFolder() + Constants.FILE_FOLDER_TEMP;
            // 设置临时文件夹目录里面的存储分片的文件夹名称 等于 用户ID + 文件ID
            String currentUserFolderName = webUserDto.getUserId() + fileId;
            // new 一个 存储分片的文件夹
            File fileFolder = new File(tempFolderName + currentUserFolderName);

            // 如果存储分片的文件夹不存在，则创建
            if (!fileFolder.exists()) {
                fileFolder.mkdirs();
            }

            // 获取文件后缀 (当前上传的文件,从数据库查到的文件信息里面拿)
            String fileSuffix = StringTools.getFileSuffix(fileInfo.getFileName());
            // 获取创建日期 (只需要年月的格式即可)
            String month = DateUtil.format(fileInfo.getCreateTime(), DateTimePatternEnum.YYYYMM.getPattern());

            // 获取目标目录 (之后要将上传的文件移动到目标目录)
            String targetFolderName = appConfig.getProjectFolder() + Constants.FILE_FOLDER_FILE;
            // new 一个 目标目录对象
            File targetFolder = new File(targetFolderName + "/" + month);

            // 如果目标目录不存在，则创建
            if (!targetFolder.exists()) {
                targetFolder.mkdirs();
            }

            // 构建真实文件名和路径
            String realFileName = currentUserFolderName + fileSuffix;
            targetFilePath = targetFolder.getPath() + "/" + realFileName;

            // 合并文件,合并成功后上传源文件块
            union(fileFolder.getPath(), targetFilePath, fileInfo.getFileName(), true);

            // 根据文件后缀确定文件类型
            fileTypeEnum = FileTypeEnums.getFileTypeBySuffix(fileSuffix);

            // 如果文件类型为视频
            if (FileTypeEnums.VIDEO == fileTypeEnum) {
                // 对视频文件进行切割
                cutFile4Video(fileId, targetFilePath);

                // 生成视频缩略图,构建缩略图的文件路径，以".png"为后缀
                cover = month + "/" + currentUserFolderName + Constants.IMAGE_PNG_SUFFIX;
                String coverPath = targetFolderName + "/" + cover;
                // 调用工具类方法，为视频生成封面缩略图，尺寸为150px宽
                ScaleFilter.createCover4Video(new File(targetFilePath), Constants.LENGTH_150, new File(coverPath));

                // 如果文件类型为图片
            } else if (FileTypeEnums.IMAGE == fileTypeEnum) {
                // 为图片生成缩略图, 构建缩略图的文件路径，将原文件名中的"."替换为"_."，例如"image.jpg"变为"image_.jpg"
                cover = month + "/" + realFileName.replace(".", "_.");
                String coverPath = targetFolderName + "/" + cover;

                // 创建缩略图, 调用工具类方法，为图片生成缩略图，尺寸为150px宽
                Boolean created = ScaleFilter.createThumbnailWidthFFmpeg(new File(targetFilePath), Constants.LENGTH_150, new File(coverPath), false);
                // 如果创建缩略图失败（例如原图片已经很小，不需要缩小），则直接复制原图作为缩略图
                if (!created) {
                    FileUtils.copyFile(new File(targetFilePath), new File(coverPath));
                }
            }

        } catch (Exception e) {
            // 记录文件转码失败日志
            log.error("文件转码失败，文件Id:{},userId:{}", fileId, webUserDto.getUserId(), e);
            transferSuccess = false;
        } finally {
            // 更新文件状态
            FileInfo updateInfo = FileInfo.builder()
                    .fileSize(new File(targetFilePath).length()) //目标文件的长度
                    .fileCover(cover)  // 设置文件封面
                    .status(transferSuccess ? FileStatusEnums.USING.getStatus() : FileStatusEnums.TRANSFER_FAIL.getStatus()) //设置文件状态
                    .build();

           LambdaQueryWrapper<FileInfo> lambdaQueryWrapper2 = new LambdaQueryWrapper<>();
           lambdaQueryWrapper2
                   .eq(FileInfo::getFileId,fileId)  //文件ID
                   .eq(FileInfo::getUserId,webUserDto.getUserId()) //用户ID
                   .eq(FileInfo::getStatus,FileStatusEnums.TRANSFER.getStatus()); //文件转码中状态

            //更改文件的状态
            fileInfoMapper.update(updateInfo,lambdaQueryWrapper2);
        }
    }

    /**
     * 合并文件块。
     * 该方法用于将多个文件块合并成一个完整的文件。
     * @param dirPath 文件块所在的目录路径
     * @param toFilePath 合并后文件的存储路径
     * @param fileName 要合并的文件的文件名
     * @param delSource 是否删除源文件块
     * @throws BusinessException 当合并过程中出现异常时抛出
     */
    public static void union(String dirPath, String toFilePath, String fileName, boolean delSource) throws BusinessException {
        // 根据dirPath创建File对象 (文件块所在的目录路径)
        File dir = new File(dirPath);

        // 判断文件块目录是否存在，不存在则抛出异常
        if (!dir.exists()) {
            throw new BusinessException("目录不存在");
        }

        // 获取临时文件夹目录下的所有文件块
        File[] fileList = dir.listFiles();

        // 创建合并后的目标文件对象 (合所有文件块 合并后 该文件的存储路径)
        File targetFile = new File(toFilePath);
        //RandomAccessFile对象 允许你在文件中的任何位置开始读/写操作
        RandomAccessFile writeFile = null;

        try {
            // 创建一个随机访问文件流来写入数据到目标文件中 (用于读写合并后的文件)
            //  mode：访问模式 "rw"：打开文件以便读取和写入。如果此文件尚不存在，则尝试创建它。
            writeFile = new RandomAccessFile(targetFile, "rw");
            byte[] b = new byte[1024 * 10]; // 创建一个缓冲区用于读取文件块的内容

            // 遍历文件块目录下的所有文件块, 通过writeFile将所有文件块写入到目标文件对象中
            for (int i = 0; i < fileList.length; i++) {
                int len = -1;
                // 根据文件块的名字创建File对象
                File chunkFile = new File(dirPath + File.separator + i);
                RandomAccessFile readFile = null;

                try {
                    // 创建一个随机访问文件流来读取文件块的内容
                    readFile = new RandomAccessFile(chunkFile, "r");
                    while ((len = readFile.read(b)) != -1) {
                        // 读取文件块的内容并写入到目标文件中
                        writeFile.write(b, 0, len);
                    }
                } catch (Exception e) {
                    log.error("合并分片失败", e);
                    throw new BusinessException("合并文件失败");
                } finally {
                    // 关闭读文件流
                    readFile.close();
                }
            }
        } catch (Exception e) {
            log.error("合并文件:{}失败", fileName, e);
            throw new BusinessException("合并文件" + fileName + "出错了");
        } finally {
            try {
                // 关闭写文件流
                if (null != writeFile) {
                    writeFile.close();
                }
            } catch (IOException e) {
                log.error("关闭流失败", e);
            }

            // 判断是否需要删除源文件块
            if (delSource) {
                if (dir.exists()) {
                    try {
                        // 删除源文件块目录
                        FileUtils.deleteDirectory(dir);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * 根据指定的视频文件路径切分视频文件。
     * @param fileId 视频文件的ID
     * @param videoFilePath 视频文件的存储路径
     */
    private void cutFile4Video(String fileId, String videoFilePath) {
        // 创建与视频文件同名的切片目录
        File tsFolder = new File(videoFilePath.substring(0, videoFilePath.lastIndexOf(".")));
        if (!tsFolder.exists()) {
            tsFolder.mkdirs();
        }

        // 定义视频转换为.ts格式的命令模板
        final String CMD_TRANSFER_2TS = "ffmpeg -y -i %s  -vcodec copy -acodec copy -vbsf h264_mp4toannexb %s";
        // 定义切片命令模板
        final String CMD_CUT_TS = "ffmpeg -i %s -c copy -map 0 -f segment -segment_list %s -segment_time 30 %s/%s_%%4d.ts";

        // 定义.ts文件的保存路径
        String tsPath = tsFolder + "/" + Constants.TS_NAME;

        // 执行命令，将视频转换为.ts格式
        String cmd = String.format(CMD_TRANSFER_2TS, videoFilePath, tsPath);
        ProcessUtils.executeCommand(cmd, false);

        // 执行命令，生成索引文件.m3u8以及切片.ts文件
        cmd = String.format(CMD_CUT_TS, tsPath, tsFolder.getPath() + "/" + Constants.M3U8_NAME, tsFolder.getPath(), fileId);
        ProcessUtils.executeCommand(cmd, false);

        // 删除中间产生的.ts文件
        new File(tsPath).delete();
    }

    /**
     * 获取指定文件
     * @param fileId  文件ID
     * @param userId  用户ID
     * @return
     */
    public FileInfo getFileInfoByFileIdAndUserId(String fileId, String userId) {
        LambdaQueryWrapper<FileInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper
                .eq(FileInfo::getUserId,userId)  // 用户ID
                .eq(FileInfo::getFileId,fileId); // 文件ID
        FileInfo fileInfo = this.fileInfoMapper.selectOne(lambdaQueryWrapper);
        return fileInfo;
    }

    /**
     * 在指定目录下创建新的文件夹。
     *
     * @param filePid 父目录ID
     * @param userId 用户ID
     * @param folderName 新文件夹的名称
     * @return 返回创建的文件夹信息对象
     */
    @Transactional(rollbackFor = Exception.class)
    public FileInfo newFolder(String filePid, String userId, String folderName) {
        // 首先检查同目录下是否存在同名的文件夹
        checkFileName(filePid, userId, folderName, FileFolderTypeEnums.FOLDER.getType());


        // 构建新文件夹的信息对象
        FileInfo fileInfo = FileInfo.builder()
                .fileId(StringTools.getRandomString(Constants.LENGTH_10)) // 生成随机的文件ID
                .userId(userId)    // 用户ID
                .filePid(filePid)  // 文件父级ID
                .fileName(folderName) // 文件名
                .folderType(FileFolderTypeEnums.FOLDER.getType()) //设置为目录类型
                .createTime(new Date())      // 文件创建时间
                .lastUpdateTime(new Date())  // 文件最后修改时间
                .status(FileStatusEnums.USING.getStatus())  // 文件状态
                .delFlag(FileDelFlagEnums.USING.getFlag())  // 文件是否删除的状态
                .build();

        // 将新文件夹的信息插入数据库
        this.fileInfoMapper.insert(fileInfo);

        // 再次查询同目录下是否存在同名的文件夹(这里检查的是数据库里是否存在同名文件夹)

        LambdaQueryWrapper<FileInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper
                .eq(FileInfo::getFolderType,FileFolderTypeEnums.FOLDER.getType()) // 设置文件夹类型为目录类型
                .eq(FileInfo::getFileName,folderName)     // 文件名
                .eq(FileInfo::getFileId,filePid)        // 文件父级ID
                .eq(FileInfo::getUserId,userId)         // 用户ID
                .eq(FileInfo::getDelFlag,FileDelFlagEnums.USING.getFlag());  // 文件是否删除的状态

        Long count = this.fileInfoMapper.selectCount(lambdaQueryWrapper);

        // 如果查到数据库里的同名文件夹数量大于1，抛出异常
        if (count > 1) {
            throw new BusinessException("文件夹" + folderName + "已经存在");
        }

        fileInfo.setLastUpdateTime(new Date());
        return fileInfo; // 返回新创建的文件夹的信息
    }

    /**
     * 检查指定目录下是否存在同名的文件或文件夹。
     * @param filePid 父目录ID
     * @param userId 用户ID
     * @param fileName 要检查的文件或文件夹名
     * @param folderType 文件或文件夹的类型
     */
    private void checkFileName(String filePid, String userId, String fileName, Integer folderType) {

        LambdaQueryWrapper<FileInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper
                .eq(FileInfo::getFolderType,folderType) // 设置文件夹类型为目录类型
                .eq(FileInfo::getFileName,fileName)     // 文件名
                .eq(FileInfo::getFilePid,filePid)        // 文件父级ID
                .eq(FileInfo::getUserId,userId)         // 用户ID
                .eq(FileInfo::getDelFlag,FileDelFlagEnums.USING.getFlag());  // 文件是否删除的状态

        Long count = fileInfoMapper.selectCount(lambdaQueryWrapper);
        // 如果存在同名的文件或文件夹，抛出异常
        if (count > 0) {
            throw new BusinessException("此目录下已存在同名文件，请修改名称");
        }
    }

    /**
     * 重命名指定的文件。
     * @param fileId 文件ID
     * @param userId 用户ID
     * @param fileName 新的文件名
     * @return 返回重命名后的文件信息对象
     */
    @Transactional(rollbackFor = Exception.class)
    public FileInfo rename(String fileId, String userId, String fileName) {
        // 从数据库中查询文件信息
        LambdaQueryWrapper<FileInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper
                .eq(FileInfo::getUserId,userId)
                .eq(FileInfo::getFileId,fileId);
        FileInfo fileInfo = this.fileInfoMapper.selectOne(lambdaQueryWrapper);

        // 如果文件不存在，抛出异常
        if (fileInfo == null) {
            throw new BusinessException("文件不存在");
        }

        // 如果新的文件名与旧的文件名相同，直接返回文件信息对象
        if (fileInfo.getFileName().equals(fileName)) {
            return fileInfo;
        }

        String filePid = fileInfo.getFilePid();
        // 检查新的文件名是否合法
        checkFileName(filePid, userId, fileName, fileInfo.getFolderType());

        // 如果是文件类型，则为新文件名添加文件后缀
        if (FileFolderTypeEnums.FILE.getType().equals(fileInfo.getFolderType())) {
            fileName = fileName + StringTools.getFileSuffix(fileInfo.getFileName());
        }

        //new Date()，它会为你生成一个当前的日期和时间
        Date curDate = new Date();

        // 构建更新的文件信息对象
        FileInfo dbInfo = FileInfo.builder()
                .fileName(fileName)         // 文件名
                .lastUpdateTime(new Date()) // 最后修改时间
                .build();

        // 通过用户ID更新数据库中的文件名
        LambdaQueryWrapper<FileInfo> lambdaQueryWrapper2 = new LambdaQueryWrapper<>();
        lambdaQueryWrapper2.eq(FileInfo::getUserId,userId);
        this.fileInfoMapper.update(dbInfo,lambdaQueryWrapper2);

        // 查询新文件名是否已经存在
        LambdaQueryWrapper<FileInfo> lambdaQueryWrapper3 = new LambdaQueryWrapper<>();
        lambdaQueryWrapper3
                .eq(FileInfo::getFilePid,filePid)           // 文件父级ID
                .eq(FileInfo::getUserId,userId)             // 用户ID
                .eq(FileInfo::getFileName,fileName)         // 文件名
                .eq(FileInfo::getDelFlag,FileDelFlagEnums.USING.getFlag()); // 文件状态为使用中
        Long count = this.fileInfoMapper.selectCount(lambdaQueryWrapper3);

        // 如果新文件名已经存在，则抛出异常
        if (count > 1) {
            throw new BusinessException("文件名" + fileName + "已经存在");
        }

        fileInfo.setFileName(fileName);
        fileInfo.setLastUpdateTime(curDate);

        return fileInfo;
    }

    /**
     * 修改文件的所在文件夹。
     *
     * @param fileIds  要修改的文件ID列表，以逗号分隔。
     * @param filePid  目标文件夹ID。
     * @param userId   用户ID。
     */
    @Transactional(rollbackFor = Exception.class)
    public void changeFileFolder(String fileIds, String filePid, String userId) {
        // 检查文件ID是否与目标文件夹ID相同
        if (fileIds.equals(filePid)) {
            throw new BusinessException(ResponseCodeEnum.CODE_600);
        }
        // 如果目标文件夹ID不为“0”(目标文件夹不是根目录)，查询目标文件夹信息
        if (!Constants.ZERO_STR.equals(filePid)) {
            FileInfo fileInfo = fileInfoService.getFileInfoByFileIdAndUserId(filePid, userId);
            // 检查目标文件夹是否存在且未被删除 （因为文件夹只有使用中状态才能将文件移动进目标文件夹）
            if (fileInfo == null || !FileDelFlagEnums.USING.getFlag().equals(fileInfo.getDelFlag())) {
                throw new BusinessException(ResponseCodeEnum.CODE_600);
            }
        }
        FileInfoQuery query = FileInfoQuery.builder()
                .filePid(filePid)  // 文件父级ID
                .userId(userId)    // 用户ID
                .build();
        // 查询目标文件夹中的文件和文件夹列表
        List<FileInfo> dbFileList = fileInfoMapper.findListByParam(query,null,null);


        // 创建一个以文件名为键的Map，这样可以快速检查文件名是否存在于目标文件夹中
        // 将dbFileList这个集合中的数据转化为Map，文件名是map的key。文件对象是map的value值，如果有重名文件，第二个文件作为map的value值
        Map<String, FileInfo> dbFileNameMap = dbFileList.stream().collect(Collectors.toMap(FileInfo::getFileName, Function.identity(), (file1, file2) -> file2));

        // 将文件ID列表字符串拆分为数组,前端传来的fileIds要移动的文件ID列表，以逗号分隔
        String[] fileIdArray = fileIds.split(",");
        // 查询选中的文件或文件夹的信息 （通过用户id和文件id查询）
        FileInfoQuery query2 = FileInfoQuery.builder()
                .userId(userId)    // 用户ID
                .build();
        List<FileInfo> selectFileList = fileInfoMapper.findListByParam(query2,null,fileIdArray);

        // 遍历选中的文件或文件夹,通过循环依次将选中的文件或文件夹移动到目标文件夹并更新数据库
        for (FileInfo item : selectFileList) {
            // 遍历选中的文件夹，同时根据选中的文件夹名去map中去找value，如果找到了，说明目标文件夹有重名文件
            FileInfo rootFileInfo = dbFileNameMap.get(item.getFileName());
            // 如果在目标文件夹中找到相同的文件名，则重命名选中的文件或文件夹
            LambdaUpdateWrapper<FileInfo> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
            if (rootFileInfo != null) {
                //rename这个方法用来重命名文件
                String fileName = StringTools.rename(item.getFileName());
                lambdaUpdateWrapper.set(FileInfo::getFileName,fileName); // 修改文件名
            }
            //设置父目录的id
            lambdaUpdateWrapper
                    .eq(FileInfo::getFileId,item.getFileId())   // 设置文件ID
                    .eq(FileInfo::getUserId,userId)             // 设置用户ID
                    .set(FileInfo::getFilePid,filePid);         // 修改父级ID
            // 更新数据库中的文件信息，将选中的文件或文件夹移动到目标文件夹
            //就是将数据库中文件id和用户id匹配的数据的父级目录id设置成另一个(就是我们要移动的父目录id位置),就完成移动文件的功能了
            this.fileInfoMapper.update(null,lambdaUpdateWrapper);
        }
    }

    /**
     * 将指定文件移动到回收站。
     *
     * @param userId   用户ID。
     * @param fileIds  要移动到回收站的文件ID列表，以逗号分隔。
     */
    @Transactional(rollbackFor = Exception.class)
    public void removeFile2RecycleBatch(String userId, String fileIds) {
        // 将文件ID列表字符串拆分为数组,前端传来的fileIds要修改的文件ID列表，以逗号分隔
        String[] fileIdArray = fileIds.split(",");
        FileInfoQuery query = FileInfoQuery.builder()
                .userId(userId)      // 用户ID
                .delFlag(FileDelFlagEnums.USING.getFlag())  // 文件状态为使用中
                .build();
        // 查询符合query条件的文件列表
        List<FileInfo> fileInfoList = this.fileInfoMapper.findListByParam(query,null,fileIdArray);
        if (fileInfoList.isEmpty()) {
            return;
        }
        // delFilePidList 这个集合 用来保存所有要删除文件的子文件 （存放需要标记为逻辑删除的文件）
        List<String> delFilePidList = new ArrayList<>();
        // 遍历我们选中的文件，如果它是文件夹，则查询其所有子文件夹的文件ID
        for (FileInfo fileInfo : fileInfoList) {
            if (FileFolderTypeEnums.FOLDER.getType().equals(fileInfo.getFolderType())) {
                //如果是文件夹,继续去递归查询文件夹下使用状态的子文件,将所有使用状态的文件添加到 列表 里面
                findAllSubFolderFileIdList(delFilePidList, userId, fileInfo.getFileId(), FileDelFlagEnums.USING.getFlag());
            }
        }
        // 将 delFilePidList 里面保存的 所有子文件 都标记为 删除状态 （逻辑删除并非真正删除）
        if (!delFilePidList.isEmpty()) {
            FileInfo updateInfo = FileInfo.builder()
                    .delFlag(FileDelFlagEnums.DEL.getFlag())  // 将文件设置成删除状态
                    .build();

            this.fileInfoMapper.updateFileDelFlagBatch(updateInfo, userId, delFilePidList, null, FileDelFlagEnums.USING.getFlag());
        }

        // 将选中的文件或者文件夹标记为回收站状态  （标记为回收站状态，在回收站里面可以看到这个文件，而逻辑删除的文件只有管理员看见）
        List<String> delFileIdList = Arrays.asList(fileIdArray);
        FileInfo fileInfo = FileInfo.builder()
                .recoveryTime(new Date())  // 回收时间
                .delFlag(FileDelFlagEnums.RECYCLE.getFlag()) // 将文件设置成回收站状态
                .build();
        this.fileInfoMapper.updateFileDelFlagBatch(fileInfo, userId, null, delFileIdList, FileDelFlagEnums.USING.getFlag());
    }

    /**
     * 递归查询所有子文件夹的文件ID列表。
     * 这个方法就是符合查询要求的文件保存到 fileIdList 这个列表里面了
     * @param fileIdList  保存文件ID的列表。
     * @param userId     用户ID。
     * @param fileId     当前查询的文件ID。
     * @param delFlag    文件的删除标记。
     */
    private void findAllSubFolderFileIdList(List<String> fileIdList, String userId, String fileId, Integer delFlag) {
        // 将当前文件ID添加到列表中
        fileIdList.add(fileId);
        LambdaQueryWrapper<FileInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper
                .eq(FileInfo::getUserId, userId)      // 用户ID
                .eq(FileInfo::getFilePid, fileId)     // 将当前文件ID作为父级ID来查询
                .eq(FileInfo::getDelFlag, delFlag)    // 文件状态为使用中
                .eq(FileInfo::getFolderType, FileFolderTypeEnums.FOLDER.getType());  // 文件类型为目录

        // 使用当前的fileId作为filePid（父目录ID）去查询数据库，找出所有子文件夹。
        List<FileInfo> fileInfoList = this.fileInfoMapper.selectList(lambdaQueryWrapper);
        // 遍历我们选中的文件，如果它是文件夹，则查询其所有子文件夹的文件ID
        for (FileInfo fileInfo : fileInfoList) {
            // 这个递归会持续到某个文件夹没有任何子文件夹为止
            findAllSubFolderFileIdList(fileIdList, userId, fileInfo.getFileId(), delFlag);
        }
    }

    /**
     * 批量恢复文件。
     *
     * @param userId  用户ID。
     * @param fileIds 要恢复的文件ID列表，以逗号分隔
     */
    @Transactional(rollbackFor = Exception.class)
    public void recoverFileBatch(String userId, String fileIds) {
        // 将文件ID列表字符串拆分为数组,前端传来的fileIds要修改的文件ID列表，以逗号分隔
        String[] fileIdArray = fileIds.split(",");
        FileInfoQuery query = FileInfoQuery.builder()
                .userId(userId)  // 用户ID
                .delFlag(FileDelFlagEnums.RECYCLE.getFlag())  // 文件设置为回收站状态
                .build();

        // 查询符合query条件的文件列表
        List<FileInfo> fileInfoList = this.fileInfoMapper.findListByParam(query,null,fileIdArray);

        // delFileSubFolderFileIdList 用于保存删除状态的文件
        List<String> delFileSubFolderFileIdList = new ArrayList<>();
        // 遍历我们选中的文件，如果它是文件夹，则去递归遍历其所有逻辑删除的子文件 （目的就是为了一起恢复）
        for (FileInfo fileInfo : fileInfoList) {
            if (FileFolderTypeEnums.FOLDER.getType().equals(fileInfo.getFolderType())) {
                // 将所有已被逻辑删除的子文件添加到集合中
                findAllSubFolderFileIdList(delFileSubFolderFileIdList, userId, fileInfo.getFileId(), FileDelFlagEnums.DEL.getFlag());
            }
        }
        // 查询当前用户所有根目录的文件
        FileInfoQuery query2 = FileInfoQuery.builder()
                .userId(userId)  // 用户ID
                .delFlag(FileDelFlagEnums.USING.getFlag())  // 设置文件为使用中状态
                .filePid(Constants.ZERO_STR)  // 设置父级目录为根目录（0代表根目录）
                .build();
        // 将当前用户所有根目录下的文件保存到 allRootFileList 里面
        List<FileInfo> allRootFileList = this.fileInfoMapper.findListByParam(query2,null,null);

        // 将所有根目录文件转为以文件名为键的Map, 后面根据文件名就可以快速定位到根目录下的文件
        Map<String, FileInfo> rootFileMap = allRootFileList.stream().collect(Collectors.toMap(FileInfo::getFileName, Function.identity(), (file1, file2) -> file2));

        // 查询所有选中文件,将其目录下所有逻辑删除的文件更新为使用中状态
        if (!delFileSubFolderFileIdList.isEmpty()) { // delFileSubFolderFileIdList里面存放了所有选中的文件
            FileInfo fileInfo = FileInfo.builder()
                    .delFlag(FileDelFlagEnums.USING.getFlag())  // 将文件状态设置为使用中
                    .build();
            // 以文件的父级ID作为修改条件，将这个父级ID下的所有子文件都恢复为使用中状态
            this.fileInfoMapper.updateFileDelFlagBatch(fileInfo, userId, delFileSubFolderFileIdList, null, FileDelFlagEnums.DEL.getFlag());
        }

        // 将选中的文件标记为正常状态，并将它们的父级目录设置为根目录,fileIdArray是前端选中要批量恢复的文件ID列表
        List<String> delFileIdList = Arrays.asList(fileIdArray);
        FileInfo fileInfo = FileInfo.builder()
                .delFlag(FileDelFlagEnums.USING.getFlag())  // 设置文件状态为使用中状态
                .filePid(Constants.ZERO_STR)                // 设置文件的父级ID为0（根目录）
                .lastUpdateTime(new Date())                 // 设置文件最后修改时间
                .build();
        // 将选中的所有文件的状态从回收站更新回使用中状态（这样就能完成批量恢复了）
        this.fileInfoMapper.updateFileDelFlagBatch(fileInfo, userId, null, delFileIdList, FileDelFlagEnums.RECYCLE.getFlag());

        // 对于每个选中的文件，检查其文件名是否与根目录中的文件名冲突，如果是，则重命名
        for (FileInfo item : fileInfoList) {
            // rootFileMap里面保存了所有根目录的文件
            FileInfo rootFileInfo = rootFileMap.get(item.getFileName());
            // 如果根据文件名能够从map里面找到文件,说明文件名已经存在，重命名被还原的文件名
            if (rootFileInfo != null) {
                // 通过rename进行重命名
                String fileName = StringTools.rename(item.getFileName());
                LambdaUpdateWrapper<FileInfo> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
                lambdaUpdateWrapper
                        .eq(FileInfo::getUserId,userId)           // 设置用户ID
                        .eq(FileInfo::getFileId,item.getFileId()) // 设置文件ID
                        .set(FileInfo::getFileName,fileName);     // 重命名文件名
                // 将重命名的文件更新到数据库
                this.fileInfoMapper.update(null,lambdaUpdateWrapper);
            }
        }
    }

    /**
     * 批量删除文件。
     *
     * @param userId   用户ID。
     * @param fileIds  要删除的文件ID列表，以逗号分隔。
     * @param adminOp  是否是管理员操作。管理员可以直接删除文件，而非管理员则只能删除在回收站中的文件。
     */
    @Transactional(rollbackFor = Exception.class)
    public void delFileBatch(String userId, String fileIds, Boolean adminOp) {
        // 将文件ID列表字符串拆分为数组,前端传来的fileIds要修改的文件ID列表，以逗号分隔
        String[] fileIdArray = fileIds.split(",");
        List<FileInfo> fileInfoList = null;
        if (!adminOp) {
            // 如果不是管理员操作，只查询在回收站中的文件
            FileInfoQuery query1 = FileInfoQuery.builder()
                    .userId(userId)  // 用户ID
                    .delFlag(FileDelFlagEnums.RECYCLE.getFlag())  // 设置文件状态为回收站状态
                    .build();
            fileInfoList = this.fileInfoMapper.findListByParam(query1,null,fileIdArray);

        }else {
            // 如果是管理员,可以查到存在数据库的所有文件
            FileInfoQuery query2 = FileInfoQuery.builder()
                    .userId(userId) // 用户ID
                    .build();
            fileInfoList = this.fileInfoMapper.findListByParam(query2,null,fileIdArray);
        }

        // delFileSubFolderFileIdList 用于保存逻辑删除状态的文件
        List<String> delFileSubFolderFileIdList = new ArrayList<>();
        // 遍历我们选中的文件，如果它是文件夹，则查询其所有子文件
        for (FileInfo fileInfo : fileInfoList) {
            if (FileFolderTypeEnums.FOLDER.getType().equals(fileInfo.getFolderType())) {
                // 这个方法是为了递归去将所有子文件中（已删除状态的）全部添加到集合中
                findAllSubFolderFileIdList(delFileSubFolderFileIdList, userId, fileInfo.getFileId(), FileDelFlagEnums.DEL.getFlag());
            }
        }

        // 如果 集合 不为空, 删除选中文件 子目录下 所有逻辑删除状态的文件 (这里是真正的删除)
        if (!delFileSubFolderFileIdList.isEmpty()) {
            // 将标记为删除状态的文件从数据库删除就是彻底的删除了, 如果不是管理员操作,只能删除标记为删除状态的文件,管理员可以直接删除任意文件
            this.fileInfoMapper.delFileBatch(userId, delFileSubFolderFileIdList, null, adminOp ? null : FileDelFlagEnums.DEL.getFlag());
        }
        // 直接根据选中的文件id去删除选中的回收站的文件, 如果不是管理员操作,用户只能删除在回收站里面的文件,管理员可以随意删除
        this.fileInfoMapper.delFileBatch(userId, null, Arrays.asList(fileIdArray), adminOp ? null : FileDelFlagEnums.RECYCLE.getFlag());

        // 从数据库查询用户已使用的空间大小
        Long useSpace = this.fileInfoMapper.selectUseSpace(userId);
        // 更新用户数据库中已使用的空间信息
        LambdaUpdateWrapper<UserInfo> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        lambdaUpdateWrapper
                .eq(UserInfo::getUserId,userId)    // 根据用户ID查询
                .set(UserInfo::getUseSpace,useSpace);  // 设置用户使用的空间
        this.userInfoMapper.update(null,lambdaUpdateWrapper);

        // 获取缓存中用户的空间使用信息对象
        UserSpaceDto userSpaceDto = redisComponent.getUserSpaceUse(userId);
        userSpaceDto.setUseSpace(useSpace);
        // 更新缓存中的用户空间使用信息
        redisComponent.saveUserSpaceUse(userId, userSpaceDto);
    }

    /**
     * 检查文件的父目录ID 与 分享的文件ID是否 相等
     *
     * @param shareFileID 当前分享的文件ID。
     * @param shareUserId      当前分享用户的ID。
     * @param filePid      前端传来的当前打开的文件的父级ID
     */
    public void checkRootFilePid(String shareFileID, String shareUserId, String filePid) {
        // 判断当前打开的文件的父级ID是否为空，为空则抛出异常
        if (StringTools.isEmpty(filePid)) {
            throw new BusinessException(ResponseCodeEnum.CODE_600);
        }
        // 判断当前分享的文件ID与当前打开文件的父级id是否相同，如果相同则返回,说明打开的文件是用户分享的文件
        if (shareFileID.equals(filePid)) {
            return;
        }
        // 如果当前打开文件的父级ID不是我们分享的文件ID,继续递归去检查打开文件的父级ID的父级ID
        // 简单点说这个递归就是为了一直检查父级ID的上一层目录ID,直到确认这个打开的文件是我们分享的文件的子文件为止
        // 如果检查到根目录还无法确定,说明不是我们分享的文件,不给权限打开
        checkFilePid(shareFileID, filePid, shareUserId);
    }

    /**
     * 递归 去检查文件的父目录ID 与 分享的文件ID是否 相等
     *
     * @param shareFileID   当前分享的文件ID。
     * @param filePid       前端传来的当前打开的文件的父级ID
     * @param shareUserId   当前分享用户的ID
     */
    private void checkFilePid(String shareFileID, String filePid, String shareUserId) {
        // 根据文件的父级ID和分享用户的ID查询文件信息
        LambdaQueryWrapper<FileInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper
                .eq(FileInfo::getFileId,filePid)
                .eq(FileInfo::getUserId,shareUserId);
        FileInfo fileInfo = this.fileInfoMapper.selectOne(lambdaQueryWrapper);
        // 如果文件信息为空，抛出异常
        if (fileInfo == null) {
            throw new BusinessException(ResponseCodeEnum.CODE_600);
        }
        // 如果文件的父目录ID是0，抛出异常,说明不是我们分享的文件
        if (Constants.ZERO_STR.equals(fileInfo.getFilePid())) {
            throw new BusinessException(ResponseCodeEnum.CODE_600);
        }
        // 如果文件的父目录ID与分享的文件ID相同，说明当前打开的文件是我们分享的文件,直接返回
        if (fileInfo.getFilePid().equals(shareFileID)) {
            return;
        }
        // 递归 去检查文件的父目录ID 与 分享的文件ID是否 相等
        checkFilePid(shareFileID, fileInfo.getFilePid(), shareUserId);
    }

    /**
     * 保存文件分享。
     *
     * @param shareRootFilePid 分享的根文件目录ID。
     * @param shareFileIds     要分享的文件ID列表，以逗号分隔。
     * @param myFolderId       我的文件夹ID。
     * @param shareUserId      分享用户的ID。
     * @param cureentUserId    当前用户的ID。
     */
    public void saveShare(String shareRootFilePid, String shareFileIds, String myFolderId, String shareUserId, String cureentUserId) {

        // 查询目标目录的文件列表
        FileInfoQuery fileInfoQuery1 = FileInfoQuery.builder()
                .userId(cureentUserId)  // 设置用户ID
                .filePid(myFolderId)    // 设置父级ID
                .build();
        List<FileInfo> currentFileList = this.fileInfoMapper.findListByParam(fileInfoQuery1,null,null);
        // 将目标目录文件列表转为映射的map，方便后续查找
        Map<String, FileInfo> currentFileMap = currentFileList.stream().collect(Collectors.toMap(FileInfo::getFileName, Function.identity(), (file1, file2) -> file2));
        // 查询要分享的文件列表
        FileInfoQuery fileInfoQuery2 = FileInfoQuery.builder()
                .userId(shareUserId)  // 设置分享的用户ID
                .build();
        // 将要分享的文件ID列表字符串拆分为数组
        String[] shareFileIdArray = shareFileIds.split(",");
        List<FileInfo> shareFileList = this.fileInfoMapper.findListByParam(fileInfoQuery2,null,shareFileIdArray);
        // 为分享的文件进行重命名，避免名称冲突
        List<FileInfo> copyFileList = new ArrayList<>();
        Date curDate = new Date();
        for (FileInfo item : shareFileList) {
            // 如果目标目录中已存在同名文件，进行重命名
            FileInfo haveFile = currentFileMap.get(item.getFileName());
            if (haveFile != null) {
                item.setFileName(StringTools.rename(item.getFileName()));
            }
            findAllSubFile(copyFileList, item, shareUserId, cureentUserId, curDate, myFolderId);
        }
        // 打印要复制文件的数量
        System.out.println(copyFileList.size());
        // 批量插入要复制的文件信息
        fileInfoService2.saveBatch(copyFileList);
    }


    /**
     * 递归地查找并复制所有子文件。
     *
     * @param copyFileList   需要复制的文件列表。
     * @param fileInfo       文件信息对象。
     * @param sourceUserId   原始文件的用户ID。
     * @param currentUserId  当前用户的ID。
     * @param curDate        当前时间。
     * @param newFilePid     新文件的父目录ID。
     */
    private void findAllSubFile(List<FileInfo> copyFileList, FileInfo fileInfo, String sourceUserId, String currentUserId, Date curDate, String newFilePid) {
        // 获取原始文件ID
        String sourceFileId = fileInfo.getFileId();
        // 设置文件创建和最后更新时间为当前时间
        fileInfo.setCreateTime(curDate);
        fileInfo.setLastUpdateTime(curDate);
        // 设置新文件的父目录ID
        fileInfo.setFilePid(newFilePid);
        // 设置新文件的用户ID为当前用户ID
        fileInfo.setUserId(currentUserId);
        // 为新文件生成一个随机的文件ID
        String newFileId = StringTools.getRandomString(Constants.LENGTH_10);
        fileInfo.setFileId(newFileId);
        // 将文件信息添加到需要复制的文件列表中
        copyFileList.add(fileInfo);
        // 如果该文件是文件夹类型
        if (FileFolderTypeEnums.FOLDER.getType().equals(fileInfo.getFolderType())) {
            FileInfoQuery query = FileInfoQuery.builder()
                    .filePid(sourceFileId)  // 设置文件父级ID
                    .userId(sourceUserId)   // 设置用户ID
                    .build();
            // 查询原始文件夹下的所有文件
            List<FileInfo> sourceFileList = this.fileInfoMapper.findListByParam(query,null,null);
            // 对于每一个文件，递归地查找其子文件
            for (FileInfo item : sourceFileList) {
                findAllSubFile(copyFileList, item, sourceUserId, currentUserId, curDate, newFileId);
            }
        }
    }
}




