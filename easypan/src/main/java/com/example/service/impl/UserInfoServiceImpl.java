package com.example.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.component.BusinessException;
import com.example.component.RedisComponent;
import com.example.config.AppConfig;
import com.example.entity.constants.Constants;
import com.example.entity.dto.QQInfoDto;
import com.example.entity.dto.SessionWebUserDto;
import com.example.entity.dto.SysSettingsDto;
import com.example.entity.dto.UserSpaceDto;
import com.example.entity.enums.PageSize;
import com.example.entity.enums.UserStatusEnum;
import com.example.entity.po.FileInfo;
import com.example.entity.po.UserInfo;
import com.example.entity.query.UserInfoQuery;
import com.example.entity.vo.PaginationResultVO;
import com.example.mapper.FileInfoMapper;
import com.example.service.UserInfoService;
import com.example.mapper.UserInfoMapper;
import com.example.utils.JsonUtils;
import com.example.utils.OKHttpUtils;
import com.example.utils.StringTools;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
* @author WuYimin
* @description 针对表【user_info(用户信息)】的数据库操作Service实现
* @createDate 2023-11-21 16:28:06
*/
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo>
    implements UserInfoService{

    @Resource
    private  UserInfoMapper userInfoMapper;

    @Resource
    private EmailCodeServiceImpl emailCodeService;

    @Resource
    private RedisComponent redisComponent;

    @Resource
    private FileInfoMapper fileInfoMapper;

    @Resource
    private AppConfig appConfig;



    /**
     * 注册
     * @param email     邮箱
     * @param nickName  昵称
     * @param password  密码
     * @param emailCode 邮箱验证码
     */
    @Transactional(rollbackFor = Exception.class) // 开启事务，发生异常时进行回滚
    public void register(String email, String nickName, String password, String emailCode) {
        // 通过邮箱查询用户信息
        LambdaQueryWrapper<UserInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(UserInfo::getEmail,email);
        UserInfo userInfo = this.userInfoMapper.selectOne(lambdaQueryWrapper);
        // 检查邮箱是否已经被注册
        if (null != userInfo) {
            throw new BusinessException("邮箱账号已经存在");
        }
        // 通过昵称查询用户信息
        lambdaQueryWrapper.eq(UserInfo::getNickName,nickName);
        UserInfo nickNameUser = this.userInfoMapper.selectOne(lambdaQueryWrapper);
        // 检查昵称是否已经存在
        if (null != nickNameUser) {
            throw new BusinessException("昵称已经存在");
        }
        // 校验邮箱验证码是否正确
        emailCodeService.checkCode(email, emailCode);
        // 生成用户ID
        String userId = StringTools.getRandomNumber(Constants.LENGTH_10);
        // 从Redis中获取系统设置信息
        SysSettingsDto sysSettingsDto = redisComponent.getSysSettingsDto();
        // 创建新的UserInfo对象
        userInfo = UserInfo.builder()
                .userId(userId)   // 设置用户ID
                .nickName(nickName) // 设置用户昵称
                .email(email)       // 设置用户邮箱
                .password(StringTools.encodeByMD5(password)) // 对密码进行MD5加密
                .joinTime(new Date()) // 设置用户加入时间为当前时间
                .status(UserStatusEnum.ENABLE.getStatus()) // 设置用户状态为启用
                .totalSpace(sysSettingsDto.getUserInitUseSpace() * Constants.MB) // 设置用户的总空间为系统设置的初始用户空间
                .useSpace(0L)    // 设置用户已使用空间为0
                .build();
        // 插入新的用户信息到数据库
        this.userInfoMapper.insert(userInfo);
    }

    /**
     * 登录
     * @param email    邮箱
     * @param password 密码
     * @return
     */
    public SessionWebUserDto login(String email, String password) {
        // 通过邮箱查询用户信息
        LambdaQueryWrapper<UserInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(UserInfo::getEmail,email);
        UserInfo userInfo = this.userInfoMapper.selectOne(lambdaQueryWrapper);
        // 判断用户是否存在，密码是否正确(前端传来的密码就是MD5加密过的)
        if (userInfo == null || !userInfo.getPassword().equals(password)) {
            // 全局异常拦截，抛出自定义业务异常
            throw new BusinessException("账号或者密码错误");
        }
        // 判断用户状态是否为禁用
        if (UserStatusEnum.DISABLE.getStatus().equals(userInfo.getStatus())) {
            // 全局异常拦截，抛出自定义业务异常
            throw new BusinessException("账号已禁用");
        }
        // 创建一个新的UserInfo对象，用于更新用户的最后登录时间
        UserInfo updateInfo = UserInfo.builder()
                .userId(userInfo.getUserId()) // 设置用户id
                .lastLoginTime(new Date())  // 设置最后登录时间为当前时间
                .build();
        // 根据用户ID更新用户信息
        this.userInfoMapper.updateUserById(updateInfo);

        // 创建SessionWebUserDto对象，用于存放用户会话信息
        SessionWebUserDto sessionWebUserDto = new SessionWebUserDto();
        // 设置用户昵称
        sessionWebUserDto.setNickName(userInfo.getNickName());
        // 设置用户ID
        sessionWebUserDto.setUserId(userInfo.getUserId());
        // 判断用户邮箱是否在管理员邮箱列表中
        if (ArrayUtils.contains(appConfig.getAdminEmails().split(","), email)) {
            // 如果是，设置admin属性为true
            sessionWebUserDto.setAdmin(true);
        } else {
            // 如果不是，设置admin属性为false
            sessionWebUserDto.setAdmin(false);
        }
        // 创建UserSpaceDto对象，用于存放用户空间使用情况
        UserSpaceDto userSpaceDto = new UserSpaceDto();
        // 从数据库用户已使用空间大小并设置
        userSpaceDto.setUseSpace(fileInfoMapper.selectUseSpace(userInfo.getUserId()));
        // 设置用户总空间大小
        userSpaceDto.setTotalSpace(userInfo.getTotalSpace());
        // 将用户空间使用情况保存到Redis中
        redisComponent.saveUserSpaceUse(userInfo.getUserId(), userSpaceDto);
        // 返回包含用户会话信息的SessionWebUserDto对象
        return sessionWebUserDto;
    }

    /**
     * 重置密码
     * @param email
     * @param password
     * @param emailCode
     */
    @Transactional(rollbackFor = Exception.class) // 开启事务，有异常时进行回滚
    public void resetPwd(String email, String password, String emailCode) {
        // 根据邮箱地址查询用户信息
        LambdaQueryWrapper<UserInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(UserInfo::getEmail,email);
        UserInfo userInfo = this.userInfoMapper.selectOne(lambdaQueryWrapper);
        // 如果查询不到用户信息，抛出业务异常，提示邮箱账号不存在
        if (null == userInfo) {
            throw new BusinessException("邮箱账号不存在");
        }
        // 校验邮箱验证码是否正确
        emailCodeService.checkCode(email, emailCode);
        // 创建一个新的UserInfo对象，用于更新密码
        UserInfo updateInfo = UserInfo.builder()
                .email(email)  // 根据邮箱地址更新用户的密码
                .password(StringTools.encodeByMD5(password))  // 将新密码进行MD5加密后设置到updateInfo中
                .build();
        // 根据邮箱地址更新用户的密码
        this.userInfoMapper.updateByEmail(updateInfo);
    }


    /**
     * QQ登录
     * @param code
     * @return
     */
    public SessionWebUserDto qqLogin(String code) {
        // 根据code获取QQ登录的accessToken
        String accessToken = getQQAccessToken(code);
        // 根据accessToken获取用户的openId
        String openId = getQQOpenId(accessToken);
        // 根据openId查询用户信息
        LambdaQueryWrapper<UserInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(UserInfo::getQqOpenId,openId);
        UserInfo user = this.userInfoMapper.selectOne(lambdaQueryWrapper);
        String avatar = null;
        // 如果用户信息为空，说明是新用户，需要注册
        if (null == user) {
            // 通过accessToken和openId获取QQ用户的信息
            QQInfoDto qqInfo = getQQUserInfo(accessToken, openId);
            // 从QQ用户信息中获取昵称，如果长度超过150则截取
            String nickName = qqInfo.getNickname();
            nickName = nickName.length() > Constants.LENGTH_150 ? nickName.substring(0, 150) : nickName;

            // 从QQ用户信息中获取用户头像，优先使用高清头像
            avatar = StringTools.isEmpty(qqInfo.getFigureurl_qq_2()) ? qqInfo.getFigureurl_qq_1() : qqInfo.getFigureurl_qq_2();

            user = UserInfo.builder()
                    .qqOpenId(openId)      // 用户的openId
                    .joinTime(new Date())  // 用户注册时间
                    .nickName(nickName)  // 用户昵称
                    .qqAvatar(avatar)  // QQ头像
                    .userId(StringTools.getRandomString(Constants.LENGTH_10)) // 用户ID
                    .lastLoginTime(new Date()) // 最后登录时间
                    .status(UserStatusEnum.ENABLE.getStatus())  // 设置用户账号为处于启用状态
                    .totalSpace(redisComponent.getSysSettingsDto().getUserInitUseSpace() * Constants.MB) // 设置用户的总空间大小
                    .useSpace(0L) // 设置用户使用的空间大小为0
                    .build();

            // 将用户信息插入到数据库
            this.userInfoMapper.insert(user);
            // 重新查询用户信息
            user = this.userInfoMapper.selectOne(lambdaQueryWrapper);
        } else {
            // 如果是老用户，更新用户的最后登录时间
            UserInfo updateInfo = UserInfo.builder()
                    .lastLoginTime(new Date())
                    .build();
            avatar = user.getQqAvatar();
            this.userInfoMapper.update(updateInfo,lambdaQueryWrapper);
        }
        // 如果用户状态被禁用，抛出异常
        if (UserStatusEnum.DISABLE.getStatus().equals(user.getStatus())) {
            throw new BusinessException("账号被禁用无法登录");
        }
        // 设置session用户的信息
        SessionWebUserDto sessionWebUserDto = new SessionWebUserDto();
        sessionWebUserDto.setUserId(user.getUserId());
        sessionWebUserDto.setNickName(user.getNickName());
        sessionWebUserDto.setAvatar(avatar);
        // 检查是否是管理员
        if (ArrayUtils.contains(appConfig.getAdminEmails().split(","), user.getEmail() == null ? "" : user.getEmail())) {
            sessionWebUserDto.setAdmin(true);
        } else {
            sessionWebUserDto.setAdmin(false);
        }

        // 设置用户空间信息
        UserSpaceDto userSpaceDto = new UserSpaceDto();
        userSpaceDto.setUseSpace(fileInfoMapper.selectUseSpace(user.getUserId())); //从数据库查询用户已使用的内存空间
        userSpaceDto.setTotalSpace(user.getTotalSpace());
        // 保存用户空间使用情况
        redisComponent.saveUserSpaceUse(user.getUserId(), userSpaceDto);
        return sessionWebUserDto;
    }


    /**
     * 根据code获取QQ登录的accessToken
     * @param code
     * @return
     */
    private String getQQAccessToken(String code) {
        /**
         * 返回结果是字符串 access_token=*&expires_in=7776000&refresh_token=*
         * 返回错误 callback({UcWebConstants.VIEW_OBJ_RESULT_KEY:111,error_description:"error msg"})
         */
        // 初始化accessToken为null
        String accessToken = null;
        String url = null;
        try {
            // 格式化URL，填充相关参数，这里使用了URLEncoder对回调地址进行编码
            url = String.format(appConfig.getQqUrlAccessToken(), appConfig.getQqAppId(), appConfig.getQqAppKey(), code, URLEncoder.encode(appConfig
                    .getQqUrlRedirect(), "utf-8"));
        } catch (UnsupportedEncodingException e) {
            // 如果URL编码失败，记录错误日志
            log.error("encode失败");
        }
        // 使用OKHttp发起GET请求，获取响应字符串
        String tokenResult = OKHttpUtils.getRequest(url);
        // 判断响应字符串是否为空，或者是否包含错误关键字
        if (tokenResult == null || tokenResult.indexOf(Constants.VIEW_OBJ_RESULT_KEY) != -1) {
            // 如果获取accessToken失败，记录错误日志，并抛出业务异常
            log.error("获取qqToken失败:{}");
            throw new BusinessException("获取qqToken失败");
        }
        // 使用&符号将响应字符串进行分割
        String[] params = tokenResult.split("&");
        // 遍历分割后的字符串数组
        if (params != null && params.length > 0) {
            for (String p : params) {
                // 查找包含access_token的字符串
                if (p.indexOf("access_token") != -1) {
                    // 使用=符号进行分割，并获取accessToken的值
                    accessToken = p.split("=")[1];
                    break;
                }
            }
        }
        // 返回获取到的accessToken
        return accessToken;
    }


    /**
     * 根据accessToken获取用户的openId
     * @param accessToken
     * @return
     * @throws BusinessException
     */
    private String getQQOpenId(String accessToken) throws BusinessException {
        // 格式化URL，插入accessToken
        String url = String.format(appConfig.getQqUrlOpenId(), accessToken);
        // 通过OKHttpUtils发起GET请求，获取OpenID的结果
        String openIDResult = OKHttpUtils.getRequest(url);
        // 处理返回的结果，获取有效的JSON字符串
        String tmpJson = this.getQQResp(openIDResult);
        // 判断处理后的JSON字符串是否为空
        if (tmpJson == null) {
            // 如果为空，记录错误日志，并抛出业务异常
            log.error("调qq接口获取openID失败:tmpJson{}");
            throw new BusinessException("调qq接口获取openID失败");
        }
        // 将JSON字符串转化为Map对象
        Map jsonData = JsonUtils.convertJson2Obj(tmpJson, Map.class);
        // 检查转换后的Map对象是否为空，或是否包含错误关键字
        if (jsonData == null || jsonData.containsKey(Constants.VIEW_OBJ_RESULT_KEY)) {
            // 如果条件满足，记录错误日志，并抛出业务异常
            log.error("调qq接口获取openID失败:{}", (Throwable) jsonData);
            throw new BusinessException("调qq接口获取openID失败");
        }
        // 从Map中获取OpenID并返回
        return String.valueOf(jsonData.get("openid"));
    }

    /**
     * 获取QQ用户的信息
     * @param accessToken
     * @param qqOpenId
     * @return
     * @throws BusinessException
     */
    private QQInfoDto getQQUserInfo(String accessToken, String qqOpenId) throws BusinessException {
        // 格式化URL，插入accessToken、appId和qqOpenId
        String url = String.format(appConfig.getQqUrlUserInfo(), accessToken, appConfig.getQqAppId(), qqOpenId);
        // 通过OKHttpUtils发起GET请求，获取用户信息的结果
        String response = OKHttpUtils.getRequest(url);
        // 检查返回的响应字符串是否不为空
        if (StringUtils.isNotBlank(response)) {
            // 将响应字符串转化为QQInfoDto对象
            QQInfoDto qqInfo = JsonUtils.convertJson2Obj(response, QQInfoDto.class);
            // 检查转换后的对象的ret属性是否不等于0
            if (qqInfo.getRet() != 0) {
                // 如果条件满足，记录错误日志，并抛出业务异常
                log.error("qqInfo:{}");
                throw new BusinessException("调qq接口获取用户信息异常");
            }
            // 返回转换后的QQInfoDto对象
            return qqInfo;
        }
        // 如果响应字符串为空，抛出业务异常
        throw new BusinessException("调qq接口获取用户信息异常");
    }

    /**
     * 处理QQ返回的结果
     * @param result
     * @return
     */
    private String getQQResp(String result) {
        // 判断result是否不为空
        if (StringUtils.isNotBlank(result)) {
            // 在result中查找"callback"的位置
            int pos = result.indexOf("callback");
            // 如果找到了"callback"
            if (pos != -1) {
                // 查找"("的位置，作为json字符串开始的位置
                int start = result.indexOf("(");
                // 查找")"的位置，作为json字符串结束的位置
                int end = result.lastIndexOf(")");
                // 截取result中的json字符串
                String jsonStr = result.substring(start + 1, end - 1);
                // 返回截取到的json字符串
                return jsonStr;
            }
        }
        // 如果result为空或未找到"callback"，则返回null
        return null;
    }

    public PaginationResultVO<UserInfo> findListByPage(UserInfoQuery query) {

        // 判断查询参数中是否有设置每页显示数量，如果没有则默认为15条记录，否则使用设置的数量
        int pageSize = query.getPageSize() == null ? PageSize.SIZE15.getSize() : query.getPageSize();
        int pageNo  = query.getPageNo() == null ? 1: query.getPageNo();

        // 创建分页结果对象，并设置相关属性如：总记录数、每页显示数量、当前页号、总页数和当前页的数据列表
        Page<UserInfo> page = new Page<>(pageNo, pageSize);
        // 调用Mapper的selectPage方法进行分页查询
        LambdaQueryWrapper<UserInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper
                .like(StrUtil.isNotBlank(query.getNickNameFuzzy()),UserInfo::getNickName,query.getNickNameFuzzy())  // 设置用户名模糊查询
                .like(query.getStatus() != null,UserInfo::getStatus,query.getStatus())  // 设置用户状态
                .last("order by " + query.getOrderBy());  // 设置排序规则
        Page<UserInfo> userInfoPage = userInfoMapper.selectPage(page,lambdaQueryWrapper);
        PaginationResultVO<UserInfo> result = new PaginationResultVO((int) userInfoPage.getTotal(), (int) userInfoPage.getSize(), pageNo, (int) userInfoPage.getPages(), userInfoPage.getRecords());
        // 返回分页结果对象
        return result;
    }

    /**
     * 更新用户状态
     * @param userId
     * @param status
     */
    @Transactional(rollbackFor = Exception.class) // 开启事务，有异常时进行回滚
    public void updateUserStatus(String userId, Integer status) {

        LambdaUpdateWrapper<UserInfo> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        lambdaUpdateWrapper
                .eq(UserInfo::getUserId,userId)     // 用户ID作为查询条件
                .set(UserInfo::getStatus,status);   // 设置用户状态
        // 如果新状态是禁用
        if (UserStatusEnum.DISABLE.getStatus().equals(status)) {
            // 设置用户使用空间为0
            lambdaUpdateWrapper.set(UserInfo::getUseSpace,0L);
            // 根据用户ID删除用户的所有文件
            fileInfoMapper.delFileBatch(userId,null,null,null);
        }
        // 根据用户ID更新用户信息
        userInfoMapper.update(null,lambdaUpdateWrapper);
    }

    /**
     * 给用户内存空间继续分配大小（在原有基础上继续添加）
     * @param userId       用户id
     * @param changeSpace  更改的新空间大小
     */
    // 使用@Transactional注解，表明该方法需要进行事务管理，如果发生异常则回滚事务
    @Transactional(rollbackFor = Exception.class)
    public void changeUserSpace(String userId, Integer changeSpace) {
        // 将changeSpace转换为字节，1MB = 1024 * 1024字节
        Long space = changeSpace * Constants.MB;
        // 调用userInfoMapper的updateUserSpace方法，更新用户的空间
        this.userInfoMapper.updateUserSpace(userId, null, space);
        // 调用redisComponent的resetUserSpaceUse方法，重置用户的空间使用情况
        redisComponent.resetUserSpaceUse(userId);
    }
}




