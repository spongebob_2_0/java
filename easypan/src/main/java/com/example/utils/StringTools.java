package com.example.utils;

import com.example.entity.constants.Constants;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;

/**
 * 字符串工具类
 */
public class StringTools {

    // 通过MD5加密给定的字符串
    public static String encodeByMD5(String originString) {
        // 如果输入的字符串为空，则返回null，否则返回加密后的MD5值
        return StringTools.isEmpty(originString) ? null : DigestUtils.md5Hex(originString);
    }

    // 检查一个字符串是否为空
    public static boolean isEmpty(String str) {
        // 如果字符串为null、空字符串、"null"或不可见字符，则返回true
        if (null == str || "".equals(str) || "null".equals(str) || "\u0000".equals(str)) {
            return true;
            // 如果字符串去掉空白字符后是空字符串，也返回true
        } else if ("".equals(str.trim())) {
            return true;
        }
        // 否则返回false
        return false;
    }

    // 获取文件的后缀名
    public static String getFileSuffix(String fileName) {
        // 获取文件名中最后一个"."的位置
        Integer index = fileName.lastIndexOf(".");
        // 如果没有找到，返回空字符串
        if (index == -1) {
            return "";
        }
        // 返回从"."开始的后缀名
        String suffix = fileName.substring(index);
        return suffix;
    }

    // 获取文件名，不包含后缀
    public static String getFileNameNoSuffix(String fileName) {
        // 获取文件名中最后一个"."的位置
        Integer index = fileName.lastIndexOf(".");
        // 如果没有找到，直接返回原文件名
        if (index == -1) {
            return fileName;
        }
        // 截取文件名到"."之前的部分并返回
        fileName = fileName.substring(0, index);
        return fileName;
    }

    // 重命名文件
    public static String rename(String fileName) {
        // 获取不包含后缀的文件名
        String fileNameReal = getFileNameNoSuffix(fileName);
        // 获取文件后缀
        String suffix = getFileSuffix(fileName);
        // 组合新文件名并返回：原文件名 + "_" + 随机字符串 + 后缀
        return fileNameReal + "_" + getRandomString(Constants.LENGTH_5) + suffix;
    }

    // 生成指定长度的随机字符串，包括字母和数字
    public static final String getRandomString(Integer count) {
        return RandomStringUtils.random(count, true, true);
    }

    // 生成指定长度的随机数字字符串
    public static final String getRandomNumber(Integer count) {
        return RandomStringUtils.random(count, false, true);
    }

    // 对标题进行简单的HTML转义，防止XSS攻击
    public static String escapeTitle(String content) {
        // 如果内容为空，直接返回
        if (isEmpty(content)) {
            return content;
        }
        // 将内容中的"<"替换为"&lt;"
        content = content.replace("<", "&lt;");
        return content;
    }

    // 对内容进行HTML转义
    public static String escapeHtml(String content) {
        // 如果内容为空，直接返回
        if (isEmpty(content)) {
            return content;
        }
        // 将内容中的"<"替换为"&lt;"
        content = content.replace("<", "&lt;");
        // 将内容中的空格替换为"&nbsp;"
        content = content.replace(" ", "&nbsp;");
        // 将内容中的换行替换为"<br>"
        content = content.replace("\n", "<br>");
        return content;
    }

    // 检查路径是否安全
    public static boolean pathIsOk(String path) {
        // 如果路径为空，则返回true
        if (StringTools.isEmpty(path)) {
            return true;
        }
        // 如果路径中包含"../"或"..\\"，则返回false
        if (path.contains("../") || path.contains("..\\")) {
            return false;
        }
        // 否则返回true
        return true;
    }
}
