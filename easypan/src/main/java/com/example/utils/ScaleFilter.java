package com.example.utils;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.math.BigDecimal;

/**
 * 处理图片和视频的工具类
 */
public class ScaleFilter {

    // 使用SLF4J创建日志记录器
    private static final Logger logger = LoggerFactory.getLogger(ScaleFilter.class);

    /**
     * 根据指定宽度创建图片的缩略图。
     *
     * @param file 原图片文件
     * @param thumbnailWidth 缩略图的宽度
     * @param targetFile 缩略图的目标文件
     * @param delSource 是否删除原文件
     * @return 如果成功，返回true；否则返回false。
     */
    public static Boolean createThumbnailWidthFFmpeg(File file, int thumbnailWidth, File targetFile, Boolean delSource) {
        try {
            // 读取原图片文件
            BufferedImage src = ImageIO.read(file);
            // 获取原图片的宽度和高度
            int sorceW = src.getWidth();
            int sorceH = src.getHeight();
            // 如果原图片宽度小于或等于指定的缩略图宽度，则返回false
            if (sorceW <= thumbnailWidth) {
                return false;
            }
            // 压缩图片
            compressImage(file, thumbnailWidth, targetFile, delSource);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 根据指定的宽度百分比压缩图片。
     *
     * @param sourceFile 原图片文件
     * @param widthPercentage 宽度的百分比
     * @param targetFile 压缩后的目标文件
     */
    public static void compressImageWidthPercentage(File sourceFile, BigDecimal widthPercentage, File targetFile) {
        try {
            // 计算压缩后的宽度 = 原图片宽度 × 宽度百分比
            BigDecimal widthResult = widthPercentage.multiply(new BigDecimal(ImageIO.read(sourceFile).getWidth()));
            // 压缩图片
            compressImage(sourceFile, widthResult.intValue(), targetFile, true);
        } catch (Exception e) {
            logger.error("压缩图片失败");
        }
    }

    /**
     * 为视频创建封面。
     *
     * @param sourceFile 原视频文件
     * @param width 封面的宽度
     * @param targetFile 封面的目标文件
     */
    public static void createCover4Video(File sourceFile, Integer width, File targetFile) {
        try {
            // 使用ffmpeg命令提取视频的第一帧作为封面
            String cmd = "ffmpeg -i %s -y -vframes 1 -vf scale=%d:%d/a %s";
            ProcessUtils.executeCommand(String.format(cmd, sourceFile.getAbsoluteFile(), width, width, targetFile.getAbsoluteFile()), false);
        } catch (Exception e) {
            logger.error("生成视频封面失败", e);
        }
    }

    /**
     * 使用FFmpeg工具压缩图片。
     *
     * @param sourceFile 原图片文件
     * @param width 压缩后的宽度
     * @param targetFile 压缩后的目标文件
     * @param delSource 是否删除原文件
     */
    public static void compressImage(File sourceFile, Integer width, File targetFile, Boolean delSource) {
        try {
            // 使用ffmpeg命令压缩图片
            String cmd = "ffmpeg -i %s -vf scale=%d:-1 %s -y";
            ProcessUtils.executeCommand(String.format(cmd, sourceFile.getAbsoluteFile(), width, targetFile.getAbsoluteFile()), false);
            // 根据条件删除原文件
            if (delSource) {
                FileUtils.forceDelete(sourceFile);
            }
        } catch (Exception e) {
            logger.error("压缩图片失败");
        }
    }

    public static void main(String[] args) {
        // 示例：压缩图片到指定宽度的70%
        compressImageWidthPercentage(new File("C:\\Users\\Administrator\\Pictures\\微信图片_20230107141436.png"), new BigDecimal(0.7),
                new File("C:\\Users\\Administrator" +
                        "\\Pictures" +
                        "\\微信图片_202106281029182.jpg"));
    }
}
