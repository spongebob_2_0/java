package com.example.utils;

import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * CopyTools工具类提供对象及对象列表的复制功能。
 */
public class CopyTools {

    /**
     * 将一个源对象列表复制到一个目标对象列表中。
     *
     * @param sList  源对象列表
     * @param classz 目标对象的类型
     * @param <T>    目标对象的类型
     * @param <S>    源对象的类型
     * @return 一个新的目标对象列表，其中包含从源对象列表复制的数据
     */
    public static <T, S> List<T> copyList(List<S> sList, Class<T> classz) {
        // 创建一个空的目标对象列表
        List<T> list = new ArrayList<T>();

        // 遍历源对象列表
        for (S s : sList) {
            T t = null;
            try {
                // 使用目标对象的无参构造函数创建一个新的实例
                t = classz.newInstance();
            } catch (Exception e) {
                // 如果有任何异常，打印堆栈跟踪
                e.printStackTrace();
            }

            // 使用Spring的BeanUtils.copyProperties方法将源对象的属性复制到目标对象
            BeanUtils.copyProperties(s, t);

            // 将复制后的目标对象添加到目标对象列表
            list.add(t);
        }

        // 返回填充了复制数据的目标对象列表
        return list;
    }

    /**
     * 将一个源对象复制到一个新的目标对象中。
     *
     * @param s      源对象
     * @param classz 目标对象的类型
     * @param <T>    目标对象的类型
     * @param <S>    源对象的类型
     * @return 一个新的目标对象，其中包含从源对象复制的数据
     */
    public static <T, S> T copy(S s, Class<T> classz) {
        T t = null;
        try {
            // 使用目标对象的无参构造函数创建一个新的实例
            t = classz.newInstance();
        } catch (Exception e) {
            // 如果有任何异常，打印堆栈跟踪
            e.printStackTrace();
        }

        // 使用Spring的BeanUtils.copyProperties方法将源对象的属性复制到目标对象
        BeanUtils.copyProperties(s, t);

        // 返回复制后的目标对象
        return t;
    }
}
