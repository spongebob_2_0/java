package com.example.utils;

import com.example.component.BusinessException;
import com.example.entity.enums.ResponseCodeEnum;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 定义OKHttpUtils工具类
 */
public class OKHttpUtils {
    // 定义静态常量，请求超时时间为8秒
    private static final int TIME_OUT_SECONDS = 8;

    // 初始化日志记录器
    private static Logger logger = LoggerFactory.getLogger(OKHttpUtils.class);

    // 定义获取OkHttpClient.Builder的私有方法
    private static OkHttpClient.Builder getClientBuilder() {
        // 创建OkHttpClient.Builder，并设置相关属性
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder().followRedirects(false).retryOnConnectionFailure(false);
        // 设置连接超时和读取超时时间
        clientBuilder.connectTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS).readTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS);
        // 返回clientBuilder实例
        return clientBuilder;
    }

    // 定义获取Request.Builder的私有方法，并传入header参数
    private static Request.Builder getRequestBuilder(Map<String, String> header) {
        // 创建Request.Builder实例
        Request.Builder requestBuilder = new Request.Builder();
        // 判断header是否不为null
        if (null != header) {
            // 遍历header的键值对，将其加入到requestBuilder的header中
            for (Map.Entry<String, String> map : header.entrySet()) {
                String key = map.getKey();
                String value;
                if (map.getValue() == null) {
                    value = "";
                } else {
                    value = map.getValue();
                }
                requestBuilder.addHeader(key, value);
            }
        }
        // 返回requestBuilder实例
        return requestBuilder;
    }

    // 定义获取FormBody.Builder的私有方法，并传入params参数
    private static FormBody.Builder getBuilder(Map<String, String> params) {
        // 创建FormBody.Builder实例
        FormBody.Builder builder = new FormBody.Builder();
        // 判断params是否为null
        if (params == null) {
            return builder;
        }
        // 遍历params的键值对，将其加入到builder中
        for (Map.Entry<String, String> map : params.entrySet()) {
            String key = map.getKey();
            String value;
            if (map.getValue() == null) {
                value = "";
            } else {
                value = map.getValue();
            }
            builder.add(key, value);
        }
        // 返回builder实例
        return builder;
    }

    // 定义公共的GET请求方法，并传入url参数，方法可能抛出BusinessException异常
    public static String getRequest(String url) throws BusinessException {
        ResponseBody responseBody = null;
        try {
            // 获取OkHttpClient.Builder实例
            OkHttpClient.Builder clientBuilder = getClientBuilder();
            // 获取Request.Builder实例
            Request.Builder requestBuilder = getRequestBuilder(null);
            // 构建OkHttpClient实例
            OkHttpClient client = clientBuilder.build();
            // 构建Request实例，设置url
            Request request = requestBuilder.url(url).build();
            // 执行请求，获取Response实例
            Response response = client.newCall(request).execute();
            // 获取响应体
            responseBody = response.body();
            // 将响应体转为字符串
            String responseStr = responseBody.string();
            // 记录日志信息
            logger.info("postRequest请求地址:{},返回信息:{}", url, responseStr);
            // 返回响应字符串
            return responseStr;
        } catch (SocketTimeoutException | ConnectException e) {
            // 捕获到超时异常，记录错误日志，并抛出BusinessException异常
            logger.error("OKhttp POST 请求超时,url:{}", url, e);
            throw new BusinessException(ResponseCodeEnum.CODE_500);
        } catch (Exception e) {
            // 捕获到其他异常，记录错误日志
            logger.error("OKhttp GET 请求异常", e);
            // 返回null
            return null;
        } finally {
            // 最后，关闭响应体
            if (responseBody != null) {
                responseBody.close();
            }
        }
    }

    // 定义公共的POST请求方法，并传入url和params参数，方法可能抛出BusinessException异常
    public static String postRequest(String url, Map<String, String> params) throws BusinessException {
        ResponseBody body = null;
        try {
            // 判断params是否为null，如果是，创建一个新的HashMap实例
            if (params == null) {
                params = new HashMap<>();
            }
            // 创建OkHttpClient.Builder实例，并设置相关属性
            OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder().followRedirects(false).retryOnConnectionFailure(false);
            // 设置连接超时和读取超时时间
            clientBuilder.connectTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS).readTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS);
            // 构建OkHttpClient实例
            OkHttpClient client = clientBuilder.build();
            // 创建FormBody.Builder实例
            FormBody.Builder builder = new FormBody.Builder();
            // 初始化RequestBody实例为null
            RequestBody requestBody = null;
            // 遍历params的键值对，将其加入到builder中
            for (Map.Entry<String, String> map : params.entrySet()) {
                String key = map.getKey();
                String value;
                if (map.getValue() == null) {
                    value = "";
                } else {
                    value = map.getValue();
                }
                builder.add(key, value);
            }
            // 构建RequestBody实例
            requestBody = builder.build();

            // 创建Request.Builder实例
            Request.Builder requestBuilder = new Request.Builder();
            // 构建Request实例，设置url和post的RequestBody
            Request request = requestBuilder.url(url).post(requestBody).build();
            // 执行请求，获取Response实例
            Response response = client.newCall(request).execute();
            // 获取响应体
            body = response.body();
            // 将响应体转为字符串
            String responseStr = body.string();
            // 记录日志信息
            logger.info("postRequest请求地址:{},参数:{},返回信息:{}", url, JsonUtils.convertObj2Json(params), responseStr);
            // 返回响应字符串
            return responseStr;
        } catch (SocketTimeoutException | ConnectException e) {
            // 捕获到超时异常，记录错误日志，并抛出BusinessException异常
            logger.error("OKhttp POST 请求超时,url:{},请求参数：{}", url, JsonUtils.convertObj2Json(params), e);
            throw new BusinessException(ResponseCodeEnum.CODE_500);
        } catch (Exception e) {
            // 捕获到其他异常，记录错误日志
            logger.error("OKhttp POST 请求异常,url:{},请求参数：{}", url, JsonUtils.convertObj2Json(params), e);
            // 返回null
            return null;
        } finally {
            // 最后，关闭响应体
            if (body != null) {
                body.close();
            }
        }
    }
}
