package com.example.utils;

// 导入SLF4J的日志接口和工厂类

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

@Component("redisUtils")
public class RedisUtils<V> {

    // 通过@Resource注解，自动注入RedisTemplate的实例
    @Resource
    private RedisTemplate<String, V> redisTemplate;

    // 定义一个静态的日志对象，用于记录日志
    private static final Logger logger = LoggerFactory.getLogger(RedisUtils.class);

    /**
     * 删除缓存的方法
     *
     * @param key 可以传入一个或多个键值
     */
    public void delete(String... key) {
        // 检查提供的键是否为空，并且长度大于0
        if (key != null && key.length > 0) {
            // 如果只有一个键值
            if (key.length == 1) {
                // 直接删除这个键值对应的缓存
                redisTemplate.delete(key[0]);
            } else {
                // 否则将键值数组转换为集合，并删除这些键值对应的缓存
                redisTemplate.delete((Collection<String>) CollectionUtils.arrayToList(key));
            }
        }
    }

    /**
     * 从Redis中获取缓存的方法
     *
     * @param key 键
     * @return 返回与键对应的值
     */
    public V get(String key) {
        // 如果键为空则返回null，否则从Redis中获取这个键对应的值
        return key == null ? null : redisTemplate.opsForValue().get(key);
    }

    /**
     * 向Redis中添加缓存的方法
     *
     * @param key   键
     * @param value 值
     * @return 如果添加成功返回true，否则返回false
     */
    public boolean set(String key, V value) {
        try {
            // 使用RedisTemplate向Redis中添加这个键值对
            redisTemplate.opsForValue().set(key, value);
            return true;
        } catch (Exception e) {
            // 如果添加失败，记录错误日志
            logger.error("设置redisKey:{},value:{}失败", key, value);
            return false;
        }
    }

    /**
     * 向Redis中添加缓存并设置其过期时间的方法
     *
     * @param key   键
     * @param value 值
     * @param time  过期时间(单位:秒)，必须大于0，否则设置为无限期
     * @return 如果添加成功返回true，否则返回false
     */
    public boolean setex(String key, V value, long time) {
        try {
            // 检查提供的过期时间是否大于0
            if (time > 0) {
                // 使用RedisTemplate向Redis中添加这个键值对，并设置过期时间
                redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
            } else {
                // 如果没有提供有效的过期时间，只是简单地添加这个键值对
                set(key, value);
            }
            return true;
        } catch (Exception e) {
            // 如果添加失败，记录错误日志
            logger.error("设置redisKey:{},value:{}失败", key, value);
            return false;
        }
    }
}
