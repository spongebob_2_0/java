package com.example.utils;


import com.example.entity.enums.DateTimePatternEnum;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 日期工具类，提供日期格式化、解析以及日期计算等功能
 */
public class DateUtil {

    // 一个私有的静态锁对象，用于synchronized同步块
    private static final Object lockObj = new Object();

    // 存放线程本地版本的SimpleDateFormat的映射，每个线程有自己独立的SimpleDateFormat实例，确保线程安全
    private static Map<String, ThreadLocal<SimpleDateFormat>> sdfMap = new HashMap<String, ThreadLocal<SimpleDateFormat>>();

    // 获取指定格式的SimpleDateFormat对象。使用ThreadLocal确保线程安全
    private static SimpleDateFormat getSdf(final String pattern) {
        ThreadLocal<SimpleDateFormat> tl = sdfMap.get(pattern);
        if (tl == null) {
            synchronized (lockObj) {
                tl = sdfMap.get(pattern);
                if (tl == null) {
                    tl = new ThreadLocal<SimpleDateFormat>() {
                        @Override
                        protected SimpleDateFormat initialValue() {
                            return new SimpleDateFormat(pattern);
                        }
                    };
                    sdfMap.put(pattern, tl);
                }
            }
        }

        return tl.get();
    }

    /**
     * 格式化日期
     *
     * @param date    需要格式化的日期对象
     * @param pattern 日期格式，例如"yyyy-MM-dd HH:mm:ss"
     * @return 返回格式化后的日期字符串
     */
    public static String format(Date date, String pattern) {
        return getSdf(pattern).format(date);
    }

    /**
     * 解析日期字符串
     *
     * @param dateStr 需要解析的日期字符串
     * @param pattern 日期格式，例如"yyyy-MM-dd HH:mm:ss"
     * @return 返回解析得到的Date对象。如果解析失败，返回当前日期
     */
    public static Date parse(String dateStr, String pattern) {
        try {
            return getSdf(pattern).parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    /**
     * 获取指定天数后的日期
     *
     * @param day 指定的天数，可以是负数
     * @return 返回计算后的日期
     */
    public static Date getAfterDate(Integer day) {
        // 获取当前日期和时间的 Calendar 对象实例
        Calendar calendar = Calendar.getInstance();

        // 将指定的天数添加到此 Calendar 对象中
        // 如果 day 是正数，得到的日期将是未来的日期
        // 如果 day 是负数，得到的日期将是过去的日期
        calendar.add(Calendar.DAY_OF_YEAR, day);

        // 将 Calendar 对象转换为 Date 对象并返回
        return calendar.getTime();
    }

    // 测试方法
    public static void main(String[] args) {
        System.out.println(format(getAfterDate(1), DateTimePatternEnum.YYYY_MM_DD_HH_MM_SS.getPattern()));
    }
}
