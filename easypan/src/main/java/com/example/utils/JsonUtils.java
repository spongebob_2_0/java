package com.example.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * JSON工具类
 */
public class JsonUtils {

    // 使用SLF4J创建日志记录器
    private static final Logger logger = LoggerFactory.getLogger(JsonUtils.class);

    /**
     * 将对象转换为JSON字符串
     *
     * @param obj 需要转换的对象
     * @return 转换后的JSON字符串
     */
    public static String convertObj2Json(Object obj) {
        return JSON.toJSONString(obj);
    }

    /**
     * 将JSON字符串转换为指定类型的对象
     *
     * @param json  要转换的JSON字符串
     * @param classz 指定的返回对象类型
     * @param <T> 泛型类型
     * @return 返回转换后的对象
     */
    public static <T> T convertJson2Obj(String json, Class<T> classz) {
        return JSONObject.parseObject(json, classz);
    }

    /**
     * 将JSON数组字符串转换为List集合
     *
     * @param json   要转换的JSON数组字符串
     * @param classz 集合中对象的类型
     * @param <T>  泛型类型
     * @return 返回转换后的List集合
     */
    public static <T> List<T> convertJsonArray2List(String json, Class<T> classz) {
        return JSONArray.parseArray(json, classz);
    }

    // 测试方法
    public static void main(String[] args) {
    }
}
