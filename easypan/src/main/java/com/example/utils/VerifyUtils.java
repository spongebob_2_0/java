package com.example.utils;


import com.example.entity.enums.VerifyRegexEnum;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 验证字符串是否匹配指定正则表达式的工具类
 */
public class VerifyUtils {

    /**
     * 验证给定的字符串是否匹配指定的正则表达式。
     *
     * @param regex 正则表达式字符串
     * @param value 要验证的字符串
     * @return 如果匹配，返回true；否则返回false。
     */
    public static boolean verify(String regex, String value) {
        if (StringTools.isEmpty(value)) {
            return false;
        }
        Pattern pattern = Pattern.compile(regex);  // 编译正则表达式
        Matcher matcher = pattern.matcher(value);  // 创建matcher对象
        return matcher.matches();  // 检查是否匹配
    }

    /**
     * 验证给定的字符串是否匹配指定的正则表达式枚举。
     *
     * @param regex 正则表达式枚举
     * @param value 要验证的字符串
     * @return 如果匹配，返回true；否则返回false。
     */
    public static boolean verify(VerifyRegexEnum regex, String value) {
        return verify(regex.getRegex(), value);  // 调用上面的方法进行验证
    }

    public static void main(String[] args) {
        // 测试文件是否存在
        System.out.println(new File("E:\\代码生成\\..\\workspace-java").exists());
    }
}
