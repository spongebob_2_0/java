package com.example.component;

import com.example.entity.enums.ResponseCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;

/**
 * 使用@RestControllerAdvice注解标注这是一个全局异常处理类
 */
@RestControllerAdvice
@Slf4j
public class AGlobalExceptionHandler {


    //定义错误状态的常量
    protected static final String STATUC_ERROR = "error";

    // 使用@ExceptionHandler标注该方法是一个异常处理方法，用于处理Exception类及其子类的异常
    @ExceptionHandler(value = Exception.class)
    Object handleException(Exception e, HttpServletRequest request) {
        // 记录错误日志，输出请求的地址和错误信息
        log.error("请求错误，请求地址{},错误信息:", request.getRequestURL(), e);

        // 创建一个响应对象
        ResponseVO ajaxResponse = new ResponseVO();

        // 如果捕获的异常是NoHandlerFoundException（即404未找到异常）
        if (e instanceof NoHandlerFoundException) {
            ajaxResponse.setCode(ResponseCodeEnum.CODE_404.getCode());
            ajaxResponse.setInfo(ResponseCodeEnum.CODE_404.getMsg());
            ajaxResponse.setStatus(STATUC_ERROR);
        }
        // 如果捕获的异常是自定义的BusinessException（业务异常）
        else if (e instanceof BusinessException) {
            BusinessException biz = (BusinessException) e;
            ajaxResponse.setCode(biz.getCode() == null ? ResponseCodeEnum.CODE_600.getCode() : biz.getCode());
            ajaxResponse.setInfo(biz.getMessage());
            ajaxResponse.setStatus(STATUC_ERROR);
        }
        // 如果捕获的异常是参数绑定异常或方法参数类型不匹配异常
        else if (e instanceof BindException || e instanceof MethodArgumentTypeMismatchException) {
            ajaxResponse.setCode(ResponseCodeEnum.CODE_600.getCode());
            ajaxResponse.setInfo(ResponseCodeEnum.CODE_600.getMsg());
            ajaxResponse.setStatus(STATUC_ERROR);
        }
        // 如果捕获的异常是主键冲突异常
        else if (e instanceof DuplicateKeyException) {
            ajaxResponse.setCode(ResponseCodeEnum.CODE_601.getCode());
            ajaxResponse.setInfo(ResponseCodeEnum.CODE_601.getMsg());
            ajaxResponse.setStatus(STATUC_ERROR);
        }
        // 其他未明确的异常类型
        else {
            ajaxResponse.setCode(ResponseCodeEnum.CODE_500.getCode());
            ajaxResponse.setInfo(ResponseCodeEnum.CODE_500.getMsg());
            ajaxResponse.setStatus(STATUC_ERROR);
        }

        // 返回处理后的响应对象
        return ajaxResponse;
    }
}
