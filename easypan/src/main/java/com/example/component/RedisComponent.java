package com.example.component;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.entity.constants.Constants;
import com.example.entity.dto.DownloadFileDto;
import com.example.entity.dto.SysSettingsDto;
import com.example.entity.dto.UserSpaceDto;
import com.example.entity.po.UserInfo;
import com.example.mapper.FileInfoMapper;
import com.example.mapper.UserInfoMapper;
import com.example.utils.RedisUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component("redisComponent")
public class RedisComponent {

    // 通过@Resource注解，自动注入RedisUtils的实例
    @Resource
    private RedisUtils redisUtils;

    // 通过@Resource注解，自动注入UserInfoMapper的实例
    @Resource
    private UserInfoMapper userInfoMapper;

    // 通过@Resource注解，自动注入FileInfoMapper的实例
    @Resource
    private FileInfoMapper fileInfoMapper;

    /**
     * 获取系统设置的方法
     *
     * @return 返回SysSettingsDto对象，包含系统设置信息
     */
    public SysSettingsDto getSysSettingsDto() {
        // 从Redis中根据特定的键获取系统设置
        SysSettingsDto sysSettingsDto = (SysSettingsDto) redisUtils.get(Constants.REDIS_KEY_SYS_SETTING);
        if (sysSettingsDto == null) {
            // 如果Redis中没有对应的系统设置，则创建一个新的对象
            sysSettingsDto = new SysSettingsDto();
            // 将新创建的对象保存到Redis中
            redisUtils.set(Constants.REDIS_KEY_SYS_SETTING, sysSettingsDto);
        }
        return sysSettingsDto;
    }

    /**
     * 保存系统设置的方法
     *
     * @param sysSettingsDto 要保存的系统设置数据传输对象
     */
    public void saveSysSettingsDto(SysSettingsDto sysSettingsDto) {
        // 将提供的系统设置对象保存到Redis中
        redisUtils.set(Constants.REDIS_KEY_SYS_SETTING, sysSettingsDto);
    }

    // 保存下载码与文件信息的对应关系到Redis中
    public void saveDownloadCode(String code, DownloadFileDto downloadFileDto) {
        redisUtils.setex(Constants.REDIS_KEY_DOWNLOAD + code, downloadFileDto, Constants.REDIS_KEY_EXPIRES_FIVE_MIN);
    }

    // 从Redis中通过下载码获取对应的文件信息
    public DownloadFileDto getDownloadCode(String code) {
        return (DownloadFileDto) redisUtils.get(Constants.REDIS_KEY_DOWNLOAD + code);
    }

    /**
     * 获取指定用户的空间使用信息
     *
     * @param userId 用户ID
     * @return 返回用户的空间使用信息对象
     */
    public UserSpaceDto getUserSpaceUse(String userId) {
        // 从Redis中根据用户ID获取用户的空间使用信息
        UserSpaceDto spaceDto = (UserSpaceDto) redisUtils.get(Constants.REDIS_KEY_USER_SPACE_USE + userId);
        if (null == spaceDto) {
            // 如果没有找到，创建一个新的空间使用信息对象
            spaceDto = new UserSpaceDto();
            // 从数据库中查询该用户已使用的空间大小
            Long useSpace = this.fileInfoMapper.selectUseSpace(userId);
            // 设置已使用的空间大小
            spaceDto.setUseSpace(useSpace);
            // 设置用户的总空间大小（这里使用了系统默认的初始空间大小）
            spaceDto.setTotalSpace(getSysSettingsDto().getUserInitUseSpace() * Constants.MB);
            // 将新的空间使用信息保存到Redis中，并设置一个过期时间
            redisUtils.setex(Constants.REDIS_KEY_USER_SPACE_USE + userId, spaceDto, Constants.REDIS_KEY_EXPIRES_DAY);
        }
        return spaceDto;
    }

    /**
     * 保存指定用户的空间使用信息到Redis
     *
     * @param userId 用户ID
     * @param userSpaceDto 用户的空间使用信息对象
     */
    public void saveUserSpaceUse(String userId, UserSpaceDto userSpaceDto) {
        // 将用户的空间使用信息保存到Redis中，并设置一个过期时间
        redisUtils.setex(Constants.REDIS_KEY_USER_SPACE_USE + userId, userSpaceDto, Constants.REDIS_KEY_EXPIRES_DAY);
    }

    /**
     * 重新计算并保存指定用户的空间使用信息到Redis
     * @param userId 用户ID
     * @return
     */
    public UserSpaceDto resetUserSpaceUse(String userId) {
        // 创建一个新的空间使用信息对象
        UserSpaceDto spaceDto = new UserSpaceDto();
        // 从数据库中查询该用户已使用的空间大小
        Long useSpace = this.fileInfoMapper.selectUseSpace(userId);
        // 设置已使用的空间大小
        spaceDto.setUseSpace(useSpace);
        // 从数据库中根据用户ID查询用户信息
        LambdaQueryWrapper<UserInfo> lambdaQueryWrapper  = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(UserInfo::getUserId,userId);
        UserInfo userInfo = this.userInfoMapper.selectOne(lambdaQueryWrapper);
        // 设置用户的总空间大小
        spaceDto.setTotalSpace(userInfo.getTotalSpace());
        // 将新的空间使用信息保存到Redis中，并设置一个过期时间
        redisUtils.setex(Constants.REDIS_KEY_USER_SPACE_USE + userId, spaceDto, Constants.REDIS_KEY_EXPIRES_DAY);
        return spaceDto;
    }

    /**
     * 保存用户文件的临时大小到Redis
     * @param userId  用户ID
     * @param fileId  文件ID
     * @param fileSize 临时文件大小
     */
    public void saveFileTempSize(String userId, String fileId, Long fileSize) {
        // 从Redis中获取当前的文件临时大小
        Long currentSize = getFileTempSize(userId, fileId);
        // 将新的文件大小加到当前大小上，并保存到Redis中,设置了过期时间1小时(一般情况下,一个文件一个小时内差不多传输完成了)
        redisUtils.setex(Constants.REDIS_KEY_USER_FILE_TEMP_SIZE + userId + fileId, currentSize + fileSize, Constants.REDIS_KEY_EXPIRES_ONE_HOUR);
    }

    /**
     * 从Redis中获取用户文件的临时大小
     * @param userId 用户ID
     * @param fileId 文件ID
     * @return
     */
    public Long getFileTempSize(String userId, String fileId) {
        // 从Redis中根据特定的键获取文件的临时大小
        Long currentSize = getFileSizeFromRedis(Constants.REDIS_KEY_USER_FILE_TEMP_SIZE + userId + fileId);
        return currentSize;
    }

    // 从Redis中根据特定的键获取文件的临时大小
    private Long getFileSizeFromRedis(String key) {
        // 从Redis中根据提供的键获取对象
        Object sizeObj = redisUtils.get(key);
        if (sizeObj == null) {
            return 0L;  // 如果对象为空，返回0
        }
        // 判断对象的类型并转换为Long类型
        if (sizeObj instanceof Integer) {
            return ((Integer) sizeObj).longValue();
        } else if (sizeObj instanceof Long) {
            return (Long) sizeObj;
        }
        return 0L;  // 如果对象类型不符合期望，返回0
    }
}
