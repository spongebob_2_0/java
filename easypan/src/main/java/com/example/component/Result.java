package com.example.component;

import com.example.entity.enums.ResponseCodeEnum;
import lombok.extern.slf4j.Slf4j;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * Date: 2023-11-21
 * Time: 19:43
 * @author WuYimin
 */
@Slf4j
public class Result {

    //定义成功状态的常量
    protected static final String STATUC_SUCCESS = "success";

    //定义一个方法用于获取成功响应的VO对象 (这个controller层都可以直接使用这个方法)
    public static <T> ResponseVO success(T t) {
        ResponseVO<T> responseVO = new ResponseVO<>();
        responseVO.setStatus(STATUC_SUCCESS); // 设置状态为成功
        responseVO.setCode(ResponseCodeEnum.CODE_200.getCode()); // 设置响应码
        responseVO.setInfo(ResponseCodeEnum.CODE_200.getMsg()); // 设置响应信息
        responseVO.setData(t); // 设置响应数据
        return responseVO; // 返回VO对象
    }
}
