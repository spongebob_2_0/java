package com.example.component;

import com.example.entity.enums.ResponseCodeEnum;

/**
 * BusinessException 是一个自定义的运行时异常类，通常用于处理应用程序中的业务逻辑错误。
 * 这个类扩展了标准的RuntimeException，并添加了一些字段和方法，使其更适合用于捕获和处理业务逻辑中的问题。
 * @author WuYimin
 */
public class BusinessException extends RuntimeException {

    // 用于存储与异常关联的响应代码枚举。
    private ResponseCodeEnum codeEnum;

    // 自定义错误代码。
    private Integer code;

    // 用于存储与此异常关联的消息。
    private String message;

    // 构造方法
    public BusinessException(String message, Throwable e) {
        super(message, e);
        this.message = message;
    }

    public BusinessException(String message) {
        super(message);
        this.message = message;
    }

    public BusinessException(Throwable e) {
        super(e);
    }

    public BusinessException(ResponseCodeEnum codeEnum) {
        super(codeEnum.getMsg());
        this.codeEnum = codeEnum;
        this.code = codeEnum.getCode();
        this.message = codeEnum.getMsg();
    }

    public BusinessException(Integer code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    // 获取与异常关联的响应代码枚举。
    public ResponseCodeEnum getCodeEnum() {
        return codeEnum;
    }

    // 获取自定义的错误代码。
    public Integer getCode() {
        return code;
    }

    // 获取与此异常关联的消息。
    @Override
    public String getMessage() {
        return message;
    }

    /**
     * 重写fillInStackTrace方法。
     * 因为业务异常不需要堆栈信息，所以这个方法被重写以提高性能。
     */
    @Override
    public Throwable fillInStackTrace() {
        return this;
    }

}
