package com.example.component;

import lombok.Data;

/**
 * ResponseVO是一个泛型视图对象(VO)，用于在API接口中返回统一的响应结构。
 * 这个类可以包括一个状态描述、一个错误或成功代码、一个信息字符串以及一个通用的数据对象。
 *
 * @param <T> 数据的类型，可以是任何Java对象或数据结构。
 */
@Data
public class ResponseVO<T> {

    // 响应的状态
    private String status;
    // 响应的代码
    private Integer code;
    // 对于响应的附加信息
    private String info;
    // 泛型数据对象，根据使用的情况，这可以是任何类型的对象。
    private T data;

}
