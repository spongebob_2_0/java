package com.example.task;

import com.example.entity.enums.FileDelFlagEnums;
import com.example.entity.po.FileInfo;
import com.example.entity.query.FileInfoQuery;
import com.example.mapper.FileInfoMapper;
import com.example.service.impl.FileInfoServiceImpl;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 定时文件清理任务
 */
@Component
public class FileCleanTask {


    @Resource
    private FileInfoServiceImpl fileInfoService;

    @Resource
    private FileInfoMapper fileInfoMapper;

    // 使用@Scheduled注解定义了一个定时任务，fixedDelay设置为3分钟（180000毫秒）,所以此方法会每3分钟执行一次
    @Scheduled(fixedDelay = 1000 * 60 * 3)
    public void execute() {
        // 创建一个新的FileInfoQuery对象来查询需要清理的文件
        FileInfoQuery fileInfoQuery = new FileInfoQuery();
        // 设置查询条件为回收站状态
        fileInfoQuery.setDelFlag(FileDelFlagEnums.RECYCLE.getFlag());
        // 设置查询过期的文件
        fileInfoQuery.setQueryExpire(true);

        // 根据上面的查询条件，从fileInfoService中查找文件列表
        List<FileInfo> fileInfoList = fileInfoMapper.findListByParam(fileInfoQuery,null,null);

        // 将文件列表按照用户ID分组，结果是一个映射，键是用户ID，值是该用户的文件列表
        Map<String, List<FileInfo>> fileInfoMap = fileInfoList.stream().collect(Collectors.groupingBy(FileInfo::getUserId));

        // 遍历上面得到的映射
        for (Map.Entry<String, List<FileInfo>> entry : fileInfoMap.entrySet()) {
            // 获取当前用户的所有文件ID列表
            List<String> fileIds = entry.getValue().stream().map(p -> p.getFileId()).collect(Collectors.toList());
            // 使用fileInfoService的delFileBatch方法批量删除这些文件
            fileInfoService.delFileBatch(entry.getKey(), String.join(",", fileIds), false);
        }
    }
}
