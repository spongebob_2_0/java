package com.example.mapper;

import com.example.entity.po.EmailCode;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
* @author WuYimin
* @description 针对表【email_code(邮箱验证码)】的数据库操作Mapper
* @createDate 2023-11-21 16:28:06
* @Entity com.example.entity.po.EmailCode
*/
@Repository
public interface EmailCodeMapper extends BaseMapper<EmailCode> {

    /**
     * 设置邮件验证码状态为不可用
     * @param email
     */
    void disableEmailCode(String email);
}




