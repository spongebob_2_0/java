package com.example.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.entity.po.FileInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.entity.query.FileInfoQuery;
import com.example.entity.vo.FileInfoVO;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @author WuYimin
* @description 针对表【file_info(文件信息)】的数据库操作Mapper
* @createDate 2023-11-21 16:28:06
* @Entity com.example.entity.po.FileInfo
*/
@Repository
public interface FileInfoMapper extends BaseMapper<FileInfo> {



    /**
     * 从数据库查询使用的内存空间
     * @param userId
     * @return
     */
    Long selectUseSpace(@Param("userId") String userId);

    /**
     * 分页查询的额外条件
     * @param page
     * @param query
     * @return
     */
    Page<FileInfo> selectPageVo(@Param("page") Page<FileInfo> page, @Param("query") FileInfoQuery query);

    /**
     * 根据路径和用户ID获取当前文件夹信息
     * @param query
     * @param orderBy
     * @param fileIdArray
     * @return
     */
    List<FileInfo> findListByParam(@Param("query") FileInfoQuery query,
                                     @Param("orderBy") String orderBy, @Param("fileIdArray") String[] fileIdArray);

    /**
     * 查询所有目录（移动文件的时候调用）
     * @param query
     * @param fileIdArray  需要排除被选择的文件id
     * @return
     */
    List<FileInfo> findAllFold(@Param("query") FileInfoQuery query, @Param("fileIdArray") String[] fileIdArray);

    void updateFileDelFlagBatch(FileInfo fileInfo,
                                @Param("userId") String userId,
                                @Param("filePidList") List<String> filePidList,
                                @Param("fileIdList") List<String> fileIdList,
                                @Param("oldDelFlag") Integer oldDelFlag);

    void delFileBatch(@Param("userId") String userId,
                      @Param("filePidList") List<String> filePidList,
                      @Param("fileIdList") List<String> fileIdList,
                      @Param("oldDelFlag") Integer oldDelFlag);

}




