package com.example.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.entity.po.FileInfo;
import com.example.entity.po.FileShare;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.entity.query.FileInfoQuery;
import com.example.entity.query.FileShareQuery;
import com.example.entity.vo.FileShareVO;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.stereotype.Repository;

/**
* @author WuYimin
* @description 针对表【file_share(分享信息)】的数据库操作Mapper
* @createDate 2023-11-21 16:28:06
* @Entity com.example.entity.po.FileShare
*/
@Repository
public interface FileShareMapper extends BaseMapper<FileShare> {


    /**
     * 分页查询的额外条件
     * @param page
     * @param query
     * @return
     */
    Page<FileShareVO> selectSharePage(@Param("page") Page<FileShare> page, @Param("query") FileShareQuery query);

    /**
     * 根据用户ID和分享的文件ID数组删除已分享的文件
     * @param shareIdArray
     * @param userId
     * @return
     */
    Integer deleteFileShareBatch(@Param("shareIdArray") String[] shareIdArray,@Param("userId") String userId);

}




