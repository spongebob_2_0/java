package com.example.mapper;

import com.example.entity.po.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
* @author WuYimin
* @description 针对表【user_info(用户信息)】的数据库操作Mapper
* @createDate 2023-11-21 16:28:06
* @Entity com.example.entity.po.UserInfo
*/
@Repository
public interface UserInfoMapper extends BaseMapper<UserInfo> {

    /**
     * 根据用户ID修改用户信息
     * @param updateInfo
     */
    void updateUserById(@Param("bean") UserInfo updateInfo);

    /**
     * 根据邮箱地址更新用户的密码
     * @param updateInfo
     */
    void updateByEmail(@Param("bean") UserInfo updateInfo);

    /**
     * 更新用户使用空间和总空间
     */
    Integer updateUserSpace(@Param("userId") String userId, @Param("useSpace") Long useSpace, @Param("totalSpace") Long totalSpace);
}




