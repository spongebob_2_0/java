package com.example.aspect;

import com.example.annotation.GlobalInterceptor;
import com.example.annotation.VerifyParam;
import com.example.component.BusinessException;
import com.example.config.AppConfig;
import com.example.entity.constants.Constants;
import com.example.entity.dto.SessionWebUserDto;
import com.example.entity.enums.ResponseCodeEnum;
import com.example.entity.po.UserInfo;
import com.example.mapper.UserInfoMapper;
import com.example.utils.StringTools;
import com.example.utils.VerifyUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.List;

@Component("operationAspect")
@Aspect  // @Aspect是一个Spring AOP框架的注解，表示这个类是一个切面。
public class GlobalOperationAspect {

    // 初始化日志对象
    private static Logger logger = LoggerFactory.getLogger(GlobalOperationAspect.class);
    // 定义了几个常量，表示不同的类型名称。
    private static final String TYPE_STRING = "java.lang.String";
    private static final String TYPE_INTEGER = "java.lang.Integer";
    private static final String TYPE_LONG = "java.lang.Long";

    @Resource
    private UserInfoMapper userInfoMapper;
    @Resource
    private AppConfig appConfig;


    /**
     * 定义了一个切入点，表示所有使用@GlobalInterceptor注解的方法都会被这个切面捕获。
     */
    @Pointcut("@annotation(com.example.annotation.GlobalInterceptor)")
    private void requestInterceptor() {
    }

    /**
     * 在捕获的方法执行之前运行的代码
     * @param point
     * @throws BusinessException
     */
    @Before("requestInterceptor()")
    public void interceptorDo(JoinPoint point) throws BusinessException {
        try {
            // 获取目标对象，即被代理的对象
            Object target = point.getTarget();
            // 获取被代理方法的参数值
            Object[] arguments = point.getArgs();
            // 获取被代理方法的名称
            String methodName = point.getSignature().getName();
            // 获取被代理方法的参数类型
            Class<?>[] parameterTypes = ((MethodSignature) point.getSignature()).getMethod().getParameterTypes();
            // 使用反射获取目标对象的方法对象
            Method method = target.getClass().getMethod(methodName, parameterTypes);
            // 获取该方法上的@GlobalInterceptor注解对象
            GlobalInterceptor interceptor = method.getAnnotation(GlobalInterceptor.class);
            // 如果该方法没有@GlobalInterceptor注解，则结束此方法
            if (null == interceptor) {
                return;
            }
            // 校验登录,如果注解中设置了需要检查登录或检查管理员权限，则进行登录检查
            if (interceptor.checkLogin() || interceptor.checkAdmin()) {
                checkLogin(interceptor.checkAdmin());
            }
            // 校验方法参数,如果注解中设置了需要检查方法参数，则进行参数校验
            if (interceptor.checkParams()) {
                validateParams(method, arguments);
            }
        } catch (BusinessException e) {
            logger.error("全局拦截器异常", e);
            throw e;
        } catch (Exception e) {
            logger.error("全局拦截器异常", e);
            throw new BusinessException(ResponseCodeEnum.CODE_500);
        } catch (Throwable e) {
            logger.error("全局拦截器异常", e);
            throw new BusinessException(ResponseCodeEnum.CODE_500);
        }
    }


    /**
     * 校验登录的方法
     * @param checkAdmin
     */
    private void checkLogin(Boolean checkAdmin) {
        // 从当前请求上下文中获取HttpServletRequest对象
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        // 从HttpServletRequest对象中获取HttpSession对象
        HttpSession session = request.getSession();
        // 从HttpSession中获取存储的当前会话用户对象，键为Constants.SESSION_KEY
        SessionWebUserDto sessionUser = (SessionWebUserDto) session.getAttribute(Constants.SESSION_KEY);
        // 如果会话中没有用户对象，并且开发模式已启用
        if (sessionUser == null && appConfig.getDev() != null && appConfig.getDev()) {
            // 查询所有的用户信息
            List<UserInfo> userInfoList = userInfoMapper.selectList(null);
            // 如果用户信息不为空
            if (!userInfoList.isEmpty()) {
                // 获取查询到的第一个用户信息
                UserInfo userInfo = userInfoList.get(0);
                // 初始化会话用户对象
                sessionUser = new SessionWebUserDto();
                sessionUser.setUserId(userInfo.getUserId());  // 设置用户ID
                sessionUser.setNickName(userInfo.getNickName()); // 设置昵称
                sessionUser.setAdmin(true);  // 设置为管理员
                // 将初始化的会话用户对象存入会话中
                session.setAttribute(Constants.SESSION_KEY, sessionUser);
            }
        }
        // 如果会话中仍然没有用户对象
        if (null == sessionUser) {
            // 抛出未登录异常
            throw new BusinessException(ResponseCodeEnum.CODE_901);
        }
        // 如果需要检查用户为管理员，但当前会话用户不是管理员
        if (checkAdmin && !sessionUser.getAdmin()) {
            // 抛出权限不足异常
            throw new BusinessException(ResponseCodeEnum.CODE_404);
        }
    }


    /**
     * 校验方法参数的方法
     * @param m
     * @param arguments
     * @throws BusinessException
     */
    private void validateParams(Method m, Object[] arguments) throws BusinessException {
        // 获取方法的所有参数对象
        Parameter[] parameters = m.getParameters();
        // 遍历方法的每一个参数
        for (int i = 0; i < parameters.length; i++) {
            // 获取当前遍历到的参数对象
            Parameter parameter = parameters[i];
            // 获取对应参数的实际值
            Object value = arguments[i];
            // 尝试获取参数对象上的@VerifyParam注解
            VerifyParam verifyParam = parameter.getAnnotation(VerifyParam.class);
            // 如果当前参数没有@VerifyParam注解，则跳过此次循环，处理下一个参数
            if (verifyParam == null) {
                continue;
            }
            // 判断参数类型是否为String、Long或Integer类型
            if (TYPE_STRING.equals(parameter.getParameterizedType().getTypeName()) ||
                    TYPE_LONG.equals(parameter.getParameterizedType().getTypeName()) ||
                    TYPE_INTEGER.equals(parameter.getParameterizedType().getTypeName())) {
                // 如果参数为基本数据类型，则直接进行值的校验
                checkValue(value, verifyParam);
            } else {
                // 如果参数为复杂对象类型，则进行对象内部属性的校验
                checkObjValue(parameter, value);
            }
        }
    }


    /**
     * 校验对象类型参数的方法
     * @param parameter
     * @param value
     */
    private void checkObjValue(Parameter parameter, Object value) {
        // 使用try-catch结构处理异常
        try {
            // 获取参数的类型名称（全类名）
            String typeName = parameter.getParameterizedType().getTypeName();
            // 根据类型名称动态加载类
            Class classz = Class.forName(typeName);
            // 获取该类的所有声明的字段，包括私有字段
            Field[] fields = classz.getDeclaredFields();
            // 遍历该类的所有字段
            for (Field field : fields) {
                // 获取字段上的VerifyParam注解
                VerifyParam fieldVerifyParam = field.getAnnotation(VerifyParam.class);
                // 如果该字段没有VerifyParam注解，跳过当前循环
                if (fieldVerifyParam == null) {
                    continue;
                }
                // 设置字段为可访问，即可以获取私有字段的值
                field.setAccessible(true);
                // 获取对象value中该字段的值
                Object resultValue = field.get(value);
                // 使用checkValue方法对该字段的值进行验证
                checkValue(resultValue, fieldVerifyParam);
            }
            // 捕获自定义的业务异常
        } catch (BusinessException e) {
            // 使用日志记录异常信息
            logger.error("校验参数失败", e);
            // 抛出捕获的异常
            throw e;
            // 捕获其他异常
        } catch (Exception e) {
            // 使用日志记录异常信息
            logger.error("校验参数失败", e);
            // 抛出通用的业务异常
            throw new BusinessException(ResponseCodeEnum.CODE_600);
        }
    }


    /**
     * 校验基本数据类型参数的方法
     *
     * @param value
     * @param verifyParam
     * @throws BusinessException
     */
    private void checkValue(Object value, VerifyParam verifyParam) throws BusinessException {
        // 判断value是否为空，或其转换为字符串后是否为空
        Boolean isEmpty = value == null || StringTools.isEmpty(value.toString());

        // 如果value为空，则其长度为0；否则，取value转换为字符串后的长度
        Integer length = value == null ? 0 : value.toString().length();

        /**
         * 检查参数是否必传
         * 如果value没有接收到前端传来的参数,但是verifyParam注解要求参数必传,就会抛出异常
         */
        if (isEmpty && verifyParam.required()) {
            throw new BusinessException(ResponseCodeEnum.CODE_600);
        }

        /**
         * 检查参数长度是否符合要求
         * 如果value不为空，并且其长度不在注解上定义的最小值和最大值范围内，则抛出业务异常
         */
        if (!isEmpty && (verifyParam.max() != -1 && verifyParam.max() < length || verifyParam.min() != -1 && verifyParam.min() > length)) {
            throw new BusinessException(ResponseCodeEnum.CODE_600);
        }

        /**
         * 参数是否匹配正则要求
         * 如果value不为空，且注解上的正则表达式不为空，并且value不匹配该正则表达式，则抛出业务异常
         */
        if (!isEmpty && !StringTools.isEmpty(verifyParam.regex().getRegex()) && !VerifyUtils.verify(verifyParam.regex(), String.valueOf(value))) {
            throw new BusinessException(ResponseCodeEnum.CODE_600);
        }
    }

}