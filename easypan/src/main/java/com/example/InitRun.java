package com.example;

import com.example.component.BusinessException;
import com.example.component.RedisComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.sql.DataSource;

// 标记为Spring组件，并为其命名为"initRun"
@Component("initRun")
public class InitRun implements ApplicationRunner {

    // 初始化日志对象
    private static final Logger logger = LoggerFactory.getLogger(InitRun.class);

    // 注入数据源对象
    @Resource
    private DataSource dataSource;

    // 注入Redis组件对象
    @Resource
    private RedisComponent redisComponent;

    /**
     * 当Spring Boot应用启动后，这个方法会被自动执行。
     *
     * @param args 从命令行传入的参数
     */
    @Override
    public void run(ApplicationArguments args) {
        try {
            // 尝试从数据源获取连接，确认数据库连接是否成功
            dataSource.getConnection();
            // 尝试从Redis获取系统设置，确认Redis连接是否成功
            redisComponent.getSysSettingsDto();
            // 如果以上操作均成功，则打印服务启动成功的日志信息
            logger.info("服务启动成功，可以开始愉快的开发了");
        } catch (Exception e) {
            // 如果捕获到异常，则打印数据库或Redis设置失败的日志信息
            logger.error("数据库或者redis设置失败，请检查配置");
            // 抛出业务异常，标识服务启动失败
            throw new BusinessException("服务启动失败");
        }
    }
}
