package com.example.entity.constants;

public class Constants {
    // 字符串常量表示的零,根目录的父目录id被定义为0
    public static final String ZERO_STR = "0";

    // 整数常量表示的零
    public static final Integer ZERO = 0;

    // 整数常量表示的一
    public static final Integer ONE = 1;

    // 整数常量，表示长度为30
    public static final Integer LENGTH_30 = 30;

    // 整数常量，表示长度为10
    public static final Integer LENGTH_10 = 10;

    // 整数常量，表示长度为20
    public static final Integer LENGTH_20 = 20;

    // 整数常量，表示长度为5
    public static final Integer LENGTH_5 = 5;

    // 整数常量，表示长度为15
    public static final Integer LENGTH_15 = 15;

    // 整数常量，表示长度为150
    public static final Integer LENGTH_150 = 150;

    // 整数常量，表示长度为50
    public static final Integer LENGTH_50 = 50;

    // Session中用于存储用户信息的键
    public static final String SESSION_KEY = "session_key";

    // 用于Session共享的键前缀
    public static final String SESSION_SHARE_KEY = "session_share_key_";

    // 文件目录常量
    public static final String FILE_FOLDER_FILE = "/file/";

    // 临时文件目录常量
    public static final String FILE_FOLDER_TEMP = "/temp/";

    // PNG图片文件后缀
    public static final String IMAGE_PNG_SUFFIX = ".png";

    // TS文件名常量
    public static final String TS_NAME = "index.ts";

    // M3U8文件名常量
    public static final String M3U8_NAME = "index.m3u8";

    // 用于检查的验证码键
    public static final String CHECK_CODE_KEY = "check_code_key";

    // 用于通过电子邮件检查的图形验证码键
    public static final String CHECK_CODE_KEY_EMAIL = "check_code_key_email";

    // 头像文件后缀
    public static final String AVATAR_SUFFIX = ".jpg";

    // 头像文件夹名称常量
    public static final String FILE_FOLDER_AVATAR_NAME = "avatar/";

    // 默认头像文件名
    public static final String AVATAR_DEFUALT = "default_avatar.jpg";

    // 用于存放结果对象的键
    public static final String VIEW_OBJ_RESULT_KEY = "result";

    // Redis中键的过期时间设置为1分钟的常量
    public static final Integer REDIS_KEY_EXPIRES_ONE_MIN = 60;

    // Redis中键的过期时间设置为1天的常量
    public static final Integer REDIS_KEY_EXPIRES_DAY = REDIS_KEY_EXPIRES_ONE_MIN * 60 * 24;

    // Redis中键的过期时间设置为1小时的常量
    public static final Integer REDIS_KEY_EXPIRES_ONE_HOUR = REDIS_KEY_EXPIRES_ONE_MIN * 60;

    // MB的字节表示
    public static final Long MB = 1024 * 1024L;

    // Redis中键的过期时间设置为5分钟的常量
    public static final Integer REDIS_KEY_EXPIRES_FIVE_MIN = REDIS_KEY_EXPIRES_ONE_MIN * 5;

    // Redis中的下载键前缀
    public static final String REDIS_KEY_DOWNLOAD = "easypan:download:";

    // Redis中的系统设置键前缀
    public static final String REDIS_KEY_SYS_SETTING = "easypan:syssetting:";

    // Redis中的用户空间使用键前缀
    public static final String REDIS_KEY_USER_SPACE_USE = "easypan:user:spaceuse:";

    // Redis中的用户文件临时大小键前缀
    public static final String REDIS_KEY_USER_FILE_TEMP_SIZE = "easypan:user:file:temp:";
}
