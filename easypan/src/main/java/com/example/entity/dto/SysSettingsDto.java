package com.example.entity.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

// 使用@JsonIgnoreProperties注解，表示在序列化/反序列化JSON时，忽略未知属性
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SysSettingsDto implements Serializable {
    /**
     * 注册邮件的默认标题
     */
    private String registerEmailTitle = "邮箱验证码";

    /**
     * 注册邮件的默认内容，其中%s是一个占位符，用于插入验证码
     */
    private String registerEmailContent = "你好，您的邮箱验证码是：%s";

    /**
     * 用户的初始空间大小为5M
     */
    private Integer userInitUseSpace = 5;


}
