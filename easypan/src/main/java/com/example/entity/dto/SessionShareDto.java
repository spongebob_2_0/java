package com.example.entity.dto;

// 导入Date类，这个类用于处理日期
import java.util.Date;

/**
 * 定义一个名为SessionShareDto的公共类
 */
public class SessionShareDto {

    // 用于存储分享ID
    private String shareId;
    // 用于存储分享用户ID
    private String shareUserId;
    // 用于存储过期时间
    private Date expireTime;
    // 用于存储文件ID
    private String fileId;



    // 定义一个公共的方法getShareId，返回shareId的值
    public String getShareId() {
        return shareId;
    }

    // 定义一个公共的方法setShareId，传入一个字符串参数，用来设置shareId的值
    public void setShareId(String shareId) {
        this.shareId = shareId;
    }

    // 定义一个公共的方法getShareUserId，返回shareUserId的值
    public String getShareUserId() {
        return shareUserId;
    }

    // 定义一个公共的方法setShareUserId，传入一个字符串参数，用来设置shareUserId的值
    public void setShareUserId(String shareUserId) {
        this.shareUserId = shareUserId;
    }

    // 定义一个公共的方法getExpireTime，返回expireTime的值
    public Date getExpireTime() {
        return expireTime;
    }

    // 定义一个公共的方法setExpireTime，传入一个Date类型参数，用来设置expireTime的值
    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }

    // 定义一个公共的方法getFileId，返回fileId的值
    public String getFileId() {
        return fileId;
    }

    // 定义一个公共的方法setFileId，传入一个字符串参数，用来设置fileId的值
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }
}
