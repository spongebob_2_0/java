package com.example.entity.dto;

// 导入Serializable接口，这个接口是Java提供的，实现这个接口表示这个类的对象可以被序列化
import java.io.Serializable;

/**
 * 定义一个名为UserSpaceDto的公共类，这个类实现了Serializable接口
 */
public class UserSpaceDto implements Serializable {

    // 定义一个私有的长整型变量useSpace，用来存储已使用的空间大小
    private Long useSpace;
    // 定义一个私有的长整型变量totalSpace，用来存储总空间大小
    private Long totalSpace;

    // 定义一个公共的方法getUseSpace，返回useSpace的值
    public Long getUseSpace() {
        return useSpace;
    }

    // 定义一个公共的方法setUseSpace，传入一个长整型参数，用来设置useSpace的值
    public void setUseSpace(Long useSpace) {
        this.useSpace = useSpace;
    }

    // 定义一个公共的方法getTotalSpace，返回totalSpace的值
    public Long getTotalSpace() {
        return totalSpace;
    }

    // 定义一个公共的方法setTotalSpace，传入一个长整型参数，用来设置totalSpace的值
    public void setTotalSpace(Long totalSpace) {
        this.totalSpace = totalSpace;
    }
}
