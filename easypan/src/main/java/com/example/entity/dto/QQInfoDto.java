package com.example.entity.dto;  // 定义包名

/**
 * 定义一个表示QQ用户信息的数据传输对象（DTO）
 */
public class QQInfoDto {

    private Integer ret;              // 返回码，用于表示请求的结果状态
    private String msg;               // 返回的消息内容，用于描述请求结果"
    private String nickname;          // QQ用户的昵称
    private String figureurl_qq_1;    // QQ用户的头像URL (40x40像素的小尺寸头像)
    private String figureurl_qq_2;    // QQ用户的另一个头像URL (100x100像素的大尺寸头像)
    private String gender;            // QQ用户的性别

    // 获取返回码的方法
    public Integer getRet() {
        return ret;
    }

    // 设置返回码的方法
    public void setRet(Integer ret) {
        this.ret = ret;
    }

    // 获取返回消息的方法
    public String getMsg() {
        return msg;
    }

    // 设置返回消息的方法
    public void setMsg(String msg) {
        this.msg = msg;
    }

    // 获取QQ用户昵称的方法
    public String getNickname() {
        return nickname;
    }

    // 设置QQ用户昵称的方法
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    // 获取QQ用户的第一个头像URL的方法
    public String getFigureurl_qq_1() {
        return figureurl_qq_1;
    }

    // 设置QQ用户的第一个头像URL的方法
    public void setFigureurl_qq_1(String figureurl_qq_1) {
        this.figureurl_qq_1 = figureurl_qq_1;
    }

    // 获取QQ用户的第二个头像URL的方法
    public String getFigureurl_qq_2() {
        return figureurl_qq_2;
    }

    // 设置QQ用户的第二个头像URL的方法
    public void setFigureurl_qq_2(String figureurl_qq_2) {
        this.figureurl_qq_2 = figureurl_qq_2;
    }

    // 获取QQ用户性别的方法
    public String getGender() {
        return gender;
    }

    // 设置QQ用户性别的方法
    public void setGender(String gender) {
        this.gender = gender;
    }
}
