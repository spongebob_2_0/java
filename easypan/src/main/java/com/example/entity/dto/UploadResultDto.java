package com.example.entity.dto;

// 导入JsonIgnoreProperties，这是Jackson库提供的注解，用于控制JSON序列化和反序列化时的行为

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;


/**
 *  定义一个名为UploadResultDto的公共类，这个类实现了Serializable接口
 */
@JsonIgnoreProperties(ignoreUnknown = true) // 在类上使用@JsonIgnoreProperties注解，意在在JSON序列化和反序列化时忽略未知属性
@Data
public class UploadResultDto implements Serializable {

    // 定义一个私有的字符串变量fileId，用来存储文件ID
    private String fileId;

    // 定义一个私有的字符串变量status，用来存储上传状态
    private String status;
}
