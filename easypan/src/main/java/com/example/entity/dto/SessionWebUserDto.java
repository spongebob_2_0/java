package com.example.entity.dto; // 定义包名，表示这个类属于com.easypan.entity.dto这个包

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

/**
 * 定义一个名为SessionWebUserDto的公共类
 */
@Data
public class SessionWebUserDto {

    // 存储用户昵称
    private String nickName;
    // 存储用户ID
    private String userId;
    // 判断用户是否是管理员
    private Boolean admin;
    // 存储用户头像
    private String avatar;

}
