package com.example.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 定义一个用于文件下载的数据传输对象（DTO）
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DownloadFileDto {

    private String downloadCode;  // 下载编码，可能是用于验证的唯一编码
    private String fileId;        // 文件的唯一标识
    private String fileName;      // 文件的名称
    private String filePath;      // 文件的存储路径

}
