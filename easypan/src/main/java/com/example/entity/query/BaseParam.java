package com.example.entity.query;

/**
 * BaseParam 类表示基础的查询参数，通常用于分页和排序的查询请求。
 * 它封装了与分页和排序相关的属性，如页码、每页的记录数和排序的字段。
 */
public class BaseParam {

	// 代表分页信息的对象。
	private SimplePage simplePage;

	// 页码，表示当前的页数。
	private Integer pageNo;

	// 每页的记录数。
	private Integer pageSize;

	// 用于排序的字段。例如："name DESC" 或 "date_created ASC"
	private String orderBy;

	/**
	 * 获取 SimplePage 对象，它封装了分页相关的信息。
	 * @return 返回 SimplePage 对象。
	 */
	public SimplePage getSimplePage() {
		return simplePage;
	}

	/**
	 * 设置 SimplePage 对象。
	 * @param simplePage 一个 SimplePage 对象。
	 */
	public void setSimplePage(SimplePage simplePage) {
		this.simplePage = simplePage;
	}

	/**
	 * 获取当前的页码。
	 * @return 当前的页码。
	 */
	public Integer getPageNo() {
		return pageNo;
	}

	/**
	 * 设置当前的页码。
	 * @param pageNo 要设置的页码。
	 */
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	/**
	 * 获取每页的记录数。
	 * @return 每页的记录数。
	 */
	public Integer getPageSize() {
		return pageSize;
	}

	/**
	 * 设置每页的记录数。
	 * @param pageSize 要设置的每页记录数。
	 */
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * 设置用于排序的字段。
	 * @param orderBy 一个表示排序字段和顺序的字符串。
	 */
	public void setOrderBy(String orderBy){
		this.orderBy = orderBy;
	}

	/**
	 * 获取用于排序的字段。
	 * @return 返回表示排序字段和顺序的字符串。
	 */
	public String getOrderBy(){
		return this.orderBy;
	}
}
