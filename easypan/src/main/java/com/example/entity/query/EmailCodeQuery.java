package com.example.entity.query;

/**
 * 邮箱验证码参数。
 * 这个类用于查询与电子邮件相关的验证码信息。它继承自 BaseParam 类，
 * 同时增加了与邮箱验证码相关的特定字段。
 */
public class EmailCodeQuery extends BaseParam {

    // 用户的电子邮件地址。
    private String email;

    // 用于模糊查询的电子邮件。
    private String emailFuzzy;

    // 邮箱验证码。
    private String code;

    // 用于模糊查询的验证码。
    private String codeFuzzy;

    // 验证码的创建时间。
    private String createTime;

    // 查询验证码的开始创建时间。
    private String createTimeStart;

    // 查询验证码的结束创建时间。
    private String createTimeEnd;

    // 验证码的使用状态：0 表示未使用，1 表示已使用。
    private Integer status;

    // Getter 和 Setter 方法：

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmailFuzzy(String emailFuzzy) {
        this.emailFuzzy = emailFuzzy;
    }

    public String getEmailFuzzy() {
        return this.emailFuzzy;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

    public void setCodeFuzzy(String codeFuzzy) {
        this.codeFuzzy = codeFuzzy;
    }

    public String getCodeFuzzy() {
        return this.codeFuzzy;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateTime() {
        return this.createTime;
    }

    public void setCreateTimeStart(String createTimeStart) {
        this.createTimeStart = createTimeStart;
    }

    public String getCreateTimeStart() {
        return this.createTimeStart;
    }

    public void setCreateTimeEnd(String createTimeEnd) {
        this.createTimeEnd = createTimeEnd;
    }

    public String getCreateTimeEnd() {
        return this.createTimeEnd;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return this.status;
    }

}
