package com.example.entity.query;


import com.example.entity.enums.PageSize;

/**
 * SimplePage是一个用于分页的实体类。
 * 它封装了分页的基本信息如当前页号、每页的大小、总页数等，并提供了一些方法来计算开始和结束的索引。
 */
public class SimplePage {
	// 当前页号
	private int pageNo;
	// 数据的总数
	private int countTotal;
	// 每页的大小
	private int pageSize;
	// 总的页数
	private int pageTotal;
	// 开始的索引
	private int start;
	// 结束的索引
	private int end;

	/**
	 * 默认构造函数
	 */
	public SimplePage() {
	}

	/**
	 * 构造函数：通过页号、数据总数和每页大小来初始化分页信息
	 *
	 * @param pageNo      当前页号
	 * @param countTotal  数据的总数
	 * @param pageSize    每页的大小
	 */
	public SimplePage(Integer pageNo, int countTotal, int pageSize) {
		if (null == pageNo) {
			pageNo = 0;
		}
		this.pageNo = pageNo;
		this.countTotal = countTotal;
		this.pageSize = pageSize;
		action();
	}

	/**
	 * 构造函数：通过开始和结束的索引来初始化分页信息
	 *
	 * @param start  开始的索引
	 * @param end    结束的索引
	 */
	public SimplePage(int start, int end) {
		this.start = start;
		this.end = end;
	}

	/**
	 * action方法用于计算分页的相关信息，如总页数、开始和结束的索引等。
	 */
	public void action() {
		if (this.pageSize <= 0) {
			this.pageSize = PageSize.SIZE20.getSize();
		}
		if (this.countTotal > 0) {
			this.pageTotal = this.countTotal % this.pageSize == 0 ? this.countTotal / this.pageSize
					: this.countTotal / this.pageSize + 1;
		} else {
			pageTotal = 1;
		}

		if (pageNo <= 1) {
			pageNo = 1;
		}
		if (pageNo > pageTotal) {
			pageNo = pageTotal;
		}
		this.start = (pageNo - 1) * pageSize;
		this.end = this.pageSize;
	}

	// 下面是一系列的getter和setter方法

	public int getStart() {
		return start;
	}

	public int getEnd() {
		return end;
	}

	public int getPageTotal() {
		return pageTotal;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public void setPageTotal(int pageTotal) {
		this.pageTotal = pageTotal;
	}

	public int getCountTotal() {
		return countTotal;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public void setCountTotal(int countTotal) {
		this.countTotal = countTotal;
		this.action();
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
}
