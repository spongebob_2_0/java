package com.example.entity.query;


import lombok.Data;

/**
 * 用户信息查询参数类。
 * 继承自基础参数类BaseParam。
 */
@Data
public class UserInfoQuery extends BaseParam {

	/**
	 * 用户ID
	 */
	private String userId;

	// 用户ID模糊查询
	private String userIdFuzzy;

	/**
	 * 用户昵称
	 */
	private String nickName;

	// 用户昵称模糊查询
	private String nickNameFuzzy;

	/**
	 * 用户邮箱
	 */
	private String email;

	// 用户邮箱模糊查询
	private String emailFuzzy;

	/**
	 * QQ头像URL
	 */
	private String qqAvatar;

	// QQ头像URL模糊查询
	private String qqAvatarFuzzy;

	/**
	 * QQ开放ID
	 */
	private String qqOpenId;

	// QQ开放ID模糊查询
	private String qqOpenIdFuzzy;

	/**
	 * 用户密码
	 */
	private String password;

	// 用户密码模糊查询
	private String passwordFuzzy;

	/**
	 * 用户加入时间
	 */
	private String joinTime;

	// 开始的加入时间，用于时间段查询
	private String joinTimeStart;

	// 结束的加入时间，用于时间段查询
	private String joinTimeEnd;

	/**
	 * 最后登录时间
	 */
	private String lastLoginTime;

	// 开始的最后登录时间，用于时间段查询
	private String lastLoginTimeStart;

	// 结束的最后登录时间，用于时间段查询
	private String lastLoginTimeEnd;

	/**
	 * 用户状态：0代表禁用，1代表正常
	 */
	private Integer status;

	/**
	 * 已使用的空间大小
	 */
	private Long useSpace;

	/**
	 * 总空间大小
	 */
	private Long totalSpace;




}
