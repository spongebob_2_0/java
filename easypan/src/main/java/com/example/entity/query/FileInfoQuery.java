package com.example.entity.query;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 文件信息查询参数。
 * 这个类用于查询与文件相关的信息。它继承自 BaseParam 类，并增加了与文件信息相关的特定字段。
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FileInfoQuery extends BaseParam {

    // 文件的唯一标识符。
    private String fileId;
    // 用于模糊查询的文件ID。
    private String fileIdFuzzy;

    // 文件所属用户的唯一标识符。
    private String userId;
    // 用于模糊查询的用户ID。
    private String userIdFuzzy;

    // 文件的MD5哈希值，用于识别文件内容的唯一性。
    private String fileMd5;
    // 用于模糊查询的文件MD5值。
    private String fileMd5Fuzzy;

    // 文件的父级标识符。用于表示文件夹结构。
    private String filePid;
    // 用于模糊查询的文件父级ID。
    private String filePidFuzzy;

    // 文件的大小，单位为字节。
    private Long fileSize;

    // 文件的名称。
    private String fileName;
    // 用于模糊查询的文件名称。
    private String fileNameFuzzy;

    // 文件的封面图像或预览图地址。
    private String fileCover;
    // 用于模糊查询的文件封面地址。
    private String fileCoverFuzzy;

    // 文件的存储路径。
    private String filePath;
    // 用于模糊查询的文件路径。
    private String filePathFuzzy;

    // 文件的创建时间。
    private String createTime;
    // 查询文件的开始创建时间。
    private String createTimeStart;
    // 查询文件的结束创建时间。
    private String createTimeEnd;

    // 文件的最后更新时间。
    private String lastUpdateTime;
    // 查询文件的开始更新时间。
    private String lastUpdateTimeStart;
    // 查询文件的结束更新时间。
    private String lastUpdateTimeEnd;

    // 表示文件类型，0为文件，1为目录。
    private Integer folderType;

    // 文件的大类别。例如：视频、音频、图片等。
    private Integer fileCategory;

    // 文件的具体类型，例如：PDF、DOC、Excel等。
    private Integer fileType;

    // 文件的转码状态：0表示转码中，1表示转码失败，2表示转码成功。
    private Integer status;

    // 文件放入回收站的时间。
    private String recoveryTime;
    // 查询开始放入回收站的时间。
    private String recoveryTimeStart;
    // 查询结束放入回收站的时间。
    private String recoveryTimeEnd;

    // 文件的删除标记：0表示已删除，1表示在回收站中，2表示正常。
    private Integer delFlag;

    // 用于批量查询的文件ID数组。
    private String[] fileIdArray;
    // 用于批量查询的文件父级ID数组。
    private String[] filePidArray;
    // 用于批量查询时排除的文件ID数组。
    private String[] excludeFileIdArray;

    // 查询文件是否已过期。
    private Boolean queryExpire;

    // 查询文件是否包含昵称。
    private Boolean queryNickName;


}
