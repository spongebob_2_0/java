package com.example.entity.enums;

/**
 * 定义公共枚举类UserStatusEnum，用于表示用户状态
 */
public enum UserStatusEnum {

    // 定义枚举常量，表示用户被禁用状态
    DISABLE(0, "禁用"),
    // 定义枚举常量，表示用户处于启用状态
    ENABLE(1, "启用");

    // 定义私有变量，存储用户状态的整数值
    private Integer status;
    // 定义私有变量，存储用户状态的描述信息
    private String desc;

    // 定义私有构造方法，用于初始化枚举常量的属性值
    UserStatusEnum(Integer status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    // 定义公共静态方法，根据状态值获取对应的枚举常量
    public static UserStatusEnum getByStatus(Integer status) {
        // 遍历枚举常量
        for (UserStatusEnum item : UserStatusEnum.values()) {
            // 如果状态值匹配，则返回对应的枚举常量
            if (item.getStatus().equals(status)) {
                return item;
            }
        }
        // 如果没有匹配的状态值，返回null
        return null;
    }

    // 定义公共方法，获取用户状态的整数值
    public Integer getStatus() {
        return status;
    }

    // 定义公共方法，获取用户状态的描述信息
    public String getDesc() {
        return desc;
    }

    // 定义公共方法，设置用户状态的描述信息
    public void setDesc(String desc) {
        this.desc = desc;
    }
}
