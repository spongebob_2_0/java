package com.example.entity.enums; // 定义包名，表示这个枚举类属于com.easypan.entity.enums这个包

/**
 * 定义一个公共的枚举类FileDelFlagEnums
 */
public enum FileDelFlagEnums {

    // 定义枚举常量，分别代表文件的不同删除状态
    DEL(0, "删除"), // 已删除状态
    RECYCLE(1, "回收站"), // 回收站状态
    USING(2, "使用中"); // 使用中状态

    // 定义一个私有的整数变量flag，用来存储文件的删除标志
    private Integer flag;
    // 定义一个私有的字符串变量desc，用来存储文件的删除状态描述
    private String desc;

    // 定义一个私有的构造方法，接受两个参数，分别是文件的删除标志和删除状态描述
    FileDelFlagEnums(Integer flag, String desc) {
        this.flag = flag;
        this.desc = desc;
    }

    // 定义一个公共的方法getFlag，返回文件的删除标志
    public Integer getFlag() {
        return flag;
    }

    // 定义一个公共的方法getDesc，返回文件的删除状态描述
    public String getDesc() {
        return desc;
    }
}
