package com.example.entity.enums;

import org.apache.commons.lang3.ArrayUtils;

/**
 * 定义一个公共的枚举类FileTypeEnums
 */
public enum FileTypeEnums {
    // 定义枚举常量，分别代表不同的文件类型
    // 每个枚举常量关联一个文件类别，类型编号，文件后缀数组和描述信息
    //1:视频 2:音频  3:图片 4:pdf 5:word 6:excel 7:txt 8:code 9:zip 10:其他文件
    VIDEO(FileCategoryEnums.VIDEO, 1, new String[]{".mp4", ".avi", ".rmvb", ".mkv", ".mov"}, "视频"),
    MUSIC(FileCategoryEnums.MUSIC, 2, new String[]{".mp3", ".wav", ".wma", ".mp2", ".flac", ".midi", ".ra", ".ape", ".aac", ".cda"}, "音频"),
    IMAGE(FileCategoryEnums.IMAGE, 3, new String[]{".jpeg", ".jpg", ".png", ".gif", ".bmp", ".dds", ".psd", ".pdt", ".webp", ".xmp", ".svg", ".tiff"}, "图片"),
    PDF(FileCategoryEnums.DOC, 4, new String[]{".pdf"}, "pdf"),
    WORD(FileCategoryEnums.DOC, 5, new String[]{".docx"}, "word"),
    EXCEL(FileCategoryEnums.DOC, 6, new String[]{".xlsx"}, "excel"),
    TXT(FileCategoryEnums.DOC, 7, new String[]{".txt"}, "txt文本"),
    PROGRAME(FileCategoryEnums.OTHERS, 8, new String[]{".h", ".c", ".hpp", ".hxx", ".cpp", ".cc", ".c++", ".cxx", ".m", ".o", ".s", ".dll", ".cs",
            ".java", ".class", ".js", ".ts", ".css", ".scss", ".vue", ".jsx", ".sql", ".md", ".json", ".html", ".xml"}, "CODE"),
    ZIP(FileCategoryEnums.OTHERS, 9, new String[]{"rar", ".zip", ".7z", ".cab", ".arj", ".lzh", ".tar", ".gz", ".ace", ".uue", ".bz", ".jar", ".iso",
            ".mpq"}, "压缩包"),
    OTHERS(FileCategoryEnums.OTHERS, 10, new String[]{}, "其他");

    // 定义私有变量，包括文件类别、类型编号、文件后缀数组和描述信息
    private FileCategoryEnums category;
    private Integer type;
    private String[] suffixs;
    private String desc;

    // 定义私有的构造方法，用于初始化枚举常量的属性值
    FileTypeEnums(FileCategoryEnums category, Integer type, String[] suffixs, String desc) {
        this.category = category;
        this.type = type;
        this.suffixs = suffixs;
        this.desc = desc;
    }

    // 定义公共的静态方法，通过文件后缀来获取文件类型枚举常量
    public static FileTypeEnums getFileTypeBySuffix(String suffix) {
        // 遍历枚举常量
        for (FileTypeEnums item : FileTypeEnums.values()) {
            // 判断当前枚举常量的后缀数组是否包含指定的后缀
            if (ArrayUtils.contains(item.getSuffixs(), suffix)) {
                return item; // 返回匹配的枚举常量
            }
        }
        return FileTypeEnums.OTHERS; // 如果没有匹配的，返回OTHERS
    }

    // 定义公共的静态方法，通过类型编号来获取文件类型枚举常量
    public static FileTypeEnums getByType(Integer type) {
        // 遍历枚举常量
        for (FileTypeEnums item : FileTypeEnums.values()) {
            // 判断当前枚举常量的类型编号是否与指定的类型编号相等
            if (item.getType().equals(type)) {
                return item; // 返回匹配的枚举常量
            }
        }
        return null; // 如果没有匹配的，返回null
    }

    // 定义公共的方法，用于获取文件后缀数组
    public String[] getSuffixs() {
        return suffixs;
    }

    // 定义公共的方法，用于获取文件类别
    public FileCategoryEnums getCategory() {
        return category;
    }

    // 定义公共的方法，用于获取类型编号
    public Integer getType() {
        return type;
    }

    // 定义公共的方法，用于获取描述信息
    public String getDesc() {
        return desc;
    }
}
