package com.example.entity.enums;

/**
 * 定义一个公共的枚举类FileStatusEnums
 */
public enum FileStatusEnums {

    // 定义枚举常量，分别代表不同的文件状态
    TRANSFER(0, "转码中"), // 转码中状态
    TRANSFER_FAIL(1, "转码失败"), // 转码失败状态
    USING(2, "使用中"); // 使用中状态

    // 定义一个私有的整数变量status，用来存储文件的状态标志
    private Integer status;
    // 定义一个私有的字符串变量desc，用来存储文件状态的描述
    private String desc;

    // 定义一个私有的构造方法，接受两个参数，分别是文件状态标志和状态描述
    FileStatusEnums(Integer status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    // 定义一个公共的方法getStatus，返回文件的状态标志
    public Integer getStatus() {
        return status;
    }

    // 定义一个公共的方法getDesc，返回文件状态的描述
    public String getDesc() {
        return desc;
    }
}
