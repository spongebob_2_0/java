package com.example.entity.enums;

/**
 * 定义一个公共的枚举类FileFolderTypeEnums
 */
public enum FileFolderTypeEnums {

    // 定义枚举常量，分别代表文件和目录
    FILE(0, "文件"), // 文件类型
    FOLDER(1, "目录"); // 目录类型

    // 定义一个私有的整数变量type，用来存储文件或目录的类型标志
    private Integer type;
    // 定义一个私有的字符串变量desc，用来存储文件或目录的类型描述
    private String desc;

    // 定义一个私有的构造方法，接受两个参数，分别是类型标志和类型描述
    FileFolderTypeEnums(Integer type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    // 定义一个公共的方法getType，返回文件或目录的类型标志
    public Integer getType() {
        return type;
    }

    // 定义一个公共的方法getDesc，返回文件或目录的类型描述
    public String getDesc() {
        return desc;
    }
}
