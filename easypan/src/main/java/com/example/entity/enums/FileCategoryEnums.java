package com.example.entity.enums; // 定义包名，表示这个枚举类属于com.easypan.entity.enums这个包

/**
 * 定义一个公共的枚举类FileCategoryEnums
 */
public enum FileCategoryEnums {

    // 定义枚举常量，分别代表不同的文件类别
    VIDEO(1, "video", "视频"), // 视频类别
    MUSIC(2, "music", "音频"), // 音频类别
    IMAGE(3, "image", "图片"), // 图片类别
    DOC(4, "doc", "文档"), // 文档类别
    OTHERS(5, "others", "其他"); // 其他类别

    // 定义一个私有的整数变量category，用来存储文件类别的数字表示
    private Integer category;
    // 定义一个私有的字符串变量code，用来存储文件类别的英文代码
    private String code;
    // 定义一个私有的字符串变量desc，用来存储文件类别的中文描述
    private String desc;

    // 定义一个私有的构造方法，接受三个参数，分别是类别的数字表示、英文代码和中文描述
    FileCategoryEnums(Integer category, String code, String desc) {
        this.category = category;
        this.code = code;
        this.desc = desc;
    }

    // 定义一个公共的静态方法getByCode，根据英文代码获取相应的枚举常量
    public static FileCategoryEnums getByCode(String code) {
        // 遍历枚举常量
        for (FileCategoryEnums item : FileCategoryEnums.values()) {
            // 如果枚举常量的code与传入的code相等，则返回这个枚举常量
            if (item.getCode().equals(code)) {
                return item;
            }
        }
        // 如果没有找到与传入code相等的枚举常量，则返回null
        return null;
    }

    // 定义一个公共的方法getCategory，返回文件类别的数字表示
    public Integer getCategory() {
        return category;
    }

    // 定义一个公共的方法getCode，返回文件类别的英文代码
    public String getCode() {
        return code;
    }
}
