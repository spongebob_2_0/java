package com.example.entity.enums;

/**
 * 定义一个公共的枚举类DateTimePatternEnum
 */
public enum DateTimePatternEnum {

    // 定义枚举常量，代表不同的时间格式
    YYYY_MM_DD_HH_MM_SS("yyyy-MM-dd HH:mm:ss"), // 表示年-月-日 时:分:秒的格式
    YYYY_MM_DD("yyyy-MM-dd"), // 表示年-月-日的格式
    YYYYMM("yyyyMM"); // 表示年月的格式

    // 定义一个私有的字符串变量pattern，用来存储时间格式
    private String pattern;

    // 定义一个私有的构造方法，接受一个字符串参数，用于构造枚举常量
    DateTimePatternEnum(String pattern) {
        this.pattern = pattern;
    }

    // 定义一个公共的方法getPattern，返回pattern的值
    public String getPattern() {
        return pattern;
    }
}
