package com.example.entity.enums;


/**
 * 定义一个名为PageSize的公共枚举类，用于表示不同的分页大小选项
 */
public enum PageSize {

	// 定义枚举常量SIZE15，其大小为15条记录每页
	SIZE15(15),
	// 定义枚举常量SIZE20，其大小为20条记录每页
	SIZE20(20),
	// 定义枚举常量SIZE30，其大小为30条记录每页
	SIZE30(30),
	// 定义枚举常量SIZE40，其大小为40条记录每页
	SIZE40(40),
	// 定义枚举常量SIZE50，其大小为50条记录每页
	SIZE50(50);

	// 声明一个整数型成员变量，用于存储每个枚举常量所对应的分页大小值
	int size;

	// 定义私有构造函数，用于初始化枚举常量时为其分配对应的分页大小值
	private PageSize(int size) {
		this.size = size;
	}

	// 定义公共的getSize方法，用于外部获取该枚举常量对应的分页大小值
	public int getSize() {
		return this.size;
	}
}
