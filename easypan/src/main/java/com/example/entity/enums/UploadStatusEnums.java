package com.example.entity.enums; // 定义包名，该枚举类属于com.easypan.entity.enums包

/**
 * 定义公共枚举类UploadStatusEnums，用于表示上传状态
 */
public enum UploadStatusEnums {

    // 定义枚举常量，分别代表不同的上传状态及其描述
    UPLOAD_SECONDS("upload_seconds", "秒传"),
    UPLOADING("uploading", "上传中"),
    UPLOAD_FINISH("upload_finish", "上传完成");

    // 定义私有变量，分别存储状态代码和描述
    private String code;
    private String desc;

    // 定义私有构造方法，用于初始化枚举常量的属性值
    UploadStatusEnums(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    // 定义公共方法，获取状态代码
    public String getCode() {
        return code;
    }

    // 定义公共方法，获取描述信息
    public String getDesc() {
        return desc;
    }
}
