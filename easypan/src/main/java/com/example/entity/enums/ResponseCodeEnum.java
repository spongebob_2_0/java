package com.example.entity.enums;

/**
 * 定义一个公共的枚举类ResponseCodeEnum，用于表示不同的响应码
 */
public enum ResponseCodeEnum {

    // 定义枚举常量，分别代表不同的响应码和响应消息
    CODE_200(200, "请求成功"),
    CODE_404(404, "请求地址不存在"),
    CODE_600(600, "请求参数错误"),
    CODE_601(601, "信息已经存在"),
    CODE_500(500, "服务器返回错误，请联系管理员"),
    CODE_901(901, "登录超时，请重新登录"),
    CODE_902(902, "分享连接不存在，或者已失效"),
    CODE_903(903, "分享验证失效，请重新验证"),
    CODE_904(904, "网盘空间不足，请扩容");

    // 定义私有变量，分别存储响应码和响应消息
    private Integer code;
    private String msg;

    // 定义私有的构造方法，用于初始化枚举常量的属性值
    ResponseCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    // 定义公共的方法，用于获取响应码
    public Integer getCode() {
        return code;
    }

    // 定义公共的方法，用于获取响应消息
    public String getMsg() {
        return msg;
    }
}
