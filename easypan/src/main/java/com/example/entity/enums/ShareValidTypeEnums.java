package com.example.entity.enums;

/**
 * 定义公共枚举类ShareValidTypeEnums，用于表示分享的有效类型
 */
public enum ShareValidTypeEnums {

    // 定义枚举常量，分别代表不同的分享有效类型和对应的有效天数及描述
    DAY_1(0, 1, "1天"),
    DAY_7(1, 7, "7天"),
    DAY_30(2, 30, "30天"),
    FOREVER(3, -1, "永久有效");

    // 定义私有变量，分别存储类型、有效天数和描述
    private Integer type;
    private Integer days;
    private String desc;

    // 定义私有构造方法，用于初始化枚举常量的属性值
    ShareValidTypeEnums(Integer type, Integer days, String desc) {
        this.type = type;
        this.days = days;
        this.desc = desc;
    }

    // 定义公共静态方法，通过类型值获取对应的枚举常量
    public static ShareValidTypeEnums getByType(Integer type) {
        // 遍历枚举常量
        for (ShareValidTypeEnums typeEnums : ShareValidTypeEnums.values()) {
            // 比较类型值是否相等，相等则返回该枚举常量
            if (typeEnums.getType().equals(type)) {
                return typeEnums;
            }
        }
        // 如果没有找到对应的枚举常量，返回null
        return null;
    }

    // 定义公共方法，获取类型值
    public Integer getType() {
        return type;
    }

    // 定义公共方法，获取有效天数
    public Integer getDays() {
        return days;
    }

    // 定义公共方法，获取描述信息
    public String getDesc() {
        return desc;
    }
}
