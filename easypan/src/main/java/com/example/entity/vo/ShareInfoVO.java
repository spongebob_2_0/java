package com.example.entity.vo;

import com.example.entity.po.FileShare;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * ShareInfoVO是一个视图对象(VO)用于表示分享的文件信息。
 * 这个类提供了文件分享的详细信息，如分享时间、过期时间、文件名等。
 * @author WuYimin
 */
@Data
public class ShareInfoVO {

    // 文件分享的时间。使用@JsonFormat注解格式化日期输出为指定的格式和时区。
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date shareTime;

    // 文件分享的过期时间。使用@JsonFormat注解格式化日期输出为指定的格式和时区。
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date expireTime;

    // 分享用户的昵称
    private String nickName;

    // 被分享的文件名称
    private String fileName;

    // 标记当前用户是否为分享此文件的用户
    private Boolean currentUser;

    // 被分享的文件ID
    private String fileId;

    // 分享用户的头像链接
    private String avatar;

    // 分享用户的ID
    private String userId;

}
