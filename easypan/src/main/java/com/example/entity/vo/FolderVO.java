package com.example.entity.vo;

/**
 * FolderVO是一个视图对象(VO)，代表了文件夹的视图模型。
 * 这个类封装了文件夹的基本信息如文件名和文件ID，通常用于表示前端需要的数据结构或者与其他系统的数据交换。
 */
public class FolderVO {

    // 文件夹的名称
    private String fileName;

    // 文件夹的唯一标识符ID
    private String fileId;

    /**
     * 获取文件夹的名称
     *
     * @return 返回文件夹的名称
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * 设置文件夹的名称
     *
     * @param fileName 文件夹的名称
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * 获取文件夹的唯一标识符ID
     *
     * @return 返回文件夹的ID
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * 设置文件夹的唯一标识符ID
     *
     * @param fileId 文件夹的ID
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }
}
