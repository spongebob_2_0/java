package com.example.entity.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * PaginationResultVO是一个通用的分页视图对象(VO)，用于表示分页结果。
 * 它提供了一个分页结果的基本信息，如总数、每页大小、当前页码、总页数和结果列表。
 *
 * @param <T> 结果列表中的对象类型
 */
@Data
public class PaginationResultVO<T> {

	// 总结果数
	private Integer totalCount;

	// 每页显示的条目数量
	private Integer pageSize;

	// 当前的页码
	private Integer pageNo;

	// 总页数
	private Integer pageTotal;

	// 结果列表
	private List<T> list = new ArrayList<T>();

	/**
	 * 构造函数，初始化分页结果对象的基本属性。
	 *
	 * @param totalCount 总结果数
	 * @param pageSize 每页显示的条目数量
	 * @param pageNo 当前的页码
	 * @param list 结果列表
	 */
	public PaginationResultVO(Integer totalCount, Integer pageSize, Integer pageNo, List<T> list) {
		this.totalCount = totalCount;
		this.pageSize = pageSize;
		this.pageNo = pageNo;
		this.list = list;
	}

	/**
	 * 构造函数，初始化所有属性。
	 *
	 * @param totalCount 总结果数
	 * @param pageSize 每页显示的条目数量
	 * @param pageNo 当前的页码
	 * @param pageTotal 总页数
	 * @param list 结果列表
	 */
	public PaginationResultVO(Integer totalCount, Integer pageSize, Integer pageNo, Integer pageTotal, List<T> list) {
		if (pageNo == 0) {
			pageNo = 1;
		}
		this.totalCount = totalCount;
		this.pageSize = pageSize;
		this.pageNo = pageNo;
		this.pageTotal = pageTotal;
		this.list = list;
	}

	/**
	 * 构造函数，只初始化结果列表。
	 *
	 * @param list 结果列表
	 */
	public PaginationResultVO(List<T> list) {
		this.list = list;
	}

	/**
	 * 默认构造函数。
	 */
	public PaginationResultVO() {
	}


}
