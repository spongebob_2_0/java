package com.example.entity.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import lombok.Builder;
import lombok.Data;

/**
 * 邮箱验证码
 * @TableName email_code
 */
@TableName(value ="email_code")
@Data
@Builder
public class EmailCode implements Serializable {



    /**
     * 邮箱
     */
    private String email;

    /**
     * 编号
     */
    private String code;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 0:未使用  1:已使用
     */
    private Integer status;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}