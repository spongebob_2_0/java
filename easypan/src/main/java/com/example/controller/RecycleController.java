package com.example.controller;

import com.example.annotation.GlobalInterceptor;
import com.example.annotation.VerifyParam;
import com.example.component.ResponseVO;
import com.example.component.Result;
import com.example.entity.dto.SessionWebUserDto;
import com.example.entity.enums.FileDelFlagEnums;
import com.example.entity.query.FileInfoQuery;
import com.example.entity.vo.FileInfoVO;
import com.example.entity.vo.PaginationResultVO;
import com.example.service.impl.FileInfoServiceImpl;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * Description: 回收站
 * Date: 2023-11-25
 * Time: 3:25
 * @author WuYimin
 */
@RestController
@RequestMapping("/recycle")
public class RecycleController extends CommonController{

    @Resource
    private FileInfoServiceImpl fileInfoService;

    /**
     * 加载回收站列表
     * @param session
     * @param query
     * @return
     */
    @RequestMapping("/loadRecycleList")
    @GlobalInterceptor(checkParams = true) //需要检查登录状态和方法参数
    public ResponseVO loadRecycleList(HttpSession session, FileInfoQuery query) {

        // 从session中获取当前登录用户的ID，并设置到查询对象中
        query.setUserId(getUserInfoFromSession(session).getUserId());
        // 设置查询的排序方式为按恢复时间降序
        query.setOrderBy("recovery_time desc");
        // 设置查询的删除标记为回收状态
        query.setDelFlag(FileDelFlagEnums.RECYCLE.getFlag());
        // 调用服务层方法，执行分页查询
        PaginationResultVO result = fileInfoService.findListByPage(query);
        // 将查询结果转换为前端需要的数据格式并返回
        return Result.success(convert2PaginationVO(result, FileInfoVO.class));
    }

    /**
     * 批量恢复指定的文件
     *
     * @param session 用于从中获取当前登录的用户信息
     * @param fileIds 需要恢复的文件ID列表，ID之间使用逗号分隔
     * @return 返回操作成功的响应对象
     */
    @RequestMapping("/recoverFile")
    @GlobalInterceptor(checkParams = true) //需要检查登录状态和方法参数,同时要求参数必传
    public ResponseVO recoverFile(HttpSession session, @VerifyParam(required = true) String fileIds) {

        // 从session中获取当前登录的用户信息
        SessionWebUserDto webUserDto = getUserInfoFromSession(session);
        // 调用服务层方法，根据文件ID列表批量恢复文件
        fileInfoService.recoverFileBatch(webUserDto.getUserId(), fileIds);
        // 返回操作成功的响应对象
        return Result.success(null);
    }

    /**
     * 批量彻底删除指定的文件。
     *
     * @param session HttpSession对象，用于从中获取当前登录的用户信息
     * @param fileIds 需要删除的文件ID列表，ID之间使用逗号分隔
     * @return 返回操作成功的响应对象
     */
    @RequestMapping("/delFile")
    @GlobalInterceptor(checkParams = true) //需要检查登录状态和方法参数,同时要求参数必传
    public ResponseVO delFile(HttpSession session, @VerifyParam(required = true) String fileIds) {

        // 从session中获取当前登录的用户信息
        SessionWebUserDto webUserDto = getUserInfoFromSession(session);
        // 根据文件ID列表批量删除文件，最后一个参数表示是否是管理员操作
        fileInfoService.delFileBatch(webUserDto.getUserId(), fileIds, false);
        return Result.success(null);
    }
}
