package com.example.controller;

import com.example.annotation.GlobalInterceptor;
import com.example.annotation.VerifyParam;
import com.example.component.ResponseVO;
import com.example.component.Result;
import com.example.entity.dto.SessionWebUserDto;
import com.example.entity.po.FileShare;
import com.example.entity.query.FileShareQuery;
import com.example.entity.vo.PaginationResultVO;
import com.example.service.impl.FileShareServiceImpl;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * Description: 分享
 * Date: 2023-11-25
 * Time: 1:25
 * @author WuYimin
 */
@RestController
@RequestMapping("/share")
public class ShareController extends CommonController{

    @Resource
    private FileShareServiceImpl fileShareService;

    /**
     * 加载用户的分享列表。
     *
     * @param session 当前的HTTP会话
     * @param query   文件分享查询对象
     * @return ResponseVO 结果对象，包含分享列表
     */
    @RequestMapping("/loadShareList")
    @GlobalInterceptor(checkParams = true)
    public ResponseVO loadShareList(HttpSession session, FileShareQuery query) {
        // 设置查询排序为按分享时间降序
        query.setOrderBy("share_time desc");

        // 从当前会话中获取用户信息并设置到查询对象中
        SessionWebUserDto userDto = getUserInfoFromSession(session);
        query.setUserId(userDto.getUserId());
        query.setQueryFileName(true);

        // 使用文件分享服务查询分享列表
        PaginationResultVO resultVO = this.fileShareService.findListByPage(query);

        // 返回查询结果
        return Result.success(resultVO);
    }

    /**
     * 分享文件
     *
     * @param session   当前的HTTP会话
     * @param fileId    要分享的文件ID
     * @param validType 有效类型
     * @param code      文件分享的验证码或密码
     * @return ResponseVO 结果对象，确认分享成功或提供错误消息
     */
    @RequestMapping("/shareFile")
    @GlobalInterceptor(checkParams = true)
    public ResponseVO shareFile(HttpSession session,
                                @VerifyParam(required = true) String fileId,
                                @VerifyParam(required = true) Integer validType,
                                String code) {
        // 从当前会话中获取用户信息
        SessionWebUserDto userDto = getUserInfoFromSession(session);

        // 设置分享的文件信息
        FileShare share = FileShare.builder()
                .fileId(fileId)    // 文件ID
                .validType(validType)  // 分享有效时间
                .code(code)  // 分享码
                .userId(userDto.getUserId())  // 用户ID
                .build();

        // 分享文件
        fileShareService.saveShare(share);

        // 返回分享的文件信息
        return Result.success(share);
    }

    /**
     * 取消文件分享。
     *
     * @param session  当前的HTTP会话
     * @param shareIds 要取消分享的文件ID列表，以逗号分隔
     * @return ResponseVO 结果对象，确认取消分享成功或提供错误消息
     */
    @RequestMapping("/cancelShare")
    @GlobalInterceptor(checkParams = true)
    public ResponseVO cancelShare(HttpSession session, @VerifyParam(required = true) String shareIds) {
        // 从当前会话中获取用户信息
        SessionWebUserDto userDto = getUserInfoFromSession(session);

        // 使用文件分享服务删除指定的分享记录
        fileShareService.deleteFileShareBatch(shareIds.split(","), userDto.getUserId());

        // 返回成功响应
        return Result.success(null);
    }
}
