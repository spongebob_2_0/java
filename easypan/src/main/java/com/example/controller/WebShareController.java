package com.example.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.annotation.GlobalInterceptor;
import com.example.annotation.VerifyParam;
import com.example.component.BusinessException;
import com.example.component.ResponseVO;
import com.example.component.Result;
import com.example.entity.constants.Constants;
import com.example.entity.dto.SessionShareDto;
import com.example.entity.dto.SessionWebUserDto;
import com.example.entity.enums.FileDelFlagEnums;
import com.example.entity.enums.ResponseCodeEnum;
import com.example.entity.po.FileInfo;
import com.example.entity.po.FileShare;
import com.example.entity.po.UserInfo;
import com.example.entity.query.FileInfoQuery;
import com.example.entity.vo.FileInfoVO;
import com.example.entity.vo.FileShareVO;
import com.example.entity.vo.PaginationResultVO;
import com.example.entity.vo.ShareInfoVO;
import com.example.mapper.FileInfoMapper;
import com.example.mapper.FileShareMapper;
import com.example.mapper.UserInfoMapper;
import com.example.service.impl.FileInfoServiceImpl;
import com.example.service.impl.FileShareServiceImpl;
import com.example.utils.CopyTools;
import com.example.utils.StringTools;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * Date: 2023-11-25
 * Time: 23:06
 * @author WuYimin
 */
@RestController
@RequestMapping("/showShare")
public class WebShareController extends CommonFileController {


    @Resource
    private FileShareMapper fileShareMapper;

    @Resource
    private FileInfoMapper fileInfoMapper;

    @Resource
    private UserInfoMapper userInfoMapper;

    @Resource
    private FileShareServiceImpl fileShareService;

    @Resource
    private FileInfoServiceImpl fileInfoService;

    /**
     * 获取分享登录信息
     *
     * @param session   // 用户的HTTP会话
     * @param shareId   // 分享ID
     * @return
     */
    @RequestMapping("/getShareLoginInfo")
    @GlobalInterceptor(checkLogin = false, checkParams = true)   // 使用拦截器进行全局拦截，不检查登录，但检查参数
    public ResponseVO getShareLoginInfo(HttpSession session, @VerifyParam(required = true) String shareId) {
        // 从session中获取分享信息
        SessionShareDto shareSessionDto = getSessionShareFromSession(session, shareId);
        // 如果从session中获取不到分享信息,返回空的响应
        if (shareSessionDto == null) {
            return Result.success(null);
        }
        // 获取分享的通用信息
        ShareInfoVO shareInfoVO = getShareInfoCommon(shareId);
        //从session中获取用户信息,判断是否是当前用户分享的文件
        SessionWebUserDto userDto = getUserInfoFromSession(session);
        // 如果用户信息不为空并且用户ID与分享的用户ID匹配
        if (userDto != null && userDto.getUserId().equals(shareSessionDto.getShareUserId())) {
            // 设置这是当前用户的分享
            shareInfoVO.setCurrentUser(true);
        } else {
            // 设置这不是当前用户的分享
            shareInfoVO.setCurrentUser(false);
        }
        // 返回成功，并带上分享的信息
        return Result.success(shareInfoVO);
    }

    /**
     * 通用方法，用于获取分享的信息
     *
     * @param shareId   // 分享ID
     * @return 返回分享的详细信息
     */
    private ShareInfoVO getShareInfoCommon(String shareId) {
        // 根据分享ID查询分享信息
        LambdaQueryWrapper<FileShare> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(FileShare::getShareId,shareId); // 设置分享ID
        FileShare share = fileShareMapper.selectOne(lambdaQueryWrapper);
        // 检查分享是否存在或者已经过期
        // after方法用于测试此日期是否晚于指定日期,如果当前日期大于失效日期返回true
        if (null == share || (share.getExpireTime() != null && new Date().after(share.getExpireTime()))) {
            // 抛出业务异常，分享不存在或已过期
            throw new BusinessException(ResponseCodeEnum.CODE_902.getMsg());
        }
        // 将分享的实体信息share 复制到 一个新的目标对象shareInfoVO中
        ShareInfoVO shareInfoVO = CopyTools.copy(share, ShareInfoVO.class);
        // 根据文件ID和用户ID查询文件信息
        LambdaQueryWrapper<FileInfo> lambdaQueryWrapper2 = new LambdaQueryWrapper<>();
        lambdaQueryWrapper2
                .eq(FileInfo::getFileId,share.getFileId())  // 设置文件Id
                .eq(FileInfo::getUserId,share.getUserId()); // 设置用户ID
        FileInfo fileInfo = fileInfoMapper.selectOne(lambdaQueryWrapper2);
        // 检查文件是否存在或已被删除
        if (fileInfo == null || !FileDelFlagEnums.USING.getFlag().equals(fileInfo.getDelFlag())) {
            // 抛出业务异常，文件不存在或已被删除
            throw new BusinessException(ResponseCodeEnum.CODE_902.getMsg());
        }
        // 设置文件名到
        shareInfoVO.setFileName(fileInfo.getFileName());
        // 根据用户ID查询用户信息
        LambdaQueryWrapper<UserInfo> lambdaQueryWrapper3 = new LambdaQueryWrapper<>();
        lambdaQueryWrapper3.eq(UserInfo::getUserId,share.getUserId());
        UserInfo userInfo = userInfoMapper.selectOne(lambdaQueryWrapper3);
        // 设置用户昵称、头像和用户ID到视图模型
        shareInfoVO.setNickName(userInfo.getNickName());
        shareInfoVO.setAvatar(userInfo.getQqAvatar());
        shareInfoVO.setUserId(userInfo.getUserId());
        // 返回分享的详细信息
        return shareInfoVO;
    }

    /**
     * 获取分享信息
     *
     * @param shareId   // 分享ID
     * @return
     */
    @RequestMapping("/getShareInfo")
    @GlobalInterceptor(checkLogin = false, checkParams = true)  // 使用拦截器进行全局拦截，不检查登录，但检查参数
    public ResponseVO getShareInfo(@VerifyParam(required = true) String shareId) {
        // 调用通用方法获取分享的信息，并返回成功的响应
        return Result.success(getShareInfoCommon(shareId));
    }

    /**
     * 校验分享码
     *
     * @param session   // 用户的HTTP会话
     * @param shareId   // 分享ID
     * @param code      // 用户输入的分享码
     * @return
     */
    @RequestMapping("/checkShareCode")
    @GlobalInterceptor(checkLogin = false, checkParams = true)  // 使用拦截器进行全局拦截，不检查登录，但检查参数
    public ResponseVO checkShareCode(HttpSession session,
                                     @VerifyParam(required = true) String shareId,
                                     @VerifyParam(required = true) String code) {
        // 根据分享ID和输入的码来校验分享码
        SessionShareDto shareSessionDto = fileShareService.checkShareCode(shareId, code);
        // 校验通过后，将分享信息保存到session中
        session.setAttribute(Constants.SESSION_SHARE_KEY + shareId, shareSessionDto);
        // 返回成功响应，不带数据
        return Result.success(null);
    }

    /**
     * 获取分享文件列表
     *
     * @param session   // 用户的HTTP会话
     * @param shareId   // 分享ID
     * @param filePid   // 父级ID
     * @return
     */
    @RequestMapping("/loadFileList")
    @GlobalInterceptor(checkLogin = false, checkParams = true)  // 使用拦截器进行全局拦截，不检查登录，但检查参数
    public ResponseVO loadFileList(HttpSession session,
                                   @VerifyParam(required = true) String shareId, String filePid) {
        // 校验session中是否有分享信息
        SessionShareDto shareSessionDto = checkShare(session, shareId);
        FileInfoQuery query = new FileInfoQuery();
        // 如果不是父级id不为空,并且不根目录
        if (!StringTools.isEmpty(filePid) && !Constants.ZERO_STR.equals(filePid)) {
            // checkRootFilePid这个方法主要就是为了检查用户打开的分享文件 下面的子文件的最顶层目录是不是我们分享的文件,如果不是就不让打开
            // shareSessionDto.getFileId()方法获取到的是分享的文件ID, filePid是前端传来的打开的当前文件的父级ID
            fileInfoService.checkRootFilePid(shareSessionDto.getFileId(), shareSessionDto.getShareUserId(), filePid);
            // 如果不是根目录,那就按照父级ID作为查询条件去查
            query.setFilePid(filePid);
        } else {
            // 如果查询的文件的父级ID为0, 那就设置查询的文件ID为分享的文件ID
            query.setFileId(shareSessionDto.getFileId());
        }
        // 设置查询条件：分享用户ID、排序方式、删除标志
        query.setUserId(shareSessionDto.getShareUserId());
        query.setFileId(shareSessionDto.getFileId());
        query.setOrderBy("last_update_time desc");
        query.setDelFlag(FileDelFlagEnums.USING.getFlag());
        // 分页查询文件列表
        PaginationResultVO resultVO = fileInfoService.findListByPage(query);
        // 返回查询的结果
        return Result.success(convert2PaginationVO(resultVO, FileInfoVO.class));
    }

    /**
     * 校验分享是否失效
     *
     * @param session  // 用户的HTTP会话
     * @param shareId  // 分享ID
     * @return 返回session中的分享信息
     */
    private SessionShareDto checkShare(HttpSession session, String shareId) {
        // 从session中获取分享信息
        SessionShareDto shareSessionDto = getSessionShareFromSession(session, shareId);
        // 如果从session中获取不到分享信息,则抛出业务异常（分享不存在）
        if (shareSessionDto == null) {
            throw new BusinessException(ResponseCodeEnum.CODE_903);
        }
        // 校验分享是否已经过期
        if (shareSessionDto.getExpireTime() != null && new Date().after(shareSessionDto.getExpireTime())) {
            throw new BusinessException(ResponseCodeEnum.CODE_902);
        }
        // 返回session中的分享信息
        return shareSessionDto;
    }

    /**
     * 获取文件夹里面的信息
     *
     * @param session  // 用户的HTTP会话
     * @param shareId  // 分享ID
     * @param path     // 要查询的目录路径
     * @return 返回目录的详细信息
     */
    @RequestMapping("/getFolderInfo")
    @GlobalInterceptor(checkLogin = false, checkParams = true)  // 使用拦截器进行全局拦截，不检查登录，但检查参数
    public ResponseVO getFolderInfo(HttpSession session,
                                    @VerifyParam(required = true) String shareId,
                                    @VerifyParam(required = true) String path) {
        // 校验分享是否失效，并获取分享信息
        SessionShareDto shareSessionDto = checkShare(session, shareId);
        // 调用父类方法获取目录信息，并返回结果
        return super.getFolderInfo(path, shareSessionDto.getShareUserId());
    }

    /**
     * 根据分享ID和文件ID获取文件
     *
     * @param response  // HTTP响应
     * @param session   // 用户的HTTP会话
     * @param shareId   // 分享ID
     * @param fileId    // 文件ID
     */
    @RequestMapping("/getFile/{shareId}/{fileId}")
    public void getFile(HttpServletResponse response, HttpSession session,
                        @PathVariable("shareId") @VerifyParam(required = true) String shareId,
                        @PathVariable("fileId") @VerifyParam(required = true) String fileId) {
        // 校验分享是否失效，并获取分享信息
        SessionShareDto shareSessionDto = checkShare(session, shareId);
        // 调用父类方法获取文件，并发送到HTTP响应中
        super.getFile(response, fileId, shareSessionDto.getShareUserId());
    }

    /**
     * 根据分享ID和文件ID获取视频信息
     *
     * @param response  // HTTP响应
     * @param session   // 用户的HTTP会话
     * @param shareId   // 分享ID
     * @param fileId    // 文件ID
     */
    @RequestMapping("/ts/getVideoInfo/{shareId}/{fileId}")
    public void getVideoInfo(HttpServletResponse response,
                             HttpSession session,
                             @PathVariable("shareId") @VerifyParam(required = true) String shareId,
                             @PathVariable("fileId") @VerifyParam(required = true) String fileId) {
        // 校验分享是否失效，并获取分享信息
        SessionShareDto shareSessionDto = checkShare(session, shareId);
        // 调用父类方法获取视频文件，并发送到HTTP响应中
        super.getFile(response, fileId, shareSessionDto.getShareUserId());
    }

    /**
     * 创建文件的下载链接
     *
     * @param session   // 用户的HTTP会话
     * @param shareId   // 分享ID
     * @param fileId    // 文件ID
     * @return 返回创建的下载链接
     */
    @RequestMapping("/createDownloadUrl/{shareId}/{fileId}")
    @GlobalInterceptor(checkLogin = false, checkParams = true)  // 使用拦截器进行全局拦截，不检查登录，但检查参数
    public ResponseVO createDownloadUrl(HttpSession session,
                                        @PathVariable("shareId") @VerifyParam(required = true) String shareId,
                                        @PathVariable("fileId") @VerifyParam(required = true) String fileId) {
        // 校验分享是否失效，并获取分享信息
        SessionShareDto shareSessionDto = checkShare(session, shareId);
        // 调用父类方法创建下载链接，并返回结果
        return super.createDownloadUrl(fileId, shareSessionDto.getShareUserId());
    }

    /**
     * 根据给定的代码进行文件下载
     *
     * @param request  // HTTP请求
     * @param response // HTTP响应
     * @param code     // 提供的下载码
     * @throws Exception  // 可能出现的异常
     */
    @RequestMapping("/download/{code}")
    @GlobalInterceptor(checkLogin = false, checkParams = true)  // 使用拦截器进行全局拦截，不检查登录，但检查参数
    public void downloadByCode(HttpServletRequest request, HttpServletResponse response,
                         @PathVariable("code") @VerifyParam(required = true) String code) throws Exception {
        // 调用父类方法进行文件下载操作
        super.download(request, response, code);
    }

    /**
     * 保存分享的文件到用户的网盘
     *
     * @param session       // 用户的HTTP会话
     * @param shareId       // 分享ID
     * @param shareFileIds  // 要分享的文件ID列表
     * @param myFolderId    // 用户的文件夹ID
     * @return 返回操作的结果信息
     */
    @RequestMapping("/saveShare")
    @GlobalInterceptor(checkParams = true)  // 使用拦截器进行全局拦截，检查参数,检查是否登录
    public ResponseVO saveShare(HttpSession session,
                                @VerifyParam(required = true) String shareId,
                                @VerifyParam(required = true) String shareFileIds,
                                @VerifyParam(required = true) String myFolderId) {
        // 校验分享是否失效，并获取分享信息
        SessionShareDto shareSessionDto = checkShare(session, shareId);
        // 从会话中获取当前登录的用户信息
        SessionWebUserDto webUserDto = getUserInfoFromSession(session);
        // 检查是否是用户分享的文件，如果是，则抛出异常
        if (shareSessionDto.getShareUserId().equals(webUserDto.getUserId())) {
            throw new BusinessException("自己分享的文件无法保存到自己的网盘");
        }
        // 调用服务保存分享的文件到用户的网盘
        fileInfoService.saveShare(shareSessionDto.getFileId(), shareFileIds, myFolderId, shareSessionDto.getShareUserId(), webUserDto.getUserId());
        // 返回成功的响应信息
        return Result.success(null);
    }
}
