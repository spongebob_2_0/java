package com.example.controller;

import cn.hutool.core.lang.Console;
import com.example.annotation.GlobalInterceptor;
import com.example.annotation.VerifyParam;
import com.example.component.ResponseVO;
import com.example.component.Result;
import com.example.entity.dto.SessionWebUserDto;
import com.example.entity.dto.UploadResultDto;
import com.example.entity.enums.FileCategoryEnums;
import com.example.entity.enums.FileDelFlagEnums;
import com.example.entity.enums.FileFolderTypeEnums;
import com.example.entity.po.FileInfo;
import com.example.entity.query.FileInfoQuery;
import com.example.entity.vo.FileInfoVO;
import com.example.entity.vo.PaginationResultVO;
import com.example.mapper.FileInfoMapper;
import com.example.service.impl.FileInfoServiceImpl;
import com.example.utils.CopyTools;
import com.example.utils.StringTools;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description: 文件控制层
 * Date: 2023-11-23
 * Time: 3:00
 * @author WuYimin
 */
@RestController
@RequestMapping("/file")
public class FileInfoController extends CommonFileController{

    @Resource
    private FileInfoServiceImpl fileInfoService;

    @Resource
    private FileInfoMapper fileInfoMapper;


    /**
     * 用户登录后，可调用此方法查看文件信息列表。
     * @param session 当前用户的session信息
     * @param query   查询的条件，包括各种参数
     * @param category 文件的类别，例如图片、视频等
     * @return 返回分页查询的结果，包含了文件信息列表和分页信息
     */
    @RequestMapping("/loadDataList")
    @GlobalInterceptor(checkParams = true)
    public ResponseVO loadDataList(HttpSession session, FileInfoQuery query, String category) {

        Console.log(query);
        // 将前端传来的分类类别转化为数字表示（通过遍历枚举转换）
        FileCategoryEnums categoryEnum = FileCategoryEnums.getByCode(category);
        // 如果找到了对应的文件类别枚举对象，就设置到查询对象中
        if (null != categoryEnum) {
            query.setFileCategory(categoryEnum.getCategory());
        }
        // 从session中获取当前登录用户的ID，并设置到查询对象中
        query.setUserId(getUserInfoFromSession(session).getUserId());
        // 设置查询的排序方式为按最后更新时间降序
        query.setOrderBy("last_update_time desc");
        // 设置查询的删除标记为正在使用状态
        query.setDelFlag(FileDelFlagEnums.USING.getFlag());
        // 调用服务层方法，执行分页查询
        PaginationResultVO result = fileInfoService.findListByPage(query);
        // 将查询结果转换为前端需要的数据格式并返回
        return Result.success(convert2PaginationVO(result, FileInfoVO.class));
    }

    /**
     * 上传文件
     * <p>
     * 用户可以使用此方法上传文件。如果上传的文件较大，它可能会被切割成多个部分进行上传。
     * 每个部分的上传都会调用这个方法，并传入当前部分的索引和总部分数。
     * </p>
     *
     * @param session     当前用户的session信息
     * @param fileId      文件的ID
     * @param file        上传的文件部分
     * @param fileName    文件的名称
     * @param filePid     文件的父ID
     * @param fileMd5     文件的MD5值，用于校验
     * @param chunkIndex  当前上传的部分的索引
     * @param chunks      文件被切割的总部分数
     * @return 返回文件上传的结果，包含了文件的上传状态
     */
    @RequestMapping("/uploadFile")
    @GlobalInterceptor(checkParams = true)
    public ResponseVO uploadFile(HttpSession session,
                                 String fileId,
                                 MultipartFile file,
                                 @VerifyParam(required = true) String fileName,
                                 @VerifyParam(required = true) String filePid,
                                 @VerifyParam(required = true) String fileMd5,
                                 @VerifyParam(required = true) Integer chunkIndex,
                                 @VerifyParam(required = true) Integer chunks) {
        // 从session中获取当前登录的用户信息
        SessionWebUserDto webUserDto = getUserInfoFromSession(session);
        // 调用服务层方法，上传文件（当前用户信息，文件ID，文件，文件名，文件父ID，文件的MD5值，文件分片的索引，文件分片的总片数）
        UploadResultDto resultDto = fileInfoService.uploadFile(webUserDto, fileId, file, fileName, filePid, fileMd5, chunkIndex, chunks);
        // 将上传结果转换为前端需要的数据格式并返回
        return Result.success(resultDto);
    }

    /**
     * 获取图片
     * 根据指定的文件夹和图片名称，向响应对象中写入图片数据。
     * @param response      用于返回客户端的响应对象
     * @param imageFolder   指定的图片所在的文件夹
     * @param imageName     指定的图片名称
     */
    @RequestMapping("/getImage/{imageFolder}/{imageName}")
    public void getPicture(HttpServletResponse response,
                         @PathVariable("imageFolder") String imageFolder,
                         @PathVariable("imageName") String imageName) {
        // 调用父类的getImage方法来获取图片
        super.getImage(response, imageFolder, imageName);
    }

    /**
     * 获取视频信息
     * <p>
     * 根据指定的文件ID，为用户提供视频信息。
     * </p>
     *
     * @param response 用于返回客户端的响应对象
     * @param session  当前会话，用于获取用户信息
     * @param fileId   要获取的视频文件的ID
     */
    @RequestMapping("/ts/getVideoInfo/{fileId}")
    public void getVideoInfo(HttpServletResponse response,
                             HttpSession session,
                             @PathVariable("fileId") @VerifyParam(required = true) String fileId) {
        // 从session中获取当前登录的用户信息
        SessionWebUserDto webUserDto = getUserInfoFromSession(session);
        // 调用父类的getFile方法来获取视频信息
        super.getFile(response, fileId, webUserDto.getUserId());
    }

    /**
     * 获取文件 （这里获取的是非视频或者图片的文件）
     * 根据指定的文件ID，向响应对象中写入文件数据。
     * @param response 用于返回客户端的响应对象
     * @param session  当前会话，用于获取用户信息
     * @param fileId   要获取的文件的ID
     */
    @RequestMapping("/getFile/{fileId}")
    public void getFile(HttpServletResponse response,
                        HttpSession session,
                        @PathVariable("fileId") @VerifyParam(required = true) String fileId) {
        // 从session中获取当前登录的用户信息
        SessionWebUserDto webUserDto = getUserInfoFromSession(session);
        // 调用父类的getFile方法来获取文件
        super.getFile(response, fileId, webUserDto.getUserId());
    }

    /**
     * 新建文件夹
     * @param session   当前的HTTP会话
     * @param filePid   父文件夹的ID
     * @param fileName  新文件夹的名称
     * @return ResponseVO 结果对象，包含新建文件夹的信息
     */
    @RequestMapping("/newFoloder")
    @GlobalInterceptor(checkParams = true)
    public ResponseVO newFoloder(HttpSession session,
                                 @VerifyParam(required = true) String filePid,
                                 @VerifyParam(required = true) String fileName) {
        // 从session中获取当前登录的用户信息
        SessionWebUserDto webUserDto = getUserInfoFromSession(session);
        // 调用服务层的newFolder方法创建新文件夹
        FileInfo fileInfo = fileInfoService.newFolder(filePid, webUserDto.getUserId(), fileName);
        // 返回成功的响应，包含新建文件夹的信息
        return Result.success(fileInfo);
    }


    /**
     * 获取当前目录里面的信息。
     * @param session     会话
     * @param path        指定文件夹路径。
     * @return ResponseVO 结果对象
     */
    @RequestMapping("/getFolderInfo")
    @GlobalInterceptor(checkParams = true)
    public ResponseVO getFolderInfo(HttpSession session,
                                    @VerifyParam(required = true) String path) {
        // 从当前会话中提取用户id
        String userId = getUserInfoFromSession(session).getUserId();
        // 调用父类的方法来根据用户ID和路径获取文件夹信息
        ResponseVO folderInfo = super.getFolderInfo(path, userId);
        return folderInfo;
    }


    /**
     * 重命名文件或文件夹
     * @param session   当前的HTTP会话
     * @param fileId    要重命名的文件或文件夹的ID
     * @param fileName  新的名称
     * @return ResponseVO 结果对象，包含重命名后的文件或文件夹信息
     */
    @RequestMapping("/rename")
    @GlobalInterceptor(checkParams = true)
    public ResponseVO rename(HttpSession session,
                             @VerifyParam(required = true) String fileId,
                             @VerifyParam(required = true) String fileName) {
        // 从session中获取当前登录的用户信息
        SessionWebUserDto webUserDto = getUserInfoFromSession(session);
        // 调用服务层的rename方法重命名文件或文件夹
        FileInfo fileInfo = fileInfoService.rename(fileId, webUserDto.getUserId(), fileName);
        // 返回成功的响应，包含重命名后的文件或文件夹信息
        return Result.success(CopyTools.copy(fileInfo, FileInfoVO.class));
    }

    /**
     * 获取所有目录信息
     * 批量移动到其它文件夹的时候调用 （需要排除被移动的文件ID）
     * @param session        当前用户会话
     * @param filePid        指定的父文件夹ID
     * @param currentFileIds 被选择的文件ID数组
     * @return 包含文件夹的详细信息的结果对象
     */
    @RequestMapping("/loadAllFolder")
    @GlobalInterceptor(checkParams = true)
    public ResponseVO loadAllFolder(HttpSession session,
                                    @VerifyParam(required = true) String filePid,
                                    String currentFileIds) {
        FileInfoQuery query = FileInfoQuery.builder()
                .userId(getUserInfoFromSession(session).getUserId())  // 用户ID
                .filePid(filePid)                                     // 文件父级ID
                .folderType(FileFolderTypeEnums.FOLDER.getType())     // 文件类型设置为目录
                .delFlag(FileDelFlagEnums.USING.getFlag())            // 文件是否删除的状态，设置为使用中
                .build();


        String[] fileIdArray = null;
        // 需要排除的文件ID
        if (!StringTools.isEmpty(currentFileIds)) {
            fileIdArray = currentFileIds.split(",");
        }
        // 执行查询并获取结果列表
        List<FileInfo> fileInfoList = fileInfoMapper.findAllFold(query,fileIdArray);
        // 将实体列表转换为VO列表返回
        return Result.success(CopyTools.copyList(fileInfoList, FileInfoVO.class));
    }

    /**
     * 修改文件目录、移动文件
     *
     * @param session 当前用户会话
     * @param fileIds 要移动的文件ID列表
     * @param filePid 目标文件夹ID
     * @return
     */
    @RequestMapping("/changeFileFolder")
    @GlobalInterceptor(checkParams = true)
    public ResponseVO changeFileFolder(HttpSession session,
                                       @VerifyParam(required = true) String fileIds,
                                       @VerifyParam(required = true) String filePid) {
        // 获取当前会话用户的信息
        SessionWebUserDto webUserDto = getUserInfoFromSession(session);
        // 执行文件移动操作
        fileInfoService.changeFileFolder(fileIds, filePid, webUserDto.getUserId());
        // 返回成功的操作结果
        return Result.success(null);
    }

    /**
     * 创建下载链接。
     *
     * @param session 当前的HTTP会话
     * @param fileId  文件的唯一ID
     * @return ResponseVO
     */
    @RequestMapping("/createDownloadUrl/{fileId}")
    @GlobalInterceptor(checkParams = true)
    public ResponseVO createDownloadUrl(HttpSession session,
                                        @PathVariable("fileId") @VerifyParam(required = true) String fileId) {
        // 调用父类方法创建下载链接 （参数是文件ID和用户ID）
        return super.createDownloadUrl(fileId, getUserInfoFromSession(session).getUserId());
    }

    /**
     * 通过提供的代码执行文件下载。
     *
     * @param request  HTTP请求对象
     * @param response HTTP响应对象
     * @param code     用于下载的唯一代码
     * @throws Exception 可能抛出的异常
     */
    @RequestMapping("/download/{code}")
    @GlobalInterceptor(checkLogin = false, checkParams = true)
    public void downloadByCode(HttpServletRequest request, HttpServletResponse response,
                               @PathVariable("code") @VerifyParam(required = true) String code) throws Exception {
        // 调用父类方法处理下载请求
        super.download(request, response, code);
    }

    /**
     * 将指定文件移到回收站。
     *
     * @param session 当前的HTTP会话
     * @param fileIds 要删除的文件ID列表，以逗号分隔
     * @return
     */
    @RequestMapping("/delFile")
    @GlobalInterceptor(checkParams = true)
    public ResponseVO delFile(HttpSession session, @VerifyParam(required = true) String fileIds) {
        // 从当前会话中获取用户信息
        SessionWebUserDto webUserDto = getUserInfoFromSession(session);

        // 调用文件信息服务，将指定文件移至回收站
        fileInfoService.removeFile2RecycleBatch(webUserDto.getUserId(), fileIds);

        // 返回成功响应，不携带任何额外数据
        return Result.success(null);
    }
}
