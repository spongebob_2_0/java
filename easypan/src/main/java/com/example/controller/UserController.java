package com.example.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.annotation.GlobalInterceptor;
import com.example.annotation.VerifyParam;
import com.example.component.BusinessException;
import com.example.component.RedisComponent;
import com.example.component.ResponseVO;
import com.example.component.Result;
import com.example.config.AppConfig;
import com.example.entity.constants.Constants;
import com.example.entity.dto.CreateImageCode;
import com.example.entity.dto.SessionWebUserDto;
import com.example.entity.enums.VerifyRegexEnum;
import com.example.entity.po.UserInfo;
import com.example.service.impl.EmailCodeServiceImpl;
import com.example.service.impl.UserInfoServiceImpl;
import com.example.utils.StringTools;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;


/**
 * Created with IntelliJ IDEA.
 * Description: 用户控制层
 * Date: 2023-11-21
 * Time: 18:23
 * @author WuYimin
 */
@RestController
@Slf4j
public class UserController extends CommonController{

    // 定义一个常量表示HTTP头部的"Content-Type"字段
    private static final String CONTENT_TYPE = "Content-Type";
    // 定义一个常量表示"Content-Type"字段的值为"application/json;charset=UTF-8"，表示返回的内容是UTF-8编码的JSON格式数据
    private static final String CONTENT_TYPE_VALUE = "application/json;charset=UTF-8";

    @Resource
    private EmailCodeServiceImpl emailCodeService;

    @Resource
    private UserInfoServiceImpl userInfoService;

    @Resource
    private AppConfig appConfig;

    @Resource
    private RedisComponent redisComponent;


    /**
     * 图片验证码
     * @param response
     * @param session
     * @param type
     * @throws IOException
     */
    @RequestMapping("/checkCode")
    public void checkCode(HttpServletResponse response, HttpSession session, Integer type) throws IOException {
        // 创建一个图像验证码对象，宽130像素，高38像素，包含5个字符和10条干扰线
        CreateImageCode vCode = new CreateImageCode(130, 38, 5, 10);
        // 设置响应头，表示资源不应被缓存
        response.setHeader("Pragma", "no-cache");
        // 设置响应头，禁用缓存
        response.setHeader("Cache-Control", "no-cache");
        // 设置响应头，表示资源已过期
        response.setDateHeader("Expires", 0);
        // 设置响应内容类型为JPEG图片
        response.setContentType("image/jpeg");
        // 获取图像验证码中的验证码文本
        String code = vCode.getCode();
        // 判断验证码类型：如果没有类型或类型为0，则使用图片验证码键，否则使用电子邮件验证码键
        if (type == null || type == 0) {
            session.setAttribute(Constants.CHECK_CODE_KEY, code);
        } else {
            session.setAttribute(Constants.CHECK_CODE_KEY_EMAIL, code);
        }
        // 将图像验证码写入响应的输出流，从而发送给客户端
        vCode.write(response.getOutputStream());
    }

    /**
     * 发送邮件
     * @param session
     * @param email
     * @param checkCode
     * @param type
     * @return
     */
    @RequestMapping("/sendEmailCode")
    // 为这个方法添加一个全局拦截器注解，其中不检查登录但是检查参数
    @GlobalInterceptor(checkLogin = false, checkParams = true)
    public ResponseVO sendEmailCode(HttpSession session,
                                    // 验证邮箱参数：必需的、匹配电子邮件正则表达式、最大长度为150
                                    @VerifyParam(required = true, regex = VerifyRegexEnum.EMAIL, max = 150) String email,
                                    // 验证检查码参数：必需的
                                    @VerifyParam(required = true) String checkCode,
                                    // 验证类型参数：必需的
                                    @VerifyParam(required = true) Integer type) {

        try {
            // 判断提供的图片验证码是否与会话中存储的图片验证码相匹配（不区分大小写）
            if (!checkCode.equalsIgnoreCase((String) session.getAttribute(Constants.CHECK_CODE_KEY_EMAIL))) {
                // 如果不匹配，抛出一个业务异常
                throw new BusinessException("图片验证码不正确");
            }
            // 调用emailCodeService来发送电子邮件验证码
            emailCodeService.sendEmail(email, type);
            // 返回成功的响应
            return Result.success(null);
        } finally {
            // 无论方法执行的结果如何，最后都会从会话中移除图形邮件验证码的键
            session.removeAttribute(Constants.CHECK_CODE_KEY_EMAIL);
        }
    }

    @RequestMapping("/register")
    // 使用自定义注解 GlobalInterceptor，指定在此方法执行前不检查登录但检查参数
    @GlobalInterceptor(checkLogin = false, checkParams = true)
    public ResponseVO register(// 从请求中获取会话对象
                               HttpSession session,
                               // 验证邮箱参数是否满足条件：必须提供、格式要满足邮箱正则、长度不能超过150个字符
                               @VerifyParam(required = true, regex = VerifyRegexEnum.EMAIL, max = 150) String email,
                               // 验证昵称参数是否满足条件：必须提供、长度不能超过20个字符
                               @VerifyParam(required = true, max = 20) String nickName,
                               // 验证密码参数是否满足条件：必须提供、格式要满足密码正则、长度在8到18个字符之间
                               @VerifyParam(required = true, regex = VerifyRegexEnum.PASSWORD, min = 8, max = 18) String password,
                               // 验证图片验证码参数是否必须提供
                               @VerifyParam(required = true) String checkCode,
                               // 验证电子邮件验证码参数是否必须提供
                               @VerifyParam(required = true) String emailCode) {
        try {
            // 校验从会话中获取的验证码与用户提交的验证码是否相同（忽略大小写）
            if (!checkCode.equalsIgnoreCase( (String) session.getAttribute(Constants.CHECK_CODE_KEY)) ) {
                // 如果不相同，抛出自定义业务异常
                throw new BusinessException("图片验证码不正确");
            }
            // 调用 userInfoService 的注册方法进行用户注册
            userInfoService.register(email, nickName, password, emailCode);
            // 注册成功后，返回一个成功响应
            return Result.success(null);
        } finally {
            // 不管注册操作是否成功，都从会话中移除图形验证码（为了安全和避免重复使用）
            session.removeAttribute(Constants.CHECK_CODE_KEY);
        }
    }

    @RequestMapping("/login")
    // 使用自定义注解 GlobalInterceptor，指定在此方法执行前不检查登录但检查参数
    @GlobalInterceptor(checkLogin = false, checkParams = true)
    public ResponseVO login(// 从请求中获取会话对象
                            HttpSession session,
                            // 获取当前请求对象
                            HttpServletRequest request,
                            // 验证邮箱参数是否必须提供
                            @VerifyParam(required = true) String email,
                            // 验证密码参数是否必须提供
                            @VerifyParam(required = true) String password,
                            // 验证图片验证码参数是否必须提供
                            @VerifyParam(required = true) String checkCode) {
        try {
            // 校验从会话中获取的验证码与用户提交的验证码是否相同（忽略大小写）
            if (!checkCode.equalsIgnoreCase((String) session.getAttribute(Constants.CHECK_CODE_KEY))) {
                // 如果不相同，抛出自定义 业务 异常
                throw new BusinessException("图片验证码不正确");
            }
            // 调用 userInfoService 的登录方法进行用户登录，并获取用户的会话信息
            SessionWebUserDto sessionWebUserDto = userInfoService.login(email, password);
            // 将用户会话信息存储在会话中
            session.setAttribute(Constants.SESSION_KEY, sessionWebUserDto);
            // 登录成功后，返回一个包含用户会话信息的成功响应
            return Result.success(sessionWebUserDto);
        } finally {
            // 不管登录操作是否成功，都从会话中移除验证码（为了安全和避免重复使用）
            session.removeAttribute(Constants.CHECK_CODE_KEY);
        }
    }

    /**
     * 登录页面重置密码
     * @param session    session会话
     * @param email      邮箱参数
     * @param password   新密码
     * @param checkCode  图片验证码
     * @param emailCode  邮箱验证码
     * @return
     */
    @RequestMapping("/resetPwd")
    // 使用自定义注解 GlobalInterceptor，指定在此方法执行前不检查登录但检查参数
    @GlobalInterceptor(checkLogin = false, checkParams = true)
    public ResponseVO resetPwd(// 从请求中获取会话对象
                               HttpSession session,
                               // 验证邮箱参数是否提供，是否满足邮箱正则表达式，并且最大长度为150
                               @VerifyParam(required = true, regex = VerifyRegexEnum.EMAIL, max = 150) String email,
                               // 验证新密码参数是否提供，是否满足密码的正则表达式，并且长度在8到18之间
                               @VerifyParam(required = true, regex = VerifyRegexEnum.PASSWORD, min = 8, max = 18) String password,
                               // 验证图片验证码参数是否提供
                               @VerifyParam(required = true) String checkCode,
                               // 验证邮箱验证码参数是否提供
                               @VerifyParam(required = true) String emailCode) {
        try {
            // 校验从会话中获取的图片验证码与用户提交的图片验证码是否相同（忽略大小写）
            if (!checkCode.equalsIgnoreCase((String) session.getAttribute(Constants.CHECK_CODE_KEY))) {
                // 如果不相同，抛出自定义业务异常
                throw new BusinessException("图片验证码不正确");
            }
            // 调用 userInfoService 来重置用户密码
            userInfoService.resetPwd(email, password, emailCode);
            // 密码重置成功后，返回一个成功的响应
            return Result.success(null);
        } finally {
            // 不管密码重置操作是否成功，都从会话中移除验证码（为了安全和避免重复使用）
            session.removeAttribute(Constants.CHECK_CODE_KEY);
        }
    }


    /**
     * 注销
     * @param session 会话
     * @return
     */
    @RequestMapping("/logout")
    @GlobalInterceptor(checkLogin = false,checkParams = true)
    public ResponseVO logout(HttpSession session) {
        // 终止HTTP会话，从而使任何与会话关联的数据都无效
        session.invalidate();
        return Result.success(null);
    }


    /**
     * 获取用户头像
     * @param response 响应体
     * @param userId  用户ID
     */
    @RequestMapping("/getAvatar/{userId}")
    // 使用GlobalInterceptor注解，表示不需要登录验证，但需要参数验证
    @GlobalInterceptor(checkLogin = false, checkParams = true)
    public void getAvatar(HttpServletResponse response,
                                // 使用@PathVariable注解获取URL中的userId，并通过@VerifyParam注解确保该参数是必须的
                                @VerifyParam(required = true) @PathVariable("userId") String userId) {

        // 定义头像文件夹的路径
        String avatarFolderName = Constants.FILE_FOLDER_FILE + Constants.FILE_FOLDER_AVATAR_NAME;
        // 根据文件夹路径创建一个File对象
        File folder = new File(appConfig.getProjectFolder() + avatarFolderName);
        // 如果该文件夹不存在，则创建该文件夹
        if (!folder.exists()) {
            folder.mkdirs();
        }

        // 定义用户头像的完整路径
        String avatarPath = appConfig.getProjectFolder() + avatarFolderName + userId + Constants.AVATAR_SUFFIX;
        File file = new File(avatarPath);
        // 如果用户头像文件不存在
        if (!file.exists()) {
            // 如果默认头像也不存在，返回无默认图片的响应
            if (!new File(appConfig.getProjectFolder() + avatarFolderName + Constants.AVATAR_DEFUALT).exists()) {
                // 用于在头像文件不存在时向响应中输出提示信息
                printNoDefaultImage(response);
                return;
            }
            // 使用默认头像的路径
            avatarPath = appConfig.getProjectFolder() + avatarFolderName + Constants.AVATAR_DEFUALT;
        }
        // 设置响应的内容类型为"image/jpg"
        // 后端已经将响应的内容类型设置为了image/jpg，所以当前端接收到这个响应时，浏览器会自动识别出这是一个图片类型的响应，并将其解析为图片
        // 前端只需要在<img>标签的src属性中设置请求的URL，浏览器会自动发送请求并处理响应，然后将图片显示在<img>标签的位置
        response.setContentType("image/jpg");
        // 读取并返回指定路径下的头像文件
        readFile(response, avatarPath);
    }

    /**
     * 用于在头像文件不存在时向响应中输出提示信息
     * @param response
     */
    private void printNoDefaultImage(HttpServletResponse response) {
        // 设置响应的内容类型头为"application/json;charset=UTF-8"
        response.setHeader(CONTENT_TYPE, CONTENT_TYPE_VALUE);
        // 设置响应的HTTP状态码为200 (OK)
        response.setStatus(HttpStatus.OK.value());
        // 初始化一个PrintWriter对象，用于向响应中写入内容
        PrintWriter writer = null;
        try {
            // 获取响应的输出流并创建PrintWriter对象
            writer = response.getWriter();
            // 向响应中写入提示信息
            writer.print("请在头像目录下放置默认头像default_avatar.jpg");
            // 关闭PrintWriter对象，释放资源
            writer.close();
        } catch (Exception e) {
            // 在出现异常时，使用日志工具记录错误信息
            log.error("输出无默认图失败", e);
        } finally {
            // 确保PrintWriter对象在任何情况下都被关闭
            writer.close();
        }
    }

    /**
     * 修改用户头像
     * @return
     */
    @RequestMapping("/updateUserAvatar")
    @GlobalInterceptor
    public ResponseVO updateUserAvatar(HttpSession session, MultipartFile avatar) {
        // 从会话中获取用户的信息
        SessionWebUserDto webUserDto = getUserInfoFromSession(session);
        // 获取基础文件夹的路径
        String baseFolder = appConfig.getProjectFolder() + Constants.FILE_FOLDER_FILE;
        // 定义头像的目标文件夹
        File targetFileFolder = new File(baseFolder + Constants.FILE_FOLDER_AVATAR_NAME);
        // 如果目标文件夹不存在，则创建它
        if (!targetFileFolder.exists()) {
            targetFileFolder.mkdirs();
        }

        // 定义用户头像的目标文件
        File targetFile = new File(targetFileFolder.getPath() + "/" + webUserDto.getUserId() + Constants.AVATAR_SUFFIX);
        try {
            // 将上传的头像文件内容保存到目标文件中
            avatar.transferTo(targetFile);
        } catch (Exception e) {
            // 如果上传失败，记录错误信息
            log.error("上传头像失败", e);
        }

        // 创建一个新的用户信息对象，并设置QQ头像字段为空
        UserInfo userInfo = UserInfo.builder()
                .qqAvatar("")
                .build();
        userInfo.setQqAvatar("");

        // 根据用户ID更新用户信息
        LambdaQueryWrapper<UserInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(UserInfo::getUserId,webUserDto.getUserId());
        userInfoService.update(userInfo,lambdaQueryWrapper);
        // 清除已存在的用户头像URL
        webUserDto.setAvatar(null);
        // 更新会话中的用户信息
        session.setAttribute(Constants.SESSION_KEY, webUserDto);
        // 返回一个成功的响应对象，不带任何额外数据
        return Result.success(null);
    }

    /**
     * 获取用户信息
     * @param session 会话
     * @return
     */
    @RequestMapping("/getUserInfo")
    public ResponseVO getUserInfo(HttpSession session) {
        // 从HTTP会话中获取用户信息，
        SessionWebUserDto sessionWebUserDto = getUserInfoFromSession(session);
        // 返回一个成功的响应对象，其中包含从会话中获取的用户信息
        return Result.success(sessionWebUserDto);
    }

    /**
     * 获取用户使用空间
     * @param session 会话
     * @return
     */
    @RequestMapping("/getUseSpace")
    public ResponseVO getUseSpace(HttpSession session) {
        // 从HTTP会话中获取用户信息
        SessionWebUserDto sessionWebUserDto = getUserInfoFromSession(session);
        // 使用Redis组件获取该用户的使用空间，然后返回一个成功的响应对象，其中包含使用的空间信息
        return Result.success(redisComponent.getUserSpaceUse(sessionWebUserDto.getUserId()));
    }

    /**
     * 修改密码
     * @param session  会话
     * @param password 密码
     * @return
     */
    @RequestMapping("/updatePassword")
    @GlobalInterceptor(checkParams = true) // 使用全局拦截器，并检查参数
    public ResponseVO updatePassword(HttpSession session,
                                     // 使用@VerifyParam注解来验证'password'参数是否符合要求
                                     @VerifyParam(required = true, regex = VerifyRegexEnum.PASSWORD, min = 8, max = 18) String password) {
        // 从会话中获取用户的信息
        SessionWebUserDto sessionWebUserDto = getUserInfoFromSession(session);
        // 创建一个新的用户信息对象
        UserInfo userInfo = UserInfo.builder()
                .password(StringTools.encodeByMD5(password)) // 将密码经过MD5加密后设置到用户信息对象中
                .build();
        // 根据用户ID更新用户的密码信息
        LambdaQueryWrapper<UserInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(UserInfo::getUserId,sessionWebUserDto.getUserId());
        userInfoService.update(userInfo, lambdaQueryWrapper);
        // 返回一个成功的响应
        return Result.success(null);
    }

    @RequestMapping("/qqlogin")
    @GlobalInterceptor(checkLogin = false, checkParams = true) // 使用全局拦截器，并确保用户未登录且请求参数有效
    public ResponseVO qqlogin(HttpSession session, String callbackUrl) throws UnsupportedEncodingException {
        // 生成一个长度为30的随机字符串用作state参数
        // 防止有人伪造登录请求。当你发起登录时，你生成一个随机的state。当登录完成返回时，
        // 你检查返回的state是否和你生成的一样。不一样则可能是伪造的，可以拒绝这个登录。(这里没做这个校验)
        // state的第二个作用就是用来存储 用户登录成功后，需要重定向的页面
        String state = StringTools.getRandomString(Constants.LENGTH_30);
        // 如果callbackUrl(回调地址)不为空,就存到session里面去
        if (!StringTools.isEmpty(callbackUrl)) {
            // 将此callbackUrl存入session，使用生成的state作为key
            session.setAttribute(state, callbackUrl);
        }
        // 格式化QQ登录的URL，其中需要使用应用配置中的QQ App ID和回调URL，并进行URL编码
        String url = String.format(appConfig.getQqUrlAuthorization(),
                appConfig.getQqAppId(),
                URLEncoder.encode(appConfig.getQqUrlRedirect(), "utf-8"),
                state);
        // 返回一个成功的响应对象，其中包含了格式化后的QQ登录URL,然后前端跳会转到QQ登录页面并完成QQ授权
        return Result.success(url);
    }

    /**
     * 用户在QQ授权页面授权成功后，会前端就会请求发出请求重定向到此URL("qqlogin/callback")
     * @param session
     * @param code
     * @param state
     * @return
     */
    @RequestMapping("qqlogin/callback")
    //授权成功后，QQ会将用户重定向回您在QQ开放平台上设置的回调URL()，并且在这个URL后面附加一些参数
    //前端发请求到这里的时候回取出路由中的参数,一起发送过来,这些参数就包括code和state
    @GlobalInterceptor(checkLogin = false, checkParams = true) // 使用全局拦截器，并确保用户未登录且请求参数有效
    public ResponseVO qqLoginCallback(HttpSession session,
                                      @VerifyParam(required = true) String code,    // 验证code参数是否存在且有效
                                      @VerifyParam(required = true) String state) { // 验证state参数是否存在且有效

        // 调用userInfoService的qqLogin方法，使用提供的code进行QQ登录并返回用户的会话信息
        SessionWebUserDto sessionWebUserDto = userInfoService.qqLogin(code);
        // 将返回的用户会话信息存入session，使用常量SESSION_KEY作为key
        session.setAttribute(Constants.SESSION_KEY, sessionWebUserDto);
        // 初始化一个结果Map对象
        Map<String, Object> result = new HashMap<>();
        // 从state里面获取 callbackUrl(回调地址) 的信息 放到map里面, 这里的回调地址为空,前端压根没传
        result.put("callbackUrl", session.getAttribute(state));
        // 将用户的信息也放大map里面
        result.put("userInfo", sessionWebUserDto);
        // 返回一个成功的响应对象，其中包含了结果Map
        return Result.success(result);
    }
}
