package com.example.controller;

import com.example.component.BusinessException;
import com.example.component.RedisComponent;
import com.example.component.ResponseVO;
import com.example.component.Result;
import com.example.config.AppConfig;
import com.example.entity.constants.Constants;
import com.example.entity.dto.DownloadFileDto;
import com.example.entity.enums.FileCategoryEnums;
import com.example.entity.enums.FileFolderTypeEnums;
import com.example.entity.enums.ResponseCodeEnum;
import com.example.entity.po.FileInfo;
import com.example.entity.query.FileInfoQuery;
import com.example.entity.vo.FolderVO;
import com.example.mapper.FileInfoMapper;
import com.example.service.impl.FileInfoServiceImpl;
import com.example.utils.CopyTools;
import com.example.utils.StringTools;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.net.URLEncoder;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description: 公共文件控制层
 * Date: 2023-11-23
 * Time: 19:26
 * @author WuYimin
 */
public class CommonFileController extends CommonController {


    @Resource
    private FileInfoServiceImpl fileInfoService;

    @Resource
    private FileInfoMapper fileInfoMapper;

    @Resource
    private AppConfig appConfig;

    @Resource
    private RedisComponent redisComponent;

    /**
     * 根据路径和用户ID获取当前文件夹信息
     * @param path
     * @param userId
     * @return
     */
    public ResponseVO getFolderInfo(String path, String userId) {

        // 将传入的路径字符串按"/"分割，得到一个路径数组 (这个分割是按照你进入的层级目录分割的,把每一层目录分割处理,最后返回的是每个目录的信息)
        String[] pathArray = path.split("/");
        // 创建一个新的FileInfoQuery对象，用于后续的文件信息查询
        FileInfoQuery infoQuery = FileInfoQuery.builder()
                .userId(userId)  //用户ID
                .folderType(FileFolderTypeEnums.FOLDER.getType()) //设置文件类型为目录
                .build();

        // 根据路径数组构建一个orderBy语句，这是为了后续查询的排序条件, MySQL的FIELD()函数用于在指定的列值列表中设置值的排序顺序
        // StringUtils.join(pathArray, "\",\"")这段代码将路径数组转换为字符串，并用,连接。例如，["a", "b", "c"]会被转换为"a","b","c"
        // 最后得到的orderBy = field(file_id,"folder1","folder2","folder3")
        // 如果存在就(file_id能匹配pathArray里面的值),orderBy 会根据 pathArray数组里面的元素进行排序,否则,就放在最前面(默认升序)
        String orderBy = "field(file_id,\"" + StringUtils.join(pathArray, "\",\"") + "\")";

        // 调用fileInfoService的findListByParam方法，
        List<FileInfo> fileInfoList = this.fileInfoMapper.findListByParam(infoQuery,orderBy,pathArray);
        // 将查询到的文件信息列表转换为FolderVO对象列表，并返回成功的响应
        return Result.success(CopyTools.copyList(fileInfoList, FolderVO.class));
    }

    /**
     * 根据图片文件夹和图片名称获取图片
     * @param response
     * @param imageFolder
     * @param imageName
     */
    public void getImage(HttpServletResponse response, String imageFolder, String imageName) {

        // 判断传入的图片文件夹名和图片名是否为空或者是空白字符
        if (StringTools.isEmpty(imageFolder) || StringUtils.isBlank(imageName)) {
            return;  // 如果任何一个参数为空，则直接结束此方法
        }
        // 使用工具类获取图片文件名的后缀（例如.jpg、.png等）
        String imageSuffix = StringTools.getFileSuffix(imageName);
        // 根据传入的imageFolder和imageName构造完整的文件路径
        String filePath = appConfig.getProjectFolder() + Constants.FILE_FOLDER_FILE + imageFolder + "/" + imageName;
        // 将得到的图片文件后缀中的"."移除，例如将".jpg"变为"jpg"
        imageSuffix = imageSuffix.replace(".", "");
        // 根据处理后的后缀构造响应的contentType，如"image/jpg"
        String contentType = "image/" + imageSuffix;
        // 设置响应的内容类型为刚刚构造的contentType
        response.setContentType(contentType);
        // 设置响应的缓存控制头，使得浏览器可以缓存图片30天（2592000秒）
        response.setHeader("Cache-Control", "max-age=2592000");
        // 调用readFile方法读取图片文件并将内容写入到响应输出流中
        readFile(response, filePath);
    }

    /**
     * 获取文件
     * @param response
     * @param fileId
     * @param userId
     */
    protected void getFile(HttpServletResponse response, String fileId, String userId) {

        // 初始化文件路径为null
        String filePath = null;
        // 判断文件ID是否以".ts"结尾，一般.ts表示视频流切片
        if (fileId.endsWith(".ts")) {
            // 根据"_"进行拆分，例如假设fileId为"12345_video.ts"，则tsAarray为["12345","video.ts"]
            String[] tsAarray = fileId.split("_");
            // 获取真实的文件ID，即数组的第一个元素
            String realFileId = tsAarray[0];
            // 通过文件ID和用户ID获取指定文件
            FileInfo fileInfo = fileInfoService.getFileInfoByFileIdAndUserId(realFileId, userId);
            // 如果没有找到相应的文件信息，则结束方法
            if (fileInfo == null) {
                return;
            }
            // 获取文件路径
            String fileName = fileInfo.getFilePath();

            // 重新设置文件名为不带后缀的文件名，然后与fileId拼接
            fileName = StringTools.getFileNameNoSuffix(fileName) + "/" + fileId;

            // 构建完整的文件路径
            filePath = appConfig.getProjectFolder() + Constants.FILE_FOLDER_FILE + fileName;
        } else {
            // 如果文件ID不是以.ts结尾，则通过文件ID和用户ID直接从服务中获取文件信息
            FileInfo fileInfo = fileInfoService.getFileInfoByFileIdAndUserId(fileId, userId);

            // 如果没有找到相应的文件信息，则结束方法
            if (fileInfo == null) {
                return;
            }
            // 判断文件类别是否为视频
            if (FileCategoryEnums.VIDEO.getCategory().equals(fileInfo.getFileCategory())) {
                // 如果是视频文件，则重新设置文件路径为不带后缀的文件路径，并与.m3u8文件名拼接
                String fileNameNoSuffix = StringTools.getFileNameNoSuffix(fileInfo.getFilePath());
                filePath = appConfig.getProjectFolder() + Constants.FILE_FOLDER_FILE + fileNameNoSuffix + "/" + Constants.M3U8_NAME;
            } else {
                // 如果不是视频文件，则直接构建完整的文件路径
                filePath = appConfig.getProjectFolder() + Constants.FILE_FOLDER_FILE + fileInfo.getFilePath();
            }
        }
        // 创建一个File对象，表示该文件路径
        File file = new File(filePath);
        // 如果该文件不存在，则结束方法
        if (!file.exists()) {
            return;
        }
        // 调用readFile方法读取文件内容并写入响应输出流中
        readFile(response, filePath);
    }

    /**
     * 为指定的文件创建下载链接。
     * @param fileId   需要下载的文件ID
     * @param userId   用户ID，可能用于权限或身份验证
     * @return         返回一个包含下载码的响应对象
     */
    protected ResponseVO createDownloadUrl(String fileId, String userId) {

        // 通过文件ID和用户ID 获取 当前文件信息
        FileInfo fileInfo = fileInfoService.getFileInfoByFileIdAndUserId(fileId, userId);
        // 如果没有找到对应的文件信息，抛出业务异常
        if (fileInfo == null) {
            throw new BusinessException(ResponseCodeEnum.CODE_600);
        }

        // 判断获取到的文件是否是一个文件夹，如果是，则抛出业务异常 （因为下载操作只针对文件，不针对文件夹）
        if (FileFolderTypeEnums.FOLDER.getType().equals(fileInfo.getFolderType())) {
            throw new BusinessException(ResponseCodeEnum.CODE_600);
        }
        // 生成一个随机的50位长度的字符串作为下载码
        String code = StringTools.getRandomString(Constants.LENGTH_50);

        DownloadFileDto downloadFileDto = DownloadFileDto.builder()
                .downloadCode(code)  // 设置下载码
                .filePath(fileInfo.getFilePath())  // 设置当前文件的路径
                .fileName(fileInfo.getFileName())  // 设置当前的文件名
                .build();

        // 将下载链接的信息保存在Redis里面（时长为5分钟）
        // 这样在之后的下载请求中，可以通过下载码找到对应的文件信息
        redisComponent.saveDownloadCode(code, downloadFileDto);

        // 返回一个包含下载码的成功响应对象，客户端可以使用这个下载码来实际下载文件
        return Result.success(code);
    }

    /**
     * 通过下载码来下载文件
     * @param request  HttpServletRequest对象，用于获取请求头信息，如User-Agent
     * @param response HttpServletResponse对象，用于设置响应头和返回文件内容
     * @param code     用于在Redis中查找相关文件信息的下载码
     * @throws Exception 如果在处理中发生错误，可能会抛出异常
     */
    protected void download(HttpServletRequest request, HttpServletResponse response, String code) throws Exception {

        // 通过下载码从Redis获取文件
        DownloadFileDto downloadFileDto = redisComponent.getDownloadCode(code);
        // 如果Redis中没有找到，结束此方法
        if (null == downloadFileDto) {
            return;
        }
        // 构造文件的完整路径，将文件的相对路径与项目配置的基础目录结合起来 （找到文件所在的路径位置）
        String filePath = appConfig.getProjectFolder() + Constants.FILE_FOLDER_FILE + downloadFileDto.getFilePath();
        // 获取要下载的文件名
        String fileName = downloadFileDto.getFileName();
        // 设置响应内容类型为可下载文件，并指定字符集为UTF-8
        response.setContentType("application/x-msdownload; charset=UTF-8");
        // 根据请求头中的User-Agent来判断客户端浏览器类型
        if (request.getHeader("User-Agent").toLowerCase().indexOf("msie") > 0) {
            // 如果是IE浏览器，使用UTF-8编码来处理文件名，以防止中文乱码
            fileName = URLEncoder.encode(fileName, "UTF-8");
        } else {
            // 对于非IE浏览器，使用ISO8859-1来处理文件名
            fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
        }
        // 设置响应头，告知客户端这是一个附件，提供下载的文件名
        response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");
        // 读取文件内容并发送给客户端
        readFile(response, filePath);
    }
}
