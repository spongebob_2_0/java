package com.example.controller;

import com.example.entity.constants.Constants;
import com.example.entity.dto.SessionShareDto;
import com.example.entity.dto.SessionWebUserDto;
import com.example.entity.vo.PaginationResultVO;
import com.example.utils.CopyTools;
import com.example.utils.StringTools;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * 公共控制层类
 * @author WuYimin
 */
@Slf4j
public class CommonController {


    /**
     * 转换分页结果，用于返回给前端的
     * @param result
     * @param classz
     * @return
     * @param <S>
     * @param <T>
     */
    protected <S, T> PaginationResultVO<T> convert2PaginationVO(PaginationResultVO<S> result, Class<T> classz) {
        PaginationResultVO<T> resultVO = new PaginationResultVO<>();
        resultVO.setList(CopyTools.copyList(result.getList(), classz)); // 复制并转换列表数据
        resultVO.setPageNo(result.getPageNo()); // 设置页码
        resultVO.setPageSize(result.getPageSize()); // 设置每页大小
        resultVO.setPageTotal(result.getPageTotal()); // 设置总页数
        resultVO.setTotalCount(result.getTotalCount()); // 设置总数据量
        return resultVO; // 返回转换后的VO对象
    }

    //定义一个方法从session中获取用户信息
    protected SessionWebUserDto getUserInfoFromSession(HttpSession session) {
        // 从session中获取用户数据
        SessionWebUserDto sessionWebUserDto = (SessionWebUserDto) session.getAttribute(Constants.SESSION_KEY);
        return sessionWebUserDto; // 返回用户数据
    }

    //定义一个方法从session中获取分享会话信息
    protected SessionShareDto getSessionShareFromSession(HttpSession session, String shareId) {
        // 从session中获取分享数据
        SessionShareDto sessionShareDto = (SessionShareDto) session.getAttribute(Constants.SESSION_SHARE_KEY + shareId);
        return sessionShareDto; // 返回分享数据
    }

    //定义一个方法读取并输出文件内容到响应中
    //使用流的方式读取并输出文件内容到响应中可以提高性能、节省内存，同时能够更快地响应用户的下载请求
    protected void readFile(HttpServletResponse response, String filePath) {
        if (!StringTools.pathIsOk(filePath)) { // 判断文件路径是否合法
            return; // 路径不合法直接返回
        }
        OutputStream out = null; // 定义输出流
        FileInputStream in = null; // 定义文件输入流
        try {
            File file = new File(filePath); // 根据路径创建文件对象
            if (!file.exists()) { // 判断文件是否存在
                return; // 文件不存在直接返回
            }
            in = new FileInputStream(file); // 创建文件输入流
            byte[] byteData = new byte[1024]; // 定义缓冲区
            out = response.getOutputStream(); // 获取响应的输出流
            int len = 0; // 定义读取的长度
            while ((len = in.read(byteData)) != -1) { // 循环读取文件内容
                out.write(byteData, 0, len); // 将读取的内容写入到响应输出流中
            }
            out.flush(); // 刷新输出流
        } catch (Exception e) {
            log.error("读取文件异常", e); // 记录异常日志
        } finally {
            if (out != null) { // 判断输出流是否非空
                try {
                    out.close(); // 关闭输出流
                } catch (IOException e) {
                    log.error("IO异常", e); // 记录IO异常日志
                }
            }
            if (in != null) { // 判断输入流是否非空
                try {
                    in.close(); // 关闭输入流
                } catch (IOException e) {
                    log.error("IO异常", e); // 记录IO异常日志
                }
            }
        }
    }
}
