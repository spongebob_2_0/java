package com.example.controller;

import com.example.annotation.GlobalInterceptor;
import com.example.annotation.VerifyParam;
import com.example.component.RedisComponent;
import com.example.component.ResponseVO;
import com.example.component.Result;
import com.example.entity.dto.SysSettingsDto;
import com.example.entity.query.FileInfoQuery;
import com.example.entity.query.UserInfoQuery;
import com.example.entity.vo.PaginationResultVO;
import com.example.entity.vo.UserInfoVO;
import com.example.service.impl.FileInfoServiceImpl;
import com.example.service.impl.UserInfoServiceImpl;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * Date: 2023-11-25
 * Time: 19:50
 * @author WuYimin
 */
@RestController
@RequestMapping("/admin")
public class AdminController extends CommonFileController{

    @Resource
    private RedisComponent redisComponent;

    @Resource
    private UserInfoServiceImpl userInfoService;

    @Resource
    private FileInfoServiceImpl fileInfoService;

    /**
     * 获取系统设置
     * @return
     */
    @RequestMapping("/getSysSettings")
    // 使用@GlobalInterceptor注解进行全局拦截，检查参数和管理员权限
    @GlobalInterceptor(checkParams = true, checkAdmin = true)
    public ResponseVO getSysSettings() {
        // 调用redisComponent的getSysSettingsDto方法获取系统设置，并通过getSuccessResponseVO方法构建一个成功的响应返回
        return Result.success(redisComponent.getSysSettingsDto());
    }


    /**
     * 保存系统设置
     * @param registerEmailTitle
     * @param registerEmailContent
     * @param userInitUseSpace
     * @return
     */
    @RequestMapping("/saveSysSettings")
    // 使用@GlobalInterceptor注解进行全局拦截，检查参数和管理员权限
    @GlobalInterceptor(checkParams = true, checkAdmin = true)
    public ResponseVO saveSysSettings(
            // 使用@VerifyParam注解确保请求参数"registerEmailTitle"存在并且是必需的
            @VerifyParam(required = true) String registerEmailTitle,
            // 使用@VerifyParam注解确保请求参数"registerEmailContent"存在并且是必需的
            @VerifyParam(required = true) String registerEmailContent,
            // 使用@VerifyParam注解确保请求参数"userInitUseSpace"存在并且是必需的，数据类型为Integer
            @VerifyParam(required = true) Integer userInitUseSpace) {

        // 创建一个SysSettingsDto对象
        SysSettingsDto sysSettingsDto = SysSettingsDto.builder()
                .registerEmailTitle(registerEmailTitle)      // 注册邮件标题属性
                .registerEmailContent(registerEmailContent)  // 注册邮件内容属性
                .userInitUseSpace(userInitUseSpace)          // 初始化用户使用空间
                .build();
        // 将系统设置保存到Redis中
        redisComponent.saveSysSettingsDto(sysSettingsDto);
        // 返回一个成功的响应，不包含额外数据
        return Result.success(null);
    }


    /**
     * 查询用户列表
     * @param userInfoQuery
     * @return
     */
    @RequestMapping("/loadUserList")
    // 使用@GlobalInterceptor注解进行全局拦截，进行参数校验和管理员权限校验
    @GlobalInterceptor(checkParams = true, checkAdmin = true)
    public ResponseVO loadUser(UserInfoQuery userInfoQuery) {
        // 设置userInfoQuery的排序方式为加入时间降序
        userInfoQuery.setOrderBy("join_time desc");
        // 调用userInfoService的findListByPage方法，根据分页信息查询用户列表
        PaginationResultVO resultVO = userInfoService.findListByPage(userInfoQuery);
        // 转换查询结果为分页VO对象，并返回成功的响应，包含查询到的用户列表数据
        return Result.success(convert2PaginationVO(resultVO, UserInfoVO.class));
    }

    /**
     * 更新用户状态
     * @param userId
     * @param status
     * @return
     */
    @RequestMapping("/updateUserStatus")
    // 使用@GlobalInterceptor注解进行全局拦截，进行参数校验和管理员权限校验
    @GlobalInterceptor(checkParams = true, checkAdmin = true)
    public ResponseVO updateUserStatus(
            // 使用@VerifyParam注解确保请求参数"userId"存在并且是必需的
            @VerifyParam(required = true) String userId,
            // 使用@VerifyParam注解确保请求参数"status"存在并且是必需的，数据类型为Integer
            @VerifyParam(required = true) Integer status) {
        // 调用userInfoService的updateUserStatus方法，更新指定用户的状态
        userInfoService.updateUserStatus(userId, status);
        // 返回一个成功的响应，不包含额外数据
        return Result.success(null);
    }

    /**
     * 更新用户空间
     * @param userId
     * @param changeSpace
     * @return
     */
    @RequestMapping("/updateUserSpace")
    // 使用@GlobalInterceptor注解进行全局拦截，进行参数校验和管理员权限校验
    @GlobalInterceptor(checkParams = true, checkAdmin = true)
    public ResponseVO updateUserSpace(
            // 使用@VerifyParam注解确保请求参数"userId"存在并且是必需的
            @VerifyParam(required = true) String userId,
            // 使用@VerifyParam注解确保请求参数"changeSpace"存在并且是必需的，数据类型为Integer
            @VerifyParam(required = true) Integer changeSpace) {
        // 调用userInfoService的changeUserSpace方法，更新指定用户的总空间
        userInfoService.changeUserSpace(userId, changeSpace);
        // 返回一个成功的响应，不包含额外数据
        return Result.success(null);
    }

    /**
     * 查询所有文件
     * @param query
     * @return
     */
    @RequestMapping("/loadFileList")
    // 使用@GlobalInterceptor注解对该方法进行全局拦截，进行参数校验和管理员权限校验
    @GlobalInterceptor(checkParams = true,checkAdmin = true)
    public ResponseVO loadDataList(FileInfoQuery query) {
        // 设置查询结果的排序方式为按照"last_update_time"的降序排列
        query.setOrderBy("last_update_time desc");
        // 设置查询时需要获取文件的昵称
        query.setQueryNickName(true);
        // 调用fileInfoService的findListByPage方法，根据查询条件进行分页查询，并获取结果
        PaginationResultVO resultVO = fileInfoService.findListByPage(query);
        // 返回一个包含查询结果的成功响应
        return Result.success(resultVO);
    }

    /**
     * 获取当前目录里面的信息
     * @param path
     * @return
     */
    @RequestMapping("/getFolderInfo")
    // 使用@GlobalInterceptor注解对该方法进行全局拦截，进行参数校验，允许未登录用户访问，进行管理员权限校验
    @GlobalInterceptor(checkLogin = false,checkAdmin = true, checkParams = true)
    public ResponseVO getFolderInfo(
            // 使用@VerifyParam注解，确保请求参数"path"存在并且是必需的
            @VerifyParam(required = true) String path) {

        // 调用父类的getFolderInfo方法，传入路径参数，获取文件夹信息
        return super.getFolderInfo(path, null);
    }

    /**
     * 获取当前文件信息
     * @param response
     * @param userId
     * @param fileId
     */
    @RequestMapping("/getFile/{userId}/{fileId}")
    // 使用@GlobalInterceptor注解对该方法进行全局拦截，进行参数校验和管理员权限校验
    @GlobalInterceptor(checkParams = true, checkAdmin = true)
    public void getMyFile(
            // HttpServletResponse对象，用于向客户端发送响应
            HttpServletResponse response,
            // 从路径中获取名为"userId"的参数，并使用@VerifyParam注解确保该参数存在且是必需的
            @PathVariable("userId") @VerifyParam(required = true) String userId,
            // 从路径中获取名为"fileId"的参数，并使用@VerifyParam注解确保该参数存在且是必需的
            @PathVariable("fileId") @VerifyParam(required = true) String fileId) {

        // 调用父类的getFile方法，传入响应对象、文件ID和用户ID，获取文件
        super.getFile(response, fileId, userId);
    }

    /**
     * 获取视频信息
     * @param response
     * @param userId
     * @param fileId
     */
    @RequestMapping("/ts/getVideoInfo/{userId}/{fileId}")
    // 使用@GlobalInterceptor注解对该方法进行全局拦截，进行参数校验和管理员权限校验
    @GlobalInterceptor(checkParams = true, checkAdmin = true)
    public void getVideoInfo(
            // HttpServletResponse对象，用于向客户端发送响应
            HttpServletResponse response,
            // 从路径中获取名为"userId"的参数，并使用@VerifyParam注解确保该参数存在且是必需的
            @PathVariable("userId") @VerifyParam(required = true) String userId,
            // 从路径中获取名为"fileId"的参数，并使用@VerifyParam注解确保该参数存在且是必需的
            @PathVariable("fileId") @VerifyParam(required = true) String fileId) {

        // 调用父类的getFile方法，传入响应对象、文件ID和用户ID，获取视频文件信息
        super.getFile(response, fileId, userId);
    }

    /**
     * 创建下载链接
     * @param userId   需要下载的文件ID
     * @param fileId   用户ID，可能用于权限或身份验证
     * @return
     */
    @RequestMapping("/createDownloadUrl/{userId}/{fileId}")
    // 使用@GlobalInterceptor注解对该方法进行全局拦截，进行参数校验和管理员权限校验
    @GlobalInterceptor(checkParams = true, checkAdmin = true)
    public ResponseVO createDownload(
            // 从路径中获取名为"userId"的参数，并使用@VerifyParam注解确保该参数存在且是必需的
            @PathVariable("userId") @VerifyParam(required = true) String userId,
            // 从路径中获取名为"fileId"的参数，并使用@VerifyParam注解确保该参数存在且是必需的
            @PathVariable("fileId") @VerifyParam(required = true) String fileId) {

        // 调用父类的createDownloadUrl方法，传入文件ID和用户ID，创建下载链接
        return super.createDownloadUrl(fileId, userId);
    }


    /**
     * 该方法用于下载文件
     * @param request HttpServletRequest对象，代表HTTP请求
     * @param response HttpServletResponse对象，代表HTTP响应
     * @param code 下载文件的编码，路径变量
     * @throws Exception 如果下载过程中出现异常则抛出
     */
    @RequestMapping("/download/{code}")
    // 使用@GlobalInterceptor注解对该方法进行全局拦截，进行参数校验，但不检查登录状态
    @GlobalInterceptor(checkLogin = false, checkParams = true)
    public void downloadByCode(HttpServletRequest request, HttpServletResponse response,
                         // 从路径中获取名为"code"的参数，并使用@VerifyParam注解确保该参数存在且是必需的
                         @PathVariable("code") @VerifyParam(required = true) String code)
            throws Exception {

        // 调用父类的download方法，进行文件下载的具体操作
        super.download(request, response, code);
    }

    /**
     * 该方法用于删除文件
     * @param fileIdAndUserIds 多个文件用","分隔,每个文件名由文件ID和用户ID组成
     * @return 返回一个响应对象，包含操作结果信息
     */
    @RequestMapping("/delFile")
    // 使用@GlobalInterceptor注解对该方法进行全局拦截，进行参数校验和管理员权限校验
    @GlobalInterceptor(checkParams = true, checkAdmin = true)
    // 接收一个String类型的参数，该参数必需存在，表示由文件ID和用户ID组成的字符串
    public ResponseVO delFile(@VerifyParam(required = true) String fileIdAndUserIds) {

        // 多个文件之间是用","号进行分割的,所以可以得到多个文件组成的数组
        String[] fileIdAndUserIdArray = fileIdAndUserIds.split(",");
        // 通过遍历依次删除数组中的每个文件 (也就是管理员选择要删除的所有文件)
        for (String fileIdAndUserId : fileIdAndUserIdArray) {
            // 每个文件的文件名是有用户ID和文件ID组成的, 中间用"_"进行分割,
            String[] itemArray = fileIdAndUserId.split("_");
            // 调用fileInfoService的delFileBatch方法，进行文件删除操作
            // 数组的第0个位置是用户ID,第一个位置是文件ID,并且是有管理员操作
            fileInfoService.delFileBatch(itemArray[0], itemArray[1], true);
        }

        return Result.success(null);
    }

}
