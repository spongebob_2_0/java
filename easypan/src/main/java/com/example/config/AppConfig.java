package com.example.config;


import com.example.utils.StringTools;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


/**
 * 配置文件读取配置
 * @author WuYimin
 */
@Component("appConfig")
@Data
public class AppConfig {

    // 定义一个私有的静态常量logger，用于日志记录，它关联了当前类AppConfig的日志工厂
    private static final Logger logger = LoggerFactory.getLogger(AppConfig.class);

    // 使用@Value注解从配置中获取project.folder的值，给文件目录变量赋值
    @Value("${project.folder:}")
    private String projectFolder;

    // 使用@Value注解从配置中获取spring.mail.username的值，给发送人变量赋值
    @Value("${spring.mail.username:}")
    private String sendUserName;

    // 使用@Value注解从配置中获取admin.emails的值，给管理员邮箱变量赋值
    @Value("${admin.emails}")
    private String adminEmails;

    // 公开的getter方法，获取管理员邮箱的值
    public String getAdminEmails() {
        return adminEmails;
    }

    // 使用@Value注解从配置中获取dev的值，给开发模式变量赋值
    @Value("${dev:false}")
    private Boolean dev;

    // 下面的变量和注解都是与QQ的应用配置相关的

    @Value("${qq.app.id:}")
    private String qqAppId;

    @Value("${qq.app.key:}")
    private String qqAppKey;

    @Value("${qq.url.authorization:}")
    private String qqUrlAuthorization;

    @Value("${qq.url.access.token:}")
    private String qqUrlAccessToken;

    @Value("${qq.url.openid:}")
    private String qqUrlOpenId;

    @Value("${qq.url.user.info:}")
    private String qqUrlUserInfo;

    @Value("${qq.url.redirect:}")
    private String qqUrlRedirect;

    // 公开的getter方法，返回文件目录，如果它不是以/结尾，则添加/
    public String getProjectFolder() {
        if (!StringTools.isEmpty(projectFolder) && !projectFolder.endsWith("/")) {
            projectFolder = projectFolder + "/";
        }
        return projectFolder;
    }

    // 公开的getter方法，返回logger对象
    public static Logger getLogger() {
        return logger;
    }

    // 公开的getter方法，返回发送人的用户名
    public String getSendUserName() {
        return sendUserName;
    }

    // 公开的getter方法，返回是否是开发模式
    public Boolean getDev() {
        return dev;
    }
}
