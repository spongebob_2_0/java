## 1. 获取文件信息

> 这里的获取文件信息就是播放视频，展示pdf，显示图片等等的接口原理都差不多，我们通过观察代码可以发现，都是通过找到文件所在磁盘位置，通过IO流的形式返回给前端的，对于流，前端会自动根据流的响应头中的格式做出解析，然后展示在前端页面

![image-20240308203552834](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202403082035901.png)

![image-20240308203952535](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202403082039614.png)

## 2.分片上传

分片上传是一种将大文件分割成多个小片（分片）并分别上传的技术。当所有分片都上传完成后，再将它们合并成原始文件的过程。这种方式主要用于提高大文件上传的效率和可靠性，特别是在网络条件不稳定的情况下。分片上传的实现步骤通常包括：

1. **客户端分片**：客户端将大文件分割成多个小片，每个小片可以并发上传。
2. **服务器接收**：服务器端接收这些文件片，并且每接收到一个片段就暂时存储起来。
3. **合并分片**：当所有分片都上传完成后，服务器端将这些片段按照顺序合并成一个完整的文件。

### 2.1 前端上传流程和逻辑

1. **文件添加与分片准备**: 用户添加文件后，组件首先计算该文件的MD5值，用于唯一标识文件，并检查文件是否已存在（秒传功能）。文件被分成多个分片，每个分片的大小由`chunkSize`变量定义（这里设定是5MB）。
2. **分片上传**: 使用`for`循环遍历每个分片，依次发送HTTP请求上传分片。上传过程考虑了用户的暂停/继续操作以及删除操作。
3. **上传控制**: 用户可以暂停当前的上传任务，然后再次启动。如果用户删除了上传任务，则会停止上传过程。
4. **进度反馈**: 上传过程中，通过计算已上传的分片和总分片的比例，实时更新文件的上传进度。
5. **上传完成处理**: 当所有分片上传完成后，后端服务器将这些分片组合成原始文件。如果上传过程中出现任何错误，上传任务将标记为失败，并显示相应的错误信息。

### 2.2 代码实现

#### 2.2.1 初始化及文件添加

当用户选择文件并添加到上传队列时，会触发`addFile`函数。这个函数负责初始化文件对象，并将其添加到`fileList`响应式数组中。接下来，为每个文件计算MD5值，并在计算完成后开始上传过程。

```javascript
// 添加文件到列表并计算MD5
const addFile = async (file, filePid) => {
  // 初始化文件对象
  const fileItem = {
    file: file,
    uid: file.uid,
    md5Progress: 0, // MD5计算进度初始化为0
    md5: null, // MD5值初始化为空
    fileName: file.name, // 文件名
    status: STATUS.init.value, // 初始状态为解析中
    uploadSize: 0, // 已上传大小为0
    totalSize: file.size, // 文件总大小
    uploadProgress: 0, // 上传进度为0
    pause: false, // 初始未暂停
    chunkIndex: 0, // 当前分片索引为0
    filePid: filePid, // 文件父级ID
    errorMsg: null, // 错误信息为空
  };
  fileList.value.unshift(fileItem); // 将文件对象添加到上传列表
  
  // 如果文件大小为0，则标记为文件为空状态并返回
  if (fileItem.totalSize == 0) {
    fileItem.status = STATUS.emptyfile.value;
    return;
  }
  
  // 异步计算文件MD5值
  let md5FileUid = await computeMD5(fileItem);
  if (md5FileUid != null) {
    // 如果计算成功，则开始上传文件
    uploadFile(md5FileUid);
  }
};
```

#### 2.2.2 分片上传逻辑

对于大文件，将其分割成多个小块（分片）上传是提高上传效率和实现断点续传的关键。下面是实现分片上传的核心逻辑：

```javascript
// 上传文件，处理每个分片
const uploadFile = async (uid, chunkIndex = 0) => {
  let currentFile = getFileByUid(uid); // 根据uid获取当前文件对象
  const file = currentFile.file; // 文件对象
  const fileSize = currentFile.totalSize; // 文件总大小
  const chunks = Math.ceil(fileSize / chunkSize); // 计算总分片数

  // 遍历并上传每个分片
  for (let i = chunkIndex; i < chunks; i++) {
    if (currentFile.pause || delList.value.includes(uid)) {
      // 如果上传被暂停或文件被删除，则停止上传
      break;
    }

    let start = i * chunkSize; // 分片起始位置
    let end = Math.min(start + chunkSize, fileSize); // 分片结束位置
    let chunkFile = file.slice(start, end); // 根据起始和结束位置切割文件

    // 发起上传请求，此处省略具体实现细节
    // 假设uploadChunk是一个上传分片的函数，需要传入分片文件、当前分片索引、文件的MD5值等
    let uploadResult = await uploadChunk(chunkFile, i, currentFile.md5);

    if (uploadResult.success) {
      // 如果分片上传成功，更新当前分片索引和上传进度等信息
      currentFile.chunkIndex = i + 1;
      currentFile.uploadSize += chunkFile.size;
      currentFile.uploadProgress = Math.floor((currentFile.uploadSize / fileSize) * 100);
    } else {
      // 如果上传失败，处理错误情况
      currentFile.status = STATUS.fail.value;
      currentFile.errorMsg = uploadResult.message;
      break; // 停止上传后续分片
    }

    if (i === chunks - 1) {
      // 如果是最后一个分片，标记上传完成
      currentFile.status = STATUS.upload_finish.value;
      currentFile.uploadProgress = 100;
      emit("uploadCallback"); // 触发上传完成回调事件


    }
  }
};
```

#### 2.2.3 暂停、继续和删除上传任务

用户可以随时暂停、继续或删除上传任务，这些操作通过修改文件对象的状态来实现。

```javascript
// 暂停上传
const pauseUpload = (uid) => {
  let currentFile = getFileByUid(uid);
  currentFile.pause = true; // 设置暂停标志
};

// 继续上传
const startUpload = (uid) => {
  let currentFile = getFileByUid(uid);
  currentFile.pause = false; // 清除暂停标志
  uploadFile(uid, currentFile.chunkIndex); // 从暂停的分片继续上传
};

// 删除上传任务
const delUpload = (uid, index) => {
  delList.value.push(uid); // 标记文件为删除
  fileList.value.splice(index, 1); // 从列表中移除
};
```



### 2.3 后端上传流程和逻辑

1. **接收文件分片**: 后端接口接收来自前端的文件分片及相关信息，包括文件名、分片索引、总分片数等。

2. **计算MD5值**: 利用前端发送的文件MD5值来实现秒传功能，即判断系统中是否已存在相同MD5值的文件。若存在，则不需要重新上传，直接使用已有文件。

3. **文件分片处理**: 根据分片索引和总分片数，将接收到的文件分片暂存到服务器的临时目录中。

4. **文件分片合并**: 当所有分片都上传完成后，后端将这些分片合并成一个完整的文件。

5. **更新用户存储空间**: 文件上传成功后，更新用户的存储空间信息。

6. **异常处理**: 对整个上传过程中可能发生的异常进行捕获和处理，确保系统的稳定性和数据的一致性。

#### 2.3.1 接收文件分片

```java
@Transactional(rollbackFor = Exception.class)
public UploadResultDto uploadFile(...) {
    ...
    // 判断是否为第一个分片，若是，则检查是否可以实现秒传
    if (chunkIndex == 0) {
        ...
        // 检查数据库中是否存在相同MD5的文件
        if (!dbFileList.isEmpty()) {
            // 实现秒传逻辑，省略详细代码
            ...
            return resultDto;
        }
    }
    ...
    // 处理文件分片，保存到临时目录中
    ...
    // 如果是最后一个分片，则执行合并操作
    if (chunkIndex == chunks - 1) {
        // 合并文件分片
        ...
    }
    ...
}
```

#### 2.3.2 秒传功能实现

```java
if (!dbFileList.isEmpty()) {
    // 如果存在相同MD5的文件，说明可以实现秒传，直接使用已存在的文件
    ...
    // 更新用户存储空间信息，返回秒传成功的响应
    updateUserSpace(webUserDto, dbFile.getFileSize());
    ...
    return resultDto;
}
```

![image-20240308203246886](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202403082032958.png)

#### 2.3.3 文件分片合并及异步处理

```java
// 所有文件上传完成后，合并文件分片
if (chunkIndex == chunks - 1) {
    ...
    // 异步处理文件合并和转存
    TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronization() {
        @Override
        public void afterCommit() {
            // 异步执行文件合并等操作
            fileInfoService.transferFile(fileInfo.getFileId(), webUserDto);
        }
    });
    ...
}
```

#### 2.3.4 异常处理

```java
catch (BusinessException e) {
    // 捕获自定义的业务异常
    ...
    throw e;
} catch (Exception e) {
    // 捕获其他异常，例如文件IO异常等
    ...
    throw new BusinessException("文件上传失败");
} finally {
    // 清理资源，例如删除临时文件等
    ...
}
```

#### 2.3.5 总结

整个文件上传流程中，后端负责接收文件分片、验证文件完整性（通过MD5）、处理文件存储（包括秒传和分片合并）以及更新用户存储空间信息。这个过程需要考虑事务的一致性，确保在发生异常时能够回滚，避免产生脏数据。通过`@Transactional`注解确保了操作的原子性。此外，利用异步处理提高了文件处理的效率，特别是在文件合并和转存时。

## 3. 后端异步合并

异步合并是在所有分片上传完成后，将它们合并成原始文件的过程。这个过程不阻塞用户操作，提高了应用的响应性能。`transferFile`方法用于异步执行文件分片的合并操作。它使用`@Async`注解标记为异步方法，以非阻塞的方式执行文件合并和处理（如视频转码、生成缩略图等）。

## 4. 循环依赖

### 4.1 循环依赖的产生

当`uploadFile`方法执行完毕，如果在文件上传的过程中没有发生任何导致事务回滚的异常，那么事务将会被提交，随后`TransactionSynchronizationManager.registerSynchronization`中注册的`afterCommit`回调就会被触发，执行`fileInfoService.transferFile`方法进行文件合并的操作。

![image-20240305142612479](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202403051426533.png)

> 当我们在自己的类中去注入了一个自己的实例去使用时，此时循环依赖就产生了。

**为何使用`fileInfoService`调用自己的`transferFile`方法**？

在Spring中，当你直接在一个类的内部方法中调用另一个方法时，这个调用是直接通过Java的正常方法调用机制进行的，`不会经过Spring的代理机制`。这意味着：

- **任何基于代理的功能（如声明式事务、基于注解的安全性、自定义注解等）都不会被触发。** 由于`@Async`和`@Transactional`等注解是基于Spring的AOP代理实现的，直接内部调用不会触发这些注解的行为。
- **为了确保`@Async`、`@Transactional`等注解的效果能够被正确应用，需要通过Spring的代理对象来调用目标方法。** 这就是为什么在`uploadFile`方法中使用`fileInfoService`（它是`FileInfoServiceImpl`的一个代理）来调用`transferFile`方法。这样，Spring就能够识别到这个调用，并应用相关的注解逻辑，如异步执行和事务管理。

### 4.2 解决循环依赖

循环依赖是指在Spring应用中，两个或多个Bean互相依赖对方，导致无法正常创建Bean实例的问题。Spring框架通过三级缓存解决了单例Bean的循环依赖问题，但对于`prototype`作用域的Bean或者手动触发的Bean创建过程，Spring无法自动解决循环依赖。

在我的项目中，是通过`@Lazy`注解延迟初始化Bean来解决循环依赖的问题。`@Lazy`确保Bean在第一次使用时才被创建，而不是在应用启动时就创建。这样，即使两个Bean互相引用，也不会在应用启动时立即触发它们的创建，从而避免了循环依赖导致的问题。

## 5. 回收站问题

### 5.1 显示回收站列表

![image-20240308204314093](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202403082043133.png)

### 5.2 批量恢复指定的文件

> 恢复完只更新数据库

![image-20240308204426762](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202403082044802.png)

![image-20240308205646074](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202403082056198.png)

### 5.2 批量删除回收站文件

> 删除完成以后先从数据库更新用户内存大小，再更新Redis用户内存大小

![image-20240308210445736](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202403082104837.png)

## 6. 文件下载

文件下载的时候，前端会调用两个接口

一个用于创建文件下载链接，另一个是根据下载码进行下载（`这个下载码就是创建文件下载链接的接口返回的`）

### 6.1 创建下载链接

![image-20240308211213080](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202403082112125.png)

![image-20240308211848081](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202403082118146.png)

### 6.1 根据`下载码`下载文件

![image-20240308211950145](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202403082119190.png)

![image-20240308212302634](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202403082123694.png)

## 7. 分享文件

### 7.1 分享文件

![image-20240308213507090](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202403082135144.png)

![image-20240308213920261](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202403082139316.png)

### 7.2 取消分享

![image-20240308214048896](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202403082140939.png)

### 7.3 打开分享文件

1. 首先校验分享码是否正确，校验成功以后，后端会生成一个`SessionDto`保存到用户的`session`里面，里面封装了分享id，分享的用户id，分享的文件id，分享的过期时间。记住这个`SessionDto`很重要，后面就是通过在`session`里面去获取这个`SessionDto`检查用户校验分享码是否成功，如果能获取到就证明分享码校验成功了！

#### 7.3.1 校验分享码是否正确

![image-20240308220617117](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202403082206161.png)

![image-20240308214627509](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202403082146566.png)

#### 7.3.2 获取分享的文件列表

![image-20240308220954014](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202403082209082.png)

![image-20240308221036339](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202403082210389.png)

#### 7.3.3 打开分享文件

打开分享文件就是校验一下分享码是否正确，然后获取文件的信息调用的就是公用的获取文件的接口，这个在第一节就讲过了！

![image-20240308221213617](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202403082212661.png)

### 7.3.4 下载分享的文件

这个下载分享的文件调用的都是前面讲过的文件下载的接口，`唯一的区别就是调用接口之前需要校验一下分享码是否正确或者过期了`

![image-20240308221440193](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202403082214254.png)
