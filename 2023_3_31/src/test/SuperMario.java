package test;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class SuperMario extends JPanel implements KeyListener {

    private static final long serialVersionUID = 1L;

    private int width = 600;
    private int height = 400;
    private int marioX = 50;
    private int marioY = 300;
    private int marioSpeedX = 0;
    private int marioSpeedY = 0;
    private boolean jumping = false;
    private int jumpHeight = 100;
    private int jumpCount = 0;
    private int score = 0;
    private Image background;
    private Image mario;
    private Image monster;
    private Image obstacle;
    private List<Monster> monsters;
    private List<Obstacle> obstacles;

    public SuperMario() {
        JFrame frame = new JFrame("Super Mario");
        frame.setSize(width, height);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.addKeyListener(this);
        frame.add(this);
        background = new ImageIcon(getClass().getResource("background.png")).getImage();
        mario = new ImageIcon(getClass().getResource("mario.png")).getImage();
        monster = new ImageIcon(getClass().getResource("monster.png")).getImage();
        obstacle = new ImageIcon(getClass().getResource("obstacle.png")).getImage();
        monsters = new ArrayList<Monster>();
        obstacles = new ArrayList<Obstacle>();
        spawnMonsters();
        spawnObstacles();
    }

    public void paint(Graphics g) {
        g.drawImage(background, 0, 0, this);
        g.drawImage(mario, marioX, marioY, this);
        for (Monster monster : monsters) {
            g.drawImage(this.monster, monster.getX(), monster.getY(), this);
        }
        for (Obstacle obstacle : obstacles) {
            g.drawImage(this.obstacle, obstacle.getX(), obstacle.getY(), this);
        }
        g.setColor(Color.BLACK);
        g.drawString("Score: " + score, 10, 20);
    }

    public void update() {
        moveMario();
        moveMonsters();
        checkCollisions();
        checkOutOfBounds();
        updateScore();
    }

    public void moveMario() {
        marioX += marioSpeedX;
        marioY += marioSpeedY;
        jump();
    }

    public void moveMonsters() {
        for (Monster monster : monsters) {
            monster.move();
        }
    }

    public void checkCollisions() {
        Rectangle marioBounds = new Rectangle(marioX, marioY, mario.getWidth(null), mario.getHeight(null));
        for (Monster monster : monsters) {
            Rectangle monsterBounds = new Rectangle(monster.getX(), monster.getY(), monster.getWidth(), monster.getHeight());
            if (marioBounds.intersects(monsterBounds)) {
                gameOver();
            }
        }
        for (Obstacle obstacle : obstacles) {
            Rectangle obstacleBounds = new Rectangle(obstacle.getX(), obstacle.getY(), obstacle.getWidth(), obstacle.getHeight());
            if (marioBounds.intersects(obstacleBounds)) {
                gameOver();
            }
        }
    }

    public void checkOutOfBounds() {
        if (marioX < 0) {
            marioX = 0;
        }
        if (marioX > width - mario.getWidth(null)) {
            marioX = width - mario.getWidth(null);
        }
        if (marioY > height - mario.getHeight(null)) {
            gameOver();
        }
    }

    public void updateScore() {
        score++;
    }

    public void gameOver() {
        System.out.println("Game over!");
        System.exit(0);
    }

    public void spawnMonsters() {
        Random rand = new Random();
        int x = width;
        int y = 280;
        int speed = -3;
        for (int i = 0; i < 5; i++) {
            monsters.add(new Monster(x, y, speed));
            x += rand.nextInt(200) + 100;
            speed -= 1;
        }
    }

    public void spawnObstacles() {
        Random rand = new Random();
        int x = width;
        int y = 300;
        for (int i = 0; i < 10; i++) {
            obstacles.add(new Obstacle(x, y));
            x += rand.nextInt(200) + 100;
        }
    }

    public void jump() {
        if (jumping) {
            if (jumpCount < jumpHeight) {
                marioY -= 5;
                jumpCount += 5;
            } else if (jumpCount >= jumpHeight && jumpCount < jumpHeight * 2) {
                marioY += 5;
                jumpCount += 5;
            } else {
                jumping = false;
                jumpCount = 0;
            }
        }
    }

    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_SPACE && !jumping) {
            jumping = true;
            jumpCount = 0;
        }
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            marioSpeedX = -5;
        }
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            marioSpeedX = 5;
        }
    }

    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_RIGHT) {
            marioSpeedX = 0;
        }
    }

    public void keyTyped(KeyEvent e) {
    }

    public void gameLoop() {
        while (true) {
            update();
            repaint();
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        SuperMario game = new SuperMario();
        game.gameLoop();
    }

    private class Monster {

        private int x;
        private int y;
        private int speed;

        public Monster(int x, int y, int speed) {
            this.x = x;
            this.y = y;
            this.speed = speed;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public int getWidth() {
            return monster.getWidth(null);
        }

        public int getHeight() {
            return monster.getHeight(null);
        }

        public void move() {
            x += speed;
        }

    }

    private class Obstacle {

        private int x;
        private int y;

        public Obstacle(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public int getWidth() {
            return obstacle.getWidth(null);
        }

        public int getHeight() {
            return obstacle.getHeight(null);
        }

    }
}

