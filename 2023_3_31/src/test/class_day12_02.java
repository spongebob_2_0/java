package test;

import java.util.Scanner;

public class class_day12_02 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for(int i=n/2;i>=2;i--){//从中间找保证了差最小的两个素数
            if(isPrime(i)&&isPrime(n-i)){
                System.out.println(i);
                System.out.println(n-i);
                break;
            }
        }
    }
    public static boolean isPrime(int n){
        for(int i=2;i<n;i++){
            if(n%i==0){   //判断是否为素数
                return false;
            }
        }
        return true;
    }
}
