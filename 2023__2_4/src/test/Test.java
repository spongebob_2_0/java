package test;

//import static test.TestBinaryTree.usedSize;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-02-04
 * Time: 20:08
 */
public class Test {
    public static void main(String[] args) {
        TestBinaryTree testBinaryTree =new TestBinaryTree();
        TestBinaryTree.TreeNode root =testBinaryTree.createTree();
        /*testBinaryTree.preOrder(root);
        System.out.println();
        testBinaryTree.inOrder(root);
        System.out.println();
        testBinaryTree.postOrder(root);*/
        System.out.println("节点个数1:"+testBinaryTree.size(root));
        testBinaryTree.size2(root);
        System.out.println("节点个数2:"+TestBinaryTree.usedSize);

        System.out.println("叶子个数1:"+testBinaryTree.getLeafNodeCount(root));
        testBinaryTree.getLeafNodeCount2(root);
        System.out.println("叶子个数2:"+TestBinaryTree.count);

        System.out.println("第k层节点的个数:"+testBinaryTree.getkleveNodeCount(root, 3));

        System.out.println("树的高度:"+testBinaryTree.getHeight(root));

        System.out.println("树的深度:"+testBinaryTree.maxDepth(root));

        TestBinaryTree.TreeNode node =testBinaryTree.find(root,'E');
        System.out.println("查找"+node.val);
    }
}
