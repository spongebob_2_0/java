package test;

import sun.reflect.generics.tree.Tree;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-02-04
 * Time: 19:37
 */
public class TestBinaryTree {
    static class TreeNode{
        public char val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(char val){
            this.val =val;
        }
    }
//    public TreeNode root;  //根节点
    public TreeNode createTree() {
        TreeNode A = new TreeNode('A');
        TreeNode B = new TreeNode('B');
        TreeNode C = new TreeNode('C');
        TreeNode D = new TreeNode('D');
        TreeNode E = new TreeNode('E');
        TreeNode F = new TreeNode('F');
        TreeNode G = new TreeNode('G');
        TreeNode H = new TreeNode('H');
        A.left = B;
        A.right =C;
        B.left =D;
        B.right =E;
        C.left =F;
        C.right =G;
        E.left =H;
//        this.root =A;
//        return root;
        return A;
    }
    public void preOrder(TreeNode root){
        if(root == null){
            return ;
        }
        System.out.print(root.val+" ");
        preOrder(root.left);
        preOrder(root.right);
    }
    public void inOrder(TreeNode root ){
        if(root == null){
            return;
        }
        inOrder(root.left);
        System.out.print(root.val+" ");
        inOrder(root.right);
    }
    public void postOrder(TreeNode root){
        if(root == null){
            return;
        }
        postOrder(root.left);
        postOrder(root.right);
        System.out.print(root.val+" ");
    }

    //获取树中节点的个数
    public int size(TreeNode root){
        if(root == null){
            return 0;
        }
        int leftSize =size(root.left);
        int rightSize =size(root.right);
        return leftSize+rightSize+1;
    }
    public static int usedSize;
    public void size2(TreeNode root){
        if(root == null){
            return;
        }
        usedSize++;
        size2(root.left);
        size2(root.right);
    }
    //获取树中叶子节点的个数
    public int getLeafNodeCount(TreeNode root){
        if(root == null){
            return 0;
        }
        if(root.left == null && root.right == null){
            return 1;
        }
        int leftSize = getLeafNodeCount(root.left);
        int rightSize =getLeafNodeCount(root.right);
        return leftSize+rightSize;
    }
    public static int count;
    public void getLeafNodeCount2(TreeNode root){
        if(root == null){
            return;
        }
        if(root.left == null && root.right == null){
            count++;
        }
        getLeafNodeCount2(root.left);
        getLeafNodeCount2(root.right);
    }
    //获取第k层节点的个数
    public int getkleveNodeCount(TreeNode root,int k){
        if(root == null){
            return 0;
        }
        if(k == 1){
            return 1;
        }
        int leftSize =getkleveNodeCount(root.left,k-1);
        int rightSize =getkleveNodeCount(root.right,k-1);
        return leftSize+rightSize;
    }
    public int getHeight(TreeNode root){
        if(root == null){
            return 0;
        }
        return getHeight(root.left) > getHeight((root.right)) ? (getHeight(root.left)+1):(getHeight(root.right)+1);
    }
    public int maxDepth(TreeNode root){
        if(root == null){
            return 0;
        }
        int leftHeight = maxDepth(root.left);
        int rightHeight = maxDepth(root.right);
        return leftHeight>rightHeight?(leftHeight+1):(rightHeight+1);
    }
    //查找value的值是否存在
    public TreeNode find(TreeNode root,char val){
        if(root == null){
            return null;
        }
        if(root.val == val){
            return root;
        }
        TreeNode leftTree =find(root.left,val);
        if(leftTree != null){
            return leftTree;
        }
        TreeNode rightTree =find(root.right,val);
        if(rightTree !=  null){
            return rightTree;
        }
        return null;
    }
}
