import java.util.Stack;

class MyQueue {
    public Stack<Integer> stack1;
    public Stack<Integer> stack2;
    public MyQueue() {
        stack1 =new Stack<>();
        stack2 =new Stack<>();
    }
    
    public void push(int x) {
        stack1.push(x);   //入队
    }
    
    public int pop() {
        if(empty()){
            return -1;
        }
        if(stack2.empty()){
            while(!stack1.empty()){
                int val =stack1.pop();
                stack2.push(val);
            }
        }
        return stack2.pop();
    }
    
    public int peek() {
        if(empty()){
            return -1;
        }
        if(stack2.empty()){
            while(!stack1.empty()){
                int val =stack1.pop();
                stack2.push(val);
            }
        }
        return stack2.peek();

    }
    
    public boolean empty() {
        return stack1.empty() && stack2.empty();
    }
}

