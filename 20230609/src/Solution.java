import java.util.*;
public class Solution {
    public static void merge(int A[], int m, int B[], int n) {
        int k = m+n-1;
        int left = m-1;
        int right = n-1;
        while(left >= 0 && right >= 0) {
            if(A[left] <= B[right]) {
                A[k--] = B[right--];
            }else {
                A[k--] = A[left--];
            }
        }
        while(right >= 0) {
            A[k--] = B[right--];
        }
    }

    public static void main(String[] args) {
        int[] A ={4,5,6,0,0,0};
        int[] B ={1,2,3};
        merge(A,3,B,3);
        for(int x: A) {
            System.out.print(x+" ");
        }
    }
}