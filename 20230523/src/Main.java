import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-05-23
 * Time: 17:08
 */
public class Main {
    public static void main(String[] args) {

        System.out.println("==============");
        System.out.println(Math.pow(2,3));
    }
    public int[] sortedSquares(int[] nums) {
        for(int i = 0; i < nums.length; i++) {
            nums[i] = (int) Math.pow(nums[i],2);
        }
        Arrays.sort(nums);
        return nums;
    }
}
