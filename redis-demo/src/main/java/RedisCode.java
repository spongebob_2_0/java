import redis.clients.jedis.Jedis;

import java.util.Random;

public class RedisCode {

    private static Jedis jedis = new Jedis("106.14.91.138",6379);

    private final static String phone = "13377492843";
    
    public static void main(String[] args) {
        jedis.auth("183193");
        String code = getCode(phone);
        verifyCode(phone,code);
    }
    
    // 1. 输入手机号，随机生成 6 位数字验证码，2 分钟有效，存入redis
    public static String getCode(String phone){
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            int code = random.nextInt(10);
            sb.append(code);
        }
        jedis.setex("phone:" + phone + ":code", 120, sb.toString());
        jedis.setex("phone:" + phone + ":count", 120, "0");
        return sb.toString();
        
    }
    
    // 2. 输入验证码，去 Redis 获取进行比对，返回结果
    public static void verifyCode(String phone,String code){
        // 获取 code
        String phoneCode = jedis.get("phone:" + phone + ":code");
        String count = jedis.get("phone:" + phone + ":count");
        // 比较验证次数
        if(Integer.parseInt(count) > 2){
            System.out.println("验证次数超过三次");
            return;
        }
        // 比较验证码
        if(!phoneCode.equals(code)){
            System.out.println("验证失败");
            jedis.incr("phone:" + phone + ":count");
        }else {
            System.out.println("验证成功");
            // 可以立即让验证码和次数过期，即删除
            jedis.unlink("phone:" + phone + ":code","phone:" + phone + ":count");
        }
        jedis.close();
    }       
}
