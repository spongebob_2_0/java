package com.example.consumer;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-08-25
 * Time: 2:16
 */
public class Consumer_PubSub1 {
    public static void main(String[] args) throws IOException, TimeoutException {
        //1.创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        //2.设置参数
        factory.setHost("106.14.91.138"); //ip默认localhost
        factory.setPort(5672);  //端口 默认5672
        factory.setVirtualHost("/itbit"); //虚拟机 默认值
        factory.setUsername("wuyimin");
        factory.setPassword("183193");
        //3.创建连接Connection
        Connection connection = factory.newConnection();
        //4.创建channel
        Channel channel = connection.createChannel();

        String que1Name = "test_fanout_queue1";
        String que2Name = "test_fanout_queue2";

        //6.接收消息
        Consumer consumer = new DefaultConsumer(channel) {
            //回调方法,当收到消息后,会自动执行
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
//                System.out.println("consumerTag: " + consumerTag);
//                System.out.println("Exchange: " + envelope.getExchange());
//                System.out.println("RoutingKey: " + envelope.getRoutingKey());
//                System.out.println("properties: " + properties);
                System.out.println("收到的消息: body =  " + new String(body));
                System.out.println("将日志信息打印到控制台...");
            }
        };
        channel.basicConsume(que1Name,true,consumer);

    }
}
