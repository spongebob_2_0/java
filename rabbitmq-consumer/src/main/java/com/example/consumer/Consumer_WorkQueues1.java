package com.example.consumer;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-08-25
 * Time: 2:16
 */
public class Consumer_WorkQueues1 {
    public static void main(String[] args) throws IOException, TimeoutException {
        //1.创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        //2.设置参数
        factory.setHost("106.14.91.138"); //ip默认localhost
        factory.setPort(5672);  //端口 默认5672
        factory.setVirtualHost("/itbit"); //虚拟机 默认值
        factory.setUsername("wuyimin");
        factory.setPassword("183193");
        //3.创建连接Connection
        Connection connection = factory.newConnection();
        //4.创建channel
        Channel channel = connection.createChannel();
        //5.创建队列
        //如果没有一个名字叫hello_world的队列,则会创建队列,否则不会创建
        channel.queueDeclare("work_queues",true,false,false,null);
        //6.接收消息
        Consumer consumer = new DefaultConsumer(channel) {
            //回调方法,当收到消息后,会自动执行
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
//                System.out.println("consumerTag: " + consumerTag);
//                System.out.println("Exchange: " + envelope.getExchange());
//                System.out.println("RoutingKey: " + envelope.getRoutingKey());
//                System.out.println("properties: " + properties);
                System.out.println("收到的消息: body =  " + new String(body));
            }
        };
        channel.basicConsume("work_queues",true,consumer);

    }
}
