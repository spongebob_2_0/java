package test;

public class ThreadDemo6 {
    public static void main(String[] args) {
        Thread t =new Thread(() -> {
            while (true) {
                System.out.println("我是后台线程");
            }
        });
        t.setDaemon(true);
        t.start();
        System.out.println("我是前台线程");
    }
}
