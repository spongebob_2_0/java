package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-16
 * Time: 23:55
 */
public class ThreadDemo5 {
    public static void main(String[] args) {
        Thread t =new Thread(()->{
            while (true) {
                System.out.println("hello-t");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"我的线程");
        t.start();
        while (true) {
            System.out.println("hello-main");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
