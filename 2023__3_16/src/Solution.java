import java.util.*;
public class Solution {
    public int InversePairs(int [] array) {
        int len = array.length;
        long count = 0;
        for (int i = 0; i < len; i++) {
            for (int j = i + 1; j < len; j++) {
                if (array[i] > array[j]) {
                    count++;
                }
            }
        }
        return Math.abs((int)(count % 1000000007));
    }
}
