package com.example.graduationproject.mapper;

import com.example.graduationproject.entity.Notice;
import com.example.graduationproject.entity.Params;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author WuYimin
* @description 针对表【notice(系统公告表)】的数据库操作Mapper
* @createDate 2023-10-28 21:23:50
* @Entity com.example.graduationproject.entity.Notice
*/
public interface NoticeMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(Notice record);

    int insertSelective(Notice record);

    Notice selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Notice record);

    int updateByPrimaryKey(Notice record);

    List<Notice> search(@Param("params") Params params);

    List<Notice> findTop();
}
