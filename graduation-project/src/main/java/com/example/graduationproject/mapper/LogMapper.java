package com.example.graduationproject.mapper;

import com.example.graduationproject.entity.Log;
import com.example.graduationproject.entity.Params;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author WuYimin
* @description 针对表【log(操作日志表)】的数据库操作Mapper
* @createDate 2023-10-28 00:34:51
* @Entity com.example.graduationproject.entity.Log
*/
public interface LogMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(Log record);

    int insertSelective(Log record);

    Log selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Log record);

    int updateByPrimaryKey(Log record);

    List<Log> search(@Param("params") Params params);
}
