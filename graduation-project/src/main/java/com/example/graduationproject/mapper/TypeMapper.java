package com.example.graduationproject.mapper;

import com.example.graduationproject.entity.Params;
import com.example.graduationproject.entity.Type;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author WuYimin
* @description 针对表【type(图书分类表)】的数据库操作Mapper
* @createDate 2023-10-25 10:23:22
* @Entity com.example.graduationproject.entity.Type
*/
public interface TypeMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(Type type);

    int insertSelective(Type record);

    Type selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Type record);

    int updateByPrimaryKey(Type record);

    Type searchByName(String name);

    Integer updateType(Type type);

    List<Type> search(@Param("params") Params params);

    List<Type> findAll();
}
