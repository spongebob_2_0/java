package com.example.graduationproject.mapper;

import com.example.graduationproject.entity.Audit;
import com.example.graduationproject.entity.Params;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author WuYimin
* @description 针对表【audit(请假审核表)】的数据库操作Mapper
* @createDate 2023-10-27 17:15:20
* @Entity com.example.graduationproject.entity.Audit
*/
public interface AuditMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(Audit record);

    int insertSelective(Audit record);

    Audit selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Audit record);

    int updateByPrimaryKey(Audit record);

    List<Audit> search(@Param("params") Params params);
}
