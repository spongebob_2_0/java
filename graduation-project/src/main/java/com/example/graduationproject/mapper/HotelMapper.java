package com.example.graduationproject.mapper;

import com.example.graduationproject.entity.Book;
import com.example.graduationproject.entity.Hotel;
import com.example.graduationproject.entity.Params;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author WuYimin
* @description 针对表【hotel(酒店表)】的数据库操作Mapper
* @createDate 2023-10-27 19:54:00
* @Entity com.example.graduationproject.entity.Hotel
*/
public interface HotelMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(Hotel record);

    int insertSelective(Hotel record);

    Hotel selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Hotel record);

    int updateByPrimaryKey(Hotel record);

    List<Hotel> search(@Param("params") Params params);

    Hotel searchByName(String name);
}
