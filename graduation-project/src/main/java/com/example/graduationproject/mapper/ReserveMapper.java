package com.example.graduationproject.mapper;

import com.example.graduationproject.entity.Audit;
import com.example.graduationproject.entity.Params;
import com.example.graduationproject.entity.Reserve;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author WuYimin
* @description 针对表【reserve(酒店表)】的数据库操作Mapper
* @createDate 2023-10-27 20:43:59
* @Entity com.example.graduationproject.entity.Reserve
*/
public interface ReserveMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(Reserve record);

    int insertSelective(Reserve record);

    Reserve selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Reserve record);

    int updateByPrimaryKey(Reserve record);

    List<Reserve> search(@Param("params") Params params);
}
