package com.example.graduationproject.mapper;

import com.example.graduationproject.entity.Book;
import com.example.graduationproject.entity.Params;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author WuYimin
* @description 针对表【book】的数据库操作Mapper
* @createDate 2023-10-24 17:15:18
* @Entity com.example.graduationproject.entity.Book
*/
public interface BookMapper {

    int deleteByPrimaryKey(Long id);

    int insert(Book record);

    int insertSelective(Book record);

    Book selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Book record);

    int updateByPrimaryKey(Book record);

    List<Book> search(@Param("params") Params params);

    Book searchByName(String name);

    Integer updateUser(Book book);

    Integer add(Book book);

    Integer delete(Integer id);

    List<Book> findAll();
}
