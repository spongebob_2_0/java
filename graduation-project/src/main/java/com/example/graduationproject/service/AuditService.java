package com.example.graduationproject.service;

import cn.hutool.core.util.ObjectUtil;
import com.example.graduationproject.common.CustomException;
import com.example.graduationproject.entity.Admin;
import com.example.graduationproject.entity.Audit;
import com.example.graduationproject.entity.Params;
import com.example.graduationproject.mapper.AdminMapper;
import com.example.graduationproject.mapper.AuditMapper;
import com.example.graduationproject.utils.JwtTokenUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-24
 * Time: 17:18
 */
@Service
public class AuditService {

    @Resource
    private AuditMapper auditMapper;

    @Resource
    private AdminMapper adminMapper;


    public PageInfo<Audit> search(Params params) {

        Admin currentUser = JwtTokenUtils.getCurrentUser();
        if(ObjectUtil.isNull(currentUser)) {
            throw new CustomException("从token中未解析到用户信息,请重新登录!");
        }
        if("ROLE_STUDENT".equals(currentUser.getRole())) {
            params.setUserId(currentUser.getId());
        }
        //开启分页查询
        PageHelper.startPage(params.getPageNum(),params.getPageSize());
        // 接下来的查询会自动按照当前开启的分页来查询
        List<Audit> list = auditMapper.search(params);
        for(Audit audit: list) {
            if(ObjectUtil.isNotEmpty(audit.getUserid())) {
                Admin user = adminMapper.selectByPrimaryKey(Long.valueOf(audit.getUserid()));
                if(ObjectUtil.isNotEmpty(user)) {
                    audit.setUserName(user.getName());
                }
            }
        }
        return PageInfo.of(list);
    }

    public Audit searchByName(Integer userId) {
        return auditMapper.selectByPrimaryKey(userId);
    }

    public Integer add(Audit audit) {
        return auditMapper.insert(audit);
    }

    public Integer updateUser(Audit audit) {
        return auditMapper.updateByPrimaryKeySelective(audit);
    }

    public Integer delete(Integer id) {
        return auditMapper.deleteByPrimaryKey(id);
    }
}
