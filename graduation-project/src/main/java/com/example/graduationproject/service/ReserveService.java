package com.example.graduationproject.service;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.example.graduationproject.common.CustomException;
import com.example.graduationproject.entity.*;
import com.example.graduationproject.mapper.AdminMapper;
import com.example.graduationproject.mapper.AuditMapper;
import com.example.graduationproject.mapper.HotelMapper;
import com.example.graduationproject.mapper.ReserveMapper;
import com.example.graduationproject.utils.JwtTokenUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-24
 * Time: 17:18
 */
@Service
public class ReserveService {

    @Resource
    private ReserveMapper reserveMapper;

    @Resource
    private AdminMapper adminMapper;

    @Resource
    private HotelMapper hotelMapper;


    public PageInfo<Reserve> search(Params params) {
        //开启分页查询
        PageHelper.startPage(params.getPageNum(),params.getPageSize());
        // 接下来的查询会自动按照当前开启的分页来查询
        List<Reserve> list = reserveMapper.search(params);

        if(CollectionUtil.isEmpty(list)) {
            return PageInfo.of(new ArrayList<>());
        }

        for(Reserve reserve: list) {
            if(ObjectUtil.isNotEmpty(reserve.getHotelid())) {
                Hotel hotel = hotelMapper.selectByPrimaryKey(Long.valueOf(reserve.getHotelid()));
                if(ObjectUtil.isNotEmpty(hotel)) {
                    reserve.setHotelName(hotel.getName());
                }
            }
            if(ObjectUtil.isNotEmpty(reserve.getUserid())) {
                Admin admin = adminMapper.selectByPrimaryKey(Long.valueOf(reserve.getUserid()));
                if(ObjectUtil.isNotEmpty(admin)) {
                    reserve.setUserName(admin.getName());
                }
            }
        }
        return PageInfo.of(list);
    }

    public Reserve searchByName(Integer userId) {
        return reserveMapper.selectByPrimaryKey(userId);
    }

    public Integer add(Reserve reserve) {
        return reserveMapper.insert(reserve);
    }

    public Integer updateUser(Reserve reserve) {
        return reserveMapper.updateByPrimaryKeySelective(reserve);
    }

    public Integer delete(Integer id) {
        return reserveMapper.deleteByPrimaryKey(id);
    }
}
