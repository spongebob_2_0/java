package com.example.graduationproject.service;

import com.example.graduationproject.common.Result;
import com.example.graduationproject.entity.Admin;
import com.example.graduationproject.entity.Params;
import com.example.graduationproject.mapper.AdminMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-23
 * Time: 17:00
 */
@Service
public class AdminService {

    @Resource
    private AdminMapper adminMapper;


    public Admin selectByPrimaryKey(Long id) {
        return adminMapper.selectByPrimaryKey(id);
    }

    public List<Admin> getAllUsers() {
        return adminMapper.getAllUsers();
    }

    public PageInfo<Admin> search(Params params) {
        //开启分页查询
        PageHelper.startPage(params.getPageNum(),params.getPageSize());
        // 接下来的查询会自动按照当前开启的分页来查询
        List<Admin> Info = adminMapper.search(params);
        return PageInfo.of(Info);
    }

    public Integer add(Admin admin) {
        //初始化一个密码
        if(admin.getPassword() == null) {
            admin.setPassword("123");
        }
        return adminMapper.add(admin);
    }

    public Integer updateUser(Admin admin) {
        return adminMapper.updateUser(admin);
    }

    public Integer delete(Integer id) {
        return adminMapper.delete(id);
    }

    public Admin searchByName(String name) {
        return adminMapper.searchByName(name);
    }
}
