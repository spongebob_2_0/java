package com.example.graduationproject.service;


import com.example.graduationproject.entity.Log;
import com.example.graduationproject.entity.Notice;
import com.example.graduationproject.entity.Params;
import com.example.graduationproject.mapper.LogMapper;
import com.example.graduationproject.mapper.NoticeMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-25
 * Time: 11:38
 */
@Service
public class NoticeService {

    @Resource
    private NoticeMapper noticeMapper;


    public Integer add(Notice notice) {
        return noticeMapper.insert(notice);
    }


    public Integer updateType(Notice notice) {
        return noticeMapper.updateByPrimaryKeySelective(notice);
    }

    public PageInfo<Notice> search(Params params) {
        //开启分页查询
        PageHelper.startPage(params.getPageNum(),params.getPageSize());
        // 接下来的查询会自动按照当前开启的分页来查询
        List<Notice> Info = noticeMapper.search(params);
        return PageInfo.of(Info);
    }

    public Integer delete(Integer id) {
        return noticeMapper.deleteByPrimaryKey(id);
    }


    public List<Notice> findTop() {
        return noticeMapper.findTop();
    }
}
