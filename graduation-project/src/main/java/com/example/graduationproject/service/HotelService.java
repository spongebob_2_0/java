package com.example.graduationproject.service;

import com.example.graduationproject.entity.Book;
import com.example.graduationproject.entity.Hotel;
import com.example.graduationproject.entity.Params;
import com.example.graduationproject.mapper.BookMapper;
import com.example.graduationproject.mapper.HotelMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-24
 * Time: 17:18
 */
@Service
public class HotelService {

    @Resource
    private HotelMapper hotelMapper;


    public PageInfo<Hotel> search(Params params) {
        //开启分页查询
        PageHelper.startPage(params.getPageNum(),params.getPageSize());
        // 接下来的查询会自动按照当前开启的分页来查询
        List<Hotel> Info = hotelMapper.search(params);
        return PageInfo.of(Info);
    }

    public Hotel searchByName(String name) {
        return hotelMapper.searchByName(name);
    }

    public Integer add(Hotel hotel) {
        return hotelMapper.insert(hotel);
    }

    public Integer updateUser(Hotel hotel) {
        return hotelMapper.updateByPrimaryKeySelective(hotel);
    }

    public Integer delete(Integer id) {
        return hotelMapper.deleteByPrimaryKey(id);
    }
}
