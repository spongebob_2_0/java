package com.example.graduationproject.service;

import com.example.graduationproject.common.AutoLog;
import com.example.graduationproject.entity.Book;
import com.example.graduationproject.entity.Params;
import com.example.graduationproject.mapper.BookMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-24
 * Time: 17:18
 */
@Service
public class BookService {

    @Resource
    private BookMapper bookMapper;


    public PageInfo<Book> search(Params params) {
        //开启分页查询
        PageHelper.startPage(params.getPageNum(),params.getPageSize());
        // 接下来的查询会自动按照当前开启的分页来查询
        List<Book> Info = bookMapper.search(params);
        return PageInfo.of(Info);
    }

    public Book searchByName(String name) {
        return bookMapper.searchByName(name);
    }

    public Integer add(Book book) {
        return bookMapper.add(book);
    }

    public Integer updateUser(Book book) {
        return bookMapper.updateUser(book);
    }

    public Integer delete(Integer id) {
        return bookMapper.delete(id);
    }

    public List<Book> findAll() {
        return bookMapper.findAll();
    }
}
