package com.example.graduationproject.service;


import com.example.graduationproject.entity.Params;
import com.example.graduationproject.entity.Type;
import com.example.graduationproject.mapper.TypeMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-25
 * Time: 11:38
 */
@Service
public class TypeService {

    @Resource
    private TypeMapper typeMapper;

    public Type searchByName(String name) {
        return typeMapper.searchByName(name);
    }

    public Integer add(Type type) {
        return typeMapper.insert(type);
    }


    public Integer updateType(Type type) {
        return typeMapper.updateType(type);
    }

    public PageInfo<Type> search(Params params) {
        //开启分页查询
        PageHelper.startPage(params.getPageNum(),params.getPageSize());
        // 接下来的查询会自动按照当前开启的分页来查询
        List<Type> Info = typeMapper.search(params);
        return PageInfo.of(Info);
    }

    public Integer delete(Integer id) {
        return typeMapper.deleteByPrimaryKey(id);
    }


    public List<Type> findAll() {
        return typeMapper.findAll();
    }
}
