package com.example.graduationproject.service;


import com.example.graduationproject.entity.Log;
import com.example.graduationproject.entity.Params;
import com.example.graduationproject.entity.Type;
import com.example.graduationproject.mapper.LogMapper;
import com.example.graduationproject.mapper.TypeMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-25
 * Time: 11:38
 */
@Service
public class LogService {

    @Resource
    private LogMapper logMapper;


    public Integer add(Log log) {
        return logMapper.insert(log);
    }


    public Integer updateType(Log log) {
        return logMapper.updateByPrimaryKeySelective(log);
    }

    public PageInfo<Log> search(Params params) {
        //开启分页查询
        PageHelper.startPage(params.getPageNum(),params.getPageSize());
        // 接下来的查询会自动按照当前开启的分页来查询
        List<Log> Info = logMapper.search(params);
        return PageInfo.of(Info);
    }

    public Integer delete(Integer id) {
        return logMapper.deleteByPrimaryKey(id);
    }


}
