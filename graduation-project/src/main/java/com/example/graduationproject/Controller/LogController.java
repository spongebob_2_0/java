package com.example.graduationproject.Controller;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.example.graduationproject.common.AutoLog;
import com.example.graduationproject.common.CustomException;
import com.example.graduationproject.common.Result;
import com.example.graduationproject.entity.Log;
import com.example.graduationproject.entity.Params;
import com.example.graduationproject.entity.Type;
import com.example.graduationproject.service.LogService;
import com.example.graduationproject.service.TypeService;
import com.example.graduationproject.utils.AliOSSUtils;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-25
 * Time: 11:36
 */
@RestController
@RequestMapping("/log")
public class LogController {

    @Resource
    private LogService logService;

    private static final Logger log = LoggerFactory.getLogger(LogController.class);



    @RequestMapping("/search")
    public Result search(Params params) {
        PageInfo<Log> result = logService.search(params);
        return Result.success(result);
    }

    @RequestMapping("/add0edit")
    public Result add(@RequestBody Log log) {
        if(log.getName() == null || "".equals(log.getName())) {
            throw new CustomException("图书名称不能为空!");
        }
        if(log.getId() == null) {
            logService.add(log);
        }else {
            logService.updateType(log);
        }
        return Result.success();
    }

    @RequestMapping("/delete/{id}")
    public Result delete(@PathVariable Integer id) {
        Integer result = logService.delete(id);
        if(result == 1) {
            return Result.success();
        }else {
            return Result.error("删除失败");
        }
    }

//    @RequestMapping("/delBatch")
//    public Result delBatch(@RequestBody List<Type> list) {
//        for(Type type: list) {
//            typeService.delete(type.getId());
//        }
//        return Result.success();
//    }



}
