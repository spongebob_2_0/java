package com.example.graduationproject.Controller;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.ObjectUtil;
import com.example.graduationproject.common.AutoLog;
import com.example.graduationproject.common.CustomException;
import com.example.graduationproject.common.Result;
import com.example.graduationproject.entity.Book;
import com.example.graduationproject.entity.Params;
import com.example.graduationproject.service.BookService;
import com.example.graduationproject.utils.AliOSSUtils;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-23
 * Time: 16:15
 */
@RestController
@RequestMapping("/book")
@CrossOrigin
public class BookController {

    @Resource
    private BookService bookService;

    @Resource
    private AliOSSUtils aliOSSUtils;


    @RequestMapping("/search")
    public Result search(Params params) {
        PageInfo<Book> result = bookService.search(params);
        return Result.success(result);
    }

    @RequestMapping("/add0edit")
    @AutoLog("修改图书信息")
    public Result add(@RequestBody Book book) {
        if(book.getName() == null || "".equals(book.getName())) {
            throw new CustomException("图书名称不能为空!");
        }
        if(book.getId() == null) {
            Book user = bookService.searchByName(book.getName());
            if(user != null) {
                throw new CustomException("该图书已存在");
            }
            bookService.add(book);
        }else {
            bookService.updateUser(book);
        }
        return Result.success();
    }

    @RequestMapping("/delete/{id}")
    @AutoLog("删除图书信息")
    public Result delete(@PathVariable Integer id) {
        Integer result = bookService.delete(id);
        if(result == 1) {
            return Result.success();
        }else {
            return Result.error("删除失败");
        }
    }

    @RequestMapping("/upload")
    public Result upload(MultipartFile file) throws IOException {
        if(file == null) {
            throw new CustomException("文件不能为空!");
        }
        //使用阿里云oss将文件上传到阿里云存储桶(同时返回ur地址)
        String url = aliOSSUtils.upload(file);
        return Result.success(url);
    }

    @RequestMapping("/echarts/bie")
    public Result bie() {
        // 先查出所有图书
        List<Book> list = bookService.findAll();
        Map<String,Long> collect = list.stream().filter(x -> ObjectUtil.isNotEmpty(x.getTypeName()))
                        .collect(Collectors.groupingBy(Book::getTypeName,Collectors.counting()));
        // 最后返回给前端的数据结构
        List<Map<String ,Object>> mapList = new ArrayList<>();
        if(CollectionUtil.isNotEmpty(collect)) {
            for(String key : collect.keySet()) {
                Map<String,Object> map = new HashMap<>();
                map.put("name",key);
                map.put("value",collect.get(key));
                mapList.add(map);
            }
        }
        return Result.success(mapList);
    }

    @RequestMapping("/echarts/bar")
    public Result bar() {
        // 先查出所有图书
        List<Book> list = bookService.findAll();
        Map<String,Long> collect = list.stream().filter(x -> ObjectUtil.isNotEmpty(x.getTypeName()))
                .collect(Collectors.groupingBy(Book::getTypeName,Collectors.counting()));
        // 最后返回给前端的数据结构
        List<String> xAxis = new ArrayList<>();
        List<Long> yAxis = new ArrayList<>();

        if(CollectionUtil.isNotEmpty(collect)) {
            for(String key : collect.keySet()) {
                xAxis.add(key);
                yAxis.add(collect.get(key));
            }
        }

        Map<String, Object> map = new HashMap<>();
        map.put("xAxis",xAxis);
        map.put("yAxis",yAxis);
        return Result.success(map);
    }

    @RequestMapping("/wang/upload")
    public Map<String,Object> wangUpload(MultipartFile file) throws IOException {
        if(file == null) {
            throw new CustomException("文件不能为空!");
        }
        //使用阿里云oss将文件上传到阿里云存储桶(同时返回ur地址)
        String url = aliOSSUtils.upload(file);
        // wangEditor上传图片成功后需要返回的参数,要按如下格式
        Map<String, Object> map = new HashMap<>();
        map.put("errno",0);
        map.put("data",CollectionUtil.newArrayList(Dict.create().set("url",url)));
        return map;
    }
}
