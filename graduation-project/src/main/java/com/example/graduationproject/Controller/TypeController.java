package com.example.graduationproject.Controller;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.example.graduationproject.common.CustomException;
import com.example.graduationproject.common.Result;
import com.example.graduationproject.entity.Params;
import com.example.graduationproject.entity.Type;
import com.example.graduationproject.service.TypeService;
import com.example.graduationproject.utils.AliOSSUtils;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-25
 * Time: 11:36
 */
@RestController
@RequestMapping("/type")
public class TypeController {

    @Resource
    private TypeService typeService;

    @Resource
    private AliOSSUtils aliOSSUtils;

    @RequestMapping("/findAll")
    public Result findAll() {
        List<Type> all = typeService.findAll();
        return Result.success(all);
    }


    @RequestMapping("/search")
    public Result search(Params params) {
        PageInfo<Type> result = typeService.search(params);
        return Result.success(result);
    }

    @RequestMapping("/add0edit")
    public Result add(@RequestBody Type type) {
        if(type.getName() == null || "".equals(type.getName())) {
            throw new CustomException("图书名称不能为空!");
        }
        if(type.getId() == null) {
            Type type1 = typeService.searchByName(type.getName());
            if(type1 != null) {
                throw new CustomException("该图书分类已存在");
            }
            typeService.add(type);
        }else {
            typeService.updateType(type);
        }
        return Result.success();
    }

    @RequestMapping("/delete/{id}")
    public Result delete(@PathVariable Integer id) {
        Integer result = typeService.delete(id);
        if(result == 1) {
            return Result.success();
        }else {
            return Result.error("删除失败");
        }
    }

    @RequestMapping("/delBatch")
    public Result delBatch(@RequestBody List<Type> list) {
        for(Type type: list) {
            typeService.delete(type.getId());
        }
        return Result.success();
    }


    /**
     * 导出数据到excel表格
     * @param response
     * @return
     * @throws IOException
     */
    @RequestMapping("/export")
    public Result export(HttpServletResponse response) throws IOException {
        // 从数据库中获取所有的Type对象
        List<Type> all = typeService.findAll();

        // 如果没有找到数据，抛出自定义异常
        if (CollectionUtil.isEmpty(all)) {
            throw new CustomException("未找到数据!");
        }

        // 创建一个用于存储导出数据的List，每个元素是一个Map，用于表示一行数据
        List<Map<String, Object>> list = new ArrayList<>(all.size());

        // 遍历所有的Type对象，将每一行数据映射为一个Map，并添加到list中
        for (Type type : all) {
            Map<String, Object> row = new HashMap<>();

            // 将“分类名称”与Type对象的名称属性相对应，放入Map中
            row.put("分类名称", type.getName());

            // 将“分类介绍”与Type对象的描述属性相对应，放入Map中
            row.put("分类介绍", type.getDescripition());

            // 将当前行的Map添加到导出数据的List中
            list.add(row);
        }

        // 创建ExcelWriter对象，准备写入Excel文件
        ExcelWriter wr = ExcelUtil.getWriter(true);

        // 将数据写入ExcelWriter对象
        wr.write(list, true);

        // 设置HTTP响应的内容类型为Excel文件
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8");

        // 设置HTTP响应头，指定文件名为type.xlsx，并且作为附件下载
        response.setHeader("Content-Disposition", "attachment;filename=type.xlsx");

        // 获取HTTP响应的输出流
        ServletOutputStream out = response.getOutputStream();

        // 将Excel数据写入响应输出流
        wr.flush(out, true);

        // 关闭ExcelWriter对象
        wr.close();

        // 关闭系统输入流
        IoUtil.close(System.out);

        // 返回成功的结果
        return Result.success();
    }

    //批量导入文件
    @RequestMapping("/upload")
    public Result upload(MultipartFile file) throws IOException {
        List<Type> infoList = ExcelUtil.getReader(file.getInputStream()).readAll(Type.class);
        if(!CollectionUtil.isEmpty(infoList)) {
            for(Type type: infoList) {
                try {
                    typeService.add(type);
                }catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return Result.success();
    }

}
