package com.example.graduationproject.Controller;

import cn.hutool.core.date.DateUtil;
import com.example.graduationproject.common.CustomException;
import com.example.graduationproject.common.Result;
import com.example.graduationproject.entity.Admin;
import com.example.graduationproject.entity.Hotel;
import com.example.graduationproject.entity.Params;
import com.example.graduationproject.entity.Reserve;
import com.example.graduationproject.mapper.HotelMapper;
import com.example.graduationproject.service.AdminService;
import com.example.graduationproject.service.ReserveService;
import com.example.graduationproject.utils.JwtTokenUtils;
import com.github.pagehelper.PageInfo;
import org.springframework.util.unit.DataUnit;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.xml.crypto.Data;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-23
 * Time: 16:15
 */
@RestController
@RequestMapping("/reserve")
@CrossOrigin
public class ReserveController {

    @Resource
    private ReserveService reserveService;

    @Resource
    private HotelMapper hotelMapper;



    @RequestMapping("/search")
    public Result search(Params params) {
        PageInfo<Reserve> result = reserveService.search(params);
        return Result.success(result);
    }

    @RequestMapping("/add0edit")
    public Result add(@RequestBody Reserve reserve) {
        // 1.酒店房间是否大于0,只有大于0的时候才可以被预定
        Hotel hotel = hotelMapper.selectByPrimaryKey(Long.valueOf(reserve.getHotelid()));
        if(hotel.getNum() == 0) {
            return Result.error("暂时没有空闲房间可以预定!");
        }

        // 2. 往预定表里面插入一条预定记录
        reserve.setTime(DateUtil.now());
        reserveService.add(reserve);

        // 3.对应的酒店房间剩余数量-1
        hotel.setNum(hotel.getNum()-1);
        hotelMapper.updateByPrimaryKeySelective(hotel);

        return Result.success();
    }

    @RequestMapping("/delete")
    public Result delete(@RequestBody Reserve reserve) {
        Integer result = reserveService.delete(reserve.getId());
        if(result == 1) {
            Hotel hotel = hotelMapper.selectByPrimaryKey(Long.valueOf(reserve.getHotelid()));
            // 取消预定后,对应的酒店房间剩余数量+1
            hotel.setNum(hotel.getNum()+1);
            hotelMapper.updateByPrimaryKeySelective(hotel);
            return Result.success();
        }else {
            return Result.error("删除失败");
        }
    }

}
