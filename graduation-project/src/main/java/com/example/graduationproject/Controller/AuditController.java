package com.example.graduationproject.Controller;

import com.example.graduationproject.common.CustomException;
import com.example.graduationproject.common.Result;
import com.example.graduationproject.entity.Audit;
import com.example.graduationproject.entity.Params;
import com.example.graduationproject.service.AuditService;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-23
 * Time: 16:15
 */
@RestController
@RequestMapping("/audit")
@CrossOrigin
public class AuditController {

    @Resource
    private AuditService auditService;


    @RequestMapping("/search")
    public Result search(Params params) {
        PageInfo<Audit> result = auditService.search(params);
        return Result.success(result);
    }

    @RequestMapping("/add0edit")
    public Result add(@RequestBody Audit audit) {
        if(audit.getName() == null || "".equals(audit.getName())) {
            throw new CustomException("请假理由不能为空!");
        }
        if(audit.getId() == null) {
            auditService.add(audit);
        }else {
            auditService.updateUser(audit);
        }
        return Result.success();
    }

    @RequestMapping("/delete/{id}")
    public Result delete(@PathVariable Integer id) {
        Integer result = auditService.delete(id);
        if(result == 1) {
            return Result.success();
        }else {
            return Result.error("删除失败");
        }
    }

}
