package com.example.graduationproject.Controller;

import com.example.graduationproject.common.CustomException;
import com.example.graduationproject.common.Result;
import com.example.graduationproject.entity.Book;
import com.example.graduationproject.entity.Hotel;
import com.example.graduationproject.entity.Params;
import com.example.graduationproject.service.BookService;
import com.example.graduationproject.service.HotelService;
import com.example.graduationproject.utils.AliOSSUtils;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-23
 * Time: 16:15
 */
@RestController
@RequestMapping("/hotel")
@CrossOrigin
public class HotelController {

    @Resource
    private HotelService hotelService;

    @Resource
    private AliOSSUtils aliOSSUtils;


    @RequestMapping("/search")
    public Result search(Params params) {
        PageInfo<Hotel> result = hotelService.search(params);
        return Result.success(result);
    }

    @RequestMapping("/add0edit")
    public Result add(@RequestBody Hotel hotel) {
        if(hotel.getName() == null || "".equals(hotel.getName())) {
            throw new CustomException("酒店名称不能为空!");
        }
        if(hotel.getId() == null) {
            Hotel hotel1 = hotelService.searchByName(hotel.getName());
            if(hotel1 != null) {
                throw new CustomException("该酒店已存在");
            }
            hotelService.add(hotel);
        }else {
            hotelService.updateUser(hotel);
        }
        return Result.success();
    }

    @RequestMapping("/delete/{id}")
    public Result delete(@PathVariable Integer id) {
        Integer result = hotelService.delete(id);
        if(result == 1) {
            return Result.success();
        }else {
            return Result.error("删除失败");
        }
    }

    @RequestMapping("/upload")
    public Result upload(MultipartFile file) throws IOException {
        if(file == null) {
            throw new CustomException("文件不能为空!");
        }
        //使用阿里云oss将文件上传到阿里云存储桶(同时返回ur地址)
        String url = aliOSSUtils.upload(file);
        return Result.success(url);
    }
}
