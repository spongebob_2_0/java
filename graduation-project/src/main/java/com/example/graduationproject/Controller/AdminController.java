package com.example.graduationproject.Controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.CircleCaptcha;
import com.example.graduationproject.common.AutoLog;
import com.example.graduationproject.common.CustomException;
import com.example.graduationproject.common.Result;
import com.example.graduationproject.entity.Admin;
import com.example.graduationproject.entity.Params;
import com.example.graduationproject.entity.VerifyCodeResp;
import com.example.graduationproject.service.AdminService;
import com.example.graduationproject.utils.JwtTokenUtils;
import com.github.pagehelper.PageInfo;
import com.google.code.kaptcha.Producer;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-23
 * Time: 16:15
 */
@RestController
@RequestMapping("/admin")
@CrossOrigin
public class AdminController {

    @Resource
    private AdminService adminService;

    @Resource
    private RedisTemplate redisTemplate;



    @RequestMapping("/login")
    @AutoLog("登录该系统")
    public Result login(@RequestBody Admin admin) {
        if(admin.getName() == null || "".equals(admin.getName())) {
            throw new CustomException("用户名不能为空!");
        }
        if(admin.getPassword() == null || "".equals(admin.getPassword())) {
            throw new CustomException("密码不能为空!");
        }
        Admin user = adminService.searchByName(admin.getName());
        if(user == null) {
            throw new CustomException("用户名或者密码错误!");
        }
        if(!user.getPassword().equals(admin.getPassword())) {
            throw new CustomException("用户名或者密码错误!");
        }
        // 从redis中获取验证码
        String redisCode = (String) redisTemplate.opsForValue().get("captcha:" + admin.getCaptchaKey());
        if(!admin.getVercode().equalsIgnoreCase(redisCode)) {
            throw new CustomException("验证码错误,请重新输入");
        }
        String token = JwtTokenUtils.genToken(String.valueOf(user.getId()), user.getPassword());
        user.setToken(token);
        user.setPassword("");
        return Result.success(user);
    }

    /**
     * 获取图片验证码和唯一key
     * @return
     * @throws IOException
     */
    @RequestMapping("/code")
    // 该方法名为getImageCode，返回类型为Result，可能会抛出IOException异常
    public Result getImageCode() throws IOException {
        // 使用hutool工具库，创建一个包含圆形干扰线的验证码，宽100像素，高40像素，4个字符，5条干扰线
        CircleCaptcha captcha = CaptchaUtil.createCircleCaptcha(100, 40, 4, 5);
        // 获取生成的验证码字符
        String code = captcha.getCode();
        // 生成一个临时的唯一标识符（例如，UUID）
        String captchaKey = UUID.randomUUID().toString();
        // 使用Redis的Template存储验证码，设置有效期为30分钟
        redisTemplate.opsForValue().set("captcha:" + captchaKey, code, 30L, TimeUnit.MINUTES);
        // 初始化一个空的base64字符串用于存放图片验证码
        String base64String = "";
        try {
            // 获取图片验证码的Base64编码，并添加到"data:image/png;base64,"前缀
            // 添加MIME类型前缀： 该字符串前面加上 "data:image/png;base64," 是一种约定，
            // 用于说明这个Base64编码字符串是一个PNG图片。这个前缀允许浏览器理解如何解析这个数据。
            // captcha.getImageBase64() 这个方法调用会返回一个已经转换为Base64格式的字符串，这个字符串实际上是图片的二进制数据
            base64String = "data:image/png;base64," + captcha.getImageBase64();
        } catch (Exception e) {
            // 如果获取失败，打印堆栈信息
            e.printStackTrace();
        }
        // 创建VerifyCodeResp对象，用于封装图形验证码和唯一key
        VerifyCodeResp verifyCodeResp = new VerifyCodeResp();
        // 设置唯一key
        verifyCodeResp.setCaptchaKey(captchaKey);
        // 设置图形验证码图片
        verifyCodeResp.setCaptchaImg(base64String);
        // 返回封装好的VerifyCodeResp对象，状态为成功
        return Result.success(verifyCodeResp);
    }


    @RequestMapping("/register")
    public Result register(@RequestBody Admin admin) {
        if(admin.getName() == null || "".equals(admin.getName())) {
            throw new CustomException("用户名不能为空!");
//            return Result.error("用户名不能为空!");
        }
        if(admin.getPassword() == null || "".equals(admin.getPassword())) {
            throw new CustomException("密码不能为空!");
        }
        adminService.add(admin);
        return Result.success();
    }


    @RequestMapping("/getAllUsers")
    public Result getAllUsers() {
        List<Admin> admins = adminService.getAllUsers();
        return Result.success(admins);
    }

    @RequestMapping("/search")
    public Result search(Params params) {
        PageInfo<Admin> result = adminService.search(params);
        return Result.success(result);
    }

    @RequestMapping("/seachMyUser")
    public Result getMyUser(@RequestBody Integer id) {
        Admin admin = adminService.selectByPrimaryKey(Long.valueOf(id));
        return Result.success(admin);
    }

    @RequestMapping("/add0edit")
    public Result add(@RequestBody Admin admin) {
        if(admin.getName() == null || "".equals(admin.getName())) {
            throw new CustomException("用户名不能为空!");
//            return Result.error("用户名不能为空!");
        }
        if(admin.getId() == null) {
            Admin user = adminService.searchByName(admin.getName());
            if(user != null) {
                throw new CustomException("该用户已存在");
//                return Result.error("该用户已存在");
            }
            adminService.add(admin);
        }else {
            Admin user = adminService.searchByName(admin.getName());
            adminService.updateUser(admin);
        }
        return Result.success();
    }

    @RequestMapping("/delete/{id}")
    public Result delete(@PathVariable Integer id) {
        Integer result = adminService.delete(id);
        if(result == 1) {
            return Result.success();
        }else {
            return Result.error("删除失败");
        }
    }

}
