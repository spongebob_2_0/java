package com.example.graduationproject.Controller;

import com.example.graduationproject.common.AutoLog;
import com.example.graduationproject.common.CustomException;
import com.example.graduationproject.common.Result;
import com.example.graduationproject.entity.Log;
import com.example.graduationproject.entity.Notice;
import com.example.graduationproject.entity.Params;
import com.example.graduationproject.service.LogService;
import com.example.graduationproject.service.NoticeService;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-25
 * Time: 11:36
 */
@RestController
@RequestMapping("/notice")
public class NoticeController {

    @Resource
    private NoticeService noticeService;

    private static final Logger log = LoggerFactory.getLogger(NoticeController.class);



    @RequestMapping("/search")
    public Result search(Params params) {
        PageInfo<Notice> result = noticeService.search(params);
        return Result.success(result);
    }

    @AutoLog("更新公告操作")
    @RequestMapping("/add0edit")
    public Result add(@RequestBody Notice notice) {
        if(notice.getName() == null || "".equals(notice.getName())) {
            throw new CustomException("公告名称不能为空!");
        }
        if(notice.getId() == null) {
            noticeService.add(notice);
        }else {
            noticeService.updateType(notice);
        }
        return Result.success();
    }

    @RequestMapping("/delete/{id}")
    public Result delete(@PathVariable Integer id) {
        Integer result = noticeService.delete(id);
        if(result == 1) {
            return Result.success();
        }else {
            return Result.error("删除失败");
        }
    }

    @RequestMapping("/findTop")
    public Result findTop() {
        List<Notice> list = noticeService.findTop();
        return Result.success(list);
    }

//    @RequestMapping("/delBatch")
//    public Result delBatch(@RequestBody List<Type> list) {
//        for(Type type: list) {
//            typeService.delete(type.getId());
//        }
//        return Result.success();
//    }



}
