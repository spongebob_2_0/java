package com.example.graduationproject.common;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.example.graduationproject.entity.Admin;
import com.example.graduationproject.entity.Log;
import com.example.graduationproject.service.LogService;
import com.example.graduationproject.utils.JwtTokenUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-28
 * Time: 14:50
 */
@Component
@Aspect   //定义一个切面
public class LogAspect {

    @Resource
    private LogService logService;

    @Around("@annotation(autoLog)")   //定义切入点,表明加上这个注解的方法会被切入,在加注解的方法前后执行这个方法里面的逻辑
    public Object doAround(ProceedingJoinPoint joinPoint,AutoLog autoLog) throws Throwable {

        // 操作内容,我们在注解里面定义了value(),然后再需要切入的接口上面去写上对应的操作内容即可
        String name = autoLog.value();
        // 操作时间(当前时间)
        String time = DateUtil.now();
        // 操作人
        String username = "";
        Admin user = JwtTokenUtils.getCurrentUser();
        if(ObjectUtil.isNotNull(user)) {
            username = user.getName();
        }
        // 操作人IP
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String ip = request.getRemoteAddr();

        // 先执行切入点的方法
        Result resulet = (Result) joinPoint.proceed(); //我们加注解的方法执行逻辑,前后是我们自己定义的逻辑

        // 这里是为了放在用户在登录的时候还没有用户信息存在JWT里面,我们在用户登录之后去拿用户信息
        Object data = resulet.getData();
        if(data instanceof Admin) {
            Admin admin = (Admin) data;
            username = admin.getName();
        }

        // 执行成功之后将这种操作插入数据库 (日志表)
        Log log = new Log(null,name,time,username,ip);
        logService.add(log);

        return resulet;
    }
}
