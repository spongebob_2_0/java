package com.example.graduationproject.common;

import java.lang.annotation.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-28
 * Time: 12:00
 */
@Target(ElementType.METHOD)  // @Target注解用于指定这个注解作用于哪里,这里作用于方法上
@Retention(RetentionPolicy.RUNTIME)  // @Retention注解用于指定这个注解的信息会保留到什么时候,这里是运行时使用
@Documented   // @Documented注解表明这个注解应该被javadoc工具记录 (生成文档的)
public @interface AutoLog {

    String value() default "";

}
