package com.example.graduationproject.utils;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.common.auth.CredentialsProvider;
import com.aliyun.oss.common.auth.DefaultCredentialProvider;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
/**
 * 阿里云 OSS 工具类
 */
@Component
public class AliOSSUtils {

    //这里是外网访问的域名(根据你选择的阿里云地域节点去确定)
    private String endpoint = "https://oss-cn-beijing.aliyuncs.com";
    // RAM用户的访问密钥（AccessKey ID和AccessKey Secret）。
    private String accessKeyId = "LTAI5t78iL22k1A3pc3FqLZX";
    private String accessKeySecret = "Jb1RXOViCtOxdbRCNVEavdS7yF6IQE";
    private String bucketName = "web-183";

    /**
     * 实现上传图片到OSS
     */
    public String upload(MultipartFile file) throws IOException {
        // 使用代码嵌入的RAM用户的访问密钥配置访问凭证。
        CredentialsProvider credentialsProvider = new DefaultCredentialProvider(accessKeyId, accessKeySecret);
        // 获取上传的文件的输入流
        InputStream inputStream = file.getInputStream();

        // 避免文件覆盖 (用UUID生成一个随机值拼接在文件后缀名的前面)
        String originalFilename = file.getOriginalFilename();  //获取文件名
        String fileName = UUID.randomUUID().toString() + originalFilename.substring(originalFilename.lastIndexOf("."));

        //上传文件到 OSS
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        ossClient.putObject(bucketName, fileName, inputStream);

        //文件访问路径 (这里拼接的就是对象存储桶+域名+存储桶中文件保存的位置)
        String url = endpoint.split("//")[0] + "//" + bucketName + "." + endpoint.split("//")[1] + "/" + fileName;
        // 关闭ossClient
        ossClient.shutdown();
        return url;// 把上传到oss的路径返回
    }
}