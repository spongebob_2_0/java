package com.example.graduationproject.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 操作日志表
 * @TableName log
 */
@Data
@AllArgsConstructor
public class Log implements Serializable {
    /**
     * 主键id
     */
    private Integer id;

    /**
     * 操作内容
     */
    private String name;

    /**
     * 操作时间
     */
    private String time;

    /**
     * 操作人
     */
    private String username;

    /**
     * 操作ip
     */
    private String ip;

    private static final long serialVersionUID = 1L;
}