package com.example.graduationproject.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName book
 */
@Data
public class Book implements Serializable {
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 图书名称
     */
    private String name;

    /**
     * 图书价格
     */
    private String price;

    /**
     * 图书作者
     */
    private String author;

    /**
     * 图书出版社
     */
    private String press;

    /**
     * 图书封面
     */
    private String img;


    /**
     * 图书分类ID
     */
    private String typeId;

    /**
     * 分类名称
     */
    private String typeName;

    private String content;

    private static final long serialVersionUID = 1L;
}