package com.example.graduationproject.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户信息表
 * @TableName user
 */
@Data
public class Admin implements Serializable {
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 密码
     */
    private String password;

    /**
     * 性别
     */
    private String sex;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 电话
     */
    private String phone;

    private String token;

    private String role;

    private String vercode;

    private String captchaKey;

    private static final long serialVersionUID = 1L;

    private String avatar;
}