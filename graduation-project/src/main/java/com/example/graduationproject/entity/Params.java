package com.example.graduationproject.entity;

import lombok.Data;
import org.apache.coyote.http11.filters.SavedRequestInputFilter;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-23
 * Time: 18:57
 */
@Data
public class Params {
    private String name;
    private String author;
    private String phone;
    private Integer pageNum;
    private Integer pageSize;
    private String descripition;
    private Integer userId;
    private String username;
    private String content;

}
