package com.example.graduationproject.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * 酒店表
 * @TableName reserve
 */
@Data
public class Reserve implements Serializable {
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 酒店ID
     */
    private Integer hotelid;

    /**
     * 用户ID
     */
    private Integer userid;

    /**
     * 预定时间
     */
    private String time;

    // 酒店名称
    private String hotelName;

    // 用户名称
    private String userName;

    private static final long serialVersionUID = 1L;
}