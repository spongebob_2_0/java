package com.example.graduationproject.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * 请假审核表
 * @TableName audit
 */
@Data
public class Audit implements Serializable {
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 请假缘由
     */
    private String name;

    /**
     * 请假日期
     */
    private String time;

    /**
     * 请假用户ID
     */
    private Integer userid;

    /**
     * 审核状态
     */
    private String status;

    /**
     * 审核意见
     */
    private String reason;

    /**
     * 请求天数
     */
    private String day;

    /**
     * 请求的用户姓名
     */
    private String userName;

    private static final long serialVersionUID = 1L;
}