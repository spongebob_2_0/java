package com.example.graduationproject.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * 系统公告表
 * @TableName notice
 */
@Data
public class Notice implements Serializable {
    /**
     * 
     */
    private Integer id;

    /**
     * 公告内容
     */
    private String content;

    /**
     * 公告时间
     */
    private String time;

    /**
     * 公告标题
     */
    private String name;

    private static final long serialVersionUID = 1L;
}