package com.example.graduationproject.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * 酒店表
 * @TableName hotel
 */
@Data
public class Hotel implements Serializable {
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 酒店名称
     */
    private String name;

    /**
     * 入住价格
     */
    private String price;

    /**
     * 酒店图片
     */
    private String img;

    /**
     * 剩余间数
     */
    private Integer num;

    private static final long serialVersionUID = 1L;
}