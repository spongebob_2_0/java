package com.example.graduationproject.entity;

import java.io.Serializable;

import cn.hutool.core.annotation.Alias;
import lombok.Data;

/**
 * 图书分类表
 * @TableName type
 */
@Data
public class Type implements Serializable {
    /**
     * 主键id
     */
    private Integer id;

    /**
     * 分类名称

     */
    @Alias("分类名称")
    private String name;

    /**
     * 分类介绍
     */
    @Alias("分类介绍")
    private String descripition;



    private static final long serialVersionUID = 1L;
}