--这个文件主要是用来写建库建表语句
--将建表的时候的 sql 保留下来，以备后续部署其他机器的时候就很方便

create database if not exists java_blog_system2 charset utf8mb4;
use java_blog_system2;

--删除旧表，重新创建新表，删除旧表是为了防止之前的残留数据对后续的程序有负面影响
drop table if exists user;
drop table if exists blog;

--创建表

--博客表
create table blog (
    blogId int primary key  auto_increment,
    title varchar(128),
    content varchar(4096),
    postTime datetime,
    userId int
);

--用户表
create table user (
    userId int primary key auto_increment,
    username varchar(20) unique,
    password varchar(20)
);

--构造一些测试数据
insert into blog values
(2, "这是我第二篇博客", "从今天开始我要好好写博客哦", now(), 1),
(4, "这是我第四篇博客", "从大前天开始我要好好写博客哦", now(), 1);

insert into user value (1,"Jack",123);