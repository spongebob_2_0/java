package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Blog;
import model.BlogDao;
import model.User;
import model.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:根据 blogId 获取用户信息
 * Author: 保护小周
 * Date: 2023-05-18
 * Time: 9:04
 */
@WebServlet("/author")
public class AuthorServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ObjectMapper objectMapper = new ObjectMapper();//转换 json 格式
        String blogId = req.getParameter("blogId");

        if(blogId == null) { //没有拿到数据
            resp.setContentType("text/html; charset=utf8");
            resp.getWriter().write("没有blogId！！！");
            return;
        }
        //根据 blogId 拿到整个博客的信息，包含用户id
        BlogDao blogDao = new BlogDao(); // 操作 blog 表的类
        Blog blog = blogDao.selectById(Integer.parseInt(blogId));

        if(blog == null) {
            resp.setContentType("text/html; charset=utf8");
            resp.getWriter().write("没有用户信息！！！");
            return;
        }

        //根据用户id拿到完整的用户信息
        UserDao userDao = new UserDao();//操作 user 表的类
        User user = userDao.selectById(blog.getUserId());

        if(user == null) {
            resp.setContentType("text/html; charset=utf8");
            resp.getWriter().write("user表没有用户信息！！！");
            return;
        }

        resp.setContentType("application/json; charset=uft8");
        //将用户信息返回给前端页面
        objectMapper.writeValue(resp.getWriter(),user);
    }
}
