package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Blog;
import model.BlogDao;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:展示全部博客，psot 添加博客
 * Author: 保护小周
 * Date: 2023-05-13
 * Time: 17:14
 */
@WebServlet("/blog")
public class BlogServlet extends HttpServlet {

    /**
     * 前端页面初始化请求博客列表页面
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ObjectMapper objectMapper = new ObjectMapper();//第三方解析 json 格式的类，来自 Jackson

        //博客详情页面根据博客 ID 发来的查询博客详情的请求
        String blogId = req.getParameter("blogId");
        System.out.println(req.getParameter("blogId"));
        System.out.println(blogId);
        BlogDao blogDao = new BlogDao();
        if(blogId == null) {
            //queryString 不存在，说明这次请求是获取博客列表
            List<Blog> blogs = blogDao.selectAll();
            //将数据转化为 json格式传输
            resp.setContentType("application/json; charset=utf-8"); //指定一个响应的格式以及，字符集
            objectMapper.writeValue(resp.getWriter(),blogs);
        } else {
            //queryString 存在，说明这次请求是获取一篇博客
            //根据博客Id 在数据库中查询这一篇博客
            Blog blog =  blogDao.selectById(Integer.parseInt(blogId));
            resp.setContentType("application/json; charset=utf-8");
            objectMapper.writeValue(resp.getWriter(), blog);
        }
    }

    /**
     * 添加博客
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf8");
        resp.setContentType("text/html; charset=utf8");
        //1.验证会话
        HttpSession session = req.getSession(false);
        if(session == null) {
            resp.getWriter().write("用户未登录？？？");
            return;
        }
        //2. 从本次会话中拿到作者信息
        User user = (User) session.getAttribute("user");
        int userId = user.getUserId();

        //3. 从请求中拿到要创建的博客信息，添加到数据库
        String title = req.getParameter("title");

        String content = req.getParameter("editor-markdown-doc");
        if(title == null || title.equals("") || content == null || content.equals("")) {
            resp.getWriter().write("当前提交数据有误! 请确定 标题和正文不为空");
            return;
        }

        //发布时间java 获取系统时间戳存时间为准，自动 now(),博客 id 数据库自增主键

        //4. 创建博客对象
        Blog blog = new Blog();
        blog.setTitle(title);
        blog.setContent(content);
        blog.setUserId(userId);
        blog.setPostTime(new Timestamp(System.currentTimeMillis()));

        //5. 针对数据库 blogDao 博客表操作
        BlogDao blogDao = new BlogDao();
        blogDao.add(blog);

        //跳转到博客列表页
        resp.sendRedirect("blog_list.html");
    }
}
