package api;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description: 注销本次登录，删除会话
 * Author: 保护小周
 * Date: 2023-05-18
 * Time: 15:20
 */
@WebServlet("/loginOut")
public class loginOutServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=utf8");
        //接收会话，将user 对象删除，注销本次登录
        HttpSession session = req.getSession(false);
        if(session == null) {
            resp.getWriter().write("无需注销");
            return;
        }
        session.removeAttribute("user");
        resp.sendRedirect("blog_login.html");
    }
}
