package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.User;
import model.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * Author: 保护小周
 * Date: 2023-05-13
 * Time: 17:15
 */
@WebServlet("/user")
public class UserServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ObjectMapper objectMapper =  new ObjectMapper();
        //1. 前端发出注册请求
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        resp.setContentType("text/html; charset=utf8");

        //2. 验证数据是否存在
        if(username == null || username.equals("") || password == null || password.equals("")) {
            String html = "<h3> 登录失败！用户不存在，请先注册账号信息</h3>";
            resp.getWriter().write(html);
        }

        //3. 数据入库
        UserDao userDao = new UserDao();
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        userDao.add(user);
        String html = "<h3> 注册成功</h3>";
        resp.sendRedirect("blog_login.html");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //根据前端的请求删除用户数据,根据用户 id
        ObjectMapper objectMapper = new ObjectMapper();
        //1. 根据前端发来的请求删除用户数据
        String username =  req.getParameter("username");

    }
}
