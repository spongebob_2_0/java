package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.User;
import model.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description: 登录界面——信息验证——创建会话
 * Author: 保护小周
 * Date: 2023-05-15
 * Time: 14:39
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //设置请求的编码，告诉 servlet 按照啥样的格式来理解请求
        req.setCharacterEncoding("UTF-8");
        //前端页面发出登录或验证请求
        ObjectMapper objectMapper = new ObjectMapper();
        //1. 读取参数总用户名和密码
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        //设置响应的格式login
        resp.setContentType("text/html; charset=utf8");

        if (username == null || username.equals("") || password.equals("")|| password == null) { //二次验证后端是否拿到数据
            String html = "<h3> 登录失败！缺少 username 和 password </h3>";
            resp.getWriter().write(html);
            return;
        }

        //2. 读取数据库，验证用户名密码是否正确
        UserDao userDao = new UserDao();
        User user = userDao.selectByUsername(username);

        if (user == null) {
            //如果数据库中没有用户信息，用户不存在
            //userDao.add();
            String html = "<h3> 登录失败！用户不存在，请先注册账号信息</h3>";
            resp.getWriter().write(html);
            return;
        } else { // 用户存在
            //密码验证
            if (!user.getPassword().equals(password)) {// 密码验证失败成功
                System.out.println("登录失败");
                String html = "<h3> 登录失败！用户名或密码错误！！</h3>";
                resp.getWriter().write(html);
                //登录失败重定向到登录界面
                //resp.sendRedirect("blog_login.html");
                return;
            }

            //3.用户密码登录成功,创建会话，使用该会话保存用户信息
            //把当前的用户名保存到会话中，此处的 HttpSession 又可以当成一个 map 使用
            HttpSession session = req.getSession(true);// 键
            session.setAttribute("user", user); //值 —— 内部又是一个键值对
            //4. 重定向到主页
            resp.sendRedirect("blog_list.html");
        }
    }

    /**
     * 使用 session会话来验证用户是否处于登录状态
     * @param req   请求附带用户信息
     * @param resp  会话存在返回用户信息
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=UTF-8");
        ObjectMapper objectMapper = new ObjectMapper();

        //验证会话 , 验证用户登录状态
        HttpSession session = req.getSession(false);
        if(session == null) {
            //未登录，返回一个空的 user 对象
            User user = new User();
            objectMapper.writeValue(resp.getWriter(), user);
            return;
        }

        User user = (User) session.getAttribute("user");
        if(user == null) {
            user = new User();
            objectMapper.writeValue(resp.getWriter(), user);
            return;
        }

        //会话确实存在,成功的取出了 user 对象，就直接返回 user
        objectMapper.writeValue(resp.getWriter(), user);
    }
}
