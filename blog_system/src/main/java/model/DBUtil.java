package model;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * Author: 保护小周
 * Date: 2023-05-13
 * Time: 12:05
 */
public class DBUtil {
    private static DataSource dataSource = new MysqlDataSource();

    static {
       /*( (MysqlDataSource) dataSource).setUrl("jdbc:mysql://http://8.130.113.189:3306/java_blog_system?characterEncoding=utf8&useSSL=false");*/
       ( (MysqlDataSource) dataSource).setUrl("jdbc:mysql://127.0.0.1:3306/java_blog_system2?characterEncoding=utf8&useSSL=false");
       ( (MysqlDataSource) dataSource).setUser("root");
       ( (MysqlDataSource) dataSource).setPassword("");
    }

    public static Connection getConnection() throws SQLException {
        System.out.println(dataSource.getConnection());
        return dataSource.getConnection();
    }

    public static void close(Connection connection, PreparedStatement statement, ResultSet resultSet) {
        if(resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        if(statement != null) {
            try {
                statement.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        if(connection != null) {
            try {
                connection.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
}
