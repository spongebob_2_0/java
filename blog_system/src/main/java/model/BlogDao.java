package model;

import model.Blog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * Author: 保护小周
 * Date: 2023-05-13
 * Time: 14:21
 */
public class BlogDao {

    /**
     * 新增一篇博客
     * @param blog
     */
    public void add(Blog blog) {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            //1. 创建一个连接
            connection = DBUtil.getConnection();
            //2. 构建sql 语句，执行环境
            String sql = "insert into blog value(null, ?, ?, ?, ?)";
            statement = connection.prepareStatement(sql);

            statement.setString(1, blog.getTitle());
            statement.setString(2, blog.getContent());
            statement.setTimestamp(3, blog.getPostTimes());
            statement.setInt(4, blog.getUserId());
            //3. 执行sql
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            //4. 关闭资源
            DBUtil.close(connection, statement, null);
        }
    }

    /**
     * 根据博客 id 来查询指定博客（博客详情页中）
     * @param blogId
     * @return
     */
    public Blog selectById(int blogId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            //1. 创建连接
            connection = DBUtil.getConnection();
            //2. 构建sql
            String sql =  "select * from blog where blogId = ? ";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,blogId);

            //3. 执行sql
            resultSet = statement.executeQuery();
            if(resultSet.next()) {
                Blog blog = new Blog();
                blog.setBlogId(resultSet.getInt("blogId"));
                blog.setTitle(resultSet.getString("title"));
                blog.setContent(resultSet.getString("content"));
                blog.setPostTime(resultSet.getTimestamp("postTime"));
                blog.setUserId(resultSet.getInt("userId"));
                return blog;
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return null;
    }

    /**
     * 直接查询出数据库所在的博客列表（用于博客列表页）
     * @return
     */
    public List<Blog> selectAll() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Blog> blogs = new LinkedList<Blog>();

        try {
            //1. 创建连接
            connection = DBUtil.getConnection();
            System.out.println("连接成功");
            //2. 构建sql
            String sql =  "select * from blog order by postTime desc";
            statement = connection.prepareStatement(sql);

            //3. 执行sql
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Blog blog = new Blog();
                blog.setBlogId(resultSet.getInt("blogId"));
                blog.setTitle(resultSet.getString("title"));
                String content = resultSet.getString("content");
                //博客列表中不需要将所有的正文显示出来，所以我们对部分正文进行截断
                if(content.length() >= 128) {
                    content = content.substring(0, 127) + "...";
                }
                blog.setContent(content);

                blog.setPostTime(resultSet.getTimestamp("postTime"));
                blog.setUserId(resultSet.getInt("userId"));
                blogs.add(blog);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            // 4.释放资源
            DBUtil.close(connection, statement, resultSet);
        }
        return blogs;
    }

    /**
     * 根据博客 id 删除指定博客
     * @param blogId
     */
    public void delete(int blogId) {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            // 1. 创建连接
            connection = DBUtil.getConnection();
            // 2. 构建sql 语句
            String sql = "delete from blog where blogId = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,blogId);
            // 3. 执行 sql
            statement.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            //4. 释放资源
            DBUtil.close(connection, statement, null);
        }

    }
}
