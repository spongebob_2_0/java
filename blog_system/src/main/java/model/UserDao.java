package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * Description: 针对用户表提供的基本操作
 * Author: 保护小周
 * Date: 2023-05-13
 * Time: 14:21
 */
public class UserDao {
    /**
     * 创建一个用户信息
     * @param user
     */
    public void add(User user) {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            //1. 创建一个连接
            connection = DBUtil.getConnection();
            //2. 构建sql 语句，执行环境
            String sql = "insert into user value(null, ?, ?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1,user.getUsername());
            statement.setString(2,user.getPassword());

            //3. 执行sql
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            //4. 关闭资源
            DBUtil.close(connection, statement, null);
        }
    }

    /**
     * 根据用户id 删除用户信息
     * @param userId
     */
    public void delete(int userId) {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            //1. 创建一个连接
            connection = DBUtil.getConnection();
            //2. 构建sql 语句，执行环境
            String sql = "delete from user where userId = ? ";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,userId);

            //3. 执行sql
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            //4. 关闭资源
            DBUtil.close(connection, statement, null);
        }
    }

    /**
     * 根据 userId 来查看用户信息
     * @param userId
     * @return
     */
    public User selectById(int userId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            //1. 创建连接
            connection = DBUtil.getConnection();
            //2. 构建sql
            String sql =  "select * from user where userId = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,userId);

            //3. 执行sql
            resultSet = statement.executeQuery();
            if(resultSet.next()) {
                User user = new User();
                user.setUserId(resultSet.getInt("userId"));
                user.setUsername(resultSet.getString("username"));
                user.setPassword(resultSet.getString("password"));
                return user;
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return null;
    }

    /**
     * 根据username 来查询用户信息（登陆的时候）
     * @param username
     * @return
     */
    public User selectByUsername(String username) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            //1. 创建连接
            connection = DBUtil.getConnection();
            //2. 构建sql
            String sql =  "select * from user where username = ? ";
            statement = connection.prepareStatement(sql);
            statement.setString(1,username);

            //3. 执行sql
            resultSet = statement.executeQuery();
            if(resultSet.next()) {
                User user = new User();
                user.setUserId(resultSet.getInt("userId"));
                user.setUsername(resultSet.getString("username"));
                user.setPassword(resultSet.getString("password"));
                return user;
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return null;
    }
}
