//界面判断
//登录/注册按钮单击事件
var register = document.querySelector(".register");//登录
var login = document.querySelector(".login");//注册
var show1 = document.querySelector(".t1"); // 显示
var show2 = document.querySelector(".t2");// 隐藏
//默认登录界面的 id，调用判断数据是否正确
FormAuthentication("#password");


//单击了登录界面切换按钮
register.onclick = function() {
    if(show1.className == "t1") {
        //location.reload();
    } else {
        show1.className = "t1";
        show2.className = "t2";

        let show = "#password";

        //判断 input 获取的内容是否合乎规范
        FormAuthentication(show);
    }
}

// //单击了注册界面切换按钮
login.onclick = function() {
    if(show2.className == "t1") {
        //location.reload();
    } else {
        show1.className = "t2";
        show2.className = "t1";

        let show = "#password2";

        //判断 input 获取的内容是否合乎规范
        FormAuthentication(show);
    }
}


//标签获取 input 属性
function FormAuthentication(show) {
    console.log(show)
    let password = document.querySelector(show);
    //执行判断
    isPassword(password);
}

//密码判断
function isPassword(password) {
    console.log(password)

    /*//focus 获取焦点，失去焦点 ：Blur
    password.onfocus = function() { //
        console.log(1)
    }*/

    password.onblur = function () { // 密码验证
        let passwordVal = password.value;
        console.log(passwordVal)
        if(passwordVal.length<6||passwordVal.length>8){
            console.log("密码长度应该在 6 ~ 8 位之间");
        } else if(passwordVal.charAt(passwordVal.length-1)!=="*") {
            console.log("密码的最后一位应该是 * ");
        }
    }

    //确认密码
    let confirmPassword = document.querySelector("#password3");
    confirmPassword.onfocus = function() {
        if(password.value.length == 0) {
            console.log("请先输入密码");
        }
    }

    confirmPassword.onblur = function() {
        if(show2.className == "t1") {
            console.log(password.value.length);
            if(password.value.length > 0) {
                if(confirmPassword.value != password.value) {
                    console.log("密码不一致，请重新输入！")
                } else {
                    console.log("成功！")
                }
            }
        }
    }
}


//提交事件
var loginbtn = document.querySelector('.loginbtn');

//登录
loginbtn.addEventListener('click',function () {
    let button = document.querySelector(".loginbtn");
    //修改样式

    let paValue = document.querySelector("#password").value;
    /*//鼠标移出事件
    button.addEventListener("mouseout",function () {
        button.className='loginbtn'
    })*/
})

let button = document.querySelector(".loginbtn");
//单击
button.onclick = function() {
    console.log("点击")
    button.className = 'loginbtnClick';
    window.location.href = "blog_list.html";
}

// 鼠标弹起事件
button.onmouseup = function() {
    console.log("弹起")
    button.className = 'loginbtn';
}