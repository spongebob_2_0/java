package com.example.websocketdemo.controller;

import com.example.websocketdemo.Service.UserService;
import com.example.websocketdemo.comment.AjaxResult;
import com.example.websocketdemo.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-21
 * Time: 0:18
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/login")
    public AjaxResult login(@RequestBody User user) {
        if(user.getUsername() == null || user.getPassword() == null) {
            return AjaxResult.fail(-2,"用户名获取密码错误");
        }
        User userInfo = userService.selectByPrimaryName(user.getUsername());
        if(userInfo == null) {
            return AjaxResult.fail(-2,"该用户不存在");
        }
        if(user.getUsername().equals(userInfo.getUsername()) && user.getPassword().equals(userInfo.getPassword())) {
           return AjaxResult.success(user);
        }
        return AjaxResult.fail(-2,"出错了,请联系管理员!");
    }

    @RequestMapping("/register")
    public Object register(@RequestBody User user) {
        User userIfno = userService.selectByPrimaryName(user.getUsername());
        if(userIfno != null) {
            return AjaxResult.fail(-2,"该用户已经被注册了");
        }
        int result = userService.insert(user);
        return AjaxResult.success(user);
    }
}
