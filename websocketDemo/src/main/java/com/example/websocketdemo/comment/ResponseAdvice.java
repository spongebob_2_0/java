package com.example.websocketdemo.comment;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
/**
 * Created with IntelliJ IDEA.
 * Description: 保底统一返回数据格式的封装
 * 当返回的数据不是 AjaxResult 的时候转换成 AjaxResult
 * User: WuYimin
 * Date: 2023-06-22
 * Time: 10:30
 */
// 使用 @ControllerAdvice 注解，表明这是一个全局的控制器建议，这种类通常用于定义全局的异常处理，全局的数据绑定和全局的数据预处理
@ControllerAdvice
public class ResponseAdvice implements ResponseBodyAdvice {

    // 自动注入 ObjectMapper 类的实例，用于对象和 JSON 字符串之间的转换
    @Autowired
    private ObjectMapper objectMapper;

    // supports方法用于判断是否要对返回体进行处理，true 表示对所有方法的返回体都进行处理
    @Override
    public boolean supports(MethodParameter returnType, Class converterType) {
        return true;
    }

    // @SneakyThrows 注解可以自动处理方法内部抛出的所有异常，避免写过多的try-catch代码
    @SneakyThrows
    @Override
    // beforeBodyWrite方法是在方法的返回结果输出之前进行处理的方法
    // 该方法接受六个参数，分别是方法的返回结果，方法的返回类型，选择的内容类型，选择的转换器类型，请求和响应对象
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        // 如果方法的返回结果是 AjaxResult 类型的，那么直接返回
        if(body instanceof AjaxResult) {
            return body;
        }
        // 如果方法的返回结果是 String 类型的，那么使用 objectMapper 将其转为 JSON 字符串，
        //并且包装为 AjaxResult 类型的成功返回
        if(body instanceof String) {
            return objectMapper.writeValueAsString(AjaxResult.success(body));
        }
        // 其他情况下，将方法的返回结果直接包装为 AjaxResult 类型的成功返回
        return AjaxResult.success(body);
    }
}