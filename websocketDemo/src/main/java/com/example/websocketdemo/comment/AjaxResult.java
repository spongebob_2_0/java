package com.example.websocketdemo.comment;

import lombok.Data;

import java.io.Serializable;

@Data
public class AjaxResult implements Serializable {
    private int code;
    private String msg;
    private Object data;

    /**
     * 返回成功数据
     * @param data
     * @param msg
     * @return
     */
    public static AjaxResult success(Object data, String msg) {

        //AjaxResult这个类是一个通用的数据传输对象（DTO），通常用于封装服务器端发送给客户端的响应数据。
        //这个对象的目的是提供一种统一的方式来封装和传输响应数据，无论请求处理的结果如何，都可以使用这个对象来传输
        AjaxResult ajaxResult = new AjaxResult();
        //表示请求处理的结果，通常是一个状态码，比如200表示成功，404表示资源未找到，500表示服务器内部错误等。
        ajaxResult.setCode(200);
        //表示与状态码相关的消息，通常用于给用户展示或者帮助开发者调试问题。
        ajaxResult.setMsg(msg);
        //这是实际的响应数据，它可以是任何类型的对象，比如一个用户对象、一个订单对象或者一个对象的列表。
        ajaxResult.setData(data);
        return ajaxResult;
    }
    public static AjaxResult success(Object data) {
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setCode(200);
        ajaxResult.setMsg("");
        ajaxResult.setData(data);
        return ajaxResult;
    }

    /**
     * 返回失败数据
     * @param code
     * @param msg
     * @return
     */
    public static AjaxResult fail(Integer code, String msg) {
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setCode(code);
        ajaxResult.setMsg(msg);
        ajaxResult.setData("");
        return ajaxResult;
    }
    public static AjaxResult fail(Integer code, String msg, Object data) {
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setCode(code);
        ajaxResult.setMsg(msg);
        ajaxResult.setData(data);
        return ajaxResult;
    }
}