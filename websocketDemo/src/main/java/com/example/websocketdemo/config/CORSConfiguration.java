package com.example.websocketdemo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * 处理前端跨域请求
 * 配置了跨域资源共享（CORS）规则，允许跨域请求来自任何源
 *
 * @Author wuyimin
 * @Version 1.0
 */
@Configuration
public class CORSConfiguration {
    @Bean
    public CorsFilter corsFilter() {
        // 1.创建 CORS 配置对象
        CorsConfiguration config = new CorsConfiguration();
        // 放行哪些原始域
        config.addAllowedOrigin("*");
        // 是否发送Cookie信息（这里将其设置为false）
        config.setAllowCredentials(false);
        // 放行哪些原始域（请求方式）
        config.addAllowedMethod("*");
        // 放行哪些原始域（头部信息）
        config.addAllowedHeader("*");

        // 2.添加地址映射
        UrlBasedCorsConfigurationSource corsConfigurationSource = new UrlBasedCorsConfigurationSource();
        corsConfigurationSource.registerCorsConfiguration("/**", config);
        // 3.返回 CorsFilter 对象
        return new CorsFilter(corsConfigurationSource);
    }
}
