import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-07
 * Time: 1:52
 */
public class JDBCDelete {
    public static void main(String[] args) throws SQLException {
        //JDBC需要通过以下实例来完成开发
        //1.创建并初始化一个数据源
        DataSource dataSource =new MysqlDataSource();
        ((MysqlDataSource)dataSource).setUrl("jdbc:mysql://127.0.0.1:3306/java108?characterEncoding=utf8&&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("183193");
        //2.和数据库服务器建立连接
        Connection connection = dataSource.getConnection();
        //3.构造SQL语句
        String sql ="delete from student where id=1";
        PreparedStatement statement = connection.prepareStatement(sql);
        //4.执行SQL语句
        int ret =statement.executeUpdate();
        if(ret > 0) {
            System.out.println("删除成功!");
        }
        //5.释放必要的资源
        statement.close();
        connection.close();
    }
}
