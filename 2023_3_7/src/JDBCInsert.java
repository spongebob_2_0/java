import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-07
 * Time: 1:52
 */
public class JDBCInsert {
    public static void main(String[] args) throws SQLException {
        Scanner scanner =new Scanner(System.in);
        //JDBC需要通过以下实例来完成开发
        //1.创建并初始化一个数据源
        DataSource dataSource =new MysqlDataSource();
        ((MysqlDataSource)dataSource).setUrl("jdbc:mysql://127.0.0.1:3306/java108?characterEncoding=utf8&&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("183193");
        //2.和数据库服务器建立连接
        Connection connection = dataSource.getConnection();
        //3.从控制台读取用户输入的内容
        System.out.println("请输入学生姓名:");
        String name = scanner.next();
        System.out.println("请输入学生学号:");
        int id = scanner.nextInt();
        //4.构造SQL语句
        String sql ="insert into student value(?,?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1,id);
        statement.setString(2,name);
        //5.执行SQL语句
        int ret =statement.executeUpdate();
        if(ret > 0) {
            System.out.println("插入成功!");
        }
        //6.释放必要的资源
        statement.close();
        connection.close();
    }
}
