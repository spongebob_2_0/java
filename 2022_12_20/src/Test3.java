/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-20
 * Time: 9:58
 */
public class Test3 {
    public static int onlyOne(int[] array){
        int ret =array[0];
        for (int i = 1; i < array.length; i++) {
            ret ^=array[i];
        }
        return ret;
    }
    public static void main(String[] args) {
        int[] array ={2,2,3,6,7,6,7};
        int ret = onlyOne(array);
        System.out.println(ret);
    }
}
