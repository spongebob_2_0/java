import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-20
 * Time: 10:54
 */
public class Test4 {
    public static void main(String[] args) {
        int[][] array ={{1,2,3,5},{4,5,6}};
//        System.out.println(Arrays.deepToString(array));
        /*for (int[] tmp:array) {
            for (int x:tmp) {
                System.out.print(x+" ");
            }
            System.out.println();
        }*/
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j]+" ");
            }
            System.out.println();
        }
    }
}
