import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-20
 * Time: 8:26
 */
public class Test1 {
    public static void bubbleSort(int[] array){
        for (int i = 0; i < array.length-1; i++) {
            boolean flag =false;
            for (int j = 0; j < array.length-1-i; j++) {
                if(array[j]>array[j+1]){
                    int tmp =array[j];
                    array[j] =array[j+1];
                    array[j+1] =tmp;
                    flag =true;
                }
            }
            if(flag == false){
                return;
            }
        }
        return;
    }
    public static void main(String[] args) {
//        int[] array ={9,8,7,6,5,4,3,2,1};
        int[] array ={1,2,3,4,6,7,5,8,9};
//        Arrays.sort(array);
        bubbleSort(array);
        System.out.println(Arrays.toString(array));
    }
}
