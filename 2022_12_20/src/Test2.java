import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-20
 * Time: 9:30
 */
public class Test2 {
    public static int[] find(int[] array,int target){
        for (int i = 0; i < array.length; i++) {
            for (int j = i+1; j < array.length; j++) {
                if(array[i]+array[j] ==target){
                    return new int[]{i,j};
                }
            }
        }
        return null;
    }
    public static void main(String[] args) {
        int[] array ={11,2,3,4,5,6,7,8,9};
        int[] ret = find(array,10);
        System.out.println(Arrays.toString(ret));
    }
}
