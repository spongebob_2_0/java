/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-12-01
 * Time: 15:20
 */

public class ListIndexOutOfException extends RuntimeException{
    public ListIndexOutOfException() {
    }

    public ListIndexOutOfException(String message) {
        super(message);
    }
}
