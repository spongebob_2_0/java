/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-12-01
 * Time: 12:21
 */

import jdk.internal.org.objectweb.asm.tree.InnerClassNode;

import java.util.*;

public class MySingleList {

    class Node{    //成员内部类
        public int val; //存储的数据
        public Node next;  //存储下一个节点的地址

        public Node(int val) {      //构造方法初始化val的值
            this.val = val;
        }
    }

    public Node head;   //代表当前链表头结点的引用

    public void createLink(){
        Node node1 =new Node(12);        //实例化一个node对象,并初始化该对象里面val的值
        Node node2 =new Node(45);
        Node node3 =new Node(23);
        Node node4 =new Node(90);
        node1.next = node2;            //将第一个node对象里面存放的下一个结点的地址换成第二个node对象所在位置的地址
        node2.next = node3;            //将第二个node对象里面存放的下一个结点的地址换成第三个node对象所在位置的地址
        node3.next = node4;            //将第三个node对象里面存放的下一个结点的地址换成第四个node对象所在位置的地址
        head =node1;                   //将头结点的引用更新成第一个node对象所在位置的地址
    }

    //遍历链表
    public void display(){
        /*如果说把整个链表遍历完成就需要head ==null遍历结束
         如果说你遍历到链表的尾巴就需要 head.next == null*/
        Node cur =head;        //在一个临时变量cur中存放头结点的地址,目的是保持头结点的位置不会变动
        while (cur != null){    //当头结点的地址为空时才会退出循环
            System.out.print(cur.val+" ");
            cur =cur.next;        //将头结点里面存放的地址更新为当前对象存放存放的下一个结点的地址(也就是下一个位置的地址)
        }
        System.out.println();      //换行
    }

    //查找是否关键字key在单链表中
    public boolean contains(int key){
        Node cur =head;         //在一个临时变量cur中存放头结点的地址,目的是保持头结点的位置不会变动
        while (cur != null){    //当头结点的地址为空时才会退出循环
            if(cur.val == key){     //判断当前对象里面的val值是否与key值相等
                return true;
            }
            cur =cur.next;      //将头结点里面存放的地址更新为当前对象存放存放的下一个结点的地址(也就是下一个位置的地址)
        }
        return  false;
    }

    //得到链表的长度O(N)
    public int size(){
        int count =0;    //定义一个临时变量count
        Node cur =head;   //在一个临时变量cur中存放头结点的地址,目的是保持头结点的位置不会变动
        while (cur != null){   //当头结点的地址为空时才会退出循环
            count++;
            cur =cur.next;
        }
        return count;
    }

    //头插法  O(1)
    public void addFirst(int data){
        Node node =new Node(data);     //创建一个node对象并传入要插入的值
        node.next =head;   //当前node对象里面存的下一个节点的地址换成当前head所在位置的地址
        head =node;        //再将head里面存的地址换成当前node所在位置的地址
    }

    //尾插法  O(N)  有找尾巴的过程
    public void addLast(int data){
        Node node =new Node(data);     //创建一个node对象并传入要插入的值
        if(head ==null){         //判断头结点是否为空
            head =node;          //如果头结点的引用为空,那么就让头结点直接指向插入的node对象即可完成尾插法
            return;              //直接退出这个方法
        }
        Node cur =head;           //在一个临时变量cur中存放头结点的地址,目的是保持头结点的位置不会变动
        while (cur.next !=null){       //当头结点停留在在最后一个位置就会退出循环,因为最后一个位置里面存放的下一个节点为空
            cur =cur.next;             //将头结点里面存放的地址更新为当前对象存放存放的下一个结点的地址(也就是下一个位置的地址)
        }
        cur.next =node;               //让最后一个位置存放的下一个结点指向插入的node对象即可完成尾插法
    }

    /*总结:链表的插入只是修改指向*/

    //任意位置插入,第一个数据节点为0号下标
    public void addIndex(int index,int data) throws ListIndexOutOfException{   //index为要插入的位置,data为要插入的值
        checkIndex(index);   //判断Index位置是否合法
        if(index == 0){
            addFirst(data);  //头插法
            return;
        }
        if(index ==size()){
            addLast(data);    //尾插法
            return;
        }
        Node cur = findIndexSubOne(index);   //此时cur处于要插入的位置的前一个位置
        Node node =new Node(data);           //创建node对象并初始化要插入的值
        node.next =cur.next;                 //将要插入node对象中的下一个节点的地址更新为cur中存的下一个节点的地址
        cur.next =node;                      //将cur中的下一个节点的地址更新为node当前对象所在位置的地址
    }
    /*找到Index-1位置节点的地址*/
    private Node findIndexSubOne(int index){
        Node cur =head;
        int count = 0;
        while(count !=index -1){
            cur =cur.next;
            count++;
        }
        return cur;
    }

    //检查要插入的index的位置是否合法
    public void checkIndex(int index) throws ListIndexOutOfException{
        if(index <0 || index >size()){        //如果index位置小于0或者大于链表的长度
            throw new ListIndexOutOfException("Index位置不合法!");   //抛出位置不合法的异常
        }
    }

    //删除第一次出现关键字为key的节点
    public void remove(int key){
        if(head ==null){
            return  ;  //说明一个节点都没有
        }
        if(head.val == key){        //如果删除的是头结点
            head =head.next;        //那么直接让头结点的引用更新为头结点存的下一个节点的地址(也就是让头结点走到下一个位置)
            return;
        }
        Node cur =searchPrev(key);   //找到了key的前驱节点
        if(cur ==null){
            return;
        }
        Node del =cur.next;  //del是要删除的节点(del就是cur下一个位置的地址)
        cur.next =del.next;    //把cur里面存的下一个节点的地址更新为del里面存的下一个节点的地址,这样就可以删除del这个节点
        /* 也可以写成:cur.next =cur.next.next  也可以删除第一次出现key位置的节点 */

    }

    //找到关键字key的前一个节点
    public Node searchPrev(int key) {
        Node cur = head;
        while (cur.next != null) {        //cur的最后一个节点为空时,也就说此时cur已经走到最后一个位置了,所以退出循环
            if (cur.next.val == key) {   //判断是不是你要删除的节点
                return cur;              //是就返回
            }
            cur =cur.next;    //cur向后走一步
        }
        return null;  //没有你要删除的节点
    }

    //删除所有值为key的节点
    public void removeAllKey(int key){
        //定义两个变量当做指针去删除所有值为key的节点
        if(head == null){    //判断头结点是否为空
            return;
        }
        Node prev =head;     //第一个指针指向头结点的位置
        Node cur =head.next;  //第二个指针指向头结点后面的位置
        while (cur != null){
            if(cur.val == key){       //判断是否为要删除的值
                prev.next =cur.next;    //删除出现key的值的位置节点
                cur =cur.next;           //cur这个指针向后走一步
            }else {
                prev =cur;      //上面判断如果不是要删除的值,那么第一个指针的位置就更新到第二个指针的位置
                cur =cur.next;  //第二个指针cur向后走一步
            }
        }
        if(head.val == key){
            head =head.next;  //如果头结点里面存的值也是要删除的,走这一步去删除
        }
    }

    //保证链表中的所有节点都可以被回收
    public void clear(){
        head = null;
    }
}

