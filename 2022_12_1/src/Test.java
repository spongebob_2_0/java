/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-12-01
 * Time: 12:33
 */

import java.util.*;

public class Test {
    public static void main(String[] args) {
        MySingleList mySingleList =new MySingleList();
        mySingleList.createLink();
        mySingleList.display();
        System.out.println("============");
        System.out.println(mySingleList.contains(90));
        System.out.println(mySingleList.size());
        System.out.println("=====测试尾插法=====");
        mySingleList.addLast(10);
        mySingleList.display();
        System.out.println("=====测试头插法=====");
        mySingleList.addFirst(30);
        mySingleList.display();
        System.out.println("=====测试任意插入======");
        mySingleList.addIndex(3,999);
        mySingleList.display();
        /*try{
            mySingleList.addIndex(9,666);
        }catch (ListIndexOutOfException e){
            e.printStackTrace();
        }
        mySingleList.display();*/
        System.out.println("====测试删除=====");
        mySingleList.remove(999);
        mySingleList.remove(10);
        mySingleList.remove(30);
        mySingleList.display();
        System.out.println("====测试删除所有出现的key值=====");
        mySingleList.addFirst(12);
        mySingleList.addIndex(2,12);
        mySingleList.addIndex(4,12);
        mySingleList.addIndex(6,12);
        System.out.println("删除前:");
        mySingleList.display();
        System.out.println("删除后:");
        mySingleList.removeAllKey(12);
        mySingleList.display();
    }
}
