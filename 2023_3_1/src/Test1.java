/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-01
 * Time: 21:13
 */
public class Test1 {
    //直接插入排序
    public static void insertSort(int[] array){
        if(array == null || array.length<2) {
            return;
        }
        for(int i =1; i< array.length;i++) {
            int tmp =array[i];
            int j =i-1;
            for(; j>=0; j--) {
                if(array[j] < tmp) {
                    break;
                }else {
                    array[j+1] = array[j];
                }
            }
            array[j+1] = tmp;
        }
    }
    //选择排序
    public static void selectSort(int[] array) {
        if(array == null || array.length <2) {
            return;
        }
        for(int i =0;i<array.length;i++) {
            int minIndex =i;
            for(int j =i+1; j<array.length;j++) {
                if(array[j] < array[minIndex]) {
                    minIndex = j;
                }
            }
            if(array[i] == array[minIndex]) {
                continue;
            }else {
                swap(array,i,minIndex);
            }
        }
    }

    //交换
    private static void swap(int[] array, int i, int j) {
        int tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
    }

    //希尔排序
    public static void shellSort(int[] array) {
        int gap = array.length;
        while (gap > 1) {
            shell(array,gap);
            gap /=2;
        }
    }

    private static void shell(int[] array, int gap) {
        if(array == null || array.length <2) {
            return;
        }
        for(int i =gap; i< array.length; i++) {
            int tmp =array[i];
            int j =i-gap;
            for(; j>=0; j-=gap) {
                if(array[j] < tmp) {
                    break;
                }else {
                    array[j+gap] = array[j];
                }
            }
            array[j+gap] =tmp;
        }
    }

    public static void main(String[] args) {
        int[] array ={9,8,7,6,5,4,3,2,1};
//        insertSort(array);
//        selectSort(array);
        shellSort(array);
        for(int x: array) {
            System.out.print(x+" ");
        }
    }
}
