public class Solution6 {
    public double Power(double base, int exponent) {
        return exponent > 0 ? quickPow(base, exponent) : quickPow(1/base, -exponent);
    }

    public double quickPow(double base, int exp) {
        if(exp == 0) {
            return 1;
        }
        if(exp == 1) {
            return base;
        }
        return Power(base, exp/2)*Power(base, exp - exp/2);
    }
}