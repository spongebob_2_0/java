import java.util.ArrayList;
import java.util.List;

class Solution4 {
    ArrayList<Integer> lis1 = new ArrayList<>();
    private List<String> list = new ArrayList<>();
    public List<String> letterCombinations(String digits) {
        if(digits == null || digits.length() == 0) {
            return list;
        }
        String[] arr ={"","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};
        backtrack(digits,arr,0);
            return list;
        }
    StringBuilder sb = new StringBuilder();
    private void backtrack(String digits, String[] arr, int num) {
        if(num == digits.length()) {
            list.add(sb.toString());
            return;
        }
        //str 表示当前num对应的字符串(应该要选择的字符串)
        String str = arr[digits.charAt(num) - '0'];
        for(int i = 0; i < str.length(); i++) {
            sb.append(str.charAt(i));
            backtrack(digits,arr,num+1);
            sb.deleteCharAt(sb.length()-1);
        }
    }
}