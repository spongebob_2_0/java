package test;

import java.util.*;
 
class TreeNode {
    char val;
    test2.TreeNode left;
    test2.TreeNode right;
    public TreeNode(char val) {
        this.val = val;
    }
}
 
public class Main {
    //主要代码
    public static int i = 0;
    //字符串是先序遍历的，所以创建这棵树就也是按照先序遍历来创建
    public static test2.TreeNode createTree(String str) {
        test2.TreeNode root = null;
        if(str.charAt(i) != '#') {
            root = new test2.TreeNode(str.charAt(i));//创建根
            i++;
            root.left = createTree(str);//创建左树
            root.right = createTree(str);//创建右数
        } else {
            i++;
        }
        return root;
    }
    //中序遍历
    public static void inorder(test2.TreeNode root) {
        if(root == null) return;
        inorder(root.left);
        System.out.print(root.val + " ");
        inorder(root.right);
    }
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        while(scan.hasNextLine()) {
            String str = scan.nextLine();
            test2.TreeNode root = createTree(str);
            inorder(root);
        }
    }
}