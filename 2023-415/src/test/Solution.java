package test;

import java.util.ArrayList;
import java.util.List;

class Solution {
    // 定义一个生成杨辉三角的方法，参数为行数
    public static List<List<Integer>> generate(int numRows) {
        // 定义一个二维列表用于存储杨辉三角的每一行
        List<List <Integer>> arrayList = new ArrayList<>();
        // 定义一个列表用于存储第一行，即 [1]
        List<Integer> one = new ArrayList<>();
        // 向列表 one 中添加数字 1
        one.add(1);
        // 将第一行添加到 arrayList 中
        arrayList.add(one);
        // 从第二行开始，遍历 numRows
        for (int i = 1; i < numRows; i++) {
            // 定义一个列表用于存储当前行
            List<Integer> cur = new ArrayList<>();
            // 当前行的第一个元素是 1
            cur.add(1);
            // 获取上一行的数据
            List<Integer> pre = arrayList.get(i - 1);
            // 从第二个元素开始，遍历上一行的数据
            for (int j = 1; j < i; j++) {
                // 当前行的第 j 个元素等于上一行的第 (j-1) 个元素与第 j 个元素之和
                int ret = pre.get(j - 1) + pre.get(j);
                // 将当前元素添加到当前行
                cur.add(ret);
            }
            // 当前行的最后一个元素是 1
            cur.add(1);
            // 将当前行添加到 arrayList 中
            arrayList.add(cur);
        }
        // 返回存储了整个杨辉三角的二维列表
        return arrayList;
    }

    public static void main(String[] args) {
         List<List<Integer>> list = generate(5);
         for(List list1: list) {
             System.out.println(list1);
         }
    }
}
