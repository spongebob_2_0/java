package test;

import java.util.List;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-04-17
 * Time: 6:13
 */
class ListNode {
    int val;
    ListNode head;
    ListNode next;
}
public class Test4 {
    public static void main(String[] args) {

    }
    //递归打印链表
    public void disPlay(ListNode pHead) {
        if(pHead == null) {
            return;
        }
        if(pHead.next == null) {
            System.out.println(pHead.val + " ");
            return;
        }
        disPlay(pHead.next);
        System.out.println(pHead.val + " ");
    }
    //用栈打印链表
    public void disPlay2(ListNode pHead) {
        Stack<ListNode> stack =new Stack<>();
        ListNode cur =pHead;
        while (cur != null) {
            stack.push(cur);
            cur =cur.next;
        }
        //遍历栈
        while (!stack.isEmpty()) {
            ListNode top = stack.pop();
            System.out.println(top.val + " ");
        }
    }
}
