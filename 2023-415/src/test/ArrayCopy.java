package test;

import java.util.Arrays;


public class ArrayCopy {

	public static void main(String[] args) {
		int[] arr = new int[] { 57, 81, 68, 75, 91 };
		int[] ans = new int[arr.length];
		method01(arr, ans);
		method02(arr, ans);
		method03(arr, ans);
		method04(arr, ans);
		method05(arr, ans);
	}


	public static void method01(int[] arr, int[] ans) {
		for (int i = 0; i < arr.length; i++) {
			ans[i] = arr[i];
		}
		System.out.println("原始复制方法复制的ans数组为 " + Arrays.toString(ans));
	}


	public static void method02(int[] arr, int[] ans) {
		ans = Arrays.copyOf(arr, arr.length + 3);
		System.out.println("copyOf()方法复制的ans数组为 " + Arrays.toString(ans));
	}


	public static void method03(int[] arr, int[] ans) {
		ans = Arrays.copyOfRange(arr, 0, arr.length + 3);
		System.out.println("copyOfRange()方法复制的ans数组为 " + Arrays.toString(ans));
	}
	

	public static void method04(int[] arr, int[] ans) {
		System.arraycopy(arr, 0, ans, 0, 5);
		System.out.println("arraycopy()方法复制的ans数组为 " + Arrays.toString(ans));
	}
	

	public static void method05(int[] arr, int[] ans) {
		ans = arr.clone();
		System.out.println("clone()方法复制的ans数组为 " + Arrays.toString(ans));
	}
}
