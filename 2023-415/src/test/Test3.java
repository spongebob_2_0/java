package test;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-04-16
 * Time: 2:43
 */
public class Test3 {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] arr =new  int[n][];
        //杨辉三角算法
        for(int i=0;i<arr.length;i++) { //外层遍历行数
            arr[i] = new int[i + 1]; //给每一行数组开辟空间
            for (int j = 0; j < arr[i].length; j++) {
                if (j== 0 || j == arr[i].length - 1) {
                    arr[i][j] = 1;
                } else {
                    arr[i][j] = arr[i - 1][j] + arr[i - 1][j - 1];
                }
            }
        }
//        for(int[] tmp: arr) {
//            for(int x: tmp) {
//                System.out.print(x + " ");
//            }
//            System.out.println();
//        }
        for(int[] tmp: arr) {
            System.out.println(Arrays.toString(tmp));
        }
    }
}
