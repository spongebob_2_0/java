package test2;

import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-04-20
 * Time: 23:26
 */
public class Test1 {
    private boolean getPath(TreeNode root, TreeNode node, Stack<TreeNode> stack) {
        if(root == null || node == null) {
            return false;
        }
        stack.push(root);
        //递归过程中，如果根等于 p 或 q ， 说明路径找完了，依次返回
        if(root == node) {
            return true;
        }

        //往左递归找 p 或 q
        boolean leftRet = getPath(root.left, node, stack);
        if(leftRet == true) {
            return true;
        }

        //往右递归找 p 或 q
        boolean rightRet = getPath(root.right, node, stack);
        if(rightRet == true) {
            return true;
        }
        //因为getPath方法，一进来就入栈，如果没有返回true，说明入栈的就不是路劲上的结点，直接弹出，然后返回false
        stack.pop();
        return false;
    }

    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        //1.分别获取 p，q 的路径，依次添加到栈中
        Stack<TreeNode> stack1 = new Stack<>();
        getPath(root, p, stack1);

        Stack<TreeNode> stack2 = new Stack<>();
        getPath(root, q, stack2);

        //2.求出两个栈的大小，让大的先走差值步
        int size1 = stack1.size();
        int size2 = stack2.size();

        if(size1 > size2) {
            int size = size1 - size2;
            while(size != 0) {
                stack1.pop();
                size--;
            }
        } else {
            int size = size2 - size1;
            while(size != 0) {
                stack2.pop();
                size--;
            }
        }

        //再一起走，直到栈顶元素相等
        while(!stack1.empty() && ! stack2.empty()) {
            if(stack1.peek() == stack2.peek()) {
                return stack1.pop();
            } else {
                stack1.pop();
                stack2.pop();
            }
        }
        return null;
    }
}
