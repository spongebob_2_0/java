package test2;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-04-20
 * Time: 3:34
 */

class TreeNode {
    private int val;
    TreeNode left;
    TreeNode right;
}
public class Test6 {
    public boolean isCompleteTree(TreeNode root){
        if(root==null){
            return true;    //空树也是完全二叉树
        }
        Queue<TreeNode> queue =new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            TreeNode cur = queue.poll();
            if(cur != null) {
                queue.offer(cur.left);
                queue.offer(cur.right);
            }else {
                break;
            }
        }
        while (!queue.isEmpty()) {
            TreeNode tmp = queue.poll();
            if(tmp != null) {
                return false;
            }
        }
        return true;
    }
    public static void main(String[] args) {

    }
}
