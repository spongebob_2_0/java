package com.example.demo;

import com.example.demo.entity.UserInfo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-24
 * Time: 17:32
 */
@RestController
@RequestMapping("/user")
public class UserController {

    //关于user session key
    private static final String SESSION_KEY_USERINFO = "SESSION_KEY_USERINFO";

    @RequestMapping("/save")
    public String save(HttpSession session) {
        //...经过一系列的判断
        UserInfo userInfo =new UserInfo();
        userInfo.setId(1);
        userInfo.setUsername("lisi");
        userInfo.setAge(18);
        session.setAttribute(SESSION_KEY_USERINFO,userInfo);
        return "ok";
    }

    @RequestMapping("/get")
    public Object get(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if(session != null && session.getAttribute(SESSION_KEY_USERINFO) != null) {
            return session.getAttribute(SESSION_KEY_USERINFO);
        }
        return "null";
    }
}
