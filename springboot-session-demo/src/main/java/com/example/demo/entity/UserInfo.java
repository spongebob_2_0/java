package com.example.demo.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-24
 * Time: 17:54
 */
@Data
public class UserInfo implements Serializable {
    private int id;
    private String username;
    private int age;
}
