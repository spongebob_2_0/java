package test2;

import java.util.*;

public class Main2 {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        String A =scanner.nextLine();
        String B =scanner.nextLine();
        int count = 0;
        for (int i = 0; i < A.length()+1; i++) {
            //分割字符串
            String a =A.substring(0,i);
            String b =A.substring(i,A.length());
            //拼接两个字符串
            StringBuffer sb =new StringBuffer();
            sb.append(a);
            sb.append(B);
            sb.append(b);
            if(func(sb.toString())){
                count++;
            }
        }
        System.out.println(count);
    }
    //判断是否是回文字符串
    public static boolean func(String str) {
        int left =0;
        int right =str.length()-1;
        while (left < right) {
            if(str.charAt(left) == str.charAt(right)) {
                left++;
                right--;
            }else {
                return false;
            }
        }
        return true;
    } 
}