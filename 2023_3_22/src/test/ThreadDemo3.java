package test;


import java.util.Timer;
import java.util.TimerTask;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-22
 * Time: 3:27
 */
public class ThreadDemo3 {
    public static void main(String[] args) {
        //定时器
        //Timer内置了线程(前台线程),会阻止进程结束
        Timer timer =new Timer();
        //TimerTask实现了Runnable接口,对Runnable进行了封装,本质上还是Runnable
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("玄武一号任务执行， 执行代号:闪电；  定时时间:3s");
            }
        },3000);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("朱雀二号任务执行， 执行代号:暴风；  定时时间:5s");
            }
        },5000);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("青龙三号任务执行， 执行代号:狂风；  定时时间:7s");
            }
        },7000);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {  //这个方法的执行是靠Timer内部的线程在时间到了之后执行的
                System.out.println("白虎四号任务执行， 执行代号:地震；  定时时间:10s");
            }
        },10000);
        System.out.println("主线程任务执行   未定时"); //主线程main直接执行
    }
}
