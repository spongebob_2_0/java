package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-22
 * Time: 2:44
 */
class MyBlockingQueue {
    private  int[] items =new int[1000];
    private volatile int head =0;  //头部
    private volatile int tail =0;  //尾部
    private volatile int size =0;  //队列长度

    //入队列
    synchronized public void put(int elem) throws InterruptedException {
            //此外为什么if换成while更合适,防止别的代码暗中调用interrupt方法,把wait提前唤醒了
            //明明还不满足唤醒条件(队列没满才或者非空才会被唤醒) ,导致提前被唤醒就行执行下面代码,会出错抛异常
            while (size == items.length) {   //判断队列是否为满,满了则不能插入
                this.wait();                 //由take方法里的notify来唤醒
            }
            items[tail] = elem;   //进行插入操作,将elem放到items里,放到tail所指向的位置
            tail++;
            if (tail == items.length) {
                tail = 0;
            }
            size++;
            this.notify();
    }

    //出队列,返回删除的元素内容
    synchronized public int take() throws InterruptedException {
            while (size == 0) {  //判断队列是否为空,为空则不能出队
                this.wait();    //由put方法里的notify来唤醒
            }
            int value = items[head];    //非空,取元素
            head++;
            if (head == items.length) {
                head = 0;
            }
            size--;
            this.notify();
            return value;
        }
}
public class ThreadDemo2 {
    public static void main(String[] args) {
        MyBlockingQueue queue =new MyBlockingQueue();
        //消费者
        Thread t1 =new Thread(() -> {
            while (true) {
                //取元素
                try {
                    int value = queue.take();
                    System.out.println("消费:"+value);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t1.start();
        //生产者
        Thread t2 =new Thread(() -> {
            int value =0;
            while (true) {
                try {
                    System.out.println("生产:" + value);
                    queue.put(value++);
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t2.start();
    }
}
