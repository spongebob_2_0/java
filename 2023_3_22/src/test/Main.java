package test;

import java.util.Scanner;
 
public class Main
{
    public static void main (String[] args)
    {
        Scanner s = new Scanner(System.in);
        while (s.hasNext())
        {
            int w = s.nextInt();
            int h = s.nextInt();
            int count = 0;
            int[][] arr = new int[w][h];//数组值默认为0
            for (int i=0; i<w; ++i)
            {
                for (int j=0; j<h; ++j)
                {
                    //只有值为0的位置可以放蛋糕
                    if (arr[i][j] == 0)
                    {
                        //count表示可以放的蛋糕数
                        count ++;
                        //加深if判断防止行下标越界
                        if (i+2 < w)
                        {
                            //此处不可以放蛋糕
                            arr[i+2][j] = -1;
                        }
                        //加上if判断防止列下标越界
                        if (j+2 < h)
                        {
                            //此处不可以放蛋糕
                            arr[i][j+2] = -1;
                        }
                    }
                }
            }
            System.out.println(count);
        }
    }
}