package test;

import java.util.PriorityQueue;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-22
 * Time: 3:59
 */
//表示一个任务
class MyTask implements Comparable<MyTask> {
    //这个是我们要执行任务
    public Runnable runnable;

    //为了方便判定,使用了绝对的时间戳
    public long time;

    public MyTask(Runnable runnable, long delay) {
        this.runnable =runnable;
        //取当前时刻的时间戳+delay,作为该任务实际执行的时间戳
        this.time =System.currentTimeMillis()+delay;    //实例化这个参数,,这个time就是系统时间加上延迟延迟时间
    }

    @Override
    public int compareTo(MyTask o) {
        //这样的写法表示每次取出的是最小元素
        return (int)(this.time - o.time);   //解决优先级阻塞队列的优先级问题
    }
}
class MyTimer {
    //创建一个锁对象
    private Object locker =new Object();

    //这个结构,带有优先级的阻塞队列,核心数据结构
    private PriorityBlockingQueue<MyTask> queue =new PriorityBlockingQueue<>();

    //此处的delay形如 3000 这样的数字(多长时间执行该任务)
    public void schedule(Runnable runnable,long delay) {
        //这个方法的作用就是用来实例化MyTask里面的参数的,然后把这个任务插入到优先级阻塞队列中
        MyTask myTask =new MyTask(runnable,delay);
        //每次添加任务用notify唤醒wait重新扫描任务
        queue.put(myTask);
        synchronized (locker) {
            locker.notify();
        }
    }

    //在这里构造扫描任务的线程,时间到了负责执行具体任务了
    public MyTimer() {
        Thread t =new Thread(() -> {
            while (true) {
                try {
                    synchronized (locker) {
                        //阻塞队列只有阻塞的入队列和阻塞的出队列,没有阻塞的查看首元素
                        //出队列,也就是取元素,如果当前队列为空就会发生阻塞,那么这个循环就会终止
                        //如果队列不为空,我们就能获取到这个元素
                        MyTask myTask = queue.take();

                        //获取系统时间
                        long curTime = System.currentTimeMillis();

                        //执行任务的时间小于系统时间就开始执行任务
                        if (myTask.time <= curTime) {
                            //时间到了,可以执行任务了
                            myTask.runnable.run();
                        } else {
                            //时间还没到
                            //把刚才取出的任务重新塞回到队列中
                            queue.put(myTask);   //入队列
                            //使用wait等待解决忙等问题,避免cpu浪费资源,同时释放掉锁
                                locker.wait(myTask.time - curTime);
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
    }
}
public class ThreadDemo4 {
    public static void main(String[] args) {
        MyTimer myTimer =new MyTimer();
        myTimer.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("玄武一号任务执行， 执行代号:闪电；  定时时间:10s");
            }
        },10000);
        myTimer.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("朱雀二号任务执行， 执行代号:暴风；  定时时间:7s");
            }
        },7000);
        myTimer.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("青龙三号任务执行， 执行代号:狂风；  定时时间:5s");
            }
        },5000);
        myTimer.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("白虎四号任务执行， 执行代号:地震；  定时时间:3s");
            }
        },3000);
        System.out.println("主线程任务执行   未定时"); //主线程main直接执行
    }
}
