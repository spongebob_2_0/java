package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-23
 * Time: 17:07
 */
class Outer {
    //成员变量
    private  int age =18;
    String name ="我是外部类的名字";
    //实例/成员内部类
    class Inter {
        String name ="我是内部类的名字";
        public void test() {
            //外部类和实例内部类具有相同名字的成员时,优先访问内部类自己的
            System.out.println(name);

            //如果想要访问外部类同名成员,必须:外部类名.this.同名成员名字
            System.out.println(Outer.this.name);

            //可以访问外部类的任意修饰符修饰的变量,包括私有的;
            System.out.println(age);
        }
    }
    // 外部类访问内部类的成员
    public void func() {
        Inter inter =new Inter();
        System.out.println(inter.name);
    }
}
public class Test1 {
    public static void main(String[] args) {
        //第一种方法先创建外部类对象,再通过外部类对象创建实例内部类对象
        Outer o =new Outer();
        Outer.Inter inter1 =o.new Inter();
        System.out.println(inter1.name);
        inter1.test();
        //第二种方法直接创建实例内部类对象
        Outer.Inter inter2 =new Outer().new Inter();
        System.out.println(inter2.name);
    }
}
