package test;

import sun.awt.windows.ThemeReader;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-22
 * Time: 2:24
 */
public class ThreadDemo1 {
    public static void main(String[] args) {
        BlockingQueue<Integer> blockingQueue =new LinkedBlockingDeque<>();
        //消费者
        Thread t1 =new Thread(() -> {
            while (true) {
                try {
                    int value =blockingQueue.take();
                    System.out.println("消费元素:"+value);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t1.start();
        //生产者
        Thread t2 =new Thread(() -> {
            int value =0;
            while (true) {
                try {
                    System.out.println("生产元素:"+value);
                    blockingQueue.put(value);
                    value++;
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t2.start();
    }
}
