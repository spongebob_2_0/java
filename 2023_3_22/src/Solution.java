import java.util.Scanner;

public class Solution {
    public static int StrToInt(String str) {
        if (str == null || str.isEmpty()) {
            return 0;
        }
        int len = str.length();
        int i = 0;
        int flag = 1;
        int sum = 0;
//        int boundary = Integer.MAX_VALUE / 10;
        while (str.charAt(i) == ' ') {
            if (++i == len) {
                return 0;
            }
        }
        if (str.charAt(i) == '-') {
            flag = -flag;
        }
        if (str.charAt(i) == '+' || str.charAt(i) == '-') {
            i++;
        }
        for (; i < len; i++) {
            char ch = str.charAt(i);
            if (ch < '0' || ch > '9') {
                return 0;
            } else {
                int c = ch - '0';
//                if (sum > boundary || sum == boundary && ch > '7') {
//                    return flag == -1 ? Integer.MIN_VALUE : Integer.MAX_VALUE;
//                }
                sum = sum * 10 + c;
            }
        }
        return sum * flag;
    }

    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        String str = scanner.nextLine();
        System.out.println(StrToInt(str));
    }
}
