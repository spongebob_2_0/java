package test3;

import java.util.concurrent.PriorityBlockingQueue;

class Task1 implements Comparable<Task1>{
    //Runnable类中有一个run方法， 通过这个方法实现任务的执行
    private Runnable command;
    //time表示执行的时间
    public long time;

    //构造方法
    public Task1(Runnable command, long time) {
        this.command = command;
        this.time = System.currentTimeMillis() + time;  //将时间转化为绝对时间
    }

    //执行任务的逻辑
    public void run() {
        command.run();
    }

    //定义比较方法 - 方便后续的优先级队列构建

    @Override
    public int compareTo(Task1 o) {
        return (int)(this.time - o.time);
    }
}

//检测线程， 继承Thread类，重写内部run方法，属于线程的创建方法之一。
     class Worker extends Thread {
    	//优先级队列 - JUC包里面
        private PriorityBlockingQueue<Task1> queue = null;
        //为了对监管线程进行阻塞和唤醒，采用同一对象
        private Object mailBox = null;
		
		//构造函数
        public Worker(PriorityBlockingQueue<Task1> queue, Object mailBox) {
            this.queue = queue;
            this.mailBox = mailBox;
        }

        @Override
        public void run() {
            //实现具体的执行逻辑
            while(true) {
                try {
                    synchronized (mailBox) {
                        //1、取优先级队列的队首元素
                        Task1 task1 = queue.peek();
                        //2、比较队首的元素的时间是否大于当前时间
                        if (task1 == null) {
                            continue;
                        }
                        long curTime = System.currentTimeMillis();
                        if (task1.time > curTime) {
                            //时间还没有到， 由于取出了任务， 需要重新放置回去
                            //优化1： 空循环等待 - wait(time) 让线程休眠time时间，然后在执行
                            //       如果在等待期间有新的任务添加， 这个时候我们唤醒线程， 继续判断（因为存在新的时间过短需要立即执行）
                            //       这个只需要添加一个新任务时， 唤醒即可
                            //优化2： 访问队首元素而不是取出， 防止无所谓的删除、插入。（维护优先级队列是有消耗的）
                            long gapTime = task1.time - curTime;
                            mailBox.wait(gapTime);
                        } else {
                            //直接执行
                            //如果执行到了， 则会删除头部元素， 调用任务的执行过程。
                            task1 = queue.take();
                            task1.run();
                        }
                    }
                }
                catch(InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
            }
        }
    }

    //定时器简单实现
     class Timer {
        //定时器的实现步骤
        //1、用一个类描述任务
        //2、用优先级队列管理这些任务， 比较方法通过任务的制定时间，每次取队首元素
        //   队首元素是执行时间最近的
        private PriorityBlockingQueue<Task1> queue = new PriorityBlockingQueue<>();
        //3、用一个线程来循环扫描当前的阻塞队列，判断队首的执行时间， 如果执行时间到了，那就执行。

        //4、创建一个Object对象，用于设置线程阻塞使用的， 存在线程阻塞， 添加任务时唤醒的操作
        private Object locker1 = new Object();

        //构造函数
        public Timer() {
            //创建线程
            Worker worker = new Worker(queue, locker1);
            worker.start();
        }


        //4、提供一个方法， 让调用者能够把任务安排起来
        public void schedule(Runnable command, long time) {
            Task1 task1 = new Task1(command, time);
            queue.put(task1);
            synchronized (locker1) {
                locker1.notify();
            }
        }
    }
    public class Demo1 {
        public static void main(String[] args) {
            Timer timer = new Timer();
            timer.schedule(new Runnable() {
                @Override
                public void run() {
                    System.out.println("郝梦武一号任务执行， 执行代号:闪电；  定时时间:2s");
                }
            }, 40000);

            timer.schedule(new Runnable() {
                @Override
                public void run() {
                    System.out.println("郝梦武二号任务执行， 执行代号:暴风；  定时时间:5s");
                }
            }, 5000);

            timer.schedule(new Runnable() {
                @Override
                public void run() {
                    System.out.println("郝梦武三号任务执行， 执行代号:狂风；  定时时间:7s");
                }
            }, 7000);

            timer.schedule(new Runnable() {
                @Override
                public void run() {
                    System.out.println("郝梦武三号任务执行， 执行代号:地震；  定时时间:10s");
                }
            }, 10000);
        }

    }
