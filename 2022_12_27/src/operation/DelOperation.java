package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-27
 * Time: 15:20
 */
public class DelOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("删除图书!");
        Scanner scanner =new Scanner(System.in);
        System.out.print("请输入要删除的书名:");
        String name = scanner.nextLine();
        int n = bookList.getUsedSize();
        for (int i = 0; i < n; i++) {
            Book book =bookList.getBook(i);
            if(book.getName().equals(name)){
                for (int j = i; j < n-1; j++) {
                    bookList.setBook(j,bookList.getBook(j+1));
                }
                bookList.setUsedSize(n-1);
                bookList.setBook(n-1,null);
                System.out.println("删除成功");
                return;
            }
        }
        System.out.println("找不到此书!");
    }
}
