package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-27
 * Time: 15:21
 */
public class ReturnOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("归还图书!");
        Scanner scanner =new Scanner(System.in);
        System.out.print("请输入要归还的书名:");
        String name = scanner.nextLine();
        int n = bookList.getUsedSize();
        for (int i = 0; i < n; i++) {
            Book book =bookList.getBook(i);
            if(book.getName().equals(name)){
                if(book.isBorrowed()){
                    book.setBorrowed(false);
                    System.out.println("归还成功!");
                }else {
                    System.out.println("该书未被借阅!");
                }
                return;
            }
        }
        System.out.println("找不到此书!");
    }
}
