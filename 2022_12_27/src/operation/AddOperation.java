package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-27
 * Time: 15:20
 */
public class AddOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("添加图书!");
        Scanner scanner =new Scanner(System.in);
        System.out.println("请输入书名");
        String name = scanner.nextLine();
        System.out.println("请输入作者");
        String author = scanner.nextLine();
        System.out.println("请输入价格");
        int price = scanner.nextInt();
        System.out.println("请输入类型");
        String type =scanner.next();
        Book book =new Book(name,author,price,type);
        int n =bookList.getUsedSize();
        bookList.setBook(n,book);
        bookList.setUsedSize(n+1);
        System.out.println("添加成功!");
    }
}
