package operation;

import book.BookList;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-27
 * Time: 15:20
 */
public class ShowOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("显示所有图书!");
        int n =bookList.getUsedSize();
        for (int i = 0; i < n; i++) {
            System.out.println(bookList.getBook(i));
        }
    }
}
