package operation;

import book.BookList;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-27
 * Time: 15:28
 */
public interface IOperation {
    public abstract void work(BookList bookList);
}
