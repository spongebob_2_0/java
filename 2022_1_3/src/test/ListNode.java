package test;

public class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
  }
 
class Solution {
    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        ListNode newHead =new ListNode();
        ListNode tmp =newHead;
        while(list1 !=null && list2!=null){
            if(list1.val <list2.val){
                tmp.next =list1;
                list1 =list1.next;
                tmp =tmp.next;
            }else{
                tmp.next =list2;
                list2 =list2.next;
                tmp =tmp.next;
            }
        }
        if(list1 !=null){
            tmp.next =list1;
        }
        if(list2 !=null){
            tmp.next =list2;
        }
        return newHead.next;
    }
}