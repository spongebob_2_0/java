package com.scholar.querytest;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-02-28
 */
public class Test1 {
	public static void main(String[] args) {
		int[] arr = new int[100000];
		for(int i = 0;i < arr.length;i++){
			arr[i] = i + 1;
		}
		long start = System.currentTimeMillis();
		for(int j = 1; j<=100000;j++){
			int temp = j;
			for(int i = 0;i < arr.length;i++){
				if(temp == arr[i]){
					break;
				}
			}
		}
		long end = System.currentTimeMillis();
		System.out.println("time： " + (end - start)); //time： 耗时823ms
	}

}
