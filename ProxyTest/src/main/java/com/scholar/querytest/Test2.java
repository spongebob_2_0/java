package com.scholar.querytest;

import java.util.HashSet;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-02-28
 */
public class Test2 {
	public static void main(String[] args) {
		HashSet<Integer> set = new HashSet<>(100000);
		for(int i = 0;i < 100000;i++){
			set.add(i + 1);
		}
		long start = System.currentTimeMillis();
		for(int j = 1; j<=100000;j++) {
			int temp = j;
			boolean contains = set.contains(temp);
		}
		long end = System.currentTimeMillis();
		System.out.println("time： " + (end - start)); //time： 耗时5ms
	}
}
