package com.scholar.extend;

// 定义一个Tank类(被代理类)
public class Tank {
    public void move() {
        System.out.println("Tank moving cla....");
    }

    public static void main(String[] args) {
        // 创建代理对象调用move方法
        new ProxyTank().move();
    }
}

// 通过继承Tank类来创建一个代理类ProxyTank
class ProxyTank extends Tank {
    // 重写move方法以实现代理功能
    @Override
    public void move() {
        System.out.println("方法执行前...");
        // 调用父类（Tank类）的move方法，即被代理的方法
        super.move();
        System.out.println("方法执行后...");
    }
}
