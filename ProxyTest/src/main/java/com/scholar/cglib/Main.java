package com.scholar.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import java.lang.reflect.Method;

// 被代理的类
class Tank {
    public void move() {
        System.out.println("Tank moving clacla....");
    }
}

public class Main {
    public static void main(String[] args) {
        Enhancer enhancer = new Enhancer(); // 创建Enhancer对象，用于生成代理类
        enhancer.setSuperclass(Tank.class); // 设置代理类的父类，即指定被代理类
        enhancer.setCallback(new TimeMethodInterceptor()); // 设置回调（方法拦截器）
        Tank proxy = (Tank)enhancer.create();  // 生成代理实例
        proxy.move();  // 调用代理实例的方法，会触发MethodInterceptor的intercept方法
    }
}

// 实现MethodInterceptor接口，自定义方法拦截逻辑
class TimeMethodInterceptor implements MethodInterceptor{
    @Override
    public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        // obj：代理类实例
        // method：被代理的方法
        // args：方法参数
        // proxy：用于调用super（父类）版本的方法
        System.out.println("生成的类名: " + obj.getClass().getName());
        System.out.println("生成的类的父类: " + obj.getClass().getSuperclass().getName());
        System.out.println("方法执行前，被代理的方法: " + method.getName());
        
        // 调用父类的方法
        Object result = proxy.invokeSuper(obj, args);
        
        System.out.println("方法执行后，被代理的方法: " + method.getName());
        
        return result; // 返回方法执行结果
    }
}

