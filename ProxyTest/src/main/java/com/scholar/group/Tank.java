package com.scholar.group;

// 定义一个Movable接口,起到一个规范作用。
interface Movable {
    void move();
}

// 定义一个Tank类（被代理类）
public class Tank implements Movable {
    @Override
    public void move() {
        System.out.println("Tank moving cla....");
    }

    public static void main(String[] args) {
        // 实例化被代理类对象
        Tank tank = new Tank();
        // 通过代理对象调用move方法
        new LogProxy(tank).move();
    }
}

// 定义一个LogProxy类，它也实现了Movable接口，用于代理所有实现了Movable接口的类。
class LogProxy implements Movable {
    // 被代理类对象的引用
    private Movable movable;

    // 构造函数，实例化被代理对象
    public LogProxy(Movable movable) {
        this.movable = movable;
    }

    // 实现接口中的move方法
    @Override
    public void move() {
        System.out.println("方法执行前....");
        // 调用持有对象的move方法，即被代理的方法
        movable.move();
        System.out.println("方法执行后....");
    }
}
