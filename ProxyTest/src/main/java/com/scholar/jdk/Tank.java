package com.scholar.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 一个接口，定义一个 move 方法。
 */
interface Movable {
    void move();
}

/**
 * 使用jdk的动态代理。
 * Tank是被代理的实际对象
 */
public class Tank implements Movable {
    @Override
    public void move() {
        System.out.println("Tank moving cla....");
    }

    public static void main(String[] args) {
        Tank tank = new Tank();
        // 利用 JDK 的 Proxy 类创建代理对象
        // 参数一: 类加载器，用于加载代理对象
        // 参数二: 要实现的接口数组，代理对象将实现这些接口
        // 参数三: 调用处理器，定义代理对象调用方法时的具体行为(传入InvocationHandler的实现类)
        Movable o = (Movable) Proxy.newProxyInstance(
                Tank.class.getClassLoader(),
                new Class[]{Movable.class},
                new LogProxy(tank));
        o.move();  // 调用代理对象的 move 方法
    }
}

/**
 * 调用处理器实现。
 */
class LogProxy implements InvocationHandler {
    private Movable movable;  // 被代理的对象，即原始对象

    public LogProxy(Movable movable) {
        this.movable = movable;
    }

	 
    // proxy：代理对象本身，大多数情况下都不使用它
    // method：正在被调用的方法的反射对象
    // args：调用方法时传递的参数数组。如果接口方法不接受参数，则为null
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        // 在原始方法调用前打印日志
        System.out.println("方法:" + method.getName() + "()执行前");
        // 调用原始对象的方法
        Object result = method.invoke(movable, args);  // 此处相当于调用 movable.move()
        // 在原始方法调用后打印日志
        System.out.println("方法:" + method.getName() + "()执行后");
        return result;
    }
}
