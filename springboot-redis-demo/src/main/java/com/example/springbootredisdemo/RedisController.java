package com.example.springbootredisdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-24
 * Time: 16:26
 */
@RestController
public class RedisController {

    @Autowired
    private RedisTemplate redisTemplate;
    //在redis中存储数据
    @RequestMapping("/save")
    public String save() {
        redisTemplate.opsForValue().set("userinfo","zhangsan");
        return "ok";
    }
    //在redis中读取数据
    @RequestMapping("/get")
    public Object get() {
        return redisTemplate.opsForValue().get("userinfo");
    }

    //在redis中存储数据
    @RequestMapping("/save2")
    public String save2() {
        //myhash是哈希表的名字，然后username是该哈希表内的一个字段，它的值是lisi
        redisTemplate.opsForHash().put("myhash","username","lisi");
        return "ok";
    }
    //在redis中读取数据
    @RequestMapping("/get2")
    public Object get2() {
        return redisTemplate.opsForHash().get("myhash","username");
    }
}
