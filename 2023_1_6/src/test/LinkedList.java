package test;


// 2、无头双向链表实现
 public class LinkedList {
    class  ListNode{
        public int val;
        public ListNode prev;
        public ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }

    public ListNode head;
    public ListNode last;
     //头插法
     public void addFirst(int data){
         ListNode node =new ListNode(data);
         if(head == null){
             head =node;
             last =node;
         }else{
             node.next =head;
             head.prev =node;
             head =node;
         }
     }
     //尾插法
     public void addLast(int data){
         ListNode node =new ListNode(data);
         if(head == null){
             head =node;
             last =node;
         }else {
             last.next =node;
             node.prev =last;
             last=node;
         }
     }
     //任意位置插入,第一个数据节点为0号下标
     public void addIndex(int index,int data){
         if(index <0 || index > size()){
             return;
         }
         if(index == 0){
             addFirst(data);
             return;
         }
         if(index ==size()){
             addLast(data);
             return;
         }
         ListNode cur =findIndex(index);
         ListNode node =new ListNode(data);
         node.next =cur;
         cur.prev.next =node;
         node.prev=cur.prev;
         cur.prev=node;
     }
     //寻找index位置
     private ListNode findIndex(int index){
         ListNode cur =head;
         while (cur !=null){
             cur =cur.next;
             index--;
         }
         return cur;
     }
     //查找是否包含关键字key是否在单链表当中
     public boolean contains(int key){
         ListNode cur =head;
         while (cur != null){
             if(cur.val ==key){
                 return true;
             }
             cur =cur.next;
         }
         return false;
     }
     //删除第一次出现关键字为key的节点
     public void remove(int key){
         ListNode cur =head;
         while (cur !=null){
             if(cur.val ==key){
                 if(cur == head){
                     head =head.next;
                     if(head != null){
                         head.prev =null;
                     }
                 }else {
                     cur.prev.next =cur.next;
                     if(cur.next != null){
                         cur.next.prev =cur.prev;
                     }else {
                         last =last.prev;
                     }
                 }
                 return;
             }
             cur =cur.next;
         }
     }
     //删除所有值为key的节点
     public void removeAllKey(int key){
         ListNode cur =head;
         while (cur !=null){
             if(cur.val ==key){
                 if(cur == head){
                     head =head.next;
                     if(head != null){
                         head.prev =null;
                     }
                 }else {
                     cur.prev.next =cur.next;
                     if(cur.next != null){
                         cur.next.prev =cur.prev;
                     }else {
                         last =last.prev;
                     }
                 }
             }
             cur =cur.next;
         }
     }
     //得到单链表的长度
     public int size(){
         int len =0;
         ListNode cur =head;
         while (cur != null){
             len++;
             cur =cur.next;
         }
         return len;
     }
     public void display(){
         ListNode cur =head;
         while (cur != null){
             System.out.print(cur.val+" ");
         }
         System.out.println();
     }
     public void clear(){
         ListNode cur =head;
         while (cur != null){
             ListNode curNext =cur.next;
             cur.prev =null;
             cur.next =null;
             cur =curNext;
         }
         head =null;
         last =null;
     }
 }
