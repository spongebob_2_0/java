package test;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-01-06
 * Time: 22:44
 */
public class Test2 {
    public static String func(String str){
        StringBuilder stringBuilder =new StringBuilder();
        int count =1;
        char[] array =str.toCharArray();
        for (int i = 0; i < array.length-1; i++) {
            if(array[i] ==array[i+1]){
                count++;
            }else {
                stringBuilder.append(array[i]);
                stringBuilder.append(count);
                count =1;
            }
        }
        if(array[array.length-2] == array[array.length-1]){
            stringBuilder.append(array[array.length-1]);
            stringBuilder.append(count);
        }else {
            stringBuilder.append(array[array.length-1]);
            stringBuilder.append(count);

        }
        return stringBuilder.toString();
    }
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        while ((scanner.hasNextLine())) {
            String str = scanner.nextLine();
//        String str ="aabbccdaa";
            System.out.println(func(str));
        }
    }
}
