package test;

import java.util.Deque;
import java.util.LinkedList;

public class TreeNode {
      int val;
      TreeNode right;
      TreeNode left;
      TreeNode(int x) {
      val = x; }
 }
class Solution {
    public Integer lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        Deque<Integer> stack1 =new LinkedList<>();
        getPath(root,p,stack1);
        Deque<Integer> stack2 =new LinkedList<>();
        getPath(root,q,stack2);

        int size1 =stack1.size();
        int size2 =stack2.size();
        if(size1 > size2){
            int size =size1 -size2;
            while(size != 0){
                stack1.pop();
                size--;
            }
        }else{
            int size =size2 -size1;
            while(size != 0){
                stack2.pop();
                size--;
            }
        }
        while(!stack1.isEmpty() && !stack2.isEmpty()){
            if(stack1.peek() != stack2.peek()){
                stack1.pop();
                stack2.pop();
            }else{
                return stack1.peek();
            }
            return null;
        }
        return null;
    }
    public boolean getPath(TreeNode root,TreeNode node,Deque<Integer> stack){
        if(root == null || node == null){
            return false;
        }
        if(root == node){
            return true;
        }
        boolean ret1 =getPath(root.left,node,stack);
        boolean ret2 =getPath(root.right,node,stack);
        if(ret1){
            return true;
        }
        if(ret2){
            return true;
        }
        stack.pop();
        return true;
    }
}