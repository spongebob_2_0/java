/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-11
 * Time: 5:50
 */
public interface Person {
    public abstract void eat();
}
