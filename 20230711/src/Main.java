import java.util.*;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s1 = "They are student.";
        String[] split = s1.split(" ");
        int start = 0;
        int end = split.length-1;
        while (start < end) {
            String tmp = split[start];
            split[start] = split[end];
            split[end] = tmp;
            start++;
            end--;
        }
        StringBuilder sb = new StringBuilder();
        for(String s: split) {
            sb.append(s+" ");
        }
        System.out.println(sb.toString());
        Map<Integer,Integer> map = new HashMap<>();
        map.getOrDefault(1,0);
    }
}