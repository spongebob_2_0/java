import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-11
 * Time: 8:04
 */
public class Main2 {
    public static void main(String[] args) {
        func(1234);
    }

    private static void func(int n) {
        if(n <= 0) {
            return;
        }
        func(n/10);
        System.out.print(n%10+" ");
    }
}
