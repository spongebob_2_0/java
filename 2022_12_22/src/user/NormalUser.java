package user;

import operation.*;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-22
 * Time: 14:56
 */
public class NormalUser extends User{
    public NormalUser(String name) {
        super(name);
        this.iOperations =new IOperation[]{
                new ExitOperation(),
                new FindOperation(),
                new BorrowOperation(),
                new ReturnOperation()
        };
    }

    @Override
    public int menu() {
        System.out.println("==========================================");
        System.out.println("尊贵的用户-"+this.name+",欢迎使用本图书管理系统");
        System.out.println("1.查找图书");
        System.out.println("2.借阅图书");
        System.out.println("3.归还图书");
        System.out.println("0.退出系统");
        System.out.println("==========================================");
        System.out.println("请输入你的操作:");
        Scanner scanner =new Scanner(System.in);
        int choice = scanner.nextInt();
        return choice;
    }
}
