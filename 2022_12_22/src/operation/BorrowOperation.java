package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-22
 * Time: 14:57
 */
public class BorrowOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("借阅图书!");
        Scanner scanner =new Scanner(System.in);
        System.out.print("请输入你要借阅的图书:");
        String name = scanner.nextLine();
        int n= bookList.getUsedSize();
        for (int i = 0; i < n; i++) {
            Book book =bookList.getBook(i);
            if(book.getName().equals(name)){
                if(!book.isBorrowed()){
                    book.setBorrowed(true);
                    System.out.println("借阅成功!");
                }else{
                    System.out.println("该图书已被借阅!");
                }
                return;
            }
        }
        System.out.println("找不到此书!");
    }
}
