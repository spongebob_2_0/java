package operation;

import book.BookList;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-22
 * Time: 14:56
 */
public interface IOperation {
    public abstract void work(BookList bookList);
}
