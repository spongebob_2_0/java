package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-22
 * Time: 14:56
 */
public class FindOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("查找图书!");
        Scanner scanner =new Scanner(System.in);
        System.out.print("请输入你要查找的图书:");
        String name = scanner.nextLine();
        int n= bookList.getUsedSize();
        for (int i = 0; i < n; i++) {
            Book book =bookList.getBook(i);
            if(book.getName().equals(name)){
                System.out.println("找到该图书:");
                System.out.println(book);
                return;
            }
        }
        System.out.println("找不到此书!");
    }
}
