package demo;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-22
 * Time: 20:10
 */
public class Test3 {
    //for循环求解斐波那契数列
    public static int fib(int n){
        if(n<=2){
            return 1;
        }
        int f1=1;
        int f2=1;
        int f3 =0;
        for (int i = 3; i <= n; i++) {
            f3 =f1+f2;
            f1 =f2;
            f2 =f3;
        }
        return f3;
    }
    //递归求斐波那契数列
    public  static int fib1(int n){
        if(n<=2){
            return 1;
        }
        return fib1(n-1)+fib1(n-2);
    }
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        while (scanner.hasNext()){
            int n = scanner.nextInt();
            long start =System.currentTimeMillis();
            System.out.println(fib1(n));
            long end =System.currentTimeMillis();
            System.out.println(end-start);
            System.out.println("=============");
            long start1 =System.currentTimeMillis();
            System.out.println(fib(n));
            long end1 =System.currentTimeMillis();
            System.out.println(end1-start1);
        }
    }
}
