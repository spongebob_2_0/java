package demo;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-22
 * Time: 20:40
 */
public class Test4 {
    public static void func(int n){
        if(n<10){
            System.out.print(n+" ");
            return;
        }
        func(n/10);
        System.out.print(n%10+" ");
    }
    public static int func1(int n){
        if(n<10){
            return 1;
        }
        return n%10+func1(n/10);
    }
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int n = scanner.nextInt();
        /*while (n!=0){
            System.out.print(n%10+" ");
            n /=10;
        }*/
//        func(n);
        int sum=0;
        int ret =n;
        while (ret!=0){
            sum +=ret%10;
            ret /=10;
        }
        System.out.println(sum);
        System.out.println("========");
        System.out.println(func1(n));
    }
}
