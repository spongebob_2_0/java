package demo;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-22
 * Time: 20:57
 */
public class Test6 {
    public static void hannto(int n,char pos1,char pos2,char pos3){
        if(n==1){
            System.out.print("将第"+n+"个盘子从"+pos1+"->"+pos3+" ");
            return;
        }
        hannto(n-1,pos1,pos3,pos2);
        System.out.print("将第"+n+"个盘子从"+pos1+"->"+pos3+" ");
        hannto(n-1,pos2,pos1,pos3);
    }
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        while (scanner.hasNext()){
            int n = scanner.nextInt();
            hannto(n,'A','B','C');
        }
    }
}
