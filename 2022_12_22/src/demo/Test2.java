package demo;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-22
 * Time: 20:03
 */
public class Test2 {
    public static int func(int n){
        if(n==1){
            return 1;
        }
        return n*func(n-1);
    }
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        while (scanner.hasNext()){
            int n = scanner.nextInt();
            int sum =0;
            for (int i = 1; i <= n; i++) {
                sum +=func(i);
            }
            System.out.println(sum);
        }
    }
}
