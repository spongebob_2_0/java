package book;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-22
 * Time: 14:55
 */
public class BookList {
    private Book[] books =new Book[10];  //存10本书
    private int usedSize;   //默认0本书

    public BookList() {
        books[0] =new Book("三国演义","罗贯中",56,"小说");
        books[1] =new Book("红楼梦","曹雪芹",88,"小说");
        books[2] =new Book("水浒传","施耐庵",99,"小说");
        this.usedSize =3;
    }

    public Book getBook(int pos) {
        return books[pos];
    }

    public Book setBook(int pos,Book book) {
        return books[pos] =book;
    }

    public int getUsedSize() {
        return usedSize;
    }

    public void setUsedSize(int usedSize) {
        this.usedSize = usedSize;
    }
}
