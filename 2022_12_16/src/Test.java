import java.util.Arrays;
 
public class Test {
	public static void main(String[] args) {
		int[] nums = {1, 4, 5, 3, 6, 7, 10};
		exchange(nums);
		System.out.println(Arrays.toString(nums));
	}
	
	public static void exchange(int[] nums) {
		int i = 0, j = nums.length - 1;
			while(i < j) {
				while(nums[i] % 2 == 0) {
				i++;
			}
			while(nums[j] % 2 == 1) {
				j--;
			}
			if (i < j) {
				swap(nums, i, j);
			}
		}
    }
 
	public static void swap(int[] nums, int i, int j) {
		// 交换数组中两个元素的值
		nums[i] = nums[i] ^ nums[j];
		nums[j] = nums[i] ^ nums[j];
		nums[i] = nums[i] ^ nums[j];
	}
}