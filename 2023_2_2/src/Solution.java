import java.util.ArrayList;
import java.util.Stack;

public class Solution {
    public boolean IsPopOrder(int [] pushA, int [] popA) {
        Stack<Integer> stack =new Stack<>();
        int j = 0;
        for (int i = 0; i < pushA.length; i++) {
            stack.push(pushA[i]);
            while (i < popA.length && !stack.empty() && stack.peek().equals(popA[j])){
                    stack.pop();
                    j++;
            }
        }
        return stack.empty();
    }
}