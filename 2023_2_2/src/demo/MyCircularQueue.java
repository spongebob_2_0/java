package demo;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-02-03
 * Time: 13:05
 */
public class MyCircularQueue {
    public int[] elem;
    public int front;  //表示队列的头
    public int rear;    //表示队列的尾

    public MyCircularQueue(int k) {
        this.elem = new int[k+1];
    }

    //入队列
    public boolean enQueue(int value){
        if(isFull()){
            return false;
        }
        elem[rear] =value;
        rear =(rear+1) %elem.length;
        return true;
    }

    public boolean isFull() {
        return (rear+1) % elem.length ==front;
    }
    //出队列
    public boolean deQueue(){
        if(isEmpty()){
            return false;
        }
        front =(front+1) %elem.length;
        return true;
    }
    //得到队头元素
    public int Front(){
        if(isEmpty(){
            return -1;
        }
        return elem[front];
    }

    //得到队尾元素
    public int Rear(){
        if(isEmpty()){
            return -1;
        }
        int index =(rear == 0)?elem.length-1:rear-1;
        return elem[index];
    }
    public boolean isEmpty(){
        return front == rear;
    }
}
