import java.util.*;
import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-02-03
 * Time: 11:06
 */
public class Test {
    public static void main1(String[] args) {
        MyQueue myQueue =new MyQueue();

        myQueue.offer(1);
        myQueue.offer(2);
        myQueue.offer(3);
        System.out.println(myQueue.peek());
        System.out.println(myQueue.poll());
    }

    public static void main(String[] args) {
        Deque<Integer> deque =new LinkedList<>(); //双端队列
        Queue<Integer> queue =new LinkedList<>(); //普通队列
        LinkedList<Integer> stack =new LinkedList<>();
        List<Integer> list =new LinkedList<>();
    }
}
