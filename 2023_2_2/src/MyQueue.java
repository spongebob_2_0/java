/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-02-03
 * Time: 10:55
 */
public class MyQueue {
    static class Node{
        public int val;
        public Node next;
        public Node(int val){
            this.val =val;
        }

        public Node() {
        }
    }
    public Node head;
    public Node last;
    public int usedSize;

    //入队
    public void offer(int val){
        Node node =new Node(val);
        if(head == null){
            head =node;
            last =node;
        }else{
            last.next =node;
            last =node;
        }
        usedSize++;
    }

    public boolean empty(){
        return usedSize == 0;
    }
    //出队列
    public int poll(){
        if(empty()){
            throw new EmptyException("队列为空");
        }
        int val = head.val;;
        head =head.next;
        if(head == null){
            last =null;
            usedSize--;
        }
        return val;
    }

    //获取队头元素
    public int peek(){
        if(empty()){
            throw new EmptyException("队列为空");
        }
        return head.val;
    }

    public int getUsedSize(){
        return usedSize;
    }
}
