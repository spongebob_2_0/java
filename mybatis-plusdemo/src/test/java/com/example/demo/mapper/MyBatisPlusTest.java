package com.example.demo.mapper;

import com.example.demo.entity.Userinfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootTest
public class MyBatisPlusTest {
    @Autowired
    private UserMapper userMapper;


    /**
     * 插入
     */
    @Test
    void testSave() {
        Userinfo userinfo = new Userinfo();
        userinfo.setUsername("小甜甜");
        userinfo.setPassword("123");
        userinfo.setState(1);
        userinfo.setCreatetime(LocalDateTime.now());
        int result = userMapper.insert(userinfo);
        System.out.println(result);
    }

    @Test
    void testDelete() {
        userMapper.deleteById(12);
    }

    /**
     * 测试查询所有数据
     */
    @Test
    void testSelectList(){
        //通过条件构造器查询一个list集合，若没有条件，则可以设置null为参数
        List<Userinfo> userinfos = userMapper.selectList(null);
        userinfos.forEach(System.out::println);
    }

}
