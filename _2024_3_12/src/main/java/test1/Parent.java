package test1;

class Parent {
    static {
        System.out.println("1. 父类的静态代码块");
    }
    
    {
        System.out.println("3. 父类的实例代码块");
    }
    
    public Parent() {
        System.out.println("4. 父类的构造方法");
    }
}

class Child extends Parent {
    static {
        System.out.println("2. 子类的静态代码块");
    }
    
    {
        System.out.println("5. 子类的实例代码块");
    }
    
    public Child() {
        System.out.println("6. 子类的构造方法");
    }
    
    public static void main(String[] args) {
        System.out.println("=== 第一次实例化子类 ===");
        new Child();
        System.out.println("\n=== 第二次实例化子类 ===");
        new Child();
    }
}