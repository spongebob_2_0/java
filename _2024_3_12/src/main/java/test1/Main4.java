package test1;

import java.util.Scanner;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-03-12
 */
public class Main4 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int rechargeAmount = scanner.nextInt();
		int packagePrice = scanner.nextInt();
		int n = scanner.nextInt();  // 优惠政策的数量
		int[][] policies = new int[n][3];
		for (int i = 0; i < n; i++) {
			policies[i][0] = scanner.nextInt(); // 最小购买时长
			policies[i][1] = scanner.nextInt(); // 是否多买多送
			policies[i][2] = scanner.nextInt(); // 赠送时长
		}
		System.out.println(MaxViewingMonths(rechargeAmount,packagePrice,policies));
	}
    // 计算最大的观看月数
	private static int MaxViewingMonths(int rechargeAmount, int packagePrice, int[][] policies) {
		int baseMonths = rechargeAmount / packagePrice; // 基础订阅月数
		int maxMonths = baseMonths; // 初始假设的最大月数

		// 遍历每个优化政策
		for (int[] policy : policies) {
			int minPurchase = policy[0];
			int multipleBenefit = policy[1];
			int extraMonths = policy[2];

			// 计算政策成本和能够使用该政策的最大次数
			int policyCost = minPurchase * packagePrice;
			int maxUses = rechargeAmount / policyCost;

			// 处理多买多送政策
			if(multipleBenefit == 1) {
				for(int uses = 1; uses <= maxUses; uses++) {
					int totalMonths = uses * (minPurchase + extraMonths)
							+ (rechargeAmount - uses * policyCost) / packagePrice;
					maxMonths = Math.max(maxMonths,totalMonths);
				}
			}else if(maxUses >=1) {
				// 只能使用一次的政策
				int totalMonths = minPurchase + extraMonths + (rechargeAmount - policyCost) / packagePrice;
				maxMonths = Math.max(maxMonths,totalMonths);
			}
		}
		return maxMonths;
	}
}
