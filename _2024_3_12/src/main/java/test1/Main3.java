package test1;

import java.util.Scanner;

/**
 * Description
 * 求最小速度
 * @author: WuYimin
 * Date: 2024-03-12
 */
public class Main3 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String[] goodsStr = scanner.next().split(",");

		int H = scanner.nextInt();
		int[] goods = new int[goodsStr.length];
		for (int i = 0; i < goods.length; i++) {
			goods[i] = Integer.parseInt(goodsStr[i]);
		}
		System.out.println(findMinSpeed(goods,H));

	}

	private static int findMinSpeed(int[] goods, int h) {
		int left = 1; // 最低速度
		int right = 0; // 最高速度
		for (int weight : goods) {
			right += weight;
		}

		while (left <right) {
			int mid = left + (right - left) /2;
			if(canFinsh(goods,h,mid)) {
				right = mid;
			}else {
				left = mid+1;
			}
		}
		return left;
	}

	private static boolean canFinsh(int[] goods, int h, int speed) {
		int time = 0;
		for (int weight : goods) {
			time += (weight + speed -1) / speed;  // 向上取整计算每批货物需要的时间
		}
		return time <=h;
	}
}
