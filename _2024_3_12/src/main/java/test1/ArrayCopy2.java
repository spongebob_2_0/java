package test1;

import java.util.Scanner;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-03-19
 */
public class ArrayCopy2 {
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		int[] arr = {1, 2, 3}; // 初始数组

		do {
			// 扩容操作
			int[] arrNew = new int[arr.length + 1];
			for (int i = 0; i < arr.length; i++) {
				arrNew[i] = arr[i];
			}

			// 添加新元素
			System.out.println("请输入你要添加的元素");
			int addNum = myScanner.nextInt();
			arrNew[arrNew.length - 1] = addNum; // 新元素放在数组最后
			arr = arrNew; // 更新原数组引用

			// 输出新数组情况
			System.out.println("====arr扩容后元素情况====");
			for (int i = 0; i < arr.length; i++) {
				System.out.print(arr[i] + "\t");
			}

			// 用户决定是否继续
			System.out.println("\n是否继续添加 y/n");
			char key = myScanner.next().charAt(0);
			if (key == 'n') {
				break; // 退出循环
			}
		} while (true);

		System.out.println("你退出了添加...");
	}
}
