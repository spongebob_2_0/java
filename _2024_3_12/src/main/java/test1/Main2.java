package test1;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Description
 * 是否形成回文串
 * @author: WuYimin
 * Date: 2024-03-12
 */
public class Main2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		String[] parts = input.split(" ");
		int n = Integer.parseInt(parts[0]);
		int k = Integer.parseInt(parts[1]);
		String s = parts[2];

		System.out.println(isPalindrome(s,k) ? "YES" : "NO") ;
	}

	private static boolean isPalindrome(String s, int k) {
		HashMap<Character,Integer> charCount = new HashMap<>();
		for (char c : s.toCharArray()) {
			charCount.put(c,charCount.getOrDefault(c,0)+1);
		}
		int oddCount = 0;
		for (Integer count : charCount.values()) {
			if(count % 2 != 0) {
				oddCount++;
			}
		}
		return oddCount - k <= 1;
	}
}
