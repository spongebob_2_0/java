package test1;

// 定义一个泛型类Person，其中T是一个类型参数，用于指定成员变量age的类型
class Person<T> {
    public T age;

    // 构造方法，接收一个类型为T的参数
    public Person(T age) {
        this.age = age;
    }

}

public class GenericsDemo {

    // 定义一个显示Person对象年龄的方法，接收的参数限定为Person<Number>
    // 这意味着，只接受Number类型或其子类类型作为T的Person对象
    public void showName(Person<Number> p) {
        System.out.println("p.age = " + p.age);
    }

    public static void main(String[] args) {
        // 创建两个Person对象，分别指定泛型类型为Integer和Number
        Person<Integer> ageInteger = new Person<>(123); // 使用Integer类型
        Person<Number> ageNumber = new Person<>(456); // 使用Number类型

        GenericsDemo demo = new GenericsDemo();

        // 这行可以正常工作，因为ageNumber的类型完全匹配showName方法的参数类型
        demo.showName(ageNumber);

        // 这行会导致编译错误，因为虽然Integer是Number的子类型，但泛型类型不支持协变
        // 即Person<Integer>不被认为是Person<Number>的子类型
        // demo.showName(ageInteger); // 编译错误

        // 为了解决上述问题，引入泛型通配符，修改showName方法如下
    }

    // 使用泛型通配符 ? extends Number，使得方法可以接受任何Number及其子类型的Person对象
    public void showNameWithWildcard(Person<? extends Number> p) {
        System.out.println("p.age with wildcard = " + p.age);
    }
}