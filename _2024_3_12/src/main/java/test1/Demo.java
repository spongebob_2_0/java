package test1;

import java.lang.reflect.Field;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

class Demo {
    // 声明一个泛型数组类型的成员变量
    List<String> list[];

    public static void main(String[] args) throws NoSuchFieldException {

        Scanner scanner = new Scanner(System.in);
        scanner.nextInt();
        // 通过反射获取Demo类中名为"list"的字段
        Field listField = Demo.class.getDeclaredField("list");
        // 获取字段的泛型类型
        Type genericType = listField.getGenericType();
        // 输出字段的泛型类型
        System.out.println("genericType = " + genericType);
        // 输出字段的泛型类型所属的类
        System.out.println("genericType.getClass() = " + genericType.getClass());

        // 检查该泛型类型是否是泛型数组类型
        if (genericType instanceof GenericArrayType) {
            // 将泛型类型强转为泛型数组类型
            GenericArrayType genericArrayType = (GenericArrayType) genericType;
            // 获取泛型数组的组件类型（即数组里元素的类型）
            Type genericComponentType = genericArrayType.getGenericComponentType();
            // 输出数组组件类型的运行时类
            System.out.println("GenericComponentType = " + genericComponentType.getClass());

            // 检查组件类型是否是参数化类型（即带有泛型参数的类型）
            if (genericComponentType instanceof ParameterizedType) {
                // 将组件类型强转为参数化类型
                ParameterizedType parameterizedType = (ParameterizedType) genericComponentType;
                // 输出声明该类型的类或接口
                System.out.println("RawType = " + parameterizedType.getRawType());
                // 获取泛型类型的实际类型参数（即List中的String）
                Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                // 输出实际类型参数
                System.out.println("actualTypeArguments = " + Arrays.toString(actualTypeArguments));
                // 输出拥有该类型的类型（对于嵌套类型有用，这里为null）
                System.out.println("OwnerType = " + parameterizedType.getOwnerType());
            }
        }
    }
}
