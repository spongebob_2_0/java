package test1;

public class ArrayResizeExample {
    public static void main(String[] args) {
        int[] oldArray = new int[5]; // 初始化原始数组
        int oldSize = oldArray.length;
        int newSize = oldSize * 2; // 计算新数组的大小
        
        // 创建新数组并复制原数组的内容
        int[] newArray = new int[newSize];
        System.arraycopy(oldArray, 0, newArray, 0, oldSize);
        
        // 更新引用
        oldArray = newArray;
        
        System.out.println("原始数组大小：" + oldSize);
        System.out.println("扩容后数组大小：" + newArray.length);
    }
}