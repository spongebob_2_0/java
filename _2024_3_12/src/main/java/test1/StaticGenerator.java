package test1;

// 泛型类
class StaticGenerator<T> {
    // 泛型类内部，定义普通方法/泛型方法
    //public void show(T t){}				// 正确
    //public <T> void show(T t){}			// 正确
    //// 泛型类内部，定义静态方法，使用泛型
	//public static void show(T t){}		// 报错
    //// 泛型类内部，定义泛型静态方法，使用泛型
    public static <T> void show(T t){}	// 正确
}