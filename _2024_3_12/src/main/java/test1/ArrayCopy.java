package test1;

import java.util.Arrays;

public class ArrayCopy {
	public static void main(String[] args) {
		int[] arr = {11, 22, 33, 44, 55, 66};
		int[] arrReverseNew2 = new int[arr.length];
		for (int i = arr.length - 1, j = 0; i >= 0; i--, j++) {
			arrReverseNew2[j] = arr[i];
		}
		System.out.println("反转之后的数组: " + Arrays.toString(arrReverseNew2));
	}
}