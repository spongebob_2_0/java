package test1;

import java.util.Scanner;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-03-19
 */
public class Main5 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String a = scanner.next();  //输入：123  qweqw
		String b = scanner.nextLine(); //先扫描缓冲区，发现还有"  qweqw"，则没有键入过程。
		System.out.println(a); //读走"123"，剩下"  qweqw"以及换行符
		System.out.println(b); //“换行符”是扫描结束的标志，则"  qweqw"
		scanner.close();
	}
}
