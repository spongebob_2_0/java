package sortTest;

import java.util.Arrays;

/**
 * 冒泡排序
 *
 * @author: WuYimin
 * Date: 2024-03-24
 */
public class BubbleSort {

	public static void main(String[] args) {
		int[] array = {10,9,8,7,6,5,4,3,2,1};
		bubbleSort(array);
		System.out.println(Arrays.toString(array));
	}

	private static void bubbleSort(int[] array) {
		for (int i = 0; i < array.length-1; i++) {
			boolean flag = false;
			for(int j = 0; j < array.length-1-i; j++) {
				if(array[j] > array[j+1]) {
					swop(array,j,j+1);
					flag = true;
				}
			}
			if(flag == false) {
				break;
			}
		}
	}

	private static void swop(int[] array, int j, int i) {
		int tmp = array[i];
		array[i] = array[j];
		array[j] = tmp;
	}


}
