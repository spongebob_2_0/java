package sortTest;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-03-24
 */
public class InsertSort {
	public static void main(String[] args) {

		new LinkedHashSet<>();
		new HashSet<>();
		int[] array = {10,9,8,7,6,5,4,3,2,1};
		insertSort(array);
		System.out.println(Arrays.toString(array));
	}

	private static void insertSort(int[] array) {
		for (int i = 1; i < array.length; i++) {
			int tmp = array[i];
			int j;
			for (j = i-1; j >=0; j--) {
				if(array[j] > tmp) {
					array[j+1] = array[j];
				}else {
					break;
				}
			}
			array[j+1] = tmp;
		}

	}
}
