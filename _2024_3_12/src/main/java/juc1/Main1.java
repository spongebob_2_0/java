package juc1;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-03-14
 */
public class Main1 {
	public static void main(String[] args) {
		System.out.println("main 开始了");
		Thread t1 = new Thread(() -> {
			System.out.println("t1任务开始了");
		});
		t1.start();;
		System.out.println("main 结束了");

	}
}
