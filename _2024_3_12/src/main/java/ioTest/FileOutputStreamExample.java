package ioTest;

import java.io.FileOutputStream;
import java.io.IOException;

public class FileOutputStreamExample {
    public static void main(String[] args) throws IOException {
        // Step 1: 创建字节输出流对象
        // 如果指定路径的文件不存在，FileOutputStream 会自动创建这个文件
        // 如果文件已经存在，则其原有内容会被清空（除非以追加模式创建FileOutputStream）
        FileOutputStream fos = new FileOutputStream("e:/test.txt");

        // Step 2: 写数据
        // 使用 write 方法将数据写入到文件中
        // 这里的参数是要写入的字节，所以写入的是 ASCII 码对应的字符 'a'、'b' 和 'c'
        fos.write(97);  // 写入字节97，即字符 'a'
        fos.write(98);  // 写入字节98，即字符 'b'
        fos.write(99);  // 写入字节99，即字符 'c'
        // 注意：文件中存储的是字节数据，如果打开文件查看，看到的是这些字节对应的字符

        // Step 3: 释放资源
        // 关闭文件输出流，释放与这个流相关联的系统资源
        // 不调用 close 方法可能会导致资源泄露
        fos.close();
    }
}