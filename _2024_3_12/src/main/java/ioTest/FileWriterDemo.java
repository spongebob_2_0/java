package ioTest;

import java.io.FileWriter;
import java.io.IOException;

public class FileWriterDemo {
    public static void main(String[] args) throws IOException {
        // 创建FileWriter对象，关联"fw.txt"
        FileWriter fw = new FileWriter("fw.txt");
        // 字符数组
        char[] chars = "程序员scholar".toCharArray();
        // 写入整个字符数组
        fw.write(chars);
        // 写入字符数组的一部分
        fw.write(chars, 1, 2); // 从索引1开始，写入2个字符
        // 关闭资源
        fw.close();
    }
}