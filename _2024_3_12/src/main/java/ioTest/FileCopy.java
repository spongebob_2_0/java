package ioTest;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileCopy {
    public static void main(String[] args) {
        FileInputStream fis = null;
        FileOutputStream fos = null;
        try {
            // 创建FileInputStream对象，读取源图片
            fis = new FileInputStream("C:\\Users\\WuYimin\\Pictures\\图库\\123.png");
            // 创建FileOutputStream对象，指定拷贝图片的目标位置（目录不存在会报错）
            fos = new FileOutputStream("E:/picture/fosCopy.jpg");
            
            byte[] buffer = new byte[1024]; // 创建字节数组作为缓冲
            int len;
            // 边读边写，直到文件末尾
            while ((len = fis.read(buffer)) != -1) {
                fos.write(buffer, 0, len);
            }
            System.out.println("拷贝图片成功!");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 关闭输入流和输出流
            try {
                if (fis != null) {
                    fis.close();
                }
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
