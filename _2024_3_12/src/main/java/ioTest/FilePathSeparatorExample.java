package ioTest;

import java.io.File;

public class FilePathSeparatorExample {
    public static void main(String[] args) {
        // 获取系统路径分隔符
        String pathSeparator = File.pathSeparator;
        char pathSeparatorChar = File.pathSeparatorChar;
        
        // 打印路径分隔符
        System.out.println("Path Separator (String): " + pathSeparator);
        System.out.println("Path Separator (char): " + pathSeparatorChar);
        
        // 示例：使用路径分隔符拼接多个路径
        String path1 = "C:\\myfolder";
        String path2 = "D:\\otherfolder";
        String combinedPath = path1 + File.pathSeparator + path2;
        
        // 打印拼接后的路径
        System.out.println("Combined Path: " + combinedPath);
    }
}