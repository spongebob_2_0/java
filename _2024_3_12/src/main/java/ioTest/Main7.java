package ioTest;

import java.util.concurrent.atomic.AtomicStampedReference;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-03-23
 */
public class Main7 {
	public static void main(String[] args) throws InterruptedException {

		ReentrantLock lock = new ReentrantLock();
		// 初始化AtomicStampedReference，引用初始值为"A"，初始版本号为1
		AtomicStampedReference<String> reference = new AtomicStampedReference<>("A", 1);

		int initialStamp = reference.getStamp(); // 获取初始版本号
		// 线程1：将"A"更改为"B"，然后再更改回"A"
		Thread t1 = new Thread(() -> {
			reference.compareAndSet(reference.getReference(), "B", initialStamp, initialStamp + 1); // 将"A"改为"B"
			int newStamp = reference.getStamp(); // 获取"B"后的新版本号
			reference.compareAndSet(reference.getReference(), "A", newStamp, newStamp + 1); // 将"B"改回"A"
		});
		t1.start();
		t1.join(); // 确保t1操作完成

		// 线程2尝试使用“初始的版本号”（即1）将"A"更新为"C"
		Thread t2 = new Thread(() -> {
			// 使用初始的版本号1尝试更新，预期由于版本号不匹配而失败
			boolean isSuccess = reference.compareAndSet("A", "C", initialStamp, initialStamp + 1);
			System.out.println("是否修改成功: " + isSuccess + "，当前版本号：" + reference.getStamp() + "，当前值：" + reference.getReference());
		});
		t2.start();
		t2.join(); // 确保t2操作完成
	}

}
