package ioTest;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FileReaderExample1 {
    public static void main(String[] args) {
        // 初始化FileReader为null
        FileReader fr = null;
        try {
            // 创建FileReader对象，指定要读取的文件路径
            File file = new File("e:/sort.txt");
            if(!file.exists()) {
                file.createNewFile();
            }
            fr = new FileReader(file);
            int data;
            // 使用read()逐个读取字符，直到返回-1表示文件末尾
            while ((data = fr.read()) != -1) {
                // 将读取的字符转换为char并打印
                System.out.print((char) data);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 确保FileReader被正确关闭
            if (fr != null) {
                try {
                    fr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}