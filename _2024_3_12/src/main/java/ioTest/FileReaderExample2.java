package ioTest;

import java.io.FileReader;
import java.io.IOException;

public class FileReaderExample2 {
    public static void main(String[] args) {
        // 初始化FileReader为null
        FileReader fr = null;
        try {
            // 创建FileReader对象，指定要读取的文件路径
            fr = new FileReader("e:/sort.txt");
            // 创建字符数组，用作缓冲区
            char[] buffer = new char[1024];
            int len;
            // 使用read(char[] cbuf)批量读取字符到数组，返回实际读取的字符数
            while ((len = fr.read(buffer)) != -1) {
                // 将字符数组转换为字符串并打印，只打印实际读取的部分
                System.out.print(new String(buffer, 0, len));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 确保FileReader被正确关闭
            if (fr != null) {
                try {
                    fr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}