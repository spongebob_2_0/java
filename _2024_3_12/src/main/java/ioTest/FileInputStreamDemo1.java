package ioTest;

import java.io.FileInputStream;
import java.io.IOException;

public class FileInputStreamDemo1 {
    public static void main(String[] args) throws IOException {
        // 创建字节输入流对象，指定要读取的文件路径
        FileInputStream fis = new FileInputStream("E:/a.txt");

        // 使用read方法读取文件中的一个字节
        int by = fis.read();  // read方法返回读取到的字节，如果达到文件末尾返回-1
        System.out.println((char) by);  // 将读取到的字节转换为字符打印

        // 完成读取操作后关闭输入流，释放资源
        fis.close();
    }
}