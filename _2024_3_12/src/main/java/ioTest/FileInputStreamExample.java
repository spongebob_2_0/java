package ioTest;

import java.io.FileInputStream;
import java.io.IOException;

public class FileInputStreamExample {
    public static void main(String[] args) {
        try (FileInputStream fis = new FileInputStream("E:/a.txt")) {
            byte[] bytes = new byte[1024]; // 创建字节数组作为缓冲
            int len; // 每次读取的字节数
            while ((len = fis.read(bytes)) != -1) {
                // 将读取的字节转换为字符串进行输出
                System.out.print(new String(bytes, 0, len));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}