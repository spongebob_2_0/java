package ioTest;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileCopyExample {
    public static void main(String[] args) {
        try (
            FileInputStream fis = new FileInputStream("C:\\Users\\WuYimin\\Pictures\\图库\\123.png");
            FileOutputStream fos = new FileOutputStream("E:/picture/copy2.jpg");
        ) {
            byte[] buffer = new byte[1024]; // 创建字节数组作为缓冲
            int len; // 每次读取的字节数
            while ((len = fis.read(buffer)) != -1) {
                fos.write(buffer, 0, len); // 将缓冲区的数据写入输出流
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}