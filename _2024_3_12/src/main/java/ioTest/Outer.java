package ioTest;

public class Outer {

	void outMethod() {
		final int a = 10;
		class Inner {
			void innerMethod() {
				System.out.println(a);
			}
		}
	}
}
