package ioTest;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 示范使用 FileOutputStream 进行文件写操作的基础示例。
 */
public class FileOutputStreamDemo {

    public static void main(String[] args) {
        FileOutputStream fos = null;

        try {
            // 创建 FileOutputStream 对象，准备向 e:/fos.txt 文件写入数据。
            // 第二个参数为 true，表示以追加形式写文件，如果为 false 或不填，则为覆盖形式。
            fos = new FileOutputStream("e:/fos.txt", true);

            // 写入一个字符串。字符串在写入时需要转换为字节数组。
            String str = "Hello scholar 666 ";
            byte[] strBytes = str.getBytes();

            // 一次写入整个字节数组。
            fos.write(strBytes);
            
            // 一次写入字节数组的一部分，这里示范写入整个数组。
            fos.write(strBytes, 0, strBytes.length);

            // 执行完写操作后，强烈建议立即刷新输出流，确保数据完全写入文件。
            fos.flush();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 最后，不要忘记关闭输出流，释放与文件相关的资源。
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
