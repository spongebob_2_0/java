package ioTest;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 字节流写数据的换行和追加写入示例。
 */
public class OutputStreamDemo3 {
    public static void main(String[] args) throws IOException {
        // 创建 FileOutputStream 对象，启用追加模式。
        // 这里的 true 参数表示以追加的形式写入数据，即新数据会被添加到文件的末尾。
        FileOutputStream fos = new FileOutputStream("e:/a.txt", true);

        // 写入单个字节的数据。这里写入的是数字97，ASCII码对应字符 'a'
        fos.write(97);
        // 写入换行符，适应不同操作系统的换行符。
        // Windows系统中使用 "\r\n"，Linux中使用 "\n"，Mac旧系统使用 "\r"。
        fos.write("\r\n".getBytes());
        
        // 继续写入其他数据，每写一次后跟一个换行符
        fos.write(98); // 写入 'b'
        fos.write("\r\n".getBytes());
        fos.write(99); // 写入 'c'
        fos.write("\r\n".getBytes());

        // 数据写入完毕后，关闭流资源
        fos.close();
    }

    /**
     * 单独的换行写入方法示例。
     */
    private static void method1() throws IOException {
        // 创建 FileOutputStream 对象，这里没有使用追加模式，每次运行会覆盖原文件
        FileOutputStream fos = new FileOutputStream("day11_demo\\a.txt");

        // 写入单个字节数据，并演示换行操作
        fos.write(97); // 写入 'a'
        fos.write("\r\n".getBytes());
        
        fos.write(98); // 写入 'b'
        fos.write("\r\n".getBytes());
        
        fos.write(99); // 写入 'c'
        fos.write("\r\n".getBytes());

        // 完成写入操作后，关闭流资源
        fos.close();
    }
}
