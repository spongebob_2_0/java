package ioTest;

import java.io.File;

public class TestDirectoryOperations {
    public static void main(String[] args) {
        // 将目录封装为File类的对象：
        File dir = new File("D:\\IDEA");

        // 演示File类常用于目录的方法
        System.out.println("目录是否可读：" + dir.canRead());
        System.out.println("目录是否可写：" + dir.canWrite());
        System.out.println("目录的名字：" + dir.getName());
        System.out.println("目录的上级目录：" + dir.getParent());
        System.out.println("是否是一个目录：" + dir.isDirectory());
        System.out.println("是否是一个文件（应为false）：" + dir.isFile());
        System.out.println("是否隐藏：" + dir.isHidden());
        System.out.println("目录的大小（字节），目录本身大小不计算内容：" + dir.length());
        System.out.println("目录是否存在：" + dir.exists());
        System.out.println("绝对路径：" + dir.getAbsolutePath());
        System.out.println("相对路径（通常与绝对路径相同）：" + dir.getPath());
        System.out.println("toString输出：" + dir.toString());

        // 创建多层目录
        File multiLevelDir = new File("D:\\a\\b\\c");
        // 使用mkdirs创建多层目录，如果使用mkdir仅能创建一层目录
        multiLevelDir.mkdirs();

        // 删除目录
        // 注意：delete方法只能删除空目录或文件。如果是目录，必须是空的，否则不会删除成功
        multiLevelDir.delete();

        // 列出目录下所有文件和子目录的名字
        String[] fileList = dir.list();
        System.out.println("目录下的文件和子目录名字：");
        for (String filename : fileList) {
            System.out.println(filename);
        }
        System.out.println("=========================");

        // 列出目录下所有文件和子目录的File对象，可以获取更多信息
        File[] files = dir.listFiles();
        System.out.println("目录下的文件和子目录详细信息：");
        for (File file : files) {
            System.out.println(file.getName() + ", 绝对路径：" + file.getAbsolutePath());
        }
    }
}
