package ioTest;

import com.sun.org.apache.bcel.internal.generic.NEW;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.Buffer;

public class FileWriterAppendDemo {
    public static void main(String[] args) {
        // 定义文件路径
        String filePath = "fw.txt";
        FileWriter fw = null;
        FileReader fe = null;
        try {
            // 创建FileWriter对象，设置append为true，开启续写模式
            fw = new FileWriter(filePath, true);
            // 写出字符串，续写到文件末尾
            fw.write("程序员scholar真棒");
            // 提示信息
            System.out.println("续写数据完成！");
            fe = new FileReader(filePath);
            char[] buffer = new char[1024];
            int len;
            while ((len = fe.read(buffer)) != -1) {
                System.out.println(new String(buffer,0,len));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 关闭资源
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
