package ioTest;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class BufferedIOLine {
    public static void main(String[] args) {
        // 使用BufferedWriter在文本文件中写入数据并换行
        try (BufferedWriter bw = new BufferedWriter(new FileWriter("out.txt"))) {
            bw.write("程"); // 写入单个字符
            bw.newLine();  // 写入换行符
            bw.write("序");
            bw.newLine();
            bw.write("员");
            bw.newLine(); // 再次写入换行符
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}