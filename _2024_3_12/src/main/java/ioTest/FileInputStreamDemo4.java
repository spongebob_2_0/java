package ioTest;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileInputStreamDemo4 {
    public static void main(String[] args) {
        FileInputStream fis = null;
        FileOutputStream fos = null;
        try {
            // 创建字节输入流对象，用于读取原图片
            fis = new FileInputStream("C:\\Users\\WuYimin\\Pictures\\图库\\123.png");

            // 创建字节输出流对象，用于写入拷贝的图片
            fos = new FileOutputStream("e:/picture/copy.jpg");

            // 定义变量by，用于存放读取到的字节
            int by;
            // 循环读取原图片的字节，并写入到拷贝图片中
            while ((by = fis.read()) != -1) {
                fos.write(by); // 一次读写一个字节
            }
        } catch (IOException e) {
            // 打印异常信息
            e.printStackTrace();
        } finally {
            // 释放资源，首先判断fis是否为null，避免NullPointerException
            if (fis != null) {
                try {
                    fis.close(); // 尝试关闭fis流
                } catch (IOException e) {
                    // 打印关闭fis流时的异常信息
                    e.printStackTrace();
                }
            }
            // 同样的方式释放fos资源
            if (fos != null) {
                try {
                    fos.close(); // 尝试关闭fos流
                } catch (IOException e) {
                    // 打印关闭fos流时的异常信息
                    e.printStackTrace();
                }
            }
        }
    }
}
