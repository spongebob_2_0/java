package ioTest;

import java.io.FileInputStream;
import java.io.IOException;

public class FileInputStreamDemo {
    public static void main(String[] args) {
        FileInputStream fis = null;
        try {
            // 创建FileInputStream对象，指定要读取的文件路径
            fis = new FileInputStream("E:/a.txt");
            Byte[] bytes = new Byte[1024];
            int readData;
            // 循环读取数据，直到文件末尾
            while ((readData = fis.read()) != -1) {
                // 将读取的字节转换为字符并输出
                System.out.print((char) readData);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    // 关闭输入流
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}