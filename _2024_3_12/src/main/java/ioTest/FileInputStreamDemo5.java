package ioTest;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileInputStreamDemo5 {
    public static void main(String[] args) {
        // 使用try-with-resources语句自动管理资源
        try (
            // 在try()中声明资源，这些资源会在try代码块执行完毕后自动关闭
            FileInputStream fis = new FileInputStream("D:/scholar Gallery/scholarSave/wallhaven-wexe9r.jpg");
            FileOutputStream fos = new FileOutputStream("D:/copy.jpg");
        ) {
            // 读取源文件，并将内容写入目标文件
            int by;
            while ((by = fis.read()) != -1) {
                fos.write(by);
            }
            // 注意：此处无需手动关闭流，try-with-resources语句已经自动处理
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}