package ioTest;

import java.io.File;
import java.io.IOException;

public class TestFileOperations {
    public static void main(String[] args) {
        // 使用绝对路径创建File对象
        File f = new File("d:\\test.txt");
        
        // 使用相同的路径创建另一个File对象，用于比较
        File f1 = new File("d:\\test.txt");
        
        // 使用不同的路径分隔符创建File对象，演示跨平台路径分隔符的处理
        File f2 = new File("d:/test.txt");
        
        // 使用File.separator确保跨平台路径分隔符的正确使用
        File f3 = new File("d:" + File.separator + "test.txt");

        // 演示File类的常用方法
        System.out.println("文件是否可读：" + f.canRead());
        System.out.println("文件是否可写：" + f.canWrite());
        System.out.println("文件的名字：" + f.getName());
        System.out.println("文件的上级目录：" + f.getParent());
        System.out.println("是否是一个目录：" + f.isDirectory());
        System.out.println("是否是一个文件：" + f.isFile());
        System.out.println("是否隐藏：" + f.isHidden());
        System.out.println("文件的大小（字节）：" + f.length());
        System.out.println("文件是否存在：" + f.exists());

        // 比较两个File对象
        System.out.println("两个File对象是否相同（地址比较）：" + (f == f1)); // false
        System.out.println("两个File对象对应的文件路径是否相同：" + f.equals(f1)); // true

        // 文件路径相关信息
        System.out.println("绝对路径：" + f.getAbsolutePath());
        System.out.println("相对路径：" + f.getPath());
        System.out.println("toString输出：" + f.toString());

        // 使用相对路径创建新的File对象，并演示其路径相关方法
        File f5 = new File("demo.txt");
        // 如果文件不存在，则尝试创建新文件
        try {
            if (!f5.exists()) {
                boolean created = f5.createNewFile();
                System.out.println("demo.txt文件创建：" + created);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        System.out.println("绝对路径：" + f5.getAbsolutePath());
        System.out.println("相对路径：" + f5.getPath());
        System.out.println("toString输出：" + f5.toString());

        // 创建嵌套目录下的File对象，并尝试创建文件
        File f6 = new File("a/b/c/demo.txt");
        try {
            // 确保目录存在，不存在则递归创建。
            new File(f6.getParent()).mkdirs();
            if (!f6.exists()) {
                boolean created = f6.createNewFile();
                System.out.println("a/b/c/demo.txt文件创建：" + created);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        System.out.println("绝对路径：" + f6.getAbsolutePath());
        System.out.println("相对路径：" + f6.getPath());
    }
}