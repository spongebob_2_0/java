package ioTest;

import java.io.*;

public class IOTest {
    // 数据准备，向文件中写入大量数据
    public static void dataReady() throws IOException {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 600000; i++) {
            sb.append("abcdefghijklmnopqrstuvwxyz");
        }
        OutputStream os = new FileOutputStream(new File("E:/picture/data.txt"));
        os.write(sb.toString().getBytes());
        os.close();
        System.out.println("数据准备完毕");
    }
    
    // 使用普通字符流（不使用数组）复制文件
    public static void copy(File in, File out) throws IOException {
        Reader reader = new FileReader(in);
        Writer writer = new FileWriter(out);

        // 每次读写单个字符
        int ch;
        while ((ch = reader.read()) != -1) {
            writer.write((char) ch);
        }
        reader.close();
        writer.close();
    }

    // 使用普通字符流（使用字符数组）复制文件
    public static void copyChars(File in, File out) throws IOException {
        Reader reader = new FileReader(in);
        Writer writer = new FileWriter(out);

        // 使用字符数组缓冲，提高读写效率
        char[] chs = new char[1024];
        while ((reader.read(chs)) != -1) {
            writer.write(chs);
        }
        reader.close();
        writer.close();
    }

    // 使用缓冲字符流复制文件
    public static void bufferedCopy(File in, File out) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(in));
        BufferedWriter bw = new BufferedWriter(new FileWriter(out));

        // 使用readLine读取一行数据，newLine写入换行，提高效率
        String line;
        while ((line = br.readLine()) != null) {
            bw.write(line);
            bw.newLine();
            bw.flush(); // 刷新缓冲区，确保数据全部写出
        }
        br.close();
        bw.close();
    }

    public static void main(String[] args) throws IOException {
        // 数据准备
        dataReady();

        File data = new File("E:/picture/data.txt");
        File a = new File("E:/picture/a.txt");
        File b = new File("E:/picture/b.txt");
        File c = new File("E:/picture/c.txt");

        // 比较不同方式复制文件的耗时和文件大小
        long start = System.currentTimeMillis();
        copy(data, a);
        long end = System.currentTimeMillis();

        long start2 = System.currentTimeMillis();
        copyChars(data, b);
        long end2 = System.currentTimeMillis();

        long start3 = System.currentTimeMillis();
        bufferedCopy(data, c);
        long end3 = System.currentTimeMillis();

        System.out.println("普通字符流(不使用数组)耗时：" + (end - start) + " ms, 文件大小：" + a.length() / 1024 + " kb");
        System.out.println("普通字符流(使用字符数组)耗时：" + (end2 - start2) + " ms, 文件大小：" + b.length() / 1024 + " kb");
        System.out.println("缓冲字符流耗(每次读取一行)时：" + (end3 - start3) + " ms, 文件大小：" + c.length() / 1024 + " kb");
    }
}
