package ioTest2;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class BufferedIOUseBuffer {
    public static void main(String[] args) throws IOException {
        long startTime = System.currentTimeMillis(); // 记录开始时间
        
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream("E:/视频/并非每场相遇都会有结局.mp4")); // 创建缓冲输入流
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("E:/picture/copy_use_buffer.exe")); // 创建缓冲输出流
        
        byte[] buffer = new byte[1024]; // 设置一个缓冲区
        int len;
        while ((len = bis.read(buffer)) != -1) {
            bos.write(buffer, 0, len); // 读取并写入数据
        }
        
        bos.close(); // 关闭输出流
        bis.close(); // 关闭输入流
        
        long endTime = System.currentTimeMillis(); // 记录结束时间
        System.out.println("使用缓冲流复制文件所需时间: " + (endTime - startTime) + " 毫秒");
    }
}