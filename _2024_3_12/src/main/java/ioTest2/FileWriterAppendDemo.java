package ioTest2;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

public class FileWriterAppendDemo {
    public static void main(String[] args) {
        // 定义要写入数据的文件路径
        String filePath = "fw.txt";

        FileWriter fw = null;
        try {
            // 创建FileWriter对象，设置append为true来开启续写模式
            // 这意味着如果fw.txt文件已存在，写入的数据会追加到文件末尾而不是覆盖它
            fw = new FileWriter(filePath, true);
            
            // 使用write方法写出字符串数据，这里的数据会追加到文件的末尾
            fw.write("程序员scholar真棒");
            
            // 输出提示信息，表明数据已经被成功追加写入
            System.out.println("续写数据完成！");
        } catch (IOException e) {
            e.printStackTrace(); // 捕获并打印可能发生的IOException
        } finally {
            // 在finally块中关闭资源，确保释放FileWriter资源，避免内存泄露
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
