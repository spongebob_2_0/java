package ioTest2;

import java.io.FileWriter;
import java.io.IOException;

public class FWWriteNewLine {
    public static void main(String[] args) {
        // 定义文件名
        String fileName = "fw.txt";
        FileWriter fw = null;
        try {
            // 创建FileWriter对象，参数为文件名
            // 这里没有设置append，所以每次运行程序都会覆盖原文件内容
            fw = new FileWriter(fileName);

            // 写出字符串
            fw.write("程序员scholar");
            // 写出换行符，实现换行
            // Windows平台使用\r\n作为换行符，Unix/Linux平台使用\n
            fw.write(System.getProperty("line.separator"));  // 更通用的换行方式

            // 再次写出字符串，此字符串会出现在新的一行
            fw.write("再次欢迎程序员scholar");

            // 输出提示信息
            System.out.println("数据写入完成，并正确换行。");
        } catch (IOException e) {
            e.printStackTrace(); // 捕获并打印异常
        } finally {
            // 无论是否发生异常，都要在finally块中关闭FileWriter资源
            if (fw != null) {
                try {
                    fw.close();  // 关闭流，释放资源
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
