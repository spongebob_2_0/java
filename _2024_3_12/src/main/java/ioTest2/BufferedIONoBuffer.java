package ioTest2;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class BufferedIONoBuffer {
    public static void main(String[] args) throws IOException {
        long startTime = System.currentTimeMillis(); // 记录开始时间
        
        FileInputStream fis = new FileInputStream("E:/视频/并非每场相遇都会有结局.mp4"); // 创建文件输入流
        FileOutputStream fos = new FileOutputStream("E:/picture/copy_no_buffer.mp4"); // 创建文件输出流
        
        byte[] buffer = new byte[1024]; // 设置一个缓冲区
        int len;
        while ((len = fis.read(buffer)) != -1) {
            fos.write(buffer, 0, len); // 读取并写入数据
        }
        
        fos.close(); // 关闭输出流
        fis.close(); // 关闭输入流
        
        long endTime = System.currentTimeMillis(); // 记录结束时间
        System.out.println("不使用缓冲流复制文件所需时间: " + (endTime - startTime) + " 毫秒");
    }
}