package ioTest2;

import java.io.*;

public class MyTest {
    public static void main(String[] args) throws IOException {
        // 定义源文件和目标文件
        File data = new File("D:\\桌面\\运维资料\\资料-并发编程\\并发编程笔记.zip");
        File a = new File("D:\\桌面\\运维资料\\a.zip");
        File b = new File("D:\\桌面\\运维资料\\b.zip");

        // 记录普通字节流复制文件的开始和结束时间
        long start = System.currentTimeMillis();
        copy(data, a);
        long end = System.currentTimeMillis();

        // 记录缓冲字节流复制文件的开始和结束时间
        long start2 = System.currentTimeMillis();
        bufferedCopy(data, b);
        long end2 = System.currentTimeMillis();

        // 输出耗时
        System.out.println("普通字节流耗时：" + (end - start) + " ms");
        System.out.println("缓冲字节流耗时：" + (end2 - start2) + " ms");
    }

    // 普通字节流复制文件的方法
    public static void copy(File in, File out) throws IOException {
        InputStream is = new FileInputStream(in);
        OutputStream os = new FileOutputStream(out);
        
        int by;
        byte[] buffer = new byte[1024];
        // 每次读写单个字节，没有使用缓冲区
        while ((by = is.read(buffer)) != -1) {
            os.write(buffer,0,by);
        }
        is.close();
        os.close();
    }

    // 缓冲字节流复制文件的方法
    public static void bufferedCopy(File in, File out) throws IOException {
        BufferedInputStream bi = new BufferedInputStream(new FileInputStream(in));
        BufferedOutputStream bo = new BufferedOutputStream(new FileOutputStream(out));
        
        int by;
        byte[] buffer = new byte[1024];
        // 利用内部缓冲区提高读写效率
        while ((by = bi.read(buffer)) != -1) {
            bo.write(buffer,0,by);
        }
        bo.close();
        bi.close();
    }
}