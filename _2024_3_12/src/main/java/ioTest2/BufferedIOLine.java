package ioTest2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class BufferedIOLine {
    public static void main(String[] args) {
        // 使用BufferedReader读取文本文件中的每一行
        try (BufferedReader br = new BufferedReader(new FileReader("fw.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line); // 打印读取的每一行
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}