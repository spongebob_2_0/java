package test2;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-03-21
 */
public class Main2 {
	public static void main(String[] args) {
		String text = "Hello, Java!";
		// 获取字符串长度
		int length = text.length(); // 返回12
		// 连接字符串
		String concatenated = text.concat(" How are you?"); // "Hello, Java! How are you?"
		// 获取指定索引处的字符
		char character = text.charAt(7); // 返回 'J'
		// 查找子字符串在字符串中第一次出现的索引
		int indexOfJava = text.indexOf("Java"); // 返回 7
		// 截取子字符串
		String subString = text.substring(7, 11); // "Java"

	}

}
