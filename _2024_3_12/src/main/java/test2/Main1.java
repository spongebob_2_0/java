package test2;

import java.util.Scanner;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-03-20
 */
public class Main1 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		while (scanner.hasNextInt()) {
			int line = scanner.nextInt();
			System.out.println(line);
		}
	}
}
