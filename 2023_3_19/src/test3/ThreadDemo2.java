package test3;

class MyRunnable implements Runnable {
    Object o =new Object();
    int ticket =10;
    @Override
        public void run() {
            for (int i = 1; i < 100; i++) {
                synchronized (o) {
                    if (ticket > 0) {
                        System.out.println("第" + i + "个人从" + Thread.currentThread().getName() + "买到了哈尔滨的第" + ticket + "票");
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    ticket--;
                }
            }
    }
}
public class ThreadDemo2 {
    public static void main(String[] args) {
        MyRunnable runnable =new MyRunnable();
        Thread t1 =new Thread(runnable,"窗口1");
        Thread t2 =new Thread(runnable,"窗口2");
        t1.start();
        t2.start();
//        while (true) {
//            System.out.println("hello-main");
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
    }
}