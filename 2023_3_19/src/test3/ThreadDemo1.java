package test3;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-21
 * Time: 0:23
 */
class Sum {
    private int count =0;
    synchronized public  void add() {
        count++;
    }
    public int getCount() {
        return count;
    }
}
public class ThreadDemo1 {
    public static void main(String[] args) {
        Sum o =new Sum();
        Thread t1 =new Thread(() -> {
           for(int i =0; i<50000; i++) {
               o.add();
           }
        });
        Thread t2 =new Thread(() -> {
            for(int i =0; i<50000; i++) {
                o.add();
            }
        });
        t1.start();
        t2.start();
        System.out.println(o.getCount());
    }
}
