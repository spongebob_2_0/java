package test2;

import java.util.*;


public class Solution1 {
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     * 比较版本号
     * @param version1 string字符串 
     * @param version2 string字符串 
     * @return int整型
     */
    public static int compare (String version1, String version2) {
        // write code here
        String[] array1 = version1.split("\\.");
        String[] array2 = version2.split("\\.");
        int len1 = array1.length;
        int len2 = array2.length;
        int len = Math.max(len1, len2);
        int[] k1 = new int[len];
        for (int i = 0; i < len1; i++) {
            k1[i] = Integer.valueOf(array1[i]);
        }
        int[] k2 = new int[len];
        for (int i = 0; i < len2; i++) {
            k2[i] = Integer.valueOf(array2[i]);
        }
        for (int i = 0; i < len; i++) {
            if (k1[i] > k2[i]) {
                return 1;
            } else if (k1[i] < k2[i]) {
                return -1;
            }
        }
        return 0;
    }
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        String version1 = scanner.nextLine();
        String version2 = scanner.nextLine();
        int ret =compare(version1,version2);
        System.out.println(ret);
    }
}