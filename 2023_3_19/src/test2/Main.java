package test2;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int n =scanner.nextInt();
        int sum =0;
        int[] array =new int[3*n];
        int len =array.length;
        for(int i = 0; i < len; i++) {
            array[i] =scanner.nextInt();
        }
        Arrays.sort(array);
        for(int i = len-1-n; i >= 0; i--) {
            if(n != 0) {
                sum +=array[i];
                n--;
            }
        }
        System.out.println(sum);
    }
}