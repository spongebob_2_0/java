import java.util.*;

public class Main2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);      
        while (in.hasNextInt()) { 
            int n = in.nextInt();
            int[] array = new int[3 * n];
            for (int i = 0; i < array.length; i++) {
                array[i] = in.nextInt();
            }
            Arrays.sort(array);
            long sum = 0;
            for (int i = array.length - 2; i >= n; i -= 2) {
                sum += array[i];
            }
            System.out.println(sum);
        }
    }
}