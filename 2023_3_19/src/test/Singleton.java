package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-19
 * Time: 12:23
 */
public class Singleton {
    //唯一实例的本体
    private static final  Singleton singleton =new Singleton();
    //禁止外部new实例
    private Singleton() {

    }
    public static Singleton getSingleton() {
        return singleton;
    }
}
class Main {
    public static void main(String[] args) {
//        Singleton singleton =new Singleton();
    }
}
