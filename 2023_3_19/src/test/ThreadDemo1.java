package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-19
 * Time: 11:30
 */
public class ThreadDemo1 {
    //t1先执行,执行到wait就阻塞了
    //1s之后t2开始执行,执行到notify
    //就会通知t1线程唤醒(注意,notify是在synchronized内部,就是需要t2释放了锁t1才能继续往下走)
    public static void main(String[] args) throws InterruptedException {
        Object o =new Object();
        Thread t1 =new Thread(() -> {
            //wait.
            synchronized(o) {
                System.out.println("wait方法开始");
                try {
                    o.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("wait方法结束");
            }
        });
        t1.start();
        Thread.sleep(1000);
        Thread t2 =new Thread(() -> {
            //notify
            synchronized(o) {
                System.out.println("notify方法开始");
                o.notify();
                System.out.println("notify方法结束");
            }
        });
        t2.start();
    }
}
