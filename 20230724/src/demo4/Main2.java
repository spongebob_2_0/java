package demo4;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-24
 * Time: 12:10
 */
public class Main2 {
    public static void main(String[] args) {
        Semaphore semaphore =new Semaphore(3);
        ExecutorService threadPool = Executors.newFixedThreadPool(30);
        for (int i = 0; i < 30; i++) {
            threadPool.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        semaphore.acquire();
                        System.out.println(Thread.currentThread().getName() + " 获得资源");
                        semaphore.release();
                        System.out.println(Thread.currentThread().getName() + " 释放资源");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        threadPool.shutdown();
    }
}