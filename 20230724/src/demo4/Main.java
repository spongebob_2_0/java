package demo4;

import sun.awt.windows.ThemeReader;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-24
 * Time: 11:35
 */
public class Main {
    public static void main(String[] args) {
        //三个停车位
        Semaphore semaphore = new Semaphore(3);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    semaphore.acquire();
                    System.out.println(Thread.currentThread().getName() + " 占据了停车位");
                    semaphore.release();
                    System.out.println(Thread.currentThread().getName() + " 离开了停车位");
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        //假设10个线程相当于10辆车
        for (int i = 1; i <= 10; i++) {
            Thread t = new Thread(runnable);
            t.start();
        }
    }
}
