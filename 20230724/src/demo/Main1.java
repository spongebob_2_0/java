package demo;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-24
 * Time: 1:41
 */
class Person {
    private String name;
    private int age;

    public Person() {

    }
    private void hello (){
        System.out.println("我是公开方法");
    }

    private void say() {
        System.out.println("我是私有方法");
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

public class Main1 {
    public static void main(String[] args) throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, NoSuchFieldException {// 获取Person类的Class对象

                // 获取Person类的Class对象
                Class<Person> personClass = Person.class;

                // 实例化对象
                Person person = personClass.newInstance();
                // 等价于 Person person = new Person();

                // 访问属性
                Field nameField = personClass.getDeclaredField("name");
                nameField.setAccessible(true); // 设置为可访问，因为name字段是私有的
                nameField.set(person, "Alice"); // 设置name字段的值为"Alice"

                Field ageField = personClass.getDeclaredField("age");
                ageField.setAccessible(true);
                ageField.setInt(person, 30); // 设置age字段的值为30
                System.out.println(person);

                // 调用方法
                Method sayHelloMethod = personClass.getMethod("hello");
                sayHelloMethod.invoke(personClass); // 调用sayHello方法

//                // 获取构造函数并实例化对象
//                Constructor<Person> constructor = personClass.getConstructor(String.class, int.class);
//                Person person2 = constructor.newInstance("Bob", 25);
//
//                // 调用私有方法
//                Method privateMethod = personClass.getDeclaredMethod("say");
//                privateMethod.setAccessible(true);
//                privateMethod.invoke(person2); // 调用私有方法


            }
        }

