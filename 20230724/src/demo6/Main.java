package demo6;

import java.sql.Time;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Main {
    private static Semaphore semA = new Semaphore(1);
    private static Semaphore semB = new Semaphore(0);
    private static Semaphore semC = new Semaphore(0);

    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(3);
        executor.execute(new Printer(semA, semB, "A"));
        executor.execute(new Printer(semB, semC, "B"));
        executor.execute(new Printer(semC, semA, "C"));

        executor.shutdown();
    }

    static class Printer implements Runnable {
        private Semaphore currentSemaphore;
        private Semaphore nextSemaphore;
        private String printChar;

        public Printer(Semaphore currentSemaphore, Semaphore nextSemaphore, String printChar) {
            this.currentSemaphore = currentSemaphore;
            this.nextSemaphore = nextSemaphore;
            this.printChar = printChar;
        }

        @Override
        public void run() {
            for (int i = 0; i < 10; i++) { // 控制打印次数
                try {
                    TimeUnit.SECONDS.sleep(1);
                    currentSemaphore.acquire();
                    System.out.print(printChar);
                    nextSemaphore.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        HashSet<Integer> hashSet = new HashSet<>();
        TreeSet<Integer> treeSet =new TreeSet<>();
        LinkedHashSet<Integer> linkedHashSet = new LinkedHashSet<>();
    }
}
