package demo6;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-26
 * Time: 16:00
 */
public class Main3 {
    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + " 打印了A");
        });
        Thread t2 = new Thread(() -> {
            try {
                t1.join();
                System.out.println(Thread.currentThread().getName() + " 打印了B");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t3 = new Thread(() -> {
            try {
                t2.join();
                System.out.println(Thread.currentThread().getName() + " 打印了C");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        t1.start();
        t2.start();
        t3.start();

    }
}
