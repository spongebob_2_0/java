package demo6;

import demo5.ThreadPool;
import org.omg.PortableServer.ThreadPolicy;

import java.util.concurrent.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-25
 * Time: 6:45
 */
public class Main2 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(3, 20, 30,
                TimeUnit.SECONDS, new LinkedBlockingDeque<>(100), new ThreadPoolExecutor.AbortPolicy());

        FutureTask<String> futureTask = new FutureTask<>(() -> {
            System.out.println(Thread.currentThread().getName() + " 正在执行任务!");
            TimeUnit.SECONDS.sleep(3);
            return "任务完成!!";
        });
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + " 正在执行任务!");
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        for (int i = 0; i < 10; i++) {
                poolExecutor.execute(futureTask);
        }
//        Thread thread = new Thread(futureTask, "线程1");
//        thread.setDaemon(true);
//        thread.start();
        System.out.println("线程1 " + futureTask.get());
        System.out.println(Thread.currentThread().getName() + " 正在忙其它任务!");
        poolExecutor.shutdown();

    }
}
