package demo5;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-24
 * Time: 15:09
 */
public class Main {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(3);
        ExecutorService threadPool = Executors.newCachedThreadPool();
        for (int i = 0; i < 10; i++) {
            threadPool.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        semaphore.acquire();
                        System.out.println("获得许可证");
                        semaphore.release();
                        System.out.println("释放许可证");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
//        threadPool.shutdown();

    }
}
