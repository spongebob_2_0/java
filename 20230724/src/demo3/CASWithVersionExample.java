package demo3;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicStampedReference;

public class CASWithVersionExample {
    private static final AtomicStampedReference<Integer> count = new AtomicStampedReference<>(1, 1);

    public static void main(String[] args) {
        // 创建两个线程
        Thread thread1 = new Thread(() -> {
            int stamp = count.getStamp(); // 获取初始版本号
            int oldValue = count.getReference(); // 获取初始值
            try {
                //此处等待1秒是为了让线程1也拿到初始版本号和值
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("线程1: 初始值是 " + oldValue + ", 初始版本号是 " + stamp);

            // 模拟线程执行过程中被其他线程干扰，使得 oldValue 被修改
            count.compareAndSet(oldValue, 2, stamp, stamp + 1); // 将变量的值由1改为2
            System.out.println("线程1: 新值是 " + count.getReference() + ", 新版本号是 " + count.getStamp());

            // 模拟操作完成之后，变量值又被改回1
            count.compareAndSet(2, 1, count.getStamp(), count.getStamp() + 1); // 将变量的值由2改回1
            System.out.println("线程1: 最新值是 " + count.getReference() + ", 最新版本号是 " + count.getStamp());
        });

        Thread thread2 = new Thread(() -> {
            int stamp = count.getStamp(); // 获取初始版本号
            int oldValue = count.getReference(); // 获取初始值
            System.out.println("线程2: 初始值是 " + oldValue + ", 初始版本号是 " + stamp);

            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // 模拟操作过程中，变量的值被其他线程改为了2
            boolean flag = count.compareAndSet(oldValue, 3, stamp, stamp + 1);// 将变量的值由1改为3，此时线程A已经将变量的值由1改为2
            System.out.println("线程2是否修改成功: " + flag);
            System.out.println("线程2: 新值是 " + count.getReference() + ", 新版本号是 " + count.getStamp() );
        });

        thread1.start();
        thread2.start();
    }
}
