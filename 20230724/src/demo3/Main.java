package demo3;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-24
 * Time: 4:10
 */
//资源类
class House {
    int saleCount = 0;
    public synchronized void saleHouse () {
        ++saleCount;
    }

//    ThreadLocal<Integer> saleVolume = new ThreadLocal<Integer>(){
//        @Override
//        protected Integer initialValue() {
//            return 0;
//        }
//    };

    ThreadLocal<Integer> saleVolume = ThreadLocal.withInitial(()-> 0);
    public void saleHouseByThreadLocal() {
        saleVolume.set(1+saleVolume.get());
    }
}
public class Main {
    public static void main(String[] args) throws InterruptedException {
        House house = new House();

        for (int i = 0; i < 5; i++) {
            new Thread(()->{
                int size = new Random().nextInt(5) + 1;
//                System.out.println(size);
                try {
                    for (int j = 1; j < size; j++) {
                        house.saleHouse();
                        house.saleHouseByThreadLocal();
                    }
                    System.out.println(Thread.currentThread().getName() + "号销售卖出" + house.saleVolume.get());
                } finally {
                    house.saleVolume.remove();
                }
            },String.valueOf(i)).start();
        }
        TimeUnit.MICROSECONDS.sleep(300);
        System.out.println(Thread.currentThread().getName()  + " 共卖出了" + house.saleCount + "套") ;

    }
}
