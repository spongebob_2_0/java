package demo3;

import javax.print.DocFlavor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-24
 * Time: 4:37
 */

class MyData {
    ThreadLocal<Integer> threadLocal = ThreadLocal.withInitial(()->0);
    public void add() {
        threadLocal.set(1+threadLocal.get());
    }
}
public class Main2 {
    public static void main(String[] args) throws InterruptedException {
        MyData myData = new MyData();
        ExecutorService threadPool = Executors.newFixedThreadPool(3);
        try {
            for (int i = 0; i < 10; i++) {
                threadPool.submit(()->{
                    try {
                        Integer beforeInt = myData.threadLocal.get();
                        myData.add();
                        Integer afterInt = myData.threadLocal.get();
                        System.out.println(Thread.currentThread().getName() + " ,beforeInt= " + beforeInt + " ,afterInt= " + afterInt);
                    } finally {
                        myData.threadLocal.remove();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            threadPool.shutdown();
        }
    }
}
