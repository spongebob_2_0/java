/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-10
 * Time: 17:47
 */

import java.util.*;

public class Role {
    private String name;
    private int blood;

    public Role() {
    }

    public Role(String name, int blood) {
        this.name = name;
        this.blood = blood;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBlood() {
        return blood;
    }

    public void setBlood(int blood) {
        this.blood = blood;
    }

    //定义一个方法用来攻击别人
    //思考:谁攻击谁?
    //Role r1 = new Role();
    //Role r2 = new Role();
    //r1.攻击(r2);
    //方法的调用者去攻击参数
    public void attack(Role role){
        //this表示方法的调用者
        //计算造成的伤害1~20;
        Random r =new Random();
        int hurt =r.nextInt(20)+1;

        //修改一下挨揍的人的血量
        //剩余血量
        int remainboold = role.getBlood() - hurt;

        //对剩余血量做一个验证,如果为负数了,就修改为0
        remainboold = remainboold< 0 ? 0:remainboold;

        //修改一下挨揍人的血量
        role.setBlood(remainboold);
        System.out.println(this.getName()+"举起了拳头打了"+role.getName()+
                "一下,造成了"+hurt+"点伤害,"+role.getName()+"还剩下"+remainboold+"血量");
    }
}
