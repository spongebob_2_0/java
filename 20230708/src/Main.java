import java.util.HashMap;
import java.util.Map;

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

class Solution {
    Map<Integer,Integer> map;
    public TreeNode buildTree(int[] inorder, int[] postorder) {
        map = new HashMap<>();
        for(int i = 0; i < inorder.length; i++) {
            map.put(inorder[i],i);
        }
        return findNode(inorder,0,inorder.length,postorder,0,postorder.length);
    }
    private TreeNode findNode(int[] inorder,int inBegin,int inEnd,int[] postorder,
                              int postBegin, int postEnd) {
        if(inBegin >= inEnd || postBegin >= postEnd) {
            return null;
        }
        int rootIndex = map.get(postorder[postEnd-1]);
        TreeNode root = new TreeNode(inorder[rootIndex]);
        int lenOfLeft = rootIndex - inBegin; //保存中序左子树个数,用来确定后序序列的个数
        root.left = findNode(inorder,inBegin,rootIndex,postorder,postBegin,
                postBegin+lenOfLeft);
        root.right = findNode(inorder,rootIndex+1,inEnd,postorder,
                postBegin+lenOfLeft,postEnd-1);
        return root;
    }
}