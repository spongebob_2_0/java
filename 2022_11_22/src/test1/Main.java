package test1;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: wu
 * @Date: 2021/3/11 10:38
 */
public class Main {
	public static String id = "m0_53882348";//主页后面那段就是你的id https://blog.csdn.net/u010756046

	public static List<Article> list;

	private static int oldAccessCount;

	public static String open(String url) {
		HttpURLConnection conn = null;
		BufferedReader in = null;
		BufferedWriter out = null;

		try {
			URL u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();
			conn.setRequestMethod("GET"); // 设置请求方式
			if (conn.getResponseCode() == 200) {
				in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
				String line;
				StringBuilder str = new StringBuilder();
				while ((line = in.readLine()) != null) {
					str.append(line);
				}
				return str.toString();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (conn != null) {
				conn.disconnect();
			}
		}
		return null;
	}

	public static String formatString(double data) {
		DecimalFormat df = new DecimalFormat("#,###.00");
		return df.format(data);
	}

	public static String formatString(int data) {
		DecimalFormat df = new DecimalFormat("#,###");
		return df.format(data);
	}

	public static List<Article> getArticle() {
		List<Article> list = new ArrayList<>();
		int page = 1;
		while (true) {
			String text = open("https://blog.csdn.net/community/home-api/v1/get-business-list?page="
					+ page + "&size=100&businessType=blog&orderby=&noMore=false&username=" + id);
			JSONObject data = JSON.parseObject(text).getJSONObject("data");
			if (data == null) {
				break;
			}
			JSONArray jsonArray = data.getJSONArray("list");
			if (jsonArray == null || jsonArray.isEmpty()) {
				break;
			}
			for (int i = 0; i < jsonArray.size(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				Article article = new Article(jsonObject.getString("title"), jsonObject.getString("url"));
				list.add(article);
			}
			page++;
		}
		return list;
	}


	public static int getAccessCount() {
		int start, end;
		String text = open("https://blog.csdn.net/" + id);
		start = text.indexOf("user-profile-statistics-num");
		start = text.indexOf(">", start) + 1;
		end = text.indexOf("<", start);
		String subText = text.substring(start, end).replace(",", "");

		// 检查字符串是否为空
		if (subText == null || subText.isEmpty()) {
			return 0;  // 如果为空，返回默认值
		} else {
			try {
				return Integer.parseInt(subText);  // 否则尝试解析为整数
			} catch (NumberFormatException e) {
				e.printStackTrace();
				return 0;  // 如果解析失败，返回默认值
			}
		}
	}



	public static void main(String[] args) {
		System.out.println("正在访问主页...");
		oldAccessCount = getAccessCount();
		System.out.println("正在获取博文...");
		list = getArticle();
		for (Article article : list) {
			System.out.println(article.url + " " + article.title);
		}
		System.out.println("获取博文成功 " + list.size() + "篇（如果您的博文超过100篇请自行修改代码）");

		while (true) {
			for (Article article : list) {
				System.out.println("正在打开:" + article.url + " " + article.title);
				open(article.url);
			}
			System.out.println("初始访问量:" + formatString(oldAccessCount));
			int accessCount = getAccessCount();
			System.out.println("当前访问量:" + formatString(accessCount));
			System.out.println("访问量增加:" + formatString(accessCount - oldAccessCount));
			System.out.println("点赞加关注爱你哟");
			try {
				Thread.sleep(60000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
class Article {
	public final String title;

	public final String url;

	public Article(String title, String url) {
		this.title = title;
		this.url = url;
	}
}

