package com.example.demo.common;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-19
 * Time: 0:29
 */
//@ControllerAdvice  //表示这个类将被用于全局的异常处理
//@ResponseBody      //表示该类返回的是json数据
public class MyExceptionAdvice {

    //这个注解表示当应用程序中发生NullPointerException时，会调用此方法进行处理
    @ExceptionHandler(NullPointerException.class)
    //这个方法返回一个HashMap，其中包含了异常处理的结果。结果以键值对的形式存储，键是字符串，值是Object对象。
    public HashMap<String,Object> doNullPointerException(NullPointerException e) {
        HashMap<String ,Object> result = new HashMap<>();
        result.put("code",-1);   //"code"：异常的状态码，这里是-1，表示发生了错误
        //"msg"：异常的信息，这里添加了"空指针"的前缀，并附加上异常的具体信息（e.getMessage()获取异常的信息）。
        result.put("msg","空指针" + e.getMessage());
        //data"：这里设置为null，表示没有返回的数据。
        result.put("data", null);
        return result;
    }

    /**
     * 默认的异异常处理(当具体的异常匹配不到时, 会执行次方法)
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    public HashMap<String,Object> doException(Exception e) {
        HashMap<String ,Object> result = new HashMap<>();
        result.put("code",-300);   //"code"：异常的状态码，这里是-300，表示发生了错误
        //"msg"：异常的信息，这里添加了"空指针"的前缀，并附加上异常的具体信息（e.getMessage()获取异常的信息）。
        result.put("msg","Exception" + e.getMessage());
        //data"：这里设置为null，表示没有返回的数据。
        result.put("data", null);
        return result;
    }
}
