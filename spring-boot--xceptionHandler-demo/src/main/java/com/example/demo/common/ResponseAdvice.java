package com.example.demo.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * Description: 统一数据格式处理
 * User: WuYimin
 * Date: 2023-06-19
 * Time: 2:00
 */
//定义的全局响应体建议（Response Body Advice）。
//其作用是在返回数据到客户端之前，对返回的数据进行处理或修改
@ControllerAdvice
public class ResponseAdvice implements ResponseBodyAdvice {

    @Autowired
    private ObjectMapper objectMapper;
    //是否执行 beforeBodyWrite 方法,true = 执行, 重写返回结果
    @Override
    //supports方法，用于决定是否对返回的数据进行处理
    public boolean supports(MethodParameter returnType, Class converterType) {
        return true;
    }

    //返回数据之前进行数据重写
    @SneakyThrows
    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        if(body instanceof HashMap) {
            return body;
        }
        //重写返回结果, 让其返回一个统一的数据格式
        HashMap<String, Object> result = new HashMap<>();
        result.put("code", 200);
        result.put("data", body);
        result.put("msg", "");
//        if(body instanceof String) {
//            //将String类型的字符串转换成json格式的字符串返回
//            return objectMapper.writeValueAsString(result);
//        }
        return result;
    }
}
