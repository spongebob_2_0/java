package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-19
 * Time: 0:44
 */
@RestController  //它结合了@Controller和@ResponseBody两个注解的功能。
//这个类级别的注解指定了这个类处理的所有请求的URL路径的公共部分
@RequestMapping("/user")
public class UserController {

    @RequestMapping("/login")
    public int login() {
        Object obj = null;
        //因为obj是null，所以当调用hashCode方法时，会抛出NullPointerException。
        System.out.println(obj.hashCode());
//        int num = 10 / 0;
        return 1;
    }
    @RequestMapping("/login1")
    public int login1() {
        return 1;
    }
    @RequestMapping("/login2")
    public int login2() {
//        Object obj = null;
//        //因为obj是null，所以当调用hashCode方法时，会抛出NullPointerException。
//        System.out.println(obj.hashCode());
        int num = 10 / 0;
        return 1;
    }
    @RequestMapping("/reg")
    public HashMap<String, Object> reg() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("code", 200);
        result.put("msg", "");
        result.put("data", 1);
        return result;
    }
    @RequestMapping("/sayHi")
    public String sayHi() {
        return "say hi";
    }
}
