package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-19
 * Time: 4:40
 */
@Configuration
public class MyConfig implements WebMvcConfigurer {
    @Override
    //configureMessageConverters 方法使用 removeIf 方法删除了所有 StringHttpMessageConverter 的实例。
    // 这样，Spring MVC 就不会再使用 StringHttpMessageConverter 来处理 String 类型的数据了。
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.removeIf(converter -> converter instanceof StringHttpMessageConverter);
    }
}
