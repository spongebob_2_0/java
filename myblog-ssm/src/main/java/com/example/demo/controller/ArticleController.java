package com.example.demo.controller;

import cn.hutool.core.lang.Range;
import com.example.demo.common.*;
import com.example.demo.entity.ArticleInfo;
import com.example.demo.entity.Userinfo;
import com.example.demo.entity.vo.ArticleInfoVo;
import com.example.demo.service.ArticleService;
import com.example.demo.service.CollectService;
import com.example.demo.service.LikeService;
import com.example.demo.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-20
 * Time: 14:30
 */
@RestController
@RequestMapping("/art")
public class ArticleController {
    @Autowired
    private ArticleService articleService;

    @Autowired
    private LikeService likeService;

    @Autowired
    private CollectService collectService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserService userService;

    /**
     * 添加一篇新的文章
     * @param articleInfo
     * @param request
     * @return
     */
    @RequestMapping("/add")
    public AjaxResult add(ArticleInfo articleInfo, HttpServletRequest request) {
        //1.非空效验
        if(articleInfo == null || !StringUtils.hasLength(articleInfo.getTitle())||
                                  !StringUtils.hasLength(articleInfo.getContent())) {
            return AjaxResult.fail(-1,"参数异常");
        }
        //2.组装数据,得到uid
        Userinfo userinfo = UserSessionTools.getLoginUser(request);
        articleInfo.setUid(userinfo.getId());
        //3.持久化,将结果返回给前端
        int result = articleService.add(articleInfo);
        List<ArticleInfo> articleInfos = userService.getArticleInfo(userinfo.getId());
        //拿到该用户所有文章后,只添加最后一篇文章入redis
        for(int i = 0; i < articleInfos.size(); i++) {
            if(i == articleInfos.size()-1) {
                redisTemplate.opsForHash().put("hashUserId", "articleInfo:" + articleInfos.get(i).getId(), userinfo.getId());
            }
        }
        return AjaxResult.success(result);
    }

    /**
     * 获取文章的详细的信息,但需要鉴权(判断权限的归属人是否登录)
     * @param id
     * @param request
     * @return
     */
    @RequestMapping("/getdetailbyid")
    public AjaxResult getDetailByIdAndUid(Integer id, HttpServletRequest request) {
        if(id == null || id <= 0) {
            return AjaxResult.fail(-1,"非法参数");
        }
        Userinfo userinfo = UserSessionTools.getLoginUser(request);
        return AjaxResult.success(articleService.getDetailByIdAndUid(id, userinfo.getId()));
    }

    /**
     * 更新一篇文章，需要鉴权
     * @param articleInfo
     * @param request
     * @return
     */
    @RequestMapping("/update")
    public AjaxResult update(ArticleInfo articleInfo, HttpServletRequest request) {
        //1.非空效验
        //验证articleInfo对象不为null，以防止空指针异常。
        //确认文章的id值大于0，这在许多系统中是一个常见的约定，因为id通常被看作是正整数。
        //确保文章的title和content属性都不为空。这通过使用StringUtils.hasLength()函数来判断字符串是否有长度（即它既不是null，也不是空字符串）。
        if(articleInfo == null || articleInfo.getId() <= 0 ||
                !StringUtils.hasLength(articleInfo.getTitle()) ||
                !StringUtils.hasLength(articleInfo.getContent())) {
            return AjaxResult.fail(-1,"参数有误! ");
        }
        //2.获取登录用户的id,填充到articleInfo对象中 (因为在修改时要验证权限)
        Userinfo userinfo = UserSessionTools.getLoginUser(request);
        articleInfo.setUid(userinfo.getId());
        articleInfo.setUpdatetime(LocalDateTime.now());
        int result = articleService.update(articleInfo);
        // 构造Redis中的文章ID键
        String articleID = "articleID:" + articleInfo.getId();
        stringRedisTemplate.delete(articleID);
        return AjaxResult.success(result);
    }

    /**
     * 根据文章ID获取文章详细信息，不需要鉴权
     * @param id
     * @return
     */
    @RequestMapping("/getdetail")
    public AjaxResult getDetail(Integer id) throws JsonProcessingException {
        // 检查参数是否合法
        if (id == null || id <= 0) {
            return AjaxResult.fail(-1, "参数有误!");
        }
        // 构造Redis中的文章ID键
        String articleID = "articleID:" + id;
        // 从Redis中获取文章详情的JSON字符串
        String result = stringRedisTemplate.opsForValue().get(articleID);
        // 如果在Redis中找到结果，则反序列化成对象并返回
        if (result != null) {
            ArticleInfoVo articleInfoVo = objectMapper.readValue(result, ArticleInfoVo.class);
            return AjaxResult.success(articleInfoVo);
        }
        // 如果Redis中没有找到，则从数据库或其他源获取文章详情
        ArticleInfoVo articleInfoVo = articleService.getDetail(id);
        // 将文章详情序列化为JSON字符串
        String articleInfoVoAsString = objectMapper.writeValueAsString(articleInfoVo);
        // 将文章详情存储到Redis中，有效期为30分钟
        stringRedisTemplate.opsForValue().set(articleID, articleInfoVoAsString, 30, TimeUnit.MINUTES);
        // 返回文章详情
        return AjaxResult.success(articleInfoVo);
    }




    /**
     * 增加一篇文章的阅读数量
     * @param id
     * @return
     */
    @RequestMapping("/addrcount")
    public AjaxResult addRCount(Integer id) {
        if(id == null || id <= 0) {
            return AjaxResult.fail(-1,"参数有误!");
        }
        int result = articleService.addRCount(id);
        ArticleInfoVo articleInfoVo = articleService.getDetail(id);
        return AjaxResult.success(articleInfoVo.getRcount());
    }

    /**
     * 获取当前登录用户的文章列表
     * @param request
     * @return
     */
    @RequestMapping("/mylist")
    public AjaxResult myList(HttpServletRequest request) {
        Userinfo userinfo = UserSessionTools.getLoginUser(request);
        List<ArticleInfo> list = articleService.getListByUid(userinfo.getId());
        // 将文章正文截取成文章摘要
        for(ArticleInfo item: list) {
            // 使用正则表达式去除Markdown关键字
            String contentWithoutMarkdown = MarkdownUtils.removeMarkdown(item.getContent());
            String content = StringTools.subLength(contentWithoutMarkdown,200);
            item.setContent(content);
        }
        return  AjaxResult.success(list);
    }

    /**
     * 删除一篇文章，需要鉴权
     * @param id
     * @param request
     * @return
     */
    @RequestMapping("/del")
    public AjaxResult del(Integer id, HttpServletRequest request) {
        if(id == null || id <= 0) {
            return AjaxResult.fail(-1,"参数有误! ");
        }
        Userinfo userinfo = UserSessionTools.getLoginUser(request);
        int result = articleService.del(id, userinfo.getId());
        //将该文章id从redis的hash表中删除
        redisTemplate.opsForHash().delete("hashUserId","articleInfo:"+id);
        return  AjaxResult.success(result);
    }

    /**
     * 分页获取文章列表
     * @param pageSize
     * @param pageIndex
     * @return
     */
    @RequestMapping("/getlistbyPage")
    public AjaxResult getListByPage(Integer pageSize, Integer pageIndex) {
        //默认值处理
        if(pageSize == null || pageSize == 0) {
            pageSize = 3;
        }
        if(pageIndex == null || pageIndex < 1) {
            pageIndex = 1;
        }
        //分页公式
        int offset = (pageIndex-1) * pageSize;
        List<ArticleInfo> list = articleService.getListByPage(pageSize, offset);
        //多线程并发循环
        //list.stream().parallel()获取流对象并转化为并行流
        list.stream().parallel().forEach(item ->{
            // 使用正则表达式去除Markdown关键字
            String contentWithoutMarkdown = MarkdownUtils.removeMarkdown(item.getContent());
            //在 forEach() 方法中，对并行流中的每个元素进行操作。
            // 对于每个 ArticleInfo 对象 item，通过调用 item.setContent(...) 来设置文章内容。
            item.setContent(StringTools.subLength(contentWithoutMarkdown, 200));
        });
        return AjaxResult.success(list);
    }

    /**
     * 获取文章总数
     * @return
     */
    @RequestMapping("/getcount")
    public AjaxResult getCount() {
        Integer count = articleService.getCount();
        return AjaxResult.success(count);
    }

    /**
     * 获取指定用户的文章数量
     */
    @RequestMapping("/getusercount")
    public AjaxResult getUserCount(String username) {
        if(!StringUtils.hasLength(username)) {
            return AjaxResult.fail(-1,"参数有误! ");
        }
        int result = articleService.getArticleCountByUsername(username);
        return AjaxResult.success(result);
    }




    //点赞
    @RequestMapping("/toggle")
    public AjaxResult toggleLike(@RequestParam("articleId") int articleId,HttpServletRequest request) {
        if(UserSessionTools.getLoginUser(request) == null) {
            //未登录
            return AjaxResult.fail(-1,"请先登录! ");
        }
        Userinfo userinfo = UserSessionTools.getLoginUser(request);
        int result = likeService.toggleLike(userinfo.getId(), articleId);
        return AjaxResult.success(result);
    }


    //收藏
    @RequestMapping("/collect")
    public AjaxResult toggleCollect(@RequestParam("articleId") int articleId,HttpServletRequest request) {
        if(UserSessionTools.getLoginUser(request) == null) {
            //未登录
            return AjaxResult.fail(-1,"请先登录");
        }
        Userinfo userinfo = UserSessionTools.getLoginUser(request);
        int result = collectService.toggleCollect(userinfo.getId(), articleId);
        return AjaxResult.success(result);
    }

    //获取点赞数量
    @RequestMapping("/getlike")
    public AjaxResult getLike(@RequestParam("articleId") int articleId,HttpServletRequest request) {
//        if(UserSessionTools.getLoginUser(request) == null) {
//            //未登录
//            return AjaxResult.fail(-1,"参数错误");
//        }
        Userinfo userinfo = UserSessionTools.getLoginUser(request);
        int result = likeService.getLikeCount(articleId);
        return AjaxResult.success(result);
    }


    //获取收藏数量
    @RequestMapping("/getcoolect")
    public AjaxResult getCoolect(@RequestParam("articleId") int articleId,HttpServletRequest request) {
//        if(UserSessionTools.getLoginUser(request) == null) {
//            //未登录
//            return AjaxResult.fail(-1,"参数错误");
//        }
        Userinfo userinfo = UserSessionTools.getLoginUser(request);
        int result = collectService.getCollectCount(articleId);
        return AjaxResult.success(result);
    }

    /**
     * 查询博客
     * @param keyword
     * @return
     */
    @RequestMapping("/fuzzysearchblog")
    public AjaxResult fuzzySearchBlog(String keyword) {
//        //默认值处理
//        if(pageSize == null || pageSize == 0) {
//            pageSize = 3;
//        }
//        if(pageIndex == null || pageIndex < 1) {
//            pageIndex = 1;
//        }
//        //分页公式
//        int offset = (pageIndex-1) * pageSize;
        List<ArticleInfo> list = articleService.fuzzySearchBlog(keyword);
        //多线程并发循环
        //list.stream().parallel()获取流对象并转化为并行流
        list.stream().parallel().forEach(item ->{
            // 使用正则表达式去除Markdown关键字
            String contentWithoutMarkdown = MarkdownUtils.removeMarkdown(item.getContent());
            //在 forEach() 方法中，对并行流中的每个元素进行操作。
            // 对于每个 ArticleInfo 对象 item，通过调用 item.setContent(...) 来设置文章内容。
            item.setContent(StringTools.subLength(contentWithoutMarkdown, 200));
        });
        return AjaxResult.success(list);
    }


    @RequestMapping("/fuzzysearchmyblog")
    public AjaxResult fuzzySearchMyBlog(String keyword,HttpServletRequest request) {
//        //默认值处理
//        if(pageSize == null || pageSize == 0) {
//            pageSize = 3;
//        }
//        if(pageIndex == null || pageIndex < 1) {
//            pageIndex = 1;
//        }
//        //分页公式
//        int offset = (pageIndex-1) * pageSize;
        Userinfo userinfo = UserSessionTools.getLoginUser(request);
        List<ArticleInfo> list = articleService.fuzzySearchMyBlog(keyword,userinfo.getId());
        //多线程并发循环
        //list.stream().parallel()获取流对象并转化为并行流
        list.stream().parallel().forEach(item ->{
            // 使用正则表达式去除Markdown关键字
            String contentWithoutMarkdown = MarkdownUtils.removeMarkdown(item.getContent());
            //在 forEach() 方法中，对并行流中的每个元素进行操作。
            // 对于每个 ArticleInfo 对象 item，通过调用 item.setContent(...) 来设置文章内容。
            item.setContent(StringTools.subLength(contentWithoutMarkdown, 200));
        });
        return AjaxResult.success(list);
    }

}
