package com.example.demo.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.CircleCaptcha;
import com.example.demo.common.*;
import com.example.demo.entity.ArticleInfo;
import com.example.demo.entity.CommentTable;
import com.example.demo.entity.Userinfo;
import com.example.demo.entity.vo.UserinfoVo;
import com.example.demo.service.UserService;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.code.kaptcha.Producer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;


/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-20
 * Time: 14:29
 */
@Slf4j //类上添加@Slf4j注解后，在类中就可以直接使用log对象
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private HttpSession session;

    @Autowired  //操作邮件发送的对象
    private MailUtils mailUtils;

    @Autowired  //生成随机验证码的对象
    private Producer producer;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${img.routePath}")
    private String routePath; //imgPath通过注解获取到了路由的映射

    @Value("${img.path}")
    private String imgPath; //imgPath通过注解获取到了文件保存的路径


    /**
     * 发送验证码
     * @param postbox
     * @return
     */
    @RequestMapping("/getcode")
    public AjaxResult contextLoad(@RequestParam String postbox) {
        try {
            // 使用正则表达式进行验证邮箱格式
            String emailRegex = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$";
            boolean isValidEmail = Pattern.matches(emailRegex, postbox);
            if (!isValidEmail) {
                return AjaxResult.fail(-1, "无效的邮箱地址");
            }
            String s = producer.createText();
            ValueOperations valueOperations = redisTemplate.opsForValue();
            mailUtils.sendMail(postbox,"验证码: " + s,"邮件注册验证");
                valueOperations.set("regCode" , s, 120, TimeUnit.SECONDS);
            return AjaxResult.success(1);
        } catch (Exception e) {
            log.error("方法执行出错! ", e);
            return AjaxResult.fail(-1, "参数有误! ");
        }
    }

    /**
     * 注册账号
     * @param userinfo
     * @return
     */
    @RequestMapping("/reg")
    public AjaxResult reg(String code,UserinfoVo userinfo) {
        // 后端需要进行非空校验吗
        // 铁律: 后端开发永远不要相信前端!!
        // 1.进行非空判断
        if(userinfo == null || !StringUtils.hasLength(userinfo.getUsername())
                || !StringUtils.hasLength(userinfo.getPassword())) {
            return AjaxResult.fail(-1,"参数有误");
        }

        String value = (String) redisTemplate.opsForValue().get("regCode");
        if(value == null) {
            return AjaxResult.fail(-1,"验证码已过期");
        } else if(!value.equalsIgnoreCase(code)) {
            return AjaxResult.fail(-1,"验证码不正确");
        }
        // 调用login方法根据用户名查询数据库是否存在, 存在就无法注册
        UserinfoVo user = userService.login(userinfo.getUsername());
        if(user != null ) {
            return AjaxResult.fail(-1,"该用户已经注册");
        }
        UserinfoVo user2 = userService.getUserByEmail(userinfo.getEmail());
        if(user2 != null) {
            return AjaxResult.fail(-2,"该邮箱已被注册! ");
        }
        // 2.调用 UserService 执行添加方法, 并将返回的结果添加 AjaxResult.data 里面进行返回
        // 将密码进行加盐加密
        userinfo.setPassword(PasswordTools.encrypt(userinfo.getPassword()));
        int result = userService.reg(userinfo);
        if(result > 0) {
            //注册成功删除验证码,防止验证码被被滥用
            redisTemplate.delete("regCode");
        }
        return AjaxResult.success(result);
    }



    /**
     * 登录成功会把用户的信息存到session里面去
     * @param username
     * @param password
     * @param request
     * @return
     */
    @RequestMapping("/login")
    public AjaxResult login(String username, String password, String captcha, HttpServletRequest request) {
        if (!StringUtils.hasLength(username) || !StringUtils.hasLength(password) || !StringUtils.hasLength(captcha)) {
            return AjaxResult.fail(-1, "参数有误");
        }

        // 检查验证码
        String sessionCaptcha = (String) request.getSession().getAttribute("SESSION_CAPTCHA");
        if (sessionCaptcha == null || !sessionCaptcha.equalsIgnoreCase(captcha)) {
            return AjaxResult.fail(-3, "验证码有误");
        }

        UserinfoVo userinfo = userService.login(username);
        if (userinfo == null || userinfo.getId() <= 0) {
            return AjaxResult.fail(-2, "用户名或者密码有误");
        }
        if (!PasswordTools.decrypt(password, userinfo.getPassword())) {
            return AjaxResult.fail(-2, "用户名或者密码有误");
        }
        // 将当前成功登录的用户信息存储到session中，getSession方法如果有就存，如果没有就会创建一个会话
        HttpSession session = request.getSession();
        session.setAttribute(ApplicationVariable.SESSION_KEY_USERINFO, userinfo);
        return AjaxResult.success(1);
    }

    /**
     * 获取验证码
     * @param response
     * @param request
     * @throws IOException
     */
    @GetMapping("code")
    void getCode(HttpServletResponse response, HttpServletRequest request) throws IOException {

        // 利用 hutool 工具，生成验证码图片资源
        CircleCaptcha captcha = CaptchaUtil.createCircleCaptcha(100, 40, 4, 5);
        // 获得生成的验证码字符
        String code = captcha.getCode();
        // 利用 session 来存储验证码
        HttpSession session = request.getSession();
        session.setAttribute("SESSION_CAPTCHA", code);
        // 将验证码图片的二进制数据写入【响应体 response 】
        captcha.write(response.getOutputStream());
    }

    /**
     * 退出操作
     * @param request
     * @return
     */
    @RequestMapping("/logout")
    public AjaxResult logout(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        session.removeAttribute(ApplicationVariable.SESSION_KEY_USERINFO);
        return AjaxResult.success(1);
    }

    /**
     * 判断当前用户是否已经登录
     * @param request
     * @return
     */
    @RequestMapping("/islogin")
    public AjaxResult isLogin(HttpServletRequest request) {
        if(UserSessionTools.getLoginUser(request) == null) {
            //未登录
            return AjaxResult.success(0);
        }
        return AjaxResult.success(1);
    }


    /**
     * 将用户上传的图片保存到指定路径中,并将该路径存储到数据库中
     * @param request
     * @param file
     * @return
     */
    @RequestMapping("/upload")
    public AjaxResult upload (HttpServletRequest request, @RequestParam("myfile")MultipartFile file) {
        if(file.isEmpty()) {
            return AjaxResult.fail(-1,"参数有误! ");
        }
        //得到原图片的名称
        String filename = file.getOriginalFilename();
        //得到图片后缀
        filename.substring(filename.lastIndexOf("."));
        //生成不重名的文件名
        filename = UUID.randomUUID().toString() + filename;
        //File对象就是一个描述文件路径的对象(只是描述,并不创建)
        File destinationFile = new File(imgPath, filename);
        if(!destinationFile.exists()) {
            //如果文件名不存在就创建
            destinationFile.mkdirs();
        }
        try {
            //transferTo 方法需要知道具体的文件路径（包括目录和文件名），才能将上传的文件保存到正确的位置。
            // 如果你只提供了目录路径，它无法确定应该将上传的文件保存为哪个文件.
            file.transferTo(destinationFile);
            // 用于存储在数据库中的图片路径
            String imgPathForDb = routePath + filename;
            Userinfo userinfo = UserSessionTools.getLoginUser(request);
            //更新用户的图片路径
            userService.updateUserImgPath(userinfo.getUsername(), imgPathForDb);
            List<ArticleInfo> articleInfos = userService.getArticleInfo(userinfo.getId());
            //拿到该用户的所有文章后,批量加入到redis的hash表中 (文章id作为hash表的key,用户id是hash表的value)
            for(ArticleInfo articleInfo: articleInfos) {
                redisTemplate.opsForHash().put("hashUserId","articleInfo:"+articleInfo.getId(),userinfo.getId());
            }
            stringRedisTemplate.opsForValue().set("userinfoPicture:" + userinfo.getId(),imgPathForDb);
            //将头像路径存作为value进redis里面,key是用户id
            return AjaxResult.success(articleInfos);
         } catch (IOException e) {
            log.error("图片上传失败! " + e.getMessage());
        }
        return AjaxResult.fail(-1,"参数有误! ");
    }


    /**
     * 根据文章id获取用户头像路径
     * @param
     * @return
     */
    @RequestMapping("/getuserpicture")
    public AjaxResult getUserPicture(Integer id) throws JsonProcessingException {
        String articleInfoID = "articleInfo:" +id;
        Integer userId = (Integer) redisTemplate.opsForHash().get("hashUserId", articleInfoID);
        String result = stringRedisTemplate.opsForValue().get("userinfoPicture:" + userId);
        if(result != null) {
            return AjaxResult.success(result);
        }
        String result1 = userService.getUserPicture(id);
        return AjaxResult.success(result1);
    }


    /**
     * 获取登录用户的头像
     * @param request
     * @return
     */
    @RequestMapping("/getuserlogopicture")
    public AjaxResult getuserLogoPicture(HttpServletRequest request) {
        Userinfo userinfo = UserSessionTools.getLoginUser(request);
        if(userinfo == null) {
            return AjaxResult.fail(-1,"未登录Logo不设置个人头像!");
        }
        String result = stringRedisTemplate.opsForValue().get("userinfoPicture:" + userinfo.getId());
        if(result != null) {
            return AjaxResult.success(result);
        }
        //根据用户id去redis获取照片路径,如果找到了就返回,没找到就去数据库查找
        Userinfo user = userService.getUserLogoPicture(userinfo.getId());
        return AjaxResult.success(user);
    }


    /**
     * 通过原密码修改密码
     * @param userinfo
     * @return
     */
    @RequestMapping("/updatepassword")
    public AjaxResult updatePassword(UserinfoVo userinfo){
        UserinfoVo user = userService.login(userinfo.getUsername());
        if(user == null) {
            return AjaxResult.fail(-1,"该用户不存在! ");
        }
        if(PasswordTools.decrypt(userinfo.getPassword(),user.getPassword())) {
            // 2.调用 UserService 执行添加方法, 并将返回的结果添加 AjaxResult.data 里面进行返回
            // 将密码进行加盐加密
            userinfo.setNewPassword(PasswordTools.encrypt(userinfo.getNewPassword()));
            int result = userService.updateUserPassword(userinfo);
//        if(result > 0) {
//            //注册成功删除验证码,防止验证码被被滥用
//            redisTemplate.delete("regCode");
//        }
            return AjaxResult.success(result);
        }else {
            return AjaxResult.fail(-2,"密码错误!");
        }
    }


    @RequestMapping("/getpassword")
    public AjaxResult forgetPassword(UserinfoVo userinfo,String code) {
        if(userinfo == null || !StringUtils.hasLength(userinfo.getNewPassword())){
            return AjaxResult.fail(-1,"参数有误");
        }

        String value = (String) redisTemplate.opsForValue().get("regCode");
        if(value == null) {
            return AjaxResult.fail(-1,"验证码已过期");
        } else if(!value.equalsIgnoreCase(code)) {
            return AjaxResult.fail(-1,"验证码不正确");
        }
        userinfo.setNewPassword(PasswordTools.encrypt(userinfo.getNewPassword()));
        int result = userService.updateUserPasswordByEmail(userinfo);
        if(result > 0) {
            return AjaxResult.success(1);
        }
        return AjaxResult.fail(-1,"修改失败! ");
    }

    /**
     * 发表评论
     * @param commentTable
     * @param request
     * @return
     */
    @RequestMapping("/addcomment")
    @Transactional
    public AjaxResult addComment(CommentTable commentTable, HttpServletRequest request) {
//        HttpSession session = request.getSession(false);
        Userinfo userinfo = UserSessionTools.getLoginUser(request);
        if(userinfo == null) {
            return AjaxResult.fail(-2,"用户未登录");
        }
        commentTable.setUser_id(userinfo.getId());
        commentTable.setUsername(userinfo.getUsername());
        int result = userService.addComment(commentTable);
        if(result > 0) {
            return AjaxResult.success(commentTable);
        }
        return AjaxResult.fail(-1,"评论发表失败! ");
    }

    /**
     * 加载页面显示所有评论
     * @param article_id
     * @return
     */
    @RequestMapping("/showcomment")
    public AjaxResult showComment(String article_id){
        List<CommentTable> commentTables = userService.showComment(article_id);
        return AjaxResult.success(commentTables);
    }

}
