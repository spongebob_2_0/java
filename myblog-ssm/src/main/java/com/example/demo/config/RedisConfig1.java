//package com.example.demo.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
//import org.springframework.data.redis.serializer.StringRedisSerializer;
//
//@Configuration
//public class RedisConfig1 {
//
//    /**
//     * *redis序列化的工具定置类，下面这个请一定开启配置
//     * 使用Lettuce作为客户端: 它使用Lettuce作为连接Redis的客户端，这是一种可扩展的线程安全的Redis客户端，与Jedis相比，更适合于高并发的场景。
//     * 依赖注入连接工厂: 这个配置接受一个LettuceConnectionFactory参数，允许外部配置连接工厂。
//     * 配置相对简单: 通过将连接工厂作为参数传入，可以让Spring Boot自动配置连接细节，使得配置更简洁。
//     * param LettuceConnectionFactory
//     * return
//     */
//    @Bean
//    public RedisTemplate<String, Object> redisTemplate(LettuceConnectionFactory lettuceConnectionFactory) {
//        RedisTemplate<String,Object> redisTemplate = new RedisTemplate<>();
//        redisTemplate.setConnectionFactory(lettuceConnectionFactory);
//        // 设置key序列化方式string
//        redisTemplate.setKeySerializer(new StringRedisSerializer());
//        // 设置value的序列化方式json，使用GenericJackson2JsonRedisSerializer替换默认序列化
//        redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer());
//
//        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
//        redisTemplate.setHashValueSerializer(new GenericJackson2JsonRedisSerializer());
//        redisTemplate.afterPropertiesSet();
//        return redisTemplate;
//    }
//}