//package com.example.demo.config; // 声明配置的包名
//
//import org.springframework.beans.factory.annotation.Value; // 导入Value注解，用于注入配置文件的值
//import org.springframework.cache.annotation.EnableCaching; // 导入EnableCaching注解，用于启用Spring的缓存支持
//import org.springframework.context.annotation.Bean; // 导入Bean注解，用于声明Spring的bean
//import org.springframework.context.annotation.Configuration; // 导入Configuration注解，用于声明配置类
//import org.springframework.data.redis.connection.RedisPassword; // 导入RedisPassword类，用于设置Redis密码
//import org.springframework.data.redis.connection.RedisStandaloneConfiguration; // 导入RedisStandaloneConfiguration类，用于配置Redis的基本信息
//import org.springframework.data.redis.connection.jedis.JedisConnectionFactory; // 导入JedisConnectionFactory类，用于创建Redis连接
//import org.springframework.data.redis.core.RedisTemplate; // 导入RedisTemplate类，用于操作Redis
//import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer; // 导入序列化器类，用于序列化Redis的值
//import org.springframework.data.redis.serializer.StringRedisSerializer; // 导入序列化器类，用于序列化Redis的键
//
//
///**
// * 这个代码定义了一个Spring配置类，用于配置Redis连接和操作。主要包括了创建一个连接工厂和一个用于操作Redis的模板。
// * 代码通过注解的方式，从配置文件中获取Redis的连接信息，并进行相应的设置。
// * 明确指定连接细节: 它使用RedisStandaloneConfiguration来明确设置Redis的连接细节，如主机、端口、密码等。
// * 使用Jedis作为客户端: 它明确选择了Jedis作为连接Redis的客户端。
// * 配置更复杂: 需要手动从配置文件中提取属性并设置它们，这可能会使配置更加繁琐。
// */
//@Configuration // 声明这是一个配置类
//@EnableCaching // 开启缓存功能
//public class RedisConfig { // 定义Redis配置类
//
//    @Value("${spring.redis.host}") // 从配置文件中获取Redis主机地址，并注入到redisHost变量中
//    private String redisHost;
//
//    @Value("${spring.redis.port}") // 从配置文件中获取Redis端口号，并注入到redisPort变量中
//    private int redisPort;
//
//    @Value("${spring.redis.password}") // 从配置文件中获取Redis密码，并注入到redisPassword变量中
//    private String redisPassword;
//
//    @Value("${spring.redis.database}") // 从配置文件中获取Redis数据库索引，并注入到redisDatabase变量中
//    private int redisDatabase;
//
//    @Bean // 声明一个JedisConnectionFactory的bean
//    public JedisConnectionFactory jedisConnectionFactory() { // 定义创建JedisConnectionFactory的方法
//        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration(redisHost, redisPort); // 创建Redis的独立配置
//        redisStandaloneConfiguration.setPassword(RedisPassword.of(redisPassword)); // 设置Redis的密码
//        redisStandaloneConfiguration.setDatabase(redisDatabase); // 设置Redis的数据库索引
//        return new JedisConnectionFactory(redisStandaloneConfiguration); // 返回JedisConnectionFactory的实例
//    }
//
//    @Bean // 声明一个RedisTemplate的bean
//    public RedisTemplate<String, Object> redisTemplate() { // 定义创建RedisTemplate的方法
//        RedisTemplate<String, Object> template = new RedisTemplate<>(); // 创建一个新的RedisTemplate实例
//        template.setConnectionFactory(jedisConnectionFactory()); // 设置连接工厂
//        template.setKeySerializer(new StringRedisSerializer()); // 设置键的序列化器
//        template.setValueSerializer(new GenericJackson2JsonRedisSerializer()); // 设置值的序列化器
//        template.setHashKeySerializer(new StringRedisSerializer()); // 设置哈希键的序列化器
//        template.setHashValueSerializer(new GenericJackson2JsonRedisSerializer()); // 设置哈希值的序列化器
//        return template; // 返回RedisTemplate的实例
//    }
//}
