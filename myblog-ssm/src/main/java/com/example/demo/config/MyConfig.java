package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-21
 * Time: 15:47
 */
// @Configuration 注解表示这是一个配置类
@Configuration
public class MyConfig implements WebMvcConfigurer {

    @Value("${img.routePath}")
    private String routePath; //imgPath通过注解获取到了路由的映射
    @Value("${img.path}")
    private String imgPath; //imgPath通过注解获取到了文件保存的路径

    // 自动注入名为 loginIntercept 的拦截器
    @Autowired
    private LoginIntercept loginIntercept;

    // 重写 addInterceptors 方法，该方法用于添加拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        // 通过 registry.addInterceptor 方法添加 loginIntercept 拦截器
        registry.addInterceptor(loginIntercept)
                // addPathPatterns 方法用于指定拦截哪些路径，这里指定拦截所有路径
                .addPathPatterns("/**")

                // excludePathPatterns 方法用于指定排除哪些路径，即这些路径不会被拦截器拦截
                // 这里排除了一些静态资源和一些特定的接口，例如登录、注册等
                .excludePathPatterns("/login.html")
                .excludePathPatterns("/amend.html")
                .excludePathPatterns("/error.html")
                .excludePathPatterns("/forget.html")
                .excludePathPatterns("/favicon.ico")
                .excludePathPatterns("/reg.html")
                .excludePathPatterns("/blog_list.html")
                .excludePathPatterns("/blog_content.html")
                .excludePathPatterns("/css/**")
                .excludePathPatterns("/editor.md/**")
                .excludePathPatterns("/img/**")
                .excludePathPatterns("/js/**")
                .excludePathPatterns("/live2d/**")
                .excludePathPatterns("/user/reg")
                .excludePathPatterns("/user/login")
                .excludePathPatterns("/user/islogin")
                .excludePathPatterns("/user/code")
                .excludePathPatterns("/user/addcomment")
                .excludePathPatterns("/user/sendcode")
                .excludePathPatterns("/user/showcomment")
                .excludePathPatterns("/user/upload/**")
                .excludePathPatterns("/user/getuserpicture")
                .excludePathPatterns("/user/getcode")
                .excludePathPatterns("/user/updatepassword")
                .excludePathPatterns("/user/getpassword")
                .excludePathPatterns("/art/getdetail")
                .excludePathPatterns("/art/addrcount")
                .excludePathPatterns("/art/getlike")
                .excludePathPatterns("/art/getlistbyPage")
                .excludePathPatterns("/art/getusercount")
                .excludePathPatterns("/art/fuzzysearchblog")
                .excludePathPatterns("/art/collect")
                .excludePathPatterns("/art/toggle")
                .excludePathPatterns("/art/getcoolect")
                .excludePathPatterns("/art/getcount");
    }

    /**
     * 这个就是springboot中springMVC让程序开发者去配置文件上传的额外静态资源服务的配置
     * 这段代码的作用是将存储在指定文件路径（`imgPath`）下的静态资源映射为以`routePath`开头的URL路径。
     * 当用户通过该URL访问时，Spring MVC会自动查找并返回对应的静态资源文件。
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(routePath+"**").addResourceLocations("file:" + imgPath);

    }
}
