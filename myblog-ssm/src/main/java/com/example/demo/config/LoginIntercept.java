package com.example.demo.config;

import com.example.demo.common.ApplicationVariable;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerAdapter;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * Description:  自定义一个拦截器,如果登录了就不拦截发送的请求,未登录就拦截发送的请求
 * User: WuYimin
 * Date: 2023-06-21
 * Time: 15:34
 */
//@Component 注解表示该类会被 Spring 容器自动识别并实例化为一个 bean 对象
@Component
public class LoginIntercept implements HandlerInterceptor {

    // preHandle 方法在请求被处理之前被调用
    // 如果这个方法返回 true，那么请求就继续处理
    // 如果这个方法返回 false，那么请求就不再向下处理
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //判断用户登录
        HttpSession session = request.getSession(false);
        if(session != null && session.getAttribute(ApplicationVariable.SESSION_KEY_USERINFO) != null) {
            //用户已经登录
            return  true;
        }
        //当代码执行到此处说明用户未登录
        response.sendRedirect("/login.html");
        return false;
    }
}
