package com.example.demo.service;

import com.example.demo.entity.ArticleInfo;
import com.example.demo.entity.vo.ArticleInfoVo;
import com.example.demo.mapper.ArticleMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-20
 * Time: 14:25
 */
@Service
public class ArticleService  {

    @Autowired
    private ArticleMapper articleMapper;

    // 添加一篇新文章到数据库
    public int add(ArticleInfo articleInfo) {
        return articleMapper.add(articleInfo);
    }

    // 根据文章ID和用户ID从数据库获取一篇文章的详情
    public ArticleInfo getDetailByIdAndUid(Integer id, Integer uid) {
        return articleMapper.getDetailByIdAndUid(id,uid);
    }

    // 更新数据库中一篇文章的信息
    public int update(ArticleInfo articleInfo) {
        return articleMapper.update(articleInfo);
    }

    // 根据文章ID从数据库获取一篇文章的详情
    public ArticleInfoVo getDetail(Integer id) {
        return articleMapper.getDetail(id);
    }

    // 增加一篇文章的阅读数量
    public int addRCount(Integer id) {
        return articleMapper.addRCount(id);
    }

    // 根据用户ID从数据库获取该用户的所有文章
    public List<ArticleInfo> getListByUid(Integer uid) {
        return articleMapper.getListByUid(uid);
    }

    // 根据文章ID和用户ID删除一篇文章
    public int del(Integer id, Integer uid) {
        return articleMapper.del(id, uid);
    }

    // 分页从数据库获取文章列表
    public List<ArticleInfo> getListByPage(Integer pageSize, Integer offset) {
        return articleMapper.getListByPage(pageSize,offset);
    }

    // 从数据库获取文章总数
    public Integer getCount() {
        return articleMapper.getCount();
    }

    //根据用户名查询该用户的文章数量
    public int getArticleCountByUsername(String username) {
        return articleMapper.getArticleCountByUsername(username);
    }

    //根据关键字进行模糊匹配搜索文章,分页获取文章列表，传入的参数是一页显示的数量和偏移量，返回一个 ArticleInfo 列表
    public List <ArticleInfo> fuzzySearchBlog(String keyword){
        return articleMapper.fuzzySearchBlog(keyword);
    }

    //根据姓名和关键字进行模糊匹配搜索个人文章
    public List <ArticleInfo> fuzzySearchMyBlog(String keyword,Integer id) {
        return articleMapper.fuzzySearchMyBlog(keyword,id);
    }
}
