package com.example.demo.service;

import com.example.demo.mapper.ArticleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

/**
 * Redis使用键值对来存储数据，这里的键是用户点赞集合的键名，格式为like:userId，值是一个无序集合(Set)，存储用户点赞的文章Id。
 *
 * toggleLike方法中使用了Redis的Set数据结构。通过redisTemplate.opsForSet().isMember(likeKey, articleId)方法来判断文章是否已经被点赞，
 * 如果是，则移除文章Id；如果否，则添加文章Id。Set数据结构可以快速判断元素是否存在，并且不会出现重复元素，非常适合用来存储用户点赞的文章Id。
 *
 * hasLiked方法同样使用了Redis的Set数据结构。通过redisTemplate.opsForSet().isMember(likeKey, articleId)方法
 * 来判断文章Id是否在用户点赞集合中，从而确定用户是否已点赞该文章。
 */
@Service
public class LikeService {
//    private final RedisTemplate<String, Object> redisTemplate;
//    //构造方法注入
//    @Autowired
//    public LikeService(RedisTemplate<String, Object> redisTemplate) {
//        this.redisTemplate = redisTemplate;
//    }

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    // 点赞文章
    public int toggleLike(int userId, int articleId) {

        // 创建用于标识特定用户和文章组合的键
        String likeKey = "like:" + userId + ":" + articleId;
        // 创建用于存储特定文章的点赞总数的键
        String articleLikeCountKey = "articleLikeCount:" + articleId;

        // 检查特定用户是否已经点赞特定文章
        if (redisTemplate.opsForSet().isMember(likeKey, articleId)) {
            // 如果已经点赞，则从集合中移除文章ID
            redisTemplate.opsForSet().remove(likeKey, articleId);
            // 减少文章的点赞总数
            redisTemplate.opsForValue().decrement(articleLikeCountKey);

            // 返回当前文章的点赞次数
            return (int) redisTemplate.opsForValue().get(articleLikeCountKey);
        } else {
            // 如果尚未点赞，则将文章ID添加到集合中
            redisTemplate.opsForSet().add(likeKey, articleId);
            // 增加文章的点赞总数
            redisTemplate.opsForValue().increment(articleLikeCountKey);

            // 返回当前文章的点赞次数
            return (int) redisTemplate.opsForValue().get(articleLikeCountKey);
        }
    }

    // 判断用户是否已点赞某篇文章
    public boolean hasLiked(int userId, int articleId) {
        // 创建用于标识特定用户和文章组合的键
        String likeKey = "like:" + userId + ":" + articleId;
        // 检查特定用户是否已经点赞特定文章
        return redisTemplate.opsForSet().isMember(likeKey, articleId);
    }

    // 获取文章的点赞次数
    public int getLikeCount(int articleId) {
        // 创建用于存储特定文章的点赞总数的键
        String articleLikeCountKey = "articleLikeCount:" + articleId;
        // 获取点赞总数，如果键不存在，则返回0
        return (int) (redisTemplate.opsForValue().get(articleLikeCountKey) == null ? 0 : redisTemplate.opsForValue().get(articleLikeCountKey));
    }
}
