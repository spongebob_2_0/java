package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class CollectService {
//    private final RedisTemplate<String, Object> redisTemplate;
//
//    // 构造方法注入，通过@Autowired注解将RedisTemplate注入到CollectService中
//    @Autowired
//    public CollectService(RedisTemplate<String, Object> redisTemplate) {
//        this.redisTemplate = redisTemplate;
//    }
    @Autowired
    private  RedisTemplate<String,Object> redisTemplate;

    // 收藏或取消收藏文章
    public int toggleCollect(int userId, int articleId) {
        // 拼接用于标识特定用户和文章组合的键名
        String collectKey = "collect:" + userId + ":" + articleId;
        // 创建用于存储特定文章的收藏总数的键名
        String articleCollectCountKey = "articleCollectCount:" + articleId;

        // 判断特定用户是否已经收藏特定文章
        if (redisTemplate.opsForSet().isMember(collectKey, articleId)) {
            // 如果已经收藏，则从集合中移除文章ID
            redisTemplate.opsForSet().remove(collectKey, articleId);
            // 减少文章的收藏总数
            redisTemplate.opsForValue().decrement(articleCollectCountKey);

            // 返回当前文章的收藏次数
            return (int) redisTemplate.opsForValue().get(articleCollectCountKey);
        } else {
            // 如果尚未收藏，则将文章ID添加到集合中
            redisTemplate.opsForSet().add(collectKey, articleId);
            // 增加文章的收藏总数
            redisTemplate.opsForValue().increment(articleCollectCountKey);

            // 返回当前文章的收藏次数
            return (int) redisTemplate.opsForValue().get(articleCollectCountKey);
        }
    }

    // 判断用户是否已收藏某篇文章
    public boolean hasCollected(int userId, int articleId) {
        // 创建用于标识特定用户和文章组合的键名
        String collectKey = "collect:" + userId + ":" + articleId;
        // 判断特定用户是否已经收藏特定文章
        return redisTemplate.opsForSet().isMember(collectKey, articleId);
    }

    // 获取文章的收藏次数
    public int getCollectCount(int articleId) {
        // 创建用于存储特定文章的收藏总数的键名
        String articleCollectCountKey = "articleCollectCount:" + articleId;
        // 获取收藏总数，如果键不存在，则返回0
        return (int) (redisTemplate.opsForValue().get(articleCollectCountKey) == null ? 0 : redisTemplate.opsForValue().get(articleCollectCountKey));
    }
}
