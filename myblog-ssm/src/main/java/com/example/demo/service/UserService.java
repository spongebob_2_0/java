package com.example.demo.service;

import com.example.demo.entity.ArticleInfo;
import com.example.demo.entity.CommentTable;
import com.example.demo.entity.Userinfo;
import com.example.demo.entity.vo.UserinfoVo;
import com.example.demo.mapper.UserMapper;
import com.sun.corba.se.spi.orb.ParserImplBase;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-20
 * Time: 14:24
 */
@Service
public class UserService {

    @Autowired
    JavaMailSender mailSender; //发送邮件
    @Autowired
    private UserMapper userMapper;

    // 在数据库中注册新的用户
    public int reg(UserinfoVo userinfo) {
        return userMapper.reg(userinfo);
    }

    // 根据用户名从数据库中获取用户信息
    public UserinfoVo login(String username) {
        return userMapper.login(username);
    }

    // 发送邮件，通常用于发送验证码或者其他通知信息
    public boolean sendMsmByEmail(String code, String email) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setSubject("【LKX】"); //邮件标题
        mailMessage.setText("验证码"+code); //邮件内容
        mailMessage.setFrom("55343818@qq.com"); //发件人
        mailMessage.setTo(email); //收件人
        try {
            mailSender.send(mailMessage);
            return true;
        }catch (MailException e){
            e.printStackTrace();
            return false;
        }
    }

    // 将上传的图片的路径保存到对应用户的表中
    public int updateUserImgPath(String username,String imgPath) {
        return userMapper.updateUserImgPath(username, imgPath);
    }

    //根据文章id查询该用户的头像路径信息
    public String getUserPicture(Integer id) {
        return userMapper.getUserPicture(id);
    }

    //根据用户id获取照片的路径
    public Userinfo getUserLogoPicture(@Param("id") Integer id) {
        return userMapper.getUserLogoPicture(id);
    }

    //修改用户密码
    public int updateUserPassword(UserinfoVo userinfo) {
        return userMapper.updateUserPassword(userinfo);
    }

    //通过邮箱去查询用户是否存在
    public UserinfoVo getUserByEmail(@Param("email") String email) {
        return userMapper.getUserByEmail(email);
    }

    //通过邮箱修改密码
    public int updateUserPasswordByEmail(UserinfoVo userinfo) {
        return userMapper.updateUserPasswordByEmail(userinfo);
    }

    //根据文章id和用户id添加评论
    public int addComment(CommentTable commentTable){
        return userMapper.addComment(commentTable);
    }

    //根据文章id去查找所有评论
    public List<CommentTable> showComment(String article_id) {
        return userMapper.showComment(article_id);
    }

    //根据用户id去获取该用户的所有文章
    public List<ArticleInfo> getArticleInfo(@Param("id") Integer id) {
        return userMapper.getArticleInfo(id);
    }
}

