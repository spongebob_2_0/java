package com.example.demo.mapper;

import com.example.demo.entity.ArticleInfo;
import com.example.demo.entity.CommentTable;
import com.example.demo.entity.Userinfo;
import com.example.demo.entity.vo.UserinfoVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-20
 * Time: 14:21
 */
@Mapper
public interface UserMapper {

    // 注册一个新用户，传入的参数是一个 Userinfo 对象，返回插入的行数
    int reg(UserinfoVo userinfo);

    // 根据用户名查询用户信息进行登录，传入的参数是用户名，返回对应的 UserinfoVo 对象
    UserinfoVo login(@Param("username") String username);

    // 将上传的图片的路径保存到对应用户的表中
    int updateUserImgPath(@Param("username") String username, @Param("imgPath") String imgPath);

    //根据文章id获取照片的路径
    String getUserPicture(@Param("id") Integer id);

    //根据用户id获取自己照片的路径
    Userinfo getUserLogoPicture(@Param("id") Integer id);

    //根据用户id去获取该用户的所有文章
    List<ArticleInfo> getArticleInfo(@Param("id") Integer id);

    //修改用户密码
    int updateUserPassword(UserinfoVo userinfo);

    //通过邮箱去查询用户
    UserinfoVo getUserByEmail(@Param("email") String email);

    //通过邮箱修改密码
    int updateUserPasswordByEmail(UserinfoVo userinfo);

    //根据文章id和用户id添加评论
    int addComment(CommentTable commentTable);

    //根据文章id去查找所有评论
    List<CommentTable> showComment(String article_id);

    

}
