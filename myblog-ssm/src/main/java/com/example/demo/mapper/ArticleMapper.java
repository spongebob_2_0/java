package com.example.demo.mapper;

import com.example.demo.entity.ArticleInfo;
import com.example.demo.entity.vo.ArticleInfoVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-20
 * Time: 14:22
 */
@Mapper
public interface ArticleMapper {

    // 添加一篇文章，传入的参数是一个 ArticleInfo 对象，返回插入的行数
    int add(ArticleInfo articleInfo);

    // 根据文章 id 和用户 id 获取文章详情，返回 ArticleInfo 对象
    ArticleInfo getDetailByIdAndUid(@Param("id") Integer id,@Param("uid") Integer uid);

    // 更新一篇文章的信息，传入的参数是一个 ArticleInfo 对象，返回更新的行数
    int update(ArticleInfo articleInfo);

    // 根据文章 id 获取文章详情，返回 ArticleInfoVo 对象
    ArticleInfoVo getDetail(@Param("id") Integer id);

    // 根据文章 id 增加文章的阅读数量，返回更新的行数
    int addRCount(@Param("id") Integer id);

    // 根据用户 id 获取该用户的所有文章列表，返回一个 ArticleInfo 列表
    List<ArticleInfo> getListByUid(@Param("uid") Integer uid);

    // 根据文章 id 和用户 id 删除一篇文章，返回删除的行数
    int del(@Param("id") Integer id, @Param("uid") Integer uid);

    // 分页获取文章列表，传入的参数是一页显示的数量和偏移量，返回一个 ArticleInfo 列表
    List<ArticleInfo> getListByPage(@Param("pageSize") Integer pageSize, @Param("offset") Integer offset);

    // 获取文章总数量，返回文章数量
    Integer getCount();

    //根据用户名查询该用户所写文章数量
    int getArticleCountByUsername(@Param("username") String username);

    //根据关键字进行模糊匹配搜索文章,分页获取文章列表，传入的参数是一页显示的数量和偏移量，返回一个 ArticleInfo 列表
    List <ArticleInfo> fuzzySearchBlog(String keyword);

    //根据姓名和关键字进行模糊匹配搜索个人文章
    List <ArticleInfo> fuzzySearchMyBlog(String keyword,Integer id);




}

