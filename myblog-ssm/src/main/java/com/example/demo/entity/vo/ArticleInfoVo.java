package com.example.demo.entity.vo;

import com.example.demo.entity.ArticleInfo;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-20
 * Time: 14:23
 */
@Data
public class ArticleInfoVo extends ArticleInfo {
    private String username;
}
