package com.example.demo.entity;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-08-01
 * Time: 10:39
 */
@Data
public class CommentTable {
    private int comment_id;
    private int user_id;
    private String username;
    private int article_id;
    private String content;
    private LocalDateTime create_time;
}
