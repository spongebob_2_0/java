package com.example.demo.entity.vo;

import com.example.demo.entity.Userinfo;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-31
 * Time: 20:14
 */
@Data
public class UserinfoVo extends Userinfo {
    private String newPassword;
    private String email;
}
