package com.example.demo.entity;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created with IntelliJ IDEA.
 * Description: 当将Java对象存储在Redis中时，需要将对象序列化为字节流或字符串，以便将其存储在Redis的键值对中。
 * 列化后的对象可以使用Redis客户端库将其作为值设置到Redis中，并在需要时从Redis中获取并反序列化为原始对象。
 * User: WuYimin
 * Date: 2023-06-20
 * Time: 14:13
 */
@Data
public class Userinfo implements Serializable {
    private int id;
    private String username;
    private String password;
    private String photo;
    private LocalDateTime createtime;
    private LocalDateTime updatetime;
    private int state;
}
