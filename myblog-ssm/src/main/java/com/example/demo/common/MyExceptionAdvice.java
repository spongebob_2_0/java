package com.example.demo.common;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Created with IntelliJ IDEA.
 * Description: 统一异常处理和返回
 * User: WuYimin
 * Date: 2023-06-22
 * Time: 10:25
 */
@RestControllerAdvice //定义一个全局异常处理类,返回类型的格式为json格式
public class MyExceptionAdvice {

    @ExceptionHandler(Exception.class) //定义异常处理方法
    public AjaxResult doException(Exception e) {
        return AjaxResult.fail(-1,e.getMessage());
    }
}
