package com.example.demo.common;

import org.springframework.util.StringUtils;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-22
 * Time: 7:47
 */
// 定义一个 StringTools 类，用于进行字符串相关的操作
public class StringTools {

    // 定义一个静态方法 subLength，用于截取字符串的前 N 个字符
    // 该方法接受两个参数，第一个参数是要被截取的字符串，第二个参数是要截取的最大字符数
    public static String subLength(String val, int maxLength) {

        // 使用 Spring 提供的 StringUtils 类的 hasLength 方法判断传入的字符串是否有长度，
        // 或者 maxLength 是否小于等于0。如果字符串没有长度，或者 maxLength 小于等于0，
        // 那么直接返回原字符串，不进行截取
        if(!StringUtils.hasLength(val) || maxLength <= 0) {
            return val;
        }

        // 判断传入的字符串的长度是否小于等于 maxLength，
        // 如果是，那么直接返回原字符串，不需要进行截取
        if(val.length() <= maxLength) {
            return val;
        }

        // 如果字符串的长度大于 maxLength，那么使用 String 类的 substring 方法
        // 截取字符串的前 maxLength 个字符并返回
        return val.substring(0,maxLength);
    }
}
