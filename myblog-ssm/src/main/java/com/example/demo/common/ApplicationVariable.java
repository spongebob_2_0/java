package com.example.demo.common;

/**
 * Created with IntelliJ IDEA.
 * Description: 全局变量类
 * User: WuYimin
 * Date: 2023-06-21
 * Time: 15:28
 */
public class ApplicationVariable {
    /**
     * 存放当前登录用户的session key
     */
    public static final String SESSION_KEY_USERINFO = "SESSION_KEY_USERINFO";

}
