package com.example.demo.common;

public class MarkdownUtils {

    // 使用静态方法，可以直接通过类名调用，无需创建对象
    public static String removeMarkdown(String markdownText) {
        String regex = "(\\*\\*|\\*|\\#|\\>|\\[\\]\\(\\)|\\`\\`\\`|\\`|\\-\\-\\-|\\=\\=\\=|\\||\\[\\])";
        String plainText = markdownText.replaceAll(regex, "");
        return plainText;
    }
}
