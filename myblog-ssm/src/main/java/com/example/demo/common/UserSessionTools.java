package com.example.demo.common;

import com.example.demo.entity.Userinfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-21
 * Time: 19:27
 */
// UserSessionTools 是一个工具类，用于操作和管理用户的 session
public class UserSessionTools {

    /**
     * 该方法用于获取当前登录的用户
     * @param request HttpServletRequest 对象，用于获取用户的 session
     * @return 如果用户已经登录，返回对应的 Userinfo 对象，否则返回 null
     */
    public static Userinfo getLoginUser(HttpServletRequest request) {

        // 从请求对象中获取 HttpSession，如果当前请求没有关联的 session 则返回 null，
        // 这是通过传递一个 false 参数给 getSession 方法实现的
        HttpSession session = request.getSession(false);

        // 检查 session 是否存在，并且 session 中是否有一个名为
        // ApplicationVariable.SESSION_KEY_USERINFO 的属性，该属性用于存储用户信息
        if(session != null && session.getAttribute(ApplicationVariable.SESSION_KEY_USERINFO) != null) {

            // 如果上述两个条件都满足，那么就从 session 中获取名为
            // ApplicationVariable.SESSION_KEY_USERINFO 的属性，强制转换为 Userinfo 对象，
            // 然后返回这个对象，这个对象就代表了当前登录的用户
            return (Userinfo) session.getAttribute(ApplicationVariable.SESSION_KEY_USERINFO);
        }

        // 如果 session 不存在，或者 session 中不存在名为
        // ApplicationVariable.SESSION_KEY_USERINFO 的属性，那么返回 null，
        // 这意味着当前没有用户登录
        return null;
    }
}

