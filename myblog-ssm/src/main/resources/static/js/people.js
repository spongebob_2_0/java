var talkContent = document.getElementById('talk-content');
var talks = ['Hello!', 'How are you?', 'Nice to meet you!'];  // 你可以替换成你需要的文字
var i = 0;

setInterval(function() {
  if (i >= talks.length) {
    i = 0;
  }
  talkContent.innerText = talks[i];
  i++;
}, 2000);  // 这里的 2000 是两秒，表示每两秒更换一次小人的话
