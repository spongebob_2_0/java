//这段代码实现了用户注销操作，包括发送注销请求、处理注销结果以及根据注销结果重定向用户或者显示警告信息。
//当调用这个 logout 函数时，首先会弹出一个确认对话框询问用户是否确认退出。这是通过 confirm 方法实现的
function logout() {
    if(confirm("是否确认退出? ")) {
        jQuery.ajax({
            url: "/user/logout",
            type: "post",
            data: {},
            success: function(res) {
                if(res.code == 200 && res.data ==1) {
                    //注销成功
                    location.href = "blog_list.html";
                }else{
                    alert("抱歉: 操作失败! " + res.msg);
                }
            }
        });
    }
}