// 这个JavaScript函数用于从URL中获取特定查询参数的值。
//这个函数接收一个字符串key作为参数，然后检查当前URL（通过 location.search 得到）的查询字符串部分是否包含这个key。
//如果找到了这个key，就返回对应的value。如果没有找到这个key，则返回null。
function getParamByKey(key) {
    var params = location.search;
    params = params.substring(1);
    var paramsArr = params.split('&');
    if(paramsArr != null && paramsArr.length > 0) {
        for(var i = 0; i < paramsArr.length; i++) {
            var item =paramsArr[i];  //ex:'id=1'
            var itemArr = item.split('=');
             //key == 目标key
            if(itemArr.length == 2 && itemArr[0] == key) {
                return itemArr[1];   // 返回value
            }
        }
    }
    return null;
}
