import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-14
 * Time: 10:40
 */
public class Main {
    public static void main(String[] args) {
        BinarySearchTree binarySearchTree =new BinarySearchTree();
        int[] array = {5,3,4,1,7,8,2,6,0,9};
        for(int i = 0; i < array.length; i++) {
            binarySearchTree.insert(array[i]);
        }
//        binarySearchTree.remove(3);
//        binarySearchTree.remove(9);
//        binarySearchTree.remove(5);
//        binarySearchTree.remove(0);
        binarySearchTree.inorder(binarySearchTree.root);
    }

    public static void main2(String[] args) {
        Map<String,Integer> map = new HashMap<>();
        map.put("小诗诗",18);
        map.put("小甜甜",19);
        map.put("小红红",20);

        //使用entrySet方法和增强for循环遍历map集合
        Set<Map.Entry<String, Integer>> entrySet = map.entrySet();
        for (Map.Entry<String, Integer> entry: entrySet) {
            System.out.println("key = " + entry.getKey()+" value = " + entry.getValue());
        }

        //使用keySet方法和增强for循环遍历map集合
        for (String key : map.keySet()) {
            Integer value = map.get(key);
            System.out.println("key = " +key+" value = "+ value);
        }

        //使用 values() 来遍历所有的值（但这种方法无法获取对应的键
        System.out.println("==========");
        for(Integer value : map.values()) {
            System.out.print(value+" ");
        }
    }

    public static void main3(String[] args) {
        Map<String,Integer> map = new HashMap<>();
        map.put("小诗诗",19);
        map.put("小甜甜",20);
        map.put("小花花",18);
        Set<Map.Entry<String, Integer>> entries = map.entrySet();
        for(Map.Entry<String, Integer> entry: entries) {
            System.out.println("key = " + entry.getKey() + " value = " + entry.getValue());
        }
        System.out.println("============");
        for(String key: map.keySet()) {
            Integer value = map.get(key);
            System.out.println("key = " + key + " value = " + value);
        }
    }
}
