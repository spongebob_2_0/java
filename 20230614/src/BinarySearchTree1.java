///**
// * Created with IntelliJ IDEA.
// * Description:
// * User: WuYimin
// * Date: 2023-06-14
// * Time: 10:19
// */
//
//public class BinarySearchTree1 {
//   TreeNode root = null;
//
//    static class TreeNode {
//        public int val;
//        public TreeNode left;
//        public TreeNode right;
//
//        public TreeNode(int val) {
//            this.val = val;
//        }
//    }
//
//    //查收key
//    public boolean searchKey(int key) {
//        TreeNode cur = root;
//        while (cur != null) {
//            if(cur.val < key) {
//                cur = cur.right;
//            }else if(cur.val > key) {
//                cur = cur.left;
//            }else {
//                return true;
//            }
//        }
//        return  false;
//    }
//
//    //插入key
//    public void insert(int key) {
//        if(root == null) {
//            root = new TreeNode(key);
//            return;
//        }
//        TreeNode cur = root;
//        TreeNode parent = null;
//        while (cur != null) {
//            if(cur.val < key) {
//                parent = cur;
//                cur = cur.right;
//            }else if(cur.val >key) {
//                parent = cur;
//                cur = cur.left;
//            }else {
//                return;
//            }
//        }
//        //到这里说明没有重复的值,可以插入
//        TreeNode node = new TreeNode(key);
//        if(parent.val < key) {
//            parent.right = node;
//        }else {
//            parent.left =node;
//        }
//    }
//
//    //中序遍历这棵树
//    public void inorder(TreeNode root) {
//        if(root == null) {
//            return;
//        }
//        inorder(root.left);
//        System.out.print(root.val+" ");
//        inorder(root.right);
//    }
//
//    //删除
//    public void remove(int key) {
//        TreeNode cur = root;
//        TreeNode parent =null;
//        while (cur != null) {
//            if(cur.val < key) {
//                parent = cur;
//                cur = cur.right;
//            }else if(cur.val > key) {
//                parent = cur;
//                cur = cur.left;
//            }else {
//                removeKey(cur,parent);
//                return;
//            }
//        }
//    }
//
//    private void removeKey(TreeNode cur, TreeNode parent) {
//        if(cur.left == null) {
//            if(cur == root) {
//                root = cur.right;
//            }else if(cur == parent.left) {
//                parent.left = cur.right;
//            }else {
//                parent.right = cur.right;
//            }
//        }else if(cur.right == null) {
//            if(cur == root) {
//                root = cur.left;
//            }else if(cur == parent.left) {
//                parent.left =cur.left;
//            }else {
//                parent.right = cur.left;
//            }
//        }else {
//            //此时cur.left != null && cur.right != null
//            TreeNode target = cur.right;
//            TreeNode targetParent = cur;
//            while (target.left != null) {
//                targetParent = target;
//                target = target.left;
//            }
//            cur.val = target.val;
//            if(target == targetParent.right) {
//                targetParent.right = target.right;
//            }else {
//                targetParent.left = target.right;
//            }
//        }
//    }
//}
