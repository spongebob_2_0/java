/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-14
 * Time: 10:19
 */

public class BinarySearchTree {
    TreeNode root;

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val) {
            this.val = val;
        }
    }

    //查找元素
    public boolean search(int key) {
        TreeNode cur = root;
        while (cur != null) {
            if(cur.val < key) {
                cur = cur.right;
            }else if(cur.val > key) {
                cur = cur.left;
            }else {
                return true;
            }
        }
        return false;
    }

    //插入元素
    public void insert(int key) {
        if(root == null) {
            root = new TreeNode(key);
            return;
        }
        TreeNode cur =root;
        TreeNode parent = null;
        while (cur != null) {
            if(cur.val < key) {
                parent = cur;
                cur = cur.right;
            }else if(cur.val > key) {
                parent = cur;
                cur = cur.left;
            }else {
                //已经有相同的元素了,无法插入重复元素,直接退出
                return;
            }
            //此时可以插入元素了
            TreeNode node = new TreeNode(key);
            if(parent.val < key) {
                parent.right =node;
            }else {
                parent.left = node;
            }
        }
    }

    //中序遍历
    public void inorder(TreeNode root) {
        if(root == null) {
            return;
        }
        inorder(root.left);
        System.out.print(root.val+" ");
        inorder(root.right);
    }
}
