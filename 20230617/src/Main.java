import java.io.Console;
import java.sql.Array;
import java.sql.Connection;
import java.util.*;
import java.util.stream.Stream;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-17
 * Time: 20:17
 */
public class Main {
    public static void main(String[] args) {
        List<Person> list = new ArrayList<>();
        list.add(new Person("小诗诗",20,175));
        list.add(new Person("小红红",18,175));
        list.add(new Person("小飞飞",32,175));
        list.add(new Person("小花花",19,175));

//        Collections.sort(list, new Comparator<Person>() {
//            @Override
//            public int compare(Person o1, Person o2) {
//                return o1.getAge()- o2.getAge();
//            }
//        });
//        for(Person person: list) {
//            System.out.println(person);
//        }

//        Collections.sort(list,(Person o1,Person o2) ->{
//            return o1.getAge() - o2.getAge();
//        });
//        for(Person person: list) {
//            System.out.println(person);
//        }
        List<String> list1 = Arrays.asList("小诗诗","小花花","小红红");
//        for(String str: list1) {
//            System.out.print(str+" ");
//        }
        list1.stream().forEach(System.out::println);

        Stream<String> s1 = Stream.of("abc");
        Stream<String> s2 = Stream.of("def");
        Stream.concat(s1,s2).forEach(System.out::println);
    }
}
