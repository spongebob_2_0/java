/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-18
 * Time: 7:18
 */
public class Main2 {
    public static void main(String[] args) {
        long start1 = System.currentTimeMillis();
        for(int i = 0; i < 10000; i++) {
            String s = "abc" + "dek";
        }
        long end1 = System.currentTimeMillis();
        long start2 = System.currentTimeMillis();
        for(int i = 0; i < 10000; i++) {
            StringBuilder sb = new StringBuilder();
            sb.append("abc");
            sb.append("def");
        }
        long end2 = System.currentTimeMillis();
        System.out.println(end1-start1);
        System.out.println(end2-start2);
    }

    private static String removeCharAt(String s, int i) {
        return s.substring(0,i) + s.substring(i+1, s.length()-1);
    }
}
