package com.example.demo.empty.query;

import com.example.demo.empty.User;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-08-22
 * Time: 0:59
 */
@Data
public class UserQuery extends User {
    private Integer age2;
}
