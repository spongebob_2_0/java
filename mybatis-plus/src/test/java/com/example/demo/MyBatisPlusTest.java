package com.example.demo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.empty.User;
import com.example.demo.empty.query.UserQuery;
import com.example.demo.mapper.UserMapper;
import net.bytebuddy.pool.TypePool;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class MyBatisPlusTest {
    @Resource
    private UserMapper userMapper;

    /**
     * 测试查询所有数据
     */
    @Test
    void testSelectList(){
//        //方式1  lt代表小于,gt代码大于
//        QueryWrapper wrapper = new QueryWrapper();
//        wrapper.lt("age",21);
//        //通过条件构造器查询一个list集合，若没有查询条件，则可以设置null为参数
//        List<User> users = userMapper.selectList(wrapper);
//        users.forEach(System.out::println);

//        //方式2 Lambda格式按条件查询
//        QueryWrapper<User> wrapper = new QueryWrapper<>();
//        wrapper.lambda().lt(User::getAge,21);
//        //通过条件构造器查询一个list集合，若没有查询条件，则可以设置null为参数
//        List<User> users = userMapper.selectList(wrapper);
//        users.forEach(System.out::println);
//

        //模拟页面传递过来的查询数据
        UserQuery uq = new UserQuery();
//        uq.setAge(18);
        uq.setAge2(21);


        //null判定
        //方式3  Lambda格式按条件查询
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        //查询的条件在小于21岁大于18岁
        wrapper.lt(null != uq.getAge2(),User::getAge,uq.getAge2());
        wrapper.ge(null != uq.getAge(),User::getAge,uq.getAge());
        //查询的条件为小于21岁或者大于22岁
//        wrapper.lt(User::getAge,uq.getAge2()).or().gt(User::getAge,uq.getAge());
        //通过条件构造器查询一个list集合，若没有查询条件，则可以设置null为参数
        List<User> users = userMapper.selectList(wrapper);
        users.forEach(System.out::println);
    }


    /**
     * 添加操作
     */
    @Test
    void testSave() {
        User user = new User();
        user.setAge(18);
        user.setEmail("1234@qq.com");
        user.setName("小诗诗");
        userMapper.insert(user);
    }

    /**
     * 删除操作
     */
    @Test
    void testDelete() {
        userMapper.deleteById(1693648905554350082L);
    }

    /**
     * 修改操作
     */
    @Test
    void testUpdate() {
        User user = new User();
        user.setId(1L);
        user.setName("小甜甜");
        user.setAge(19);
        userMapper.updateById(user);
    }

//    @Test
//    void testSelect() {
//        QueryWrapper<User> wrapper = new QueryWrapper<>();
//        wrapper.select("count(*) as count,email");
//        wrapper.groupBy("email");
////        wrapper.select("id","name","age");
////        wrapper.select(User::getAge,User::getId, User::getName);
////        List<User> userList = userMapper.selectList(wrapper);
//        List<Map<String, Object>> userList = userMapper.selectMaps(wrapper);
//        userList.forEach(System.out::println);
//    }

    @Test
    void testSelect() {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getName,"小诗诗").eq(User::getName,"小诗诗");
        User user = userMapper.selectOne(wrapper);
        System.out.println(user);
    }



    /**
     * 分页操作
     */
    @Test
    void testGetPage() {
        //获取分页对象
        IPage page = new Page<>(1,5);
        userMapper.selectPage(page, null);
        System.out.println("当前页码值: "+page.getCurrent());;
        System.out.println("每页多少数据: "+page.getSize());
        System.out.println("一共多少页: "+page.getPages());
        System.out.println("一共多少条数据: "+page.getTotal());
        System.out.println("每页数据展示: " + page.getRecords());
    }
}
