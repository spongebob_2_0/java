package demo5;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
public class ConditionTest {
    public static void main(String[] args) {
        ConditionTest01 conditionTest01 = new ConditionTest01();
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                conditionTest01.method01();
            }
        },"AAAA").start();
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                conditionTest01.method02();
            }
        },"BBBB").start();
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                conditionTest01.method03();
            }
        },"CCCC").start();
    }
}
class ConditionTest01  {
    Lock lock = new ReentrantLock();
    Condition condition1 = lock.newCondition();
    Condition condition2 = lock.newCondition();
    Condition condition3 = lock.newCondition();

    private int number = 2;
    public void method01() {
        lock.lock();
        try {
// 这里为了测试是否真的精准唤醒不会虚假唤醒而使用if  实际使用上就应该按上文说的用while
            if (number != 1){
                try {
                    condition1.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            number = 2;
            System.out.println(Thread.currentThread().getName()+"唤醒了condition2");
            condition2.signal();
        } finally {
            lock.unlock();
        }
    }
    public void method02() {
        lock.lock();
        try {
            if (number != 2){
                condition2.await();
            }
            number = 3;
            System.out.println(Thread.currentThread().getName()+"唤醒了condition3");
            condition3.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
    public void method03() {
        lock.lock();
        try {
            if (number != 3){
                condition3.await();
            }
            number = 1;
            System.out.println(Thread.currentThread().getName()+"唤醒了condition1");
            condition1.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}