package demo6;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
public class ListTest {
//    java.util.ConcurrentModificationException 并发修改异常
    public static void main(String[] args) {
        /*
        * 解决方案
        * 1.List<String> list = new Vector<>();
        * 2.List<String> list = Collections.synchronizedList(new ArrayList<>());
        * 3.List<String> list = new CopyOnWriteArrayList<>();
        * COW 读写分离 在写入的时候复制一份再编辑 再塞回去
        * */
        List<String> list = new CopyOnWriteArrayList<>();
//        List<String> list = new ArrayList<>();
        for (int i = 1; i <= 20; i++) {
            new Thread(() -> {
                list.add(UUID.randomUUID().toString().substring(0, 5));
                System.out.println(list);
            }, String.valueOf(i)).start();
        }
    }
}