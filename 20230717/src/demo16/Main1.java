package demo16;

import javax.sound.sampled.FloatControl;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-23
 * Time: 10:08
 */
class Ticket {
    private static int ticket = 50;
    ReentrantLock lock = new ReentrantLock(true);
    public void sale () {
        lock.lock();
        if(ticket > 0) {
            System.out.println(Thread.currentThread().getName() + "卖出了第" + (ticket--) + "票, 还剩" + ticket + "票");
        }
        lock.unlock();
    }
}
public class Main1 {
    public static void main(String[] args) {
        Ticket ticket = new Ticket();
            new Thread(()->{
                for (int i = 0; i < 55; i++) {
                    ticket.sale();

                }
            },"窗口1").start();
            new Thread(()->{
                for (int i = 0; i < 55; i++) {
                    ticket.sale();

                }
            },"窗口2").start();
            new Thread(()->{
                for (int i = 0; i < 55; i++) {
                    ticket.sale();

                }
            },"窗口3").start();
    }
}
