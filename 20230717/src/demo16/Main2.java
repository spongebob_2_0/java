package demo16;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-23
 * Time: 9:56
 */
class Ticket2 implements Runnable {
    private  int number = 50;
    @Override
    public  void run() {
                for (int i = 0; i < 100; i++) {
                    if(number > 0) {
                        System.out.println("我在"+Thread.currentThread().getName() + "抢到了第" + (number--) + "票");
                    }
                }
    }
}

public class Main2 {
    public static void main(String[] args) {
        Ticket2 ticket = new Ticket2();
        Thread t1 = new Thread(ticket,"窗口1");
        Thread t2 = new Thread(ticket,"窗口2");
        Thread t3 = new Thread(ticket,"窗口3");
        t1.start();
        t2.start();
        t3.start();

    }
}
