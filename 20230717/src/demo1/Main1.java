package demo1;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-17
 * Time: 18:34
 */
public class Main1 {
    public static void main(String[] args) {
        Person person = new Person();
        try {
            Person person1 = person.getClass().newInstance();
            person1.setAge(18);
            System.out.println(person1.getAge());
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        System.out.println(person.getClass());
        System.out.println(Person.class);
        try {
            System.out.println(Class.forName("demo1.Person"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        int[] nums = {1,2,3};
        for(int x: nums) {
            System.out.print(x+" ");
        }

    }
}
