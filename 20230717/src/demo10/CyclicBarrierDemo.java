package demo10;

import javax.crypto.spec.PSource;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierDemo {
    public static void main(String[] args) {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(7,()->{
            System.out.println("召唤神龙成功!!! ");
        });

        for(int i = 1; i <= 7; i++) {
            final int tmp = i;
            new Thread(()-> {
                System.out.println(Thread.currentThread().getName() + "收集" + tmp + "个龙珠!");
                try {
                    cyclicBarrier.await();  //等待
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }
}