package demo10;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierExample {
    private static final int THREAD_COUNT = 5;
    private static CyclicBarrier cyclicBarrier = new CyclicBarrier(THREAD_COUNT, new Runnable() {
        @Override
        public void run() {
            System.out.println("所有线程已经到达barrier，开始执行任务...");
        }
    });

    public static void main(String[] args) {
        func();
    }

    private static void func() {
        for (int i = 0; i < THREAD_COUNT; i++) {
            new Thread(new Task()).start();
        }
    }

    static class Task implements Runnable {
        @Override
        public void run() {
            try {
                System.out.println(Thread.currentThread().getName() + " 到达 barrier.");
                cyclicBarrier.await();
                System.out.println(Thread.currentThread().getName() + " 执行任务完成.");
                // 重置 CyclicBarrier 的计数器
                cyclicBarrier.reset();
                func();
            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }
        }
    }
}