package demo17;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-23
 * Time: 6:14
 */
class Person {
    public static synchronized void run () {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("我在跑步");
    }
    public  synchronized void swim() {
        System.out.println("我在游泳");
    }
    public void hell0() {
        System.out.println("你好小诗诗");
    }

}
public class Main4 {
    public static void main(String[] args) {

        Person person = new Person();
        Person person2 = new Person();

        new Thread(()->{
            person.run();
        },"线程A").start();

//        try {
//            Thread.sleep(2000)
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        new Thread(()->{
            person2.swim();
        },"线程B").start();

    }
}
