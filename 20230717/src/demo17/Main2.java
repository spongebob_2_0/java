package demo17;

import javax.swing.text.SimpleAttributeSet;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-23
 * Time: 2:42
 */
public class Main2 {
    private static String str1;
    private static ThreadLocal<String> str2 = new ThreadLocal<>();
    public static void main(String[] args) {

        str1 = "main";
        str2.set("main");

        Thread thread = new Thread(()->{
            str1 = "child";
            str2.set("child");
        });
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("str1= " + str1);
        System.out.println("str2= " + str2.get());
    }
}
