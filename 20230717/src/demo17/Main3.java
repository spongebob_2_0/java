package demo17;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-23
 * Time: 5:48
 */

interface Demo {
    int age = 0;
    public abstract void fun();
}
public class Main3 {
    public static void main(String[] args) {
        final int a = 10;
        //在匿名内部类中，变量捕获（外部变量的捕获），看使用的外部变量一定是未被修改过的
        new Demo() {
            @Override
            public void fun() {
                int c = 99;
                c = 100;
                System.out.println(c+"重写一下" + a);
            }
        }.fun();
    }
}
