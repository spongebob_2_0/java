package demo8;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;
public class SetTest {
    public static void main(String[] args) {
//        Set set = new HashSet();
        Set set = Collections.synchronizedSet(new HashSet());
//        Set set = new CopyOnWriteArraySet();
        for (int i = 1; i <= 30; i++) {
            new Thread(() ->{
                set.add(UUID.randomUUID().toString().substring(0, 5));
                System.out.println(set);
            },String.valueOf(i)).start();
        }
    }
}