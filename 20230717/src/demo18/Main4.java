package demo18;

import javax.swing.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-23
 * Time: 11:51
 */
public class Main4 {
    static volatile boolean flag = false;
    static AtomicBoolean atomicBoolean = new AtomicBoolean(false);

    public static void main(String[] args) {
        new Thread(()->{
            while (true) {
                if(atomicBoolean.get()) {
                    System.out.println(Thread.currentThread().getName() + " 程序被修改为true,立刻停止");
                    break;
                }
            }
        }).start();
        new Thread(()->{
            try {
                Thread.sleep(2000);
//                flag = true;
//                atomicBoolean.set(true);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

    }
}
