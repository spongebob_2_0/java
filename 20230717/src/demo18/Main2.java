package demo18;

import java.util.concurrent.locks.LockSupport;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-23
 * Time: 11:26
 */
public class Main2 {
    public static void main(String[] args) {
        Thread a = new Thread(() -> {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "\t" + "-----come in " + System.currentTimeMillis());
            LockSupport.park();  // 被阻塞....等待通知，他需要通过许可证
            System.out.println(Thread.currentThread().getName() + "\t" + "-----被 唤 醒 " + System.currentTimeMillis());
        }, "a");
        a.start();

        Thread b = new Thread(() -> {
            LockSupport.unpark(a);
            System.out.println(Thread.currentThread().getName() + "\t" + "-----通知了");
        }, "b");
        b.start();
    }

}
