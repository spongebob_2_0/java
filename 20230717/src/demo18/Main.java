package demo18;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-23
 * Time: 11:03
 */
public class Main {
    public static void main(String[] args) {
        Object object1 = new Object();
        Object object2 = new Object();

        new Thread(()->{
            synchronized (object1) {
                System.out.println(Thread.currentThread().getName() + " 自己持有A锁,想获取B锁");
                synchronized (object2) {
                    System.out.println(Thread.currentThread().getName() + " 获得B锁");
                }
            }
        },"A").start();
        new Thread(()->{
            synchronized (object2) {
                System.out.println(Thread.currentThread().getName() + " 自己持有B锁,想获取A锁");
                synchronized (object1) {
                    System.out.println(Thread.currentThread().getName() + " 获得A锁");
                }
            }
        },"B").start();

    }
}
