package demo18;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-23
 * Time: 12:12
 */
class MyCallable implements Callable<Integer> {

    @Override
    public Integer call() {
        while (true) {
//            if(Thread.interrupted()) {
//                System.out.println(Thread.interrupted());
//                break;
            System.out.println(Thread.currentThread().isInterrupted());
            if(Thread.currentThread().isInterrupted()) {
                break;
            }
            try {
                    Thread.sleep(2000);
                    System.out.println(Thread.currentThread().isInterrupted());
                    Thread.currentThread().interrupt();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
//                    break;
                }
        }
        return  1;
    }
}
public class Main5 {
    public static void main(String[] args) {
        MyCallable myCallable   = new MyCallable();
        FutureTask<Integer> futureTask = new FutureTask<>(myCallable);
        Thread t = new Thread(futureTask);
        t.start();
        try {
            Thread.sleep(200);
            t.interrupt();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
