package demo19;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-23
 * Time: 21:14
 */
class User {
    String name;
    int age;

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
public class Main {
    public static void main(String[] args) {
        User user1 = new User("zhangsan",22);
        User user2 = new User("lisi",22);
        AtomicReference<User> atomicReference = new AtomicReference<>();

        atomicReference.set(user1);
        System.out.println(atomicReference.compareAndSet(user1, user2) + " " + atomicReference.get().toString());
        System.out.println(atomicReference.compareAndSet(user1, user2) + " " + atomicReference.get().toString());
    }
}
