package demo13;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
/**
 * @program: springboot-08-shiro
 * @description:
 * @author: zhengzx
 * @create: 2022-03-08 17:47
 **/
public class Demo1 {
    public static void main(String[] args) {
//        ExecutorService threadPool = Executors.newSingleThreadExecutor();//单个线程
//        ExecutorService threadPool = Executors.newFixedThreadPool(3);//固定大小的线程池
        ExecutorService threadPool = Executors.newCachedThreadPool();//可伸缩的线程池
        try {
            for (int i = 0; i < 10; i++) {
                threadPool.execute(() -> {
                    System.out.println(Thread.currentThread().getName() + "OK");
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            threadPool.shutdown();
        }
    }
}