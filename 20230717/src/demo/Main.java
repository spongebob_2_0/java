package demo;

import java.util.concurrent.Exchanger;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-23
 * Time: 2:39
 */
public class Main {
    public static void main(String[] args) {
        Exchanger<String> exchanger = new Exchanger<String>();

// ThreadA
        new Thread(()->{
            String strA = null;
            try {
                strA = exchanger.exchange("大佬 A 的钥匙");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("A: 获取" + strA);
        },"A线程").start();

// ThreadB
        new Thread(()->{
            String strB = null;
            try {
                strB = exchanger.exchange("大佬 B 的钥匙");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("B: 获取" + strB);
        },"B线程").start();


    }
}
