package demo9;

import java.util.concurrent.CountDownLatch;

/**
 * Created with IntelliJ IDEA.
 * Description: 计数器
 * User: WuYimin
 * Date: 2023-07-20
 * Time: 3:18
 */
public class CountDownLatchDemo2 {
    public static void main(String[] args) throws InterruptedException {
        //总数6
        CountDownLatch countDownLatch = new CountDownLatch(6);
        for (int i = 1; i <= 6; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName() + "go out");
                countDownLatch.countDown();  //数量-1
            },String.valueOf(i)).start();
        }
        countDownLatch.await(); //等待计数器归零,然后再向下执行
        System.out.println("close door");
    }
}
