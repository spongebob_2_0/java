package thread;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-13
 * Time: 1:34
 */
public class ThreadDemo6 {
    public static void main(String[] args) throws InterruptedException {
        Object object = new Object();
        Thread t1 = new Thread(()-> {
           for(int i = 0; i < 3; i++) {
               System.out.println("我是t1线程");
               try {
                   Thread.sleep(1000);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
           }
           synchronized (object) {
               object.notify();
           }
            System.out.println("t1线程结束");
        });
        Thread t2 = new Thread(()-> {
           synchronized (object) {
               try {
                   object.wait();
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
           }
        });
        System.out.println("线程启动前"+t1.getState());
        t1.start();
        t2.start();
        Thread.sleep(100);
        System.out.println("两个线程同时启动,t2的状态: " + t2.getState());
        System.out.println("线程启动后"+t1.getState());
        t1.join();
        t2.join();
        System.out.println("线程执行完成后"+t1.getState());
    }
}
