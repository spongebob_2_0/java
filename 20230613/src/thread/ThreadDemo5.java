package thread;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-13
 * Time: 0:52
 */
public class ThreadDemo5 {
    private static boolean flag = true;
    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(()-> {
           for(int i = 0; i < 3; i++) {
               System.out.println("我是t线程");
               try {
                   Thread.sleep(1000);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
           }
        },"Thread4");

        t.start();
        Thread.sleep(5000);
        System.out.println("join之前");
        t.join();
        System.out.println("join之后");
//        t.interrupt();
    }
}
