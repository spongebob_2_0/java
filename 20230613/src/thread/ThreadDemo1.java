package thread;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-13
 * Time: 0:34
 */
class MyThread extends Thread{
    @Override
    public void run() {
        while (true) {
            System.out.println("我是t线程");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
public class ThreadDemo1 {
    public static void main(String[] args) throws InterruptedException {
        MyThread t = new MyThread();
        t.start();
        while (true) {
            System.out.println("我是main线程");
            Thread.sleep(1000);
        }
    }
}