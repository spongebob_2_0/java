package thread;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-13
 * Time: 14:48
 */
public class ThreadDemo7 {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(5);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("我来借书了");
                    semaphore.acquire();
                    System.out.println("我拿到算法导论了!");
                    Thread.sleep(500); //借阅时间
                    System.out.println("我还书了");
                    semaphore.release();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        for (int i = 0; i < 10; i++) {
            Thread t = new Thread(runnable);
            t.start();
        }
    }

}
