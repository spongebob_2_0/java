package thread;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
 
/**
 * 使用CountDownLatch.await方法的线程会阻塞，直到所有等待线程全部执行结束为止
 * 大号进阶版本的join方法
 */
public class CountDownLatchTest {
    public static void main(String[] args) throws InterruptedException {
        // 等待线程需要等待的线程数，必须等这10个子线程全部执行完毕再恢复执行
        CountDownLatch latch = new CountDownLatch(10);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(new Random().nextInt(1000));
                    System.out.println(Thread.currentThread().getName() + "到达终点");
                    // 计数器 - 1
                    latch.countDown();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        for (int i = 0; i < 10; i++) {
            Thread t = new Thread(runnable, "运动员" + i);
            t.start();
        }
        // main线程就是裁判线程，需要等待所有运动员到终点再恢复执行
        // 直到所有线程调用countdown方法将计数器减为0继续执行
        latch.await();
        System.out.println("比赛结束");
    }
}