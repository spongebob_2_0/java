package thread;

import javax.swing.tree.TreeNode;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-13
 * Time: 0:43
 */
class MyRunnable implements Runnable {
    @Override
    public void run() {
        while (true) {
            System.out.println("我是t线程");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
public class ThreadDemo2 {
    public static void main(String[] args) throws InterruptedException {
        MyRunnable  runnable = new MyRunnable();
        Thread t = new Thread(runnable);
        t.start();
        while (true) {
            System.out.println("我是main线程");
            Thread.sleep(1000);
        }
    }
}
