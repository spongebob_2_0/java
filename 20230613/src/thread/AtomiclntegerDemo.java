package thread;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomiclntegerDemo {
    private static int number = 0;
    private static AtomicInteger atomicInteger = new AtomicInteger(0);

    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(() -> {
            for (int i = 0; i < 50000; i++) {
                number++;
                atomicInteger.getAndIncrement(); //相当于++操作
            }
        });

        Thread thread2 = new Thread(() -> {
            for (int i = 0; i < 50000; i++) {
                number++;
                atomicInteger.getAndIncrement(); //相当于++操作
            }
        });

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();

        System.out.println("最终结果（number）：" + number);  // 结果可能会小于100000
        System.out.println("最终结果（AtomicInteger）：" + atomicInteger.get());  // 结果始终是100000
    }
}
