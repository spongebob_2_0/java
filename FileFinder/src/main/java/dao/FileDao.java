package dao;

import jdk.nashorn.internal.codegen.DumpBytecode;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 通过这个类来封装针对 file_meta 表的操作.
 */
public class FileDao {

    /**
     * 初始化数据库，执行建表操作。
     */
    public void initDB() {
        // 1) 首先从 db.sql 文件中获取 SQL 语句。
        // 2) 使用 JDBC 执行数据库操作，根据 SQL 语句创建表格。

        // 初始化数据库连接和语句对象
        Connection connection = null;
        Statement statement = null;
        try {
            // 获取数据库连接
            connection = DBUtil.getConnection();

            // 创建 Statement 对象用于执行 SQL 语句
            statement = connection.createStatement();

            // 获取初始化 SQL 语句数组 (调用下面的方法去获取SQL语句数组)
            String[] sqls = getInitSQL();

            // 遍历 SQL 语句数组，逐个执行建表操作
            for (String sql : sqls) {
                System.out.println("[initDB] sql: " + sql);

                // 执行 SQL 语句，创建表格
                statement.executeUpdate(sql);
            }
        } catch (SQLException e) {
            // 捕获并处理 SQL 异常，打印异常信息
            e.printStackTrace();
        } finally {
            // 关闭数据库连接和语句对象
            DBUtil.close(connection, statement, null);
        }
    }


    /**
     * 从名为 db.sql 的文件中读取内容，并将其解析为多个独立的 SQL 语句。
     * @return 包含从文件中解析的多个独立 SQL 语句的字符串数组。
     */
    private String[] getInitSQL() {
        // 创建一个 StringBuilder 以存储最终的结果。
        StringBuilder stringBuilder = new StringBuilder();

        // 使用当前类 FileDao 的类加载器获取名为 db.sql 的资源文件的输入流(字节流对象)。
        try (InputStream inputStream = FileDao.class.getClassLoader().getResourceAsStream("db.sql")) {
            // 使用指定编码 utf8 创建 InputStreamReader 来读取输入流中的内容。(将字节流对象转字符流对象)
            try (InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf8")) {
                // 循环读取输入流中的字符，直到结束标记 -1。
                while (true) {
                    // 注意：inputStreamReader.read() 方法返回一个表示字符的整数值。
                    int ch = inputStreamReader.read();

                    // 检查是否已读取到输入流的末尾。
                    if (ch == -1) {
                        // 如果已到达末尾，则退出循环。
                        break;
                    }

                    // 将读取的字符添加到 StringBuilder 中，以构建文件内容。
                    stringBuilder.append((char) ch);
                }
            }
        } catch (IOException e) {
            // 捕获并处理可能出现的 IO 异常，打印异常信息。
            e.printStackTrace();
        }

        // 将构建的字符串按分号（;）进行分割，得到多个独立的 SQL 语句。
        // 注意：这里假设每个 SQL 语句都以分号作为结束符，且不存在分号本身的情况。
        // 实际应用中可能需要更复杂的分割逻辑。
        return stringBuilder.toString().split(";");
    }


    /**
     * 插入文件/目录数据到数据库中。
     * 该方法提供了批量插入的功能。
     * @param fileMetas 要插入的文件元数据列表
     */
    public void add(List<FileMeta> fileMetas) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            // 获取数据库连接
            connection = DBUtil.getConnection();

            // 关闭连接的自动提交功能，以便在插入完成后进行手动提交
            connection.setAutoCommit(false);

            // SQL 插入语句，使用预编译语句
            String sql = "insert into file_meta values(null, ?, ?, ?, ?, ?, ?, ?)";
            preparedStatement = connection.prepareStatement(sql);

            // 遍历要插入的文件元数据列表
            for (FileMeta fileMeta : fileMetas) {
                // 为预编译语句设置参数
                preparedStatement.setString(1, fileMeta.getName()); //文件名
                //+ File.separator+fileMeta.getName()
                preparedStatement.setString(2, fileMeta.getPath()); //文件路径
                preparedStatement.setBoolean(3, fileMeta.isDirectory()); //文件目录
                preparedStatement.setString(4, fileMeta.getPinyin());  //字符串的拼音表示
                preparedStatement.setString(5, fileMeta.getPinyinFirst());  //字符串的首字母拼音
                preparedStatement.setLong(6, fileMeta.getSize());  //文件大爱小
                preparedStatement.setTimestamp(7, new Timestamp(fileMeta.getLastModified()));//文件最后修改时间
                // 将当前要插入的文件元数据添加到批处理中
                // 使用 addBatch, 把这个构造好的片段, 累计起来
                // addBatch 会把已经构造好的 SQL 保存好, 同时又会允许重新构造一个新的 SQL 出来.
                preparedStatement.addBatch();

                System.out.println("[insert] 插入文件: " + fileMeta.getPath() + File.separator + fileMeta.getName());
            }

            // 执行批处理中的所有 SQL 片段
            preparedStatement.executeBatch();

            // 执行完毕后，通过手动提交告知数据库插入完成
            connection.commit();
        } catch (SQLException e) {
            // 如果插入过程中出现异常，进行回滚操作
            try {
                if (connection != null) {
                    connection.rollback();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            // 关闭数据库连接和预编译语句
            DBUtil.close(connection, preparedStatement, null);
        }
    }


    /**
     * 按照特定关键词进行文件搜索的方法。
     * 注意：查询的关键词 pattern 可能是文件名的一部分、文件名拼音的一部分，或拼音首字母的一部分。
     * @param pattern 查询的关键词
     * @return 包含符合查询条件的文件元数据的列表
     */
    public List<FileMeta> searchByPattern(String pattern) {
        // 用于存储符合查询条件的文件元数据列表
        List<FileMeta> fileMetas = new ArrayList<>();

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            // 获取数据库连接
            connection = DBUtil.getConnection();

            // 构造 SQL 查询语句，通过 LIKE 运算符匹配文件名、拼音、拼音首字母
            String sql = "SELECT name, path, is_directory, size, last_modified FROM file_meta" +
                    " WHERE name LIKE ? OR pinyin LIKE ? OR pinyin_first LIKE ?" +
                    " ORDER BY path, name";
            statement = connection.prepareStatement(sql);

            // 设置预编译语句的参数，使用 %pattern% 进行模糊匹配
            //pattern 可能是文件名的一部分、文件名拼音的一部分，或拼音首字母的一部分
            statement.setString(1, "%" + pattern + "%");
            statement.setString(2, "%" + pattern + "%");
            statement.setString(3, "%" + pattern + "%");

            // 执行查询语句并获取结果集
            resultSet = statement.executeQuery();

            // 遍历结果集，构建符合条件的文件元数据对象并添加到列表中
            while (resultSet.next()) {
                String name = resultSet.getString("name");
                String path = resultSet.getString("path");
                boolean isDirectory = resultSet.getBoolean("is_directory");
                long size = resultSet.getLong("size");
                Timestamp lastModified = resultSet.getTimestamp("last_modified");
                //获取每一条查询到的结果,将其保存在一个对象中
                FileMeta fileMeta = new FileMeta(name, path, isDirectory, size, lastModified.getTime());
                //将每个拥有一条结果的对象保存到列表中
                fileMetas.add(fileMeta);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // 关闭数据库连接、预编译语句和结果集
            DBUtil.close(connection, statement, resultSet);
        }
        return fileMetas;
    }


    /**
     * 根据给定的路径查询该路径下的文件和目录。
     * 注意：该方法在后续重新扫描和更新数据库时会使用。
     * @param targetPath 给定的路径
     * @return 包含给定路径下文件和目录的文件元数据列表
     */
    public List<FileMeta> searchByPath(String targetPath) {
        // 用于存储查询结果的文件元数据列表
        List<FileMeta> fileMetas = new ArrayList<>();

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            // 获取数据库连接
            connection = DBUtil.getConnection();

            // 构造 SQL 查询语句，根据路径查询文件和目录信息
            String sql = "SELECT name, path, is_directory, size, last_modified FROM file_meta" +
                    " WHERE path = ?";
            statement = connection.prepareStatement(sql);

            // 设置预编译语句的参数为目标路径
            statement.setString(1, targetPath);

            // 执行查询语句并获取结果集
            resultSet = statement.executeQuery();

            // 遍历结果集，构建文件元数据对象并添加到列表中
            while (resultSet.next()) {
                String name = resultSet.getString("name");
                String path = resultSet.getString("path");
                boolean isDirectory = resultSet.getBoolean("is_directory");
                long size = resultSet.getLong("size");
                Timestamp lastModified = resultSet.getTimestamp("last_modified");
                //获取每一条查询到的结果,将其保存在一个对象中
                FileMeta fileMeta = new FileMeta(name, path, isDirectory, size, lastModified.getTime());
                //将每个拥有一条结果的对象保存到列表中
                fileMetas.add(fileMeta);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // 关闭数据库连接、预编译语句和结果集
            DBUtil.close(connection, statement, resultSet);
        }
        return fileMetas;
    }


    /**
     * 5. 删除数据
     * @param fileMetas
     */
    //    发现某个文件已经从磁盘上删掉了, 此时就需要把表里的内容也进行更新.
    //    进行删除的时候, 可能当前被删除的是一个普通文件(直接删除对应的表记录就行了)
    //    也有可能删除的时候, 被删除的是一个目录, 此时, 就需要把目录里包含的子文件/子目录也都统一删除掉.
    public void delete(List<FileMeta> fileMetas) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            connection.setAutoCommit(false);

            // 此处构造的 SQL , 要根据当前删除的内容情况, 来区分对待
            for (FileMeta fileMeta : fileMetas) {
                String sql = null;
                if (!fileMeta.isDirectory()) {
                    // 针对普通文件的删除操作
                    sql = "delete from file_meta where name=? and path=?";
                } else {
                    // 针对目录的删除操作
                    // 例如, 当前要删除的 path 是 d:/test
                    // 此处 path like ? 要替换成形如 'd:/test%' => 目的就是把当前这个被删除的目录下面的子文件和子目录都删掉.
                    sql = "delete from file_meta where (name=? and path=?) or (path like ?)";
                }
                // 此处就不能像前面的 add 一样, 使用 addBatch 了. addBatch 前提是, sql 是一个模板
                // 把 ? 替换成不同的值. 此处 sql 是不一定相同的.
                // 因此此处就需要重新构造出 statement 对象来表示这个 SQL 了
                statement = connection.prepareStatement(sql);
                if (!fileMeta.isDirectory()) {
                    // 普通文件, 需要替换两个 ?
                    statement.setString(1, fileMeta.getName());
                    statement.setString(2, fileMeta.getPath());
                } else {
                    // 针对目录, 需要替换三个 ?
                    statement.setString(1, fileMeta.getName());
                    statement.setString(2, fileMeta.getPath());
                    statement.setString(3, fileMeta.getPath() + File.separator + fileMeta.getName() + File.separator + "%");
                }
                // 真正执行这里的删除操作
                statement.executeUpdate();
                System.out.println("[delete] " + fileMeta.getPath() + fileMeta.getName());

                // 注意!! 此处代码中是有多个 Statement 对象的. 每个对象都得关闭一次.
                statement.close();
            }
            // 告诉数据库, 事务执行完毕了.
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            DBUtil.close(connection, null, null);
        }
    }
}
