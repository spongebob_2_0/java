package dao;

import org.sqlite.SQLiteDataSource;

import javax.sql.DataSource;
import java.io.File;
import java.sql.*;

/**
 * 用来连接sqlite数据库和关闭资源
 */
public class DBUtil {
    // 使用 单例模式 来提供 DataSource
    private static volatile DataSource dataSource = null;


    public static DataSource getDataSource() {
        if (dataSource == null) {
            synchronized (DBUtil.class) {
                if (dataSource == null) {
                    dataSource = new SQLiteDataSource();
                    // 使用 File.separator 使路径跨平台
                    //使用相对路径：在应用程序的工作目录或某个子目录中存放数据库。
                    // 这样，只要应用程序能够访问其工作目录，它就可以找到数据库。
                    String dbPath = "." + File.separator + "myDatabase.db";
                    ((SQLiteDataSource)dataSource).setUrl("jdbc:sqlite:" + dbPath);
                }
            }
        }
        return dataSource;
    }

    public static Connection getConnection() throws SQLException {
        //先获取数据源,然后连接数据库
        return getDataSource().getConnection();
    }

    public static void close(Connection connection, Statement statement, ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
