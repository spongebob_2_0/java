package util;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

/**
 * 拼音工具类，用于将汉字转换成拼音字符串。
 */
public class PinyinUtil {

    /**
     * 获取指定字符串的拼音表示。
     *
     * @param src       要获取拼音的字符串
     * @param fullSpell 是否为全拼（true 表示全拼，false 表示首字母拼音）
     * @return 拼音表示字符串
     */
    public static String get(String src, boolean fullSpell) {
        // 对输入字符串进行前后空白字符修剪，确保字符串内容有效。
        if (src == null || src.trim().length() == 0) {
            // 字符串为空或只包含空白字符，无需转换，返回 null。
            return null;
        }

        // 针对 Pinyin4j 进行稍许配置，允许将拼音中的 yu 用 v 表示。
        HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
        format.setVCharType(HanyuPinyinVCharType.WITH_V);

        // 遍历字符串的每个字符，将每个字符的拼音结果转换并拼接到 StringBuilder 中。
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < src.length(); i++) {
            char ch = src.charAt(i);

            // 针对单个字符进行拼音转换。
            String[] tmp = null;
            try {
                tmp = PinyinHelper.toHanyuPinyinStringArray(ch, format);
            } catch (BadHanyuPinyinOutputFormatCombination e) {
                // 如果拼音转换异常，则打印异常信息。
                e.printStackTrace();
            }

            if (tmp == null || tmp.length == 0) {
                // 如果转换结果为空数组，表示当前字符无汉语拼音，
                // 在结果中保留原字符，直接加入到拼音表示中。
                stringBuilder.append(ch);
            } else if (fullSpell) {
                // 拼音结果非空，且获取全拼。
                stringBuilder.append(tmp[0]);
            } else {
                // 拼音结果非空，获取首字母拼音。
                stringBuilder.append(tmp[0].charAt(0));
            }
        }
        return stringBuilder.toString();
    }
}