package service;

import dao.FileDao;
import dao.FileMeta;
import manager.FileManager;

import java.io.File;
import java.util.List;

/**
 * SearchService 类用于描述程序的核心业务逻辑，主要负责文件的搜索和同步操作。
 */
public class SearchService {

    // 创建 FileDao 对象用于数据库操作
    private FileDao fileDao = new FileDao();

    // 创建 FileManager 对象用于文件系统扫描操作
    private FileManager fileManager = new FileManager();

    // 使用一个线程来周期性地扫描文件系统，确保数据的实时同步
    private Thread t = null;

    /**
     * 初始化 SearchService，包括数据库初始化和启动文件系统扫描线程。
     *
     * @param basePath 需要进行扫描的基础路径。
     */
    public void init(String basePath) {

        // 初始化数据库
        fileDao.initDB();

        // 使用线程来周期性地扫描指定的文件目录，这样可以确保数据库中的记录与文件系统实际内容保持同步。
        // 注意，此扫描操作现在是在一个新线程中进行，而不是界面主线程中进行了。
        // fileManager.scanAll(new File(basePath));
        t = new Thread(() -> {
            while (!t.isInterrupted()) {
                fileManager.scanAll(new File(basePath));
                try {
                    // 每次扫描结束后，线程休眠一段时间（目前设置为20秒）再开始下一次扫描。
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                    // 如果线程被中断，则终止扫描，并打印异常信息。
                    e.printStackTrace();
                    break;
                }
            }
        });
        // 启动扫描线程
        t.start();
        System.out.println("[SearchService] 初始化完成!");
    }

    /**
     * 关闭文件系统扫描线程，停止所有后台扫描操作。
     */
    public void shutdown() {
        // 如果扫描线程存在并运行中，那么中断它。
        if (t != null) {
            t.interrupt();
        }
    }

    /**
     * 提供一个根据模式查找文件的方法。
     *
     * @param pattern 要查找的文件名模式。
     * @return 返回符合模式的文件列表。
     */
    public List<FileMeta> search(String pattern) {
        // 从数据库中根据给定的模式查找文件，并返回文件列表。
        return fileDao.searchByPattern(pattern);
    }
}
