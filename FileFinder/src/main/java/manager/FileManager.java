package manager;

import dao.FileDao;
import dao.FileMeta;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * FileManager 类用于执行对指定目录的扫描，并将扫描结果同步更新到数据库中。
 */
public class FileManager {

    // 创建一个 FileDao 对象，用于访问数据库
    private FileDao fileDao = new FileDao();
    // 初始化计数器值为1，当线程池执行完所有任务之后，就立即调用一次 countDown 方法通知主线程继续执行
    private CountDownLatch countDownLatch = null;
    // 用来衡量任务结束的计数器，初始值为 0，后续会根据任务完成情况进行自增和自减操作
    private AtomicInteger taskCount = new AtomicInteger(0);


    /**
     * 执行针对指定基础路径的完整扫描操作。
     * @param basePath 要扫描的基础路径
     */
    public void scanAll(File basePath) {
        System.out.println("[FileManager] scanAll 开始!"); // 打印扫描开始信息
        long beg = System.currentTimeMillis(); // 记录开始时间

        // 初始化同步计数器值为1，用于等待任务完成 (每次调用这个方法都会重新初始化)
        countDownLatch = new CountDownLatch(1);

        // 通过线程池方式对指定基础路径及其子目录进行扫描操作。
        scanAllByThreadPool(basePath);
        //通过单线程方式对指定基础路径及其子目录进行完整扫描操作。
//        scanAllByOneThread(basePath);

        try {
            countDownLatch.await(); // 等待所有任务完成
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long end = System.currentTimeMillis(); // 记录结束时间
        System.out.println("[FileManager] scanAll 结束! 执行时间: " + (end - beg) + " ms"); // 打印扫描结束信息及执行时间
    }


    /**
     * 通过单线程方式对指定基础路径及其子目录进行完整扫描操作。
     * @param basePath 要扫描的基础路径
     */
    private void scanAllByOneThread(File basePath) {
        if (!basePath.isDirectory()) {
            return; // 如果路径不是目录，则直接返回，无需扫描
        }

        // 针对当前目录进行扫描操作
        scan(basePath);

        // 列出当前目录下包含的所有文件
        File[] files = basePath.listFiles();
        if (files == null || files.length == 0) {
            return; // 当前目录下没有文件或子目录，直接返回
        }
        for (File f : files) {
            if (f.isDirectory()) {  //判断是否是目录
                scanAllByOneThread(f); // 递归调用自身，扫描子目录
            }
        }
    }

    //创建一个包含固定线程数的线程池。
    private static ExecutorService executorService = Executors.newFixedThreadPool(8);

    /**
     * 通过线程池方式对指定基础路径及其子目录进行扫描操作。
     * @param basePath 要扫描的基础路径
     */
    private void scanAllByThreadPool(File basePath) {
        if (!basePath.isDirectory()) {
            return; // 如果路径不是目录，则直接返回，无需扫描
        }

        // 计数器自增
        taskCount.getAndIncrement(); // 增加任务计数

        // 将扫描操作提交到线程池中进行
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    scan(basePath); // 执行扫描操作
                } finally {
                    // 计数器自减
                    // 使用 finally 块确保计数器递减，即使发生异常也能正常执行
                    taskCount.getAndDecrement(); // 减少任务计数
                    if (taskCount.get() == 0) {
                        // 如果所有任务已完成，通知主线程继续执行
                        countDownLatch.countDown();
                    }
                }
            }
        });

        // 继续递归处理其他目录
        File[] files = basePath.listFiles();
        if (files == null || files.length == 0) {
            return; // 当前目录下没有文件或子目录，直接返回
        }
        for (File f : files) {
            if (f.isDirectory()) {
                scanAllByThreadPool(f); // 递归调用自身，处理子目录
            }
        }
    }


    /**
     *  scan 方法针对一个目录进行处理. (整个遍历目录过程中的基本操作)
     *  这个方法只针对当前 path 对应的目录进行分析.
     *  列出这个 path 下包含的文件和子目录, 并且把这些内容更新到数据库中.
     *  此方法不考虑子目录里面的内容.
     * @param path
     */
    private void scan(File path) {
        // 需要debug调试的时候,可以注释下面扫描日志打印
         System.out.println("[FileManager] 扫描路径: " + path.getAbsolutePath());

        // 1. 首先，我们要获取文件系统中真实存在的文件和目录列表。
        // 初始化一个 List 用于存储扫描到的文件。
        List<FileMeta> scanned = new ArrayList<>();
        File[] files = path.listFiles(); // 获取目录下所有文件和子目录
        if (files != null) {
            for (File f : files) {
                // 将每个文件或目录构建为一个 FileMeta 对象，并加入到 scanned 列表中。
//                System.out.println("本地文件" + f.getPath());
                scanned.add(new FileMeta(f));
            }
        }

        // 2. 接下来，我们要获取数据库中已有的记录，这些记录是该路径下之前保存的文件和目录列表。
        // 使用 fileDao 从数据库中搜索该路径下的所有文件。
        List<FileMeta> saved = fileDao.searchByPath(path.getPath());

        // 3. 现在，我们需要找出数据库中有但文件系统中已经不存在的文件。
        // 这意味着这些文件可能已经被用户删除或移动了。
        List<FileMeta> forDelete = new ArrayList<>();
        for (FileMeta fileMeta : saved) {
            //重写了fileMeta的equals方法
            //scanned(系统中存在的文件),  fileMeta(数据库中存在的文件)
            if (!scanned.contains(fileMeta)) {
                // 如果 saved 中的 fileMeta 在 scanned 中不存在，则说明该文件/目录已经在文件系统中被删除了。
                // 那么我们就需要在数据库中也将其删除。
                forDelete.add(fileMeta);
            }
        }
        // 调用 fileDao 的删除方法，删除那些不存在的文件记录。
        fileDao.delete(forDelete);

        // 4. 最后，我们要找出文件系统中新添加的，但数据库中尚未有记录的文件。
        List<FileMeta> forAdd = new ArrayList<>();
        for (FileMeta fileMeta : scanned) {
            if (!saved.contains(fileMeta)) {
                // 如果 scanned 中的 fileMeta 在 saved 中不存在，说明这是一个新文件/目录。
                // 我们需要将其添加到数据库中。
                forAdd.add(fileMeta);
            }
        }
        // 调用 fileDao 的添加方法，插入那些新的文件记录。
        fileDao.add(forAdd);
    }
}
