package gui;


import dao.FileMeta;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Window;
import service.SearchService;

import java.awt.*;
import java.io.File;
import java.net.URI;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.MouseButton;

/**
 * GUIController 是用于处理与图形用户界面交互的逻辑。
 */
public class GUIController implements Initializable {

    // 使用 @FXML 注解将 FXML 文件中的元素和这里的变量绑定起来

    @FXML
    private GridPane gridPane; // 界面上的网格布局面板

    @FXML
    private Button button; // 界面上的按钮

    @FXML
    private Label label; // 界面上的标签

    @FXML
    private TextField textField; // 界面上的文本输入框

    @FXML
    private TableView<FileMeta> tableView; // 界面上的表格视图


    // 将上下文菜单设置为类成员，这样其他方法也可以访问它
    private ContextMenu contextMenu;

    // 这个属性用于搜索服务逻辑，不是界面元素
    private SearchService searchService = null;

    /**
     * 这个方法会在FXML文件加载完毕后立即执行，用于初始化界面和设置事件监听。
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // 为输入框的文本属性添加一个监听器
        textField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                // 用户每次修改输入框的内容时都会调用此方法
                // 此处的任务是根据新值重新执行查询操作
                freshTable(newValue);
            }
        });
        // 为表格添加双击事件监听器
        tableView.setOnMousePressed(event -> {
            if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                openAndLocateFile();
            }
        });
        //初始化右键上下文菜单的方法
        initContextMenu();

    }

    /**
     * 当用户点击按钮时，此方法会被调用。用户可以选择一个目录。
     * @param mouseEvent 鼠标事件对象。
     */
    public void choose(MouseEvent mouseEvent) {
        // 创建一个目录选择器
        DirectoryChooser directoryChooser = new DirectoryChooser();
        // 显示目录选择对话框
        Window window = gridPane.getScene().getWindow();
        File file = directoryChooser.showDialog(window);
        if (file == null) {
            System.out.println("当前用户选择的路径为 空 ");
            return;
        }
        System.out.println(file.getAbsolutePath());

        // 将用户选择的路径显示在标签上
        label.setText(file.getAbsolutePath());

        // 如果 searchService 已经存在，则停止之前的扫描任务
        if (searchService != null) {
            searchService.shutdown();
        }
        // 为选定的目录启动一个新的扫描任务，并将数据添加到数据库中
        searchService = new SearchService();
        searchService.init(file.getAbsolutePath());
    }


    /**
     * 重新查询数据库，并将结果设置到表格中。
     *
     * @param query 需要查询的关键词。
     */
    private void freshTable(String query) {
        // 查询操作依赖于 SearchService
        if (searchService == null) {
            System.out.println("searchService 尚未初始化, 不能查询!");
            return;
        }
        // 获取表格的数据集合并清空旧数据
        ObservableList<FileMeta> fileMetas = tableView.getItems();
        fileMetas.clear();
        // 根据查询关键词搜索文件
        List<FileMeta> results = searchService.search(query);
        // 将查询结果添加到表格中
        fileMetas.addAll(results);
    }

    /**
     * 用于打开并定位文件的方法
     */
    private void openAndLocateFile() {
        // 获取表格中当前选中的文件
        FileMeta selectedItem = tableView.getSelectionModel().getSelectedItem();
        // 如果选中了某个文件
        if (selectedItem != null) {
            try {
                // 获取文件的绝对路径和文件名
                String fullPath = selectedItem.getPath() + File.separator + selectedItem.getName();
                // 使用系统命令来在资源管理器中打开文件的完整位置
                ProcessBuilder processBuilder = new ProcessBuilder("explorer", "/select,", fullPath);
                processBuilder.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



    /**
     * 联系作者的功能
     * @param mouseEvent
     */
    public void contactAuthor(MouseEvent mouseEvent) {
        try {
            // 打开与作者的QQ对话框
            Desktop.getDesktop().browse(new URI("tencent://message/?uin=55434818&Site=&Menu=yes"));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("打开QQ对话框失败");
        }
    }

    /**
     * 初始化右键上下文菜单的方法
     */
    private void initContextMenu() {
        // 创建一个新的上下文菜单对象
        contextMenu = new ContextMenu();

        // 创建一个"资源管理器中显示"菜单项
        MenuItem showItem = new MenuItem("资源管理器中显示");
        // 为"显示"菜单项设置点击事件
        showItem.setOnAction(event -> {
            // 获取表格中当前选中的文件
            FileMeta selectedItem = tableView.getSelectionModel().getSelectedItem();
            // 如果选中了某个文件
            if (selectedItem != null) {
                try {
//                    // 使用默认应用程序打开该文件
//                    Desktop.getDesktop().open(new File(selectedItem.getPath()));
                    // 获取文件的绝对路径和文件名
                    String fullPath = selectedItem.getPath() + File.separator + selectedItem.getName();
                    // 使用系统命令来在资源管理器中打开文件的位置
                    ProcessBuilder processBuilder = new ProcessBuilder("explorer", "/select,", fullPath);
                    processBuilder.start();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        // 创建一个"复制父路径"菜单项
        MenuItem copyParentItem = new MenuItem("复制父路径");
        // 为"复制路径"菜单项设置点击事件
        copyParentItem.setOnAction(event -> {
            // 获取表格中当前选中的文件
            FileMeta selectedItem = tableView.getSelectionModel().getSelectedItem();
            // 如果选中了某个文件
            if (selectedItem != null) {
                // 获取系统剪贴板
                Clipboard clipboard = Clipboard.getSystemClipboard();
                // 创建剪贴板内容
                ClipboardContent content = new ClipboardContent();
                // 将文件路径添加到剪贴板内容中
                content.putString(selectedItem.getPath());
                // 将内容放入系统剪贴板
                clipboard.setContent(content);
            }
        });

        // 创建一个"复制所在路径"菜单项
        MenuItem copyItem = new MenuItem("复制所在路径");
        // 为"复制路径"菜单项设置点击事件
        copyItem.setOnAction(event -> {
            // 获取表格中当前选中的文件
            FileMeta selectedItem = tableView.getSelectionModel().getSelectedItem();
            // 如果选中了某个文件
            if (selectedItem != null) {
                // 获取系统剪贴板
                Clipboard clipboard = Clipboard.getSystemClipboard();
                // 创建剪贴板内容
                ClipboardContent content = new ClipboardContent();
                // 将文件路径添加到剪贴板内容中
                content.putString(selectedItem.getPath() + File.separator + selectedItem.getName());
                // 将内容放入系统剪贴板
                clipboard.setContent(content);
            }
        });

        // 创建一个"打开当前文件"菜单项
        MenuItem openFileItem = new MenuItem("打开当前文件");
        // 为"打开当前文件"菜单项设置点击事件
        openFileItem.setOnAction(event -> {
            // 获取表格中当前选中的文件
            FileMeta selectedItem = tableView.getSelectionModel().getSelectedItem();
            // 如果选中了某个文件
            if (selectedItem != null) {
                try {
                    // 获取文件的绝对路径和文件名
                    String fullPath = selectedItem.getPath() + File.separator + selectedItem.getName();
                    // 使用默认应用程序打开该文件
                    Desktop.getDesktop().open(new File(fullPath));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        // 将上述两个菜单项添加到上下文菜单中
        contextMenu.getItems().addAll(openFileItem,showItem, copyParentItem, copyItem);

        // 为表格设置鼠标点击事件
        tableView.setOnMouseClicked(event -> {
            // 如果是右键点击
            if (event.getButton() == MouseButton.SECONDARY) {
                // 在指定的屏幕位置显示上下文菜单
                contextMenu.show(tableView, event.getScreenX(), event.getScreenY());
            } else {
                // 如果是左键或中键点击，关闭上下文菜单
                contextMenu.hide();
            }
        });
    }

}

