package gui;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * GUIClient 类是 JavaFX 应用程序的入口，负责启动图形用户界面。
 */
public class GUIClient extends Application {

    /**
     * 这个 start 方法是 JavaFX 应用程序的主入口点。
     * 当应用程序启动时，此方法会被自动调用，用于进行程序的初始化操作。
     *
     * @param primaryStage 主舞台，用于展示应用程序的主场景。
     * @throws Exception 如果初始化过程中发生异常，将抛出该异常。
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        // 使用 FXMLLoader 加载 FXML 文件内容。FXML 文件定义了应用程序的界面布局和元素。
        Parent parent = FXMLLoader.load(GUIClient.class.getClassLoader().getResource("app.fxml"));

        // 设置主舞台的场景，同时定义场景的大小为 1000x800。
        primaryStage.setScene(new Scene(parent, 840, 610));

        // 设置主舞台的标题。
        primaryStage.setTitle("文件搜索工具-书生");

        // 显示主舞台，这样用户就可以看到应用程序的界面了。
        primaryStage.show();
    }

    /**
     * 程序的主方法，Java 程序的执行从这里开始。
     * @param args 命令行参数。
     */
    public static void main(String[] args) {
        // 使用 JavaFX 提供的 launch 方法来启动应用程序。此方法将调用上面的 start 方法。
        launch(args);
    }
}
