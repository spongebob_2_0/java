package login;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-05-27
 * Time: 16:37
 */
@WebServlet("/index")
public class IndexServlet extends HttpServlet {
    //通过重定向,浏览器发送的是 GET.
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //先判断用户的登录状态
        //如果用户还没登录,要求先登录
        //已经登录了,根据 会话 中的用户名,来显示到页面上
        //1.这个操作不会触发会话的创建
        HttpSession session = req.getSession(false);
        if(session == null) {
            //未登录状态
            System.out.println("用户未登录!");
            resp.sendRedirect("login.html");
            return;
        }
        //2.已经登录
        String username = (String) session.getAttribute("username");
        //3.构造页面
        resp.setContentType("text/html;charSet=utf8");
        resp.getWriter().write("欢迎" + username + "回来!");
    }
}
