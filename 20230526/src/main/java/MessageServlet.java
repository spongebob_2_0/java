import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-05-26
 * Time: 14:46
 */
class Message {
    public String from;
    public String to;
    public String message;
}
@WebServlet("/message")
public class MessageServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    //从服务器获取数据
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //显示的告诉浏览器,数据是json格式的,字符集是utf8的
        resp.setContentType("application/json;charSet=utf8");
        //顺序表接收从数据库读取到的所有message对象
        List<Message> messageList = load();
        //writeValue方法只是把messageList转成json格式的字符串
        //并写在HTTP响应的正文body中
        objectMapper.writeValue(resp.getWriter(),messageList);
    }

    //向服务器提交数据
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //把body中的数据读取出来了,然后解析成Message对象
        Message message =  objectMapper.readValue(req.getInputStream(),Message.class);
        //将message对象存入数据库
        save(message);
        //此处的状态码可以省略.默认也是200
        resp.setStatus(200);
    }

    //提供一对方法
    //往数据库中存一条消息
    private void save(Message message) {
        //JDBC操作
        Connection connection = null;
        PreparedStatement statement = null;
        //1.建立连接
        try {
            connection = DBUtil.getConnection();
            //2.构造sql语句
            String sql = "insert into message values(?,?,?,null)";
            statement = connection.prepareStatement(sql);
            statement.setString(1,message.from);
            statement.setString(2,message.to);
            statement.setString(3,message.message);
            //3.执行sql
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            //4,关闭连接
            DBUtil.close(connection,statement,null);
        }
    }

    //从数据库取所有消息
    private List<Message> load() {
        List<Message> messageList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "select * from message";
            statement = connection.prepareStatement(sql);
            //3.执行sql
            resultSet = statement.executeQuery();
            //遍历结果集合
            while (resultSet.next()) {
                Message message = new Message();
                message.from = resultSet.getString("from");
                message.to = resultSet.getString("to");
                message.message = resultSet.getString("message");
                messageList.add(message);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            //释放资源,断开连接
            DBUtil.close(connection,statement,resultSet);
        }
        return messageList;
    }
}
