import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@WebServlet("/revert")
public class RevertServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    
    // 处理删除最新消息的 HTTP POST 请求
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        deleteLatestMessage();
        resp.setStatus(200);
    }
    
    private void deleteLatestMessage() {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            // 建立连接
            connection = DBUtil.getConnection();
            // 构造sql语句
            String sql = "DELETE FROM message WHERE id = (\n" +
                    "  SELECT id FROM (\n" +
                    "    SELECT id FROM message ORDER BY id DESC LIMIT 1\n" +
                    "  ) tmp\n" +
                    ");\n";
            statement = connection.prepareStatement(sql);
            // 执行sql
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            // 关闭连接
            DBUtil.close(connection, statement, null);
        }
    }
}
