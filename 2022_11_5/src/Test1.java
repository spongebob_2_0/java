/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-05
 * Time: 0:51
 */

import java.util.*;

public class Test1 {
    public static void main(String[] args) {
        int[] arr ={1,2,3,4,5,6};
        System.out.println(Arrays.toString(reorder(arr)));
    }
    public static int[] reorder(int[] arr){
        int left =0;
        int right =arr.length-1;
        int[] tmp =new int[arr.length];
        for (int j:arr) {
            if(j%2!=0){
                tmp[left]=j;
                left++;
            }else{
                tmp[right]=j;
                right--;
            }
        }
        arr=tmp;
        return arr;
    }
}
