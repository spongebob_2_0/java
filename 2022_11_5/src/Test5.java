/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-06
 * Time: 1:31
 */

import java.util.*;

public class Test5 {
    public static boolean threeOddNumbers(int[] arr) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 1) {
                count++;
            } else {
                count = 0;
            }
            if (count == 3) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        int[] arr ={1,2,34,3,4,5,7,23,12};
        System.out.println(threeOddNumbers(arr));
    }
}
