/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-06
 * Time: 1:23
 */

import java.util.*;

public class Test4 {
        public static int mostElements(int[] arr) {
            Arrays.sort(arr);
            return arr[arr.length / 2];
        }

    public static void main(String[] args) {
        int[] arr={2,2,1,1,1,2,2};
        int ret = mostElements(arr);
        System.out.println(ret);
    }
}
