/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-05
 * Time: 18:12
 */

import java.util.*;

public class Test3 {
    public static int[] twoSum(int[] nums,int n){
        int len =nums.length;
        for(int i =0;i<len;i++){
            for(int j =i+1;j<len;j++){
                if(nums[i]+nums[j]==n){
                    return new int[]{i,j};
                }
            }
        }
        return null;
    }
    public static void main(String[] args) {
        int[] nums ={1,5,6,7,15};
        Scanner sc =new Scanner(System.in);
        int target = sc.nextInt();
        int[] ret = twoSum(nums,target);
        System.out.println("下标是"+Arrays.toString(ret));
    }
}
