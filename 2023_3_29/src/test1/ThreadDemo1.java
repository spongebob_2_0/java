package test1;

public class ThreadDemo1 {
    private static Object lock = new Object();
    private static int count = 0;

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
            try {
                synchronized (lock) {
                    while (count < 30) {
                        while (count % 3 != 0) {
                            lock.wait();
                        }
                        System.out.print("A");
                        count++;
                        lock.notifyAll();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread t2 = new Thread(() -> {
            try {
                synchronized (lock) {
                    while (count < 30) {
                        while (count % 3 != 1) {
                            lock.wait();
                        }
                        System.out.print("B");
                        count++;
                        lock.notifyAll();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread t3 = new Thread(() -> {
            try {
                synchronized (lock) {
                    while (count < 30) {
                        while (count % 3 != 2) {
                            lock.wait();
                        }
                        System.out.print("C\n");
                        count++;
                        lock.notifyAll();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        t1.start();
        t2.start();
        t3.start();
        t1.join();
        t2.join();
        t3.join();
    }
}
