package test2;

/**
 * 三个线程
 * 打印十行ABC
 */

public class ThreadDemo1 {
    public static void main(String[] args) throws InterruptedException {
        for(int i=0;i<10;i++) {

            Thread t1 = new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.print("A");
                }
            });
            t1.start();
            t1.join();
            Thread t2 = new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.print("B");
                }
            });
            t2.start();
            t2.join();
            Thread t3 = new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.println("C");
                }
            });
            t3.start();
            t3.join();
        }
    }
}






