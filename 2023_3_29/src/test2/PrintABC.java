package test2;

public class PrintABC {
    private static Object lock = new Object();
    private static int count = 0;

    public static void main(String[] args) {
        Thread threadA = new Thread(() -> {
            try {
                synchronized (lock) {
                    while (count < 30) {
                        while (count % 3 != 0) {
                            lock.wait();
                        }
                        System.out.print("A");
                        count++;
                        lock.notifyAll();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread threadB = new Thread(() -> {
            try {
                synchronized (lock) {
                    while (count < 30) {
                        while (count % 3 != 1) {
                            lock.wait();
                        }
                        System.out.print("B");
                        count++;
                        lock.notifyAll();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread threadC = new Thread(() -> {
            try {
                synchronized (lock) {
                    while (count < 30) {
                        while (count % 3 != 2) {
                            lock.wait();
                        }
                        System.out.print("C\n");
                        count++;
                        lock.notifyAll();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        for (int i = 0; i < 10; i++) {
            threadA.start();
            threadB.start();
            threadC.start();

            try {
                threadA.join();
                threadB.join();
                threadC.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
