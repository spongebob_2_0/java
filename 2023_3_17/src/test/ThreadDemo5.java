package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-17
 * Time: 3:45
 */
//线程不安全
class Counter {
    private int count = 0;
    private Object locker =new Object();
    public synchronized void add() {
            count++;
    }
    public int get() {
        return count;
    }
}
public class ThreadDemo5 {
    public static void main(String[] args) throws InterruptedException {
        Counter counter =new Counter();

        //搞两个线程,两个线程分别对这个counter自增 5w 次
        Thread t1 =new Thread(() -> {
            for(int i = 0; i < 50000; i++) {
                counter.add();
            }
        });
        Thread t2 =new Thread(() -> {
            for(int i = 0; i < 50000; i++) {
                counter.add();
            }
        });
        t1.start();
        t2.start();

        //等待两个线程结束,然后看结果
        t1.join();;
        t2.join();
        System.out.println(counter.get());
    }
}
