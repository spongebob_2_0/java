package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-17
 * Time: 1:53
 */
public class ThreadDemo2 {
    public static void main(String[] args) {
        Thread t =new Thread(() -> {
            //currentThread()方法 是获取当前线程实例
            //此处currentThread() 获取的对象就是t
            //isInterrupted() 就是t对象里自带的一个标志位
            while (!Thread.currentThread().isInterrupted()) {
                System.out.println("hello-敏");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
            }
        });
        t.start();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //把t内部的标志位设置成true
        t.interrupt();
    }
}
