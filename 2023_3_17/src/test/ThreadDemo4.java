package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-17
 * Time: 3:31
 */
public class ThreadDemo4 {
    public static void main(String[] args) throws InterruptedException {
        Thread t =new Thread(() -> {
            while (true) {
//                System.out.println("hello");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        System.out.println(t.getState());
        t.start();
        Thread.sleep(2000);
        System.out.println(t.getState());
    }
}
