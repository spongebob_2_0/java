package test;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-18
 * Time: 12:49
 */
public class ThreadDemo10 {
    static int sum1 =0;
    static int sum2 =0;
    public static void main(String[] args) throws InterruptedException {
        long startTime =System.currentTimeMillis();
        int[] array =new int[10000000];
        Random random =new Random();
        for(int i = 0; i < array.length; i++) {
            array[i]=random.nextInt(100)+1;
        }
        Thread t =new Thread(() -> {
                for (int i = 0; i < array.length; i += 2) {
                    sum1 += array[i];
                }
                System.out.println("线程1:"+sum1);
        });
        t.start();

            for(int i = 1; i < array.length; i+=2) {
                sum2 +=array[i];
            }
            System.out.println("线程2:"+sum2);
        t.join();
        System.out.println(sum1+sum2);
        long endTime =System.currentTimeMillis();
        System.out.println("执行时间:"+(endTime-startTime));
    }
}
