package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-17
 * Time: 3:07
 */
public class ThreadDemo3 {
    public static void main(String[] args) {
        Thread t =new Thread(() -> {
            System.out.println("hell0-min");
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("hell0-main");
    }
}
