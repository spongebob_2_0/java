package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-17
 * Time: 17:26
 */

public class ThreadDemo7 {
    public static void main(String[] args) throws InterruptedException {
        Thread t =new Thread(() -> {
            for(int i =0; i<20; i++) {
                System.out.println(i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
        t.join();
        System.out.println("ok");
    }
}
