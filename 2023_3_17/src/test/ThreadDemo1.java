package test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-17
 * Time: 1:36
 */
public class ThreadDemo1 {
    public static boolean isQuit =false;
    public static void main(String[] args) {
        List<Integer> list =new ArrayList<>();
        Thread t =new Thread(() -> {
            while (!isQuit) {
                System.out.println("hello-敏");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("t-线程终止");
        });
        t.start();
        //在主线程中修改isQuit
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        isQuit =true;
    }
}
