package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-17
 * Time: 17:31
 */
class MyThread extends Thread {
    @Override
    public void run() {
        while (true) {
            System.out.println("hello-t");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
public class ThreadDemo8 {
    public static void main(String[] args) {
        Thread t =new MyThread();
        //如果不启动线程直接调用run方法,那么run方法还是在同一个线程中执行
        t.run();
        //start方法是创建一个新的线程,自动调用新线程的run方法
        t.start();
        while (true) {
            System.out.println("hello-main");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
