package test2;

public class ThreadDemo3 {
    public static void main(String[] args) throws InterruptedException {
        Object obj = new Object();
        Thread t1 = new Thread(()->{
            for(int i = 0;i<1000;i++){
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            synchronized (obj){
                obj.notify();
            }
            System.out.println("线程t1执行完毕!");
        });
        Thread t2 = new Thread(()->{
            synchronized (obj){
                try {
                    obj.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        t1.start();
        //此时t2没有调用start方法,是NEW
        System.out.println("t2 start之前:"+t2.getState());
        t2.start();
        Thread.sleep(10);
        //此时线程t2等待线程t1执行完毕,应该是WAITING状态
        System.out.println("t2 wait中:"+t2.getState());
        t1.join();
        t2.join();
        //此时t2执行完毕,TERMINATED
        System.out.println("t2执行完成:"+t2.getState());
    }
}