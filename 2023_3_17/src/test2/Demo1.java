package test2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-18
 * Time: 18:47
 */
interface Run {
    public abstract void func();
}
abstract class Swim {
    public abstract void func2();
}
public class Demo1 {
    public static void main(String[] args) {
        new Run() {
            @Override
            public void func() {
                System.out.println("跑步能减肥");
            }
        }.func();
        new Swim() {
            @Override
            public void func2() {
                System.out.println("他在游泳");
            }
        }.func2();
        Swim s =new Swim() {
            @Override
            public void func2() {
                System.out.println("小敏在和我一起游泳");
            }
        };
        s.func2();
    }
}
