package test2;

public class ThreadDemo4 {
    public static void main(String[] args) throws InterruptedException {
        Object obj1 = new Object();
        Object obj2 = new Object();
        Thread t1 = new Thread(()->{
            synchronized (obj1){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                synchronized (obj2){
 
                }
            }
        });
        Thread t2 = new Thread(()->{
            synchronized (obj2){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                synchronized (obj1){
 
                }
            }
        });
 
        t1.start();
        Thread.sleep(100);
        t2.start();
        Thread.sleep(1500);
        System.out.println(t2.getState());
        t1.join();
        t2.join();
 
    }
}