package test2;

public class OutClass {
    private int a;
    static int b;
    int c;

    public void methodA() {
        a = 10;
        System.out.println(a);
    }

    public static void methodB() {
        System.out.println(b);
    }

    // 实例内部类：未被static修饰
     class InnerClass {
        int c;
        double sum =0;
        public void methodInner() {
// 在实例内部类中可以直接访问外部类中：任意访问限定符修饰的成员
            a = 100;
            b = 200;
            methodA();
            methodB();
// 如果外部类和实例内部类中具有相同名称成员时，优先访问的是内部类自己的
            c = 300;
            System.out.println(c);
// 如果要访问外部类同名成员时候，必须：外部类名称.this.同名成员名字
            OutClass.this.c = 400;
            System.out.println(OutClass.this.c);
        }
    }
}
class Main{
    public static void main1(String[] args) {
//        OutClass t1 =new OutClass();
//        OutClass.InnerClass s2 =t1.new InnerClass();
        OutClass.InnerClass s2 =new OutClass().new InnerClass();
            s2.sum =100;
        System.out.println(s2.sum);
    }

    public static void main(String[] args) {
        OutClass t =new OutClass();

    }
}
