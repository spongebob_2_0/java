package test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class TcpEchoClient {
    private Socket socket = null;

    public TcpEchoClient(String serverIp, int port) throws IOException {
        // 这个操作相当于让客户端和服务器建立 tcp 连接.
        // 这里的连接连上了, 服务器的 accept 就会返回.
        socket = new Socket(serverIp, port);
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);
        try (InputStream inputStream = socket.getInputStream();
             OutputStream outputStream = socket.getOutputStream()) {
            //outputStream 是一个字节输出流，通过 PrintWriter 对象的构造方法将其包装成字符输出流，以便能够方便地写入字符数据。
            PrintWriter printWriter = new PrintWriter(outputStream);
            //scannerFromSocket 对象是将输入流对象包装成一个 Scanner 对象，以便能够方便地读取输入流中的数据。
            // Scanner 对象会自动解析和分隔输入流中的数据，并将其转换为相应的数据类型或字符串。
            Scanner scannerFromSocket = new Scanner(inputStream);

            while (true) {
                // 1. 从键盘上读取用户输入的内容.
                System.out.print("-> ");
                String request = scanner.next();
                // 2. 把读取的内容构造成请求, 发送给服务器.
                //在 Java 中，可以通过 PrintWriter 对象的 println() 方法发送带有换行符的数据包。
                // 该方法会将指定的字符串添加一个换行符，并将其发送到输出流中。
                printWriter.println(request);
                //数据此时还在缓存区,使用 flush() 方法，可以确保数据立即发送
                printWriter.flush();
                // 3. 从服务器读取响应内容
                //next() 方法会读取下一个标记，而不是一行数据。标记通常是以空格、制表符或换行符为分隔符的单词或符号。
                // 因此，在读取数据时，如果数据包中只有一个标记，则可以使用 next() 方法读取该标记。
                String response = scannerFromSocket.next();
                // 4. 把响应结果显示到控制台上.
                // request 是客户端发送的请求，response 是服务器响应的数据。
                // 通过格式化输出的方式将这些信息打印出来，方便程序员进行调试和查看。
                System.out.printf("req: %s; resp: %s\n", request, response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
        TcpEchoClient client = new TcpEchoClient("127.0.0.1", 9090);
        client.start();
    }
}