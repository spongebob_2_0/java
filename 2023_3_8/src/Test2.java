import java.sql.Connection;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-08
 * Time: 18:33
 */
public class Test2 {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        Deque<Character> stack =new ArrayDeque<>();
        List<Character> list =new ArrayList<>();
        String str = scanner.nextLine();
        for(int i = 0; i < str.length(); i++) {
            char ch =str.charAt(i);
            if(!stack.isEmpty() && stack.peek() == ch){
                stack.pop();
            }else {
                stack.push(ch);
            }
        }
        if(stack.isEmpty()) {
            System.out.println("0");
        }else {
            while (!stack.isEmpty()) {
                list.add(stack.pop());
            }
            Collections.reverse(list);
        }
        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i));
        }
    }
}
