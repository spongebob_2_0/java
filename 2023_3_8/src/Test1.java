import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-08
 * Time: 17:46
 */
public class Test1 {
    public static boolean func(String s,String t) {
        int lens =s.length();
        int lent =t.length();
        int i = 0;
        int j = 0;
        while (i < lens && j < lent) {
            if(s.charAt(i) == t.charAt(j)) {
                i++;
                j++;
            }else {
                j++;
            }
        }
        return i == lens;
    }
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String s = scanner.nextLine();
            String t = scanner.nextLine();
            boolean ret = func(s, t);
            System.out.println(ret);
        }
    }
}
