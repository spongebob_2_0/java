/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-01
 * Time: 18:56
 */

import java.util.*;

public class Test2 {
    public static void twoSearch(int[] arr, int k) {
        int left = 0;
        int right = arr.length - 1;
        while (left <= right) {

            int i = (left + right) / 2;
            if (k > arr[i]) {
                left = i + 1;
            } else if (k < arr[i]) {
                right = i - 1;
            } else {
                System.out.println("找到了,下标是" + i);
                break;
            }
        }
        if (left > right) {
            System.out.println("找不到");
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNext()){
            int n = scanner.nextInt();
//        twoSearch(arr, n);
            int ret =Arrays.binarySearch(arr,n);
            if(ret>=0) {
                System.out.println("找到了,下标是"+ret);
            }else{
                System.out.println("找不到");
            }
        }
    }
}
