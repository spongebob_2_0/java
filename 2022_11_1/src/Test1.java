/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-01
 * Time: 16:58
 */

import java.util.*;

public class Test1 {
    public static void bubbleSort(int[] arr, int len) {
        for (int j = 0; j < len - 1; j++) {
            for (int i = 0; i < len - 1 - j; i++) {
                if (arr[i] < arr[i + 1]) {
                    int tmp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = tmp;
                }
            }
        }
    }

    public static void print(int[] arr, int len) {
        for (int i = 0; i < len; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] arr = {12, 2, 18, 24, 76, 13};
        int len = arr.length;
        System.out.print("排序前:");
        print(arr, len);
        bubbleSort(arr, len);
        System.out.print("排序后:");
        print(arr, len);
        System.out.println("数组元素的最大值为" + arr[0]);
    }
}
