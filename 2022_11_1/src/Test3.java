/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-11-01
 * Time: 20:10
 */

import java.util.*;

public class Test3 {
    public  static boolean isUp(int[] arr){
        int[] ret =Arrays.copyOf(arr,arr.length);
        Arrays.sort(ret);
        if(ret.equals(arr)){
            return true;
        }else{
            return false;
        }
    }
    public static void main(String[] args) {
        int[] arr={1,2,3,10,5,6};
        boolean n= isUp(arr);
        if(n){
            System.out.println("该数组有序");
        }else{
            System.out.println("该数组无序");
        }
    }
}
