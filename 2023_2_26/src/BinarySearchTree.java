/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-02-26
 * Time: 2:56
 */
public class BinarySearchTree {
    static class TreeNode{
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val) {
            this.val =val;
        }
    }
    public TreeNode root = null;
    //查找二叉搜索树中指定的val值
    public TreeNode find(int val) {
        TreeNode cur =root;
        while(cur != null) {
            if(cur.val < val) {
                cur = cur.right;
            }else if(cur.val == val) {
                return cur;
            }else {
                cur = cur.right;
            }
        }
        return null;
    }
    //插入一个数据
    public void insert(int val) {
        if(root == null) {
            root = new TreeNode(val);
            return;
        }
        TreeNode cur =root;
        TreeNode parent =null;
        while (cur != null) {
            if(cur.val < val) {
                parent= cur;
                cur = cur.right;
            }else if(cur.val == val) {
                return;
            }else {
                parent = cur;
                cur = cur.left;
            }
        }
        TreeNode node =new TreeNode(val);
        if(parent.val < val) {
            parent.right = node;
        }else {
            parent.left = node;
        }
    }
    //删除值为val的节点
    public void remove(int val) {
        TreeNode cur = root;
        TreeNode parent = null;
        while (cur != null) {
            if(cur.val == val) {
                removeNode(parent,cur);
                return;
            }else if(cur.val < val) {
                parent = cur;
                cur = cur.right;
            }else {
                parent = cur;
                cur = cur.left;
            }
        }
    }

    private void removeNode(TreeNode parent, TreeNode cur) {
        if(cur.left == null) {
            if(cur == root) {
                root = cur.right;
            }else if(parent.left == cur) {
                parent.left = cur.right;
            }else {
                parent.right =cur.right;
            }
        }else if(cur.right == null) {
            if(cur == root) {
                root = cur.left;
            }else if(parent.left == cur) {
                parent.left = cur.left;
            }else {
                parent.right = cur.left;
            }
        }else {
            TreeNode target = cur.right;
            TreeNode targetP= cur;
            while (target.left != null) {
                targetP = target;
                target = target.left;
            }
            cur.val = target.val;
            if(target == targetP.left) {
                targetP.left = target.right;
            }else {
                targetP.right = target.right;
            }
        }
    }
    public void inOrder(TreeNode root) {
        if(root == null) {
            return;
        }
        inOrder(root.left);
        System.out.print(root.val+" ");
        inOrder(root.right);
    }

    public static void main(String[] args) {
        BinarySearchTree binarySearchTree =new BinarySearchTree();
        binarySearchTree.insert(7);
        binarySearchTree.insert(2);
        binarySearchTree.insert(4);
        binarySearchTree.insert(6);
        binarySearchTree.insert(3);
        binarySearchTree.insert(1);
        binarySearchTree.insert(5);
        binarySearchTree.inOrder(binarySearchTree.root);
        System.out.println();
        binarySearchTree.remove(5);
        binarySearchTree.inOrder(binarySearchTree.root);
    }
}
