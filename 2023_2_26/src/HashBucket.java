public class HashBucket {
    private static class Node {
        private int key;
        private int value;
        Node next;
 
 
        public Node(int key, int value) {
            this.key = key;
            this.value = value;
        }
    }
 
 
    private Node[]  array;
    private int size;   // 当前的数据个数
    private static final double LOAD_FACTOR = 0.75;
    private static final int DEFAULT_SIZE = 8;//默认桶的大小
 
    public void put(int key, int value) {
        int index = key%array.length;
        Node cur =array[index];
        while (cur != null) {
            if(cur.key == key) {
                cur.value =value;
                return;
            }
            cur = cur.next;
        }
        //采用头插法进行插入
        Node node =new Node(key,value);
        node.next = array[index];
        array[index] = node;
        size++;
        if(loadFactor() >= LOAD_FACTOR) {
            resize();
        }
    }
 
 
    private void resize() {
        Node[] newArray =new Node[array.length*2];
        for(int i =0; i < array.length; i++) {
            Node cur =array[i];
            while (cur != null) {
                Node curNext = cur.next;
                int index =cur.key % newArray.length;
                cur.next =newArray[index];
                newArray[index] = cur;
                cur =curNext;
            }
        }
        array =newArray;
    }
 
 
    private double loadFactor() {
        return size * 1.0 / array.length;
    }
 
 
    public HashBucket() {
        array =new Node[DEFAULT_SIZE];
    }
 
 
    public int get(int key) {
        int index = key % array.length;
        Node cur = array[index];
        while (cur != null) {
            if(cur.key == key) {
                return cur.value;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        HashBucket hashBucket =new HashBucket();
        hashBucket.put(1,12);
        hashBucket.put(2,22);
        hashBucket.put(3,33);
        hashBucket.put(4,44);
        hashBucket.put(5,55);
        hashBucket.put(6,66);
        hashBucket.put(7,77);
        hashBucket.put(8,88);
        Integer val = hashBucket.get(5);
        System.out.println(val);
    }
}/