package tset1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-01-01
 * Time: 0:53
 */
public class Game {
    private static final String[] suits ={"♥","♣","♦","♠"};
    public List<Poker> buyPoker(){
        List<Poker> pokers =new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            for (int j = 1; j <= 13; j++) {
                Poker poker =new Poker(suits[i],j);
                pokers.add(poker);
            }
        }
        return pokers;
    }
    public void shuffle(List<Poker> pokers){
        for (int i = pokers.size()-1; i >0 ; i--) {
            Random random =new Random();
            int index =random.nextInt(i);
            swap(pokers,i,index);
        }
    }
    public void swap(List<Poker> pokers,int i,int j){
        Poker tmp =pokers.get(i);
        pokers.set(i,pokers.get(j));
        pokers.set(j,tmp);
    }
    public List<List<Poker>> game(List<Poker> pokers){
        List<List<Poker>> hand =new ArrayList<>();
        List<Poker> hand1 =new ArrayList<>();
        List<Poker> hand2 =new ArrayList<>();
        List<Poker> hand3 =new ArrayList<>();
        hand.add(hand1);
        hand.add(hand2);
        hand.add(hand3);

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 3; j++) {
                Poker removePoker = pokers.remove(0);
                hand.get(j).add(removePoker);
            }
        }
        return hand;
    }
}
