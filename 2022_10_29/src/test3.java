/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: W
 * Date: 2022-10-29
 * Time: 18:03
 */

import java.util.*;

public class test3 {
    public static void print(int[] arr,int len){
        for(int i=0;i<len;i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println();
    }
    public static void bubbleSort(int[] arr,int len){
        int i,j;
        for(i=0;i<len-1;i++){
            for(j=0;j<len-1-i;j++){
                if(arr[j]>arr[j+1]){
                    int tmp =arr[j];
                    arr[j]=arr[j+1];
                    arr[j+1]=tmp;
                }
            }
        }
    }
    public static void main(String[] args) {
        int[] arr = {9, 8, 7, 6, 5, 4, 3, 2, 1};
        int len = arr.length;
        System.out.print("排序前:");
        print(arr, len);
        bubbleSort(arr, len);
        System.out.print("排序后:");
        print(arr, len);
    }
}
