package test;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-01-12
 * Time: 8:26
 */
public class Test1 {
    public static int func(int[] nums,int val){
        int n =nums.length;
        int left =0;
        for (int i = 0; i < n; i++) {
            if(nums[i] != val){
                nums[left] =nums[i];
                left++;
            }
        }
        return left;
    }
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int[] nums ={0,1,2,2,3,0,4,2};
        int val = scanner.nextInt();
        int ret =func(nums,val);
        System.out.println(ret);
//        for (int x,nums) {
//            System.out.print(x+" ");
//        }
        for (int i = 0; i < nums.length; i++) {
            System.out.print(nums[i]+" ");
        }
    }
}
