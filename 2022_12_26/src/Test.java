/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-26
 * Time: 3:31
 */
public class Test {
    public static int func(int[] nums){
        int ret=nums[0];
        for (int i = 1; i < nums.length; i++) {
             ret ^=nums[i];
        }
        return ret;
    }
    public static void main(String[] args) {
        int[] array ={2,2,3,1,5,5,1};
        System.out.println(func(array));
    }
}
