/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-26
 * Time: 19:14
 */
public interface Work {
    public abstract void work();
}
