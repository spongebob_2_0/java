package test;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-26
 * Time: 19:31
 */
public class Test {
    public static boolean func(int[][] array,int row,int col,int num){
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if(array[i][j] ==num){
                    return true;
                }
            }
        }
        return false;
    }
    public static void main(String[] args) {
        int[][] array ={{1,2,3,4},{5,6,7,8}};
        int row =array.length;
        int col =array[0].length;
        Scanner scanner =new Scanner(System.in);
        while (scanner.hasNext()){
            int num = scanner.nextInt();
            System.out.println(func(array, row, col, num));
        }
    }
}
