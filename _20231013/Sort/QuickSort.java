package Sort;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-13
 * Time: 18:32
 */
public class QuickSort {
    public static void main(String[] args) {
        int[] array = {9,8,7,6,5,4,3,2,1};
        //        int[] array = {1,2,3,4,5,6,7,8,9,};
        quickSort(array);
        System.out.println(Arrays.toString(array));
    }

    // 快速排序
    private static void quickSort(int[] array) {
        quick(array,0,array.length-1);
    }

    private static void quick(int[] array, int start, int end) {
        if(start >= end) {
            return;
        }
        int pivot = partition3(array,start,end);
        quick(array,0,pivot-1);
        quick(array,pivot+1,end);
    }

    //hoare法
    private static int partition(int[] array, int left, int right) {
        int tmp = array[left];
        int i = left;
        while (left < right) {
            while (left < right && array[right] >= tmp) {
                right--;
            }
            while (left < right && array[left] <= tmp) {
                left++;
            }
            swap(array,left,i);
        }
        return left;
    }

    //挖坑法
    private static int partition2(int[] array, int left, int right) {
        int tmp = array[left];
        while (left < right) { // 一趟排序
            while (left < right && array[right] >= tmp) {
                right--;
            }
            array[left] = array[right];
            while (left < right && array[left] <= tmp) {
                left++;
            }
            array[right] = array[left];
        }
        array[left] = tmp;
        return left;
    }

    //前后指针法 【了解即可】
    private static int partition3(int[] array,int left,int right) {
        int prev = left ;
        int cur = left+1;
        while (cur <= right) {
            if(array[cur] < array[left] && array[++prev] != array[cur]) {
                swap(array,cur,prev);
            }
            cur++;
        }
        swap(array,prev,left);
        return prev;
    }

    private static void swap(int[] array, int left, int right) {
        int tmp = array[left];
        array[left] = array[right];
        array[right]  =tmp;
    }
}
