package Sort;


import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-13
 * Time: 15:33
 */
public class HeapSort {
    public static void main(String[] args) {
        int[] array = {9,8,7,6,5,4,3,2,1};
        //        int[] array = {1,2,3,4,5,6,7,8,9,};
        heapSort(array);
        System.out.println(Arrays.toString(array));
    }

    private static void heapSort(int[] array) {
        // 建立大根堆
        creapBigHeap(array);
        int end = array.length-1;
        while (end > 0) {
            swap(array,0,end);
            shiftDown(array,0,end);
            end--;
        }
    }

    // 创建大根堆
    private static void creapBigHeap(int[] array) {
        // (array.length - 1) 是数组最后一个元素的索引，因为数组索引从 0 开始
        // (array.length - 1 - 1) / 2 计算得到的是最后一个非叶子节点的索引
        // 在堆中，最后一个非叶子节点的索引可以通过这个公式得到，它是父节点的索引
        for(int parent = (array.length -1 -1) /2; parent >= 0; parent--) {
            // 对每个父节点调用 shiftDown 方法，以确保以该父节点为根的子树满足大根堆的性质
            shiftDown(array,parent,array.length);
        }
    }

    // 向下调整
    private static void shiftDown(int[] array, int parent, int len) {
        // child是左孩子节点
        int child = 2 * parent +1;
        while (child < len) {
            // child + 1 < len 是为了保证右孩子存在再去比较
            if(child + 1 < len && array[child] < array[child+1]) {
                child++;
            }
            // 如果孩子节点比父亲节点大就交换
            if(array[child] > array[parent]){
                swap(array,child,parent);
                //继续判断它子树是否调整
                parent =child;
                child = 2 * parent + 1;
            }else {
                break;
            }
        }
    }

    private static void swap(int[] array, int child, int parent) {
        int tmp = array[child];
        array[child] = array[parent];
        array[parent] = tmp;
    }
}
