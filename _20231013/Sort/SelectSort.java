package Sort;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-13
 * Time: 13:30
 */
public class SelectSort {
    public static void main(String[] args) {
        int[] array = {9,8,7,6,5,4,3,2,1};
        //        int[] array = {1,2,3,4,5,6,7,8,9,};
        selectSort2(array);
        System.out.println(Arrays.toString(array));
    }

    // 版本1,每次找出最小值
    private static void selectSort(int[] array) {
        if(array == null || array.length < 2) {
            return;
        }
        for (int i = 0; i < array.length - 1; i++) {
            int midIndex = i;
            for(int j = i+1; j < array.length; j++) {
                if(array[j] < array[midIndex]) {
                    midIndex = j;
                }
            }
            swap(array,i,midIndex);
        }
    }

    //版本2,每次同时找出最大值和最小值
    public  static void selectSort2(int[] array) {
        int left = 0;
        int right = array.length -1;
        while (left < right) {
            int maxIndex = left;
            int midIndex = left;
            for (int i = left+1; i <= right ; i++) {
                if(array[i] < array[midIndex]) {
                    midIndex = i;   // 找出最小值的索引
                }
                if(array[i] > array[maxIndex]) {
                    maxIndex = i;  // 找出最大值的索引
                }
            }
            swap(array,left,midIndex);  // 将最小值放到left位置上
            // 如果 maxIndex 正好等于 left，意味着最大值就是当前未排序部分的第一个元素。
            // 但是前面我们把left位置的值交互到minIndex位置上面去了
            // 所以我们要更新maxIndex的索引为minIndex的索引
            if(maxIndex == left) {
                maxIndex = midIndex;
            }
            swap(array,right,maxIndex);
            left++;
            right--;
        }
    }

    private static void swap(int[] array, int i, int midIndex) {
        int tmp = array[midIndex];
        array[midIndex] = array[i];
        array[i] = tmp;
    }
}
