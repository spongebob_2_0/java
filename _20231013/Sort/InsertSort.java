package Sort;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-13
 * Time: 2:09
 */
public class InsertSort {
    public static void main(String[] args) {
        int[] array = {9,8,7,6,5,4,3,2,1};
//        int[] array = {1,2,3,4,5,6,7,8,9,};
        bubbleSort(array);
        System.out.println(Arrays.toString(array));
    }

    private static void bubbleSort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            // 从数组第二个元素开始遍历
            int tmp = array[i];  // 记录当前需要插入的元素
            int j;
            // 从当前元素的前一个元素开始向前遍历，找到合适的位置插入当前元素
            for(j = i -1; j >=0; j--) {
                if(array[j] > tmp) {
                    array[j+1] = array[j];  // 元素后移
                }else {
                    break;  // 找到合适的位置，退出循环
                }
            }
            //因为j是最后一次比较时的索引,退出时的j位置的元素是小于tmp的,索引要插入的元素tmp应该放到j+1位置
            array[j+1] = tmp;  // 插入当前元素到合适位置
        }
    }
}
