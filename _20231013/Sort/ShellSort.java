package Sort;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-13
 * Time: 2:48
 */
public class ShellSort {
    public static void main(String[] args) {
        int[] array = {9,8,7,6,5,4,3,2,1};
        //        int[] array = {1,2,3,4,5,6,7,8,9,};
        shellSort(array);
        System.out.println(Arrays.toString(array));
    }

    private static void shellSort(int[] array) {
        // 初始gap(组)分为数组长度的一把
        int gap = (array.length) / 2;
        // 当gap(组) 为 1 的时候 直接进行 直接插入排序
        while (gap > 1) {
            shell(array,gap);
            gap /= 2;
        }
        // 最后整体进行直接插入排序 (就是gap为1组的时候)
        shell(array,1);
    }

    private static void shell(int[] array, int gap) {
        // 从数组第二个元素开始遍历,而每一组数据元素之间的间隔大小是gap.所以从gap开始
        for (int i = gap; i < array.length; i++) {
            int tmp = array[i];
            int j;
            // 从数组中当前元素的前一个元素开始向前遍历，找到合适的位置插入当前元素
            for (j = i - gap; j >= 0; j -= gap) {
                if (array[j] > tmp) {
                    //比插入的数大就向后移动gap,因为每一组数据元素之间的间隔大小是gap,
                    array[j + gap] = array[j];
                } else {
                    //比插入的数小，跳出循环
                    break;
                }
            }
            // 因为j是最后一次比较时的索引,退出时的j位置的元素是小于tmp的,索引要插入的元素tmp应该放到j+gap位置
            // 因为每一组数据元素之间的间隔大小是gap,
            array[j + gap] = tmp;
        }
    }
}
