package Sort;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-13
 * Time: 1:52
 */
public class BubbleSort {
    public static void main(String[] args) {
//        int[] array = {9,8,7,6,5,4,3,2,1};
        int[] array = {1,2,3,4,5,6,7,8,9,};
        bubbleSort(array);
        System.out.println(Arrays.toString(array));

    }

    //时间复杂度分析：O(n^2) 最好情况下O(1)
    private static void bubbleSort(int[] array) {
        for (int i = 0; i < array.length-1; i++) {
            boolean flag = false;
            for (int j = 0; j < array.length - 1 - i; j++) {
                if(array[j] > array[j+1]) {
                    swap(array,j,j+1);
                    flag = true;
                }
            }
            // 如果有一轮排序走完flag 没有被置为true,说明每个这个数组是有序的
            // 因为每次排序都是相邻的两个元素进行比较,依次向后递推
            if(flag == false) {
                break;
            }
        }
    }

    private static void swap(int[] array, int j, int i) {
        int tmp = array[j];
        array[j] = array[j+1];
        array[j+1] = tmp;
    }
}
