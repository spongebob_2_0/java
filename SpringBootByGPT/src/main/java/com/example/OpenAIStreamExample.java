package com.example;

import okhttp3.*;
import okio.BufferedSource;
import okio.ByteString;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class OpenAIStreamExample {

    private static final String apiUrl = "https://api.openai.com/v1/chat/completions";
    private static final String apiKey = "sk-your-api-key"; // 请替换为你的API密钥

    public static void main(String[] args) {
        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(0, TimeUnit.MILLISECONDS)
                .build();

        Request request = new Request.Builder()
                .url(apiUrl)
                .addHeader("Authorization", "Bearer " + apiKey)
                .post(RequestBody.create(MediaType.get("application/json"), "{\"model\": \"gpt-4\",\"messages\": [{\"role\": \"user\", \"content\": \"Say this is a test\"}], \"stream\": true}"))
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try (ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                    // Here we process the stream of JSON objects
                    // This is a simple example, you'll need to parse the JSON and handle the stream accordingly
                    BufferedSource source = responseBody.source();
                    while (!source.exhausted()) {
                        String line = source.readUtf8LineStrict();
                        // Process the line, it will contain a JSON object per event
                        System.out.println(line);
                    }
                }
            }
        });

        // Keep the application running to listen to the stream
        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
