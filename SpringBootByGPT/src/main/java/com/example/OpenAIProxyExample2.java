package com.example;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

public class OpenAIProxyExample2 {

    private static final String API_URL = "https://api.openai.com/v1/chat/completions";
    private static final String API_KEY = "sk-yourapikey"; // Replace with your API key

    public static void main(String[] args) {
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             Scanner scanner = new Scanner(System.in)) {

            // Select the model
            String model = selectModel(scanner);

            // Start the chat session
            JSONArray messages = new JSONArray();
            while (true) {
                System.out.print("Your message: ");
                String userInput = scanner.nextLine();

                // Add user message to the conversation history
                messages.put(new JSONObject().put("role", "user").put("content", userInput));

                // Send the streaming request
                HttpPost request = new HttpPost(API_URL);
                request.setHeader("Authorization", "Bearer " + API_KEY);
                request.setHeader("Content-Type", "application/json");
                JSONObject body = new JSONObject();
                body.put("model", model);
                body.put("messages", messages);
                body.put("stream", true);
                request.setEntity(new StringEntity(body.toString()));

                HttpResponse response = httpClient.execute(request);
                HttpEntity entity = response.getEntity();

                if (entity != null) {
                    try (InputStream inputStream = entity.getContent();
                         BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
                        String line;
                        while ((line = reader.readLine()) != null) {
                            // Process and print the stream content here
                            // Assuming the stream is line-delimited JSON objects
                            JSONObject jsonResponse = new JSONObject(line);
                            JSONArray choices = jsonResponse.getJSONArray("choices");
                            if (choices.length() > 0) {
                                JSONObject firstChoice = choices.getJSONObject(0);
                                if (firstChoice.has("message")) {
                                    JSONObject message = firstChoice.getJSONObject("message");
                                    String content = message.getString("content");
                                    if (content != null) {
                                        System.out.println("AI: " + content);
                                        // Add AI response to the conversation history
                                        messages.put(message);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String selectModel(Scanner scanner) {
        String[] models = {"gpt-3.5-turbo", "gpt-3.5-turbo-16k", "gpt-4-1106-preview"};
        System.out.println("Select the model to use:");
        for (int i = 0; i < models.length; i++) {
            System.out.println((i + 1) + ". " + models[i]);
        }
        System.out.print("Enter the number of the model: ");
        int modelIndex = scanner.nextInt();
        scanner.nextLine(); // Consume the newline
        return models[modelIndex - 1];
    }
}
