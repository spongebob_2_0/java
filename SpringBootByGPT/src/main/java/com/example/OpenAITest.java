package com.example;


import com.unfbx.chatgpt.OpenAiClient;
import com.unfbx.chatgpt.OpenAiStreamClient;
import com.unfbx.chatgpt.entity.chat.BaseChatCompletion;
import com.unfbx.chatgpt.entity.chat.ChatCompletion;
import com.unfbx.chatgpt.entity.chat.ChatCompletionResponse;
import com.unfbx.chatgpt.entity.chat.Message;
import com.unfbx.chatgpt.sse.ConsoleEventSourceListener;
import okhttp3.OkHttpClient;

import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-12-14
 * Time: 0:41
 */
public class OpenAITest {
    public static void main1(String[] args) {
        //创建流式输出客户端
        OpenAiStreamClient client = OpenAiStreamClient.builder()
                .apiHost("https://api.openai-proxy.org/")
                .apiKey(Arrays.asList("sk-m69JrAfGZ5RxsQr4ML6k96quBjJtYTxdU8XzCTQ8oj3hAouY", "sk-********"))
                .build();
        //聊天
        ConsoleEventSourceListener eventSourceListener = new ConsoleEventSourceListener();
        Message message = Message.builder().role(Message.Role.USER).content("你好！").build();
        ChatCompletion chatCompletion = ChatCompletion.builder().messages(Arrays.asList(message)).build();
        client.streamChatCompletion(chatCompletion, eventSourceListener);
        CountDownLatch countDownLatch = new CountDownLatch(1);
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        OkHttpClient okHttpClient = new OkHttpClient
                .Builder()
                //自定义代理
                .connectTimeout(30, TimeUnit.SECONDS)//自定义超时时间
                .writeTimeout(30, TimeUnit.SECONDS)//自定义超时时间
                .readTimeout(30, TimeUnit.SECONDS)//自定义超时时间
                .build();
        OpenAiClient openAiClient = OpenAiClient.builder()
                .okHttpClient(okHttpClient)
                .apiHost("https://api.openai-proxy.org/")
                .apiKey(Arrays.asList("sk-m69JrAfGZ5RxsQr4ML6k96quBjJtYTxdU8XzCTQ8oj3hAouY"))
                .build();

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.print("你的问题：");
            String text = scanner.nextLine();
            //聊天模型：gpt-3.5
            Message message = Message.builder().role(Message.Role.USER).content(text).build();

            ChatCompletion chatCompletion = ChatCompletion.builder()
                    .model(BaseChatCompletion.Model.GPT_3_5_TURBO_16K.getName())
                    .messages(Arrays.asList(message))
                    .build();
            ChatCompletionResponse chatCompletionResponse = openAiClient.chatCompletion(chatCompletion);
            chatCompletionResponse.getChoices().forEach(e -> {
                System.out.println(e.getMessage());
            });
        }
    }

}
