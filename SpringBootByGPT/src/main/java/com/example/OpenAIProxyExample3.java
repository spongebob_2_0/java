package com.example;

import java.io.IOException;
import java.util.Scanner;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OpenAIProxyExample3 {

    public static void main(String[] args) throws IOException {
        // 设置API端点和API密钥
        String apiUrl = "https://api.openai-proxy.org/v1/chat/completions";
        String apiKey = "sk-m69JrAfGZ5RxsQr4ML6k96quBjJtYTxdU8XzCTQ8oj3hAouY"; // 请替换为你的API密钥

        // 创建HttpClient实例
        CloseableHttpClient client = HttpClients.createDefault();

        Scanner scanner = new Scanner(System.in);

        // 定义模型列表
        String[] models = {"gpt-3.5-turbo", "gpt-3.5-turbo-16k", "gpt-4-1106-preview"};

        // 选择要使用的模型
        System.out.println("请选择要使用的模型：");
        for (int i = 0; i < models.length; i++) {
            System.out.println((i + 1) + ". " + models[i]);
        }
        System.out.print("请输入选择的模型编号：");
        int modelIndex = scanner.nextInt();
        scanner.nextLine(); // 读取换行符

        // 获取选择的模型名称
        String model = models[modelIndex - 1];

        while (true) {
            System.out.print("你的问题：");
            String text = scanner.nextLine();

            // 创建请求体
            String requestBody = "{\"messages\": [{\"role\": \"user\",\"content\": \"" + text + "\"}], \"model\": \"" + model + "\"}";

            // 创建POST请求
            HttpPost post = new HttpPost(apiUrl);
            post.setHeader("Content-Type", "application/json");
            post.setHeader("Authorization", "Bearer " + apiKey);
            post.setEntity(new StringEntity(requestBody, "UTF-8"));

            // 发送请求并获取响应
            HttpResponse response = client.execute(post);
            String responseBody = EntityUtils.toString(response.getEntity());

//            System.out.println("完整的响应内容：");
//            System.out.println(responseBody);

            // 解析回答的正文并显示在控制台上
            String answer = parseAnswer(responseBody);
            System.out.println("回答：");
            System.out.println(answer);
            System.out.println();
        }
    }

    // 解析回答的正文
    private static String parseAnswer(String responseBody) {
        //回答的正文JSON中的"choices"字段的第一个元素的"message"字段中
        String answer = "";
        try {
            JSONObject json = new JSONObject(responseBody);
            JSONArray choices = json.getJSONArray("choices");
            if (choices.length() > 0) {
                JSONObject firstChoice = choices.getJSONObject(0);
                JSONObject message = firstChoice.getJSONObject("message");
                answer = message.getString("content");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return answer;
    }
}
