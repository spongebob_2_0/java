package test1;

import java.util.*;

public class Main {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str =scanner.next();
        Set<String> set =new HashSet<>();
        for(int i = 0; i < str.length(); i++) {
            char ch =str.charAt(i);
            if(!set.contains(ch+"")) {
                set.add(ch+"");
            }else {
                set.remove(ch+"");
            }
        }
        for(int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if(set.contains(ch+"")) {
                System.out.println(ch+"");
                return;
            }
        }
        System.out.println(-1);
    }
}