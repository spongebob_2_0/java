package test;

class Solution1 {
    public boolean isPalindrome(int x) {
        String str =String.valueOf(x);
        int left =0;
        int right =str.length()-1;
        while(left < right){
            String s1 =str.charAt(left)+"";
            String s2 =str.charAt(right)+""; 
            if(s1.equals(s2)){
                left++;
                right--;
            }else{
                return false;
            }
        }
        return true;
    }
}