package test;

class Solution {
    public static int searchInsert(int[] nums, int target) {
        int left = 0;
        int right =nums.length-1;
        while(left <= right){
            int mid =(left+right)/2;
            if(nums[mid] < target){
                left =mid+1;
            }else if(nums[mid] >target){
                right =mid-1;
            }else{
                return mid;
            }
        }
        for(int i =0;i<nums.length-1;i++){
            if(target>nums[i] && target<nums[i+1]){
                return i+1;
            }
        }
        if(target > nums[nums.length-1]){
            return nums.length;
        }
        return 0;
    }

    public static void main(String[] args) {
        int[] array ={1,3};
        System.out.println(searchInsert(array, 3));
    }
}