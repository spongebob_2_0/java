package test;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

class Solution4 {
    public int[] topKFrequent(int[] nums, int k) {
        Map<Integer,Integer> map =new HashMap<>();
        for(int x: nums) {
            map.put(x,map.getOrDefault(x,0)+1);
        }
        PriorityQueue<int[]> minHeap = new PriorityQueue<>(new Comparator<int[]>() {
            public int compare(int[] nums1, int[] nums2) {
                return nums1[1] - nums2[1];
            }
        });
        for(Map.Entry<Integer,Integer> entrySet:map.entrySet()) {
            if(minHeap.size() < k) {
                minHeap.add(new int[]{entrySet.getKey(),entrySet.getValue()});
            }else {
                if(entrySet.getValue() > minHeap.peek()[1]) {
                    minHeap.poll();
                    minHeap.add(new int[]{entrySet.getKey(),entrySet.getValue()});
                }
            }
        }
        int[] res =new int[k];
        for(int i = k-1; i >= 0; i--) {
            res[i] = minHeap.poll()[0];
        }
        return res;
    }
}