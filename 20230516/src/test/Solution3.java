package test;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

class Solution3 {
    public int[] topKFrequent(int[] nums, int k) {
        Map<Integer,Integer> map = new HashMap<>();
        for(int i = 0; i < nums.length; i++) {
            map.put(nums[i],map.getOrDefault(nums[i],0)+1);
        }
        PriorityQueue<int[]> maxHeap = new PriorityQueue<>(new Comparator<int[]>() {
            public int compare(int[] nums1, int[] nums2) {
                return nums2[1] - nums1[1];
            }
        });
        for(Map.Entry<Integer,Integer> entry:map.entrySet()) {
            maxHeap.add(new int[]{entry.getKey(),entry.getValue()});
        }
        int[] array = new int[k];
        for(int i = 0; i < k ; i++) {
            //大根堆里每个对象是一个数组,每次弹出数组对象并将该数组0下标的值记录在另一个数组
            array[i] = maxHeap.poll()[0];
        }
        return array;
    }
}