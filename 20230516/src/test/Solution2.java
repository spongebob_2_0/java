package test;

import java.util.Comparator;
import java.util.PriorityQueue;

class Solution2 {
    public int[] topKFrequent(int[] nums, int k) {
        PriorityQueue<Integer> maxHeap =
                new PriorityQueue<>(new Comparator<Integer>() {
            public int compare(Integer o1, Integer o2) {
                return o2 - o1;
            }
        });
        for(int i = 0; i < k; i++) {
            maxHeap.offer(nums[i]);
        }
        for(int i = k; i < nums.length; i++) {
            if(nums[i] < maxHeap.peek()) {
                maxHeap.poll();
                maxHeap.offer(nums[i]);
            }
        }
        int[] array =new int[maxHeap.size()];
        int i = 0;
        for(int x:maxHeap) {
            array[i++] = x;
        }
        return array;
    }
}