package test;

import java.util.ArrayDeque;
import java.util.Deque;

class Solution {
    public int[] maxSlidingWindow(int[] nums, int k) {
        if(nums.length == 1) {
            return nums;
        }
        int len =nums.length-k+1;
        int[] res =new int[len];
        int num =0;
        MyQueue myqueue = new MyQueue();
        for(int i = 0; i < k; i++) {
            myqueue.add(nums[i]);
        }
        res[num++] = myqueue.getMax();
        for(int i = k; i < nums.length; i++) {
            myqueue.pop(nums[i-k]);
            myqueue.add(nums[i]);
            res[num++] = myqueue.getMax();
        }
        return nums;
    }
}

class MyQueue {
    Deque<Integer> queue = new ArrayDeque<>();
    public MyQueue () {

    }
    public void pop(int val) {
        while(!queue.isEmpty() && val == queue.peek()) {
            queue.poll();
        }
    }
    public void add(int val) {
        while(!queue.isEmpty() && val > queue.getLast()) {
            queue.removeLast();
        }
        queue.add(val);
    }
    public int getMax() {
        return queue.peek();
    }
}