package com.example.producer;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-08-25
 * Time: 2:16
 */
public class Producer_HelloWorld {
    public static void main(String[] args) throws IOException, TimeoutException {
        //1.创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        //2.设置参数
        factory.setHost("106.14.91.138"); //ip默认localhost
        factory.setPort(5672);  //端口 默认5672
        factory.setVirtualHost("/itbit"); //虚拟机 默认值
        factory.setUsername("wuyimin");
        factory.setPassword("183193");
        //3.创建连接Connection
        Connection connection = factory.newConnection();
        //4.创建channel
        Channel channel = connection.createChannel();
        //5.创建队列
        //如果没有一个名字叫hello_world的队列,则会创建队列,否则不会创建
        channel.queueDeclare("hello_world",true,false,false,null);
        //6.发送消息
        String body =  "hello rabbitmq....";
        channel.basicPublish("","hello_world",null,body.getBytes());
        //7.释放资源
        channel.close();
        connection.close();
    }
}
