package com.example.producer;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-08-25
 * Time: 2:16
 */
public class Producer_PubSub {
    public static void main(String[] args) throws IOException, TimeoutException {
        //1.创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        //2.设置参数
        factory.setHost("106.14.91.138"); //ip默认localhost
        factory.setPort(5672);  //端口 默认5672
        factory.setVirtualHost("/itbit"); //虚拟机 默认值
        factory.setUsername("wuyimin");
        factory.setPassword("183193");
        //3.创建连接Connection
        Connection connection = factory.newConnection();
        //4.创建channel
        Channel channel = connection.createChannel();
        //5.创建交换机
        String exchangeName = "test_fanout";
        channel.exchangeDeclare(exchangeName, BuiltinExchangeType.FANOUT,true,false,false,null);
        //6.创建队列
        String que1Name = "test_fanout_queue1";
        String que2Name = "test_fanout_queue2";
        channel.queueDeclare(que1Name,true,false,false,null);
        channel.queueDeclare(que2Name,true,false,false,null);
        //7.绑定队列和交换机
        channel.queueBind(que1Name,exchangeName,"");
        channel.queueBind(que2Name,exchangeName,"");
        String body = "日志信息:张三调用了findAll方法,...日志级别info...";
        //8.发送消息
        channel.basicPublish(exchangeName,"",null,body.getBytes());

        //9.释放资源
        channel.close();
        connection.close();
    }
}
