/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-01-25
 * Time: 19:23
 */
public class Test {
    public static void main(String[] args) {
        int[] array ={1,2,3,4};
        func(array,array.length);
    }

    private static void func(int[] array, int len) {
        if(len == 1){
            System.out.print(array[0]+" ");
        }else{
            System.out.print(array[len-1]+" ");
            func(array,len-1);
        }
    }
}
