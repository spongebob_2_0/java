import java.util.*;


public class Solution4 {
    /**
     * 
     * @param x int整型 
     * @return int整型
     */
    public static int reverse (int x) {
        // write code here
        String s =String.valueOf(x);
        char[] array =s.toCharArray(); 
        int left =0;
        int right =array.length-1;
        while(left < right){
            while(left < right && !func(array[left])){
                left++;
            }
            char tmp =array[left];
            array[left] =array[right];
            array[right] =tmp;
            left++;
            right--;
        }
        String s1 =new String(array);
        double num1 =Math.pow(-2,31);
        double num2 =Math.pow(2,31)-1;
        if(Long.valueOf(s1) <= num1 || Long.valueOf(s1) >= num2){
            return 0;
        }
        int ret =Integer.valueOf(s1);
        return ret;
    }
    public static boolean func(char ch){
        if(ch >= '0' && ch <= '9'){
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        int x =2123456789;
        System.out.println(reverse(x));
    }
}