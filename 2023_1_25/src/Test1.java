import java.util.Arrays;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-01-25
 * Time: 19:50
 */
public class Test1 {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String s = scanner.nextLine();
            char[] array = s.toCharArray();
            Arrays.sort(array);
            StringBuilder sb =new StringBuilder();
            for (int i = 0; i < array.length; i++) {
                sb.append(array[i]);
            }
            System.out.println(sb);
        }
    }
}
