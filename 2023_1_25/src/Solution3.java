
class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 }
class Solution3 {
    public ListNode mergeTwoLists(ListNode head1, ListNode head2) {
        ListNode newHead =new ListNode();
        ListNode tmp =newHead;
        while(head1 != null && head2 !=null){
            if(head1.val < head2.val){
                tmp.next =head1;
                head1 =head1.next;
                tmp =tmp.next;
            }else{
                tmp.next =head2;
                head2=head2.next;
                tmp =tmp.next;
            }
        }
        if(head1 != null){
            tmp.next =head1;
        }
        if(head2 != null){
            tmp.next =head2;
        }        
        return newHead.next;
    }
}