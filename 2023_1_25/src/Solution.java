class Solution {
    public String reverseOnlyLetters(String s) {
        char[] array =s.toCharArray();
        int left =0;
        int right =array.length-1;
        while(left<right){
             while (left<right) {
                if (!func(array[left])) {
                    left++;
                }
                if (!func(array[right])) {
                    right--;
                }
                if(func(array[left]) && func(array[right])){
                    break;
                }
            }
            if(left > right){
                break;
            }
            char tmp = array[left];
            array[left] = array[right];
            array[right] =tmp;
            left++;
            right--;
        }
        return new String(array);
    }

    public boolean func(char ch){
        if(ch >= '0' && ch <= '9'){
            return false;
        }
        if(ch >='a' && ch <='z' || ch >= 'A' && ch <= 'Z'){
            return true;
        }
        return false;
    }
}