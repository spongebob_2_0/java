package com.example.mapper;
import org.apache.ibatis.annotations.Param;

import com.example.entity.Admin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author WuYimin
* @description 针对表【admin(用户信息表)】的数据库操作Mapper
* @createDate 2023-11-12 20:38:09
* @Entity com.example.entity.Admin
*/
public interface AdminMapper extends BaseMapper<Admin> {

    Admin selectOneByName(@Param("name") String name);
}




