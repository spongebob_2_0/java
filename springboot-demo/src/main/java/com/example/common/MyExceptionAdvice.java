package com.example.common;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice //定义一个全局异常处理类,返回类型的格式为json格式
public class MyExceptionAdvice {

    @ExceptionHandler(Exception.class) //捕获所有异常
    public AjaxResult doException(Exception e) { //处理异常
        return AjaxResult.fail(-1,e.getMessage());
    }
}