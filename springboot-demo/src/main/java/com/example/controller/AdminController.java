package com.example.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.CircleCaptcha;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.common.AjaxResult;
import com.example.entity.Admin;
import com.example.entity.Params;
import com.example.service.impl.AdminServiceImpl;
import com.example.service.impl.EmailService;
import com.example.utils.JwtTokenUtils;
import com.example.utils.MailUtils;
import com.example.entity.VerifyCodeResp;
import com.google.code.kaptcha.Producer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * Date: 2023-11-12
 * Time: 20:54
 * @author WuYimin
 */
@Slf4j //类上添加@Slf4j注解后，在类中就可以直接使用log对象
@RestController
@RequestMapping("/admin")
public class AdminController {

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private MailUtils mailUtils;  // 邮箱工具类（用来发送邮件）

    @Resource
    private Producer producer; //生成邮箱验证码

    @Resource
    private EmailService emailService; //邮箱服务类（用来发送邮件）

    @Resource
    private AdminServiceImpl adminService;




    /**
     * 登录
     * @param admin
     * @return
     */
    @RequestMapping("/login")
    public AjaxResult login(@RequestBody Admin admin) {
        if(StrUtil.hasEmpty(admin.getName(),admin.getPassword(),admin.getPassword())) {
            return AjaxResult.fail(-1,"数据不能为空");
        }
        LambdaQueryWrapper<Admin> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Admin::getName,admin.getName());
        Admin userinfo = adminService.getOne(lambdaQueryWrapper);
        if(userinfo == null) {
            return AjaxResult.fail(-1,"该用户不存在！");
        }
        if(!userinfo.getName().equals(admin.getName())) {
            return AjaxResult.fail(-1,"用户名或者密码错误！");
        }
        if(!userinfo.getPassword().equals(admin.getPassword())) {
            return AjaxResult.fail(-1,"用户名或者密码错误！");
        }
        String captchaValue = (String) redisTemplate.opsForValue().get("captcha:"+admin.getCaptchaKey());
        if(StrUtil.isEmpty(captchaValue) || !captchaValue.equalsIgnoreCase(admin.getVercode())) {
            return AjaxResult.fail(-1,"验证码错误！");
        }
        // 验证码验证成功，删除redis里面保存的验证码
        redisTemplate.delete("captcha:"+admin.getCaptchaKey());
        // 生成jwt token给前端
        String token = JwtTokenUtils.genToken(userinfo.getId().toString(), userinfo.getPassword());
        userinfo.setToken(token);
        return AjaxResult.success(userinfo);
    }

    @RequestMapping("/register")
    public AjaxResult register(@RequestBody Admin admin) {
        if(StrUtil.hasBlank(admin.getEmail(),admin.getEmailCode(),admin.getName(),admin.getPassword(),
                admin.getCaptchaKey(), admin.getEmailKey(),admin.getVercode())) {
            return AjaxResult.fail(-1,"参数错误");
        }
        LambdaQueryWrapper<Admin>  lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Admin::getName,admin.getName());
        Admin user = adminService.getOne(lambdaQueryWrapper);
        if(user != null) {
            return AjaxResult.fail(-1,"该用户已存在！");
        }

        String emailValue = (String) redisTemplate.opsForValue().get("emailCode:" + admin.getEmailKey());
        if(StrUtil.isEmpty(emailValue) || !emailValue.equalsIgnoreCase(admin.getEmailCode())) {
            return AjaxResult.fail(-1,"邮箱验证码错误！");
        }

        String  captchaValue = (String) redisTemplate.opsForValue().get("captcha:"+admin.getCaptchaKey());
        if(StrUtil.isEmpty(captchaValue) || !captchaValue.equalsIgnoreCase(admin.getVercode())) {
            return AjaxResult.fail(-1,"验证码错误");
        }

        boolean save = adminService.save(admin);
        if(!save) {
            return AjaxResult.fail(-1,"注册失败");
        }
        Admin admin1 = adminService.getOne(lambdaQueryWrapper);
        return AjaxResult.success(admin1);
    }

    /**
     * 获取图片验证码和唯一key
     * @return
     * @throws IOException
     */
    @RequestMapping("/code")
    // 该方法名为getImageCode，返回类型为Result，可能会抛出IOException异常
    public AjaxResult getImageCode() throws IOException {
        // 使用hutool工具库，创建一个包含圆形干扰线的验证码，宽100像素，高40像素，4个字符，5条干扰线
        CircleCaptcha captcha = CaptchaUtil.createCircleCaptcha(100, 40, 4, 5);
        // 获取生成的验证码字符
        String code = captcha.getCode();
        // 生成一个临时的唯一标识符（例如，UUID）
        String captchaKey = UUID.randomUUID().toString();
        // 使用Redis的Template存储验证码，设置有效期为30分钟
        redisTemplate.opsForValue().set("captcha:" + captchaKey, code, 30L, TimeUnit.MINUTES);
        // 初始化一个空的base64字符串用于存放图片验证码
        String base64String = "";
        try {
            // 获取图片验证码的Base64编码，并添加到"data:image/png;base64,"前缀
            base64String = "data:image/png;base64," + captcha.getImageBase64();
        } catch (Exception e) {
            // 如果获取失败，打印堆栈信息
            e.printStackTrace();
        }
        // 创建VerifyCodeResp对象，用于封装图形验证码和唯一key
        VerifyCodeResp verifyCodeResp = new VerifyCodeResp();
        // 设置唯一key
        verifyCodeResp.setCaptchaKey(captchaKey);
        // 设置图形验证码图片
        verifyCodeResp.setCaptchaImg(base64String);
        // 返回封装好的VerifyCodeResp对象，状态为成功
        return AjaxResult.success(verifyCodeResp);
    }

    /**
     * 通过邮箱修改密码
     * @param admin
     * @return
     */
    @RequestMapping("/forgetPassword")
    public AjaxResult forgetPassword(@RequestBody Admin admin) {

        return AjaxResult.success("");
    }

    @RequestMapping("/search")
    public AjaxResult search(Params params) {
        Page<Admin> adminPage = adminService.selectPage(params);
        return AjaxResult.success(adminPage);
    }

    @RequestMapping("/add0edit")
    public AjaxResult add0edit(@RequestBody Admin admin) {
        if(StrUtil.hasBlank(admin.getName(),admin.getSex(),admin.getPhone(),admin.getRole()) || admin.getAge() == null) {
            return AjaxResult.fail(-1,"数据不能为空！");
        }
        if(admin.getId() == null) {
            LambdaQueryWrapper<Admin> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(Admin::getName,admin.getName());
            Admin user1 = adminService.getOne(lambdaQueryWrapper);
            if(user1 != null) {
                return AjaxResult.fail(-1,"该用户已存在");
            }
            //添加
            adminService.save(admin);
        }else {
            // 编辑
            adminService.updateById(admin);
        }
        return AjaxResult.success("");
    }

    @RequestMapping("/delete/{id}")
    public AjaxResult deleteUser(@PathVariable Integer id) {
        if(id == null) {
            return AjaxResult.fail(-1,"参数错误");
        }
        LambdaQueryWrapper<Admin> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Admin::getId,id);
        adminService.remove(lambdaQueryWrapper);
        return AjaxResult.success("");
    }


    /**
     * 发送邮箱验证码
     * @param
     * @return
     */
    @RequestMapping("/getcode")
    public AjaxResult contextLoad(@RequestBody Admin admin) {
        // 因为前端发的是json格式的对象，所以拿对象接收
        String postbox = admin.getEmail();
        try {
            // 使用正则表达式进行验证邮箱格式
            String emailRegex = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$";
            boolean isValidEmail = Pattern.matches(emailRegex, postbox);
            if (!isValidEmail) {
                return AjaxResult.fail(-1, "无效的邮箱地址");
            }
            String s = producer.createText();
//            mailUtils.sendMail(postbox,"验证码: " + s+",有效期三分钟","邮件注册验证");
            String html =  "<head>\n" +
                    "    <meta charset=\"utf-8\">\n" +
                    "    <base target=\"_blank\"/>\n" +
                    "    <style type=\"text/css\">::-webkit-scrollbar {\n" +
                    "        display: none;\n" +
                    "    }</style>\n" +
                    "    <style id=\"cloudAttachStyle\" type=\"text/css\">#divNeteaseBigAttach, #divNeteaseBigAttach_bak {\n" +
                    "        display: none;\n" +
                    "    }</style>\n" +
                    "    <style id=\"blockquoteStyle\" type=\"text/css\">blockquote {\n" +
                    "        display: none;\n" +
                    "    }</style>\n" +
                    "    <style type=\"text/css\">\n" +
                    "        body {\n" +
                    "            font-size: 14px;\n" +
                    "            font-family: arial, verdana, sans-serif;\n" +
                    "            line-height: 1.666;\n" +
                    "            padding: 0;\n" +
                    "            margin: 0;\n" +
                    "            overflow: auto;\n" +
                    "            white-space: normal;\n" +
                    "            word-wrap: break-word;\n" +
                    "            min-height: 100px\n" +
                    "        }\n" +
                    "\n" +
                    "        td, input, button, select, body {\n" +
                    "            font-family: Helvetica, 'Microsoft Yahei', verdana\n" +
                    "        }\n" +
                    "\n" +
                    "        pre {\n" +
                    "            white-space: pre-wrap;\n" +
                    "            white-space: -moz-pre-wrap;\n" +
                    "            white-space: -pre-wrap;\n" +
                    "            white-space: -o-pre-wrap;\n" +
                    "            word-wrap: break-word;\n" +
                    "            width: 95%\n" +
                    "        }\n" +
                    "\n" +
                    "        th, td {\n" +
                    "            font-family: arial, verdana, sans-serif;\n" +
                    "            line-height: 1.666\n" +
                    "        }\n" +
                    "\n" +
                    "        img {\n" +
                    "            border: 0\n" +
                    "        }\n" +
                    "\n" +
                    "        header, footer, section, aside, article, nav, hgroup, figure, figcaption {\n" +
                    "            display: block\n" +
                    "        }\n" +
                    "\n" +
                    "        blockquote {\n" +
                    "            margin-right: 0px\n" +
                    "        }\n" +
                    "    </style>\n" +
                    "</head>\n" +
                    "\n" +
                    "<body tabindex=\"0\" role=\"listitem\">\n" +
                    "<table width=\"700\" border=\"0\" align=\"center\" cellspacing=\"0\" style=\"width:700px;\">\n" +
                    "    <tbody>\n" +
                    "    <tr>\n" +
                    "        <td>\n" +
                    "            <div style=\"width:700px;margin:0 auto;border-bottom:1px solid #ccc;margin-bottom:30px;\">\n" +
                    "                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"700\" height=\"39\"\n" +
                    "                       style=\"font:12px Tahoma, Arial, 宋体;\">\n" +
                    "                    <tbody>\n" +
                    "                    <tr>\n" +
                    "                        <td width=\"210\"></td>\n" +
                    "                    </tr>\n" +
                    "                    </tbody>\n" +
                    "                </table>\n" +
                    "            </div>\n" +
                    "            <div style=\"width:680px;padding:0 10px;margin:0 auto;\">\n" +
                    "                <div style=\"line-height:1.5;font-size:14px;margin-bottom:25px;color:#4d4d4d;\">\n" +
                    "                    <strong style=\"display:block;margin-bottom:15px;\">尊敬的用户，您好：<span\n" +
                    "                            style=\"color:#f60;font-size: 16px;\"></span></strong>\n" +
                    "                    <strong style=\"display:block;margin-bottom:15px;\">\n" +
                    "                        您正在进行<span style=\"color: red\">注册账号</span>操作，请在验证码输入框中输入：<span\n" +
                    "                            style=\"color:#f60;font-size: 24px\">"+ s +"</span>，以完成操作。\n" +
                    "                    </strong>\n" +
                    "                </div>\n" +
                    "                <div style=\"margin-bottom:30px;\">\n" +
                    "                    <small style=\"display:block;margin-bottom:20px;font-size:12px;\">\n" +
                    "                        <p style=\"color:#747474;\">\n" +
                    "                            注意：验证码5分钟有效，此操作将会注册一个新账户。如非本人操作，请勿泄露验证码以保证帐户安全\n" +
                    "                            <br>（工作人员不会向你索取此验证码，请勿泄漏！)\n" +
                    "                        </p>\n" +
                    "                    </small>\n" +
                    "                </div>\n" +
                    "            </div>\n" +
                    "\n" +
                    "            <div style=\"width:700px;margin:0 auto;\">\n" +
                    "                <div style=\"padding:10px 10px 0;border-top:1px solid #ccc;color:#747474;margin-bottom:20px;line-height:1.3em;font-size:12px;\">\n" +
                    "                    <p>此为系统邮件，请勿回复<br>\n" +
                    "                        请保管好您的邮箱，避免账号被他人盗用\n" +
                    "                    </p>\n" +
                    "                    <p>书生团队</p>\n" +
                    "                </div>\n" +
                    "            </div>\n" +
                    "        </td>\n" +
                    "    </tr>\n" +
                    "    </tbody>\n" +
                    "</table>\n" +
                    "</body>";
            emailService.sendEmail(postbox,"邮件注册验证",html);
            // 将邮箱验证码存进redis里面，设置三分钟
            String emailKey = UUID.randomUUID().toString();
            redisTemplate.opsForValue().set("emailCode:"+emailKey , s, 300, TimeUnit.SECONDS);
            // 将uuid返回给前端，前端带着参数去redis里面去获取到验证码做一个邮箱验证码的校验
            return AjaxResult.success(emailKey);
        } catch (Exception e) {
            log.error("方法执行出错! ", e);
            return AjaxResult.fail(-1, "参数有误! ");
        }
    }


    
}
