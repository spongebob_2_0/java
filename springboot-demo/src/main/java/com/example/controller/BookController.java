package com.example.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.common.AjaxResult;
import com.example.entity.Book;
import com.example.entity.Params;
import com.example.service.impl.BookServiceImpl;
import com.example.utils.AliOSSUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * Date: 2023-11-12
 * Time: 20:54
 * @author WuYimin
 */
@Slf4j //类上添加@Slf4j注解后，在类中就可以直接使用log对象
@RestController
@RequestMapping("/book")
public class BookController {

    @Resource
    private RedisTemplate redisTemplate;


    @Resource
    private BookServiceImpl bookService;


    @Resource
    private AliOSSUtils aliOSSUtils;


    @RequestMapping("/search")
    public AjaxResult search(Params params) {
        Page<Book> adminPage = bookService.selectPage(params);
        return AjaxResult.success(adminPage);
    }

    @RequestMapping("/add0edit")
    public AjaxResult add0edit(@RequestBody Book book) {
        if(StrUtil.hasBlank(book.getName(),book.getPrice(),book.getAuthor(),book.getPress(),book.getImg())) {
            return AjaxResult.fail(-1,"数据不能为空！");
        }
        if(book.getId() == null) {
            LambdaQueryWrapper<Book> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(Book::getName,book.getName());
            Book book1 = bookService.getOne(lambdaQueryWrapper);
            if(book1 != null) {
                return AjaxResult.fail(-1,"该图书已存在");
            }
            //添加
            bookService.save(book);
        }else {
            // 编辑
            bookService.updateById(book);
        }
        return AjaxResult.success("");
    }

    @RequestMapping("/delete/{id}")
    public AjaxResult deleteUser(@PathVariable Integer id) {
        if(id == null) {
            return AjaxResult.fail(-1,"参数错误");
        }
        LambdaQueryWrapper<Book> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Book::getId,id);
        bookService.remove(lambdaQueryWrapper);
        return AjaxResult.success("");
    }

    @RequestMapping("/upload")
    public AjaxResult upload(MultipartFile file) throws IOException {
        if(file == null) {
            return AjaxResult.fail(-1,"参数有误");
        }
        String url = aliOSSUtils.upload(file);
        return AjaxResult.success(url);
    }

    // 后端的接口
    @RequestMapping("/delBatch")
    public AjaxResult delBatch(@RequestBody List<Book> list) {
        for(Book book: list) {
            bookService.removeById(book);
        }
        return AjaxResult.success("");
    }
}
