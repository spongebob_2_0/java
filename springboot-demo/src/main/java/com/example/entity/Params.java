package com.example.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-11-15
 * Time: 21:17
 */
@Data
public class Params {

    private Long pageNum;
    private Long pageSize;

    private String name;

    private String phone;
}
