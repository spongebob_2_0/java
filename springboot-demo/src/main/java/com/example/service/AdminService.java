package com.example.service;

import com.example.entity.Admin;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.mapper.AdminMapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
* @author WuYimin
* @description 针对表【admin(用户信息表)】的数据库操作Service
* @createDate 2023-11-12 20:38:09
*/
public interface AdminService extends IService<Admin> {

}
