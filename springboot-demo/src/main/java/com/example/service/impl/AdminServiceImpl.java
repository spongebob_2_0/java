package com.example.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.entity.Admin;
import com.example.entity.Params;
import com.example.service.AdminService;
import com.example.mapper.AdminMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author WuYimin
* @description 针对表【admin(用户信息表)】的数据库操作Service实现
* @createDate 2023-11-12 20:38:09
*/
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin>
    implements AdminService{

    @Resource
    private AdminMapper adminMapper;

    /**
     * 分页获取全部用户数据
     * @param params
     * @return
     */
    public Page<Admin> selectPage(Params params) {
        // 构建一个分页对象
        Page<Admin> page = new Page<>(params.getPageNum(),params.getPageSize());
        // 构建模糊查询的参数
        LambdaQueryWrapper<Admin> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.like(StrUtil.isNotBlank(params.getName()),Admin::getName,params.getName())
                          .like(StrUtil.isNotBlank(params.getPhone()),Admin::getPhone,params.getPhone());
        // 将分页查到的数据封装在Page对象里面
        Page<Admin> adminPage = adminMapper.selectPage(page, lambdaQueryWrapper);
        return adminPage;
    }
}




