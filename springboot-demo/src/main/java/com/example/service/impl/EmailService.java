package com.example.service.impl;

import com.example.mapper.AdminMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class EmailService {
    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private AdminMapper adminMapper;

    @Value("${spring.mail.username}")
    private String email;

    /**
     * 发送邮件
     *
     * @param to      收件人邮箱地址
     * @param subject 邮件主题
     * @param text    邮件正文
     * @throws MessagingException 邮件发送异常
     */
    public void sendEmail(String to, String subject, String text) throws MessagingException {
        // 创建 MimeMessage 对象
        MimeMessage message = javaMailSender.createMimeMessage();

        // 使用 MimeMessageHelper 构建邮件内容
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setFrom(email);     // 设置发件人
        helper.setTo(to);         // 设置收件人
        helper.setSubject(subject); // 设置邮件主题
        helper.setText(text, true); // 设置邮件正文，支持HTML格式

        // 发送邮件
        javaMailSender.send(message);
    }
}

