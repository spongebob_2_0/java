package com.example.rabbitmqdemo;

import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
public class SpringAmqpTest {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void testSimpleQueue() {
        // 路由规则
        String routingkey = "routingkey";


        for (int i = 0; i < 100; i++) {
            // 消息
            String message = "hello, spring amqp!";
            Map<String, Object> map = new HashMap<>();
            map.put("name","小诗诗"+ i+"号");
            map.put("age","18");
            map.put("list", Arrays.asList(1,2,3,4,5));
            // 发送消息
            System.out.println(map);
            rabbitTemplate.convertAndSend("direct.exchange",routingkey, map);
        }
    }

    @Test
    public void testReceiveDirect(){
        // 从指定队列中接收数据
        Object data = rabbitTemplate.receiveAndConvert("baobao");
        System.out.println(data.getClass());
        System.out.println(data);
    }
}