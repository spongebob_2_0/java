//package com.example.rabbitmqdemo.config;
//
//import org.springframework.amqp.core.*;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class MyRabbitConfig2 {
//    @Autowired
//    private RabbitTemplate rabbitTemplate;
//
//    @Bean
//    public Exchange exchange2(){
//        // 使用ExchangeBuilder创建一个交换机
//        return ExchangeBuilder
//                .directExchange("direct.exchange") // 指定创建一个"direct"类型的交换机并给它命名为"direct.exchange"
//                .durable(true) // 设置交换机为持久化，这意味着它在RabbitMQ重启后依然存在
//                .build(); // 完成交换机的创建
//    }
//    @Bean
//    public Queue queue2(){
//        // 使用QueueBuilder创建一个队列
//        return QueueBuilder
//                .durable("baobao") // 创建一个名为"queue"的持久化队列，持久化意味着队列在RabbitMQ重启后依然存在
//                .build(); // 完成队列的创建
//    }
//    @Bean
//    public Binding binding2(
//            @Qualifier("queue2") Queue queue, // 使用Qualifier注解来注入名为"queue2"的Bean，这里是上面创建的队列
//            @Qualifier("exchange2") Exchange exchange // 使用Qualifier注解来注入名为"exchange2"的Bean，这里是上面创建的交换机
//    ){
//        // 使用BindingBuilder来创建一个绑定关系
//        return BindingBuilder
//                .bind(queue) // 指定队列，这里的队列是方法参数传递进来的
//                .to(exchange) // 指定交换机，这里的交换机是方法参数传递进来的
//                .with("routingkey") // 指定路由键为"routingkey"
//                .noargs(); // 这表示没有额外的参数需要为这个绑定设置
//    }
//}