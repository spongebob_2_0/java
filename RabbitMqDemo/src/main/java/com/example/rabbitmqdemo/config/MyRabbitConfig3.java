package com.example.rabbitmqdemo.config;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class MyRabbitConfig3 {

    @Autowired // 通过Spring进行依赖注入，自动装配RabbitMQ的AmqpAdmin，用于管理队列、交换机等
    private AmqpAdmin amqpAdmin;

    @PostConstruct
    public void createExchangeQueueBinding (){
        // 使用AmqpAdmin来创建一个Direct类型的exchange
        amqpAdmin.declareExchange(new DirectExchange("direct.exchange"));
        // 使用AmqpAdmin来创建一个queue
        amqpAdmin.declareQueue(new Queue("baobao"));
        // 使用AmqpAdmin来创建一个绑定。绑定决定了消息如何路由从交换机到队列
        amqpAdmin.declareBinding(new Binding(
            "baobao",                 // 队列名称
            Binding.DestinationType.QUEUE,     // 目的地类型（这里是队列）
            "direct.exchange",              // 交换机名称
            "routingkey",                 // 路由键，决定了消息如何从交换机路由到队列
            null                               // 附加参数，这里没有额外参数所以为null
        ));
    }
}