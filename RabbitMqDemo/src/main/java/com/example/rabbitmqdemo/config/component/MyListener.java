package com.example.rabbitmqdemo.config.component;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class MyListener {

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "baobao", autoDelete = "false"),
            exchange = @Exchange(value = "direct.exchange", type = ExchangeTypes.DIRECT),
            key = "routingkey"))
    public void processMessage(Map<String, Object> map) {

        System.out.println("Received message: " + map);

    }
}