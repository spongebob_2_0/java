import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-10-08
 * Time: 20:32
 */
public class Main1 {
        public static void main(String[] args) {
            Scanner in = new Scanner(System.in);
            // 注意 hasNext 和 hasNextLine 的区别
            while (in.hasNextLine()) { // 注意 while 处理多个 case
                String[] str = in.nextLine().split(",");
                Arrays.sort(str);
                StringBuffer sb  = new StringBuffer();
                for(int i = 0; i < str.length; i++) {
                    if(i != str.length-1) {
                        sb.append(str[i]+",");
                    }else {
                        sb.append(str[i]);
                    }
                }
                System.out.println(sb);
            }
        }
}
