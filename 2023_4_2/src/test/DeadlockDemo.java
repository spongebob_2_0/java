package test;

public class DeadlockDemo {

    public static void main(String[] args) {
        Object lock = new Object();
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (lock) {
                    System.out.println("线程 1 第一次获取锁");
                    // 在持有锁的情况下再次获取锁
                    synchronized (lock) {
                        System.out.println("线程 1 第二次获取锁");
                    }
                }
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (lock) {
                    System.out.println("线程 2 获取锁");
                }
            }
        });

        thread1.start();
        thread2.start();
    }
}
