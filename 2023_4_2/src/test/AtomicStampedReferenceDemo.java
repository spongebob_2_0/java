package test;

import java.util.concurrent.atomic.AtomicStampedReference;

public class AtomicStampedReferenceDemo {
    private static AtomicStampedReference<String> ref = new AtomicStampedReference<>("A", 0);

    public static void main(String[] args) throws InterruptedException {
        // 线程1将值从A修改为B，然后又修改回A
        Thread thread1 = new Thread(() -> {
            int stamp = ref.getStamp();
            String prev = ref.getReference();
            ref.compareAndSet(prev, "B", stamp, stamp + 1);
            stamp = ref.getStamp();
            prev = ref.getReference();
            ref.compareAndSet(prev, "A", stamp, stamp + 1);
        });

        // 线程2尝试将值从A修改为C
        Thread thread2 = new Thread(() -> {
            int stamp = ref.getStamp();
            String prev = ref.getReference();
            while (!ref.compareAndSet(prev, "C", stamp, stamp + 1)) {
                // 如果CAS操作失败，则重新获取变量的值和版本号
                stamp = ref.getStamp();
                prev = ref.getReference();
            }
        });

        thread1.start();
        thread1.join();
        thread2.start();
        thread2.join();

        System.out.println(ref.getReference()); // 输出C
    }
}
