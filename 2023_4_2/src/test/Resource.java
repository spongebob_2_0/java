package test;

public class Resource {
    private int value;

    // 获取资源
    public synchronized int getValue() {
        return value;
    }

    // 更新资源
    public synchronized void setValue(int value) {
        this.value = value;
    }
}

class ThreadDemo extends Thread {
    private Resource resource;

    public ThreadDemo(Resource resource) {
        this.resource = resource;
    }

    public void run() {
        // 通过调用 getValue 方法获取资源
        int value = resource.getValue();
        // 对资源进行操作
        value++;
        // 通过调用 setValue 方法更新资源
        resource.setValue(value);
    }
}

class Main {
    public static void main(String[] args) {
        // 创建共享资源
        Resource resource = new Resource();

        // 创建多个线程并启动
        for (int i = 0; i < 5; i++) {
            ThreadDemo thread = new ThreadDemo(resource);
            thread.start();
        }

        // 等待所有线程执行完毕
        try {
            for (int i = 0; i < 5; i++) {
                ThreadDemo thread = new ThreadDemo(resource);
                thread.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // 输出资源最终的值
        System.out.println("Resource value: " + resource.getValue());
    }
}
