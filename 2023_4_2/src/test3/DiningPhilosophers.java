package test3;

public class DiningPhilosophers {

    private static final int NUM_PHILOSOPHERS = 5;
    private static final Object[] forks = new Object[NUM_PHILOSOPHERS];

    public static void main(String[] args) {
        // 初始化筷子对象
        for (int i = 0; i < NUM_PHILOSOPHERS; i++) {
            forks[i] = new Object();
        }

        // 创建哲学家线程并启动
        for (int i = 0; i < NUM_PHILOSOPHERS; i++) {
            final int philosopher = i;
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        // 哲学家思考
                        think(philosopher);
                        // 拿起左边的筷子
                        synchronized (forks[philosopher]) {
                            System.out.println("哲学家 " + philosopher + " 拿起左边的筷子");
                            // 拿起右边的筷子
                            synchronized (forks[(philosopher + 1) % NUM_PHILOSOPHERS]) {
                                System.out.println("哲学家 " + philosopher + " 拿起右边的筷子");
                                // 哲学家进餐
                                eat(philosopher);
                            }
                            System.out.println("哲学家 " + philosopher + " 放下右边的筷子");
                        }
                        System.out.println("哲学家 " + philosopher + " 放下左边的筷子");
                    }
                }
            });
            thread.start();
        }
    }

    private static void think(int philosopher) {
        try {
            Thread.sleep((long) (Math.random() * 10000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("哲学家 " + philosopher + " 思考");
    }

    private static void eat(int philosopher) {
        try {
            Thread.sleep((long) (Math.random() * 10000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("哲学家 " + philosopher + " 进餐");
    }
    //在这个示例中，使用了与之前相同的思考、拿起筷子和放下筷子的逻辑。不同之处在于，每个哲学家现在必须先拿起编号较小的筷子，再拿编号较大的筷子，从而避免出现所有哲学家都拿起左边的筷子的情况。这个示例并没有使用任何锁排序算法，而是直接通过代码
    //来保证筷子排序。
    //
    //注意到在哲学家线程中，先对左边的筷子加锁，再对右边的筷子加锁。这个顺序是非常重要的，因为如果两个哲学家同时想要拿起相邻的筷子，就有可能出现死锁的情况。如果所有哲学家都按照相同的顺序拿起筷子，就可以避免这种情况的发生。
    //
    //在这个示例中，为了简化代码，直接使用了编号较小的筷子来加锁。这样就可以保证所有哲学家都按照相同的顺序拿起筷子。需要注意的是，这个示例中并没有使用任何锁排序算法，所以如果每个哲学家都同时拿起了左边的筷子，仍然会发生死锁。
    //
    //总之，通过给筷子排序来解决哲学家就餐问题是一种简单而有效的方法，可以避免死锁的发生。但是，如果实现不当，仍然可能出现死锁的情况。因此，在编写多线程程序时，一定要谨慎地处理锁的使用，以避免出现死锁和其他并发问题。
}
