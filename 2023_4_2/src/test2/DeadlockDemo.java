package test2;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DeadlockDemo {

    private static Lock lock = new ReentrantLock(false); // 创建一个不可重入锁

    public static void main(String[] args) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                lock.lock();
                System.out.println("Thread acquired the lock for the first time");
                lock.lock(); // 再次获取同一把锁
                System.out.println("Thread acquired the lock for the second time");
                lock.unlock(); // 释放锁
                lock.unlock(); // 释放锁
            }
        });
        thread.start();
    }
}
