package com.wym.rabbitmq.spriingbootrabbitma.consumer;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;

/**
 * 死信队列消息消费者。
 */
@Slf4j 
@Component 
public class DeadLetterQueueConsumer {
    /**
     * 使用@RabbitListener注解监听死信队列QD，当队列中有消息时自动调用此方法。
     * @param message 接收到的消息，Spring AMQP Message对象，包含了消息的详细信息。
     */
    @RabbitListener(queues = "QD") // 监听死信队列QD
    public void receiveD(Message message) {
        String msg = new String(message.getBody()); // 从Message对象获取消息内容
        // 记录日志，显示当前时间和收到的死信队列消息内容
        log.info("当前时间：{}, 收到死信队列信息: {}", new Date(), msg);
    }
}
