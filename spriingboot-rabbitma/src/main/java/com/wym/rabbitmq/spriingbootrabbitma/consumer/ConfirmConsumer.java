package com.wym.rabbitmq.spriingbootrabbitma.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 消费者类，用于监听并消费RabbitMQ队列中的消息。
 */
@Component  
@Slf4j  
public class ConfirmConsumer {

    // 静态常量，代表监听的队列名称
    public static final String CONFIRM_QUEUE_NAME = "confirm.queue";

    /**
     * 使用@RabbitListener注解监听指定的RabbitMQ队列。
     * 当队列中有消息时，Spring会调用这个方法，并将消息作为参数传递给它。
     * 
     * @param message 从队列中接收到的消息对象，包含了消息的详细信息。
     */
    @RabbitListener(queues = CONFIRM_QUEUE_NAME)
    public void receiveMsg(Message message) {
        // 将消息体转换为字符串
        String msg = new String(message.getBody());
        // 使用log记录接收到的消息
        log.info("接受到队列 confirm.queue 的消息:{}", msg);
    }
}
