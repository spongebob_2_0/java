package com.wym.rabbitmq.spriingbootrabbitma.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class TtlQueueConfig {
    // 定义交换机、队列和路由键的名称常量
    public static final String X_EXCHANGE = "X";
    public static final String QUEUE_QA = "QA";
    public static final String QUEUE_QB = "QB";
    public static final String Y_DEAD_LETTER_EXCHANGE = "Y";
    public static final String DEAD_LETTER_QUEUE = "QD";

    /**
     * 声明xExchange交换机
     * @return Direct类型的交换机X
     */
    @Bean("xExchange")
    public DirectExchange xExchange() {
        return new DirectExchange(X_EXCHANGE);
    }

    /**
     * 声明yExchange死信交换机
     * @return Direct类型的死信交换机Y
     */
    @Bean("yExchange")
    public DirectExchange yExchange() {
        return new DirectExchange(Y_DEAD_LETTER_EXCHANGE);
    }

    /**
     * 声明队列QA，设置TTL为10秒，并绑定到死信交换机Y
     * @return 配置了TTL属性的队列QA
     */
    @Bean("queueA")
    public Queue queueA() {
        Map<String, Object> params = new HashMap<>();
        params.put("x-dead-letter-exchange", Y_DEAD_LETTER_EXCHANGE); // 设置死信交换机
        params.put("x-dead-letter-routing-key", "YD"); // 设置死信路由键
        params.put("x-message-ttl", 10000); // 设置消息TTL为10秒
        return QueueBuilder.durable(QUEUE_QA).withArguments(params).build();
    }

    /**
     * 绑定队列QA到交换机X
     * @return 绑定对象
     */
    @Bean
    public Binding queueBindingX(@Qualifier("queueA") Queue queueA, @Qualifier("xExchange") DirectExchange xExchange) {
        return BindingBuilder.bind(queueA).to(xExchange).with("XA");
    }

    /**
     * 声明队列QB，设置TTL为40秒，并绑定到死信交换机Y
     * @return 配置了TTL属性的队列QB
     */
    @Bean("queueB")
    public Queue queueB() {
        Map<String, Object> params = new HashMap<>();
        params.put("x-dead-letter-exchange", Y_DEAD_LETTER_EXCHANGE); // 同上
        params.put("x-dead-letter-routing-key", "YD"); // 同上
        params.put("x-message-ttl", 40000); // 设置消息TTL为40秒
        return QueueBuilder.durable(QUEUE_QB).withArguments(params).build();
    }

    /**
     * 绑定队列QB到交换机X
     * @return 绑定对象
     */
    @Bean
    public Binding queuebBindingX(@Qualifier("queueB") Queue queueB, @Qualifier("xExchange") DirectExchange xExchange) {
        return BindingBuilder.bind(queueB).to(xExchange).with("XB");
    }
    
    /**
     * 声明死信队列QD
     * @return 死信队列QD
     */
    @Bean("queueD")
    public Queue queueD() {
        return new Queue(DEAD_LETTER_QUEUE);
    }

    /**
     * 绑定死信队列QD到死信交换机Y
     * @return 绑定对象
     */
    @Bean
    public Binding deadLetterBindingQAD(@Qualifier("queueD") Queue queueD, @Qualifier("yExchange") DirectExchange yExchange) {
        return BindingBuilder.bind(queueD).to(yExchange).with("YD");
    }
}
