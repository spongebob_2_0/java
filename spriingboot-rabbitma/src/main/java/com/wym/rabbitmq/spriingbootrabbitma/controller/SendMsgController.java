package com.wym.rabbitmq.spriingbootrabbitma.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;

/**
 * 演示如何通过Spring Boot应用的Controller层向RabbitMQ发送消息。
 */
@RestController
@RequestMapping("/ttl") 
@Slf4j 
public class SendMsgController {
    
    @Autowired // 自动注入RabbitTemplate
    private RabbitTemplate rabbitTemplate;

    // 延迟交换机的名称和路由键常量
    public static final String DELAYED_EXCHANGE_NAME = "delayed.exchange";
    public static final String DELAYED_ROUTING_KEY = "delayed.routingkey";

    /**
     * 通过路径变量接收消息内容，并将该消息发送到两个不同TTL设置的队列中。
     * @param message 消息内容，通过URL路径传入
     */
    @GetMapping("/sendMsg/{message}")
    public void sendMsg(@PathVariable("message") String message) {
        // 记录日志，显示当前时间和要发送的消息
        log.info("当前时间：{}, 发送一条信息给两个TTL队列: {}", new Date(), message);
        // 发送消息到交换机X，路由键为XA，消息内容附加说明来自10S TTL队列
        rabbitTemplate.convertAndSend("X", "XA", "消息来自ttl为10S的队列: " + message);
        // 发送消息到交换机X，路由键为XB，消息内容附加说明来自40S TTL队列
        rabbitTemplate.convertAndSend("X", "XB", "消息来自ttl为40S的队列: " + message);
    }

    /**
     * 发送带有TTL的消息到队列C。
     * @param message 消息内容
     * @param ttlTime 消息的TTL时间（毫秒）
     */
    @GetMapping("/sendExpirationMsg/{message}/{ttlTime}")
    public void sendMsg(@PathVariable("message") String message, @PathVariable("ttlTime") String ttlTime) {
        // 动态设置消息的TTL，并发送到队列XC
        rabbitTemplate.convertAndSend("X", "XC", message, correlationData -> {
            correlationData.getMessageProperties().setExpiration(ttlTime); // 设置消息的TTL
            return correlationData;
        });
        log.info("当前时间：{}, 发送一条时长{}毫秒TTL信息给队列C:{}", new Date(), ttlTime, message);
    }

    /**
     * 发送带有延迟时间的消息到延迟队列。
     * @param message 消息内容
     * @param delayTime 延迟时间(毫秒)
     */
    @GetMapping("sendDelayMsg/{message}/{delayTime}")
    public void sendMsg(@PathVariable String message, @PathVariable("delayTime") Integer delayTime) {
        // 发送消息
        rabbitTemplate.convertAndSend(DELAYED_EXCHANGE_NAME, DELAYED_ROUTING_KEY, message,
                correlationData -> {
                    // 设置消息的延迟时间
                    correlationData.getMessageProperties().setDelay(delayTime);
                    return correlationData; // 返回配置后的消息
                });
        log.info(" 当 前 时 间 ： {}, 发 送 一 条 延 迟 {} 毫秒的信息给队列 delayed.queue:{}", new Date(), delayTime, message);
    }

    @GetMapping("/sendExpirationMsg2")
    public void sendMsg2() {

        // 发送10条消息，其中info5消息优先级最高
        for (int i = 1; i <= 10; i++) {
            String message = "info" + i;
            rabbitTemplate.convertAndSend("X", "XC", message, correlationData -> {
                // 设置消息的优先级
                correlationData.getMessageProperties().setPriority(10);
                return correlationData;
            });
            log.info("发送消息完成：{}",message);

        }

    }

}
