package com.wym.rabbitmq.spriingbootrabbitma.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class QueueConfig {
    /**
     * 声明优先级队列
     */
    @Bean("queue")
    public Queue queue() {
        Map<String, Object> args = new HashMap<>(3);
        args.put("x-max-priority", 10);
        return QueueBuilder.durable("priority_queue").withArguments(args).build();
    }
    
	 /**
     * 声明一个交换机
     */
    @Bean("exchange")
    public DirectExchange exchange() {
        return new DirectExchange("priority_exchange");
    }
	/* 
	 * 交换机和优先级队列进行绑定
	 */
    @Bean
    public Binding queuecBindingX(@Qualifier("queue") Queue queue, @Qualifier("exchange") DirectExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("priority");
    }
}