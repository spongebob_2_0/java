package com.wym.rabbitmq.spriingbootrabbitma.test;

import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;

import java.util.HashMap;
import java.util.Map;

/**
 * 优先级队列的消费者。
 */
public class PriorityConsumer {

    // 定义队列名称
    private final static String QUEUE_NAME = "priority_queue";

    public static void main(String[] args) throws Exception {
        // 获取与RabbitMQ服务器的连接通道
        Channel channel = RabbitMQUtils.getChannel();

        // 设置队列的最大优先级，官方推荐范围是1-10，过高的优先级设置会影响性能
        Map<String, Object> params = new HashMap<>();
        params.put("x-max-priority", 10); // 设置队列的最大优先级为10
        // 声明队列并指定其优先级参数
        channel.queueDeclare(QUEUE_NAME, true, false, false, params);

        // 消息消费时的回调
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody());
            System.out.println("消费的消息：" + message);
        };
        
        // 消费被取消时的回调，例如队列被删除
        CancelCallback cancelCallback = (consumerTag) -> {
            System.out.println("消息消费被中断");
        };

        // 启动消费者消费消息
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, cancelCallback);
    }
}