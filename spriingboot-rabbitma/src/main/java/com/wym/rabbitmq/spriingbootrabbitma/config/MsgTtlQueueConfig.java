package com.wym.rabbitmq.spriingbootrabbitma.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * 配置类用于创建延时队列QC和设置其死信交换机。
 */
@Configuration
public class MsgTtlQueueConfig {
    // 死信交换机名称
    public static final String Y_DEAD_LETTER_EXCHANGE = "Y";
    // 新增的队列名称
    public static final String QUEUE_C = "QC";

    /**
     * 创建队列C，并配置死信交换机。
     * 该队列没有设置TTL，TTL将在消息发送时动态指定。
     */
    @Bean("queueC")
    public Queue queueC() {
        Map<String, Object> args = new HashMap<>();
        // 绑定死信交换机
        args.put("x-dead-letter-exchange", Y_DEAD_LETTER_EXCHANGE);
        // 指定死信路由键
        args.put("x-dead-letter-routing-key", "YD");
        // 不设置TTL属性
        return QueueBuilder.durable(QUEUE_C).withArguments(args).build();
    }

    /**
     * 将队列C绑定到交换机X上。
     * 使用路由键"XC"进行绑定。
     */
    @Bean
    public Binding queuecBindingX(@Qualifier("queueC") Queue queueC, @Qualifier("xExchange") DirectExchange xExchange) {
        return BindingBuilder.bind(queueC).to(xExchange).with("XC");
    }
}
