package com.wym.rabbitmq.spriingbootrabbitma.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 报警消费者类，负责处理不能被路由的消息。
 */
@Component
@Slf4j 
public class WarningConsumer {
    // 警告队列的名称
    public static final String WARNING_QUEUE_NAME = "warning.queue";

    /**
     * 使用@RabbitListener注解监听警告队列。
     * 当警告队列中有消息时，此方法会被自动调用。
     *
     * @param message 从队列中接收到的消息对象
     */
    @RabbitListener(queues = WARNING_QUEUE_NAME)
    public void receiveWarningMsg(Message message) {
        // 将消息体转换为字符串
        String msg = new String(message.getBody());
        // 使用日志记录下接收到的不能被路由的消息
        log.error("报警发现不可路由消息：{}", msg);
    }
}
