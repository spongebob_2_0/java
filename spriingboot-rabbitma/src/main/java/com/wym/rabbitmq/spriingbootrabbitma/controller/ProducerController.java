package com.wym.rabbitmq.spriingbootrabbitma.controller;

import com.wym.rabbitmq.spriingbootrabbitma.component.MyCallBack;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

/**
 * 消息生产者控制器，用于演示高级消息发布。
 */
@RestController  
@RequestMapping("/confirm")  
@Slf4j  
public class ProducerController {
    // 交换机名称
    public static final String CONFIRM_EXCHANGE_NAME = "confirm.exchange";
    
    @Autowired
    private RabbitTemplate rabbitTemplate; 
    
    @Autowired
    private MyCallBack myCallBack;  // 自定义的消息确认回调类
    
    /**
     * 在依赖注入完成后设置RabbitTemplate的确认回调对象。
     */
    @PostConstruct // 此注解确保此方法在依赖注入完成后被自动调用
    public void init() {
        // 设置消息确认回调处理，以便确认消息是否成功发送到交换机
        rabbitTemplate.setConfirmCallback(myCallBack);

        /**
         * 设置mandatory标志为true，这确保如果消息不能被路由到任何队列，
         * 那么消息会被返回给生产者（而不是直接被丢弃）。
         */
        rabbitTemplate.setMandatory(true);

        // 设置消息返回回调处理，以便处理无法被路由到队列的消息
        rabbitTemplate.setReturnCallback(myCallBack);
    }

    /**
     * 通过HTTP GET请求发送消息，演示消息回调和退回。
     * @param message 通过URL路径传递的消息内容
     */
    @GetMapping("/sendMessage/{message}")
    public void sendMessage(@PathVariable String message) {
        // 创建关联数据对象，携带消息的唯一标识
        CorrelationData correlationData1 = new CorrelationData("1");
        String routingKey = "key1";
        // 发送消息到交换机，并指定路由键
        rabbitTemplate.convertAndSend(CONFIRM_EXCHANGE_NAME, routingKey, message + routingKey, correlationData1);
        log.info(routingKey + " 发送消息内容:{}", message + routingKey);

        // 演示发送另一条消息
        CorrelationData correlationData2 = new CorrelationData("2");
        routingKey = "key2";
        rabbitTemplate.convertAndSend(CONFIRM_EXCHANGE_NAME, routingKey, message + routingKey, correlationData2);
        log.info(routingKey + " 发送消息内容:{}", message + routingKey);
    }
}
