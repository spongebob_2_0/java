package com.wym.rabbitmq.spriingbootrabbitma.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.CustomExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class DelayedQueueConfig {
    // 定义延迟队列的名称
    public static final String DELAYED_QUEUE_NAME = "delayed.queue";
    // 定义延迟交换机的名称
    public static final String DELAYED_EXCHANGE_NAME = "delayed.exchange";
    // 定义路由键
    public static final String DELAYED_ROUTING_KEY = "delayed.routingkey";

    /**
     * 创建延迟队列。
     * @return 创建的延迟队列实例。
     */
    @Bean
    public Queue delayedQueue() {
        return new Queue(DELAYED_QUEUE_NAME);
    }

    /**
     * 自定义延迟交换机。
     * 使用 CustomExchange 类型来创建，这是因为我们需要一个支持延迟消息的交换机。
     * @return 创建的自定义延迟交换机实例。
     */
    @Bean
    public CustomExchange delayedExchange() {
        Map<String, Object> args = new HashMap<>();
        // 设置延迟交换机的类型为 direct
        args.put("x-delayed-type", "direct");
        // 创建一个类型为 "x-delayed-message" 的交换机，这个类型是插件提供的，专门用于处理延迟消息
        return new CustomExchange(DELAYED_EXCHANGE_NAME, "x-delayed-message", true, false, args);
    }

    /**
     * 将延迟队列绑定到延迟交换机。
     * @param queue 延迟队列
     * @param delayedExchange 自定义的延迟交换机
     * @return 创建的绑定关系实例。
     */
    @Bean
    public Binding bindingDelayedQueue(@Qualifier("delayedQueue") Queue queue, @Qualifier("delayedExchange") CustomExchange delayedExchange) {
        // 绑定延迟队列与延迟交换机，并设置使用的路由键
        return BindingBuilder.bind(queue).to(delayedExchange).with(DELAYED_ROUTING_KEY).noargs();
    }
}
