package com.wym.rabbitmq.spriingbootrabbitma.consumer;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 延迟队列消费者类。
 */
@Slf4j
@Component
public class DelayQueueConsumer {

    // 延迟队列名称常量
    public static final String DELAYED_QUEUE_NAME = "delayed.queue";
    //
    ///**
    // * 监听延迟队列。
    // * @param message 接收到的消息体
    // */
    //@RabbitListener(queues = DELAYED_QUEUE_NAME)
    //public void receiveDelayedQueue(Message message) {
    //    String msg = new String(message.getBody()); // 将消息体转换为字符串
    //    log.info("当前时间：{}, 收到延时队列的消息：{}", new Date().toString(), msg); // 记录日志
    //}

    /**
     * 监听优先级队列 priority_queue
     */
    @RabbitListener(queues = "priority_queue")
    public void receiveD(Message message, Channel channel) {
        String msg = new String(message.getBody());
        System.out.println("消费的消息：" + message);
    }
}
