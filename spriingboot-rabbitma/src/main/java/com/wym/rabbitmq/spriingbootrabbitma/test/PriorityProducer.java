package com.wym.rabbitmq.spriingbootrabbitma.test;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;

/**
 * 优先级队列生产者示例。
 */
public class PriorityProducer {

    private static final String QUEUE_NAME = "priority_queue";

    public static void main(String[] args) throws Exception{
        // 获取RabbitMQ连接中的通道
        Channel channel = RabbitMQUtils.getChannel();
        
        // 设置具有高优先级的消息属性
        AMQP.BasicProperties highPriorityProps = new AMQP.BasicProperties.Builder().priority(10).build();
        
        // 发送10条消息，其中info5消息优先级最高
        for (int i = 1; i <= 10; i++) {
            String message = "info" + i;
            if (i == 5) {
                // 对于info5消息，使用带有高优先级的属性发送
                channel.basicPublish("", QUEUE_NAME, highPriorityProps, message.getBytes());
            } else {
                // 其他消息正常发送，不设置优先级属性
                channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
            }
            System.out.println("发送消息完成：" + message);
        }
    }
}