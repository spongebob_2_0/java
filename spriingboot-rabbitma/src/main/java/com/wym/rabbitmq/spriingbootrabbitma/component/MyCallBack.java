package com.wym.rabbitmq.spriingbootrabbitma.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

/**
 * 实现RabbitMQ消息发布后的确认回调和无法路由消息的回退处理。
 */
@Component
@Slf4j
public class MyCallBack implements RabbitTemplate.ConfirmCallback, RabbitTemplate.ReturnCallback {
    /**
     * 当消息成功或失败送达交换机时，会触发此回调方法。
     *
     * @param correlationData 消息的相关数据，包括消息的唯一ID。
     * @param ack             表示交换机是否接收到消息，true为成功，false为失败。
     * @param cause           交换机未成功接收消息的原因描述。
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        // 获取消息的唯一标识
        String id = correlationData != null ? correlationData.getId() : "";
        if(ack) {
            // 如果消息成功送达交换机
            log.info("交换机已经收到 id 为:{}的消息", id);
        } else {
            // 如果消息未能成功送达交换机
            log.info("交换机还未收到 id 为:{}的消息，原因:{}", id, cause);
        }
    }

    /**
     * 当消息无法被路由到任何队列时的回调方法。
     *
     * @param message    发送的消息对象，包含消息内容和属性。
     * @param replyCode  退回的错误码，表示消息退回的原因。
     * @param replyText  退回的错误文本，描述消息为何被退回。
     * @param exchange   消息尝试发送到的交换机。
     * @param routingKey 消息使用的路由键。
     */
    @Override
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
        // 日志记录无法路由的消息详情
        log.error("消息：{}，被交换机 {} 退回，原因：{}，路由key：{}, 错误码:{}",
                new String(message.getBody()), exchange, replyText, routingKey, replyCode);
    }
}
