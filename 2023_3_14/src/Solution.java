import java.util.*;


public class Solution {
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     * 
     * @param nums int整型一维数组 
     * @return int整型
     */
    public int findPeakElement (int[] nums) {
        // write code here
        int index =0;
        for(int i =1; i< nums.length; i++) {
            if(nums[i] > nums[index]) {
                index =i;
            }
        }

        return index;
    }
}