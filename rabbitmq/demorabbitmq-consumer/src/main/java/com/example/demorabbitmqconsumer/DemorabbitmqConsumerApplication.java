package com.example.demorabbitmqconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemorabbitmqConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemorabbitmqConsumerApplication.class, args);
	}

}
