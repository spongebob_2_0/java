/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-06
 * Time: 4:33
 */
public class Main {

    public static void test(Animal animal) {
        animal.run();
    }

    public static void main(String[] args) {
        Animal animal1 = new Cat();

        animal1.run();
        ((Cat) animal1).eat();

    }


}
