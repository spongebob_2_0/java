/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-06
 * Time: 4:37
 */
public class Bird extends Animal{

    @Override
    public void run() {
        System.out.println("我是鸟, 我会唱歌! ");
    }
}
