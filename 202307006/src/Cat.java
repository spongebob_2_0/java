/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-06
 * Time: 4:28
 */
public class Cat  extends  Animal{

    @Override
    public void run() {
        System.out.println("我是猫,我会喵喵喵! ");
    }

    public void eat() {
        System.out.println("我是爱吃的小猫! ");
    }
}
