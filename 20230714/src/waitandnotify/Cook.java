package waitandnotify;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-16
 * Time: 19:17
 */
public class Cook extends Thread{
    @Override
    public void run() {
        while (true) {
            synchronized (Desk.lock) {
                if(Desk.count == 0) {
                    break;
                }else {
                    if (Desk.foodFlag == 0) {
                        System.out.println(getName() + "做了一碗面条");
                        Desk.foodFlag = 1;
                        Desk.lock.notify();
                    } else {
                        try {
                            Desk.lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
