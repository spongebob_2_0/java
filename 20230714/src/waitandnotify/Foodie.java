package waitandnotify;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-16
 * Time: 19:25
 */
public class Foodie extends Thread{
    @Override
    public void run() {
        while (true) {
            synchronized (Desk.lock) {
                if(Desk.count == 0) {
                    break;
                }else {
                    if (Desk.foodFlag == 1) {
                        Desk.count--;
                        System.out.println(getName() + "还能吃" + Desk.count + "碗");
                        Desk.foodFlag = 0;
                        Desk.lock.notify();
                    } else {
                        try {
                            Desk.lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
