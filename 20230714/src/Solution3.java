import java.util.*;
public class Solution3 {
    public int StrToInt(String str) {
        char[] array = str.toCharArray();
        if(array == null || array.length == 0) {
            return 0;
        }
        int sum = 0;
        int flag  = 1;
        if(array[0] == '+') {
            array[0] = 0;
            flag = 1;
        }
        if(array[0] == '-') {
            array[0] = 0;
            flag = -1;
        }
        for(int i = 0; i < array.length; i++) {
            if(array[i] < '0' || array[i] > '9') {
                return 0;
            }
            sum = sum*10 + array[i] - '0';
        }
        return sum*flag;
    }
}
