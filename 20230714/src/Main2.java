/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-15
 * Time: 15:21
 */
class  Node {

    int val;
    Node left;
    Node right;

    public Node(int val) {
        this.val = val;
    }

}
public class Main2 {
    public static void main(String[] args) {
        Thread t = Thread.currentThread();
        System.out.println(t.getName());
        t.setName("123");
        System.out.println(t.getName());
    }
}
