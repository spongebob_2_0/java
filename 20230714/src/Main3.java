/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-16
 * Time: 2:44
 */
public class Main3 {
    public static void main(String[] args) {

        Thread t1 = new Thread(()->{
            while (true) {
                try {
                    Thread.sleep(1000);
                    System.out.println(Thread.currentThread().getName()+"测试run");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"线程1");
        Thread t2 = new Thread(()-> {
            while (true) {
                try {
                    Thread.sleep(1000);
                    Thread.yield();
                    System.out.println(Thread.currentThread().getName()+"测试run");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        },"线程2");

        t1.start();
        t2.start();

    }
}
