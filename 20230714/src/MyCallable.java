import jdk.nashorn.internal.codegen.CompilerConstants;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-16
 * Time: 2:50
 */
public class MyCallable implements Callable{
    @Override
    public String call() throws Exception {
        System.out.println("测试call方法!!");
        return "12123";
    }

    public static void main(String[] args) {
        MyCallable myCallable = new MyCallable();
        FutureTask<String> futureTask = new FutureTask<>(myCallable);
        Thread t = new Thread(futureTask);
//        t.start();

        Callable<String> callable = new Callable<String>() {
            @Override
            public String call() throws Exception {
                System.out.println("测试callable 方法  ");
                return "1231";
            }
        };

        FutureTask<String> futureTask1 = new FutureTask<>(callable);
        Thread t1 = new Thread(futureTask1);
        Thread t2 = new Thread(futureTask1);
        t1.start();
    }


}
