import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-07-16
 * Time: 2:41
 */
public class MyThread1 extends Thread{
    private static Object object = new Object();
    private static int ticket = 0;
    static Lock lock = new ReentrantLock();
    @Override
    public void run() {
        while (true) {
//            synchronized (object) {
                lock.lock();
            try {
                if (ticket < 100) {
                    ticket++;
                    System.out.println("我在" + getName() + "买到了第" + ticket + "票");
                }else {
                    break;
                }
            } finally {
                lock.unlock();
            }
//            }
        }
    }

    public static void main(String[] args) {
        MyThread1 t1 = new MyThread1();
        MyThread1 t2 = new MyThread1();
        MyThread1 t3 = new MyThread1();
        t1.setName("窗口1");
        t2.setName("窗口2");
        t3.setName("窗口3");
        t1.start();
        t2.start();
        t3.start();
    }

}
