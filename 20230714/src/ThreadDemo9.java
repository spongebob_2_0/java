public class ThreadDemo9 {
    private static int sum = 10;
    public static void main(String[] args) {
        Object ob = new Object();
        while (sum > 0) {
            Thread t1 = new Thread(()->{
                if (sum > 0) {
                    synchronized (ob) {
                        if (sum > 0 )
                            System.out.println( " t1 + 第" +sum + "车牌");
                        sum--;
                    }
                }
            });
            Thread t2 = new Thread(()->{
                if (sum > 0) {
                    synchronized (ob) {
                        if (sum > 0) {

                            System.out.println("t2 + 第" + sum + "车牌");
                            sum--;
                        }
                    }
                }
            });
            Thread t3 = new Thread(()->{
                if (sum > 0) {
                    synchronized (ob) {
                        if (sum > 0) {
                            System.out.println("t3 + 第" + sum + "车牌");
                            sum--;
                        }
                    }
                }
            });
            t1.start();
            t2.start();
            t3.start();
        }

    }
}