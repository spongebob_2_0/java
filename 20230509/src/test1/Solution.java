package test1;

class Solution {
    public String reverseLeftWords(String s, int n) {
        char[] array =s.toCharArray();
        int len =array.length;
        reverseString(array,0,n-1);
        reverseString(array,n,len-1);
        reverseString(array,0,len-1);
        return new String(array);
    }
    private void reverseString(char[] array,int start,int end) {
        while(start < end) {
            char tmp =array[start];
            array[start] = array[end];
            array[end] =tmp;
            start++;
            end--;
        }
    }
}