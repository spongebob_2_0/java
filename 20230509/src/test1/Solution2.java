package test1;

class Solution2 {
    public String reverseWords(String s) {
        //移除多余的空格
        StringBuilder sb =removeSpace(s);
        //翻转整个字符串
        reverseString(sb,0,sb.length()-1);
        //翻转每个单词
        reverseEachWord(sb);
        return sb.toString();
    }
    //移除多余的空格
    private StringBuilder removeSpace(String s) {
        int start = 0;
        int end = s.length()-1;
        while(s.charAt(start) == ' ') {
            start++;
        }
        while(s.charAt(end) == ' ') {
            end--;
        }
        StringBuilder sb =new StringBuilder();
        while(start <= end) {
            char ch =s.charAt(start);
            if(ch != ' ' || sb.charAt(sb.length()-1) != ' ') {
                sb.append(ch);
            }
            start++;
        }
        return sb;
    }
    //指定区间翻转字符串
    private void reverseString(StringBuilder sb,int start,int end) {
        while(start < end) {
            char tmp =sb.charAt(start);
            sb.setCharAt(start,sb.charAt(end));
            sb.setCharAt(end,tmp);
            start++;
            end--;
        }
    }
    //翻转部分单词
    private void reverseEachWord(StringBuilder sb) {
        int start =0;
        int end =1;
        int n = sb.length();
        while(start < n) {
            while(end < n && sb.charAt(end) != ' ') {
                end++;
            }
            reverseString(sb,start,end-1);
            start = end+1;
            end = start+1;
        }
    }

}