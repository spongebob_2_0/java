package test1;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-04-08
 * Time: 3:10
 */
public class JDBCInsert {
    public static void main(String[] args) throws SQLException {
        Scanner scanner =new Scanner(System.in);
        //JDBC需要一下步骤完成开发
        //1.创建并初始化一个数据源
        DataSource dataSource =new MysqlDataSource();
        ((MysqlDataSource)dataSource).setURL("jdbc:mysql://127.0.0.1:3306/demo?characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("root");
        //2.和数据库服务器建立连接
        Connection connection = dataSource.getConnection();
        //3.构造sql语句
        System.out.println("请输入名字:->");
        String name = scanner.next();
        System.out.println("请输入年龄:->");
        int age = scanner.nextInt();
        System.out.println("请输入班级:->");
        String clas  = scanner.next();
        // 此处光是一个 String 类型的 SQL 还不行，需要把这个 String 包装成一个 "语句对象"
        String sql = "insert into student values(?,?,?)";
        PreparedStatement statement =connection.prepareStatement(sql);
        statement.setString(1,name);
        statement.setInt(2,age);
        statement.setString(3,clas);
        //这个打印需要加到拼接数据之后
        System.out.println(statement);
        //4.执行sql语句
        int ret = statement.executeUpdate();
        System.out.println("ret = " + ret);
        //5.释放必要的资源
        statement.close();
    }
}
