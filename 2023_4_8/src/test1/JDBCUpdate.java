package test1;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-04-08
 * Time: 20:55
 */
public class JDBCUpdate {
    public static void main(String[] args) throws SQLException {
        Scanner scanner =new Scanner(System.in);
        //1.创建数据源
        DataSource dataSource = new MysqlDataSource();
        ((MysqlDataSource)dataSource).setURL("jdbc:mysql://127.0.0.1:3306/demo?characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("root");
        Connection connection = dataSource.getConnection();
        // 构造SQL
        System.out.println("请输入修改前的name：");
        String name1 = scanner.next();
        System.out.println("请输入修改后的name：");
        String name2 = scanner.next();
        String sql = "update student set name = ? where name = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1,name2);
        statement.setString(2,name1);

        //执行SQL
        int ret = statement.executeUpdate();
        System.out.println("ret: "+ ret);

        //回收资源
        statement.close();
        connection.close();
    }
}
