package test1;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-04-08
 * Time: 17:47
 */
public class JDBCSelect {
    public static void main(String[] args) throws SQLException {
        //1.创建并初始化数据源
        DataSource dataSource =new MysqlDataSource();
        ((MysqlDataSource)dataSource).setUrl("jdbc:mysql://127.0.0.1:3306/demo?characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("root");
        //2.建立连接
        Connection connection =dataSource.getConnection();
        //3.构造sql语句
        String sql ="select * from student";
        PreparedStatement statement = connection.prepareStatement(sql);
        //4.执行sql语句
        ResultSet resultSet = statement.executeQuery();
        //遍历结果集合
        while (resultSet.next()) {
            //把resultSet想象成一个表格,同时表格里有个光标,初始情况下光标指向表格最上面
            //每次调用next(),光标往下走一行
            //当光标指向某一行的时候,就可以通过getXXX获取当前行的数据
            String name =resultSet.getString("name");
            int age =resultSet.getInt("age");
            String clas =resultSet.getString("class");
            System.out.println("name = "+ name+",age = " +age+",class = "+clas);
        }
        //5.释放资源
        resultSet.close();
        statement.close();
        connection.close();
    }
}
