package test1;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-04-08
 * Time: 20:45
 */
public class JDBCDelete {
    public static void main(String[] args) throws SQLException {
        Scanner scanner =new Scanner(System.in);
        //1. 创建数据源，设置信息
        DataSource dataSource = new MysqlDataSource();
        ((MysqlDataSource)dataSource).setURL("jdbc:mysql://127.0.0.1:3306/demo?characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("root");
        //2. 和数据库建立连接
        Connection connection = dataSource.getConnection();
        //3. 构造 SQL
        System.out.println("请输入要删除的age：");
        int age = scanner.nextInt();
        String sql = "delete from student where age = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1,age);
        //4. 执行 SQL
        int ret = statement.executeUpdate();
        System.out.println("ret=" + ret);
        //5. 释放资源
        statement.close();
        connection.close();

    }
}
