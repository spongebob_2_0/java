import java.util.Stack;

class Solution {
    public static boolean isValid(String s) {
        Stack<Character> stack =new Stack<>();
        for(int i =0; i< s.length(); i++) {
            char ch =s.charAt(i);
            if(ch == '(' || ch == '{' || ch == '['){
                stack.push(ch);
            }else{
                if(stack.empty()){
                    return false;
                }
                char ch2 =stack.peek();
            if(ch == ')' && ch2 == '(' || ch == '}' && ch2 == '{' ||
               ch == ']' && ch2 == '[' ){
                   stack.pop();
               }else{
                   return false;
               }
            }
        }
        if(!stack.empty()){
        return false;
    }
    return true;
    }

    public static void main(String[] args) {
        String s ="()";
        boolean ret = isValid(s);
        System.out.println(ret);
    }
}