import java.util.Stack;

class MinStack {
        private Stack<Integer> stack;
        private Stack<Integer>  mimStack;

        public MinStack(){
            stack =new Stack();
            mimStack =new Stack();
        }
        public void push(int val) {
        stack.push(val);
        if(mimStack.empty()){
            mimStack.push(val);
        }else{
            if(val <= mimStack.peek()){
                mimStack.push(val);
            }
        }
    }
    
    public void pop() {
        if(!stack.empty()){
            Integer val =stack.pop();
            if(val.equals(mimStack.peek())){
                mimStack.pop();
            }
        }
    }
    
    public int top() {
        if(!stack.empty()){
            return stack.peek();
        }
        return -1;
    }
    
    public int getMin() {
        return mimStack.peek();
    }
}
