package teet3;

import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class JDBCInsert {
    public static void main(String[] args) throws SQLException {
        //JDBC需要一下步骤完成开发
        //1.创建并初始化一个数据源
        DataSource dataSource =new MysqlDataSource();
        ((MysqlDataSource)dataSource).setURL("jdbc:mysql://127.0.0.1:3306/demo?characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("root");
        //2.和数据库服务器建立连接
        Connection connection = dataSource.getConnection();
        //3.构造sql语句
        String sql = "insert into student values('小诗诗',18,'java1班')";
        PreparedStatement statement =connection.prepareStatement(sql);
        //4.执行sql语句
        int ret = statement.executeUpdate();
        System.out.println("ret = " + ret);
        //5.释放必要的资源
        statement.close();
        connection.close();

    }
}
