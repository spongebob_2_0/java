package test2;

import java.io.IOException;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;

// 使用继承, 是为了复用之前的代码.
public class UdpDictServer1 extends UdpEchoServer {
    private Map<String, String> dict = new HashMap<>();

    public UdpDictServer1(int port) throws SocketException {
        super(port);

        dict.put("dog", "小狗");
        dict.put("cat", "小猫");
        dict.put("fuck", "卧槽");

    }

    @Override
    public String process(String request) {

        return dict.getOrDefault(request, "该单词没有查到!");
    }

    public static void main1(String[] args) throws IOException {
        UdpDictServer1 udpDictServer = new UdpDictServer1(9090);
        udpDictServer.start();
    }

}