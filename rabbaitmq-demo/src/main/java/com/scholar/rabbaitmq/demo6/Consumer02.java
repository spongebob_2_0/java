package com.scholar.rabbaitmq.demo6;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import com.scholar.rabbaitmq.demo2.RabbitMQUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

/**
 * 消费者C2专门消费死信队列中的消息。
 */
public class Consumer02 {
    // 死信交换机名称
    private static final String DEAD_EXCHANGE = "dead_exchange";

    public static void main(String[] args) throws IOException, TimeoutException {
        // 获取与RabbitMQ的连接通道
        Channel channel = RabbitMQUtils.getChannel();
        
        // 声明死信交换机，类型为直接交换机
        channel.exchangeDeclare(DEAD_EXCHANGE, BuiltinExchangeType.DIRECT);
        
        // 声明死信队列
        String deadQueue = "dead-queue";
        channel.queueDeclare(deadQueue, false, false, false, null);
        
        // 将死信队列绑定到死信交换机，路由键为"lisi"
        channel.queueBind(deadQueue, DEAD_EXCHANGE, "lisi");

        System.out.println("等待接收死信消息........... ");
        
        // 定义消息接收的回调
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            // 解析消息内容
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            // 打印接收到的死信消息
            System.out.println("Consumer02 接收到消息" + message);
        };
        
        // 开始消费死信队列中的消息，不需要手动ACK确认
        channel.basicConsume(deadQueue, true, deliverCallback, consumerTag -> {});
    }
}