package com.scholar.rabbaitmq.demo6;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.scholar.rabbaitmq.demo2.RabbitMQUtils;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Producer {
    private static final String NORMAL_EXCHANGE = "normal_exchange"; // 定义交换机名称

    public static void main(String[] args) throws IOException, TimeoutException {
        // 获取通道
        Channel channel = RabbitMQUtils.getChannel();
        // 声明一个直接类型的交换机
        channel.exchangeDeclare(NORMAL_EXCHANGE, BuiltinExchangeType.DIRECT);

        // 创建消息属性配置，设置消息TTL为10秒
        //AMQP.BasicProperties properties = new AMQP.BasicProperties().builder().expiration("10000").build();
        
        // 循环发送10条消息
        for (int i = 1; i <= 10; i++) {
            String message = "info" + i; // 消息内容
            // 发送消息到交换机，路由键为'zhangsan'，带有TTL属性
            channel.basicPublish(NORMAL_EXCHANGE, "zhangsan", null, message.getBytes());
            System.out.println("生产者发送消息:" + message); // 控制台打印发送的消息
        }
    }
}