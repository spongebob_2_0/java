package com.scholar.rabbaitmq.demo6;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import com.scholar.rabbaitmq.demo2.RabbitMQUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

public class Consumer01 {
    private static final String NORMAL_EXCHANGE = "normal_exchange"; // 普通交换机名称
    private static final String DEAD_EXCHANGE = "dead_exchange"; // 死信交换机名称

    public static void main(String[] args) throws IOException, TimeoutException {
        Channel channel = RabbitMQUtils.getChannel();
        
        // 声明普通交换机和死信交换机，类型为直接交换机
        channel.exchangeDeclare(NORMAL_EXCHANGE, BuiltinExchangeType.DIRECT);
        channel.exchangeDeclare(DEAD_EXCHANGE, BuiltinExchangeType.DIRECT);
        
        // 声明死信队列，并绑定到死信交换机
        String deadQueueName = "dead-queue";
        channel.queueDeclare(deadQueueName, false, false, false, null);
        channel.queueBind(deadQueueName, DEAD_EXCHANGE, "lisi");

        // 设置普通队列绑定死信队列的参数
        Map<String, Object> params = new HashMap<>();
        params.put("x-dead-letter-exchange", DEAD_EXCHANGE); // 指定死信交换机
        params.put("x-dead-letter-routing-key", "lisi"); // 指定死信路由键
        
        // 声明普通队列，并绑定到普通交换机
        String normalQueue = "normal-queue";
        channel.queueDeclare(normalQueue, false, false, false, params);
        channel.queueBind(normalQueue, NORMAL_EXCHANGE, "zhangsan");
        System.out.println("等待接收消息........... ");

        // 定义消息接收的回调
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            if (message.equals("info5")) {
                System.out.println("Consumer01 接收到消息" + message + "并拒绝签收该消息");
                // 对"info5"消息执行拒绝操作，requeue设置为false代表不重新入队，该消息会被发送到死信队列
                channel.basicReject(delivery.getEnvelope().getDeliveryTag(), false);
            } else {
                System.out.println("Consumer01 接收到消息" + message);
                // 对其他消息执行正常的确认操作
                channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            }
        };
        // 开启手动应答模式消费消息
        channel.basicConsume(normalQueue, false, deliverCallback, consumerTag -> {});
    }
}
