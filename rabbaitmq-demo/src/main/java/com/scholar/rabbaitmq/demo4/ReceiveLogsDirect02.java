package com.scholar.rabbaitmq.demo4;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import com.scholar.rabbaitmq.demo2.RabbitMQUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

/**
 * 消费者 C2 - 专门接收error级别的日志消息。
 */
public class ReceiveLogsDirect02 {
    // 直接交换机的名称
    public static final String EXCHANGE_NAME = "direct_logs";

    public static void main(String[] args) throws IOException, TimeoutException {
        // 获取与RabbitMQ服务的连接通道
        Channel channel = RabbitMQUtils.getChannel();
        // 声明一个直接类型的交换机
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        // 指定队列名称
        String queueName = "disk";
        // 声明队列（非持久、非独占、自动删除）
        channel.queueDeclare(queueName, false, false, false, null);
        // 绑定队列到交换机，并指定接收error路由键的消息
        channel.queueBind(queueName, EXCHANGE_NAME, "error");

        System.out.println("等待接收消息...");
        
        // 消息到达的回调函数
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            // 将消息体转换为字符串
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            // 构造显示的消息，包括路由键和消息体
            message = "接收绑定键:" + delivery.getEnvelope().getRoutingKey() + ",消息:" + message;
            System.out.println("error 消息已经接收：\n" + message);
        };
        // 开始消费队列上的消息
        channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {});
    }
}