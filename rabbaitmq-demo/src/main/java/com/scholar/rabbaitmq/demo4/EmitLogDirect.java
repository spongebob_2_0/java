package com.scholar.rabbaitmq.demo4;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.scholar.rabbaitmq.demo2.RabbitMQUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

/**
 * 生产者 EmitLogDirect
 * 使用直接交换机发送带有不同路由键的消息。
 */
public class EmitLogDirect {
    // 直接交换机的名称
    private static final String EXCHANGE_NAME = "direct_logs";

    public static void main(String[] args) throws IOException, TimeoutException {
        // 获取与RabbitMQ服务的连接通道
        Channel channel = RabbitMQUtils.getChannel();
        // 声明一个直接类型的交换机
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        
        // 创建一个包含不同路由键及对应消息的映射关系
        Map<String, String> bindingKeyMap = new HashMap<>();
        bindingKeyMap.put("info", "这是一个 info 信息");
        bindingKeyMap.put("warning", "这是一个 warning 信息");
        bindingKeyMap.put("error", "这是一个 error 信息");
        // 注意：debug 没有对应的队列消费者，所以这个消息会丢失
        bindingKeyMap.put("debug", "这是一个 debug 信息");

        // 遍历映射关系，发送消息
        for (Map.Entry<String, String> bindingKeyEntry : bindingKeyMap.entrySet()) {
            String bindingKey = bindingKeyEntry.getKey(); // 路由键
            String message = bindingKeyEntry.getValue(); // 消息内容
            
            // 使用指定的路由键发布消息
            channel.basicPublish(EXCHANGE_NAME, bindingKey, null, message.getBytes());
            System.out.println("生产者发出消息:" + message);
        }
    }
}