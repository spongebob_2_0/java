package com.scholar.rabbaitmq.demo5;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import com.scholar.rabbaitmq.demo2.RabbitMQUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

/**
 * 消费者 1 - 订阅 "*.orange.*" 模式的消息。
 */
public class ReceiveLogsTopic01 {
    // 主题交换机的名称
    private static final String EXCHANGE_NAME = "topic_logs";

    public static void main(String[] args) throws IOException, TimeoutException {
        // 获取与RabbitMQ服务的连接通道
        Channel channel = RabbitMQUtils.getChannel();
        // 声明一个主题类型的交换机
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.TOPIC);
        
        // 队列名称
        String queueName = "Q1";
        // 声明队列（非持久、非独占、自动删除）
        channel.queueDeclare(queueName, false, false, false, null);
        // 将队列绑定到交换机，并指定符合 "*.orange.*" 模式的路由键
        channel.queueBind(queueName, EXCHANGE_NAME, "*.orange.*");

        System.out.println("等待接收消息........... ");

        // 消息到达的回调函数
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            // 将消息体转换为字符串
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            // 打印接收到的消息，包括队列名、绑定键和消息内容
            System.out.println("接收队列:" + queueName + " 绑定键:" + delivery.getEnvelope().getRoutingKey() + ",消息:" + message);
        };
        // 开始消费队列上的消息
        channel.basicConsume(queueName, true, deliverCallback, consumerTag -> { });
    }
}