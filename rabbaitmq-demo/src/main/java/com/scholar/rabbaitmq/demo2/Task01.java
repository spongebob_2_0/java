package com.scholar.rabbaitmq.demo2;

import com.rabbitmq.client.Channel; // 引入RabbitMQ通道类

import java.util.Scanner; // 引入Java的Scanner类，用于从命令行接收输入

/**
 * 生产者类，用于发送消息到RabbitMQ队列。
 */
public class Task01 {
    // 定义要发送消息的队列名称
    public static final String QUEUE_NAME = "queue2";

    public static void main(String[] args) throws Exception {
        // 获取RabbitMQ通道，使用工具类RabbitMQUtils来简化连接和通道的创建过程
        Channel channel = RabbitMQUtils.getChannel();

        // 创建Scanner对象，用于从命令行读取输入的消息
        Scanner scanner = new Scanner(System.in);
        // 使用hasNext()方法检查是否有输入，如果有，循环会继续
        while (scanner.hasNext()) {
            // 读取一行输入作为消息
            String message = scanner.next();
            // 使用basicPublish方法发送消息到RabbitMQ
            // 参数说明：交换机名，队列名称，配置参数，消息
            channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
            // 在控制台打印发送完成的消息
            System.out.println("消息发送完成：" + message);
        }
    }
}
