package com.scholar.rabbaitmq.demo2;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConfirmCallback;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.TimeoutException;

/**
 * 演示异步确认发布的实现方式，适合高吞吐量的消息发送场景。
 */
public class ConfirmMessage3 {
    public static final int MESSAGE_COUNT = 1000;

    public static void main(String[] args) throws IOException, TimeoutException {
        // 获取通道
        Channel channel = RabbitMQUtils.getChannel();
        // 声明队列
        String queueName = UUID.randomUUID().toString();
        channel.queueDeclare(queueName, false, false, false, null);
        // 开启发布确认模式
        channel.confirmSelect();
        // 使用ConcurrentSkipListMap来存储所有已发送但未确认的消息，确保线程安全和高效的操作
        ConcurrentSkipListMap<Long, String> outstandingConfirms = new ConcurrentSkipListMap<>();
        // 定义消息确认（ACK）的回调处理
        ConfirmCallback ackCallback = (deliveryTag, multiple) -> {
            if (multiple) {
                // 如果multiple为true，表示批量确认，那么就删除所有小于等于deliveryTag的消息
                ConcurrentNavigableMap<Long, String> confirmed = outstandingConfirms.headMap(deliveryTag, true);
                confirmed.clear();
            } else {
                // 如果multiple为false，只确认当前的deliveryTag
                outstandingConfirms.remove(deliveryTag);
            }
        };
       // 定义未确认（NACK）的回调处理
        ConfirmCallback nackCallback = (deliveryTag, multiple) -> {
           // 获取未确认消息的内容
            String message = outstandingConfirms.get(deliveryTag);
            System.out.println("发布的消息" + message + "未被确认，序列号：" + deliveryTag);
        };
        /**
         * 添加一个异步确认的监听器
         * 1.确认收到消息的回调
         * 2.未收到消息的回调
         */
        channel.addConfirmListener(ackCallback, nackCallback);
        long begin = System.currentTimeMillis();
        for (int i = 0; i < MESSAGE_COUNT; i++) {
            String message = "消息" + i;
            // 在发送消息之前，先记录下该消息的序列号和内容
            //  channel.getNextPublishSeqNo()预先获取这条消息将要被分配的序列号
            outstandingConfirms.put(channel.getNextPublishSeqNo(), message);
            // 发送消息
            channel.basicPublish("", queueName, null, message.getBytes());
        }
        long end = System.currentTimeMillis();
        System.out.println("发布" + MESSAGE_COUNT + "个异步确认消息,耗时" + (end - begin) + "ms");
    }
}