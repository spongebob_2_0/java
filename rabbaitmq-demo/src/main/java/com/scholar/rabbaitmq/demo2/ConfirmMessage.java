package com.scholar.rabbaitmq.demo2;

import com.rabbitmq.client.Channel;
import java.io.IOException;
import java.util.UUID; 
import java.util.concurrent.TimeoutException;

/**
 * 演示单个确认发布模式。
 * 这种模式下，每发送一条消息就等待RabbitMQ的确认，确保消息被可靠投递。
 */
public class ConfirmMessage {
    // 定义要发送的消息数量
    public static final int MESSAGE_COUNT = 1000;

    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        // 通过工具类获取与RabbitMQ的连接通道
        Channel channel = RabbitMQUtils.getChannel();
        
        // 随机生成队列名称，并声明一个新的队列
        String queueName = UUID.randomUUID().toString();
        channel.queueDeclare(queueName, true, false, false, null);
        
        // 开启发布确认模式
        channel.confirmSelect();

        // 记录开始时间，用于计算整个发布过程的耗时
        long begin = System.currentTimeMillis();

        // 循环发送消息
        for (int i = 0; i < MESSAGE_COUNT; i++) {
            String message = i + "";
            // 发布消息到随机生成的队列中
            channel.basicPublish("", queueName, null, message.getBytes());
            // 等待服务端的确认响应，如果服务端在指定时间内未确认，可以考虑消息重发
            boolean flag = channel.waitForConfirms();
            if (flag) {
                // 当消息得到确认时，打印消息发送成功的信息
                System.out.println("消息发送成功");
            }
        }

        // 记录结束时间
        long end = System.currentTimeMillis();
        // 打印发送完所有消息所耗费的总时间
        System.out.println("发布" + MESSAGE_COUNT + "个单独确认消息,耗时" + (end - begin) + "ms");
    }
}