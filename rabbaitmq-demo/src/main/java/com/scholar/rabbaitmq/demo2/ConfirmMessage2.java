package com.scholar.rabbaitmq.demo2;

import com.rabbitmq.client.Channel;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

/**
 * 实现RabbitMQ的批量确认发布机制。
 * 与单个确认发布相比，批量确认可以显著提高消息发布的吞吐量。
 */
public class ConfirmMessage2 {
    // 定义要发送的消息总数
    public static final int MESSAGE_COUNT = 1000;

    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        // 从工具类中获取与RabbitMQ的连接通道
        Channel channel = RabbitMQUtils.getChannel();
        // 随机生成一个队列名，并声明一个队列
        String queueName = UUID.randomUUID().toString();
        channel.queueDeclare(queueName, true, false, false, null);
        // 开启发布确认模式
        channel.confirmSelect();

        // 记录开始时间
        long begin = System.currentTimeMillis();

        // 设置每批次确认消息的大小
        int batchSize = 100;
        // 初始化未确认消息的计数器
        int outstandingMessageCount = 0;

        for (int i = 0; i < MESSAGE_COUNT; i++) {
            // 构建消息
            String message = i + "";
            // 发布消息到队列
            channel.basicPublish("", queueName, null, message.getBytes());
            // 未确认的消息数量加1
            outstandingMessageCount++;
            // 当发布的消息数量达到批量大小时，进行一次批量确认，接收到mq给予明确的确认（ACK）后再继续执行
            if (outstandingMessageCount == batchSize) {
                channel.waitForConfirms();
                outstandingMessageCount = 0; // 重置未确认消息计数器
            }
        }
        // 最后确认是否还有未确认的消息，确保所有消息都被确认
        if (outstandingMessageCount > 0) {
            channel.waitForConfirms();
        }
        // 记录结束时间，并计算发送消息的总耗时
        long end = System.currentTimeMillis();
        System.out.println("发布" + MESSAGE_COUNT + "个批量确认消息,耗时" + (end - begin) + "ms");
    }
}