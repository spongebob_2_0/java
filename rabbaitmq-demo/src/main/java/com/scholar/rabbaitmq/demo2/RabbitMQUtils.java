package com.scholar.rabbaitmq.demo2;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 连接 RabbitMQ 的工具类
 */
public class RabbitMQUtils {
    
    public static Channel getChannel() throws IOException, TimeoutException {
        // 创建连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 设置RabbitMQ服务器地址
        connectionFactory.setHost("106.14.91.138");
        // 设置用户名
        connectionFactory.setUsername("root");
        // 设置密码
        connectionFactory.setPassword("183193");
        // 创建连接
        Connection connection = connectionFactory.newConnection();
        // 创建信道并返回
        return connection.createChannel();
    }
}