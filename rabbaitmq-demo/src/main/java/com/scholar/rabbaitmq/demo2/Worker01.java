package com.scholar.rabbaitmq.demo2;

import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 工作线程，相当于消费者
 */
public class Worker01 {
    public static final String QUEUE_NAME = "queue2";

    public static void main(String[] args) throws IOException, TimeoutException {
        Channel channel = RabbitMQUtils.getChannel();

        // 声明（创建）队列
        // 数依次是队列名、是否持久化、是否独占队列、是否自动删除和其他参数
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        // 消息接收的回调
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String receivedMessage = new String(delivery.getBody());
            System.out.println("接收到消息:" + receivedMessage);
        };
        // 消息被取消时的回调
        CancelCallback cancelCallback = (consumerTag) -> {
            System.out.println(consumerTag + " 消费者取消消费接口回调逻辑");
        };
        
        System.out.println("second 消费者启动等待消费.................. ");
        // 开始消费消息
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, cancelCallback);
    }
}