package com.scholar.rabbaitmq.demo2;
import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;

public class Worker03 {
    // 定义队列名称
    private static final String TASK_QUEUE_NAME = "ack_queue";

    public static void main(String[] args) throws Exception {
        // 通过工具类获取与RabbitMQ的连接通道
        Channel channel = RabbitMQUtils.getChannel();
        // 控制台打印，表示消费者正在等待接收消息
        System.out.println("C2 等待接收消息处理时间8S，预期值5");
        
        // 定义消息接收的回调
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            // 从消息载体中获取消息内容
            String message = new String(delivery.getBody());
            try {
                // 模拟消息处理时间，此处为1秒
                Thread.sleep(8000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // 控制台打印接收到的消息
            System.out.println("接收到消息:" + message);
            // 手动发送ACK（消息确认），通知RabbitMQ消息已被处理
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
        };

        // 定义取消消费的回调
        CancelCallback cancelCallback = consumerTag -> System.out.println(consumerTag + "消费者取消消费接口回调逻辑");
        
        // 设置预取计数为1。这意味着RabbitMQ在消费者发送ack之前，不会将多于一个消息分发给该消费者
        int prefetchCount = 5;
        channel.basicQos(prefetchCount);

        // 关闭自动应答模式，开启手动应答。这是不公平分发策略的关键设置
        boolean autoAck = false;
        // 开始消费消息
        channel.basicConsume(TASK_QUEUE_NAME, autoAck, deliverCallback, cancelCallback);
    }
}
