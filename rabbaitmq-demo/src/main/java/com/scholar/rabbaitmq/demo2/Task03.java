package com.scholar.rabbaitmq.demo2;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MessageProperties;

import java.util.Scanner;

public class Task03 {
    private static final String TASK_QUEUE_NAME = "ack_queue"; // 队列名称

    public static void main(String[] args) throws Exception {
        Channel channel = RabbitMQUtils.getChannel();
        // 让队列持久化
        boolean durable = true;
        
        // 声明队列时，将durable参数设置为true，开启持久化
        channel.queueDeclare(TASK_QUEUE_NAME, durable, false, false, null);
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入信息：");
        while (sc.hasNext()) {
            String message = sc.nextLine();
            // 发布消息
            channel.basicPublish("", TASK_QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes("UTF-8"));
            System.out.println("生产者发出消息：" + message);
        }
    }
}