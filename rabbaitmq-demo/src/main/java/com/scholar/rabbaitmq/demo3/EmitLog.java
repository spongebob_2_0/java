package com.scholar.rabbaitmq.demo3;

import com.rabbitmq.client.Channel;
import com.scholar.rabbaitmq.demo2.RabbitMQUtils;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

/**
 * 生产者 - EmitLog
 * 使用扇出交换机发送日志消息。
 */
public class EmitLog {
    // 定义扇出交换机的名称
    private static final String EXCHANGE_NAME = "logs";
    
    public static void main(String[] args) throws IOException, TimeoutException {
        // 通过工具类获取与RabbitMQ服务的连接通道
        Channel channel = RabbitMQUtils.getChannel();
        /**
         * 声明（创建）一个扇出类型的交换机
         * 参数1: 交换机名称
         * 参数2: 交换机类型 - fanout 表示扇出交换机
         */
        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

        // 创建一个扫描器对象，用于接收控制台输入的消息
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入信息：");
        // 循环读取控制台输入的消息
        while (sc.hasNext()) {
            // 读取一行输入作为消息
            String message = sc.nextLine();
            /**
             * 将消息发布到扇出交换机
             * 参数1: 交换机名称
             * 参数2: 路由键 - 在扇出交换机中，此参数被忽略
             * 参数3: 消息的其他属性 - 如消息头等
             * 参数4: 消息内容字节数组
             */
            channel.basicPublish(EXCHANGE_NAME, "", null, message.getBytes("UTF-8"));
            // 控制台输出发送的消息
            System.out.println("生产者发出消息：" + message);
        }
    }
}