package com.scholar.rabbaitmq.demo3;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import com.scholar.rabbaitmq.demo2.RabbitMQUtils;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 消费者 ReceiveLogs01
 * 从扇出交换机接收消息并打印到控制台。
 */
public class ReceiveLogs01 {
    // 交换机名称
    private static final String EXCHANGE_NAME = "logs";

    public static void main(String[] args) throws IOException, TimeoutException {
        // 获取RabbitMQ连接通道
        Channel channel = RabbitMQUtils.getChannel();
        // 声明扇出类型的交换机
        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        /**
         * 创建一个临时队列，队列名称是随机的。
         * 这种队列是非持久的、独占的、自动删除的，主要用于此类临时场景。
         */
        String queueName = channel.queueDeclare().getQueue();
        // 将临时队列绑定到指定的交换机上，无需指定路由键（fanout交换机不需要路由键）
        channel.queueBind(queueName, EXCHANGE_NAME, "");
        System.out.println("等待接收消息,把接收到的消息打印在屏幕...........");

        // 消息到达的回调函数
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            // 在控制台打印接收到的消息
            System.out.println("控制台打印接收到的消息：" + message);
        };
        // 开始监听队列上的消息
        channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {});
    }
}