package com.scholar.rabbaitmq.demo3;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import com.scholar.rabbaitmq.demo2.RabbitMQUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 消费者2 - ReceiveLogs02
 * 从扇出交换机接收消息，并将消息内容写入到指定文件中。
 */
public class ReceiveLogs02 {
    // 扇出交换机的名称
    private static final String EXCHANGE_NAME = "logs";

    public static void main(String[] args) throws IOException, TimeoutException {
        // 获取与RabbitMQ服务器的连接通道
        Channel channel = RabbitMQUtils.getChannel();
        // 声明一个扇出类型的交换机
        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        /**
         * 创建一个临时队列，队列名称是随机的。
         * 这种队列是非持久的、独占的、且断开连接时自动删除的。
         */
        String queueName = channel.queueDeclare().getQueue();
        // 将临时队列绑定到扇出交换机上，路由键为空字符串（因为扇出交换机会忽略路由键）
        channel.queueBind(queueName, EXCHANGE_NAME, "");
        System.out.println("等待接收消息,把接收到的消息写到文件...........");

        // 定义接收消息的回调
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            // 指定消息写入的文件
            File file = new File("D:\\rabbitmq_info.txt");
            if(!file.exists()) {
                file.createNewFile();
            }
            // 使用Apache Commons IO库将消息写入文件
            FileUtils.writeStringToFile(file, message, "UTF-8", true);
            System.out.println("数据写入文件成功");
        };
        // 开始接收消息。自动消息确认打开。
        channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {});
    }
}