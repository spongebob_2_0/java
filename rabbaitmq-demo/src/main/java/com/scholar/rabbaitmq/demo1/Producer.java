package com.scholar.rabbaitmq.demo1;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Producer {
    
    // 定义队列名称
    public static final String QUEUE_NAME = "queue1";

    public static void main(String[] args) throws IOException, TimeoutException {
        // 1. 创建连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 2. 配置RabbitMQ连接信息
        connectionFactory.setHost("106.14.91.138"); // 设置RabbitMQ服务地址
        connectionFactory.setPort(5672); // 设置RabbitMQ服务端口
        connectionFactory.setUsername("root"); // 设置访问RabbitMQ的用户名
        connectionFactory.setPassword("183193"); // 设置访问密码
        // 3. 创建连接
        Connection connection = connectionFactory.newConnection();
        // 4. 创建信道
        Channel channel = connection.createChannel();
        // 5. 声明（创建）队列
        // 数依次是队列名、是否持久化、是否独占队列、是否自动删除和其他参数
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        // 准备消息内容
        String message = "Hello RabbitMQ";
        // 6. 发送消息到队列
        // 参数依次是交换机名、队列名、消息的其他属性、消息内容的字节数组
        channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
        // 打印消息已发送的提示信息
        System.out.println("消息发送完毕");
    }
}