package com.scholar.rabbaitmq.demo1;

import com.rabbitmq.client.*;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Consumer {
    // 队列名称
    public static final String QUEUE_NAME = "queue1";

    public static void main(String[] args) throws IOException, TimeoutException {
        // 1. 创建连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 2. 设置RabbitMQ服务地址和认证信息
        connectionFactory.setHost("106.14.91.138"); // 设置RabbitMQ服务地址
        connectionFactory.setPort(5672); // 设置RabbitMQ服务端口
        connectionFactory.setUsername("root"); // 设置访问RabbitMQ的用户名
        connectionFactory.setPassword("183193"); // 设置访问密码
        // 3. 创建连接
        Connection connection = connectionFactory.newConnection();
        // 4. 创建信道
        Channel channel = connection.createChannel();
        System.out.println("等待接收消息......");

        // 5. 设置消息接收回调
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody());
            System.out.println("接收到的消息：" + message);
        };
        
        // 6. 设置取消消费的回调
        CancelCallback cancelCallback = consumerTag -> {
            System.out.println("消息消费被中断");
        };
        
        // 7. 开始消费消息
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, cancelCallback);
    }
}