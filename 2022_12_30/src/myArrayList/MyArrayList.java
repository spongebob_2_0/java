package myArrayList;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-31
 * Time: 4:22
 */
public class MyArrayList {
    public int[] elem;
    public int usedSize;

    public MyArrayList() {
        this.elem = new int[5];
    }

    public void disPlay() {
        for (int i = 0; i < this.usedSize; i++) {
            System.out.print(elem[i] + " ");
        }
        System.out.println();
    }

    //判断坐标位置是否合法
    public void indexSub(int index) throws IndexOfException {
        if (index < 0 || index > usedSize) {
            throw new IndexOfException("位置不合法!");
        }
    }

    //判断坐标位置是否合法
    public void indexSub2(int index) throws IndexOfException {
        if (index < 0 || index >= usedSize) {
            throw new IndexOfException("位置不合法!");
        }
    }

    //判断是否满了
    public boolean isFUll() {
        return elem.length == this.usedSize;
    }

    //扩容
    public int[] expansion(int[] elem) {
        this.elem = Arrays.copyOf(elem, elem.length * 2);
        return elem;
    }

    public void add(int data) {
        if (isFUll()) {
            expansion(elem);
        }
        this.elem[usedSize] = data;
        usedSize++;
    }

    public void add(int pos, int data) {
        indexSub(pos);
        if (isFUll()) {
            expansion(elem);
        }
        for (int i = usedSize - 1; i >= pos; i--) {
            elem[i + 1] = elem[i];
        }
        elem[pos] = data;
        usedSize++;
    }

    public boolean contains(int toFind) {
        for (int i = 0; i < this.usedSize; i++) {
            if (toFind == elem[i]) {
                return true;
            }
        }
        return false;
    }
    public int indexOf(int toFind) {
        for (int i = 0; i < this.usedSize; i++) {
            if (toFind == elem[i]) {
                return i;
            }
        }
        return -1;
    }
    public int get(int pos){
        indexSub2(pos);
        return elem[pos];
    }
    public void set(int pos,int value){
        indexSub(pos);
        this.elem[pos] =value;
    }

    public void remove(int key){
        int index =indexOf(key);
        if(index ==-1){
            System.out.println("没有这个数据");
            return;
        }
        for (int i = index; i < usedSize-1; i++) {
            elem[i] =elem[i+1];
        }
        usedSize--;
        elem[usedSize] =0;
    }
    public int size(){
        return usedSize;
    }
    public void clear(){
        this.usedSize =0;
    }
}