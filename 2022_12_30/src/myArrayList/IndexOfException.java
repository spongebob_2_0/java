package myArrayList;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-31
 * Time: 4:32
 */
public class IndexOfException extends RuntimeException{
    public IndexOfException() {
    }

    public IndexOfException(String message) {
        super(message);
    }
}
