package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-31
 * Time: 19:10
 */
public class Test3 {
    public static void LeftRotateString(String str,int n){
        String ret =str.substring(n);
        for (int i = n-1; i >=0 ; i--) {
             ret =ret+str.charAt(i);
        }
        System.out.println(ret);
    }
    public static void main(String[] args) {
        String str ="abcdef";
        LeftRotateString(str,4);
    }
}
