package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-31
 * Time: 18:47
 */
public class Test2 {
    public static void fnid_val(int[][] array,int row,int col,int num){
        int i =0;   //代表第0行
        int j =col;  //代表最后一列
        while(i<=row && j>=0){
            if(array[i][j] < num){
                i++;
            }else if(array[i][j] > num){
                j--;
            }else{
                System.out.println("该数组中含有"+num);
                return;
            }
        }
        System.out.println("该数组中不含有"+num);
    }
    public static void main(String[] args) {
        int [][]array = {
                {1,2,8,9},
                {2,4,9,12},
                {4,7,10,13},
                {6,8,11,15}
        };
        fnid_val(array,3,3,10);
    }
}
