package test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-31
 * Time: 19:44
 */
public class Test {
    public static String func(String s1, String s2) {
        for (int i = 0; i < s2.length(); i++) {
            String ret1 = s2.charAt(i) + "";
            if (s1.contains(ret1)) {
                s1 =s1.replaceAll(ret1, "");
            }
        }
        return s1;
    }

    public static void main(String[] args) {
        String s1 = "welcome to bit";
        String s2 = "come";
        String ret = func(s1, s2);
        System.out.println(ret);
    }
}
