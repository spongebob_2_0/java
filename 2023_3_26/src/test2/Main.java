package test2;

import java.util.ArrayList;
import java.util.Scanner;

// 定义一个Book类，表示图书
class Book {
    private String title;  // 标题
    private String author;  // 作者
    private String publisher;  // 出版社
    private int year;  // 出版年份

    // 构造函数，用于创建Book对象
    public Book(String title, String author, String publisher, int year) {
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.year = year;
    }

    // 获取图书的标题
    public String getTitle() {
        return title;
    }

    // 获取图书的作者
    public String getAuthor() {
        return author;
    }

    // 获取图书的出版社
    public String getPublisher() {
        return publisher;
    }

    // 获取图书的出版年份
    public int getYear() {
        return year;
    }

    // 重写toString方法，用于输出Book对象的信息
    @Override
    public String toString() {
        return "Title: " + title + ", Author: " + author + ", Publisher: " + publisher + ", Year: " + year;
    }
}

// 定义一个BookManager类，用于管理图书
class BookManager {
    private ArrayList<Book> books;  // 用ArrayList保存图书

    // 构造函数，用于创建BookManager对象
    public BookManager() {
        books = new ArrayList<>();
    }

    // 添加一本图书
    public void addBook(Book book) {
        books.add(book);
    }

    // 删除一本图书
    public void removeBook(Book book) {
        books.remove(book);
    }

    // 根据标题查找图书
    public ArrayList<Book> searchByTitle(String title) {
        ArrayList<Book> result = new ArrayList<>();
        for (Book book : books) {
            if (book.getTitle().equals(title)) {
                result.add(book);
            }
        }
        return result;
    }

    // 根据作者查找图书
    public ArrayList<Book> searchByAuthor(String author) {
        ArrayList<Book> result = new ArrayList<>();
        for (Book book : books) {
            if (book.getAuthor().equals(author)) {
                result.add(book);
            }
        }
        return result;
    }

    // 根据出版社查找图书
    public ArrayList<Book> searchByPublisher(String publisher) {
        ArrayList<Book> result = new ArrayList<>();
        for (Book book : books) {
            if (book.getPublisher().equals(publisher)) {
                result.add(book);
            }
        }
        return result;
    }

    // 根据出版年份查找图书
    public ArrayList<Book> searchByYear(int year) {
        ArrayList<Book> result = new ArrayList<>();
        for (Book book : books) {
            if (book.getYear() == year) {
                result.add(book);
            }
        }
        return result;
    }

    // 输出所有图书的信息
    public void displayAllBooks() {
        for (Book book : books) {
            System.out.println(book);
        }
    }
}

// 主程序
public class Main {
    public static void main(String[] args) {
        BookManager manager = new BookManager();  // 创建一个BookManager对象

        // 添加一些图书
        // 这里添加了5本图书，可以根据需要修改
        manager.addBook(new Book("The Great Gatsby", "F. Scott Fitzgerald", "Scribner", 1925));
        manager.addBook(new Book("To Kill a Mockingbird", "Harper Lee", "J. B. Lippincott & Co.", 1960));
        manager.addBook(new Book("1984", "George Orwell", "Secker & Warburg", 1949));
        manager.addBook(new Book("Pride and Prejudice", "Jane Austen", "T. Egerton, Whitehall", 1813));
        manager.addBook(new Book("The Catcher in the Rye", "J. D. Salinger", "Little, Brown and Company", 1951));
        Scanner scanner = new Scanner(System.in);

        while (true) {
            // 显示菜单
            System.out.println("1. Add a book");
            System.out.println("2. Remove a book");
            System.out.println("3. Search for a book by title");
            System.out.println("4. Search for a book by author");
            System.out.println("5. Search for a book by publisher");
            System.out.println("6. Search for a book by year");
            System.out.println("7. Display all books");
            System.out.println("0. Exit");

            // 获取用户输入
            int choice = scanner.nextInt();

            if (choice == 1) {
                // 添加图书
                System.out.println("Enter the title:");
                String title = scanner.next();
                System.out.println("Enter the author:");
                String author = scanner.next();
                System.out.println("Enter the publisher:");
                String publisher = scanner.next();
                System.out.println("Enter the year:");
                int year = scanner.nextInt();
                manager.addBook(new Book(title, author, publisher, year));
            } else if (choice == 2) {
                // 删除图书
                System.out.println("Enter the title:");
                String title = scanner.next();
                ArrayList<Book> result = manager.searchByTitle(title);
                if (result.size() == 0) {
                    System.out.println("No such book.");
                } else if (result.size() == 1) {
                    manager.removeBook(result.get(0));
                    System.out.println("Book removed.");
                } else {
                    System.out.println("Multiple books found:");
                    for (Book book : result) {
                        System.out.println(book);
                    }
                    System.out.println("Please enter the author of the book you want to remove:");
                    String author = scanner.next();
                    for (Book book : result) {
                        if (book.getAuthor().equals(author)) {
                            manager.removeBook(book);
                            System.out.println("Book removed.");
                            break;
                        }
                    }
                }
            } else if (choice == 3) {
                // 根据标题查找图书
                System.out.println("Enter the title:");
                String title = scanner.next();
                ArrayList<Book> result = manager.searchByTitle(title);
                if (result.size() == 0) {
                    System.out.println("No such book.");
                } else {
                    for (Book book : result) {
                        System.out.println(book);
                    }
                }
            } else if (choice == 4) {
                // 根据作者查找图书
                System.out.println("Enter the author:");
                String author = scanner.next();
                ArrayList<Book> result = manager.searchByAuthor(author);
                if (result.size() == 0) {
                    System.out.println("No such book.");
                } else {
                    for (Book book : result) {
// 输出符合条件的所有图书信息
                        System.out.println(book);
                    }
                }
            } else if (choice == 5) {
// 根据出版社查找图书
                System.out.println("Enter the publisher:");
                String publisher = scanner.next();
                ArrayList<Book> result = manager.searchByPublisher(publisher);
                if (result.size() == 0) {
                    System.out.println("No such book.");
                } else {
                    for (Book book : result) {
                        System.out.println(book);
                    }
                }
            } else if (choice == 6) {
// 根据出版年份查找图书
                System.out.println("Enter the year:");
                int year = scanner.nextInt();
                ArrayList<Book> result = manager.searchByYear(year);
                if (result.size() == 0) {
                    System.out.println("No such book.");
                } else {
                    for (Book book : result) {
                        System.out.println(book);
                    }
                }
            } else if (choice == 7) {
// 显示所有图书信息
                manager.displayAllBooks();
            } else if (choice == 0) {
// 退出程序
                System.out.println("Goodbye!");
                break;
            } else {
                System.out.println("Invalid choice. Please try again.");
            }
        }
    }
}