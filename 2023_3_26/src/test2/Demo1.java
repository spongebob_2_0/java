package test2;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-26
 * Time: 22:02
 */
public class Demo1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()){
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            //行
            //记录第一行的蛋糕数量
            int sum1 = 0;
            //看图可以发现规律,如果第一行值是sum1那么第三四行的值就是x-sum1,处于互补关系
            int sum2 = 0;
            //记录蛋糕总数
            int sum = 0;
            //用于判断
            //
            int count = 0;
            //这个for循环是求第一个sum1的值
            for (int i = 0; i < x ; i++) {
                count++;
                //为1就count++,为2就count++
                if(count == 1 || count == 2){
                    sum1++;
                }
                //count为3,不加加,什么也不做
                //count为4,不加加,将count置为0
                if (count == 4){
                    count = 0;
                    //置为0后count又从0开始计数
                }
            }
            //求的是sum2的值
            sum2 = x-sum1;
            count = 0;
            for (int i = 0; i < y; i++) {
                //以就是跟上述一样的逻辑
                count++;
                if(count == 1 || count == 2){
                    //此时加的是sum1
                    //count为1.2时才+sum1
                    sum+=sum1;
                }

                if(count == 3||count == 4){
                    //当count为3,4时才+sum2
                    sum+=sum2;
                }
                if (count == 4){
                    //依旧是4个值一个循环
                    count = 0;
                }
            }
            //此时就是所有蛋糕的值

            System.out.println(sum);
        }

    }
}
