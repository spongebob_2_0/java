package test3;

import java.util.Scanner;

public class 图书管理系统 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Book[] books = new Book[100]; // 定义一个最多存储100本书的数组

        int count = 0; // 记录已经存储的书的数量

        while (true) {
            // 显示菜单
            System.out.println("欢迎来到图书管理系统，请选择功能：");
            System.out.println("1. 添加图书");
            System.out.println("2. 查看所有图书");
            System.out.println("3. 按照书名查找图书");
            System.out.println("4. 按照作者查找图书");
            System.out.println("5. 借阅图书");
            System.out.println("6. 归还图书");
            System.out.println("7. 退出系统");

            int choice = input.nextInt(); // 获取用户的选择

            switch (choice) {
                case 1:
                    addBook(input, books, count);
                    count++;
                    break;
                case 2:
                    showAllBooks(books, count);
                    break;
                case 3:
                    searchBookByName(input, books, count);
                    break;
                case 4:
                    searchBookByAuthor(input, books, count);
                    break;
                case 5:
                    borrowBook(input, books, count);
                    break;
                case 6:
                    returnBook(input, books, count);
                    break;
                case 7:
                    System.out.println("谢谢使用图书管理系统，再见！");
                    System.exit(0); // 退出程序
                    break;
                default:
                    System.out.println("输入无效，请重新输入。");
            }
        }
    }

    // 添加图书
    private static void addBook(Scanner input, Book[] books, int count) {
        System.out.println("请输入书名：");
        String name = input.next();
        System.out.println("请输入作者：");
        String author = input.next();
        System.out.println("请输入出版社：");
        String press = input.next();
        System.out.println("请输入价格：");
        double price = input.nextDouble();

        Book book = new Book(name, author, press, price);
        books[count] = book;

        System.out.println("添加成功！");
    }

    // 查看所有图书
    private static void showAllBooks(Book[] books, int count) {
        if (count == 0) {
            System.out.println("暂无图书。");
        } else {
            System.out.println("所有图书：");
            for (int i = 0; i < count; i++) {
                System.out.println(books[i]);
            }
        }
    }

    // 按照书名查找图书
    private static void searchBookByName(Scanner input, Book[] books, int count) {
        System.out.println("请输入书名：");
        String name = input.next();

        for (int i = 0; i < count; i++) {
            if (books[i].getName().equals(name)) {
                System.out.println(books[i]);
            }
        }
    }

    // 按照作者查找图书
    private static void searchBookByAuthor(Scanner input, Book[] books, int count) {
        System.out.println("请输入作者：");
        String author = input.next();

        for (int i = 0; i < count; i++) {
            if (books[i].getAuthor().equals(author)) {
                System.out.println(books[i]);
            }
        }
    }

    // 借阅图书
    private static void borrowBook(Scanner input, Book[] books, int count) {
        System.out.println("请输入书名：");
        String name = input.next();

        for (int i = 0; i < count; i++) {
            if (books[i].getName().equals(name)) {
                if (books[i].isBorrowed()) {
                    System.out.println("这本书已经被借出去了。");
                } else {
                    books[i].setBorrowed(true);
                    System.out.println("借阅成功！");
                }
                return;
            }
        }

        System.out.println("没有找到这本书。");
    }

    // 归还图书
    private static void returnBook(Scanner input, Book[] books, int count) {
        System.out.println("请输入书名：");
        String name = input.next();

        for (int i = 0; i < count; i++) {
            if (books[i].getName().equals(name)) {
                if (books[i].isBorrowed()) {
                    books[i].setBorrowed(false);
                    System.out.println("归还成功！");
                } else {
                    System.out.println("这本书没有被借出去，无需归还。");
                }
                return;
            }
        }

        System.out.println("没有找到这本书。");
    }
}

class Book {
    private String name;
    private String author;
    private String press;
    private double price;
    private boolean borrowed;

    public Book(String name, String author, String press, double price) {
        this.name = name;
        this.author = author;
        this.press = press;
        this.price = price;
        this.borrowed = false;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getPress() {
        return press;
    }

    public double getPrice() {
        return price;
    }

    public boolean isBorrowed() {
        return borrowed;
    }

    public void setBorrowed(boolean borrowed) {
        this.borrowed = borrowed;
    }

    @Override
    public String toString() {
        String status = borrowed ? "已借出" : "未借出";
        return "书名：" + name + "，作者：" + author + "，出版社：" + press + "，价格：" + price + "，状态：" + status;
    }
}