package com.mycnblog.controller;

import com.mycnblog.entity.po.Videoinfo;
import com.mycnblog.entity.query.VideoinfoQuery;
import com.mycnblog.entity.vo.ResponseVO;
import com.mycnblog.service.VideoinfoService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 *  Controller
 */
@RestController("videoinfoController")
@RequestMapping("/videoinfo")
public class VideoinfoController extends ABaseController{

	@Resource
	private VideoinfoService videoinfoService;
	/**
	 * 根据条件分页查询
	 */
	@RequestMapping("/loadDataList")
	public ResponseVO loadDataList(VideoinfoQuery query){
		return getSuccessResponseVO(videoinfoService.findListByPage(query));
	}

	/**
	 * 新增
	 */
	@RequestMapping("/add")
	public ResponseVO add(Videoinfo bean) {
		videoinfoService.add(bean);
		return getSuccessResponseVO(null);
	}

	/**
	 * 批量新增
	 */
	@RequestMapping("/addBatch")
	public ResponseVO addBatch(@RequestBody List<Videoinfo> listBean) {
		videoinfoService.addBatch(listBean);
		return getSuccessResponseVO(null);
	}

	/**
	 * 批量新增/修改
	 */
	@RequestMapping("/addOrUpdateBatch")
	public ResponseVO addOrUpdateBatch(@RequestBody List<Videoinfo> listBean) {
		videoinfoService.addBatch(listBean);
		return getSuccessResponseVO(null);
	}

	/**
	 * 根据Vid查询对象
	 */
	@RequestMapping("/getVideoinfoByVid")
	public ResponseVO getVideoinfoByVid(Integer vid) {
		return getSuccessResponseVO(videoinfoService.getVideoinfoByVid(vid));
	}

	/**
	 * 根据Vid修改对象
	 */
	@RequestMapping("/updateVideoinfoByVid")
	public ResponseVO updateVideoinfoByVid(Videoinfo bean,Integer vid) {
		videoinfoService.updateVideoinfoByVid(bean,vid);
		return getSuccessResponseVO(null);
	}

	/**
	 * 根据Vid删除
	 */
	@RequestMapping("/deleteVideoinfoByVid")
	public ResponseVO deleteVideoinfoByVid(Integer vid) {
		videoinfoService.deleteVideoinfoByVid(vid);
		return getSuccessResponseVO(null);
	}
}