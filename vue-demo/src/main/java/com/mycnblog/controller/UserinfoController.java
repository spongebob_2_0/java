package com.mycnblog.controller;

import com.mycnblog.entity.po.Userinfo;
import com.mycnblog.entity.query.UserinfoQuery;
import com.mycnblog.entity.vo.ResponseVO;
import com.mycnblog.service.UserinfoService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 *  Controller
 */
@RestController("userinfoController")
@RequestMapping("/userinfo")
public class UserinfoController extends ABaseController{

	@Resource
	private UserinfoService userinfoService;
	/**
	 * 根据条件分页查询
	 */
	@RequestMapping("/loadDataList")
	public ResponseVO loadDataList(UserinfoQuery query){
		return getSuccessResponseVO(userinfoService.findListByPage(query));
	}

	/**
	 * 新增
	 */
	@RequestMapping("/add")
	public ResponseVO add(Userinfo bean) {
		userinfoService.add(bean);
		return getSuccessResponseVO(null);
	}

	/**
	 * 批量新增
	 */
	@RequestMapping("/addBatch")
	public ResponseVO addBatch(@RequestBody List<Userinfo> listBean) {
		userinfoService.addBatch(listBean);
		return getSuccessResponseVO(null);
	}

	/**
	 * 批量新增/修改
	 */
	@RequestMapping("/addOrUpdateBatch")
	public ResponseVO addOrUpdateBatch(@RequestBody List<Userinfo> listBean) {
		userinfoService.addBatch(listBean);
		return getSuccessResponseVO(null);
	}

	/**
	 * 根据Id查询对象
	 */
	@RequestMapping("/getUserinfoById")
	public ResponseVO getUserinfoById(Integer id) {
		return getSuccessResponseVO(userinfoService.getUserinfoById(id));
	}

	/**
	 * 根据Id修改对象
	 */
	@RequestMapping("/updateUserinfoById")
	public ResponseVO updateUserinfoById(Userinfo bean,Integer id) {
		userinfoService.updateUserinfoById(bean,id);
		return getSuccessResponseVO(null);
	}

	/**
	 * 根据Id删除
	 */
	@RequestMapping("/deleteUserinfoById")
	public ResponseVO deleteUserinfoById(Integer id) {
		userinfoService.deleteUserinfoById(id);
		return getSuccessResponseVO(null);
	}
}