package com.mycnblog.controller;

import com.mycnblog.entity.po.Articleinfo;
import com.mycnblog.entity.query.ArticleinfoQuery;
import com.mycnblog.entity.vo.ResponseVO;
import com.mycnblog.service.ArticleinfoService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 *  Controller
 */
@RestController("articleinfoController")
@RequestMapping("/articleinfo")
public class ArticleinfoController extends ABaseController{

	@Resource
	private ArticleinfoService articleinfoService;
	/**
	 * 根据条件分页查询
	 */
	@RequestMapping("/loadDataList")
	public ResponseVO loadDataList(ArticleinfoQuery query){
		return getSuccessResponseVO(articleinfoService.findListByPage(query));
	}

	/**
	 * 新增
	 */
	@RequestMapping("/add")
	public ResponseVO add(Articleinfo bean) {
		articleinfoService.add(bean);
		return getSuccessResponseVO(null);
	}

	/**
	 * 批量新增
	 */
	@RequestMapping("/addBatch")
	public ResponseVO addBatch(@RequestBody List<Articleinfo> listBean) {
		articleinfoService.addBatch(listBean);
		return getSuccessResponseVO(null);
	}

	/**
	 * 批量新增/修改
	 */
	@RequestMapping("/addOrUpdateBatch")
	public ResponseVO addOrUpdateBatch(@RequestBody List<Articleinfo> listBean) {
		articleinfoService.addBatch(listBean);
		return getSuccessResponseVO(null);
	}

	/**
	 * 根据Id查询对象
	 */
	@RequestMapping("/getArticleinfoById")
	public ResponseVO getArticleinfoById(Integer id) {
		return getSuccessResponseVO(articleinfoService.getArticleinfoById(id));
	}

	/**
	 * 根据Id修改对象
	 */
	@RequestMapping("/updateArticleinfoById")
	public ResponseVO updateArticleinfoById(Articleinfo bean,Integer id) {
		articleinfoService.updateArticleinfoById(bean,id);
		return getSuccessResponseVO(null);
	}

	/**
	 * 根据Id删除
	 */
	@RequestMapping("/deleteArticleinfoById")
	public ResponseVO deleteArticleinfoById(Integer id) {
		articleinfoService.deleteArticleinfoById(id);
		return getSuccessResponseVO(null);
	}
}