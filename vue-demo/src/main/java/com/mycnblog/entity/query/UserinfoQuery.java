package com.mycnblog.entity.query;


/**
 * 参数
 */
public class UserinfoQuery extends BaseParam {


	/**
	 * 
	 */
	private Integer id;

	/**
	 * 
	 */
	private String username;

	private String usernameFuzzy;

	/**
	 * 
	 */
	private String password;

	private String passwordFuzzy;

	/**
	 * 
	 */
	private String photo;

	private String photoFuzzy;

	/**
	 * 
	 */
	private String createtime;

	private String createtimeStart;

	private String createtimeEnd;

	/**
	 * 
	 */
	private String updatetime;

	private String updatetimeStart;

	private String updatetimeEnd;

	/**
	 * 
	 */
	private Integer state;


	public void setId(Integer id){
		this.id = id;
	}

	public Integer getId(){
		return this.id;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return this.username;
	}

	public void setUsernameFuzzy(String usernameFuzzy){
		this.usernameFuzzy = usernameFuzzy;
	}

	public String getUsernameFuzzy(){
		return this.usernameFuzzy;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return this.password;
	}

	public void setPasswordFuzzy(String passwordFuzzy){
		this.passwordFuzzy = passwordFuzzy;
	}

	public String getPasswordFuzzy(){
		return this.passwordFuzzy;
	}

	public void setPhoto(String photo){
		this.photo = photo;
	}

	public String getPhoto(){
		return this.photo;
	}

	public void setPhotoFuzzy(String photoFuzzy){
		this.photoFuzzy = photoFuzzy;
	}

	public String getPhotoFuzzy(){
		return this.photoFuzzy;
	}

	public void setCreatetime(String createtime){
		this.createtime = createtime;
	}

	public String getCreatetime(){
		return this.createtime;
	}

	public void setCreatetimeStart(String createtimeStart){
		this.createtimeStart = createtimeStart;
	}

	public String getCreatetimeStart(){
		return this.createtimeStart;
	}
	public void setCreatetimeEnd(String createtimeEnd){
		this.createtimeEnd = createtimeEnd;
	}

	public String getCreatetimeEnd(){
		return this.createtimeEnd;
	}

	public void setUpdatetime(String updatetime){
		this.updatetime = updatetime;
	}

	public String getUpdatetime(){
		return this.updatetime;
	}

	public void setUpdatetimeStart(String updatetimeStart){
		this.updatetimeStart = updatetimeStart;
	}

	public String getUpdatetimeStart(){
		return this.updatetimeStart;
	}
	public void setUpdatetimeEnd(String updatetimeEnd){
		this.updatetimeEnd = updatetimeEnd;
	}

	public String getUpdatetimeEnd(){
		return this.updatetimeEnd;
	}

	public void setState(Integer state){
		this.state = state;
	}

	public Integer getState(){
		return this.state;
	}

}
