package com.mycnblog.entity.query;


/**
 * 参数
 */
public class ArticleinfoQuery extends BaseParam {


	/**
	 * 
	 */
	private Integer id;

	/**
	 * 
	 */
	private String title;

	private String titleFuzzy;

	/**
	 * 
	 */
	private String content;

	private String contentFuzzy;

	/**
	 * 
	 */
	private String createtime;

	private String createtimeStart;

	private String createtimeEnd;

	/**
	 * 
	 */
	private String updatetime;

	private String updatetimeStart;

	private String updatetimeEnd;

	/**
	 * 
	 */
	private Integer uid;

	/**
	 * 
	 */
	private Integer rcount;

	/**
	 * 
	 */
	private Integer state;


	public void setId(Integer id){
		this.id = id;
	}

	public Integer getId(){
		return this.id;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return this.title;
	}

	public void setTitleFuzzy(String titleFuzzy){
		this.titleFuzzy = titleFuzzy;
	}

	public String getTitleFuzzy(){
		return this.titleFuzzy;
	}

	public void setContent(String content){
		this.content = content;
	}

	public String getContent(){
		return this.content;
	}

	public void setContentFuzzy(String contentFuzzy){
		this.contentFuzzy = contentFuzzy;
	}

	public String getContentFuzzy(){
		return this.contentFuzzy;
	}

	public void setCreatetime(String createtime){
		this.createtime = createtime;
	}

	public String getCreatetime(){
		return this.createtime;
	}

	public void setCreatetimeStart(String createtimeStart){
		this.createtimeStart = createtimeStart;
	}

	public String getCreatetimeStart(){
		return this.createtimeStart;
	}
	public void setCreatetimeEnd(String createtimeEnd){
		this.createtimeEnd = createtimeEnd;
	}

	public String getCreatetimeEnd(){
		return this.createtimeEnd;
	}

	public void setUpdatetime(String updatetime){
		this.updatetime = updatetime;
	}

	public String getUpdatetime(){
		return this.updatetime;
	}

	public void setUpdatetimeStart(String updatetimeStart){
		this.updatetimeStart = updatetimeStart;
	}

	public String getUpdatetimeStart(){
		return this.updatetimeStart;
	}
	public void setUpdatetimeEnd(String updatetimeEnd){
		this.updatetimeEnd = updatetimeEnd;
	}

	public String getUpdatetimeEnd(){
		return this.updatetimeEnd;
	}

	public void setUid(Integer uid){
		this.uid = uid;
	}

	public Integer getUid(){
		return this.uid;
	}

	public void setRcount(Integer rcount){
		this.rcount = rcount;
	}

	public Integer getRcount(){
		return this.rcount;
	}

	public void setState(Integer state){
		this.state = state;
	}

	public Integer getState(){
		return this.state;
	}

}
