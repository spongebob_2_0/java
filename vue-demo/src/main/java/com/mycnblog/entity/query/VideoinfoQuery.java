package com.mycnblog.entity.query;


/**
 * 参数
 */
public class VideoinfoQuery extends BaseParam {


	/**
	 * 
	 */
	private Integer vid;

	/**
	 * 
	 */
	private String title;

	private String titleFuzzy;

	/**
	 * 
	 */
	private String url;

	private String urlFuzzy;

	/**
	 * 
	 */
	private String createtime;

	private String createtimeStart;

	private String createtimeEnd;

	/**
	 * 
	 */
	private String updatetime;

	private String updatetimeStart;

	private String updatetimeEnd;

	/**
	 * 
	 */
	private Integer uid;


	public void setVid(Integer vid){
		this.vid = vid;
	}

	public Integer getVid(){
		return this.vid;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return this.title;
	}

	public void setTitleFuzzy(String titleFuzzy){
		this.titleFuzzy = titleFuzzy;
	}

	public String getTitleFuzzy(){
		return this.titleFuzzy;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return this.url;
	}

	public void setUrlFuzzy(String urlFuzzy){
		this.urlFuzzy = urlFuzzy;
	}

	public String getUrlFuzzy(){
		return this.urlFuzzy;
	}

	public void setCreatetime(String createtime){
		this.createtime = createtime;
	}

	public String getCreatetime(){
		return this.createtime;
	}

	public void setCreatetimeStart(String createtimeStart){
		this.createtimeStart = createtimeStart;
	}

	public String getCreatetimeStart(){
		return this.createtimeStart;
	}
	public void setCreatetimeEnd(String createtimeEnd){
		this.createtimeEnd = createtimeEnd;
	}

	public String getCreatetimeEnd(){
		return this.createtimeEnd;
	}

	public void setUpdatetime(String updatetime){
		this.updatetime = updatetime;
	}

	public String getUpdatetime(){
		return this.updatetime;
	}

	public void setUpdatetimeStart(String updatetimeStart){
		this.updatetimeStart = updatetimeStart;
	}

	public String getUpdatetimeStart(){
		return this.updatetimeStart;
	}
	public void setUpdatetimeEnd(String updatetimeEnd){
		this.updatetimeEnd = updatetimeEnd;
	}

	public String getUpdatetimeEnd(){
		return this.updatetimeEnd;
	}

	public void setUid(Integer uid){
		this.uid = uid;
	}

	public Integer getUid(){
		return this.uid;
	}

}
