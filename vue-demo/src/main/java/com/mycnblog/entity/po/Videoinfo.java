package com.mycnblog.entity.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mycnblog.entity.enums.DateTimePatternEnum;
import com.mycnblog.utils.DateUtil;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 */
public class Videoinfo implements Serializable {


	/**
	 * 
	 */
	private Integer vid;

	/**
	 * 
	 */
	private String title;

	/**
	 * 
	 */
	private String url;

	/**
	 * 
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createtime;

	/**
	 * 
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updatetime;

	/**
	 * 
	 */
	private Integer uid;


	public void setVid(Integer vid){
		this.vid = vid;
	}

	public Integer getVid(){
		return this.vid;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return this.title;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return this.url;
	}

	public void setCreatetime(Date createtime){
		this.createtime = createtime;
	}

	public Date getCreatetime(){
		return this.createtime;
	}

	public void setUpdatetime(Date updatetime){
		this.updatetime = updatetime;
	}

	public Date getUpdatetime(){
		return this.updatetime;
	}

	public void setUid(Integer uid){
		this.uid = uid;
	}

	public Integer getUid(){
		return this.uid;
	}

	@Override
	public String toString (){
		return "vid:"+(vid == null ? "空" : vid)+"，title:"+(title == null ? "空" : title)+"，url:"+(url == null ? "空" : url)+"，createtime:"+(createtime == null ? "空" : DateUtil.format(createtime, DateTimePatternEnum.YYYY_MM_DD_HH_MM_SS.getPattern()))+"，updatetime:"+(updatetime == null ? "空" : DateUtil.format(updatetime, DateTimePatternEnum.YYYY_MM_DD_HH_MM_SS.getPattern()))+"，uid:"+(uid == null ? "空" : uid);
	}
}
