package com.mycnblog.entity.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mycnblog.entity.enums.DateTimePatternEnum;
import com.mycnblog.utils.DateUtil;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 */
public class Userinfo implements Serializable {


	/**
	 * 
	 */
	private Integer id;

	/**
	 * 
	 */
	private String username;

	/**
	 * 
	 */
	private String password;

	/**
	 * 
	 */
	private String photo;

	/**
	 * 
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createtime;

	/**
	 * 
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updatetime;

	/**
	 * 
	 */
	private Integer state;


	public void setId(Integer id){
		this.id = id;
	}

	public Integer getId(){
		return this.id;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return this.username;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return this.password;
	}

	public void setPhoto(String photo){
		this.photo = photo;
	}

	public String getPhoto(){
		return this.photo;
	}

	public void setCreatetime(Date createtime){
		this.createtime = createtime;
	}

	public Date getCreatetime(){
		return this.createtime;
	}

	public void setUpdatetime(Date updatetime){
		this.updatetime = updatetime;
	}

	public Date getUpdatetime(){
		return this.updatetime;
	}

	public void setState(Integer state){
		this.state = state;
	}

	public Integer getState(){
		return this.state;
	}

	@Override
	public String toString (){
		return "id:"+(id == null ? "空" : id)+"，username:"+(username == null ? "空" : username)+"，password:"+(password == null ? "空" : password)+"，photo:"+(photo == null ? "空" : photo)+"，createtime:"+(createtime == null ? "空" : DateUtil.format(createtime, DateTimePatternEnum.YYYY_MM_DD_HH_MM_SS.getPattern()))+"，updatetime:"+(updatetime == null ? "空" : DateUtil.format(updatetime, DateTimePatternEnum.YYYY_MM_DD_HH_MM_SS.getPattern()))+"，state:"+(state == null ? "空" : state);
	}
}
