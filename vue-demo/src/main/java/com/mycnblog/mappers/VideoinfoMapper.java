package com.mycnblog.mappers;

import org.apache.ibatis.annotations.Param;

/**
 *  数据库操作接口
 */
public interface VideoinfoMapper<T,P> extends BaseMapper<T,P> {

	/**
	 * 根据Vid更新
	 */
	 Integer updateByVid(@Param("bean") T t,@Param("vid") Integer vid);


	/**
	 * 根据Vid删除
	 */
	 Integer deleteByVid(@Param("vid") Integer vid);


	/**
	 * 根据Vid获取对象
	 */
	 T selectByVid(@Param("vid") Integer vid);


}
