package com.mycnblog.service;

import com.mycnblog.entity.po.Videoinfo;
import com.mycnblog.entity.query.VideoinfoQuery;
import com.mycnblog.entity.vo.PaginationResultVO;

import java.util.List;


/**
 *  业务接口
 */
public interface VideoinfoService {

	/**
	 * 根据条件查询列表
	 */
	List<Videoinfo> findListByParam(VideoinfoQuery param);

	/**
	 * 根据条件查询列表
	 */
	Integer findCountByParam(VideoinfoQuery param);

	/**
	 * 分页查询
	 */
	PaginationResultVO<Videoinfo> findListByPage(VideoinfoQuery param);

	/**
	 * 新增
	 */
	Integer add(Videoinfo bean);

	/**
	 * 批量新增
	 */
	Integer addBatch(List<Videoinfo> listBean);

	/**
	 * 批量新增/修改
	 */
	Integer addOrUpdateBatch(List<Videoinfo> listBean);

	/**
	 * 多条件更新
	 */
	Integer updateByParam(Videoinfo bean,VideoinfoQuery param);

	/**
	 * 多条件删除
	 */
	Integer deleteByParam(VideoinfoQuery param);

	/**
	 * 根据Vid查询对象
	 */
	Videoinfo getVideoinfoByVid(Integer vid);


	/**
	 * 根据Vid修改
	 */
	Integer updateVideoinfoByVid(Videoinfo bean,Integer vid);


	/**
	 * 根据Vid删除
	 */
	Integer deleteVideoinfoByVid(Integer vid);

}