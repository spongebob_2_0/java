package com.mycnblog.service;

import com.mycnblog.entity.po.Articleinfo;
import com.mycnblog.entity.query.ArticleinfoQuery;
import com.mycnblog.entity.vo.PaginationResultVO;

import java.util.List;


/**
 *  业务接口
 */
public interface ArticleinfoService {

	/**
	 * 根据条件查询列表
	 */
	List<Articleinfo> findListByParam(ArticleinfoQuery param);

	/**
	 * 根据条件查询列表
	 */
	Integer findCountByParam(ArticleinfoQuery param);

	/**
	 * 分页查询
	 */
	PaginationResultVO<Articleinfo> findListByPage(ArticleinfoQuery param);

	/**
	 * 新增
	 */
	Integer add(Articleinfo bean);

	/**
	 * 批量新增
	 */
	Integer addBatch(List<Articleinfo> listBean);

	/**
	 * 批量新增/修改
	 */
	Integer addOrUpdateBatch(List<Articleinfo> listBean);

	/**
	 * 多条件更新
	 */
	Integer updateByParam(Articleinfo bean,ArticleinfoQuery param);

	/**
	 * 多条件删除
	 */
	Integer deleteByParam(ArticleinfoQuery param);

	/**
	 * 根据Id查询对象
	 */
	Articleinfo getArticleinfoById(Integer id);


	/**
	 * 根据Id修改
	 */
	Integer updateArticleinfoById(Articleinfo bean,Integer id);


	/**
	 * 根据Id删除
	 */
	Integer deleteArticleinfoById(Integer id);

}