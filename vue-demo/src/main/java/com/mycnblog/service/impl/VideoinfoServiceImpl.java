package com.mycnblog.service.impl;

import com.mycnblog.entity.enums.PageSize;
import com.mycnblog.entity.po.Videoinfo;
import com.mycnblog.entity.query.SimplePage;
import com.mycnblog.entity.query.VideoinfoQuery;
import com.mycnblog.entity.vo.PaginationResultVO;
import com.mycnblog.mappers.VideoinfoMapper;
import com.mycnblog.service.VideoinfoService;
import com.mycnblog.utils.StringTools;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 *  业务接口实现
 */
@Service("videoinfoService")
public class VideoinfoServiceImpl implements VideoinfoService {

	@Resource
	private VideoinfoMapper<Videoinfo, VideoinfoQuery> videoinfoMapper;

	/**
	 * 根据条件查询列表
	 */
	@Override
	public List<Videoinfo> findListByParam(VideoinfoQuery param) {
		return this.videoinfoMapper.selectList(param);
	}

	/**
	 * 根据条件查询列表
	 */
	@Override
	public Integer findCountByParam(VideoinfoQuery param) {
		return this.videoinfoMapper.selectCount(param);
	}

	/**
	 * 分页查询方法
	 */
	@Override
	public PaginationResultVO<Videoinfo> findListByPage(VideoinfoQuery param) {
		int count = this.findCountByParam(param);
		int pageSize = param.getPageSize() == null ? PageSize.SIZE15.getSize() : param.getPageSize();

		SimplePage page = new SimplePage(param.getPageNo(), count, pageSize);
		param.setSimplePage(page);
		List<Videoinfo> list = this.findListByParam(param);
		PaginationResultVO<Videoinfo> result = new PaginationResultVO(count, page.getPageSize(), page.getPageNo(), page.getPageTotal(), list);
		return result;
	}

	/**
	 * 新增
	 */
	@Override
	public Integer add(Videoinfo bean) {
		return this.videoinfoMapper.insert(bean);
	}

	/**
	 * 批量新增
	 */
	@Override
	public Integer addBatch(List<Videoinfo> listBean) {
		if (listBean == null || listBean.isEmpty()) {
			return 0;
		}
		return this.videoinfoMapper.insertBatch(listBean);
	}

	/**
	 * 批量新增或者修改
	 */
	@Override
	public Integer addOrUpdateBatch(List<Videoinfo> listBean) {
		if (listBean == null || listBean.isEmpty()) {
			return 0;
		}
		return this.videoinfoMapper.insertOrUpdateBatch(listBean);
	}

	/**
	 * 多条件更新
	 */
	@Override
	public Integer updateByParam(Videoinfo bean, VideoinfoQuery param) {
		StringTools.checkParam(param);
		return this.videoinfoMapper.updateByParam(bean, param);
	}

	/**
	 * 多条件删除
	 */
	@Override
	public Integer deleteByParam(VideoinfoQuery param) {
		StringTools.checkParam(param);
		return this.videoinfoMapper.deleteByParam(param);
	}

	/**
	 * 根据Vid获取对象
	 */
	@Override
	public Videoinfo getVideoinfoByVid(Integer vid) {
		return this.videoinfoMapper.selectByVid(vid);
	}

	/**
	 * 根据Vid修改
	 */
	@Override
	public Integer updateVideoinfoByVid(Videoinfo bean, Integer vid) {
		return this.videoinfoMapper.updateByVid(bean, vid);
	}

	/**
	 * 根据Vid删除
	 */
	@Override
	public Integer deleteVideoinfoByVid(Integer vid) {
		return this.videoinfoMapper.deleteByVid(vid);
	}
}