package com.mycnblog.service.impl;

import com.mycnblog.entity.enums.PageSize;
import com.mycnblog.entity.po.Userinfo;
import com.mycnblog.entity.query.SimplePage;
import com.mycnblog.entity.query.UserinfoQuery;
import com.mycnblog.entity.vo.PaginationResultVO;
import com.mycnblog.mappers.UserinfoMapper;
import com.mycnblog.service.UserinfoService;
import com.mycnblog.utils.StringTools;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 *  业务接口实现
 */
@Service("userinfoService")
public class UserinfoServiceImpl implements UserinfoService {

	@Resource
	private UserinfoMapper<Userinfo, UserinfoQuery> userinfoMapper;

	/**
	 * 根据条件查询列表
	 */
	@Override
	public List<Userinfo> findListByParam(UserinfoQuery param) {
		return this.userinfoMapper.selectList(param);
	}

	/**
	 * 根据条件查询列表
	 */
	@Override
	public Integer findCountByParam(UserinfoQuery param) {
		return this.userinfoMapper.selectCount(param);
	}

	/**
	 * 分页查询方法
	 */
	@Override
	public PaginationResultVO<Userinfo> findListByPage(UserinfoQuery param) {
		int count = this.findCountByParam(param);
		int pageSize = param.getPageSize() == null ? PageSize.SIZE15.getSize() : param.getPageSize();

		SimplePage page = new SimplePage(param.getPageNo(), count, pageSize);
		param.setSimplePage(page);
		List<Userinfo> list = this.findListByParam(param);
		PaginationResultVO<Userinfo> result = new PaginationResultVO(count, page.getPageSize(), page.getPageNo(), page.getPageTotal(), list);
		return result;
	}

	/**
	 * 新增
	 */
	@Override
	public Integer add(Userinfo bean) {
		return this.userinfoMapper.insert(bean);
	}

	/**
	 * 批量新增
	 */
	@Override
	public Integer addBatch(List<Userinfo> listBean) {
		if (listBean == null || listBean.isEmpty()) {
			return 0;
		}
		return this.userinfoMapper.insertBatch(listBean);
	}

	/**
	 * 批量新增或者修改
	 */
	@Override
	public Integer addOrUpdateBatch(List<Userinfo> listBean) {
		if (listBean == null || listBean.isEmpty()) {
			return 0;
		}
		return this.userinfoMapper.insertOrUpdateBatch(listBean);
	}

	/**
	 * 多条件更新
	 */
	@Override
	public Integer updateByParam(Userinfo bean, UserinfoQuery param) {
		StringTools.checkParam(param);
		return this.userinfoMapper.updateByParam(bean, param);
	}

	/**
	 * 多条件删除
	 */
	@Override
	public Integer deleteByParam(UserinfoQuery param) {
		StringTools.checkParam(param);
		return this.userinfoMapper.deleteByParam(param);
	}

	/**
	 * 根据Id获取对象
	 */
	@Override
	public Userinfo getUserinfoById(Integer id) {
		return this.userinfoMapper.selectById(id);
	}

	/**
	 * 根据Id修改
	 */
	@Override
	public Integer updateUserinfoById(Userinfo bean, Integer id) {
		return this.userinfoMapper.updateById(bean, id);
	}

	/**
	 * 根据Id删除
	 */
	@Override
	public Integer deleteUserinfoById(Integer id) {
		return this.userinfoMapper.deleteById(id);
	}
}