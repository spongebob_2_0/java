package com.mycnblog.service.impl;

import com.mycnblog.entity.enums.PageSize;
import com.mycnblog.entity.po.Articleinfo;
import com.mycnblog.entity.query.ArticleinfoQuery;
import com.mycnblog.entity.query.SimplePage;
import com.mycnblog.entity.vo.PaginationResultVO;
import com.mycnblog.mappers.ArticleinfoMapper;
import com.mycnblog.service.ArticleinfoService;
import com.mycnblog.utils.StringTools;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 *  业务接口实现
 */
@Service("articleinfoService")
public class ArticleinfoServiceImpl implements ArticleinfoService {

	@Resource
	private ArticleinfoMapper<Articleinfo, ArticleinfoQuery> articleinfoMapper;

	/**
	 * 根据条件查询列表
	 */
	@Override
	public List<Articleinfo> findListByParam(ArticleinfoQuery param) {
		return this.articleinfoMapper.selectList(param);
	}

	/**
	 * 根据条件查询列表
	 */
	@Override
	public Integer findCountByParam(ArticleinfoQuery param) {
		return this.articleinfoMapper.selectCount(param);
	}

	/**
	 * 分页查询方法
	 */
	@Override
	public PaginationResultVO<Articleinfo> findListByPage(ArticleinfoQuery param) {
		int count = this.findCountByParam(param);
		int pageSize = param.getPageSize() == null ? PageSize.SIZE15.getSize() : param.getPageSize();

		SimplePage page = new SimplePage(param.getPageNo(), count, pageSize);
		param.setSimplePage(page);
		List<Articleinfo> list = this.findListByParam(param);
		PaginationResultVO<Articleinfo> result = new PaginationResultVO(count, page.getPageSize(), page.getPageNo(), page.getPageTotal(), list);
		return result;
	}

	/**
	 * 新增
	 */
	@Override
	public Integer add(Articleinfo bean) {
		return this.articleinfoMapper.insert(bean);
	}

	/**
	 * 批量新增
	 */
	@Override
	public Integer addBatch(List<Articleinfo> listBean) {
		if (listBean == null || listBean.isEmpty()) {
			return 0;
		}
		return this.articleinfoMapper.insertBatch(listBean);
	}

	/**
	 * 批量新增或者修改
	 */
	@Override
	public Integer addOrUpdateBatch(List<Articleinfo> listBean) {
		if (listBean == null || listBean.isEmpty()) {
			return 0;
		}
		return this.articleinfoMapper.insertOrUpdateBatch(listBean);
	}

	/**
	 * 多条件更新
	 */
	@Override
	public Integer updateByParam(Articleinfo bean, ArticleinfoQuery param) {
		StringTools.checkParam(param);
		return this.articleinfoMapper.updateByParam(bean, param);
	}

	/**
	 * 多条件删除
	 */
	@Override
	public Integer deleteByParam(ArticleinfoQuery param) {
		StringTools.checkParam(param);
		return this.articleinfoMapper.deleteByParam(param);
	}

	/**
	 * 根据Id获取对象
	 */
	@Override
	public Articleinfo getArticleinfoById(Integer id) {
		return this.articleinfoMapper.selectById(id);
	}

	/**
	 * 根据Id修改
	 */
	@Override
	public Integer updateArticleinfoById(Articleinfo bean, Integer id) {
		return this.articleinfoMapper.updateById(bean, id);
	}

	/**
	 * 根据Id删除
	 */
	@Override
	public Integer deleteArticleinfoById(Integer id) {
		return this.articleinfoMapper.deleteById(id);
	}
}