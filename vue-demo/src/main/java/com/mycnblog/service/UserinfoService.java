package com.mycnblog.service;

import com.mycnblog.entity.po.Userinfo;
import com.mycnblog.entity.query.UserinfoQuery;
import com.mycnblog.entity.vo.PaginationResultVO;

import java.util.List;


/**
 *  业务接口
 */
public interface UserinfoService {

	/**
	 * 根据条件查询列表
	 */
	List<Userinfo> findListByParam(UserinfoQuery param);

	/**
	 * 根据条件查询列表
	 */
	Integer findCountByParam(UserinfoQuery param);

	/**
	 * 分页查询
	 */
	PaginationResultVO<Userinfo> findListByPage(UserinfoQuery param);

	/**
	 * 新增
	 */
	Integer add(Userinfo bean);

	/**
	 * 批量新增
	 */
	Integer addBatch(List<Userinfo> listBean);

	/**
	 * 批量新增/修改
	 */
	Integer addOrUpdateBatch(List<Userinfo> listBean);

	/**
	 * 多条件更新
	 */
	Integer updateByParam(Userinfo bean,UserinfoQuery param);

	/**
	 * 多条件删除
	 */
	Integer deleteByParam(UserinfoQuery param);

	/**
	 * 根据Id查询对象
	 */
	Userinfo getUserinfoById(Integer id);


	/**
	 * 根据Id修改
	 */
	Integer updateUserinfoById(Userinfo bean,Integer id);


	/**
	 * 根据Id删除
	 */
	Integer deleteUserinfoById(Integer id);

}