package test;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2022-12-31
 * Time: 21:23
 */
public class Test {
    public static List<List<Integer>>  generate(int numRows){
        List<List<Integer>>  list =new ArrayList<>();  //列表里存放这列表
        List<Integer>  row =new ArrayList<>();  //实例化列表的第一行
        row.add(1);   //第一行添加一个1
        list.add(row);  //把第一行整个列表添加到外面这个列表里面
        for (int i = 1; i < 5; i++) {
            List<Integer> prevRow =list.get(i-1);  //上一行
            List<Integer> curRow =new ArrayList<>();
            curRow.add(1);   //第一个位置添加1
            for (int j = 1; j < i; j++) {
                int x =prevRow.get(j)+prevRow.get(j-1);  //获取上一行j位置和j-1位置的元素之和
                curRow.add(x);  //把结果放进列表中
            }
            curRow.add(1);  //最后一个位置添加1
            list.add(curRow);
        }
        return list;
    }
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int n = scanner.nextInt();
        List<List<Integer>> list = generate(n);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }
}
