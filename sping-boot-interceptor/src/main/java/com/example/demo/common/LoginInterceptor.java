package com.example.demo.common;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * Description: 拦截器
 * User: WuYimin
 * Date: 2023-06-18
 * Time: 16:28
 */

/**
 * 拦截器
 */
@Component
public class LoginInterceptor implements HandlerInterceptor {
    //调用目标方法执行之前的方法
    //此方法返回的是boolean 类型的值
    //如果返回的true 表示(拦截器)验证成功, 继续走后续的流程,执行目标方法
    //如果返回false, 表示拦截器执行失败, 验证为通过, 后续的流程和目标方法就不要执行了
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //用户登录判断业务
        HttpSession session = request.getSession(false);
        if(session != null && session.getAttribute("session_userinfo") != null) {
            //用户已经登录
            return true;
        }
        //登录失败,页面返回一个错误状态码
        response.setStatus(500);
        return false;
    }
}
