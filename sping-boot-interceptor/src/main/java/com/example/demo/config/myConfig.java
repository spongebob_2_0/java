package com.example.demo.config;

import com.example.demo.common.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-18
 * Time: 16:44
 */
@Configuration
public class myConfig implements WebMvcConfigurer {
    @Autowired //属性注入(注入loginInterceptor对象)
    private LoginInterceptor loginInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInterceptor)
                .addPathPatterns("/**") //拦截所有url
                .excludePathPatterns("/api/user/login")  //排除url /user/login 登录不拦截
                .excludePathPatterns("/api/user/reg")     //注册不拦截
                .excludePathPatterns("/image/**")  //排除image 文件夹下的所有文件
        ;
    }
    // 所有的接口添加 com 前缀
    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        configurer.addPathPrefix("api", c -> true);
    }
}
