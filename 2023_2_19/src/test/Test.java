package test;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-02-19
 * Time: 2:26
 */
public class Test {
    public static void main(String[] args) {
        int[] array ={2,3,4,3,5,6,7,6,9,9};
        Set<Integer> set =new HashSet<>();
        for(int x: array){
            set.add(x);
        }
        System.out.println(set);
    }
}
