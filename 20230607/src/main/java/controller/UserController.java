package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-07
 * Time: 11:26
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    public void init() {
        userService.func();
    }
}
