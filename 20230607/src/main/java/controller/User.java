package controller;


import lombok.*;
import org.springframework.stereotype.Controller;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-07
 * Time: 11:17
 */
@Data
public class User {
    private String name;
    private String color;
    private int age;

    public User(String name, String color, int age) {
        this.name = name;
        this.color = color;
        this.age = age;
    }
}
