package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-06-07
 * Time: 11:17
 */
@Controller
public class UserService {

    @Autowired
    private User user;

    public void func() {
        System.out.println(user);
    }

}
