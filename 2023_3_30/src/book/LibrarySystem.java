package book;

import java.util.ArrayList;
import java.util.Scanner;

public class LibrarySystem {
    private ArrayList<Book> books; // 存储图书信息
    private Scanner scanner; // 用于读取用户输入

    public LibrarySystem() {
        books = new ArrayList<>();
        scanner = new Scanner(System.in);
    }

    public void addBook(Book book) {
        books.add(book);
    }

    public void listBooks() {
        System.out.println("书名\t\t作者\t\t状态");
        for (Book book : books) {
            System.out.printf("%s\t\t%s\t\t%s\n", book.getTitle(), book.getAuthor(), book.isAvailable() ? "可借" : "已借出");
        }
    }

    public void borrowBook(String title) {
        for (Book book : books) {
            if (book.getTitle().equals(title) && book.isAvailable()) {
                book.setAvailable(false);
                System.out.println("借阅成功！");
                return;
            }
        }
        System.out.println("抱歉，此书不可借阅。");
    }

    public void returnBook(String title) {
        for (Book book : books) {
            if (book.getTitle().equals(title) && !book.isAvailable()) {
                book.setAvailable(true);
                System.out.println("归还成功！");
                return;
            }
        }
        System.out.println("抱歉，此书不存在。");
    }

    public void start() {
        System.out.println("欢迎来到图书管理系统！");
        System.out.println("1. 管理员登录");
        System.out.println("2. 用户登录");
        System.out.print("请选择：");
        int choice = scanner.nextInt();
        scanner.nextLine(); // 清除换行符

        if (choice == 1) {
            adminLogin();
        } else {
            userLogin();
        }
    }

    private void adminLogin() {
        System.out.println("请输入密码：");
        String password = scanner.nextLine();
        if (password.equals("admin123")) {
            System.out.println("登录成功！");
            adminMenu();
        } else {
            System.out.println("密码错误！");
        }
    }

    private void adminMenu() {
        while (true) {
            System.out.println("1. 添加图书");
            System.out.println("2. 查看图书");
            System.out.println("3. 退出");
            System.out.print("请选择：");
            int choice = scanner.nextInt();
            scanner.nextLine(); // 清除换行符

            if (choice == 1) {
                addBookPrompt();
            } else if (choice == 2) {
                listBooks();
            } else {
                break;
            }
        }
    }

    private void addBookPrompt() {
        System.out.println("请输入书名：");
        String title = scanner.nextLine();
        System.out.println("请输入作者：");
        String author = scanner.nextLine();
        addBook(new Book(title, author));
        System.out.println("图书添加成功！");
    }

    private void userLogin() {
        System.out.println("请输入用户名：");
        String username = scanner.nextLine();
        System.out.println("欢迎，" + username + "！");
        userMenu();
    }

    private void userMenu() {
        while (true) {
            System.out.println("1. 查看图书");
            System.out.println("2. 借阅图书");
            System.out.println("3. 归还图书");
            System.out.println("4. 退出");
            System.out.print("请选择：");
            int choice = scanner.nextInt();
            scanner.nextLine(); // 清除换行符

            if (choice == 1) {
                listBooks();
            } else if (choice == 2) {
                System.out.println("请输入书名：");
                String title = scanner.nextLine();
                borrowBook(title);
            } else if (choice == 3) {
                System.out.println("请输入书名：");
                String title = scanner.nextLine();
                returnBook(title);
            } else {
                break;
            }
        }
    }

    public static void main(String[] args) {
        LibrarySystem librarySystem = new LibrarySystem();
        librarySystem.start();
    }
}


