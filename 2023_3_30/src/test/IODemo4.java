package test;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-31
 * Time: 2:01
 */
public class IODemo4 {
    public static void main(String[] args) {
        File file =new File("test-dir");
        //返回file对象代表的目录下所有的文件名
        String[] results1 =file.list();
        //返回file对象代表的目录下所有的文件名,以file对象表示
        File[] results2  =file.listFiles();
        System.out.println(Arrays.toString(results1));
        System.out.println(Arrays.toString(results2));
    }
}
