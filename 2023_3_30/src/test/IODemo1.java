package test;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-30
 * Time: 19:04
 */
public class IODemo1 {
    public static void main(String[] args) throws IOException {
        //创建一个新的file实例
        File file =new File("./cat.jpg");
        //file对象的父目录文件路径
        System.out.println(file.getParent());
        //文件名称
        System.out.println(file.getName());
        //file对象的相对文件路径
        System.out.println(file.getPath());
        //file对象的绝对路径
        System.out.println(file.getAbsoluteFile());
        //file对象修饰过的绝对路径
        System.out.println(file.getCanonicalFile());
    }
}
