package test;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-30
 * Time: 20:06
 */
public class IODemo2 {
    public static void main(String[] args) throws IOException {
        //在相对路劲中 ./ 可以省略
        File file =new File("./hello_word3.txt");
        //判断文件是否存在
        System.out.println(file.exists());
        //判断文件是否在同一个目录
        System.out.println(file.isDirectory());
        //是否是一个普通文件
        System.out.println(file.isFile());

//        创建文件
        file.createNewFile();
        System.out.println("===============");

        //判断文件是否存在
        System.out.println(file.exists());
        //判断文件是否在同一个目录
        System.out.println(file.isDirectory());
        //是否是一个普通文件
        System.out.println(file.isFile());
    }
}
