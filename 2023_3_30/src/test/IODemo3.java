package test;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-03-31
 * Time: 1:48
 */
public class IODemo3 {
    public static void main(String[] args) {
        File file =new File("test-dir/aaa/bbb");
        //创建file对象代表的目录,只能创建一级目录
//        file.mkdir();
        //创建多级目录
        file.mkdirs();
    }
}
