import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-08-06
 * Time: 7:52
 */
public class Main4 {

    public static void main(String[] args) throws IOException {
        File file = new File("C:/Program Files (x86)/bin");
        searchFile(file,"QQ.exe");
    }
    public static void searchFile(File dir, String fileName) throws IOException {
        if(dir == null || !dir.exists() || dir.isFile()) {
            return;
        }
        File[] files = dir.listFiles();
        if(files != null && files.length >0) {
            for (File file : files) {
                if(file.isFile()) {
                    if(file.getName().contains(fileName)){
                        System.out.println("找到了: " + file.getAbsoluteFile());
                        Runtime runtime = Runtime.getRuntime();
                        runtime.exec(file.getAbsolutePath());
                        return;
                    }else {
                        //是文件夹,继续搜索
                        searchFile(file,fileName);
                    }
                }
            }
            System.out.println("没找到!!");

        }
    }
}
