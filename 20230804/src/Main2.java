import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-08-06
 * Time: 2:36
 */
public class Main2 {
    public static void main(String[] args) throws IOException {
        File file = new File("D:/桌面/下载/test.txt");
        File file1 = new File("/桌面/下载/");
        System.out.println(file.exists());
        System.out.println(file.isFile());
        System.out.println(file.isDirectory());
        System.out.println(file.getName());
        System.out.println(file.length());
        System.out.println(file.lastModified());
        long time = file.lastModified();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MMM/dd HH:mm:ss");
        System.out.println(simpleDateFormat.format(time));
        System.out.println(file.getParent());
        System.out.println(file1.getParent());
        System.out.println(file.getAbsoluteFile());
        System.out.println(file1.getAbsoluteFile());
        File file2 = new File("D:/桌面/下载/ab.txt");
        System.out.println(file2.createNewFile());
        File file3 = new File("D:/桌面/下载/ccc/ddd/eee/fff");
        if(!file3.exists()) {
            System.out.println(file3.mkdirs());
        }
        System.out.println(file.delete());
        File file4 = new File("D:/桌面/下载/aaa/ab.txt");
//        System.out.println(file4.mkdirs());
//        System.out.println(file4.delete());
    }
}
