/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-08-13
 * Time: 16:54
 */
import java.util.*;

import java.util.*;
import java.util.*;

public class Main6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int m = scanner.nextInt();
            int n = scanner.nextInt();
            func(m,n);
        }
    }
    private static void func(int m, int n) {
        double sum = 0;
        double currentTerm = m;
        for(int i = 0; i < n; i++) {
            sum += currentTerm;
            currentTerm = Math.sqrt(currentTerm);
        }
        System.out.printf("%.2f",sum);
        System.out.println();
    }
}