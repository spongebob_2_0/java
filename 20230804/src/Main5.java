import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-08-06
 * Time: 10:33
 */
class A {

}

class B {

}

public class Main5 {
    public static void main(String[] args) {
        List<Integer> values = Arrays.asList(1, 3, 5);
        //分隔符 – 添加到 StringJoiner
        //前缀 – 开头要使用的字符序列
        //后缀 – 要在末尾使用的字符序列
        StringJoiner sj = new StringJoiner(",", "(", ")");

        for (Integer value : values) {
            sj.add(value.toString());
        }
        List<Integer> list = new ArrayList<>();
        Iterator<Integer> it = list.iterator();
        while (it.hasNext()) {
            Integer value = it.next();
            if(value != null) {
                it.remove();
                System.out.print(value+ " ");
            }
        }
        System.out.println("=======");
        for(Integer s: list) {
            System.out.print(s+" ");
        }
        HashSet<Integer> set = new HashSet<>();
        set.add(1);
        ThreadLocal<Integer> threadLocal = new ThreadLocal<>();
        threadLocal.set(1);
        System.out.println(threadLocal.get());

    }
    public static void func(String a) {

    }
    public static void func1(A a) {

    }

}
