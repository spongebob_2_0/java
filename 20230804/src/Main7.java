import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: WuYimin
 * Date: 2023-08-18
 * Time: 17:10
 */
public class Main7 {
    public static void main(String[] args) throws InterruptedException {
        Object locker = new Object();
        while (true) {
            new Thread(()->{
                synchronized (locker) {
                    try {
                        TimeUnit.SECONDS.sleep(1);
                        System.out.println(Thread.currentThread().getName()+"退出了");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },"我是1").start();
//        TimeUnit.SECONDS.sleep(1);
            Thread thread = new Thread(() -> {
                synchronized (locker) {
                    System.out.println(Thread.currentThread().getName() + "退出了");
                }
            }, "我是2");
//        thread.setPriority(9);
            thread.start();
//        TimeUnit.SECONDS.sleep(1);
            Thread thread1 = new Thread(() -> {
                synchronized (locker) {
                    System.out.println(Thread.currentThread().getName() + "退出了");
                }
            }, "我是3");
//        thread1.setPriority(1);
            thread1.start();
        }
    }
}
