package test;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DoubanTop250Crawler {
    public static void main(String[] args) {
        String url = "https://movie.douban.com/top250";
        try {
            Document document = Jsoup.connect(url).get();
            Elements elements = document.select("div#content div#wrapper div#content div#left div#main div#content ul li");
            for (Element element : elements) {
                String title = element.select("div.hd span.title").text();
                String info = element.select("div.bd p").text();
                Pattern pattern = Pattern.compile("(\\d{4}).*?([\\d.]+).*?导演:(.*?)主演:(.*?)$");
                Matcher matcher = pattern.matcher(info);
                if (matcher.find()) {
                    String year = matcher.group(1);
                    String rating = matcher.group(2);
                    String director = matcher.group(3);
                    String actors = matcher.group(4);
                    System.out.println(title + " (" + year + ") " + "评分：" + rating + " " + "导演：" + director + " " + "主演：" + actors);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
