package test;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TencentAnimeCrawler {
    public static void main(String[] args) {
        String url = "https://v.qq.com/biu/ranks/?t=anime";
        try {
            Document document = Jsoup.connect(url).get();
            Elements elements = document.select("div#list_items li.list_item");
            int count = 0;
            for (Element element : elements) {
                count++;
                if (count > 10) break;
                String title = element.select("div.figure_detail a.figure_title").text();
                String info = element.select("div.figure_detail p").text();
                Pattern pattern = Pattern.compile("播放量：(\\d+).*?评分：([\\d.]+)");
                Matcher matcher = pattern.matcher(info);
                if (matcher.find()) {
                    String playCount = matcher.group(1);
                    String rating = matcher.group(2);
                    System.out.println("排名：" + count + " " + title + " " + "播放量：" + playCount + " " + "评分：" + rating);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
