package test3;

import java.util.*;

public class Parenthesis {
    public boolean chkParenthesis(String A, int n) {
        if(n % 2 == 0) {
            return false;
        }
        Stack<Character> stack =new Stack<>();
        for(char c: A.toCharArray()) {
            if(c == '(') {
                stack.push(c);
            }else if(c == ')') {
                if(stack.isEmpty()) {
                    return false;
                }else if(stack.peek() == '(') {
                    stack.pop();
                }
            }else {
                return false;
            }
        }
        return stack.isEmpty();
    }
}