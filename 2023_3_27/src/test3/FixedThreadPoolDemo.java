package test3;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FixedThreadPoolDemo {
    public static void main(String[] args) {
        // 创建一个固定大小为 5 的线程池
        ExecutorService executor = Executors.newFixedThreadPool(5);
        // 提交 10 个任务给线程池执行
        for (int i = 0; i < 10; i++) {
            executor.execute(new Task(i));
        }
        // 关闭线程池
        executor.shutdown();
    }

    static class Task implements Runnable {
        private int taskId;

        public Task(int taskId) {
            this.taskId = taskId;
        }

        public void run() {
            System.out.println("Task #" + taskId + " is running.");
        }
    }
}
