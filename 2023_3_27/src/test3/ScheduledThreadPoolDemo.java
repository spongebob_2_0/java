package test3;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledThreadPoolDemo {
    public static void main(String[] args) {
        // 创建一个固定大小为 2 的线程池
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(2);

        // 延迟执行任务
        for (int i = 0; i < 5; i++) {
            int taskId = i + 1;
            // 提交延迟执行任务给线程池执行
            executor.schedule(new Task(taskId), i + 1, TimeUnit.SECONDS);
        }

        // 固定时间间隔执行任务
        int taskId = 6;
        // 提交固定时间间隔执行任务给线程池执行
        executor.scheduleAtFixedRate(new Task(taskId), 2, 2, TimeUnit.SECONDS);

        // 固定延迟时间执行任务
        taskId = 7;
        // 提交固定延迟时间执行任务给线程池执行
        executor.scheduleWithFixedDelay(new Task(taskId), 4, 2, TimeUnit.SECONDS);

        // 关闭线程池
        executor.shutdown();
    }

    static class Task implements Runnable {
        private int taskId;

        public Task(int taskId) {
            this.taskId = taskId;
        }

        public void run() {
            System.out.println("Task #" + taskId + " is running.");
        }
    }
}
