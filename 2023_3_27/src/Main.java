import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int n =scanner.nextInt();
        String[] array =new String[n];
        for(int i = 0; i < array.length; i++) {
            array[i] = scanner.nextLine();
        }
        boolean flag1 =true;
        boolean flag2 =true;
        for(int i = 0; i< array.length-1; i++) {
            if(flag1 && (array[i].length() > array[i+1].length())) {
                flag1 =false;
            }
            if(flag2 && (array[i].charAt(i) > array[i+1].charAt(i))) {
                flag2 =false;
            }
        }
        if(flag1 == false && flag2 == true) {
            System.out.println("lexicographically");
        }else if(flag1 == true && flag2 == false) {
            System.out.println("lengths");
        }else if(flag1 == false && flag2 == false) {
            System.out.println("none");
        }else {
            System.out.println("both");
        }
    }
}