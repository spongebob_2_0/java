package test2;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

//自定义一个线程池
class MyThreadPool {
    //阻塞队列用来存放任务
    private BlockingQueue<Runnable> queue =new LinkedBlockingDeque<>();
 
    public void submit(Runnable runnable) throws InterruptedException {
        queue.put(runnable);
    }
    //此处实现一个固定线程数的线程池
        public MyThreadPool(int n) {
        for (int i = 0; i < n; i++) {
            Thread t =new Thread(() -> {
                    try {
                        while (true) {
                            //取任务
                            Runnable runnable = queue.take();
                            runnable.run();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
            });
            t.start();
        }
    }
}
public class ThreadDemo1 {
    public static void main(String[] args) throws InterruptedException {
        MyThreadPool myThreadPool =new MyThreadPool(10);
        for (int i = 0; i < 1000; i++) {
            int number =i;
            myThreadPool.submit(new Runnable() {
                @Override
                public void run() {
                    System.out.println("hello "+number);
                }
            });
        }
    }
}